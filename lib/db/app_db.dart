import 'dart:developer';

import 'package:ashera_flutter/models/member_model.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart';

import '../models/chat_message_model.dart';

class AppDb {
  final String _friendTable = 'Friend';
  final String _messageTable = 'Message';
  final String _blacklistTable = 'blacklist';
  late Database database;

  //初始化
  void initDatabase() async {
    database = await openDatabase(join(await getDatabasesPath(), 'app_db'),
        onCreate: (db, version) => _createDb(db, version),
        onUpgrade: (db, oldVersion, newVersion) => _upGrade(db, oldVersion, newVersion), version: 10);
  }

  //建立
  void _createDb(Database db, int newVersion) async {
    //建立db
    Batch batch = db.batch();
    batch.execute(
        "CREATE TABLE $_friendTable(id INTEGER PRIMARY KEY UNIQUE, name TEXT, nickname TEXT, gender INTEGER, height REAL, weight REAL, birthday TEXT, cellphone TEXT, mugshot TEXT, zodiacSign TEXT, bloodType TEXT, aboutMe TEXT, job TEXT, asheraUid TEXT, updatedAt TEXT, latitude REAL, longitude REAL, faceId TEXT, verify INTEGER, reVerify INTEGER, facePic TEXT, s3Etag TEXT, vip INTEGER, activeStart INTEGER, activeEnd INTEGER, hideBirthday INTEGER, status INTEGER, initGame INTEGER, initGameAt INTEGER, interactivePic TEXT)"); //<-好友表 可依照此格式往下開多個表
    batch.execute(
        "CREATE TABLE $_messageTable(id INTEGER, fromMember TEXT, targetMember TEXT, content TEXT, type INTEGER, isRead INTEGER, status INTEGER, createdAt TEXT, updatedAt TEXT, block INTEGER)"); //<-聊天紀錄表 可依照此格式往下開多個表
    batch.execute(
        "CREATE TABLE $_blacklistTable(id INTEGER PRIMARY KEY UNIQUE, fromMemberId INTEGER, targetMemberId INTEGER)");
    await batch.commit(noResult: true, continueOnError: true);
  }
  
  //更新
  void _upGrade(Database db, int oldVersion, int newVersion) async {
    Batch batch = db.batch();
    //v9 To v10
    if(oldVersion == 9){
      batch.execute('ALTER TABLE $_friendTable ADD interactivePic TEXT');
    }
    await batch.commit();
  }

  //新增好友
  Future<void> insertFriend(MemberModel _friend) async {
    final db = database;
    await db.insert(_friendTable, _friend.insertDB(),
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  //搜尋
  Future<List<MemberModel>> queryFriend(int id) async {
    final db = database;
    List<Map<String, dynamic>> result =
        await db.query(_friendTable, where: 'id = ?', whereArgs: [id]);
    if (result.isNotEmpty) {
      return List.generate(
          result.length,
          (index) => MemberModel(
                id: result[index]['id'],
                name: result[index]['name'],
                nickname: result[index]['nickname'],
                gender: result[index]['gender'],
                height: result[index]['height'],
                weight: result[index]['weight'],
                birthday: result[index]['birthday'],
                cellphone: result[index]['cellphone'],
                mugshot: result[index]['mugshot'],
                zodiacSign: result[index]['zodiacSign'],
                bloodType: result[index]['bloodType'],
                aboutMe: result[index]['aboutMe'],
                job: result[index]['job'],
                asheraUid: result[index]['asheraUid'],
                updatedAt: result[index]['updatedAt'],
                latitude: result[index]['latitude'],
                longitude: result[index]['longitude'],
                faceId: result[index]['faceId'],
                verify: result[index]['verify'],
                facePic: result[index]['facePic'],
                s3Etag: result[index]['s3Etag'],
                vip: result[index]['vip'],
                activeStart: result[index]['activeStart'],
                activeEnd: result[index]['activeEnd'],
                hideBirthday: result[index]['hideBirthday'],
                status: result[index]['status'],
                initGame: result[index]['initGame'],
                initGameAt: result[index]['initGameAt'],
                interactivePic: result[index]['interactivePic']
              ));
    } else {
      return [];
    }
  }

  //取得好友
  Future<List<MemberModel>> getDbFriends() async {
    final db = database;
    final List<Map<String, dynamic>> maps = await db.query(_friendTable);
    return List.generate(
        maps.length,
        (index) => MemberModel(
              id: maps[index]['id'],
              name: maps[index]['name'],
              nickname: maps[index]['nickname'],
              gender: maps[index]['gender'],
              height: maps[index]['height'],
              weight: maps[index]['weight'],
              birthday: maps[index]['birthday'],
              cellphone: maps[index]['cellphone'],
              mugshot: maps[index]['mugshot'],
              zodiacSign: maps[index]['zodiacSign'],
              bloodType: maps[index]['bloodType'],
              aboutMe: maps[index]['aboutMe'],
              job: maps[index]['job'],
              asheraUid: maps[index]['asheraUid'],
              updatedAt: maps[index]['updatedAt'],
              latitude: maps[index]['latitude'],
              longitude: maps[index]['longitude'],
              faceId: maps[index]['faceId'],
              verify: maps[index]['verify'],
              /*reVerify: maps[index]['reVerify'],*/
              facePic: maps[index]['facePic'],
              s3Etag: maps[index]['s3Etag'],
              vip: maps[index]['vip'],
              activeStart: maps[index]['activeStart'],
              activeEnd: maps[index]['activeEnd'],
              hideBirthday: maps[index]['hideBirthday'],
              status: maps[index]['status'],
              initGame: maps[index]['initGame'],
              initGameAt: maps[index]['initGameAt'],
              interactivePic: maps[index]['interactivePic']
            ));
  }

  //更新好友 內容
  Future<void> updateFriend(MemberModel _friend) async {
    final db = database;
    await db.update(_friendTable, _friend.insertDB(),
        where: 'id = ?', whereArgs: [_friend.id]);
  }
  //更新好友 訊息時間
  Future<void> updateFriendUpdateAt(MemberModel _friend) async{
    final db = database;
    await db.update(_friendTable, _friend.insertDBUpdateAt(),
        where: 'id = ?', whereArgs: [_friend.id]);
  }

  //更新好友 內容
  Future<void> updateFriendNickName(MemberModel _friend) async {
    //log('更新好友');
    final db = database;
    await db.update(_friendTable, _friend.insertDBNickname(),
        where: 'id = ?', whereArgs: [_friend.id]);
  }

  //刪除好友
  Future<void> deleteFriend(int id) async {
    final db = database;
    await db.delete(_friendTable, where: 'id = ?', whereArgs: [id]);
  }

  //刪除全部好友
  Future<void> deleteAllFriend() async {
    final db = database;
    await db.delete(_friendTable, where: null, whereArgs: null);
  }

  //新增訊息
  Future<void> insertMessage(ChatMessageModel _message) async {
    final db = database;
    await db.insert(_messageTable, _message.toJson(),
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  //取得訊息
  Future<List<ChatMessageModel>> queryMessage(String name) async {
    final db = database;
    List<Map<String, dynamic>> result = await db.query(_messageTable,
        where: 'fromMember = ? OR targetMember = ?', whereArgs: [name, name]);
    if (result.isNotEmpty) {
      return List.generate(
          result.length,
          (index) => ChatMessageModel(
              id: result[index]['id'],
              fromMember: result[index]['fromMember'],
              targetMember: result[index]['targetMember'],
              content: result[index]['content'],
              type: result[index]['type'],
              isRead: result[index]['isRead'],
              status: result[index]['status'],
              createdAt: result[index]['createdAt'],
              updatedAt: result[index]['updatedAt'],
              block: result[index]['block']
          ));
    } else {
      return [];
    }
  }

  //刪除訊息
  Future<void> deleteMessage(int id) async {
    final db = database;
    await db.delete(_messageTable, where: 'id = ?', whereArgs: [id]);
  }

  //刪除全部訊息
  Future<void> deleteAllMessage() async {
    final db = database;
    await db.delete(_messageTable, where: null, whereArgs: null);
  }

  ///新增黑名單
  Future<void> insertBlacklist(MemberBlackModel _blacklist) async {
    final db = database;
    await db.insert(_blacklistTable, _blacklist.toJson(),
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  ///搜尋黑名單
  Future<List<MemberBlackModel>> queryBlacklist(int id) async {
    final db = database;
    List<Map<String, dynamic>> result =
        await db.query(_blacklistTable, where: 'id = ?', whereArgs: [id]);
    if (result.isNotEmpty) {
      return List.generate(
          result.length,
          (index) => MemberBlackModel(
              id: result[index]['id'],
              fromMemberId: result[index]['fromMemberId'],
              targetMemberId: result[index]['targetMemberId']));
    } else {
      return [];
    }
  }

  ///取得黑名單
  Future<List<MemberBlackModel>> getBlacklist() async {
    final db = database;
    final List<Map<String, dynamic>> maps = await db.query(_blacklistTable);
    return List.generate(
        maps.length,
        (index) => MemberBlackModel(
            id: maps[index]['id'],
            fromMemberId: maps[index]['fromMemberId'],
            targetMemberId: maps[index]['targetMemberId']));
  }

  ///刪除黑名單 (某人)
  Future<void> deleteBlacklistMember(int id) async {
    final db = database;
    await db.delete(_blacklistTable, where: 'id = ?', whereArgs: [id]);
  }
  ///刪除全部
  Future<void> deleteBlacklist() async {
    final db = database;
    await db.delete(_blacklistTable, where: null, whereArgs: null);
  }
}
