import 'package:ashera_flutter/app.dart';
import 'package:ashera_flutter/generated/l10n.dart';
import 'package:ashera_flutter/provider/member_provider.dart';
import 'package:ashera_flutter/provider/util_api_provider.dart';
import 'package:ashera_flutter/utils/app_color.dart';
import 'package:ashera_flutter/utils/no_shadow_scroll_behavior.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../widget/button_widget.dart';
import '../widget/titlebar_widget.dart';

//意見
class OpinionPage extends StatefulWidget {
  const OpinionPage({Key? key}) : super(key: key);

  @override
  State createState() => _OpinionPageState();
}

class _OpinionPageState extends State<OpinionPage> {
  UtilApiProvider? _utilApiProvider;
  MemberProvider? _memberProvider;
  //List<DropdownMenuItem<String>> items = [];
  //String values = 'type1';
  final TextEditingController _input = TextEditingController();

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  _onLayoutDone(_) async {
    /*items.add(const DropdownMenuItem(
      child: Text('type1'),
      value: 'type1',
    ));
    items.add(const DropdownMenuItem(
      child: Text('type2'),
      value: 'type2',
    ));
    items.add(const DropdownMenuItem(
      child: Text('type3'),
      value: 'type3',
    ));
    items.add(const DropdownMenuItem(
      child: Text('type4'),
      value: 'type4',
    ));
    items.add(const DropdownMenuItem(
      child: Text('type5'),
      value: 'type5',
    ));
    setState(() {});*/
  }

  @override
  Widget build(BuildContext context) {
    _utilApiProvider = Provider.of<UtilApiProvider>(context);
    _memberProvider = Provider.of<MemberProvider>(context);
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: AppColor.appBackgroundColor,
      body: Column(
        children: [
          //返回 與 標題
          //titleBar(S.of(context).opinion),
          titleBarIntegrate([_backButtonWidget()], S.of(context).opinion, []),
          //內容
          Expanded(flex: 10, child: _body())
        ],
      ),
    );
  }

  //返回按鈕
  Widget _backButtonWidget() {
    return GestureDetector(
      onTap: () => {delegate.popRoute()},
      child: const Icon(
        Icons.arrow_back,
        color: AppColor.appTitleBarTextColor,
        size: 30,
      ),
    );
  }

  //內容
  Widget _body() {
    return ScrollConfiguration(
      behavior: NoShadowScrollBehavior(),
      child: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.only(top: 2.h),
          padding: const EdgeInsets.symmetric(horizontal: 40),
          color: Colors.white,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: 2.h,
              ),
              //問題類型
              /*Text(
                S.of(context).question_type,
                style: TextStyle(fontSize: 20.sp, fontWeight: FontWeight.bold),
              ),*/
              SizedBox(
                height: 1.h,
              ),
              //類型
              //_typeField(),
              SizedBox(
                height: 5.h,
              ),
              //意見
              Text(
                S.of(context).opinion,
                style: TextStyle(fontSize: 20.sp, fontWeight: FontWeight.bold),
              ),
              SizedBox(
                height: 1.h,
              ),
              //意見輸入框
              opinionInput(),
              //送出
              _sendButton()
            ],
          ),
        ),
      ),
    );
  }

  //問題類型
  /*Widget _typeField() {
    return Container(
      decoration: BoxDecoration(
          border: Border.all(color: AppColor.grayLine, width: 1),
          borderRadius: BorderRadius.circular(5)),
      width: 80.w,
      height: 8.h,
      child: ButtonTheme(
        alignedDropdown: true,
        child: DropdownButtonHideUnderline(
          child: DropdownButton(
            alignment: Alignment.center,
            hint: Text(S.of(context).select_question_type),
            dropdownColor: Colors.white,
            icon: const Icon(Icons.keyboard_arrow_down_rounded),
            items: items,
            value: values,
            onChanged: (String? _value) {
              setState(() {
                values = _value!;
              });
            },
            isExpanded: true,
            iconSize: 40,
          ),
        ),
      ),
    );
  }*/

  //意見輸入框
  Widget opinionInput() {
    return TextField(
      style: const TextStyle(color: Colors.black),
      decoration: InputDecoration(
        contentPadding: const EdgeInsets.all(5),
        enabledBorder: OutlineInputBorder(
            borderSide: const BorderSide(color: AppColor.grayLine),
            borderRadius: BorderRadius.circular(5)),
        border: OutlineInputBorder(borderRadius: BorderRadius.circular(5)),
        fillColor: Colors.white,
        hintText: S.of(context).opinion_content,
        filled: true,
      ),
      maxLines: 8,
      keyboardType: TextInputType.multiline,
      textInputAction: TextInputAction.newline,
      controller: _input,
    );
  }

  //送出
  Widget _sendButton() {
    return Container(
      width: MediaQuery.of(context).size.width,
      alignment: Alignment.center,
      child: GestureDetector(
          onTap: () => _sendOnTap(),
          child: Container(
              width: 58.w,
              height: 7.h,
              margin: EdgeInsets.symmetric(vertical: 5.h),
              child: buttonWidget(S.of(context).send))),
    );
  }

  //送出事件
  void _sendOnTap() {
    debugPrint('送出意見');
    if(_input.text.isNotEmpty){
      _utilApiProvider!.postSuggestion(_memberProvider!.memberModel.id!, _input.text, '').then((value) {
        if(value){
          EasyLoading.showSuccess(S.of(context).handle_it_for_you_now);
          //傳送成功
          delegate.popRoute();
        }
      });
    } else {
      EasyLoading.showToast(S.of(context).opinion_content);
    }
  }
}
