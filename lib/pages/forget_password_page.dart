import 'dart:async';

import 'package:ashera_flutter/provider/auth_provider.dart';
import 'package:ashera_flutter/utils/app_color.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../app.dart';
import '../generated/l10n.dart';
import '../models/member_model.dart';
import '../utils/app_image.dart';
import '../utils/app_size.dart';
import '../utils/no_shadow_scroll_behavior.dart';
import '../utils/util.dart';
import '../widget/titlebar_widget.dart';

//忘記密碼頁
class ForgetPasswordPage extends StatefulWidget{
  const ForgetPasswordPage({Key? key}): super(key: key);

  @override
  State createState() => _ForgetPasswordPageState();
}

class _ForgetPasswordPageState extends State<ForgetPasswordPage>{
  AuthProvider? _authProvider;
  //表單驗證
  final _formKey = GlobalKey<FormState>();
  final TextEditingController _account = TextEditingController();
  final TextEditingController _verification = TextEditingController();
  final TextEditingController _password = TextEditingController();
  //是否顯示密碼
  bool isShowPassword = false;
  //是否發送驗證碼
  bool isSendVerification = false;
  //發送倒數秒數
  Timer? _timer;
  int _countdownTime = 0;

  // 點擊控制密碼是否顯示
  void showPassword(){
    setState(() {
      isShowPassword = !isShowPassword;
    });
  }

  _onLayoutDone(_) async {
    debugPrint('忘記密碼頁');
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  void dispose() {
    super.dispose();
    if(_timer != null){
      _timer!.cancel();
      _timer = null;
    }
  }

  @override
  Widget build(BuildContext context) {
    _authProvider = Provider.of<AuthProvider>(context);
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: AppColor.appBackgroundColor,
      body: Column(
        children: [
          //返回 與 標題
          titleBar(S.of(context).forgot_password),
          //內容
          Expanded(flex: 10, child: _body())
        ],
      ),
    );
  }

  //內容
  Widget _body(){
    return Container(
      decoration: const BoxDecoration(
        color: Colors.transparent
      ),
      child: ScrollConfiguration(
          behavior: NoShadowScrollBehavior(),
          child: SingleChildScrollView(
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(height: 3.h,),
                  //AppIcon與名稱
                  _appNameAndIcon(),
                  //輸入框
                  _input(),
                  //送出按鈕
                  SizedBox(height: 4.h,),
                  _sendButton(),
                ],
              ),
            ),
          )),
    );
  }

  //region AppIcon與名稱
  Widget _appNameAndIcon() {
    return Image(
      height: 15.h,
      filterQuality: FilterQuality.medium,
      image: AssetImage(AppImage.logo),
    );
  }
  //endregion

  //region 輸入框
  Widget _input() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        _accountTextField(),
        _verificationTextField(),
        SizedBox(height: 1.h,),
        _passwordTextField()],
    );
  }
  //帳號
  Widget _accountTextField() {
    return SizedBox(
      width: 80.w,
      height: 7.h,
      child: TextFormField(
        style: const TextStyle(color: Colors.black),
        decoration: InputDecoration(
          fillColor: Colors.transparent,
          filled: true,
          hintStyle: const TextStyle(color: Colors.grey),
          hintText: S.of(context).enter_a_phone_number,
        ),
        keyboardType: TextInputType.number,
        textInputAction: TextInputAction.next,
        controller: _account,
        validator: (value){
          if(value == null || value.isEmpty){
            return S.of(context).enter_a_phone_number;
          } else if(!Util().phoneVerification(value)){
            return S.of(context).mobile_number_entered_incorrectly;
          }
          return null;
        },
      ),
    );
  }

  //密碼
  Widget _passwordTextField() {
    return SizedBox(
      width: 80.w,
      height: 8.h,
      child: TextFormField(
        style: const TextStyle(color: Colors.black),
        decoration: InputDecoration(
          suffixIcon: Container(
            padding: const EdgeInsets.only(right: 8.0),
            child: IconButton(
              icon: Icon(
                isShowPassword ? Icons.visibility : Icons
                    .visibility_off,
                color: Colors.grey[600],
                size: 25,
              ),
              onPressed: () {
                setState(() {
                  isShowPassword = !isShowPassword;
                });
              },
            ),
          ),
          fillColor: Colors.transparent,
          filled: true,
          hintStyle: const TextStyle(color: Colors.grey),
          hintText: S.of(context).enter_password,
        ),
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.done,
        controller: _password,
        obscureText: !isShowPassword,
        validator: (value){
          if(value == null || value.isEmpty){
            return S.of(context).enter_password;
          }else if(value.trim().length < 6 || value.trim().length > 12){
            return S.of(context).enter_alphanumeric_characters;
          }
          return null;
        },
      ),
    );
  }

  //驗證碼
  Widget _verificationTextField(){
    return SizedBox(
      width: 80.w,
      height: 8.h,
      child: Stack(
        alignment: Alignment.center,
        children: [
          TextFormField(
            style: const TextStyle(color: Colors.black),
            decoration: InputDecoration(
              fillColor: Colors.transparent,
              filled: true,
              hintStyle: const TextStyle(color: Colors.grey),
              hintText: S.of(context).enter_confirmation_code,
            ),
            keyboardType: TextInputType.number,
            textInputAction: TextInputAction.next,
            controller: _verification,
            validator: (value){
              if(value == null || value.isEmpty){
                return S.of(context).enter_confirmation_code;
              }
              return null;
            },
          ),
          Positioned(right: 0, child: _sendSms())
        ],
      ),
    );
  }

  Widget _sendSms(){
    return GestureDetector(
      onTap: () => _sendSmsOnTap(),
      child: Container(
        height: 5.h,
        width: 25.w,
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: AppColor.yellowButton,
          borderRadius: BorderRadius.circular(30)
        ),
        child: Text(
            isSendVerification 
            ? '$_countdownTime s'
            : S.of(context).send_verification_code,
            style: TextStyle(color: AppColor.buttonTextColor, fontSize: 16.sp),
        ),
      ),
    );
  }

  void _sendSmsOnTap(){
    if(_account.text.trim().isNotEmpty){
      _reciprocal();
      _authProvider!.forgotPasswordVerificationCode(phone: _account.text.trim()).then((value) {
        if(value){
          EasyLoading.showSuccess(S.of(context).send);
        }else{
          EasyLoading.showError(S.of(context).fail);
        }
      });
    } else {
      //帳號為空
      EasyLoading.showToast(S.of(context).enter_a_phone_number);
    }
  }

  void _reciprocal() {
    if(_countdownTime == 0 && !isSendVerification){
      _countdownTime = 60;
      isSendVerification = true;
      setState(() {});
      _startCountdownTimer();
    }
  }

  void _startCountdownTimer(){
    _timer = Timer.periodic(const Duration(milliseconds: 1000), (timer) {
      if(mounted){
        setState(() {
          if(_countdownTime < 1){
            _timer!.cancel();
            _timer = null;
            isSendVerification = false;
          }else{
            _countdownTime--;
          }
        });
      }
    });
  }

  //endregion

  //region 送出按鈕
  Widget _sendButton(){
    return GestureDetector(
      onTap: () => _sendOnTap(),
      child: Container(
        width: AppSize.buttonW,
        height: AppSize.buttonH,
        alignment: Alignment.center,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(AppSize.buttonCircular),
            gradient: AppColor.appMainColor
        ),
        child: FittedBox(
          child: Text(S.of(context).send, style: TextStyle(color: AppColor.buttonTextColor, fontSize: 18.sp, fontWeight: FontWeight.bold),),
        ),
      ),
    );
  }

  void _sendOnTap(){
    if(_account.text.trim().isEmpty){
      //帳號為空
      EasyLoading.showToast(S.of(context).enter_a_phone_number);
      return;
    } else if(_verification.text.trim().isEmpty){
      //驗證碼為空
      EasyLoading.showToast(S.of(context).enter_confirmation_code);
      return;
    } else if(_password.text.trim().isEmpty){
      //密碼為空
      EasyLoading.showToast(S.of(context).enter_password);
      return;
    } else {
      debugPrint('送出更改密碼');
      EasyLoading.show(status: 'Loading...');
      MemberForgotPasswordDTO _memberForgotPassword = MemberForgotPasswordDTO(
          number: _account.text,
          code: _verification.text,
          password: _password.text.trim());
      _authProvider!.forgotPassword(memberForgotPasswordDTO: _memberForgotPassword).then((value) {
        if(value){
          EasyLoading.showSuccess(S.of(context).finish);
          delegate.popRoute();
        } else {
          EasyLoading.showError(S.of(context).fail);
        }
      });
    }

  }
  //endregion
}