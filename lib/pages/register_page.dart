
import 'package:ashera_flutter/app.dart';
import 'package:ashera_flutter/utils/util.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../enum/app_enum.dart';
import '../generated/l10n.dart';
import '../models/member_model.dart';
import '../routers/route_name.dart';
import '../utils/app_color.dart';
import '../utils/app_size.dart';
import '../utils/no_shadow_scroll_behavior.dart';
import '../utils/shared_preference.dart';
import '../widget/titlebar_widget.dart';
/*
* 08/04
* 1.反饋強制驗證手機
* 2.頭像與人臉辨識在此頁不需要
* */
class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  State createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  SharedPreferenceUtil sharedPreferenceUtil = SharedPreferenceUtil();
  //UtilApiProvider? _apiProvider;
  //PhotoType _photoType = PhotoType.tmp;

  /*String _localPic = '';
  String _filename = '';
  String _localdetectPic = '';
  String _detectname = '';*/
  //表單驗證
  final _formKey = GlobalKey<FormState>();
  //驗證狀態管理
  //AuthProvider? _authProvider;
  //MemberProvider? _memberProvider;
  //StompClientProvider? _stompClientProvider;

  final TextEditingController _nickName = TextEditingController();
  final TextEditingController _account = TextEditingController();
  final TextEditingController _password = TextEditingController();
  final TextEditingController _checkPassword = TextEditingController();
  final TextEditingController _gender = TextEditingController();
  final TextEditingController _birthday = TextEditingController();

  //是否顯示密碼
  bool isShowPassword = false;
  //確認密碼是否顯示密碼
  bool isShowCheckPassword = false;

  //性別
  Gender values = Gender.male;
  List<DropdownMenuItem<Gender>> items = [];

  //用來存生日
  Map<String, String> _birthDayMap = {};

  // 點擊控制密碼是否顯示
  void showPassWord() {
    setState(() {
      isShowPassword = !isShowPassword;
    });
  }

  void showCheckPassWord() {
    setState(() {
      isShowCheckPassword = !isShowCheckPassword;
    });
  }

  _onLayoutDone(_) async {
    items.add(DropdownMenuItem(
      child: Text(S.of(context).male),
      value: Gender.male,
    ));
    items.add(DropdownMenuItem(
      child: Text(S.of(context).female),
      value: Gender.female,
    ));
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    //_authProvider = Provider.of<AuthProvider>(context);
    //_apiProvider = Provider.of<UtilApiProvider>(context);
    //_stompClientProvider = Provider.of<StompClientProvider>(context);
    //_memberProvider = Provider.of<MemberProvider>(context);
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: AppColor.appBackgroundColor,
      body: Column(
        children: [
          //返回 與 標題
          titleBar(S.of(context).register),
          //內容
          Expanded(flex: 10, child: _body())
        ],
      ),
    );
  }

  //內容
  Widget _body() {
    return Container(
      decoration: const BoxDecoration(
        color: Colors.transparent,
      ),
      child: ScrollConfiguration(
          behavior: NoShadowScrollBehavior(),
          child: SingleChildScrollView(
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  //大頭照
                  //_avatarImage(),
                  Container(
                    height: 15.h,
                  ),
                  //暱稱
                  _nickNameTextField(),
                  //手機號碼
                  _accountTextField(),
                  //密碼
                  _passwordTextField(),
                  //再次輸入密碼
                  _checkPasswordTextField(),
                  //性別
                  _genderTextField(),
                  //生日
                  _birthdayTextField(),
                  //註冊
                  _registerButton(),
                ],
              ),
            ),
          )),
    );
  }

  //大頭照與人臉
  /*Widget _avatarImage() {
    return Container(
      margin: const EdgeInsets.only(left: 50, right: 50, bottom: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          //人臉辨識
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                height: 2.h,
              ),
              SizedBox(
                width: 25.w,
                height: 10.h,
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    _localdetectPic.isEmpty
                        ? Container(
                            width: 20.w,
                            height: 10.h,
                            decoration: const BoxDecoration(
                                shape: BoxShape.circle,
                                color: AppColor.appFaceBackgroundColor),
                            child: const CircleAvatar(
                              backgroundColor: Colors.transparent,
                              child: Icon(
                                FontAwesome5Solid.user,
                                size: 40,
                                color: Colors.white,
                              ),
                            ),
                          )
                        : Container(
                            width: 20.w,
                            height: 10.h,
                            decoration: const BoxDecoration(
                                shape: BoxShape.circle,
                                color: AppColor.appFaceBackgroundColor),
                            child: CircleAvatar(
                              backgroundImage:
                                  Image.file(File(_localdetectPic)).image,
                            ),
                          ),
                    //+號
                    Positioned(
                        right: 0,
                        bottom: 0,
                        child: GestureDetector(
                          onTap: () => _chooseFace(),
                          child: Stack(
                            alignment: Alignment.center,
                            children: [
                              Positioned.fill(
                                  child: Container(
                                margin: const EdgeInsets.all(10),
                                color: Colors.white,
                              )),
                              const Icon(
                                Icons.add_circle,
                                color: AppColor.addButtonBackgroundColor,
                                size: 35,
                              )
                            ],
                          ),
                        ))
                  ],
                ),
              ),
              SizedBox(
                height: 1.h,
              ),
              FittedBox(
                child: Text(
                  S.of(context).face_recognition,
                  style: TextStyle(
                      color: AppColor.appTitleBarTextColor, fontSize: 16.sp),
                ),
              )
            ],
          ),
          //大頭照
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                height: 2.h,
              ),
              SizedBox(
                width: 25.w,
                height: 10.h,
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    _localPic.isEmpty
                        ? Container(
                            width: 20.w,
                            height: 10.h,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                gradient: AppColor.appMainColor),
                            child: const CircleAvatar(
                              backgroundColor: Colors.transparent,
                              child: Icon(
                                FontAwesome5Solid.user,
                                size: 40,
                                color: Colors.white,
                              ),
                            ),
                          )
                        : Container(
                            width: 20.w,
                            height: 10.h,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                gradient: AppColor.appMainColor),
                            child: CircleAvatar(
                              backgroundImage:
                                  Image.file(File(_localPic)).image,
                            ),
                          ),
                    //+號
                    Positioned(
                        right: 0,
                        bottom: 0,
                        child: GestureDetector(
                          onTap: () => _changePhoto(),
                          child: Stack(
                            alignment: Alignment.center,
                            children: [
                              Positioned.fill(
                                  child: Container(
                                margin: const EdgeInsets.all(10),
                                color: Colors.white,
                              )),
                              const Icon(
                                Icons.add_circle,
                                color: AppColor.addButtonBackgroundColor,
                                size: 35,
                              )
                            ],
                          ),
                        ))
                  ],
                ),
              ),
              SizedBox(
                height: 1.h,
              ),
              FittedBox(
                child: Text(
                  S.of(context).profile_picture,
                  style: TextStyle(
                      color: AppColor.appTitleBarTextColor, fontSize: 16.sp),
                ),
              )
            ],
          )
        ],
      ),
    );
  }*/

  //修改照片
  /*void _changePhoto() {
    if (_account.text.isEmpty) {
      EasyLoading.showToast(S.of(context).enter_a_phone_number);
      return;
    }
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return _chooseImage();
        });
  }*/

  //拍照或相簿選擇框
  /*Widget _chooseImage() {
    return Container(
      height: 30.h,
      padding: const EdgeInsets.only(top: 10),
      child: Column(
        children: [
          //相片選擇
          Text(
            S.of(context).photo_selection,
            style: TextStyle(
                color: AppColor.appTitleBarTextColor, fontSize: 18.sp),
          ),
          SizedBox(
            height: 2.h,
          ),
          //拍照
          GestureDetector(
            onTap: () => _takePhoto(),
            child: Container(
              width: 70.w,
              height: 9.h,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: Colors.white,
                  border:
                      Border.all(color: AppColor.buttonFrameColor, width: 1),
                  borderRadius: BorderRadius.circular(30)),
              child: Text(
                S.of(context).photograph,
                style: TextStyle(
                    color: AppColor.buttonFrameColor, fontSize: 20.sp),
              ),
            ),
          ),
          SizedBox(
            height: 2.h,
          ),
          //相簿
          GestureDetector(
            onTap: () => _selectImage(),
            child: Container(
              width: 70.w,
              height: 9.h,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: Colors.white,
                  border:
                      Border.all(color: AppColor.buttonFrameColor, width: 1),
                  borderRadius: BorderRadius.circular(30)),
              child: Text(
                S.of(context).album,
                style: TextStyle(
                    color: AppColor.buttonFrameColor, fontSize: 20.sp),
              ),
            ),
          )
        ],
      ),
    );
  }*/

  //打開相機
  /*void _takePhoto() {
    ImagePickers.openCamera(
            cameraMimeType: CameraMimeType.photo,
            cropConfig: CropConfig(enableCrop: false, width: 2, height: 3))
        .then((media) {
      if (_account.text.isNotEmpty) {
        _apiProvider!.memberNameExist(_account.text).then((value) {
          if (value) {
            if (media != null) {
              _localPic = media.path.toString();
              setState(() {});
              //上傳圖片並顯示成功或失敗
              EasyLoading.show(status: S.of(context).uploading);
              _photoType = PhotoType.mugshot;
              _apiProvider!
                  .upDataImage(_account.text, media.path!, _photoType)
                  .then((value) {
                debugPrint('拍照成功或失敗: ${value['status']}');
                if (value['status']) {
                  EasyLoading.showToast(S.of(context).finish);
                  _filename = value['filename'];
                } else {
                  EasyLoading.showToast(S.of(context).fail);
                }
              });
            }
          } else {
            EasyLoading.showToast(S.of(context).duplicate_registered);
          }
        });
      } else {
        EasyLoading.showToast(S.of(context).enter_a_phone_number);
      }
    });
  }*/

  //打開相簿
  /*void _selectImage() async {
    ImagePickers.pickerPaths(
            galleryMode: GalleryMode.image,
            showGif: false,
            selectCount: 1,
            showCamera: false,
            cropConfig: CropConfig(enableCrop: false, height: 1, width: 1),
            compressSize: 500,
            uiConfig: UIConfig(uiThemeColor: AppColor.buttonFrameColor))
        .then((media) {
      if (_account.text.isNotEmpty) {
        _apiProvider!.memberNameExist(_account.text).then((value) {
          if (value) {
            if (media.first.path != null) {
              _localPic = media.first.path.toString();
              setState(() {});
              //上傳圖片並顯示成功或失敗
              EasyLoading.show(status: S.of(context).uploading);
              _photoType = PhotoType.mugshot;
              _apiProvider!
                  .upDataImage(_account.text, media.first.path!, _photoType)
                  .then((value) {
                debugPrint('拍照成功或失敗: ${value['status']}');
                if (value['status']) {
                  EasyLoading.showToast(S.of(context).finish);
                  _filename = value['filename'];
                } else {
                  EasyLoading.showToast(S.of(context).fail);
                }
              });
            }
          } else {
            EasyLoading.showToast(S.of(context).duplicate_registered);
          }
        });
      } else {
        EasyLoading.showToast(S.of(context).enter_a_phone_number);
      }
    });
  }*/

  /*void _chooseFace(){
    if (_account.text.isEmpty) {
      EasyLoading.showToast(S.of(context).enter_a_phone_number);
      return;
    }
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return _chooseFaceImage();
        });
  }*/

  //人臉
  /*Widget _chooseFaceImage() {
    return Container(
      height: 18.h,
      padding: const EdgeInsets.only(top: 10),
      child: Column(
        children: [
          //相片選擇
          Text(
            S.of(context).photo_selection,
            style: TextStyle(
                color: AppColor.appTitleBarTextColor, fontSize: 18.sp),
          ),
          SizedBox(
            height: 2.h,
          ),
          //拍照
          GestureDetector(
            onTap: () => _takeFacePhoto(),
            child: Container(
              width: 70.w,
              height: 9.h,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: Colors.white,
                  border:
                      Border.all(color: AppColor.buttonFrameColor, width: 1),
                  borderRadius: BorderRadius.circular(30)),
              child: Text(
                S.of(context).photograph,
                style: TextStyle(
                    color: AppColor.buttonFrameColor, fontSize: 20.sp),
              ),
            ),
          ),
          SizedBox(
            height: 2.h,
          ),
        ],
      ),
    );
  }*/

  //人臉 打開相機
  /*void _takeFacePhoto() {
    ImagePickers.openCamera(
            cameraMimeType: CameraMimeType.photo,
            cropConfig: CropConfig(enableCrop: false, width: 2, height: 3))
        .then((media) {
      if (_account.text.isNotEmpty) {
        _apiProvider!.memberNameExist(_account.text).then((value) {
          if (value) {
            if (media != null) {
              _localdetectPic = media.path.toString();
              setState(() {});
              //上傳圖片並顯示成功或失敗
              EasyLoading.show(status: S.of(context).uploading);
              _photoType = PhotoType.face;
              _apiProvider!
                  .upDataImage(_account.text, media.path!, _photoType)
                  .then((value) {
                debugPrint('拍照成功或失敗: ${value['status']}');
                if (value['status']) {
                  EasyLoading.showToast(S.of(context).finish);
                  _detectname = value['filename'];
                } else {
                  EasyLoading.showToast(S.of(context).fail);
                }
              });
            }
          } else {
            EasyLoading.showToast(S.of(context).duplicate_registered);
          }
        });
      } else {
        EasyLoading.showToast(S.of(context).enter_a_phone_number);
      }
    });
  }*/

  //暱稱
  Widget _nickNameTextField() {
    return SizedBox(
      width: 80.w,
      height: 8.h,
      child: TextFormField(
        style: const TextStyle(color: Colors.black),
        decoration: InputDecoration(
          fillColor: Colors.transparent,
          filled: true,
          hintStyle: const TextStyle(color: Colors.grey),
          hintText: S.of(context).enter_a_nickname,
        ),
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.next,
        controller: _nickName,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return S.of(context).enter_a_nickname;
          }
          if (value.length > Util.nicknameLen) {
            return '${S.of(context).wordLimit}${Util.nicknameLen}';
          }
          return null;
        },
      ),
    );
  }

  //手機號碼
  Widget _accountTextField() {
    return SizedBox(
      width: 80.w,
      height: 8.h,
      child: TextFormField(
        style: const TextStyle(color: Colors.black),
        decoration: InputDecoration(
          fillColor: Colors.transparent,
          filled: true,
          hintStyle: const TextStyle(color: Colors.grey),
          hintText: S.of(context).enter_a_phone_number,
        ),
        keyboardType: TextInputType.number,
        textInputAction: TextInputAction.next,
        controller: _account,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return S.of(context).enter_a_phone_number;
          } else if (!Util().phoneVerification(value)) {
            return S.of(context).mobile_number_entered_incorrectly;
          }
          return null;
        },
      ),
    );
  }

  //密碼
  Widget _passwordTextField() {
    return SizedBox(
      width: 80.w,
      height: 8.h,
      child: TextFormField(
        style: const TextStyle(color: Colors.black),
        decoration: InputDecoration(
          suffixIcon: IconButton(
            icon: Icon(
              isShowPassword ? Icons.visibility : Icons.visibility_off,
              color: Colors.grey[600],
              size: 25,
            ),
            onPressed: () => showPassWord(),
          ),
          fillColor: Colors.transparent,
          filled: true,
          hintStyle: const TextStyle(color: Colors.grey),
          hintText: S.of(context).enter_password,
        ),
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.next,
        controller: _password,
        obscureText: !isShowPassword,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return S.of(context).enter_password;
          } else if (value.trim().length < 6 || value.trim().length > 12) {
            return S.of(context).enter_alphanumeric_characters;
          }
          return null;
        },
      ),
    );
  }

  //再次輸入密碼
  Widget _checkPasswordTextField() {
    return SizedBox(
      width: 80.w,
      height: 8.h,
      child: TextFormField(
        style: const TextStyle(color: Colors.black),
        decoration: InputDecoration(
          suffixIcon: IconButton(
            icon: Icon(
              isShowCheckPassword ? Icons.visibility : Icons.visibility_off,
              color: Colors.grey[600],
              size: 25,
            ),
            onPressed: () => showCheckPassWord(),
          ),
          fillColor: Colors.transparent,
          filled: true,
          hintStyle: const TextStyle(color: Colors.grey),
          hintText: S.of(context).enter_password_again,
        ),
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.done,
        controller: _checkPassword,
        obscureText: !isShowCheckPassword,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return S.of(context).enter_password_again;
          } else if (value.trim().length < 6 || value.trim().length > 12) {
            return S.of(context).enter_alphanumeric_characters;
          } else if (value != _password.text) {
            return S.of(context).different_passwords_entered_twice;
          }
          return null;
        },
      ),
    );
  }

  //性別
  Widget _genderTextField() {
    return SizedBox(
      width: 80.w,
      height: 8.h,
      child: ButtonTheme(
        alignedDropdown: true,
        child: DropdownButton(
          hint: Text(S.of(context).select_gender),
          underline: Container(
            height: 1.0,
            decoration: const BoxDecoration(
              border: Border(
                bottom: BorderSide(
                  color: Color(0xFF8E8E8E),
                ),
              ),
            ),
          ),
          dropdownColor: Colors.white,
          icon: const Icon(Icons.arrow_drop_down_sharp),
          items: items,
          value: values,
          onChanged: (Gender? _value) {
            setState(() {
              values = _value!;
              if (values == Gender.male) {
                _gender.text = S.of(context).male;
              } else {
                _gender.text = S.of(context).female;
              }
            });
          },
          isExpanded: true,
          iconSize: 40,
        ),
      ),
    );
  }

  //生日
  Widget _birthdayTextField() {
    return SizedBox(
      width: 80.w,
      height: 10.h,
      child: Stack(
        alignment: Alignment.center,
        children: [
          TextField(
            readOnly: true,
            onTap: () => _selectBirthDay(),
            style: const TextStyle(color: Colors.black),
            decoration: InputDecoration(
              fillColor: Colors.transparent,
              filled: true,
              hintStyle: const TextStyle(color: Colors.grey),
              hintText: S.of(context).select_a_birthday,
            ),
            keyboardType: TextInputType.number,
            textInputAction: TextInputAction.next,
            controller: _birthday,
          ),
          if (_birthday.text.isEmpty)
            Positioned(
                right: 0,
                child: Text(
                  '(YYYY/MM/DD)',
                  style: TextStyle(fontSize: 16.sp, color: Colors.grey),
                ))
        ],
      ),
    );
  }

  //選擇日期
  void _selectBirthDay() {
    DateTime _now = DateTime.now();
    DatePicker.showDatePicker(context,
        showTitleActions: true,
        minTime: DateTime(1922, 1, 1),
        maxTime: DateTime(_now.year - 18, _now.month, _now.day), onChanged: (date) {
      debugPrint('change $date');
      _birthDayMap = Util().getBirthDay(date);
      _birthday.text = _birthDayMap['showText']!;
      setState(() {});
    }, onConfirm: (date) {
      debugPrint('confirm $date');
      _birthDayMap = Util().getBirthDay(date);
      _birthday.text = _birthDayMap['showText']!;
      setState(() {});
    }, onCancel: () {
      _birthDayMap = {};
      _birthday.text = '';
      setState(() {});
    }, locale: LocaleType.tw);
  }

  //註冊按鈕
  Widget _registerButton() {
    return GestureDetector(
      onTap: () => _registerOnTap(),
      child: Container(
        width: AppSize.buttonW,
        height: AppSize.buttonH,
        alignment: Alignment.center,
        padding: const EdgeInsets.all(5),
        margin: const EdgeInsets.all(10),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(AppSize.buttonCircular),
            gradient: AppColor.appMainColor),
        child: FittedBox(
          child: Text(S.of(context).next_step,
            style: TextStyle(
                color: AppColor.buttonTextColor,
                fontSize: 18.sp,
                fontWeight: FontWeight.bold),
          ),
        ),
      ),
    );
  }

  //點擊註冊
  void _registerOnTap() {
    if (!_formKey.currentState!.validate()) {
      //資料未填或有誤
      if (!EasyLoading.isShow) {
        EasyLoading.showToast(S.of(context).information_is_incomplete);
      }
    } else if (_birthday.text.isEmpty) {
      //生日未填
      if (!EasyLoading.isShow) {
        EasyLoading.showToast(S.of(context).select_a_birthday);
      }
    } else {
      //註冊
      MemberRegisterDTO _model = MemberRegisterDTO(
          name: _account.text,
          password: _password.text,
          nickname: _nickName.text,
          cellphone: _account.text,
          gender: values.index,
          birthday: _birthday.text,
          mugshot: '',
          age: Util().getRegionAge(_birthday.text),
          zodiacSign: Util().dateToHoroscope(_birthday.text).name
      );

      //下一步
      delegate.push(name: RouteName.phoneVerificationPage, arguments: _model);
    }
  }
}
