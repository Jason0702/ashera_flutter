import 'package:ashera_flutter/provider/util_api_provider.dart';
import 'package:ashera_flutter/utils/app_color.dart';
import 'package:ashera_flutter/utils/no_shadow_scroll_behavior.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../generated/l10n.dart';
import '../widget/titlebar_widget.dart';

class AboutAsheraPage extends StatefulWidget {
  const AboutAsheraPage({Key? key}) : super(key: key);


  @override
  State createState() => _AboutAsheraPageState();
}

class _AboutAsheraPageState extends State<AboutAsheraPage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: AppColor.appBackgroundColor,
      body: Column(
        children: [
          //返回
          titleBar(S.of(context).about_ashera),
          SizedBox(height: 1.h,),
          Expanded(flex: 10, child: _body())
        ],
      ),
    );
  }

  //內容
  Widget _body(){
    return Container(
      width: Device.boxConstraints.maxWidth,
      decoration: const BoxDecoration(
          color: Colors.white
      ),
      child: ScrollConfiguration(
        behavior: NoShadowScrollBehavior(),
        child: SingleChildScrollView(
          child: Consumer<UtilApiProvider>(builder: (context, about, _){
            return Padding(
                padding: const EdgeInsets.only(left: 25, right: 25),
                child: Html(
                  data: about.aboutUsModel.about!,
                  style: const {},
            ),);
          },),
        ),
      ),
    );
  }
}