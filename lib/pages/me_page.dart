import 'dart:io';
import 'dart:math';

import 'package:ashera_flutter/dialog/visit_dialog.dart';
import 'package:ashera_flutter/provider/member_provider.dart';
import 'package:ashera_flutter/provider/network_status_provider.dart';
import 'package:ashera_flutter/provider/packages_info_provider.dart';
import 'package:ashera_flutter/provider/util_api_provider.dart';
import 'package:ashera_flutter/utils/app_color.dart';
import 'package:ashera_flutter/utils/shared_preference.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_font_icons/flutter_font_icons.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../app.dart';
import '../dialog/game_point_insufficient_dialog.dart';
import '../dialog/member_upgrade_vip_dialog.dart';
import '../enum/app_enum.dart';
import '../generated/l10n.dart';
import '../provider/identification_provider.dart';
import '../provider/stomp_client_provider.dart';
import '../routers/route_name.dart';
import '../utils/api.dart';
import '../utils/app_image.dart';
import '../utils/util.dart';
import '../widget/avatar_image.dart';
import '../widget/gender_icon.dart';

class MePage extends StatefulWidget {
  const MePage({Key? key}) : super(key: key);

  @override
  State createState() => _MePageState();
}

class _MePageState extends State<MePage> {
  UtilApiProvider? _utilApiProvider;
  StompClientProvider? _stompClientProvider;
  NetworkStatusProvider? _networkStatusProvider;
  MemberProvider? _memberProvider;
  IdentificationProvider? _identificationProvider;

  final SharedPreferenceUtil _sharedPreferenceUtil = SharedPreferenceUtil();
  final GlobalKey _faceRecognitionKey = GlobalKey();
  final GlobalKey _changePasswordKey = GlobalKey();
  final GlobalKey _blacklistKey = GlobalKey();
  final GlobalKey _aboutAsheraKey = GlobalKey();
  final GlobalKey _signOutKey = GlobalKey();
  final GlobalKey _logoutKey = GlobalKey();
  final GlobalKey _dynamicAppIconKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    _utilApiProvider = Provider.of<UtilApiProvider>(context, listen: false);
    _stompClientProvider =
        Provider.of<StompClientProvider>(context, listen: false);
    _networkStatusProvider =
        Provider.of<NetworkStatusProvider>(context, listen: false);
    _memberProvider = Provider.of<MemberProvider>(context);
    _identificationProvider = Provider.of<IdentificationProvider>(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: AppColor.appBackgroundColor,
      body: SingleChildScrollView(
          child: Column(
        children: [
          //性別icon mars
          //個人資料
          _personalInformation(),
          SizedBox(
            height: 1.h,
          ),
          //點數
          _points(),
          //儀錶板
          _dashboardWidget(),
          //選項
          //人臉辨識
          Consumer<MemberProvider>(
            builder: (context, member, _) {
              return _options(
                  _faceRecognitionKey,
                  S.of(context).face_recognition,
                  true,
                  _faceRecognitionOnTap,
                  member.memberModel.verify);
            },
          ),
          //變更密碼
          _options(_changePasswordKey, S.of(context).change_password, true,
              _changePasswordOnTap),
          //封鎖名單
          _options(
              _blacklistKey, S.of(context).block_list, true, _blacklistOnTap),
          //關於 Ashera
          _options(_aboutAsheraKey, S.of(context).about_ashera, true,
              _aboutAsheraOnTap),
          //變更APPIcon
          if (Api.baseUrl.contains('dev') && Platform.isIOS)
            _options(_dynamicAppIconKey, S.of(context).change_icon, true,
                _goToDynamicAppIconOnTap),
          //登出
          _options(_signOutKey, S.of(context).sign_out, false, _signOutOnTap),
          //註銷
          _options(_logoutKey, S.of(context).logout, false, _logoutOnTap),
          //VIP升級
          Consumer2<MemberProvider, UtilApiProvider>(
            builder: (context, member, util, _) {
              return Offstage(
                offstage: member.memberModel.vip == VIP.isVip.index,
                child: Offstage(
                  offstage: !util.systemSetting!.showVip!,
                  child: Container(
                    margin: EdgeInsets.only(top: 2.h, left: 5.w, right: 5.w),
                    child: GestureDetector(
                      onTap: () => _memberUpgradeOnTap(),
                      child: Image(
                        image: AssetImage(AppImage.imgVipDirections),
                      ),
                    ),
                  ),
                ),
              );
            },
          ),
          Consumer<PackagesInfoProvider>(
            builder: (context, info, _) {
              return Container(
                margin: EdgeInsets.only(top: 0.5.h, right: 8.w),
                alignment: Alignment.centerRight,
                child: Text(
                  'ver ${info.version}(${info.buildNumber})',
                  style: const TextStyle(
                      color: AppColor.buttonFrameDisposeColor, fontSize: 14),
                ),
              );
            },
          )
        ],
      )),
    );
  }

  void _memberUpgradeOnTap() async {
    //判斷金額是否足夠
    if (_memberProvider!.memberPoint.point! >=
        _utilApiProvider!.systemSetting!.vipPointsPerMonth!) {
      bool? _result = await showGeneralDialog(
          context: context,
          pageBuilder: (context, anim1, anim2) {
            return MemberUpgradeVipDialog(
              key: UniqueKey(),
              title: S.of(context).message,
              contentForward: S.of(context).does_it_cost,
              content: _utilApiProvider!.systemSetting!.vipPointsPerMonth!
                  .toString(),
              contentBack: S.of(context).upgrade_vip,
              confirmButtonText: S.of(context).sure,
            );
          },
          barrierColor: Colors.black54,
          barrierDismissible: false,
          transitionDuration: const Duration(milliseconds: 100),
          transitionBuilder: (context, anim1, anim2, child) {
            return Transform.scale(
              scale: anim1.value,
              child: child,
            );
          });
      if (_result != null) {
        if (_result) {
          //同意購買
          _stompClientProvider!.sendMemberUpgrade();
        }
      }
    } else {
      if (_utilApiProvider!.systemSetting!.addPointsStatus ==
          PointsStatus.closure.index) {
        EasyLoading.showToast(S.of(context).not_enough_points);
      } else {
        //點數不足
        _pointInsufficient();
      }
    }
  }

  //個人資料
  Widget _personalInformation() {
    return Container(
        height: 15.h,
        decoration: const BoxDecoration(color: Colors.white),
        child: Container(
          margin: EdgeInsets.only(left: 6.w, right: 6.w),
          child: Stack(
            alignment: Alignment.centerLeft,
            children: [
              Row(
                children: [
                  //大頭照
                  Consumer<MemberProvider>(builder: (context, member, _) {
                    return meAvatarImage(member.memberModel);
                  }),
                  //資料
                  SizedBox(
                    width: 2.w,
                  ),
                  Container(
                      height: 11.h,
                      alignment: Alignment.center,
                      child: Consumer<MemberProvider>(
                          builder: (context, member, _) {
                        return Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            //名稱
                            Row(
                              children: [
                                FittedBox(
                                    child: _getText(
                                        member.memberModel.nickname!, 18.sp)),
                                SizedBox(
                                  width: 1.w,
                                ),
                                //性別Icon
                                getGender(member.memberModel.gender!),
                              ],
                            ),
                            SizedBox(
                              height: 0.3.h,
                            ),
                            //ID
                            FittedBox(
                                child: _getText(
                                    'ID : ${member.memberModel.asheraUid!}',
                                    16.sp)),
                            SizedBox(
                              height: 1.h,
                            ),
                            Row(
                              crossAxisAlignment: CrossAxisAlignment.end,
                              children: [
                                //編輯個人檔案
                                GestureDetector(
                                  onTap: () => _editProfile(),
                                  child: Container(
                                    width: 25.w,
                                    height: 3.h,
                                    alignment: Alignment.center,
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(20),
                                      border:
                                          Border.all(color: AppColor.grayLine),
                                    ),
                                    child: Text(
                                      S.of(context).edit_profile,
                                      style: TextStyle(
                                          color: AppColor.grayText,
                                          fontSize: 16.sp),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 3.w,
                                ),
                                //會員類型
                                _memberType(isVIP: member.memberModel.vip == VIP.isVip.index),
                                //訂閱取消
                                _memberCancelVIP(
                                  isVIP: member.memberModel.vip == VIP.isVip.index,
                                  isRenew: member.memberModel.vipSubscription == VIPSubscription.isRenew.index,
                                )
                              ],
                            )
                          ],
                        );
                      }))
                ],
              ),
              //QRCode
              Positioned(
                  right: 0,
                  top: 3.h,
                  child: GestureDetector(
                    onTap: () => _myQRCodeOnTap(),
                    child: const Icon(
                      FontAwesome.qrcode,
                      size: 30,
                      color: Colors.black,
                    ),
                  ))
            ],
          ),
        ));
  }

  ///會員類型
  /// * [isVIP] 是否VIP
  Widget _memberType({required bool isVIP}) {
    return Row(
      children: [
        //Icon
        Image(
          image: AssetImage(
              isVIP ? AppImage.iconVipMember : AppImage.iconNormalMember),
          width: 5.w,
        ),
        SizedBox(
          width: 1.w,
        ),
        Text(
          isVIP ? S.of(context).vip_member : S.of(context).at_large_membership,
          style: const TextStyle(color: AppColor.yellowIcon),
        )
      ],
    );
  }

  ///訂閱取消
  /// * [isVIP] 是否VIP
  /// * [isRenew] 是否續訂
  Widget _memberCancelVIP({required bool isVIP, required isRenew}){
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 9, vertical: 2),
      child: Offstage(
        offstage: !isVIP,
        child: Offstage(
          offstage: !isRenew,
          child: GestureDetector(
            onTap: () => _memberCancelVIPOnTap(),
            child: FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                S.of(context).cancel,
                style: TextStyle(
                    color: Colors.grey[500],
                    fontSize: 12
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _getText(String _text, double _textSize) {
    return Text(
      _text,
      style: TextStyle(
          color: Colors.black,
          fontSize: _textSize,
          fontWeight: FontWeight.bold),
    );
  }

  //點數
  Widget _points() {
    return Container(
      width: Device.boxConstraints.maxWidth,
      decoration: const BoxDecoration(color: Colors.white),
      child: Container(
        margin: EdgeInsets.only(left: 6.w, top: 2.h, bottom: 2.h, right: 6.w),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            //鑽石與點數
            Row(
              children: [
                //鑽石
                Image(
                  width: 8.w,
                  image: AssetImage(AppImage.iconDiamond),
                ),
                SizedBox(
                  width: 3.w,
                ),
                //點數
                Consumer<MemberProvider>(builder: (context, point, _) {
                  return Text(
                    '${point.memberPoint.point}',
                    style: TextStyle(
                        color: AppColor.buttonFrameColor,
                        fontWeight: FontWeight.bold,
                        fontSize: 20.sp),
                  );
                })
              ],
            ),
            //加值按鈕
            Consumer<UtilApiProvider>(
              builder: (context, status, _) {
                return status.systemSetting != null
                    ? Offstage(
                        offstage: status.systemSetting!.addPointsStatus ==
                            PointsStatus.closure.index,
                        child: GestureDetector(
                          onTap: () => _pointsBonusOnTap(),
                          child: Container(
                            width: 18.w,
                            height: 5.h,
                            alignment: Alignment.center,
                            decoration: BoxDecoration(
                              gradient: AppColor.appMainColor,
                              borderRadius: BorderRadius.circular(50),
                            ),
                            child: Text(
                              S.of(context).bonus,
                              style: TextStyle(
                                  color: AppColor.buttonTextColor,
                                  fontSize: 16.sp),
                            ),
                          ),
                        ),
                      )
                    : Container();
              },
            )
          ],
        ),
      ),
    );
  }

  void _editProfile() {
    delegate.push(name: RouteName.editProfilePage);
  }

  void _myQRCodeOnTap() {
    delegate.push(name: RouteName.myQrCodePage);
  }

  //加值
  void _pointsBonusOnTap() {
    delegate.push(name: RouteName.myPointsPage);
  }

  //人臉辨識
  void _faceRecognitionOnTap() {
    delegate.push(name: RouteName.faceRecognitionPage);
  }

  //變更密碼
  void _changePasswordOnTap() {
    delegate.push(name: RouteName.changePasswordPage);
  }

  //封鎖名單
  void _blacklistOnTap() {
    delegate.push(name: RouteName.blackListPage);
  }

  //關於 Ashera
  void _aboutAsheraOnTap() {
    _utilApiProvider!.getAboutUs();
    delegate.push(name: RouteName.aboutAsheraPage);
  }

  //登出
  void _signOutOnTap() async {
    bool? result = await showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return VisitDialog(
              key: UniqueKey(),
              title: S.of(context).sign_out,
              content:
                  '${S.of(context).are_you_sure}${S.of(context).sign_out}?',
              confirmButtonText: S.of(context).sure,
              cancelButtonText: S.of(context).cancel);
        },
        barrierColor: Colors.black54,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 100),
        transitionBuilder: (context, anim1, anim2, child) {
          return child;
        });
    if (result != null) {
      if (result) {
        _signOut();
      }
    }
  }

  void _signOut() async {
    _utilApiProvider!
        .putFcmToken(id: _memberProvider!.memberModel.id!, token: '');
    _networkStatusProvider!.cancel(); //停止網路狀態監聽
    _stompClientProvider!.deactivate(); //STOMP斷開連線
    _stompClientProvider!.clearFriend(); //清除好友
    _sharedPreferenceUtil.delAccessToken(); //清除Token
    _sharedPreferenceUtil.delAccountAndPassword(); //清除帳號與密碼
    await appDb.deleteAllFriend();
    await appDb.deleteAllMessage();
    await appDb.deleteBlacklist();
    delegate.replace(name: RouteName.welcomePage); //跳轉回註冊或登入選擇
  }

  //取消訂閱
  void _memberCancelVIPOnTap() async {
    bool? result = await showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2){
          return VisitDialog(
            key: UniqueKey(),
            title: S.of(context).unsubscribe,
            content: S.of(context).are_you_sure_unsubscribe,
            confirmButtonText: S.of(context).sure,
            cancelButtonText: S.of(context).cancel
          );
        },
        barrierColor: Colors.black54,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 100),
        transitionBuilder: (context, anim1, anim2, child) {
          return child;
        }
    );
    if(result != null){
      if(result){
        //取消訂閱API
        _memberProvider!.vipUnSubscription();
      }
    }
  }

  //選項
  Widget _options(Key _key, String _text, bool _isEnter, VoidCallback _click,
      [int? _isVerify]) {
    return GestureDetector(
        key: _key,
        onTap: () => _click(),
        child: Container(
          decoration: const BoxDecoration(color: Colors.white),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              Container(
                color: AppColor.grayLine,
                height: 0.5,
              ),
              Container(
                alignment: Alignment.centerLeft,
                margin: EdgeInsets.only(
                    left: 6.w, top: 2.h, bottom: 2.h, right: 6.w),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    //Title
                    FittedBox(
                      child: Text(
                        _text,
                        style: TextStyle(
                            color: _text == S.of(context).logout
                                ? Colors.red
                                : Colors.black,
                            fontSize: 18.sp,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        //(已驗證/未驗證)
                        if (_isVerify != null)
                          Text(
                            _getVerifyText(_isVerify),
                            style: TextStyle(
                                color: _getVerifyColor(_isVerify),
                                fontSize: 18.sp),
                          ),
                        SizedBox(
                          width: 5.w,
                        ),
                        // >
                        if (_isEnter)
                          Container(
                            margin: const EdgeInsets.only(top: 5),
                            child: const Icon(
                              FontAwesome.chevron_right,
                              color: AppColor.grayLine,
                              size: 15,
                            ),
                          )
                      ],
                    )
                  ],
                ),
              ),
              if (!_isEnter)
                Container(
                  color: AppColor.grayLine,
                  height: 0.5,
                ),
            ],
          ),
        ));
  }

  String _getVerifyText(int _isVerify) {
    switch (_isVerify) {
      case 0: //未驗證
        return S.of(context).unverified;
      case 1: //已驗證
        return S.of(context).verified;
      case 2: //需重新驗證
        return S.of(context).need_to_re_authenticate;
      case 3: //審核中
        return S.of(context).review;
      default:
        return S.of(context).unverified;
    }
  }

  Color _getVerifyColor(int _isVerify) {
    switch (_isVerify) {
      case 0: //未驗證
        return Colors.grey[400]!;
      case 1: //已驗證
        return AppColor.appFaceBackgroundColor;
      case 2: //需重新驗證
        return Colors.grey[400]!;
      case 3: //審核中
        return AppColor.appFaceBackgroundColor;
      default:
        return Colors.grey[400]!;
    }
  }

  ///點數不足統一方法
  void _pointInsufficient() async {
    bool? result = await _showPointInsufficient();
    if (result != null) {
      if (result) {
        //跳轉加值
        delegate.push(name: RouteName.myPointsPage);
      }
    }
  }

  ///點數不足
  Future<bool?> _showPointInsufficient() async {
    return await showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return GamePointInsufficientDialog(
              key: UniqueKey(),
              title: S.of(context).message,
              content: S.of(context).please_add_value,
              confirmButtonText: S.of(context).go_to_premium,
              cancelButtonText: S.of(context).cancel);
        },
        barrierColor: Colors.black54,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 100),
        transitionBuilder: (context, anim1, anim2, child) {
          return child;
        });
  }

  void _goToDynamicAppIconOnTap() {
    delegate.push(name: RouteName.dynamicAppIconPage);
  }

  void _logoutOnTap() async {
    bool? result = await showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return VisitDialog(
              key: UniqueKey(),
              title: S.of(context).logout,
              content: '${S.of(context).are_you_sure}${S.of(context).logout}?',
              confirmButtonText: S.of(context).sure,
              cancelButtonText: S.of(context).cancel);
        },
        barrierColor: Colors.black54,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 100),
        transitionBuilder: (context, anim1, anim2, child) {
          return child;
        });
    if (result != null) {
      if (result) {
        _delete();
      }
    }
  }

  void _delete() async {
    _utilApiProvider!
        .putFcmToken(id: _memberProvider!.memberModel.id!, token: '');
    bool? _result = await _utilApiProvider!
        .deleteMember(id: _memberProvider!.memberModel.id!);
    if (_result) {
      _networkStatusProvider!.cancel(); //停止網路狀態監聽
      _stompClientProvider!.deactivate(); //STOMP斷開連線
      _stompClientProvider!.clearFriend(); //清除好友
      _sharedPreferenceUtil.delAccessToken(); //清除Token
      _sharedPreferenceUtil.delAccountAndPassword(); //清除帳號與密碼
      await appDb.deleteAllFriend();
      await appDb.deleteAllMessage();
      await appDb.deleteBlacklist();
      delegate.replace(name: RouteName.welcomePage); //跳轉回註冊或登入選擇
    } else {
      EasyLoading.showToast('${S.of(context).logout}${S.of(context).fail}');
    }
  }

  ///儀表板
  Widget _dashboardWidget() {
    return Container(
      padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
      decoration: const BoxDecoration(
        color: Colors.white
      ),
      height: 180,
      child: Column(
        children: [
          Expanded(child: _dashboardYellowWidget()),
          const SizedBox(
            height: 10,
          ),
          Expanded(child: _dashboardRedWidget()),
        ],
      ),
    );
  }

  ///儀錶板Item 紅 總次數
  Widget _dashboardRedWidget() {
    return GestureDetector(
      onTap: () => _identificationOnTap(),
      child: Container(
        decoration: BoxDecoration(
          color: AppColor.meterRed,
          borderRadius: BorderRadius.circular(10),
          boxShadow: const [
            BoxShadow(
              color: AppColor.buttonFrameColor,
              blurRadius: 1,
              spreadRadius: 0.0,
              offset: Offset(4.0, 5.0), // shadow direction: bottom right
            )
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Expanded(
                flex: 1,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    //總次數
                    FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(
                        S.of(context).total,
                        style: const TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                            height: 1.1),
                      ),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Consumer<IdentificationProvider>(
                      builder: (context, identification, _) {
                        return FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Text(
                            '${identification.facesDetectHistoryRecord.length}',
                            style: const TextStyle(
                                color: AppColor.meterText,
                                fontSize: 30,
                                fontWeight: FontWeight.bold,
                                height: 1.1),
                          ),
                        );
                      },
                    )
                  ],
                )),
            Expanded(
                flex: 1,
                child: Consumer2<IdentificationProvider, MemberProvider>(
                  builder: (context, identification, member, _) {
                    if (identification.facesDetectHistoryRecord.isEmpty) {
                      //如果是空的
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          noAvatarImage(),
                          const SizedBox(
                            width: 10,
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              const FittedBox(
                                fit: BoxFit.scaleDown,
                                child: Text(
                                  '無',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20,
                                      height: 1.1),
                                ),
                              ),
                              const SizedBox(
                                width: 20,
                              ),
                              Row(
                                children: [
                                  getGender(0),
                                  FittedBox(
                                    fit: BoxFit.scaleDown,
                                    child: Text('0',
                                        style: TextStyle(
                                          color: Colors.grey[400],
                                          fontSize: 18,
                                          height: 1.1,
                                        )),
                                  )
                                ],
                              )
                            ],
                          )
                        ],
                      );
                    } else {
                      if(identification.facesDetectHistoryRecord.first.targetMemberId == member.memberModel.id){
                        //不能是自己
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            GestureDetector(
                              onTap: () => _identificationOnTap(),
                              child: SizedBox(
                                width: 16.w,
                                height: 8.h,
                                child: meAvatarImage(
                                  identification.facesDetectHistoryRecord.first
                                      .fromMember!,
                                ),
                              ),
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                FittedBox(
                                  fit: BoxFit.scaleDown,
                                  child: Text(
                                    '${identification.facesDetectHistoryRecord.first.fromMember!.nickname}',
                                    style: const TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20,
                                        height: 1.1),
                                  ),
                                ),
                                Row(
                                  children: [
                                    getGender(identification
                                        .facesDetectHistoryRecord
                                        .first
                                        .fromMember!
                                        .gender!),
                                    FittedBox(
                                      fit: BoxFit.scaleDown,
                                      child: Text(
                                          Util().getAge(identification
                                              .facesDetectHistoryRecord
                                              .first
                                              .fromMember!
                                              .birthday!),
                                          style: TextStyle(
                                            color: Colors.grey[400],
                                            fontSize: 18,
                                            height: 1.1,
                                          )),
                                    )
                                  ],
                                )
                              ],
                            )
                          ],
                        );
                      }else{
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            GestureDetector(
                              onTap: () => _identificationOnTap(),
                              child: SizedBox(
                                width: 16.w,
                                height: 8.h,
                                child: meAvatarImage(
                                  identification.facesDetectHistoryRecord.first
                                      .targetMember!,
                                ),
                              ),
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                FittedBox(
                                  fit: BoxFit.scaleDown,
                                  child: Text(
                                    '${identification.facesDetectHistoryRecord.first.targetMember!.nickname}',
                                    style: const TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20,
                                        height: 1.1),
                                  ),
                                ),
                                Row(
                                  children: [
                                    getGender(identification
                                        .facesDetectHistoryRecord
                                        .first
                                        .targetMember!
                                        .gender!),
                                    FittedBox(
                                      fit: BoxFit.scaleDown,
                                      child: Text(
                                          Util().getAge(identification
                                              .facesDetectHistoryRecord
                                              .first
                                              .targetMember!
                                              .birthday!),
                                          style: TextStyle(
                                            color: Colors.grey[400],
                                            fontSize: 18,
                                            height: 1.1,
                                          )),
                                    )
                                  ],
                                )
                              ],
                            )
                          ],
                        );
                      }
                    }
                  },
                ))
          ],
        ),
      ),
    );
  }

  ///儀錶板Item 黃 本周
  Widget _dashboardYellowWidget() {
    return GestureDetector(
      onTap: () => _identificationOnTap(),
      child: Container(
        decoration: BoxDecoration(
          color: AppColor.meterYellow,
          borderRadius: BorderRadius.circular(10),
          boxShadow: const [
            BoxShadow(
              color: AppColor.appFaceBackgroundColor,
              blurRadius: 1,
              spreadRadius: 0.0,
              offset: Offset(4.0, 5.0), // shadow direction: bottom right
            )
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Expanded(
                flex: 1,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    //本周
                    FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(
                        S.of(context).this_week,
                        style: const TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                            height: 1.1),
                      ),
                    ),
                    const SizedBox(
                      width: 10,
                    ),
                    Consumer<IdentificationProvider>(
                      builder: (context, identification, _) {
                        return FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Text(
                            '${identification.facesDetectHistoryWeekRecord.length}',
                            style: const TextStyle(
                                color: AppColor.meterText,
                                fontSize: 30,
                                fontWeight: FontWeight.bold,
                                height: 1.1),
                          ),
                        );
                      },
                    )
                  ],
                )),
            Expanded(
                flex: 1,
                child: Consumer2<IdentificationProvider, MemberProvider>(
                  builder: (context, identification, member, _) {
                    if (identification.facesDetectHistoryWeekRecord.isEmpty) {
                      //如果是空的
                      return Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          noAvatarImage(),
                          const SizedBox(
                            width: 10,
                          ),
                          Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              const FittedBox(
                                fit: BoxFit.scaleDown,
                                child: Text(
                                  '無',
                                  style: TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20,
                                      height: 1.1),
                                ),
                              ),
                              Row(
                                children: [
                                  getGender(0),
                                  FittedBox(
                                    fit: BoxFit.scaleDown,
                                    child: Text('0',
                                        style: TextStyle(
                                          color: Colors.grey[400],
                                          fontSize: 18,
                                          height: 1.1,
                                        )),
                                  )
                                ],
                              )
                            ],
                          )
                        ],
                      );
                    }else{
                      if(identification.facesDetectHistoryWeekRecord.first.targetMemberId == member.memberModel.id!){
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SizedBox(
                              width: 16.w,
                              height: 8.h,
                              child: meAvatarImage(
                                identification.facesDetectHistoryWeekRecord
                                    .first.fromMember!,
                              ),
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                FittedBox(
                                  fit: BoxFit.scaleDown,
                                  child: Text(
                                    '${identification.facesDetectHistoryWeekRecord.first.fromMember!.nickname}',
                                    style: const TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20,
                                        height: 1.1),
                                  ),
                                ),
                                Row(
                                  children: [
                                    getGender(identification.facesDetectHistoryWeekRecord
                                        .first
                                        .fromMember!
                                        .gender!),
                                    FittedBox(
                                      fit: BoxFit.scaleDown,
                                      child: Text(
                                          Util().getAge(identification.facesDetectHistoryWeekRecord
                                              .first
                                              .fromMember!
                                              .birthday!),
                                          style: TextStyle(
                                            color: Colors.grey[400],
                                            fontSize: 18,
                                            height: 1.1,
                                          )),
                                    )
                                  ],
                                )
                              ],
                            )
                          ],
                        );
                      }else{
                        return Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            SizedBox(
                              width: 16.w,
                              height: 8.h,
                              child: meAvatarImage(
                                identification.facesDetectHistoryWeekRecord
                                    .first.targetMember!,
                              ),
                            ),
                            const SizedBox(
                              width: 10,
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                FittedBox(
                                  fit: BoxFit.scaleDown,
                                  child: Text(
                                    '${identification.facesDetectHistoryWeekRecord.first.targetMember!.nickname}',
                                    style: const TextStyle(
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 20,
                                        height: 1.1),
                                  ),
                                ),
                                Row(
                                  children: [
                                    getGender(identification.facesDetectHistoryWeekRecord
                                        .first
                                        .targetMember!
                                        .gender!),
                                    FittedBox(
                                      fit: BoxFit.scaleDown,
                                      child: Text(
                                          Util().getAge(identification.facesDetectHistoryWeekRecord
                                              .first
                                              .targetMember!
                                              .birthday!),
                                          style: TextStyle(
                                            color: Colors.grey[400],
                                            fontSize: 18,
                                            height: 1.1,
                                          )),
                                    )
                                  ],
                                )
                              ],
                            )
                          ],
                        );
                      }
                    }
                  },
                ))
          ],
        ),
      ),
    );
  }

  //辨識紀錄
  void _identificationOnTap() async {
    if (EasyLoading.isShow) {
      return;
    }
    EasyLoading.show(status: 'Loading...');
    /*await Future.wait([
      //取得我辯識過的
      _identificationProvider!.getFacesDetectHistoryByFromMemberId(
          id: _memberProvider!.memberModel.id!),
      //取得辯識過我的
      _identificationProvider!.getFacesDetectHistoryByTargetMemberId(
          id: _memberProvider!.memberModel.id!)
    ]);*/
    _identificationProvider!.getFacesDetectHistoryByTargetMemberIdAndFromMemberId(_memberProvider!.memberModel.id!);
    EasyLoading.dismiss();
    //預設辯識過我的
    _identificationProvider!
        .setFacesMember(faces: FacesMember.facesTargetMember);
    delegate.push(name: RouteName.identificationPage);
  }
}
