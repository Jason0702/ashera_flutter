import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../app.dart';
import '../../dialog/identify_friends_byId_dialog.dart';
import '../../enum/app_enum.dart';
import '../../generated/l10n.dart';
import '../../provider/peekaboo_game_provider.dart';
import '../../provider/util_api_provider.dart';
import '../../utils/app_color.dart';
import '../../utils/app_image.dart';
import '../../utils/no_shadow_scroll_behavior.dart';
import '../../widget/peekaboo_record_list_card.dart';
import '../../widget/titlebar_widget.dart';

class PeekabooHuntList extends StatefulWidget{
  const PeekabooHuntList({Key? key}): super(key: key);

  @override
  State createState() => _PeekabooHuntListState();
}
class _PeekabooHuntListState extends State<PeekabooHuntList>{
  PeekabooGameProvider? _peekabooGameProvider;

  @override
  Widget build(BuildContext context) {
    _peekabooGameProvider = Provider.of<PeekabooGameProvider>(context);
    return Scaffold(
      body: Column(
        children: [
          //標題
          titleBarIntegrate([back()], S.of(context).hunt_list, []),
          //捕捉被捕捉按鈕
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              gotchaRecordButton(),
              beGotchaRecordButton(),
            ],
          ),
          //列表
          Expanded(child: _listWidget())
        ],
      ),
    );
  }

  //返回
  Widget back() {
    return GestureDetector(
      onTap: () => {delegate.popRoute()},
      child: const Icon(
        Icons.arrow_back,
        color: AppColor.appTitleBarTextColor,
        size: 30,
      ),
    );
  }


  //捕捉紀錄
  Widget gotchaRecordButton() {
    return GestureDetector(
      onTap: () => buttonOnTap(status: PeekabooUserStatus.seek),
      child: Consumer<PeekabooGameProvider>(
        builder: (context, status, _) {
          return Container(
            alignment: Alignment.center,
            width: 45.w,
            margin: EdgeInsets.symmetric(vertical: 2.h),
            padding: EdgeInsets.symmetric(vertical: 1.h),
            decoration: BoxDecoration(
                color: status.huntRecordStatus == PeekabooUserStatus.sought
                    ? Colors.white
                    : AppColor.yellowButton,
                borderRadius: BorderRadius.circular(30),
                border: Border.all(color: AppColor.yellowButton, width: 1)),
            child: Text(
              S.of(context).dog,
              style: TextStyle(
                  color: status.huntRecordStatus == PeekabooUserStatus.sought
                      ? AppColor.yellowButton
                      : AppColor.buttonTextColor,
                  fontWeight: FontWeight.bold,
                  fontSize: 20.sp),
            ),
          );
        },
      ),
    );
  }

  //被捕捉紀錄
  Widget beGotchaRecordButton() {
    return GestureDetector(
      onTap: () => buttonOnTap(status: PeekabooUserStatus.sought),
      child: Consumer<PeekabooGameProvider>(
        builder: (context, status, _) {
          return Container(
            alignment: Alignment.center,
            width: 45.w,
            margin: EdgeInsets.symmetric(vertical: 2.h),
            padding: EdgeInsets.symmetric(vertical: 1.h),
            decoration: BoxDecoration(
                color: status.huntRecordStatus == PeekabooUserStatus.seek
                    ? Colors.white
                    : AppColor.yellowButton,
                borderRadius: BorderRadius.circular(30),
                border: Border.all(color: AppColor.yellowButton, width: 1)),
            child: Text(
              S.of(context).cat,
              style: TextStyle(
                  color: status.huntRecordStatus == PeekabooUserStatus.seek
                      ? AppColor.yellowButton
                      : AppColor.buttonTextColor,
                  fontWeight: FontWeight.bold,
                  fontSize: 20.sp),
            ),
          );
        },
      ),
    );
  }

  void buttonOnTap({required PeekabooUserStatus status}) {
    if (_peekabooGameProvider!.huntRecordStatus == status) {
      return;
    }
    _peekabooGameProvider!.setHuntRecordStatus(status: status);
  }


  //列表
  Widget _listWidget() {
    return ScrollConfiguration(
      behavior: NoShadowScrollBehavior(),
      child: Consumer2<PeekabooGameProvider, UtilApiProvider>(
          builder: (context, status, record, _) {
            if (status.huntRecordStatus == PeekabooUserStatus.seek) {
              //捕捉紀錄
              //log('捕捉紀錄 ${record.toDayPeekabooRecordFromMemberCompleteList.length}');
              return record.toDayPeekabooRecordFromMemberCompleteList.isEmpty ? SizedBox(
                width: Device.width,
                height: Device.height,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image(
                      width: 20.w,
                      height: 20.h,
                      image: AssetImage(AppImage.imgWaiting),
                    ),
                    Text(
                      S.of(context).there_are_currently_no_records,
                      style: TextStyle(
                          color: Colors.grey[300]!,
                          fontWeight: FontWeight.bold,
                          fontSize: 20.sp),
                    )
                  ],
                ),
              ) : ListView.builder(
                  shrinkWrap: true,
                  addAutomaticKeepAlives: true,
                  itemCount: record.toDayPeekabooRecordFromMemberCompleteList.length,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      onTap: () => _recordOnTap(record.toDayPeekabooRecordFromMemberCompleteList[index].targetMemberId!),
                      child: PeekabooRecordFromListCard(
                        model: record.toDayPeekabooRecordFromMemberCompleteList[index],
                      ),
                    );
                  });
            } else {
              //被捕捉紀錄
              //log('被捕捉紀錄 ${record.toDayPeekabooRecordTargetMemberCompleteList.length}');
              return record.toDayPeekabooRecordTargetMemberCompleteList.isEmpty ? SizedBox(
                width: Device.width,
                height: Device.height,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image(
                      width: 20.w,
                      height: 20.h,
                      image: AssetImage(AppImage.imgWaiting),
                    ),
                    Text(
                      S.of(context).there_are_currently_no_records,
                      style: TextStyle(
                          color: Colors.grey[300]!,
                          fontWeight: FontWeight.bold,
                          fontSize: 20.sp),
                    )
                  ],
                ),
              ) : ListView.builder(
                  shrinkWrap: true,
                  addAutomaticKeepAlives: true,
                  itemCount: record.toDayPeekabooRecordTargetMemberCompleteList.length,
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      onTap: () => _recordOnTap(record.toDayPeekabooRecordTargetMemberCompleteList[index].fromMemberId!),
                      child: PeekabooRecordTargetListCard(
                          model: record.toDayPeekabooRecordTargetMemberCompleteList[index],

                      ),
                    );
                  });
            }
          }),
    );
  }

  //加入好友對話框
  void _recordOnTap(int id){
    showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2){
          return IdentifyFriendsByIdDialog(
            key: UniqueKey(),
            id: id,
          );
        },
        barrierColor: Colors.transparent,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 200),
        transitionBuilder: (context, anim1, anim2, child){
          return Transform.scale(
            scale: anim1.value,
            child: child,
          );
        }
    );
  }
}