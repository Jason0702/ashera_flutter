import 'dart:developer';

import 'package:ashera_flutter/enum/app_enum.dart';
import 'package:ashera_flutter/generated/l10n.dart';
import 'package:ashera_flutter/provider/peekaboo_game_provider.dart';
import 'package:ashera_flutter/provider/util_api_provider.dart';
import 'package:ashera_flutter/utils/app_color.dart';
import 'package:ashera_flutter/widget/titlebar_widget.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../app.dart';
import '../../utils/app_image.dart';
import '../../utils/no_shadow_scroll_behavior.dart';
import '../../utils/shared_preference.dart';
import '../../widget/peekaboo_record_list_card.dart';

class PeekabooRecord extends StatefulWidget {
  const PeekabooRecord({Key? key}) : super(key: key);

  @override
  _PeekabooRecordState createState() => _PeekabooRecordState();
}

class _PeekabooRecordState extends State<PeekabooRecord> {
  SharedPreferenceUtil sharedPreferenceUtil = SharedPreferenceUtil();
  PeekabooGameProvider? _peekabooGameProvider;

  @override
  Widget build(BuildContext context) {
    _peekabooGameProvider = Provider.of<PeekabooGameProvider>(context, listen: false);
    return Scaffold(
      body: Column(
        children: [
          //標題
          titleBarIntegrate([back()], S.of(context).record, []),
          //捕捉被捕捉按鈕
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              gotchaRecordButton(),
              beGotchaRecordButton(),
            ],
          ),
          //列表
          Expanded(child: _listWidget())
        ],
      ),
    );
  }

  //返回
  Widget back() {
    return GestureDetector(
      onTap: () => {delegate.popRoute()},
      child: const Icon(
        Icons.arrow_back,
        color: AppColor.appTitleBarTextColor,
        size: 30,
      ),
    );
  }

  //捕捉紀錄
  Widget gotchaRecordButton() {
    return GestureDetector(
      onTap: () => buttonOnTap(status: PeekabooUserStatus.seek),
      child: Consumer<PeekabooGameProvider>(
        builder: (context, status, _) {
          return Container(
            alignment: Alignment.center,
            width: 45.w,
            margin: EdgeInsets.symmetric(vertical: 2.h),
            padding: EdgeInsets.symmetric(vertical: 1.h),
            decoration: BoxDecoration(
                color: status.recordStatus == PeekabooUserStatus.sought
                    ? Colors.white
                    : AppColor.yellowButton,
                borderRadius: BorderRadius.circular(30),
                border: Border.all(color: AppColor.yellowButton, width: 1)),
            child: Text(
              "${S.of(context).gotcha}${S.of(context).record}",
              style: TextStyle(
                  color: status.recordStatus == PeekabooUserStatus.sought
                      ? AppColor.yellowButton
                      : AppColor.buttonTextColor,
                  fontWeight: FontWeight.bold,
                  fontSize: 20.sp),
            ),
          );
        },
      ),
    );
  }

  //被捕捉紀錄
  Widget beGotchaRecordButton() {
    return GestureDetector(
      onTap: () => buttonOnTap(status: PeekabooUserStatus.sought),
      child: Consumer<PeekabooGameProvider>(
        builder: (context, status, _) {
          return Container(
            alignment: Alignment.center,
            width: 45.w,
            margin: EdgeInsets.symmetric(vertical: 2.h),
            padding: EdgeInsets.symmetric(vertical: 1.h),
            decoration: BoxDecoration(
                color: status.recordStatus == PeekabooUserStatus.seek
                    ? Colors.white
                    : AppColor.yellowButton,
                borderRadius: BorderRadius.circular(30),
                border: Border.all(color: AppColor.yellowButton, width: 1)),
            child: Text(
              "${S.of(context).be_gotcha}${S.of(context).record}",
              style: TextStyle(
                  color: status.recordStatus == PeekabooUserStatus.seek
                      ? AppColor.yellowButton
                      : AppColor.buttonTextColor,
                  fontWeight: FontWeight.bold,
                  fontSize: 20.sp),
            ),
          );
        },
      ),
    );
  }

  void buttonOnTap({required PeekabooUserStatus status}) {
    if (_peekabooGameProvider!.recordStatus == status) {
      return;
    }
    _peekabooGameProvider!.setPeekabooRecordStatus(status: status);
  }

  //列表
  Widget _listWidget() {
    return ScrollConfiguration(
      behavior: NoShadowScrollBehavior(),
      child: Consumer2<PeekabooGameProvider, UtilApiProvider>(
          builder: (context, status, record, _) {
        if (status.recordStatus == PeekabooUserStatus.seek) {
          //捕捉紀錄
          //log('捕捉紀錄 ${record.peekabooRecordFromMemberCompleteList.length}');
          return ListView.builder(
              shrinkWrap: true,
              addAutomaticKeepAlives: true,
              itemCount: record.peekabooRecordFromMemberCompleteList.length,
              itemBuilder: (context, index) {
                return record.peekabooRecordFromMemberCompleteList.isEmpty ? SizedBox(
                  width: Device.width,
                  height: Device.height,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image(
                        width: 20.w,
                        height: 20.h,
                        image: AssetImage(AppImage.imgWaiting),
                      ),
                      Text(
                        S.of(context).there_are_currently_no_records,
                        style: TextStyle(
                            color: Colors.grey[300]!,
                            fontWeight: FontWeight.bold,
                            fontSize: 20.sp),
                      )
                    ],
                  ),
                ) : PeekabooRecordFromListCard(
                  model: record.peekabooRecordFromMemberCompleteList[index],
                );
              });
        } else {
          //被捕捉紀錄
          //log('被捕捉紀錄 ${record.peekabooRecordTargetMemberCompleteList.length}');
          return ListView.builder(
              shrinkWrap: true,
              addAutomaticKeepAlives: true,
              itemCount: record.peekabooRecordTargetMemberCompleteList.length,
              itemBuilder: (context, index) {
                return record.peekabooRecordTargetMemberCompleteList.isEmpty ? SizedBox(
                  width: Device.width,
                  height: Device.height,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Image(
                        width: 20.w,
                        height: 20.h,
                        image: AssetImage(AppImage.imgWaiting),
                      ),
                      Text(
                        S.of(context).there_are_currently_no_records,
                        style: TextStyle(
                            color: Colors.grey[300]!,
                            fontWeight: FontWeight.bold,
                            fontSize: 20.sp),
                      )
                    ],
                  ),
                ) : PeekabooRecordTargetListCard(
                    model: record.peekabooRecordTargetMemberCompleteList[index],
                );
              });
        }
      }),
    );
  }
}
