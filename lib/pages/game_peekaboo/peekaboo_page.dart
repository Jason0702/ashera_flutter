import 'dart:async';
import 'dart:developer';
import 'dart:typed_data';
import 'dart:ui';

import 'package:ashera_flutter/enum/app_position_enum.dart';
import 'package:ashera_flutter/models/update_member_location_dto.dart';
import 'package:ashera_flutter/provider/peekaboo_game_provider.dart';
import 'package:ashera_flutter/provider/position_provider.dart';
import 'package:ashera_flutter/provider/stomp_client_provider.dart';
import 'package:ashera_flutter/provider/util_api_provider.dart';
import 'package:ashera_flutter/utils/util.dart';
import 'package:ashera_flutter/widget/button_widget.dart';
import 'package:ashera_flutter/widget/drawer/peekaboo_drawer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:google_maps_cluster_manager/google_maps_cluster_manager.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:lottie/lottie.dart' as lottie;
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../app.dart';
import '../../dialog/game_point_insufficient_dialog.dart';
import '../../dialog/unblock_mugshot_dialog.dart';
import '../../enum/app_enum.dart';
import '../../generated/l10n.dart';
import '../../models/add_unlock_payment_record_dto.dart';
import '../../models/member_local_model.dart';
import '../../models/member_model.dart';
import '../../models/peekaboo_record_model.dart';
import '../../models/place.dart';
import '../../provider/member_provider.dart';
import '../../provider/unlock_mugshot_provider.dart';
import '../../routers/route_name.dart';
import '../../utils/app_color.dart';
import '../../utils/app_image.dart';
import '../../widget/avatar_image.dart';
import '../../widget/drawer/buttom_drawer.dart';
import '../../widget/image_shower.dart';
import '../../widget/titlebar_widget.dart';

class PeekabooPage extends StatefulWidget {
  const PeekabooPage({Key? key}) : super(key: key);

  @override
  _PeekabooPageState createState() => _PeekabooPageState();
}

class _PeekabooPageState extends State<PeekabooPage>{
  //GoogleMapController
  GoogleMapController? mapController;
  final Completer<GoogleMapController> _controller = Completer();

  //左側抽屜Key
  final BottomDrawerController _globalKey = BottomDrawerController();

  //大頭針
  Set<Marker> _markers = {};

  late ClusterManager _manager;
  List<Place> item = [];

  //隱身
  bool _stealth = false;

  PositionProvider? _positionProvider;
  MemberProvider? _memberProvider;
  UtilApiProvider? _utilApiProvider;
  StompClientProvider? _stompClientProvider;
  PeekabooGameProvider? _peekabooGameProvider;
  UnlockMugshotProvider? _unlockMugshotProvider;

  //倒數
  Timer? _stopTimer;
  Timer? _playTimer;

  String _stopTimeText = '00:00:00';
  //地圖重整
  bool _mapReorganization = false;

  void _timerStart() {
    if (_utilApiProvider!.peekabooRecordTargetMemberList.isEmpty) {
      return;
    }
    if (_peekabooGameProvider!.userStatus == PeekabooUserStatus.sought) {
      //被捕方
      if (_stopTimeLeft() != '00:00:00') {
        _stopTimer ??=
            Timer.periodic(const Duration(milliseconds: 1000), (timer) {
          _stopTimeText = _stopTimeLeft();
          if (mounted) {
            setState(() {});
          }
        });
      }
    }
  }

  void _playTimeStart() {
    _playTimer ??= Timer.periodic(const Duration(milliseconds: 30000), (timer) {
      _getRecordOrPlayer();
    });
  }

  //取得躲貓貓紀錄或捕捉方玩家
  void _getRecordOrPlayer() {
    if (_peekabooGameProvider!.userStatus == PeekabooUserStatus.seek) {
      //捕捉方刷新紀錄
      _utilApiProvider!.getHideAndSeekRecordByStatus().then((value) {
        _mapReorganization = true;
        setState(() {});
        addMark();
      });
    } else {
      //被捕捉方取得線上人數
      _utilApiProvider!.getPeekabooLocal().then((value) {
        Future.delayed(const Duration(milliseconds: 500), () {
          _mapReorganization = true;
          setState(() {});
          addMark();
        });
      }); //現在有多少人在捕捉
    }
  }

  //倒數計時時間計算
  String _stopTimeLeft() {
    DateTime _nowTime = DateTime.now(); //開始時間
    DateTime _expirationStartTime;
    //if(_peekabooGameProvider!.catchStatus == CatchStatus.catchStatusProcessing){
    /*_expirationStartTime = DateTime.fromMillisecondsSinceEpoch(_utilApiProvider!.peekabooRecordTargetMemberList.last.startAt! *
          1000).add(const Duration(hours: 5)); //加5小時*/
    //}else{
    /*_expirationStartTime = DateTime.fromMillisecondsSinceEpoch(_utilApiProvider!.peekabooRecordTargetMemberList.last.startAt! *
          1000).add(const Duration(minutes: 5)); //加5分鐘*/
    //}
    if (_peekabooGameProvider!.catchStatus == CatchStatus.catchStatusCancel || _peekabooGameProvider!.catchStatus == CatchStatus.catchStatusComplete) {
      _expirationStartTime = DateTime.fromMillisecondsSinceEpoch(
              _utilApiProvider!.peekabooRecordTargetMemberList.last.cancelAt! *
                  1000)
          .add(const Duration(minutes: 5));
    } else {
      _expirationStartTime = DateTime.fromMillisecondsSinceEpoch(
              _utilApiProvider!.peekabooRecordTargetMemberList.last.startAt! *
                  1000)
          .add(const Duration(hours: 4)); //加5分鐘
    }
    Duration timeLag = _nowTime.difference(_expirationStartTime);
    Duration secLag = Duration(seconds: timeLag.inSeconds);
    List<String> parts = secLag.toString().split(':');
    List<String> _delDot = parts[2].toString().split('.');
    if (timeLag.inSeconds.isNegative) {
      return '${int.parse(parts[0]).abs().toString().padLeft(2, '0')}:${parts[1].padLeft(2, '0')}:${_delDot[0].padLeft(2, '0')}';
    } else {
      if (_stopTimer != null) {
        _stopTimer!.cancel();
        _stopTimer = null;
      }
    }
    return '00:00:00';
  }

  @override
  void initState() {
    _manager = _initClusterManager();
    super.initState();
    //GPS設定
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  ClusterManager _initClusterManager() {
    return ClusterManager<Place>(
        item,
        _updateMarkers,
        markerBuilder: _markerBuilder,
        levels: [1, 4.25, 6.75, 8.25, 11.5, 14.5, 16.0, 16.5, 20.0],
        extraPercent: 0.2,
        stopClusteringZoom: 16.5
    );
  }

  void _updateMarkers(Set<Marker> markers) {
    log('Updated ${markers.length} ${markers.toString()} markers');
    setState(() {
      _markers = markers;
    });
  }

  Future<Marker> Function(Cluster<Place>) get _markerBuilder =>
          (cluster) async {
        log('cluster: ${cluster.items.first.model.interactivePic!}');
        return Marker(
          markerId: MarkerId(cluster.getId()),
          position: cluster.location,
          onTap: () {
            log('---- $cluster');
            if(cluster.count == 1){
              log('只有一個人: ${cluster.items.first.name}');
              _singleOnTap(cluster.items.first.model);
            }else{
              _peekabooGameProvider!.setItem(cluster.items);
              _globalKey.open();
              //cluster.items.forEach((p) => log('${p.toString()}'));
            }
          },
          icon: await _getMarkerBitmap(cluster.isMultiple ? 125 : 75,
              cluster.items.first.model.id!,
              cluster.items.first.model.interactivePic!,
              text: cluster.isMultiple ? cluster.count.toString() : null),
        );
      };

  Future<BitmapDescriptor> _getMarkerBitmap(int size,int id, String pic, {String? text}) async {
    final PictureRecorder pictureRecorder = PictureRecorder();
    final Canvas canvas = Canvas(pictureRecorder);
    final Paint paint1 = Paint()..color = Colors.orange;
    final Paint paint2 = Paint()..color = Colors.white;

    canvas.drawCircle(Offset(size / 2, size / 2), size / 2.0, paint1);
    canvas.drawCircle(Offset(size / 2, size / 2), size / 2.2, paint2);
    canvas.drawCircle(Offset(size / 2, size / 2), size / 2.8, paint1);

    if (text != null) {
      TextPainter painter = TextPainter(textDirection: TextDirection.ltr);
      painter.text = TextSpan(
        text: text,
        style: TextStyle(
            fontSize: size / 3,
            color: Colors.white,
            fontWeight: FontWeight.normal),
      );
      painter.layout();
      painter.paint(
        canvas,
        Offset(size / 2 - painter.width / 2, size / 2 - painter.height / 2),
      );
      final img = await pictureRecorder.endRecording().toImage(size, size);
      final data = await img.toByteData(format: ImageByteFormat.png) as ByteData;

      return BitmapDescriptor.fromBytes(data.buffer.asUint8List());
    } else {
      return _getMarkerIcon(id, pic);
    }
  }

  void _singleOnTap(MemberModel model){
    if (_memberProvider!.memberModel.vip == VIPStatus.closure.index) {
      showImage(files: model.interactivePic!);
    } else if (_isFriend(id: model.id!)) {
      showImage(files: model.interactivePic!);
    }else {
      unblockMemberMugshot(
          '0',
          _memberProvider!.memberModel.id!,
          model.nickname!,
          model.gender!,
          model.birthday!,
          model.id!);
    }
  }

  _onLayoutDone(_) async {
    _positionProvider!.setAppPosition(position: AppPosition.peekaboo);
    //建立地圖刷新監聽
    _positionProvider!.addListener(_myListener);
    _peekabooGameProvider!.addListener(_timerStart);
    _utilApiProvider!.addListener(_timerStart);
    _stompClientProvider!.addListener(_timerStart);
    //上線
    _stompClientProvider!
        .sendUpdateHideStatus(status: OnlineStatus.onlineStatusOnline);
    _stompClientProvider!.sendUpdateMemberLocation(
        location: UpdateMemberLocationDTO(
            latitude: _positionProvider!.currentPosition.latitude,
            longitude: _positionProvider!.currentPosition.longitude));

    _getRecordOrPlayer(); //取得躲貓貓紀錄或捕捉方玩家

    //每30秒刷新一次
    _playTimeStart();
  }

  //地圖刷新監聽
  void _myListener() {
    //Log.log('地圖刷新監聽');
    mapController?.animateCamera(CameraUpdate.newCameraPosition(
        CameraPosition(target: _positionProvider!.currentPosition, zoom: 0.0)));
    //更新用戶位置
    _stompClientProvider!.sendUpdateMemberLocation(
        location: UpdateMemberLocationDTO(
            latitude: _positionProvider!.currentPosition.latitude,
            longitude: _positionProvider!.currentPosition.longitude));
    /*setState(() {});*/
  }

  @override
  void dispose() {
    //回首頁
    _positionProvider!.setAppPosition(position: AppPosition.home);
    //移除地圖刷新監聽
    _positionProvider!.removeListener(_myListener);
    super.dispose();

    //下線
    _stompClientProvider!
        .sendUpdateHideStatus(status: OnlineStatus.onlineStatusOffline);
    _peekabooGameProvider!.removeListener(_timerStart);
    _utilApiProvider!.removeListener(_timerStart);
    _stompClientProvider!.removeListener(_timerStart);
    if (_stopTimer != null) {
      _stopTimer!.cancel();
      _stopTimer = null;
    }
    if (_playTimer != null) {
      _playTimer!.cancel();
      _playTimer = null;
    }
  }

  @override
  Widget build(BuildContext context) {
    _positionProvider = Provider.of<PositionProvider>(context, listen: false);
    _utilApiProvider = Provider.of<UtilApiProvider>(context, listen: false);
    _memberProvider = Provider.of<MemberProvider>(context, listen: false);
    _peekabooGameProvider =
        Provider.of<PeekabooGameProvider>(context, listen: false);
    _stompClientProvider =
        Provider.of<StompClientProvider>(context, listen: false);
    _unlockMugshotProvider =
        Provider.of<UnlockMugshotProvider>(context, listen: false);
    return Scaffold(
      backgroundColor: AppColor.appBackgroundColor,
      body: Stack(
        children: [
          GestureDetector(
            onTap: () => _globalKey.close(),
            child: Column(
              children: [
                //標題
                titleBarIntegrate([back()], S.of(context).peekaboo, [_titleBarReportButton(), giftBox()]),
                //自身資訊
                _selfInformation(),
                //中間地圖內容
                Expanded(
                  child: _googleMap(),
                ),
                //抓捕資訊
                gotchaInformation(),
              ],
            ),
          ),
          PeekabooDrawer(
              globalKey: _globalKey,
          )
        ],
      ),
    );
  }

  //獎賞
  Widget giftBox() {
    return GestureDetector(
      onTap: () => giftOnTap(),
      child: FittedBox(
        child: SizedBox(
            height: 8.w, width: 8.w, child: Image.asset(AppImage.iconGiftBox)),
      ),
    );
  }

  //返回
  Widget back() {
    return GestureDetector(
      onTap: () {
        _globalKey.close();
        delegate.popRoute();
        },
      child: const Icon(
        Icons.arrow_back,
        color: AppColor.appTitleBarTextColor,
        size: 30,
      ),
    );
  }

  //個人資訊
  Widget _selfInformation() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 10),
      height: 14.h,
      child: Row(
        children: [
          //大頭貼
          Consumer<MemberProvider>(builder: (context, member, _) {
            return SizedBox(
              width: 15.w,
              height: 10.h,
              child: interactiveAvatarImage(
                  member.memberModel, member.interactiveAvatarVer),
            );
          }),
          //資訊
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                //名稱
                Consumer<MemberProvider>(builder: (context, member, _) {
                  return Container(
                    child: FittedBox(
                      child: Text(
                        member.memberModel.nickname!,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 18.sp,
                            color: Colors.black),
                      ),
                    ),
                    margin: const EdgeInsets.only(left: 10),
                  );
                }),
                Row(
                  children: [
                    //點數icon
                    Container(
                      width: 5.w,
                      child: Image.asset(AppImage.iconDiamond),
                      margin: const EdgeInsets.only(left: 10),
                    ),
                    //點數
                    Consumer<MemberProvider>(builder: (context, point, _) {
                      return FittedBox(
                        child: Text(
                          " ${point.memberPoint.point} ",
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 20.sp,
                              color: Colors.black),
                        ),
                      );
                    }),
                    //充值按鈕
                    if (_utilApiProvider!.systemSetting!.addPointsStatus !=
                        PointsStatus.closure.index)
                      GestureDetector(
                        onTap: (){
                              _globalKey.close();
                              delegate.push(name: RouteName.myPointsPage);
                            },
                        child: const Icon(
                          Icons.add_circle,
                          color: AppColor.buttonFrameColor,
                        ),
                      ),
                  ],
                ),
              ],
            ),
          ),
          //隱身
          Offstage(
            offstage: _memberProvider!.memberModel.name != '0900493972',
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                SizedBox(
                  height: 0.1.h,
                ),
                const FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text('隱身', style: TextStyle(color: Colors.black, fontSize: 16, height: 1.1),),
                ),
                _flutterSwitch(),
                SizedBox(
                  height: 0.1.h,
                ),
              ],
            ),
          ),
          //目前是哪方
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              //切換按鈕
              switchButton(),
              SizedBox(
                height: 0.1.h,
              ),
              //切換按鈕底下文字
              returnWhichSide(),
            ],
          ),
        ],
      ),
    );
  }

  //切換按鈕
  Widget switchButton() {
    return Container(
      width: 33.w,
      height: 14.5.w,
      decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(color: AppColor.grayLine, width: 1),
          borderRadius: BorderRadius.circular(30)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          GestureDetector(
            onTap: () {
              _globalKey.close();
              changeSeekStatus(PeekabooUserStatus.seek);},
            child: Container(
              margin: EdgeInsets.all(returnIsSeek() ? 0 : 5),
              width: 14.w,
              decoration: const BoxDecoration(shape: BoxShape.circle),
              child: returnIsSeek()
                  ? Image.asset(AppImage.iconDog)
                  : Image.asset(AppImage.iconDogUnpressed),
            ),
          ),
          GestureDetector(
            onTap: () {
              _globalKey.close();
              changeSeekStatus(PeekabooUserStatus.sought);},
            child: Container(
              margin: EdgeInsets.all(!returnIsSeek() ? 0 : 5),
              width: 14.w,
              decoration: const BoxDecoration(shape: BoxShape.circle),
              child: !returnIsSeek()
                  ? Image.asset(AppImage.iconCat)
                  : Image.asset(AppImage.iconCatUnpressed),
            ),
          )
        ],
      ),
    );
  }

  //切換按鈕事件
  void changeSeekStatus(PeekabooUserStatus input) {
    if (_peekabooGameProvider!.userStatus == input) {
      return;
    }
    if (_peekabooGameProvider!.catchStatus ==
        CatchStatus.catchStatusProcessing) {
      EasyLoading.showToast(S.of(context).please_close_status);
      return;
    }
    _markers.clear();
    setState(() {});
    if (_stopTimeText == '00:00:00') {
      _peekabooGameProvider!.setPeekabooUserStatus(status: input);
      if (_peekabooGameProvider!.userStatus == PeekabooUserStatus.seek) {
        //抓到
        _stompClientProvider!.sendUpdateHideType(status: Catch.isCatch);
      } else {
        _stompClientProvider!.sendUpdateHideType(status: Catch.beCaught);
        _utilApiProvider!.getPeekabooLocal(); //現在有多少人在捕捉
      }
    } else {
      EasyLoading.showToast(S.of(context).It_s_not_time_to_switch); //還不到切換時間
    }
  }

  //切換按鈕底下文字
  RichText returnWhichSide() {
    switch (_peekabooGameProvider!.userStatus) {
      case PeekabooUserStatus.seek:
        return RichText(
            text: TextSpan(children: [
          TextSpan(
              text: S.of(context).nowPeekabooStatus('seek'),
              style: TextStyle(color: Colors.black, fontSize: 17.sp)),
        ]));
      case PeekabooUserStatus.sought:
        return RichText(
            text: TextSpan(children: [
          TextSpan(
              text: S.of(context).nowPeekabooStatus('Sought'),
              style: TextStyle(color: Colors.black, fontSize: 17.sp)),
        ]));
    }
  }

  //返回是否是找尋者
  bool returnIsSeek() {
    if (_peekabooGameProvider!.userStatus == PeekabooUserStatus.seek) {
      return true;
    } else {
      return false;
    }
  }

  //地圖
  Widget _googleMap() {
    return Stack(
      children: [
        GoogleMap(
          markers: _markers,
          mapType: MapType.normal,
          zoomControlsEnabled: false,
          myLocationButtonEnabled: true,
          myLocationEnabled: !_stealth,
          mapToolbarEnabled: false,
          onMapCreated: (GoogleMapController controller) {
            _controller.complete(controller);
            _manager.setMapId(controller.mapId);
          },
          initialCameraPosition: CameraPosition(
              target: _positionProvider!.initialPosition, zoom: 16.0),
          onTap: (LatLng latLng) => _globalKey.close(),
          /*onCameraMove: (CameraPosition position){
        log('camera: ${position.zoom}');
      },*/
        ),
        Offstage(
          offstage: !_mapReorganization,
          child: Container(
            width: MediaQuery.of(context).size.width,
            decoration: const BoxDecoration(
                color: Colors.black87
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Flexible(child: SizedBox(
                  height: 200,
                  child: lottie.Lottie.asset(
                      AppImage.loadingJson,
                      repeat: true,
                  ),
                )),
                FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text(
                    S.of(context).updating_information,
                    style: const TextStyle(
                        color: Colors.white,
                        fontSize: 18
                    ),
                  ),
                )
              ],
            ),
          ),
        )
      ],
    );
  }

  //加入大頭針
  Future<void> addMark() async {
    _markers.clear();
    if (_peekabooGameProvider!.userStatus == PeekabooUserStatus.sought) {
      //被捕方顯示線上的捕捉方
      /*for (var element in _utilApiProvider!.memberLocalList) {
        String markerId = '${element.id}';
        _markers.add(Marker(
          markerId: MarkerId(markerId),
          position: LatLng(element.latitude!, element.longitude!),
          icon: await _getMarkerIcon(element.id!, element.interactivePic!),
          onTap: () {
            if (_memberProvider!.memberModel.vip == VIPStatus.closure.index) {
              showImage(files: element.interactivePic!);
            } else if (_isFriend(id: element.id!)) {
              showImage(files: element.interactivePic!);
            }else {
              unblockMemberMugshot(
                  markerId,
                  _memberProvider!.memberModel.id!,
                  element.nickname!,
                  element.gender!,
                  element.birthday!,
                  element.id!);
            }
          },
        ));
      }
      if (mounted) {
        setState(() {});
      }*/
      //底下是新的
      await Future.forEach(_utilApiProvider!.memberLocalList, (MemberLocalModel element) {
        MemberModel _model = MemberModel(
            id: element.id,
            name: '',
            nickname: element.nickname,
            gender: element.gender,
            height: 0.0,
            weight: 0.0,
            birthday: element.birthday,
            cellphone: '',
            mugshot: '',
            zodiacSign: '',
            bloodType: '',
            aboutMe: '',
            job: '',
            asheraUid: '',
            updatedAt: '',
            latitude: element.latitude,
            longitude: element.longitude,
            faceId: '',
            verify: 0,
            facePic: '',
            s3Etag: '',
            vip: 0,
            activeStart: 0,
            activeEnd: 0,
            hideBirthday: 0,
            status: 0,
            initGame: 0,
            initGameAt: 0,
            interactivePic: element.interactivePic,
            online: element.online
        );
        if(item.where((item) => item.model == _model).isEmpty){
          item.add(Place(
              name: _model.nickname!,
              model: _model,
              latLng: LatLng(_model.latitude!, _model.longitude!)));
        }
      });
      _manager.setItems(item);
      Future.delayed(const Duration(milliseconds: 4000), (){
        _mapReorganization = false;
        setState(() {});
      });

    } else {
      //捕捉方 顯示進行中的
      //底下是新的
      await Future.forEach(_utilApiProvider!.peekabooRecordProcessingList, (PeekabooRecordModel element) {
        if(item.where((item) => item.model == element.targetMember).isEmpty){
          item.add(Place(
              name: element.targetMember!.nickname!,
              model: element.targetMember!,
              latLng: LatLng(element.latitude!, element.longitude!)));
        }
      });
      _manager.setItems(item);
      Future.delayed(const Duration(milliseconds: 4000), (){
        _mapReorganization = false;
        setState(() {});
      });
      //底下是舊的
      /*for (var element in _utilApiProvider!.peekabooRecordProcessingList) {
        String markerId = '${element.id}';
        _markers.add(Marker(
          markerId: MarkerId(markerId),
          position: LatLng(element.latitude!, element.longitude!),
          icon: await _getMarkerIcon(element.targetMemberId!, element.targetMember!.interactivePic!),
          onTap: () {
            if (_memberProvider!.memberModel.vip == VIPStatus.closure.index) {
              showImage(files: element.targetMember!.interactivePic!);
            }else if (_isFriend(id: element.targetMemberId!)) {
              showImage(files: element.targetMember!.interactivePic!);
            }else {
              unblockMemberMugshot(
                  markerId,
                  _memberProvider!.memberModel.id!,
                  element.targetMemberNickname!,
                  element.targetMemberGender!,
                  element.targetMemberBirthday!,
                  element.targetMemberId!);
            }
          },
        ));
      }
      if (mounted) {
        setState(() {});
      }*/
    }
  }

  //大頭針Icon
  Future<BitmapDescriptor> _getMarkerIcon(int id, String pic) async {
    Uint8List? markerIcon;
    if (_peekabooGameProvider!.userStatus == PeekabooUserStatus.sought) {
      //被捕方
      if (_memberProvider!.memberModel.vip == VIPStatus.closure.index) {
        markerIcon = await Util().makeReceiptImage(
          avatar: Util().getMemberMugshotUrl(pic),
          marker: AppImage.imgUser,
        );
      } else if (_isFriend(id: id)) {
        //如果是朋友了
        markerIcon = await Util().makeReceiptImage(
          avatar: Util().getMemberMugshotUrl(_stompClientProvider!.friendList
              .where((element) => element.id == id)
              .first
              .interactivePic!),
          marker: AppImage.imgUser,
        );
      } else if (_isUnlock(id: id)) {
        //如果是解鎖了
        markerIcon = await Util().makeReceiptImage(
            avatar: Util().getMemberMugshotUrl(_unlockMugshotProvider!
                .memberMugshotList
                .where((element) => element.id == id)
                .first
                .interactivePic!),
            marker: AppImage.imgUser);
      } else {
        //都不是
        markerIcon = await Util().makeReceiptImage(
            avatar: AppImage.iconCat, marker: AppImage.imgUser);
      }
    } else {
      if (_memberProvider!.memberModel.vip == VIPStatus.closure.index) {
        log('Pic: $pic');
        markerIcon = await Util().makeReceiptImage(
          avatar: Util().getMemberMugshotUrl(pic),
          marker: AppImage.imgUser,
        );
      } else if (_isFriend(id: id)) {
        //捕捉方
        markerIcon = await Util().makeReceiptImage(
            avatar: Util().getMemberMugshotUrl(_stompClientProvider!.friendList
                .where((element) => element.id == id)
                .first
                .interactivePic!),
            marker: AppImage.imgUser);
      } else if (_isUnlock(id: id)) {
        //如果是解鎖了
        markerIcon = await Util().makeReceiptImage(
            avatar: Util().getMemberMugshotUrl(_unlockMugshotProvider!
                .memberMugshotList
                .where((element) => element.id == id)
                .first
                .interactivePic!),
            marker: AppImage.imgUser);
      } else {
        markerIcon = await Util().makeReceiptImage(
            avatar: AppImage.iconDog, marker: AppImage.imgUser);
      }
    }
    return BitmapDescriptor.fromBytes(markerIcon);
  }

  //判斷是否是朋友
  bool _isFriend({required int id}) {
    return _stompClientProvider!.friendList
        .where((element) => element.id == id)
        .isNotEmpty;
  }

  //判斷此人是否有解鎖過
  bool _isUnlock({required int id}) {
    return _unlockMugshotProvider!.memberMugshotList
        .where((element) => element.id == id)
        .isNotEmpty;
  }

  //解鎖頭像
  Future<void> unblockMemberMugshot(String markerId, int fromMemberId,
      String nickName, int gender, String birthday, int targetMemberId) async {
    if (_isFriend(id: targetMemberId)) {
      if (_stompClientProvider!.friendList
          .firstWhere((element) => element.id == targetMemberId)
          .interactivePic!
          .isEmpty) {
        EasyLoading.showToast('此頭像無法放大查看');
        return;
      }
      showImage(
          files: _stompClientProvider!.friendList
              .firstWhere((element) => element.id == targetMemberId)
              .interactivePic!);
      return;
    } else if (_isUnlock(id: targetMemberId)) {
      if (_unlockMugshotProvider!.memberMugshotList
          .firstWhere((element) => element.id == targetMemberId)
          .interactivePic!
          .isEmpty) {
        EasyLoading.showToast('此頭像無法放大查看');
        return;
      }
      showImage(
          files: _unlockMugshotProvider!.memberMugshotList
              .firstWhere((element) => element.id == targetMemberId)
              .interactivePic!);
      return;
    } else {
      bool? result = await showGeneralDialog(
          context: context,
          pageBuilder: (context, anim1, anim2) {
            return UnblockMugshotDialog(
              key: UniqueKey(),
              nickName: nickName,
              gender: gender,
              birthday: birthday,
            );
          },
          barrierColor: Colors.black54,
          barrierDismissible: false,
          transitionDuration: const Duration(milliseconds: 100),
          transitionBuilder: (context, anim1, anim2, child) {
            return Transform.scale(
              scale: anim1.value,
              child: child,
            );
          });
      if (result != null) {
        if (result) {
          //想要解鎖
          if (_memberProvider!.memberPoint.point! >=
              _utilApiProvider!.systemSetting!.unlockMugshotPoints!) {
            //Log.log('解鎖大頭照');
            //點數判斷夠不夠
            AddUnlockPaymentRecordDTO _addUnlockPaymentRecordDTO =
                AddUnlockPaymentRecordDTO(
                    fromMemberId: fromMemberId, targetMemberId: targetMemberId);
            _stompClientProvider!.sendUnlockMugshot(
                addUnlockPaymentRecordDTO: _addUnlockPaymentRecordDTO);
          } else {
            if (_utilApiProvider!.systemSetting!.addPointsStatus ==
                PointsStatus.closure.index) {
              EasyLoading.showToast(S.of(context).not_enough_points);
            } else {
              //點數不足
              _pointInsufficient();
            }
          }
        } else {
          //不解鎖
          final Uint8List? markerIcon = await Util().makeReceiptImage(
              avatar:
                  _peekabooGameProvider!.userStatus == PeekabooUserStatus.sought
                      ? AppImage.iconCat
                      : AppImage.iconDog,
              marker: AppImage.imgUser);
          LatLng position = _markers
              .firstWhere((element) => element.markerId.value == markerId)
              .position;
          _markers.removeWhere((element) => element.markerId.value == markerId);
          _markers.add(Marker(
            markerId: MarkerId(markerId),
            position: position,
            icon: BitmapDescriptor.fromBytes(markerIcon!),
            onTap: () => unblockMemberMugshot(markerId, fromMemberId, nickName,
                gender, birthday, targetMemberId),
          ));
          setState(() {});
        }
      }
    }
  }

  //抓捕資訊
  Widget gotchaInformation() {
    return SizedBox(
      height: 20.h,
      child: Column(
        children: [
          SizedBox(
            height: 1.h,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              //獵取名單
              GestureDetector(
                onTap: () => _huntListOnTap(),
                child: Container(
                  padding: const EdgeInsets.only(left: 30),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                          margin: EdgeInsets.only(right: 1.w),
                          width: 10.w,
                          child: Image.asset(AppImage.iconPeekabooTodayRecord)),
                      Text(
                        S.of(context).hunt_list,
                        style: TextStyle(
                            fontSize: 18.sp, fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ),
              ),
              //紀錄
              GestureDetector(
                onTap: () => _recordOnTap(),
                child: Container(
                  padding: const EdgeInsets.only(right: 30),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                          margin: EdgeInsets.only(right: 1.w),
                          width: 10.w,
                          child: Image.asset(AppImage.iconPeekabooRecord)),
                      Text(
                        S.of(context).record,
                        style: TextStyle(
                            fontSize: 18.sp, fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                ),
              )
            ],
          ),
          //按鈕
          Container(
            padding: const EdgeInsets.only(left: 20, right: 20),
            alignment: Alignment.center,
            child: _buttonWidget(),
          ),
          //狀態
          if (_peekabooGameProvider!.userStatus != PeekabooUserStatus.seek)
            Consumer<PeekabooGameProvider>(
              builder: (context, status, _) {
                return Text(
                  status.catchStatus == CatchStatus.catchStatusComplete ||
                          status.catchStatus == CatchStatus.catchStatusCancel
                      ? S.of(context).closing
                      : S.of(context).opening,
                  style: const TextStyle(
                      color: Colors.black,
                      fontSize: 20,
                      fontWeight: FontWeight.bold),
                );
              },
            ),
        ],
      ),
    );
  }

  Widget _buttonWidget(){
    if (_peekabooGameProvider!.userStatus == PeekabooUserStatus.seek){
      return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Flexible(
            child: gotchaButton(),
          ),
        ],
      );
    }else{
      return Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          if (_stopTimeText != '00:00:00') Expanded(child: Container()),
          Flexible(
            child: gotchaButton(),
          ),
          if (_stopTimeText != '00:00:00')
            Expanded(
              child: Container(
                height: 7.h,
                alignment: Alignment.center,
                child: Text('${S.of(context).count_down}: $_stopTimeText',
                    style: TextStyle(
                        fontSize: 18.sp,
                        fontWeight: FontWeight.bold,
                        height: 1.1)),
              ),
            ),
        ],
      );
    }
  }

  //點擊獵取名單事件
  void _huntListOnTap() {
    delegate.push(name: RouteName.peekabooHuntListPage);
  }

  //點擊紀錄事件
  void _recordOnTap() {
    delegate.push(name: RouteName.peekabooRecordPage);
  }

  //切換按鈕底下文字
  Widget gotChaInfo() {
    switch (_peekabooGameProvider!.userStatus) {
      case PeekabooUserStatus.seek:
        return Consumer<UtilApiProvider>(builder: (context, isCatch, _) {
          return RichText(
              text: TextSpan(children: [
            TextSpan(
                text: S.of(context).today_gotcha,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 23.sp,
                    fontWeight: FontWeight.bold)),
            TextSpan(
                text: "  ${isCatch.isCatchCount}  ",
                style: TextStyle(
                    color: Colors.red,
                    fontSize: 27.sp,
                    fontWeight: FontWeight.bold)),
            TextSpan(
                text: S.of(context).how_many_cat,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 23.sp,
                    fontWeight: FontWeight.bold)),
          ]));
        });
      case PeekabooUserStatus.sought:
        return Consumer<UtilApiProvider>(builder: (context, beCaught, _) {
          return RichText(
              text: TextSpan(children: [
            TextSpan(
                text: S.of(context).today_be_gotcha,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 23.sp,
                    fontWeight: FontWeight.bold)),
            TextSpan(
                text: "  ${beCaught.beCaughtCount}  ",
                style: TextStyle(
                    color: Colors.red,
                    fontSize: 27.sp,
                    fontWeight: FontWeight.bold)),
            TextSpan(
                text: S.of(context).how_many_times,
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 23.sp,
                    fontWeight: FontWeight.bold)),
          ]));
        });
    }
  }

  //底下按鈕送出
  Widget gotchaButton() {
    return GestureDetector(
      onTap: () => gotchaButtonOnTap(),
      child: Container(
        width: 58.w,
        height: 7.h,
        margin: EdgeInsets.only(top: 1.h),
        child: _peekabooGameProvider!.userStatus == PeekabooUserStatus.seek
            ? buttonWidget(S.of(context).gotcha)
            : _switchButton(),
      ),
    );
  }

  //
  Widget _switchButton() {
    return Consumer<PeekabooGameProvider>(builder: (context, status, _) {
      return FlutterSwitch(
        height: 40,
        width: 80,
        padding: 5.0,
        borderRadius: 60 / 2,
        toggleSize: 30,
        activeColor: AppColor.yellowButton,
        inactiveColor: Colors.grey,
        value: status.catchStatus == CatchStatus.catchStatusProcessing,
        onToggle: (bool value) {
          gotchaButtonOnTap();
        },
      );
    });
  }

  //底下按鈕事件
  void gotchaButtonOnTap() {
    if (_peekabooGameProvider!.userStatus == PeekabooUserStatus.seek) {
      //找人的
      delegate.push(name: RouteName.peekabooCameraPage);
    } else {
      if (_stopTimeText != '00:00:00') {
        if (_peekabooGameProvider!.catchStatus ==
            CatchStatus.catchStatusProcessing) {
          EasyLoading.showToast(S.of(context).closing_time_not_yet); //還不到關閉時間
          return;
        } else {
          EasyLoading.showToast(S.of(context).not_yet_open_time); //還不到關閉時間
          return;
        }
      }
      if (_peekabooGameProvider!.catchStatus ==
              CatchStatus.catchStatusComplete ||
          _peekabooGameProvider!.catchStatus == CatchStatus.catchStatusCancel) {
        //開啟
        /*Log.log(
            '_peekabooGameProvider!.catchStatus: ${_peekabooGameProvider!.catchStatus}');*/
        if (_utilApiProvider!.hideAndSeekButtonStatus ==
            ButtonStatus.notClick) {
          _utilApiProvider!
              .addHideAndSeekRecord(
                  targetMemberId: _memberProvider!.memberModel.id!,
                  targetMemberName: _memberProvider!.memberModel.name!,
                  targetMemberNickname: _memberProvider!.memberModel.nickname!,
                  targetMemberGender: _memberProvider!.memberModel.gender!,
                  targetMemberBirthday: _memberProvider!.memberModel.birthday!,
                  address: _positionProvider!.address,
                  latLng: _positionProvider!.currentPosition,
                  catchType: WhoSide.from.index)
              .then((value) {
            _stompClientProvider!.sendStartHideAndSeek(
                id: _utilApiProvider!.peekabooRecordTargetMemberList.last.id!);
          });
        }
      } else {
        if (_utilApiProvider!.hideAndSeekButtonStatus ==
            ButtonStatus.notClick) {
          //關閉
          _stompClientProvider!.sendCancelHideAndSeek(
              id: _utilApiProvider!.peekabooRecordTargetMemberList.last.id!);
        }
      }
    }
  }

  //禮物事件
  void giftOnTap() {
  }

  ///照片放大功能
  void showImage({required String files}) {
    showGeneralDialog(
        context: context,
        pageBuilder: (context, animation, secondaryAnimation) => ImageShower(
              data: [files],
              index: 0,
            ),
        transitionBuilder: (context, animation, secondaryAnimation, child) {
          const begin = Offset(0.0, 1.0);
          const end = Offset.zero;
          const curve = Curves.ease;
          var tween =
              Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
          return SlideTransition(
            position: animation.drive(tween),
            child: child,
          );
        });
  }

  ///點數不足統一方法
  void _pointInsufficient() async {
    bool? result = await _showPointInsufficient();
    if (result != null) {
      if (result) {
        //跳轉加值
        delegate.push(name: RouteName.myPointsPage);
      }
    }
  }

  ///點數不足
  Future<bool?> _showPointInsufficient() async {
    return await showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return GamePointInsufficientDialog(
              key: UniqueKey(),
              title: S.of(context).message,
              content: S.of(context).please_add_value,
              confirmButtonText: S.of(context).go_to_premium,
              cancelButtonText: S.of(context).cancel);
        },
        barrierColor: Colors.black54,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 100),
        transitionBuilder: (context, anim1, anim2, child) {
          return child;
        });
  }

  //隱身
  Widget _flutterSwitch(){
    return FlutterSwitch(
        width: 15.w,
        height: 4.h,
        value: _stealth,
        onToggle: (val) => _onToggle(val));
  }

  void _onToggle(bool _value){
    if(_stealth != _value){
      _stealth = _value;
      setState(() {});
    }
  }

  Widget _titleBarReportButton(){
    return GestureDetector(
      onTap: () => _reportButton(),
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 5),
        child: const Icon(
          Icons.warning_amber_outlined,
          color: AppColor.appTitleBarTextColor,
          size: 25,
        ),
      ),
    );
  }

  void _reportButton() async {
    delegate.push(name: RouteName.reportDialog, arguments: _memberProvider!.memberModel.id!);
  }

}
