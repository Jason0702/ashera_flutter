import 'dart:developer';

import 'package:ashera_flutter/models/peekaboo_record_dto.dart';
import 'package:ashera_flutter/provider/stomp_client_provider.dart';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../app.dart';
import '../../dialog/identify_friends_dialog.dart';
import '../../enum/app_enum.dart';
import '../../generated/l10n.dart';
import '../../models/faces_detect_history_model.dart';
import '../../provider/member_provider.dart';
import '../../provider/position_provider.dart';
import '../../provider/util_api_provider.dart';
import '../../utils/app_color.dart';
import '../../widget/scan_ui.dart';

class PeekabooCamera extends StatefulWidget{
  const PeekabooCamera({Key? key}): super(key: key);

  @override
  State createState() => _PeekabooCameraState();
}
class _PeekabooCameraState extends State<PeekabooCamera> with TickerProviderStateMixin, WidgetsBindingObserver{
  final GlobalKey _key = GlobalKey();
  UtilApiProvider? _utilApiProvider;
  MemberProvider? _memberProvider;
  StompClientProvider? _stompClientProvider;
  PositionProvider? _positionProvider;

  //相機控制器
  CameraController? _cameraController;
  //拍攝的照片
  List<CameraDescription> _cameras = [];
  XFile? imageFile; //檔案

  double _zoom = 1.0;
  double _scaleFactor = 1.0;


  void _getCameras() async {
    _cameras = await availableCameras();
    _cameraController = CameraController(_cameras[0], ResolutionPreset.max, imageFormatGroup: ImageFormatGroup.yuv420, enableAudio: false,);
    _cameraController!.initialize().then((value) {
      if (!mounted) {
        return;
      }
      setState(() {});
    });
  }

  void _onLayoutDone(_) async {
    //先清除資料
    _utilApiProvider!.clearFaceImageList();
  }

  @override
  void initState() {
    super.initState();
    _getCameras();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    final CameraController? cameraController = _cameraController;

    // App state changed before we got the chance to initialize.
    if (cameraController == null || !cameraController.value.isInitialized) {
      return;
    }

    if (state == AppLifecycleState.inactive) {
      cameraController.dispose();
    } else if (state == AppLifecycleState.resumed) {
      onNewCameraSelected(cameraController.description);
    }
  }

  Future<void> onNewCameraSelected(CameraDescription cameraDescription) async {
    if (_cameraController != null) {
      await _cameraController!.dispose();
    }

    final CameraController cameraController = CameraController(
      cameraDescription,
      ResolutionPreset.max,
      enableAudio: false,
      imageFormatGroup: ImageFormatGroup.yuv420,
    );

    _cameraController = cameraController;

    // If the controller is updated then update the UI.
    /*cameraController.addListener(() {
      if (mounted) {
        setState(() {});
      }
      if (cameraController.value.hasError) {
        log('Camera error ${cameraController.value.errorDescription}');
      }
    });*/

    try {
      await cameraController.initialize();
    } on CameraException catch (e) {
      log('CameraException ${e.toString()}');
    }

    if (mounted) {
      setState(() {});
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    _cameraController?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _utilApiProvider = Provider.of<UtilApiProvider>(context, listen: false);
    _memberProvider = Provider.of<MemberProvider>(context, listen: false);
    _stompClientProvider = Provider.of<StompClientProvider>(context, listen: false);
    _positionProvider = Provider.of<PositionProvider>(context, listen: false);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: AppColor.appBackgroundColor,
      body: SizedBox(
        width: Device.boxConstraints.maxWidth,
        height: Device.boxConstraints.maxHeight,
        child: Stack(
          alignment: Alignment.center,
          children: [
            _cameraPreviewWidget(),
            Positioned(
                top: 40.h,
                child: CustomPaint(
                  painter: ScanFramePainter(),
                )),
            //關閉
            Positioned(
                right: 5.w,
                top: 3.h,
                child: GestureDetector(
                  onTap: () => _close(),
                  child: const Icon(
                    Icons.clear,
                    size: 30,
                    color: Colors.white,
                  ),
                )),
            //提示文字
            Positioned(
                bottom: 20.h,
                child: FittedBox(
                  child: Text(
                    S.of(context).scan_your_face_for_identification,
                    style: TextStyle(color: Colors.white, fontSize: 20.sp),
                  ),
                )),
            //按鈕
            Positioned(
                bottom: 10.h,
                child: GestureDetector(
                  onTap: () => _action(),
                  child: Container(
                    alignment: Alignment.center,
                    height: 5.h,
                    width: 20.w,
                    padding: EdgeInsets.only(right: 2.w, left: 2.w),
                    decoration: BoxDecoration(
                        color: Colors.black54,
                        borderRadius: BorderRadius.circular(30)),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        const Icon(
                          Icons.camera_alt,
                          color: Colors.white,
                          size: 25,
                        ),
                        SizedBox(
                          width: 2.w,
                        ),
                        Expanded(
                            child: FittedBox(
                              child: Text(
                                S.of(context).photograph,
                                style:
                                TextStyle(color: Colors.white, fontSize: 16.sp),
                              ),
                            ))
                      ],
                    ),
                  ),
                )),
            //相機縮放
            GestureDetector(
              behavior: HitTestBehavior.translucent,
              onScaleStart: (details){
                _zoom = _scaleFactor;
              },
              onScaleUpdate: (details){
                _scaleFactor = _zoom * details.scale;
                if(_scaleFactor >= 1.0 && _scaleFactor <= 8.0){
                  _cameraController!.setZoomLevel(_scaleFactor);
                }
              },
            )
          ],
        ),
      ),
    );
  }

  //關閉
  void _close() {
    delegate.popRoute();
  }
  //動作
  void _action() {
    //拍照
    onTakePicturePressed();
  }

  //拍照
  void onTakePicturePressed() {
    takePicture().then((XFile? file) {
      if (mounted) {
        EasyLoading.show(status: S.of(context).uploading_comparison);
        //imageFile = file; //<- 拍照結果
        PhotoType _photoType = PhotoType.tmp;
        _utilApiProvider!.upDataImage(_memberProvider!.memberModel.name!, file!.path, _photoType).then((value) { //先上傳照片
          EasyLoading.dismiss();
          if(value['status']){//上傳成功
            EasyLoading.show(status: S.of(context).photo_comparison);
            String _fileName = value['filename'];
            _utilApiProvider!.getMemberByFaceImage(_fileName).then((value) {
              if(value) {
                if(_utilApiProvider!.faceImageList.isNotEmpty){
                  //有找到人
                  EasyLoading.dismiss();
                  //跳出 捕捉成功
                  log('---------------------------有找到人! ${_utilApiProvider!.faceImageList.first.toJson()}-------------------');
                  _utilApiProvider!.getPeekabooRecordByTargetMemberName(name: _utilApiProvider!.faceImageList.first.name!, id: _memberProvider!.memberModel.id!).then((value) {
                    if(value != null){
                      CatchHideAndSeekRecordDTO _recordDTO = CatchHideAndSeekRecordDTO(
                          id: value.id,
                          fromMemberId: _memberProvider!.memberModel.id,
                          fromMemberName: _memberProvider!.memberModel.name,
                          fromMemberNickname: _memberProvider!.memberModel.nickname,
                          fromMemberGender: _memberProvider!.memberModel.gender,
                          fromMemberBirthday: _memberProvider!.memberModel.birthday);
                      _stompClientProvider!.sendDoCatchHideAndSeek(recordDTO: _recordDTO);
                      AddFacesDetectHistoryModel _addFacesDetectHistoryModel =
                      AddFacesDetectHistoryModel(
                          fromMemberId: _memberProvider!.memberModel.id,
                          fromMemberName: _memberProvider!.memberModel.name,
                          fromMemberNickname:
                          _memberProvider!.memberModel.nickname,
                          fromMemberGender: _memberProvider!.memberModel.gender,
                          targetMemberId:
                          _utilApiProvider!.faceImageList.first.id,
                          targetMemberName:
                          _utilApiProvider!.faceImageList.first.name,
                          targetMemberNickname:
                          _utilApiProvider!.faceImageList.first.nickname,
                          targetMemberGender:
                          _utilApiProvider!.faceImageList.first.gender,
                          latitude: _positionProvider!.currentPosition.latitude,
                          longitude: _positionProvider!.currentPosition.longitude,
                          address: _positionProvider!.address,
                          detectType: DetectType.followFriend.index);
                      _utilApiProvider!.postFacesDetectHistory(addFacesDetectHistoryModel: _addFacesDetectHistoryModel).then((value) {
                        _showQuickSuccess();
                      });
                    } else {
                      //沒找到人
                      EasyLoading.dismiss();
                      EasyLoading.showToast(S.of(context).no_profile_for_this_person);
                    }
                  });

                }else{
                  //沒找到人
                  EasyLoading.dismiss();
                  EasyLoading.showToast(S.of(context).no_profile_for_this_person);
                }
              }else{ //沒找到人
                EasyLoading.dismiss();
                EasyLoading.showToast(S.of(context).no_profile_for_this_person);
              }
            });
          } else { //沒上傳成功
            EasyLoading.dismiss();
            EasyLoading.showToast(S.of(context).fail);
          }
        });
      }
      if (file != null) {
        log('Picture saved to ${file.path}');
      }
    });
  }

  //拍照結果
  Future<XFile?> takePicture() async {
    final CameraController? cameraController = _cameraController;
    if (cameraController == null || !cameraController.value.isInitialized) {
      log('Error: select a camera first.');
      return null;
    }

    if (cameraController.value.isTakingPicture) {
      // A capture is already pending, do nothing.
      return null;
    }

    try {
      final XFile file = await cameraController.takePicture();
      return file;
    } on CameraException catch (e) {
      log('CameraException ${e.toString()}');
      return null;
    }
  }

  //拍照畫面
  Widget _cameraPreviewWidget() {
    final CameraController? cameraController = _cameraController;
    if (cameraController == null || !cameraController.value.isInitialized) {
      return Container(
        width: Device.boxConstraints.maxWidth,
        height: Device.boxConstraints.maxHeight,
        color: Colors.black,
      );
    } else {
      return SizedBox(
        width: Device.boxConstraints.maxWidth,
        height: Device.boxConstraints.maxHeight,
        child: CameraPreview(
          _cameraController!,
        ),
      );
    }
  }

  //捕捉成功
  void _showQuickSuccess() async{
    bool? result = await showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2){
          return IdentifyFriendsDialog(
            key: _key,
            isGame: true,
          );
        },
        barrierColor: Colors.transparent,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 200),
        transitionBuilder: (context, anim1, anim2, child){
          return Transform.scale(
            scale: anim1.value,
            child: child,
          );
        }
    );
    if(result != null){
      _utilApiProvider!.getHideAndSeekRecordByFromMemberId(id: _memberProvider!.memberModel.id!); //取得紀錄
      _utilApiProvider!.clearFaceImageList();
      //關閉
      delegate.popRoute();
    }
  }
}
