import 'package:ashera_flutter/app.dart';
import 'package:ashera_flutter/provider/member_provider.dart';
import 'package:ashera_flutter/utils/no_shadow_scroll_behavior.dart';
import 'package:ashera_flutter/utils/shared_preference.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../generated/l10n.dart';
import '../utils/app_color.dart';
import '../utils/app_size.dart';
import '../widget/button_widget.dart';
import '../widget/titlebar_widget.dart';

class ChangePasswordPage extends StatefulWidget{
  const ChangePasswordPage({Key? key}):super(key: key);

  @override
  State createState() => _ChangePasswordPageState();
}

class _ChangePasswordPageState extends State<ChangePasswordPage>{
  MemberProvider? _memberProvider;
  SharedPreferenceUtil sharedPreferenceUtil = SharedPreferenceUtil();
  //表單驗證
  final _formKey = GlobalKey<FormState>();
  //舊密碼
  final TextEditingController _oldPassword = TextEditingController();
  //新密碼
  final TextEditingController _newPassword = TextEditingController();
  //再次輸入新密碼
  final TextEditingController _twicePassword = TextEditingController();

  //是否顯示新密碼
  bool _isShowNewPassword = false;
  //是否顯示再次輸入的密碼
  bool _isShowTwicePassword = false;

  //控制
  void _showNewPassword() {
    setState(() {
      _isShowNewPassword = !_isShowNewPassword;
    });
  }

  void _showTwicePassword() {
    setState(() {
      _isShowTwicePassword = !_isShowTwicePassword;
    });
  }

  @override
  Widget build(BuildContext context) {
    _memberProvider = Provider.of<MemberProvider>(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: AppColor.appBackgroundColor,
      body: Column(
        children: [
          //返回 與 標題
          titleBar(S.of(context).change_password),
          SizedBox(height: 1.h,),
          Expanded(flex: 10, child: _body())
        ],
      ),
    );
  }
  //內容
  Widget _body() {
    return Container(
      width: Device.boxConstraints.maxWidth,
      decoration: const BoxDecoration(
        color: Colors.white
      ),
      child: ScrollConfiguration(
        behavior: NoShadowScrollBehavior(),
        child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(height: 5.h,),
                //舊密碼
                _oldPasswordTextField(),
                //新密碼
                _newPasswordTextField(),
                //再次輸入新密碼
                _twicePasswordTextField(),
                SizedBox(height: 5.h,),
                //確認變更按鈕
                _confirmChangesButton(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  //舊密碼
  Widget _oldPasswordTextField(){
    return SizedBox(
      width: 80.w,
      height: 8.h,
      child: TextFormField(
        style: const TextStyle(color: Colors.black),
        decoration: InputDecoration(
          fillColor: Colors.transparent,
          filled: true,
          hintStyle: const TextStyle(color: Colors.grey),
          hintText: S.of(context).enter_old_password,
        ),
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.next,
        controller: _oldPassword,
        validator: (value){
          if(value == null || value.isEmpty){
            return S.of(context).enter_password;
          }else if(value.trim().length < 6 || value.trim().length > 12){
            return S.of(context).enter_alphanumeric_characters;
          }
          return null;
        },
      ),
    );
  }

  //新密碼
  Widget _newPasswordTextField(){
    return SizedBox(
      width: 80.w,
      height: 8.h,
      child: TextFormField(
        style: const TextStyle(color: Colors.black),
        decoration: InputDecoration(
          suffixIcon: IconButton(
            icon: Icon(
              _isShowNewPassword ? Icons.visibility : Icons.visibility_off,
              color: Colors.grey[600],
              size: 25,
            ),
            onPressed: () => _showNewPassword(),
          ),
          fillColor: Colors.transparent,
          filled: true,
          hintStyle: const TextStyle(color: Colors.grey),
          hintText: S.of(context).enter_new_password,
        ),
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.next,
        controller: _newPassword,
        obscureText: !_isShowNewPassword,
        validator: (value){
          if(value == null || value.isEmpty){
            return S.of(context).enter_password;
          }else if(value.trim().length < 6 || value.trim().length > 12){
            return S.of(context).enter_alphanumeric_characters;
          }
          return null;
        },
      ),
    );
  }

  //再次輸入新密碼
  Widget _twicePasswordTextField(){
    return SizedBox(
      width: 80.w,
      height: 8.h,
      child: TextFormField(
        style: const TextStyle(color: Colors.black),
        decoration: InputDecoration(
          suffixIcon: IconButton(
            icon: Icon(
              _isShowTwicePassword ? Icons.visibility : Icons.visibility_off,
              color: Colors.grey[600],
              size: 25,
            ),
            onPressed: () => _showTwicePassword(),
          ),
          fillColor: Colors.transparent,
          filled: true,
          hintStyle: const TextStyle(color: Colors.grey),
          hintText: S.of(context).enter_password_again,
        ),
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.done,
        controller: _twicePassword,
        obscureText: !_isShowTwicePassword,
        validator: (value){
          if(value == null || value.isEmpty){
            return S.of(context).enter_password;
          }else if(value.trim().length < 6 || value.trim().length > 12){
            return S.of(context).enter_alphanumeric_characters;
          }else if(value != _newPassword.text){
            return S.of(context).different_passwords_entered_twice;
          }
          return null;
        },
      ),
    );
  }

  //確認變更按鈕
  Widget _confirmChangesButton(){
    return GestureDetector(
      onTap: () => _confirmChangesOnTap(),
      child: Container(
        width: 58.w,
        height: 7.h,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(AppSize.buttonCircular),
            gradient: AppColor.appMainColor),
        child: buttonWidget(S.of(context).confirm_changes)
      ),
    );
  }
  //點擊確認變更
  void _confirmChangesOnTap(){
    if(!_formKey.currentState!.validate()){
      if(!EasyLoading.isShow){
        EasyLoading.showToast(S.of(context).information_is_incomplete);
      }
    }else{//變更密碼
      EasyLoading.show(status: 'Loading...');
      _memberProvider!.putMemberPassword(_memberProvider!.memberModel.id! ,_newPassword.text.trim(), _oldPassword.text.trim()).then((value){
        if(value){
          EasyLoading.showToast(S.of(context).finish);
          sharedPreferenceUtil.savePassword(_newPassword.text.trim());
          delegate.popRoute();
        }
      });
    }
  }

}