import 'dart:convert';
import 'dart:developer';

import 'package:ashera_flutter/dialog/friend_blockade_delete_dialog.dart';
import 'package:ashera_flutter/models/add_follower_request_dto.dart';
import 'package:ashera_flutter/models/message_record_model.dart';
import 'package:ashera_flutter/provider/member_provider.dart';
import 'package:ashera_flutter/provider/stomp_client_provider.dart';
import 'package:ashera_flutter/utils/no_shadow_scroll_behavior.dart';
import 'package:ashera_flutter/widget/app_popup_windows/popup_window.dart';
import 'package:ashera_flutter/widget/friend_slidable.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_font_icons/flutter_font_icons.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../app.dart';
import '../enum/app_enum.dart';
import '../generated/l10n.dart';
import '../models/get_chat_message_dto.dart';
import '../models/member_model.dart';
import '../provider/member_black_list_provider.dart';
import '../provider/menu_status_provider.dart';
import '../routers/route_name.dart';
import '../utils/api.dart';
import '../utils/app_color.dart';
import '../utils/app_image.dart';
import '../utils/util.dart';
import '../widget/app_popup_windows/app_popup_menu.dart';
import '../dialog/menu_dialog.dart';
import '../widget/avatar_image.dart';
import '../widget/titlebar_widget.dart';
import 'package:ashera_flutter/widget/app_popup_windows/popup_menu_copy.dart'
    as copy;

class FriendPage extends StatefulWidget {
  const FriendPage({Key? key}) : super(key: key);

  @override
  State createState() => _FriendPageState();
}

class _FriendPageState extends State<FriendPage> {
  StompClientProvider? _stompClientProvider;
  MemberProvider? _memberProvider;
  MemberBlackListProvider? _memberBlackListProvider;
  //加好友選單
  late AppPopupMenu<AddFriendStatus> _addFriendMenu;

  //好友
  List<MemberModel> _friendList = [];
  MenuStatusProvider? _menuStatusProvider;
  //顯示用
  List<MemberModel> items = [];

  final TextEditingController _searchText = TextEditingController();

  _onLayoutDone(_) async {
    _memberBlackListProvider!.addListener(_refreshFriend);
  }

  @override
  void dispose() {
    super.dispose();
    _memberBlackListProvider!.removeListener(_refreshFriend);
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);

    _addFriendMenu = PopupWindows(
      initialValue: AddFriendStatus.none,
      icon: const Icon(
        FontAwesome5Solid.user_plus,
        color: AppColor.appTitleBarTextColor,
        size: 25,
      ),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(16),
      ),
      onSelected: (AddFriendStatus value) {
        debugPrint('選擇了 $value');
        if (value == AddFriendStatus.addFriend) {
          //新增好友
          delegate.push(name: RouteName.addFriendPage);
        } else if (value == AddFriendStatus.scanQr) {
          //選擇掃描QRcode
          delegate.push(
              name: RouteName.cameraPage,
              arguments: {'type': PhotographOrScanning.scanning});
        }
      },
      onCanceled: () {
        debugPrint('什麼也沒選');
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    _stompClientProvider = Provider.of<StompClientProvider>(context);
    _memberProvider = Provider.of<MemberProvider>(context, listen: false);
    _memberBlackListProvider =
        Provider.of<MemberBlackListProvider>(context, listen: false);
    _refreshFriend();
    return Consumer<MenuStatusProvider>(builder: (context, menuStatus, _) {
      _menuStatusProvider = menuStatus;
      return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: AppColor.appBackgroundColor,
        body: menuStatus.menuStatus == MenuStatus.menu
            //選單
            ? MenuWidget(
                key: UniqueKey(),
              )
            //好友
            : Column(
                children: [
                  //選項 與 標題 提醒 加好友
                  titleBarIntegrate([_menuWidget()], S.of(context).friend,
                      [_noticeWidget(), _addFriendWidget()]),
                  //搜尋
                  Container(
                    width: 90.w,
                    margin: const EdgeInsets.only(top: 13, bottom: 13),
                    decoration: const BoxDecoration(color: Colors.transparent),
                    child: TextField(
                      style: const TextStyle(color: Colors.black),
                      onChanged: (value) {
                        _filterSearchResults(value);
                      },
                      controller: _searchText,
                      decoration: InputDecoration(
                        contentPadding: const EdgeInsets.only(
                            left: 20.0, right: 10.0, top: 10.0, bottom: 10.0),
                        enabledBorder: OutlineInputBorder(
                            borderSide: const BorderSide(color: Colors.grey),
                            borderRadius: BorderRadius.circular(25.0)),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25.0),
                          borderSide: const BorderSide(color: Colors.grey),
                        ),
                        focusedBorder: OutlineInputBorder(
                            borderSide: const BorderSide(color: Colors.grey),
                            borderRadius: BorderRadius.circular(25.0)),
                        prefixIcon: const Icon(Icons.search),
                        fillColor: Colors.transparent,
                        filled: true,
                        hintStyle: const TextStyle(color: Colors.grey),
                        hintText: S.of(context).search,
                      ),
                    ),
                  ),
                  //內容
                  Expanded(flex: 8, child: _bodyFriend())
                ],
              ),
      );
    });
  }

  void _refreshFriend() {
    if (_searchText.text.isEmpty) {
      items.clear();
      _friendList.clear();
      _friendList = List.from(_stompClientProvider!.friendList);
      if (_memberBlackListProvider!.blackListByFromMember.isNotEmpty) {
        //log('黑名單過濾');
        for (var element in _memberBlackListProvider!.blackListByFromMember) {
          _friendList
              .removeWhere((value) => value.id == element.targetMemberId);
        }
      }
      _friendList
          .sort((first, last) => first.nickname!.compareTo(last.nickname!));
      items.addAll(_friendList);
    } else {
      _friendList.clear();
      _friendList = List.from(_stompClientProvider!.friendList);
      if (_memberBlackListProvider!.blackListByFromMember.isNotEmpty) {
        //log('黑名單過濾');
        for (var element in _memberBlackListProvider!.blackListByFromMember) {
          _friendList
              .removeWhere((value) => value.id == element.targetMemberId);
        }
      }
      _friendList
          .sort((first, last) => first.nickname!.compareTo(last.nickname!));
    }
  }

  //左三條線
  Widget _menuWidget() {
    return GestureDetector(
      onTap: () => _barsOnTap(),
      child: const Icon(
        FontAwesome.bars,
        color: AppColor.appTitleBarTextColor,
        size: 25,
      ),
    );
  }

  //右邊 通知
  Widget _noticeWidget() {
    return Consumer<StompClientProvider>(builder: (context, notice, _) {
      return Stack(
        children: [
          GestureDetector(
            onTap: () => _noticeOnTap(),
            child: Container(
              margin: const EdgeInsets.all(10),
              child: const Icon(
                FontAwesome.bell,
                color: AppColor.appTitleBarTextColor,
                size: 25,
              ),
            ),
          ),
          if (notice.wantAddMeList.isNotEmpty)
            Positioned(
                top: 8,
                right: 8,
                child: Container(
                  width: 2.w,
                  height: 2.w,
                  decoration: BoxDecoration(
                      color: Colors.red,
                      borderRadius: BorderRadius.circular(20)),
                ))
        ],
      );
    });
  }

  //右邊 加好友
  Widget _addFriendWidget() {
    return Container(
      child: _addFriendMenu.set(
        offset: const Offset(0, 50),
        menuItems: [
          copy.PopupMenuItem(
              value: AddFriendStatus.addFriend,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  const Icon(
                    FontAwesome5Solid.user_plus,
                    color: AppColor.appTitleBarTextColor,
                    size: 25,
                  ),
                  SizedBox(
                    width: 3.w,
                  ),
                  Text(
                    S.of(context).add_friends,
                    style: TextStyle(color: Colors.black, fontSize: 18.sp),
                  )
                ],
              )),
          copy.PopupMenuItem(
              value: AddFriendStatus.scanQr,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  const Icon(
                    FontAwesome5Solid.qrcode,
                    color: AppColor.appTitleBarTextColor,
                    size: 25,
                  ),
                  SizedBox(
                    width: 3.w,
                  ),
                  Text(
                    S.of(context).scan_qr,
                    style: TextStyle(color: Colors.black, fontSize: 18.sp),
                  )
                ],
              )),
        ],
      ),
    );
  }

  //選項
  void _barsOnTap() {
    _menuStatusProvider!.openMenu();
  }

  //通知
  void _noticeOnTap() {
    delegate.push(name: RouteName.noticePage);
  }

  void chatListOnTap(int index) {
    //log('好友點擊 ${items[index].toJson()}');
    //取得聊天紀錄
    GetChatMessageDTO _getChatMessageDTO = GetChatMessageDTO(
        fromMember: _memberProvider!.memberModel.name!,
        targetMember: items[index].name,
        date: '',
        page: 0,
        size: 10,
        sortBy: 'created_at');
    _stompClientProvider!
        .sendMessage(Stomp.getChatMessagePage, json.encode(_getChatMessageDTO));
    delegate.push(name: RouteName.chatRoomPage, arguments: items[index]);
  }

  Widget _bodyFriend() {
    return Container(
      decoration: const BoxDecoration(color: Colors.white),
      child: Column(
        children: [
          //小幫手
          Container(
            margin: const EdgeInsets.only(left: 25, top: 10),
            child: _customerServiceWidget(),
          ),
          //好友
          Container(
            height: 3.h,
            alignment: Alignment.centerLeft,
            margin: const EdgeInsets.only(left: 20),
            child: Text(
              '${S.of(context).friend}${_friendList.length}',
              style: TextStyle(
                  color: AppColor.buttonFrameColor,
                  fontSize: 18.sp,
                  fontWeight: FontWeight.bold),
            ),
          ),
          //好友列表
          Expanded(
              child: Container(
            margin: const EdgeInsets.only(left: 25, top: 10),
            child: ScrollConfiguration(
              behavior: NoShadowScrollBehavior(),
              child: items.isEmpty
                  ? SizedBox(
                      width: Device.width,
                      height: Device.height,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image(
                            width: 20.w,
                            height: 20.h,
                            image: AssetImage(AppImage.imgWaiting),
                          ),
                          Text(
                            S.of(context).currently_have_no_friends,
                            style: TextStyle(
                                color: Colors.grey[300]!,
                                fontWeight: FontWeight.bold,
                                fontSize: 20.sp),
                          )
                        ],
                      ),
                    )
                  : ListView.builder(
                      shrinkWrap: true,
                      itemCount: items.length,
                      itemBuilder: (context, index) {
                        return friendCard(index);
                      }),
            ),
          ))
        ],
      ),
    );
  }

  Widget friendCard(int index) {
    return FriendSlidable(
        blockade: (context) =>
            _showFriendBlockadeDialog(blockadeUser: items[index]),
        delete: (context) => _showFriendDeleteDialog(deleteUser: items[index]),
        child: GestureDetector(
          onTap: () => chatListOnTap(index),
          child: Container(
            margin: const EdgeInsets.only(top: 0, bottom: 5),
            color: Colors.white,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                //大頭照
                Consumer<StompClientProvider>(builder: (context, friend, _) {
                  return avatarImage(index, friend, items);
                }),
                Container(
                  child: Text(
                    items[index].nickname!,
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 18.sp,
                        color: Colors.black),
                  ),
                  margin: const EdgeInsets.only(left: 10),
                )
              ],
            ),
          ),
        ));
  }

//好友搜尋功能
  void _filterSearchResults(String _query) {
    List<MemberModel> _dummySearchList = [];
    //log('搜尋好友');
    _dummySearchList.addAll(_friendList);
    if (_query.isNotEmpty) {
      List<MemberModel> _dummyListData = [];
      for (var item in _dummySearchList) {
        if (item.nickname!.contains(_query)) {
          _dummyListData.add(item);
        }
      }
      items.clear();
      items.addAll(_dummyListData);
      items.sort((first, last) => first.nickname!.compareTo(last.nickname!));
      setState(() {});
      return;
    } else {
      items.clear();
      items.addAll(_friendList);
      items.sort((first, last) => first.nickname!.compareTo(last.nickname!));
      setState(() {});
    }
  }

  ///封鎖對話框
  /// * [blockadeUser] 要被封鎖的用戶
  void _showFriendBlockadeDialog({required MemberModel blockadeUser}) async {
    bool? _result = await showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return FriendBlockadeDeleteDialog(
              key: UniqueKey(),
              title: S.of(context).message,
              content:
                  '${S.of(context).sure}${S.of(context).blockade} ${blockadeUser.nickname}?',
              confirmButtonText: S.of(context).sure);
        },
        barrierColor: Colors.black54,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 100),
        transitionBuilder: (context, anim1, anim2, child) {
          return Transform.scale(
            scale: anim1.value,
            child: child,
          );
        });
    if (_result != null) {
      //確定封鎖
      if (_result) {
        MemberBlacklistDTO _memberBlacklistDTO = MemberBlacklistDTO(
            fromMemberId: _memberProvider!.memberModel.id!,
            targetMemberId: blockadeUser.id!);
        EasyLoading.show(status: 'Loading...');
        _memberBlackListProvider!
            .addMemberBlackList(memberBlacklistDTO: _memberBlacklistDTO)
            .then((value) {
          if (value) {
            EasyLoading.showSuccess(
                '${S.of(context).blockade}${S.of(context).finish}');
            _memberBlackListProvider!.getMemberBlackListByFromMemberId(
                id: _memberProvider!.memberModel.id!);
          } else {
            EasyLoading.showError(
                '${S.of(context).blockade}${S.of(context).fail}');
          }
        });
      }
    }
  }

  ///刪除對話框
  /// * [deleteUser] 要被刪除的用戶
  void _showFriendDeleteDialog({required MemberModel deleteUser}) async {
    bool? _result = await showGeneralDialog(
      context: context,
      pageBuilder: (context, anim1, anim2) {
        return FriendBlockadeDeleteDialog(
            key: UniqueKey(),
            title: S.of(context).message,
            content:
                '${S.of(context).sure}${S.of(context).delete} ${deleteUser.nickname}?',
            confirmButtonText: S.of(context).sure);
      },
    );
    if (_result != null) {
      //確定刪除
      if (_result) {
        AddFollowerRequestDTO addFollowerRequestDTO = AddFollowerRequestDTO(
            followerId: deleteUser.id!,
            memberId: _memberProvider!.memberModel.id!);
        _stompClientProvider!
            .sendDeleteFriend(addFollowerRequestDTO: addFollowerRequestDTO);
      }
    }
  }

  //小幫手
  Widget _customerServiceWidget() {
    return GestureDetector(
      onTap: () => _customerServiceOnTap(),
      child: Container(
        margin: const EdgeInsets.only(top: 0, bottom: 5),
        color: Colors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.max,
          children: [
            //小幫手
            Flexible(
                child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                Stack(
                  children: [
                    Consumer<MemberProvider>(
                      builder: (context, vm, _){
                        if(vm.customerService == null){
                          return noAvatarImage();
                        }
                        return customerServiceAvatarImage(vm.customerService);
                      },
                    ),
                    Positioned(
                        top: 5,
                        right: 0,
                        child: Consumer<MemberProvider>(
                          builder: (context, vm, _) {
                            if (vm.customerService == null) {
                              return Container(
                                width: 11,
                                height: 11,
                                decoration: BoxDecoration(
                                    color: Colors.grey,
                                    border: Border.all(
                                        color: Colors.white, width: 1),
                                    borderRadius: BorderRadius.circular(20)),
                              );
                            }
                            return Container(
                              width: 11,
                              height: 11,
                              decoration: BoxDecoration(
                                  color: vm.customerService!.hideStatus! == 1
                                      ? Colors.green
                                      : Colors.grey,
                                  border:
                                      Border.all(color: Colors.white, width: 1),
                                  borderRadius: BorderRadius.circular(20)),
                            );
                          },
                        ))
                  ],
                ),
                Container(
                  margin: const EdgeInsets.only(left: 10),
                  child: Text(
                    S.of(context).little_helper,
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 18.sp,
                        color: Colors.black),
                  ),
                )
              ],
            )),
            //未讀訊息
            Flexible(child: Consumer2<MemberProvider, StompClientProvider>(builder: (context, v, s, _){
              if(v.customerService == null){
                return Container();
              }
              if(s.messageRecordList.where((element) => element.fromMember == v.customerService!.name).isNotEmpty){
                MessageLastRecordModel _messageRecordData = s.messageRecordList.where((element) => element.fromMember == v.customerService!.name).first;
                if(_messageRecordData.unreadCnt != 0){
                  return Container(
                      margin: const EdgeInsets.only(right: 30),
                      height: 5.w,
                      width:
                      Util().redDotWidth(_messageRecordData.unreadCnt.toString()),
                      alignment: Alignment.center,
                      decoration: BoxDecoration(
                          color: AppColor.buttonFrameColor,
                          borderRadius: BorderRadius.circular(30)),
                      child: Text(
                        _messageRecordData.unreadCnt.toString(),
                        textScaleFactor: 1,
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 16.sp,
                            fontWeight: FontWeight.bold),
                      ));
                }
              }
              return Container();
            },))
          ],
        ),
      ),
    );
  }

  //客服
  void _customerServiceOnTap() {
    GetChatMessageDTO _getChatMessageDTO = GetChatMessageDTO(
        fromMember: _memberProvider!.memberModel.name!,
        targetMember: _memberProvider!.customerService!.name!,
        date: '',
        page: 0,
        size: 10,
        sortBy: 'created_at');
    _stompClientProvider!
        .sendMessage(Stomp.getChatMessagePage, json.encode(_getChatMessageDTO));
    delegate.push(
        name: RouteName.chatRoomPage,
        arguments: _memberProvider!.customerService);
  }
}
