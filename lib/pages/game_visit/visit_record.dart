
import 'package:ashera_flutter/provider/util_api_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../enum/app_enum.dart';
import '../../generated/l10n.dart';
import '../../provider/member_provider.dart';
import '../../provider/visit_game_provider.dart';
import '../../utils/app_color.dart';
import '../../utils/app_image.dart';
import '../../utils/no_shadow_scroll_behavior.dart';
import '../../utils/shared_preference.dart';
import '../../utils/util.dart';
import '../../widget/mugshot_widget.dart';
import '../../widget/titlebar_widget.dart';

class VisitRecord extends StatefulWidget {
  const VisitRecord({Key? key}) : super(key: key);

  @override
  _VisitRecordState createState() => _VisitRecordState();
}

class _VisitRecordState extends State<VisitRecord>{
  VisitGameProvider? _visitGameProvider;
  MemberProvider? _memberProvider;
  UtilApiProvider? _utilApiProvider;

  _onLayoutDone(_) async {
    if(_visitGameProvider!.userStatus == VisitMode.visitModeVisit){
      //探班
      await _utilApiProvider!.getByFromMemberId(_memberProvider!.memberModel.id!, true);
      setState(() {});
    }else{
      //被探班
      await _utilApiProvider!.getByTargetMemberId(_memberProvider!.memberModel.id!, true);
      setState(() {});
    }
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  Widget build(BuildContext context) {
    _visitGameProvider = Provider.of<VisitGameProvider>(context);
    _memberProvider = Provider.of<MemberProvider>(context);
    _utilApiProvider = Provider.of<UtilApiProvider>(context);
    return Scaffold(
      body: Column(
        children: [
          _titleBar(),
          SizedBox(height: 1.h,),
          Expanded(child: _listWidget())
        ],
      ),
    );
  }

  //標題
  Widget _titleBar() {
    if (_visitGameProvider?.userStatus == VisitMode.visitModeVisit) {
      return titleBar("${S.of(context).visit}${S.of(context).record}");
    } else {
      return titleBar("${S.of(context).visited}${S.of(context).record}");
    }
  }

  //列表
  Widget _listWidget() {
    return ScrollConfiguration(
      behavior: NoShadowScrollBehavior(),
      child: Consumer<UtilApiProvider>(builder: (context, record, _){
        if(_visitGameProvider!.userStatus == VisitMode.visitModeVisit){ //探班
          record.fromMemberAllVisitRecord.sort((first, last) => last.completeAt!.compareTo(first.completeAt!));
          return record.fromMemberAllVisitRecord.isEmpty ? SizedBox(
            width: Device.width,
            height: Device.height,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image(
                  width: 20.w,
                  height: 20.h,
                  image: AssetImage(AppImage.imgWaiting),
                ),
                Text(
                  S.of(context).there_are_currently_no_records,
                  style: TextStyle(
                      color: Colors.grey[300]!,
                      fontWeight: FontWeight.bold,
                      fontSize: 20.sp),
                )
              ],
            ),
          ) : ListView.builder(
              shrinkWrap: true,
              itemCount: record.fromMemberAllVisitRecord.length,
              itemBuilder: (context, index) {
                return _listCard(index);
              });
        } else {
          record.targetMemberAllVisitRecord.sort((first, last) => last.completeAt!.compareTo(first.completeAt!));
          return record.targetMemberAllVisitRecord.isEmpty ? SizedBox(
            width: Device.width,
            height: Device.height,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image(
                  width: 20.w,
                  height: 20.h,
                  image: AssetImage(AppImage.imgWaiting),
                ),
                Text(
                  S.of(context).there_are_currently_no_records,
                  style: TextStyle(
                      color: Colors.grey[300]!,
                      fontWeight: FontWeight.bold,
                      fontSize: 20.sp),
                )
              ],
            ),
          ) : ListView.builder(
              shrinkWrap: true,
              itemCount: record.targetMemberAllVisitRecord.length,
              itemBuilder: (context, index) {
                return _listCard(index);
              });
        }
      }),
    );
  }
  //列表卡片
  Widget _listCard(int index){
    return Consumer<UtilApiProvider>(builder: (context, record, _){
     if(_visitGameProvider!.userStatus == VisitMode.visitModeVisit){
       return Container(
         padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 5),
         decoration: const BoxDecoration(
             color: Colors.white,
             border: Border(
                 bottom: BorderSide(width: 1,color: AppColor.grayLine)
             )
         ),
         child: Row(
           children: [
             //頭像
             MugshotWidget(
                 fromMemberId: _memberProvider!.memberModel.id!,
                 nickName: record.fromMemberAllVisitRecord[index].targetMemberNickname!,
                 gender:record.fromMemberAllVisitRecord[index].targetMemberGender!,
                 birthday: record.fromMemberAllVisitRecord[index].targetMemberBirthday!,
                 targetMemberId: record.fromMemberAllVisitRecord[index].targetMemberId!,
                  interactive: true,
             ),
             Expanded(
               child: Container(
                 margin: const EdgeInsets.only(left: 10),
                 child: Column(
                   crossAxisAlignment: CrossAxisAlignment.start,
                   children: [
                     //名稱&時間
                     Row(
                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
                       children: [
                         //名稱
                         FittedBox(
                           child: Text(
                             record.fromMemberAllVisitRecord[index].targetMemberNickname!,
                             style: TextStyle(
                                 fontWeight: FontWeight.bold,
                                 fontSize: 18.sp,
                                 color: Colors.black),
                           ),
                         ),
                         //時間
                         FittedBox(
                           alignment: Alignment.topCenter,
                           child: Text(
                             _getTime(record.fromMemberAllVisitRecord[index].createdAt!.ceil()),
                             style: TextStyle(
                                 fontWeight: FontWeight.bold,
                                 fontSize: 15.sp,
                                 color:  AppColor.grayText),
                           ),
                         ),
                       ],
                     ),
                     //備註
                     FittedBox(
                       child: Text(
                         record.fromMemberAllVisitRecord[index].requirement!,
                         style: TextStyle(
                             fontWeight: FontWeight.bold,
                             fontSize: 15.sp,
                             color: AppColor.grayText),
                       ),
                     ),
                   ],
                 ),
               ),
             ),

           ],
         ),
       );
     } else {
       return Container(
         padding: const EdgeInsets.symmetric(horizontal: 20,vertical: 5),
         decoration: const BoxDecoration(
             color: Colors.white,
             border: Border(
                 bottom: BorderSide(width: 1,color: AppColor.grayLine)
             )
         ),
         child: Row(
           children: [
             //頭像
             MugshotWidget(
                 fromMemberId: _memberProvider!.memberModel.id!,
                 nickName: record.targetMemberAllVisitRecord[index].fromMemberNickname!,
                 gender: record.targetMemberAllVisitRecord[index].fromMemberGender!,
                 birthday: record.targetMemberAllVisitRecord[index].fromMemberBirthday!,
                 targetMemberId:record.targetMemberAllVisitRecord[index].fromMemberId!,
               interactive: true,),
             Expanded(
               child: Container(
                 margin: const EdgeInsets.only(left: 10),
                 child: Column(
                   crossAxisAlignment: CrossAxisAlignment.start,
                   children: [
                     //名稱&時間
                     Row(
                       mainAxisAlignment: MainAxisAlignment.spaceBetween,
                       children: [
                         //名稱
                         FittedBox(
                           child: Text(
                             record.targetMemberAllVisitRecord[index].fromMemberNickname!,
                             style: TextStyle(
                                 fontWeight: FontWeight.bold,
                                 fontSize: 18.sp,
                                 color: Colors.black),
                           ),
                         ),
                         //時間
                         FittedBox(
                           alignment: Alignment.topCenter,
                           child: Text(
                             _getTime(record.targetMemberAllVisitRecord[index].createdAt!.ceil()),
                             style: TextStyle(
                                 fontWeight: FontWeight.bold,
                                 fontSize: 15.sp,
                                 color:  AppColor.grayText),
                           ),
                         ),
                       ],
                     ),
                     //備註
                     FittedBox(
                       child: Text(
                         record.targetMemberAllVisitRecord[index].requirement!,
                         overflow: TextOverflow.ellipsis,
                         style: TextStyle(
                             fontWeight: FontWeight.bold,
                             fontSize: 15.sp,
                             color: AppColor.grayText),
                       ),
                     ),
                   ],
                 ),
               ),
             ),

           ],
         ),
       );
     }
    });
  }

  //探班時間
  String _getTime(int _time){
    DateTime _createdAt = DateTime.fromMillisecondsSinceEpoch(_time * 1000, isUtc: false);
    return Util().visitRecordTime(_createdAt);
  }
}
