import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'dart:typed_data';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_font_icons/flutter_font_icons.dart';
import 'package:flutter_linkify/flutter_linkify.dart';
import 'package:heic_to_jpg/heic_to_jpg.dart';
import 'package:image_pickers/image_pickers.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:url_launcher/url_launcher.dart';

import '../../app.dart';
import '../../enum/app_enum.dart';
import '../../generated/l10n.dart';
import '../../models/chat_message_model.dart';
import '../../models/member_model.dart';
import '../../models/message_dto.dart';
import '../../models/visit_record_model.dart';
import '../../provider/member_provider.dart';
import '../../provider/stomp_client_provider.dart';
import '../../provider/util_api_provider.dart';
import '../../provider/web_socket_provider.dart';
import '../../routers/route_name.dart';
import '../../utils/api.dart';
import '../../utils/app_color.dart';
import '../../utils/app_size.dart';
import '../../utils/shared_preference.dart';
import '../../utils/util.dart';
import '../../widget/aspect_ratio_video.dart';
import '../../widget/audio_player.dart';
import '../../widget/image_shower.dart';
import '../../widget/mugshot_widget.dart';

class VisitChatRoomPage extends StatefulWidget{

  const VisitChatRoomPage({Key? key}): super(key: key);

  @override
  State createState() => _VisitChatRoomPageState();
}

class _VisitChatRoomPageState extends State<VisitChatRoomPage>{

  //訊息Controller
  final TextEditingController _message = TextEditingController();

  //未解鎖大頭貼
  UtilApiProvider? _utilApiProvider;

  //STOMP 傳訊息用
  StompClientProvider? _stompClientProvider;

  //WebSocket 傳檔案用
  WebSocketProvider? _webSocketProvider;

  //訂單
  VisitRecordModel? _visitRecord;

  //對方
  MemberModel? member;

  //我自己
  MemberProvider? _memberProvider;

  //聊天Controller
  ScrollController chat = ScrollController();

  //已讀用
  int _messageLen = 0;

  //移動到最底
  void _moveToBottom() {
    Future.delayed(const Duration(milliseconds: 500), () {
      if (chat.positions.isNotEmpty) {
        chat.animateTo(chat.position.minScrollExtent,
            duration: const Duration(milliseconds: 10),
            curve: Curves.easeInOut);
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _stompClientProvider = Provider.of<StompClientProvider>(context);
    _memberProvider = Provider.of<MemberProvider>(context);
    _webSocketProvider = Provider.of<WebSocketProvider>(context);
    _utilApiProvider = Provider.of<UtilApiProvider>(context);
    Map<String, dynamic> _map = ModalRoute.of(context)!.settings.arguments as Map<String, dynamic>;
    member = _map['member'] as MemberModel;
    _visitRecord = _map['visitRecord'] as VisitRecordModel;
    //log('member: ${member!.toJson()}');
    var _webUploadStatus =
    Provider.of<WebSocketProvider>(context, listen: false);
    _uploadStatus(_webUploadStatus);
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: AppColor.appBackgroundColor,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          //標題
          titleBarChatIntegrate([_titleBarBackButton()],member!.nickname!,[_titleBarReportButton()]),
          //訊息
          Flexible(child: _messageList()),
          //底部欄
          _bottomBar(),
        ],
      ),
    );
  }

  //訊息串滾動
  Widget _messageList() {
    return SizedBox(
      height: MediaQuery.of(context).size.height,
      child: Stack(
        alignment: Alignment.topCenter,
        children: [
          Consumer<StompClientProvider>(builder: (context, p ,_){
            return ListView.builder(
              controller: chat,
              shrinkWrap: true,
              reverse: true,
              itemBuilder: (context, index) {
                if (p.isRoomNewMessage) {
                  Future.delayed(Duration.zero, () async {
                    p.setRoomNewMessage();
                  });
                  _moveToBottom();
                }
                if (_messageLen != p.visitChatRecordList.length) {
                  _messageLen = p.visitChatRecordList.length;
                  if (p.visitChatRecordList
                      .where((element) => element.fromMember == member!.name)
                      .isNotEmpty) {
                    p.updateVisitIsRead(p.visitChatRecordList.firstWhere(
                            (element) => element.fromMember == member!.name));
                  }
                }
                return _buildItem(index, p.visitChatRecordList);
              },
              itemCount: p.visitChatRecordList.length,
            );
          }),
        ],
      ),
    );
  }

  //訊息串
  Widget _buildItem(index, List<ChatMessageModel> messageData) {
    bool lastMessageIsMe = false;
    //region 判斷上面是否需要加天數
    DateTime time;
    if (index == messageData.length - 1) {
      time = Util().utcTimeToLocal(messageData.first.createdAt!);
      lastMessageIsMe = true;
    } else {
      time = Util().utcTimeToLocal(messageData[index + 1].createdAt!);
      lastMessageIsMe =
          messageData[index].fromMember != messageData[index + 1].fromMember;
    }
    //留言時間
    DateTime messageTime = Util().utcTimeToLocal(messageData[index].createdAt!);
    bool isChangeDay = false;
    //兩者差距>1
    if ((messageTime.day - time.day).abs() > 0) {
      time = messageTime;
      isChangeDay = true;
    }
    if (isChangeDay) {
      lastMessageIsMe = true;
    }
    //endregion
    //自己發的訊息
    bool isMe =
        messageData[index].fromMember == _memberProvider!.memberModel.name;
    return Column(
      children: [
        //上方日期
        isChangeDay || index == messageData.length - 1
            ? Container(
          width: 20.w,
          padding: EdgeInsets.only(top: 0.5.h, bottom: 0.5.h),
          margin: EdgeInsets.only(top: 3.h, bottom: 3.h),
          alignment: Alignment.center,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(25)),
          child: Text(Util().titleTime(time),
              textScaleFactor: 1,
              style: TextStyle(
                  color: AppColor.buttonFrameColor,
                  fontSize: 16.sp,
                  fontWeight: FontWeight.bold)),
        )
            : Container(),
        //訊息內容
        Container(
          margin: const EdgeInsets.only(top: 10, right: 10, left: 20),
          child: Column(
            children: [
              Row(
                mainAxisAlignment:
                isMe ? MainAxisAlignment.end : MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  //建立時間(自己用)
                  if (isMe)
                    Text(
                      Util().messageTime(messageData[index].createdAt!),
                      textScaleFactor: 1,
                      style:
                      TextStyle(color: AppColor.grayText, fontSize: 15.sp),
                    ),
                  //對方大頭貼
                  !isMe && lastMessageIsMe && member != null
                      ? Container(
                      margin: const EdgeInsets.only(bottom: 10, right: 10),
                      width: 8.w,
                      height: 8.w,
                      child: MugshotWidget(
                        fromMemberId: 0,
                        targetMemberId: member!.id!,
                        nickName: member!.nickname!,
                        gender: member!.gender!,
                        birthday: member!.birthday!,
                        interactive: true,
                      ))
                      : isMe
                      ? const SizedBox()
                      : Container(
                    margin:
                    const EdgeInsets.only(bottom: 10, right: 10),
                    width: 8.w,
                    height: 8.w,
                  ), //有照片
                  //訊息內容
                  Column(
                    crossAxisAlignment: isMe
                        ? CrossAxisAlignment.end
                        : CrossAxisAlignment.start,
                    children: [
                      _contentWidget(isMe, messageData[index]),
                      const SizedBox(
                        height: 5,
                      ),
                    ],
                  ),
                  //建立時間(對方用)
                  if (!isMe)
                    Text(
                      Util().messageTime(messageData[index].createdAt!),
                      textScaleFactor: 1,
                      style:
                      TextStyle(color: AppColor.grayText, fontSize: 15.sp),
                    ),
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }

  //判斷背景顏色
  Color _messageContentBackGroundColor(bool _isMe) {
    return _isMe ? AppColor.buttonFrameColor : AppColor.grayText;
  }

  //個別訊息內容
  Widget _contentWidget(bool isMe, ChatMessageModel messageData) {
    double _padding = 10;
    BoxDecoration _boxDecoration = BoxDecoration(
        color: _messageContentBackGroundColor(isMe),
        borderRadius: BorderRadius.circular(20));
    BoxConstraints _boxConstraints =
    BoxConstraints(maxWidth: MediaQuery.of(context).size.width * 0.5);
    switch (_getMessageType(messageData.type!)) {
    //普通訊息 含URL
      case MessageType.TEXT:
        return Container(
          padding: EdgeInsets.all(_padding),
          decoration: _boxDecoration,
          constraints: _boxConstraints,
          child: Linkify(
            onOpen: (link) async {
              if (await canLaunch(link.url)) {
                await launch(link.url);
              } else {
                throw 'Could not launch $link';
              }
            },
            text: messageData.content ?? "",
            style: TextStyle(
              color: isMe ? Colors.white : Colors.black,
              fontSize: 18.sp,
            ),
          ),
        );
      case MessageType.PIC:
      //List list = jsonDecode(messageData.content!);
      //if (list.isNotEmpty) {
        return Container(
          constraints: _boxConstraints,
          child: GestureDetector(
            onTap: () {
              showImage(messageData.content!, 0);
            },
            child: ClipRRect(
              borderRadius: BorderRadius.circular(10.0),
              child: CachedNetworkImage(
                  httpHeaders: {"authorization": "Bearer " + Api.accessToken},
                  imageUrl: Util().getMemberMugshotUrl(messageData.content!),
                  fit: BoxFit.cover,
                  placeholder: (context, url) => const Padding(
                    padding: EdgeInsets.all(10.0),
                    child: CircularProgressIndicator(
                      color: AppColor.buttonFrameColor,
                    ),
                  ),
                  errorWidget: (context, url, error) => const Padding(
                    padding: EdgeInsets.all(10.0),
                    child: Icon(
                      Icons.error_outline,
                      color: AppColor.buttonFrameColor,
                    ),
                  )),
            ),
          ),
        );
      case MessageType.VIDEO:
        return ClipRRect(
          borderRadius: BorderRadius.circular(10.0),
          child: Container(
              constraints: _boxConstraints,
              child: AspectRatioVideo(
                key: Key(messageData.content!),
                list: Util().getMemberMugshotUrl(messageData.content!),
              )),
        );
      case MessageType.AUDIO:
        return Container(
            padding: EdgeInsets.all(_padding),
            decoration: _boxDecoration,
            constraints: _boxConstraints,
            child: RecordPlayer(
                path: Util().getMemberMugshotUrl(messageData.content!)));
      default:
        return Text(
          "無法讀取訊息",
          style: TextStyle(
            color: isMe ? Colors.white : Colors.black,
            fontSize: 18.sp,
          ),
        );
    }
  }

  void showImage(String files, index) {
    showGeneralDialog(
        context: context,
        pageBuilder: (context, animation, secondaryAnimation) =>
            ImageShower(data: [files], index: index),
        transitionBuilder: (context, animation, secondaryAnimation, child) {
          const begin = Offset(0.0, 1.0);
          const end = Offset.zero;
          const curve = Curves.ease;
          var tween =
          Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
          return SlideTransition(
            position: animation.drive(tween),
            child: child,
          );
        });
  }

  //取得訊息類型
  MessageType _getMessageType(int _type) {
    return Util.messageType[_type]!;
  }

  //底下輸入框
  Widget _bottomBar() {
    return Container(
      height: 8.h,
      padding:
      EdgeInsets.only(right: 5.w, left: 5.w, top: 0.5.h, bottom: 0.5.h),
      color: AppColor.grayFrame,
      child: Row(
        children: [
          //普通相機
          GestureDetector(
              onTap: () => _takePhoto(),
              child: Container(
                margin: const EdgeInsets.only(top: 5),
                child: const Icon(
                  Icons.camera_alt,
                  color: AppColor.appTitleBarTextColor,
                  size: 35,
                ),
              )),
          SizedBox(
            width: 1.5.w,
          ),
          //相簿(選擇影片或相片)
          GestureDetector(
              onTap: () => _changePhoto(),
              child: const Icon(
                Icons.add_photo_alternate_rounded,
                color: AppColor.appTitleBarTextColor,
                size: 37,
              )),
          SizedBox(
            width: 1.5.w,
          ),
          //輸入框
          Expanded(
              child: TextField(
                style: const TextStyle(color: Colors.black),
                decoration: InputDecoration(
                  contentPadding: const EdgeInsets.only(left: 20, right: 20, top: 5, bottom: 5),
                  enabledBorder: OutlineInputBorder(
                      borderSide: const BorderSide(color: Colors.white),
                      borderRadius: BorderRadius.circular(30)),
                  border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(30)),
                  fillColor: AppColor.appBackgroundColor,
                  hintText: S.of(context).enter_message,
                  filled: true,
                ),
                minLines: 1,
                maxLines: 10,
                keyboardType: TextInputType.multiline,
                textInputAction: TextInputAction.newline,
                onTap: () {
                  _moveToBottom();
                },
                controller: _message,
              )),
          SizedBox(
            width: 2.w,
          ),
          //送出訊息
          GestureDetector(
              onTap: _sentMessage,
              child: Icon(
                FontAwesome.send,
                color: AppColor.buttonFrameColor,
                size: AppSize.iconSizeW - 1.w,
              )),
        ],
      ),
    );
  }

  //發送訊息
  void _sentMessage() {
    if (_message.text == "") {
      return;
    }
    VisitChatMessageDTO _chatMessageModel = VisitChatMessageDTO(
        visitRecordId: _visitRecord!.id!,
        fromMember: _memberProvider!.memberModel.name,
        fromMemberNickname: _memberProvider!.memberModel.nickname,
        content: _message.text,
        targetMember: member!.name,
        messageType: MessageType.TEXT, destination: '',);
    //log('發訊息 ${_visitRecord!.id!} ${_chatMessageModel.toJson()}');
    _stompClientProvider!.sendMessage(
        Stomp.peer2PeerVisitMessage, json.encode(_chatMessageModel.toJson()));
    _message.text = "";
    setState(() {});
    _moveToBottom(); //移至最底
  }

  //修改照片
  void _changePhoto() {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return _chooseImage();
        });
  }

  //相簿選擇框(影片或相片)
  Widget _chooseImage() {
    return Container(
      height: 27.h,
      padding: const EdgeInsets.only(top: 10),
      child: Column(
        children: [
          //影片
          GestureDetector(
            onTap: () => _selectVideo(),
            child: Container(
              width: 70.w,
              height: 9.h,
              alignment: Alignment.center,
              padding: const EdgeInsets.all(5),
              margin: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                  color: Colors.white,
                  border:
                  Border.all(color: AppColor.buttonFrameColor, width: 1),
                  borderRadius: BorderRadius.circular(30)),
              child: Text(
                S.of(context).video,
                style: TextStyle(
                    color: AppColor.buttonFrameColor, fontSize: 20.sp),
              ),
            ),
          ),
          SizedBox(
            height: 1.h,
          ),
          //相簿
          GestureDetector(
            onTap: () => _selectImage(),
            child: Container(
              width: 70.w,
              height: 9.h,
              alignment: Alignment.center,
              padding: const EdgeInsets.all(5),
              margin: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                  color: Colors.white,
                  border:
                  Border.all(color: AppColor.buttonFrameColor, width: 1),
                  borderRadius: BorderRadius.circular(30)),
              child: Text(
                S.of(context).album,
                style: TextStyle(
                    color: AppColor.buttonFrameColor, fontSize: 20.sp),
              ),
            ),
          )
        ],
      ),
    );
  }

  //打開相機
  void _takePhoto() {
    ImagePickers.openCamera(
        cameraMimeType: CameraMimeType.photo,
        cropConfig: CropConfig(enableCrop: false, width: 2, height: 3))
        .then((media) {
      if (media != null) {
        _webSocketProvider!.setMessageTypeToPIC();
        EasyLoading.show(status: S.of(context).uploading);
        //log('media: ${_getFileName(media.path!)}');
        String _path = media.path!;
        _getFileName(_path).then((_fileName) {
          _webSocketProvider!.uploadFile(_fileName, _getFile(_path));
        });
      }
    });
  }

  //打開相簿選擇相片
  void _selectImage() async {
    Navigator.of(context).pop(); //關閉選擇框
    ImagePickers.pickerPaths(
        galleryMode: GalleryMode.image,
        showGif: false,
        selectCount: 1,
        showCamera: false,
        cropConfig: CropConfig(enableCrop: false, height: 1, width: 1),
        compressSize: 500,
        uiConfig: UIConfig(uiThemeColor: AppColor.buttonFrameColor))
        .then((media) {
      if (media.isNotEmpty) {
        _webSocketProvider!.setMessageTypeToPIC();
        EasyLoading.show(status: S.of(context).uploading);
        //log('media: ${_getFileName(media.first.path!)}');
        String _path = media.first.path!;
        _getFileName(_path).then((_fileName) {
          _webSocketProvider!.uploadFile(_fileName, _getFile(_path));
        });
      }
    });
  }

  //打開相簿選擇影片
  void _selectVideo() async {
    Navigator.of(context).pop(); //關閉選擇框
    ImagePickers.pickerPaths(
        galleryMode: GalleryMode.video,
        showGif: false,
        selectCount: 1,
        showCamera: false,
        cropConfig: CropConfig(enableCrop: false, height: 1, width: 1),
        compressSize: 500,
        uiConfig: UIConfig(uiThemeColor: AppColor.buttonFrameColor))
        .then((media) async {
      if (media.isNotEmpty) {
        //log('檔案大小: ${File(media.first.path!).lengthSync()}');
        int sizeInBytes = File(media.first.path!).lengthSync(); //取得檔案大小
        double sizeInMb = sizeInBytes / (1024 * 1024);
        if (sizeInMb < 50) {
          //小於50
          //log('media: ${_getFileName(media.first.path!)}');
          _webSocketProvider!.setMessageTypeToVideo();
          EasyLoading.show(status: S.of(context).uploading);
          String _path = media.first.path!;
          _getFileName(_path).then((_fileName) {
            _webSocketProvider!.uploadFile(_fileName, _getFile(_path));
          });
        } else {
          //大於50
          EasyLoading.showToast(S.of(context).upload_file_too_large);
        }
      }
    });
  }

  Widget _titleBarBackButton(){
    return GestureDetector(
      onTap: () => delegate.popRoute(),
      child: const Icon(
        Icons.arrow_back,
        color: AppColor.appTitleBarTextColor,
        size: 30,
      ),
    );
  }

  //標題 跟 左邊右邊list 自定義icon
  Widget titleBarChatIntegrate(List<Widget> _left, String _title, List<Widget> _right, [VoidCallback? titleOnClick]){
    return Container(
      height: AppSize.titleBarH,
      width: Device.boxConstraints.maxWidth,
      decoration: const BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
              color: Colors.grey,
              offset: Offset(0.0, 0.5), //陰影y軸偏移量
              blurRadius: 1, //陰影模糊程度
              spreadRadius: 1 //陰影擴散程度
          )
        ],
      ),
      child: Stack(
        alignment: Alignment.center,
        children: [
          //左按鈕
          if(_left.isNotEmpty)
            Positioned(left: 5.w,child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: _left,
            )),
          //標題文字
          GestureDetector(
            onTap: () => titleOnClick != null ? titleOnClick() : null,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                FittedBox(
                  child: Text(
                    _title,
                    style: TextStyle(
                        color: AppColor.appTitleBarTextColor,
                        fontSize: 21.sp,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                SizedBox(width: 2.w,),
              ],
            ),
          ),
          //右按紐
          if(_right.isNotEmpty)
            Positioned(right: 5.w,child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: _right,
            )),
        ],
      ),
    );
  }

  //取得檔案名稱
  Future<String> _getFileName(String _path) async {
    String _fileName;
    if (_path.contains('.heic')) {
      debugPrint('path: $_path');
      _fileName =
      '${_memberProvider!.memberModel.name}_${_path.substring(_path.lastIndexOf("/") + 1, _path.length)}';
      _path = (await HeicToJpg.convert(_path).catchError((e) {
        debugPrint(e);
      }))!;
    } else {
      _fileName =
      '${_memberProvider!.memberModel.name}_${_path.substring(_path.lastIndexOf("/") + 1, _path.length)}';
    }
    return _fileName;
  }

  Uint8List _getFile(String _path) {
    return Uint8List.fromList(File(_path).readAsBytesSync());
  }

  Widget _titleBarReportButton(){
    return GestureDetector(
      onTap: () => _reportButton(),
      child: const Icon(
        Icons.warning_amber_outlined,
        color: AppColor.appTitleBarTextColor,
        size: 25,
      ),
    );
  }

  void _reportButton() async {
    delegate.push(name: RouteName.reportDialog, arguments: member!.id!);
  }

  void _uploadStatus(WebSocketProvider _webUploadStatus) {
    if (_webUploadStatus.uploadFileStatus != UpLoadingStatus.notUpLoading) {
      //log('上傳狀態: ${_webUploadStatus.uploadFileStatus}');
      if (_webUploadStatus.uploadFileStatus != UpLoadingStatus.upLoading) {
        //不在上傳中
        EasyLoading.dismiss(); //關閉
        if (_webUploadStatus.uploadFileStatus ==
            UpLoadingStatus.uploadCompleted) {
          //完成
          //圖片檔案內容上傳
          VisitChatMessageDTO _chatMessageModel = VisitChatMessageDTO(
              visitRecordId: _visitRecord!.id!,
              fromMember: _memberProvider!.memberModel.name,
              fromMemberNickname: _memberProvider!.memberModel.nickname,
              content: _webSocketProvider!.fileName,
              targetMember: member!.name,
              messageType: _webSocketProvider!.messageType, destination: '');
          _stompClientProvider!.sendMessage(
              Stomp.peer2PeerVisitMessage, json.encode(_chatMessageModel.toJson()));
          _webSocketProvider!.setFileUploadState();
        } else {
          //失敗
          EasyLoading.showError(S.of(context).fail);
          _webSocketProvider!.setFileUploadState();
        }
      }
    }
  }
}