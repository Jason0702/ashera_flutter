import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../app.dart';
import '../../generated/l10n.dart';
import '../../models/visit_record_model.dart';
import '../../provider/util_api_provider.dart';
import '../../provider/visit_game_provider.dart';
import '../../utils/api.dart';
import '../../utils/app_color.dart';
import '../../utils/app_image.dart';
import '../../utils/util.dart';
import '../../widget/titlebar_widget.dart';

class VisitStarRecordIndexPage extends StatefulWidget{
  const VisitStarRecordIndexPage({Key? key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _VisitStarRecordIndexPageState();
}

class _VisitStarRecordIndexPageState extends State<VisitStarRecordIndexPage>{

  VisitStarRecordModel? model;

  @override
  Widget build(BuildContext context) {
    model ??= ModalRoute.of(context)!.settings.arguments as VisitStarRecordModel;
    return Scaffold(
      body: Stack(
        children: [
          Column(
            children: [
              //標題
              titleBarIntegrate([], S.of(context).star_rating, [closeWidget()]),
              //內容
              Expanded(child: _body()),
            ],
          ),
        ],
      ),
    );
  }

  //內容
  Widget _body() {
    return Consumer2<UtilApiProvider, VisitGameProvider>(builder: (context, order, visit, _){
        return Container(
          margin: EdgeInsets.only(top: 1.h),
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
          decoration: const BoxDecoration(
              color: Colors.white,
              border: Border(
                  bottom: BorderSide(color: AppColor.grayFrame, width: 1))),
          child: Column(
            children: [
              Row(
                children: [
                  //頭像
                  FutureBuilder(
                      future: visit.getMugshotMemberById(id: model!.memberId!, interactive: true),
                      builder: (context, AsyncSnapshot<String> snapshot){
                        if(snapshot.connectionState == ConnectionState.done){
                          return Container(
                              width: 13.w,
                              height: 9.h,
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  image: DecorationImage(
                                      fit: BoxFit.cover,
                                      image: CachedNetworkImageProvider(
                                        Util().getMemberMugshotUrl(snapshot.data!),
                                        headers: {"authorization": "Bearer " + Api.accessToken},
                                      ),
                                      onError: (error, stackTrace) {
                                        debugPrint('Error: ${error.toString()}');
                                      })));
                        }
                        return Container(
                          width: 13.w,
                          height: 9.h,
                          decoration: BoxDecoration(
                              shape: BoxShape.circle, gradient: AppColor.appMainColor),
                          child: CircleAvatar(
                              backgroundColor: Colors.transparent,
                              child: Image.asset(AppImage.iconCat)
                          ),
                        ); /*const CircularProgressIndicator();*/
                      }),
                  //名稱&星星
                  Expanded(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        //名稱
                        Container(
                          child: FittedBox(
                            child: Text(
                              model!.memberNickname!,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18.sp,
                                  color: Colors.black),
                            ),
                          ),
                          margin: const EdgeInsets.only(left: 10),
                        ),
                        //星星
                        Row(
                          children: [
                            Container(
                              margin: const EdgeInsets.only(left: 10, right: 5),
                              width: 5.w,
                              child: Image.asset(AppImage.iconStar),
                            ),
                            Text(
                              model!.memberScore!.toStringAsFixed(1),
                              style: const TextStyle(color: Colors.black),
                            ),
                          ],
                        )
                      ],
                    ),
                  ),
                ],
              ),
              Container(
                height: 1,
                color: Colors.grey,
              ),
              //內容
              Expanded(
                  flex: 2,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      //內容title
                      Text(
                        '${S.of(context).content}:',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 18.sp,
                            color: Colors.black),
                      ),
                      //內容
                      Text(
                        model!.memberComment ?? '',
                        overflow: TextOverflow.ellipsis,
                        maxLines: 20,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 18.sp,
                            color: Colors.black),
                      ),
                    ],
                  ))
            ],
          ),
        );
      },);
  }

  //X
  Widget closeWidget() {
    return GestureDetector(
      onTap: () => _closeOnTap(),
      child: const Icon(
        Icons.clear,
        color: AppColor.appTitleBarTextColor,
        size: 30,
      ),
    );
  }

  //X點擊
  void _closeOnTap() {
    delegate.popRoute();
  }
}