import 'dart:developer';
import 'dart:typed_data';

import 'package:ashera_flutter/app.dart';
import 'package:ashera_flutter/dialog/visit_dialog.dart';
import 'package:ashera_flutter/enum/app_enum.dart';
import 'package:ashera_flutter/provider/member_provider.dart';
import 'package:ashera_flutter/provider/stomp_client_provider.dart';
import 'package:ashera_flutter/provider/unlock_mugshot_provider.dart';
import 'package:ashera_flutter/provider/util_api_provider.dart';
import 'package:ashera_flutter/utils/api.dart';
import 'package:ashera_flutter/utils/app_image.dart';
import 'package:ashera_flutter/utils/no_shadow_scroll_behavior.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:webview_flutter/platform_interface.dart';

import '../../dialog/game_point_insufficient_dialog.dart';
import '../../dialog/visit_ask_cancel_dialog.dart';
import '../../dialog/visit_search_setting_dialog.dart';
import '../../generated/l10n.dart';
import '../../models/visit_record_dto.dart';
import '../../models/visit_record_model.dart';
import '../../provider/position_provider.dart';
import '../../provider/visit_game_provider.dart';
import '../../routers/route_name.dart';
import '../../utils/app_color.dart';
import '../../utils/util.dart';
import '../../widget/avatar_image.dart';
import '../../widget/gender_icon.dart';
import '../../widget/mugshot_widget.dart';
import '../../widget/titlebar_widget.dart';

class VisitPage extends StatefulWidget {
  const VisitPage({Key? key}) : super(key: key);

  @override
  _VisitPageState createState() => _VisitPageState();
}

class _VisitPageState extends State<VisitPage> {
  VisitGameProvider? _visitGameProvider;
  PositionProvider? _positionProvider;
  UtilApiProvider? _utilApiProvider;
  StompClientProvider? _stompClientProvider;
  MemberProvider? _memberProvider;
  UnlockMugshotProvider? _unlockMugshotProvider;

  //list:false map:true
  bool listOrMap = false;

  //大頭針
  final Set<Marker> _markers = {};
  //隱身
  bool _stealth = false;


  _onLayoutDone(_) async {
    if (_visitGameProvider!.userStatus == VisitMode.visitModeVisit) {
      await _utilApiProvider!.getNearMeDistance(memberId: _memberProvider!.memberModel.id!, distance: _visitGameProvider!.distance);
      await _utilApiProvider!.getFindPairingRecordByBeingVisit(
          gender: _visitGameProvider!.genderStatus.index,
          whoPay: null,
          minAge: _visitGameProvider!.minAge,
          maxAge: _visitGameProvider!.maxAge,
          name: null);
      _addMark(); //加入大頭針
    } else {
      _addMark(); //加入大頭針
    }
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  Widget build(BuildContext context) {
    _stompClientProvider = Provider.of<StompClientProvider>(context, listen: false);
    _visitGameProvider = Provider.of<VisitGameProvider>(context, listen: false);
    _positionProvider = Provider.of<PositionProvider>(context, listen: false);
    _utilApiProvider = Provider.of<UtilApiProvider>(context, listen: false);
    _memberProvider = Provider.of<MemberProvider>(context, listen: false);
    _unlockMugshotProvider = Provider.of(context, listen: false);
    return Scaffold(
      body: Stack(
        children: [
          Column(
            children: [
              //標題
              titleBar(),
              //找尋按鈕
              selectSeekGenderButton(),
              Expanded(child: bodyWidget()),
              //自身資訊
              selfIngo(),
            ],
          ),
        ],
      ),
    );
  }

  //標題
  Widget titleBar() {
    return Consumer<VisitGameProvider>(builder: (context, visit, _){
      if (visit.userStatus == VisitMode.visitModeVisit) {
        return titleBarIntegrate([back()], S.of(context).visit, [ _titleBarReportButton(), record()]);
      } else {
        return titleBarIntegrate([back()], S.of(context).visited, [_titleBarReportButton(), cancel(),record()]);
      }
    },);
  }
  //取消被探班
  Widget cancel(){
    return GestureDetector(
      onTap: () => cancelOnTap(),
      child: Container(
        margin: EdgeInsets.only(right: 5.w),
        child: FittedBox(
          child: SizedBox(
              height: 8.w,
              width: 8.w,
              child: Image.asset(AppImage.iconExit)),
        ),
      ),
    );
  }

  cancelOnTap() async {
    bool? _result = await showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2){
          return VisitAskCancelDialog(
            key: UniqueKey(),
            title: S.of(context).cancel_the_visit,
            content:
            '${S.of(context).are_you_sure}${S.of(context).cancel_the_visit}?',
            confirmButtonText: '${S.of(context).sure}${S.of(context).cancel}',
            haveClose: true,
          );
        });
    if(_result != null){
      if(_result){
        _stompClientProvider!.sendCancelVisit(id: _utilApiProvider!.visitRecordModel.id!);
      }
    }
  }

  //紀錄
  Widget record() {
    return GestureDetector(
      onTap: () => bookOnTap(),
      child: FittedBox(
        child: SizedBox(
            height: 8.w,
            width: 8.w,
            child: Image.asset(AppImage.iconVisitRecord)),
      ),
    );
  }

  //返回
  Widget back() {
    return GestureDetector(
      onTap: () => {delegate.popRoute()},
      child: const Icon(
        Icons.arrow_back,
        color: AppColor.appTitleBarTextColor,
        size: 30,
      ),
    );
  }

  //找尋按鈕
  Widget selectSeekGenderButton() {
    return Consumer<VisitGameProvider>(builder: (context, p, _) {
      if (p.userStatus == VisitMode.visitModeVisit) {
        return Container(
          margin: EdgeInsets.only(top: 3.h),
          padding: EdgeInsets.symmetric(horizontal: 5.w, vertical: 15),
          width: Device.width,
          color: Colors.white,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                S.of(context).seek,
                style:
                    TextStyle(fontSize: 18.sp, fontWeight: FontWeight.bold),
              ),
              Expanded(
                child: Container(
                  margin: const EdgeInsets.symmetric(horizontal: 16),
                  height: 7.h,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      border: Border.all(color: AppColor.grayLine, width: 1),
                      borderRadius: BorderRadius.circular(30)),
                  child: Row(
                    children: [
                      //探班
                      Expanded(
                          child: GestureDetector(
                        onTap: () => changeSeekGenderStatus(GenderStatus.girl),
                        child: Container(
                          alignment: Alignment.center,
                          margin: p.seekGenderStatusMargin(GenderStatus.girl),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30),
                              color: p.seekGenderStatusButtonColor(
                                  GenderStatus.girl)),
                          child: Text(
                            S.of(context).female,
                            style: TextStyle(
                                fontSize: 19.sp,
                                color: p.seekGenderStatusTextColor(
                                    GenderStatus.girl)),
                          ),
                        ),
                      )),
                      SizedBox(
                        width: 1.w,
                      ),
                      //被探班
                      Expanded(
                          child: GestureDetector(
                        onTap: () => changeSeekGenderStatus(GenderStatus.boy),
                        child: Container(
                          alignment: Alignment.center,
                          margin: p.seekGenderStatusMargin(GenderStatus.boy),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30),
                              color: p.seekGenderStatusButtonColor(
                                  GenderStatus.boy)),
                          child: Text(
                            S.of(context).male,
                            style: TextStyle(
                                fontSize: 19.sp,
                                color: p.seekGenderStatusTextColor(
                                    GenderStatus.boy)),
                          ),
                        ),
                      )),
                    ],
                  ),
                ),
              ),
              GestureDetector(
                onTap: () => _filterOnTap(),
                child: Image(
                  width: 30,
                  image: AssetImage(
                      AppImage.iconFilter
                  ),
                ),
              )
            ],
          ),
        );
      } else {
        return const SizedBox();
      }
    });
  }

  //搜尋按鈕
  void _filterOnTap() async {
    bool? _result = await showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2){
          return VisitSearchSettingDialog(key: UniqueKey(),);
        });
    if(_result != null){
      if(_result){
        EasyLoading.show(status: '${S.of(context).search}...');
        /*log('返回');
        log('誰付款: ${_visitGameProvider!.paidStatus}');
        log('年齡範圍: ${_visitGameProvider!.minAge} ~ ${_visitGameProvider!.maxAge}');
        log('距離: ${_visitGameProvider!.distance}');*/
        await _utilApiProvider!.getNearMeDistance(memberId: _memberProvider!.memberModel.id!, distance: _visitGameProvider!.distance);
        await _utilApiProvider!.getFindPairingRecordByBeingVisit(
          gender: _visitGameProvider!.genderStatus.index,
          whoPay: _getPaidStatus(_visitGameProvider!.paidStatus),
          minAge: _visitGameProvider!.minAge,
          maxAge: _visitGameProvider!.maxAge,
          name: null
        );
        EasyLoading.dismiss();
      }
    }
  }

  int? _getPaidStatus(WhoPay value){
    if(value == WhoPay.whoPaySelf || value == WhoPay.whoPayOther){
      return value.index;
    } else {
      return null;
    }
  }

  //找尋按鈕事件
  void changeSeekGenderStatus(GenderStatus input) {
    if (_visitGameProvider!.genderStatus == input) {
      return;
    }
    _visitGameProvider?.changeSeekGenderStatus(input);
    _utilApiProvider!
        .getFindPairingRecordByBeingVisit(
        gender: input.index,
        whoPay: _getPaidStatus(_visitGameProvider!.paidStatus),
        minAge: _visitGameProvider!.minAge,
        maxAge: _visitGameProvider!.maxAge,
        name: null)
        .then((value) {
      _addMark(); //加入大頭針
    });
  }

  //中間部分
  Widget bodyWidget() {
    return listOrMap ? _googleMap() : _listWidget();
  }

  //列表
  Widget _listWidget() {
    return Consumer<UtilApiProvider>(builder: (context, record, _) {
      //log('呼叫了兩次?');
      return record.visitProcessingRecordList.isEmpty
          ? SizedBox(
              width: Device.width,
              height: Device.height,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image(
                    width: 20.w,
                    height: 20.h,
                    image: AssetImage(AppImage.imgWaiting),
                  ),
                  Text(
                    S.of(context).waiting,
                    style: TextStyle(color: Colors.grey[300]!, fontWeight: FontWeight.bold, fontSize: 20.sp),
                  )
                ],
              ),
            )
          : ScrollConfiguration(
              behavior: NoShadowScrollBehavior(),
              child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: record.visitProcessingRecordList.length,
                  itemBuilder: (context, index) {
                    if (_visitGameProvider!.userStatus ==
                        VisitMode.visitModeVisit) {
                      return fromMemberCard(model: record.visitProcessingRecordList[index]);
                    } else {
                      return targetMemberCard(model: record.visitProcessingRecordList[index]);
                    }
                  }),
            );
    });
  }

  //列表卡片 被探班
  Widget targetMemberCard({required VisitRecordModel model}) {
    return GestureDetector(
      onTap: () => _memberCardOnTap(model.id!),
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        decoration: const BoxDecoration(
            color: Colors.white,
            border: Border(
                bottom: BorderSide(color: AppColor.grayFrame, width: 1))),
        child: Row(
          children: [
            //頭像
            MugshotWidget(
              fromMemberId: _memberProvider!.memberModel.id!,
              nickName: model.fromMemberNickname!,
              gender: model.fromMemberGender!,
              birthday: model.fromMemberBirthday!,
              targetMemberId: model.fromMemberId!,
              interactive: true,
            ),
            //名稱&星星&性別年齡&距離
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      //名稱
                      Container(
                        child: FittedBox(
                          child: Text(
                            model.fromMemberNickname!,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 18.sp,
                                color: Colors.black),
                          ),
                        ),
                        margin: const EdgeInsets.only(left: 10),
                      ),
                      //星星
                      GestureDetector(
                        onTap: (){},
                        child: Row(
                          children: [
                            Container(
                              margin: const EdgeInsets.only(left: 10, right: 5),
                              width: 5.w,
                              child: Image.asset(AppImage.iconStar),
                            ),
                            FutureBuilder(
                              future: _utilApiProvider!.getListOtherStar(id: model.fromMemberId!),
                              builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                                if(snapshot.connectionState == ConnectionState.done){
                                  return Text(
                                    snapshot.data!.toStringAsFixed(1),
                                    style: const TextStyle(color: Colors.black),
                                  );
                                }
                                return Container();
                              },
                            ),
                          ],
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 0.5.h,
                  ),
                  //性別年齡
                  Row(
                    children: [
                      getGender(model.fromMemberGender!),
                      //年齡
                      Container(
                        margin: const EdgeInsets.only(left: 5),
                        child: Text(
                          Util().getAge(model.fromMemberBirthday!),
                          style: const TextStyle(color: Colors.black),
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
            //距離
            Row(
              children: [
                SizedBox(
                  width: 8.w,
                  height: 8.w,
                  child: Image.asset(AppImage.iconPosition),
                ),
                SizedBox(
                  width: 2.w,
                ),
                Consumer<PositionProvider>(builder: (context, position, _) {
                  LatLng _latLng = LatLng(
                      model.endLatitude!,
                      model.endLongitude!);
                  return Text(
                    "${S.of(context).distance}${_getDistanceBetween(position.currentPosition, _latLng)}",
                    style: TextStyle(color: AppColor.grayText, fontSize: 16.sp),
                  );
                })
              ],
            )
          ],
        ),
      ),
    );
  }

  //列表卡片 探班
  Widget fromMemberCard({required VisitRecordModel model}) {
    return GestureDetector(
      onTap: () => _memberCardOnTap(
          model.id!),
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        decoration: const BoxDecoration(
            color: Colors.white,
            border: Border(
                bottom: BorderSide(color: AppColor.grayFrame, width: 1))),
        child: Row(
          children: [
            //頭像
            MugshotWidget(
                fromMemberId: _memberProvider!.memberModel.id!,
                nickName: model.targetMemberNickname!,
                gender: model.targetMemberGender!,
                birthday: model.targetMemberBirthday!,
                targetMemberId: model.targetMemberId!,
                interactive: true
            ),
            //名稱&星星&性別年齡&距離&支付方式
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    children: [
                      //名稱
                      Container(
                        child: FittedBox(
                          child: Text(
                            model.targetMemberNickname!,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 18.sp,
                                color: Colors.black),
                          ),
                        ),
                        margin: const EdgeInsets.only(left: 10),
                      ),
                      //星星
                      GestureDetector(
                        onTap: () => _starOnTap(id: model.targetMemberId!),
                        child: Row(
                          children: [
                            Container(
                              margin: const EdgeInsets.only(left: 10, right: 5),
                              width: 5.w,
                              child: Image.asset(AppImage.iconStar),
                            ),
                            FutureBuilder(
                              future: _utilApiProvider!.getListOtherStar(id: model.targetMemberId!),
                              builder: (BuildContext context, AsyncSnapshot<dynamic> snapshot) {
                                if(snapshot.connectionState == ConnectionState.done){
                                  return Text(
                                    snapshot.data!.toStringAsFixed(1),
                                    style: const TextStyle(color: Colors.black),
                                  );
                                }
                                return Container();
                              },
                            ),
                          ],
                        ),
                      )
                    ],
                  ),

                  SizedBox(
                    height: 0.5.h,
                  ),
                  //性別年齡
                  Row(
                    children: [
                      getGender(model.targetMemberGender!),
                      //年齡
                      Container(
                        margin: const EdgeInsets.only(left: 5),
                        child: Text(
                          Util().getAge(model.targetMemberBirthday!),
                          style: const TextStyle(color: Colors.black),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(left: 5),
                        padding: const EdgeInsets.all(5),
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(50),
                            border: Border.all(color: Colors.red, width: 2)
                        ),
                        child: Text(model.whoPay! == 0
                            ? S.of(context).self_paid
                            : S.of(context).opposite_paid,
                          style: const TextStyle(color: Colors.red, fontSize: 12, height: 1.1, fontWeight: FontWeight.bold),
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
            Expanded(child: Column(
              children: [
                //距離
                Row(
                  children: [
                    SizedBox(
                      width: 8.w,
                      height: 8.w,
                      child: Image.asset(AppImage.iconPosition),
                    ),
                    SizedBox(
                      width: 2.w,
                    ),
                    Consumer<PositionProvider>(builder: (context, position, _) {
                      LatLng _latLng = LatLng(
                          model.endLatitude!,
                          model.endLongitude!);
                      return Text(
                        "${S.of(context).distance}${_getDistanceBetween(position.currentPosition, _latLng)}",
                        style: TextStyle(color: AppColor.grayText, fontSize: 16.sp),
                      );
                    })
                  ],
                ),
                //需求
                Text(
                  '${S.of(context).needed}: ${model.requirement!}',
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 16),
                )
              ],
            ))
          ],
        ),
      ),
    );
  }

  ///星星點擊
  /// * [id] 用戶ID
  void _starOnTap({required int id}){
    //log('點的到星星? $id');
    delegate.push(name: RouteName.visitStarRecordPage, arguments: id);
  }


  //距離計算
  String _getDistanceBetween(LatLng _myLatLng, LatLng _visitLatLng) {
    String _value = '';
    double _distance = Geolocator.distanceBetween(_myLatLng.latitude,
        _myLatLng.longitude, _visitLatLng.latitude, _visitLatLng.longitude);
    if (_distance > 1000) {
      _value = '${(_distance / 1000).toStringAsFixed(2)}${S.of(context).km}';
    } else {
      _value = '${_distance.toStringAsFixed(2)}${S.of(context).m}';
    }
    return _value;
  }

  //查看探班資訊
  void _memberCardOnTap(int id) {
    //如果是探班
    if (_visitGameProvider!.userStatus == VisitMode.visitModeVisit) {
      _utilApiProvider!.getOtherStar(_utilApiProvider!.visitProcessingRecordList
          .where((element) => element.id == id)
          .last
          .targetMemberId!);
      _utilApiProvider!.setVisitRecordModel(_utilApiProvider!
          .visitProcessingRecordList
          .where((element) => element.id == id)
          .last);
      delegate.push(
          name: "/VisitProcessPage",
          arguments: _utilApiProvider!.visitProcessingRecordList
              .where((element) => element.id == id)
              .last);
    } else {
      //不是探班
      _showDialog(id);
    }
  }

  //地圖內容
  Widget _googleMap() {
    return GoogleMap(
      markers: _markers,
      mapType: MapType.normal,
      zoomControlsEnabled: false,
      myLocationButtonEnabled: true,
      myLocationEnabled: !_stealth,
      mapToolbarEnabled: false,
      initialCameraPosition: CameraPosition(
          target: _positionProvider!.initialPosition, zoom: 16.0),
    );
  }

  //加入大頭針
  Future<void> _addMark() async {
    _markers.clear();
    for (var element in _utilApiProvider!.visitProcessingRecordList) {
      String makerId = '${element.id}';
      _markers.add(Marker(
        markerId: MarkerId(makerId),
        position: LatLng(element.endLatitude!, element.endLongitude!),
        icon: await _getMarkerIcon(
            id: element.targetMemberId!, gender: element.targetMemberGender!),
        onTap: () => _memberCardOnTap(element.id!),
      ));
    }
    setState(() {});
  }

  //自己的資訊
  Widget selfIngo() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      color: Colors.white,
      child: Consumer<MemberProvider>(
        builder: (context, member, _) {
          return Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              //頭像
              member.memberModel.interactivePic == null
                  ? noAvatarImage()
                  //沒照片
                  : Container(
                      width: 13.w,
                      height: 9.h,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                            fit: BoxFit.cover,
                            image: CachedNetworkImageProvider(Util()
                                .getMemberMugshotUrl(
                                    member.memberModel.interactivePic!),
                              headers: {"authorization": "Bearer " + Api.accessToken},)),
                      ),
                    ),
              //名稱&星星&探班被探班標籤
              Expanded(child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  //名稱
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      //名稱
                      Container(
                        child: FittedBox(
                          child: Text(
                            member.memberModel.nickname!,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 18.sp,
                                color: Colors.black),
                          ),
                        ),
                        margin: const EdgeInsets.only(left: 10),
                      ),
                      const SizedBox(width: 10,),
                      //探班被探班標籤
                      Expanded(child: _userStatusLabel())
                    ],
                  ),
                  SizedBox(
                    height: 0.5.h,
                  ),
                  //星星
                  GestureDetector(
                    onTap: () => _starOnTap(id: _memberProvider!.memberModel.id!),
                    child: Row(
                      children: [
                        //Icon
                        Container(
                          margin: const EdgeInsets.only(left: 10, right: 5),
                          width: 5.w,
                          child: Image.asset(AppImage.iconStar),
                        ),
                        //分數
                        Consumer<UtilApiProvider>(
                          builder: (context, star, _) {
                            return Text(
                              star.memberStar.toStringAsFixed(1),
                              style: const TextStyle(color: Colors.black),
                            );
                          },
                        ),
                        const SizedBox(width: 20),
                        //發佈需求
                        Expanded(child: _releaseRequirements())
                      ],
                    ),
                  )

                ],
              ),),
              //隱身
              Offstage(
                offstage: !listOrMap,
                child: Offstage(
                  offstage: _memberProvider!.memberModel.name != '0900493972',
                  child: Column(
                    children: [
                      const FittedBox(
                        fit: BoxFit.scaleDown,
                        child: Text('隱身', style: TextStyle(color: Colors.black, fontSize: 16, height: 1.1),),
                      ),
                      _flutterSwitch()
                    ],
                  ),
                ),
              ),
              //切換按鈕
              switchListOrMap(),
            ],
          );
        },
      ),
    );
  }

  //探班被探班標籤
  Widget _userStatusLabel() {
    return Consumer<VisitGameProvider>(builder: (context, p, _) {
      return Stack(
        children: [
          Container(
            padding: const EdgeInsets.only(left: 10, right: 10, top: 3,bottom: 3),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(50),
              border: Border.all(color: Colors.red, width: 2),
            ),
            child: Text(
              p.userStatus == VisitMode.visitModeVisit
                  ? S.of(context).visit
                  : S.of(context).visited,
              style: const TextStyle(color: Colors.red, fontSize: 12, fontWeight: FontWeight.bold, height: 1.1),
            ),
          )
        ],
      );
    });
  }
  //發佈需求
  Widget _releaseRequirements(){
    return Consumer<VisitGameProvider>(builder: (context, p, _) {
      return Stack(
        children: [
          Offstage(
            offstage: p.userStatus == VisitMode.visitModeBeingVisit,
            child: GestureDetector(
              onTap: () => _releaseRequirementsOnTap(),
              child: Container(
                padding: const EdgeInsets.only(left: 10, right: 10, top: 5,bottom: 5),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(50),
                    gradient: AppColor.appMainColor
                ),
                child: Text(S.of(context).release_requirements,
                  style: const TextStyle(color: Colors.white, fontSize: 12, fontWeight: FontWeight.bold, height: 1.1),
                ),
              ),
            ),
          )
        ],
      );
    });
  }
  //發佈需求點擊
  void _releaseRequirementsOnTap(){
    delegate.push(name: RouteName.visitReleaseRequirementsPage);
  }

  //切換地圖列表
  Widget switchListOrMap() {
    return GestureDetector(
      onTap: () => switchListOrMapOnTap(),
      child: Row(
        children: [
          Container(
            margin: const EdgeInsets.only(right: 5),
            width: listOrMap ? 8.w : 10.w,
            child: Image.asset(
              listOrMap ? AppImage.iconList : AppImage.iconMap,
            ),
          ),
          Text(
            listOrMap ? S.of(context).list : S.of(context).map,
            style: TextStyle(
                fontSize: 18.sp,
                fontWeight: FontWeight.bold,
                color: Colors.black),
          )
        ],
      ),
    );
  }

  //切換地圖列表事件
  void switchListOrMapOnTap() {
    listOrMap = !listOrMap;
    setState(() {});
  }

  //紀錄點擊事件
  void bookOnTap() {
    //_utilApiProvider!.getAllVisitRecord(_memberProvider!.memberModel.id!);
    delegate.push(name: RouteName.visitRecordPage);
  }

  //是否同意對方探班
  void _showDialog(int _id) async {
    bool? result = await showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return VisitDialog(
            key: UniqueKey(),
            title: S.of(context).visiting_class_notice,
            content: S.of(context).received_an_invitation_to_visit,
            confirmButtonText: S.of(context).agree,
            cancelButtonText: S.of(context).disagree,
          );
        },
        barrierColor: Colors.black54,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 100),
        transitionBuilder: (context, anim1, anim2, child) {
          return child;
        });
    if (result != null) {
      ApproveVisitRecordDTO _approveVisitRecord =
          ApproveVisitRecordDTO(id: _id, approve: result);
      _stompClientProvider!.sendApprove(approveVisitRecord: _approveVisitRecord);
    }
  }


  //大頭針Icon
  Future<BitmapDescriptor> _getMarkerIcon(
      {required int id, required int gender}) async {
    Uint8List? markerIcon;
    if (_isFriend(id: id)) {
      //如果是朋友了
      String _avatarUrl = await _visitGameProvider!.getMugshotMemberById(id: id, interactive: true);
      if(_avatarUrl.isNotEmpty){
        markerIcon = await Util().makeReceiptImage(avatar:
        Util().getMemberMugshotUrl(_avatarUrl),
            marker: AppImage.imgUser);
      }else{
        markerIcon = await Util().makeReceiptImage(avatar:
        gender == 0 ? AppImage.iconDog : AppImage.iconCat,
            marker: AppImage.imgUser);
      }
    } else if (_isUnlock(id: id)) {
      //如果是解鎖了
      String _avatarUrl = await _visitGameProvider!.getMugshotMemberById(id: id, interactive: true);
      if(_avatarUrl.isNotEmpty){
        markerIcon = await Util().makeReceiptImage(avatar:
        Util().getMemberMugshotUrl(_avatarUrl),
            marker: AppImage.imgUser);
      } else {
        markerIcon = await Util().makeReceiptImage(avatar:
        gender == 0 ? AppImage.iconDog : AppImage.iconCat,
            marker: AppImage.imgUser);
      }
    } else {
      //都不是
      markerIcon = await Util().makeReceiptImage(avatar:
          gender == 0 ? AppImage.iconDog : AppImage.iconCat,
          marker: AppImage.imgUser);
    }
    return BitmapDescriptor.fromBytes(markerIcon);
  }

  //判斷是否是朋友
  bool _isFriend({required int id}) {
    return _stompClientProvider!.friendList
        .where((element) => element.id == id)
        .isNotEmpty;
  }

  //判斷此人是否有解鎖過
  bool _isUnlock({required int id}) {
    return _unlockMugshotProvider!.memberMugshotList
        .where((element) => element.id == id)
        .isNotEmpty;
  }

  ///點數不足統一方法
  void _pointInsufficient() async {
    bool? result = await _showPointInsufficient();
    if (result != null) {
      if (result) {
        //跳轉加值
        delegate.push(name: RouteName.myPointsPage);
      }
    }
  }

  ///點數不足
  Future<bool?> _showPointInsufficient() async {
    return await showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return GamePointInsufficientDialog(
              key: UniqueKey(),
              title: S.of(context).message,
              content: S.of(context).please_add_value,
              confirmButtonText: S.of(context).go_to_premium,
              cancelButtonText: S.of(context).cancel);
        },
        barrierColor: Colors.black54,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 100),
        transitionBuilder: (context, anim1, anim2, child) {
          return child;
        });
  }

  //隱身
  Widget _flutterSwitch(){
    return FlutterSwitch(
        width: 15.w,
        height: 4.h,
        value: _stealth,
        onToggle: (val) => _onToggle(val));
  }

  void _onToggle(bool _value){
    if(_stealth != _value){
      _stealth = _value;
      setState(() {});
    }
  }

  Widget _titleBarReportButton(){
    return GestureDetector(
      onTap: () => _reportButton(),
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 5),
        child: const Icon(
          Icons.warning_amber_outlined,
          color: AppColor.appTitleBarTextColor,
          size: 25,
        ),
      ),
    );
  }

  void _reportButton() async {
    delegate.push(name: RouteName.reportDialog, arguments: _memberProvider!.memberModel.id!);
  }
}
