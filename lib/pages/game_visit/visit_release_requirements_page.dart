import 'package:ashera_flutter/utils/app_color.dart';
import 'package:ashera_flutter/utils/no_shadow_scroll_behavior.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../app.dart';
import '../../enum/app_enum.dart';
import '../../generated/l10n.dart';
import '../../provider/member_provider.dart';
import '../../provider/position_provider.dart';
import '../../provider/util_api_provider.dart';
import '../../provider/visit_game_provider.dart';
import '../../widget/button_widget.dart';
import '../../widget/titlebar_widget.dart';

class VisitReleaseRequirementsPage extends StatefulWidget {
  const VisitReleaseRequirementsPage({Key? key}) : super(key: key);

  @override
  State createState() => _VisitReleaseRequirementsPageState();
}

class _VisitReleaseRequirementsPageState
    extends State<VisitReleaseRequirementsPage> {
  VisitGameProvider? _visitGameProvider;
  //API
  UtilApiProvider? _utilApiProvider;
  //self
  MemberProvider? _memberProvider;
  //位置
  PositionProvider? _positionProvider;

  //地址
  final TextEditingController _address = TextEditingController();

  //需求輸入框
  final TextEditingController _input = TextEditingController();

  //至少輸入幾個字
  int atLeastNumber = 8;
  int atMaxNumber = 30;

  _onLayoutDone(_) {
    _address.text = _positionProvider!.address;
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  Widget build(BuildContext context) {
    _visitGameProvider = Provider.of<VisitGameProvider>(context);
    _utilApiProvider = Provider.of<UtilApiProvider>(context);
    _memberProvider = Provider.of<MemberProvider>(context);
    _positionProvider = Provider.of<PositionProvider>(context);
    return Scaffold(
      backgroundColor: AppColor.appBackgroundColor,
      body: ScrollConfiguration(
        behavior: NoShadowScrollBehavior(),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              //標題
              titleBar(S.of(context).visit),
              //需求輸入框
              _neededInput(),
              //支付方式
              _paidModeSwitchButton(),
              SizedBox(height: 9.h,),
              //送出
              _sendButton()
            ],
          ),
        ),
      ),
    );
  }

  //需求輸入框
  Widget _neededInput() {
    return Container(
      margin: EdgeInsets.only(top: 3.h),
      padding: EdgeInsets.only(top: 2.h),
      decoration: const BoxDecoration(color: Colors.white),
      child: Column(
        children: [
          //您可在此發佈需求
          Text(
            S.of(context).you_can_post_your_requirements_here,
            style: TextStyle(color: Colors.grey[400]!, fontSize: 20),
          ),
          //需求
          Container(
            padding: EdgeInsets.symmetric(horizontal: 5.w),
            width: Device.width,
            color: Colors.white,
            child: SizedBox(
              height: 30.h,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  //需求
                  Container(
                      margin: EdgeInsets.symmetric(vertical: 1.h),
                      child: Text(
                        S.of(context).needed,
                        style: TextStyle(
                            fontSize: 19.sp, fontWeight: FontWeight.bold),
                      )),
                  //需求輸入框
                  TextField(
                    style: const TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                      contentPadding: const EdgeInsets.all(5),
                      enabledBorder: OutlineInputBorder(
                          borderSide:
                              const BorderSide(color: AppColor.grayLine),
                          borderRadius: BorderRadius.circular(5)),
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(5)),
                      fillColor: Colors.white,
                      hintText:
                          "${S.of(context).at_least} $atLeastNumber ${S.of(context).how_many_word}",
                      filled: true,
                    ),
                    maxLines: 8,
                    keyboardType: TextInputType.multiline,
                    textInputAction: TextInputAction.newline,
                    controller: _input,
                  ),
                ],
              ),
            ),
          ),
          //地址
          Container(
            margin: EdgeInsets.only(bottom: 3.h),
            padding: EdgeInsets.symmetric(horizontal: 5.w),
            child: TextField(
              style: const TextStyle(color: Colors.black),
              decoration: InputDecoration(
                prefixText: '${S.of(context).address}  ',
                prefixStyle: TextStyle(color: Colors.grey[400]),
                contentPadding:
                    const EdgeInsets.only(left: 15, top: 5, bottom: 5),
                enabledBorder: OutlineInputBorder(
                    borderSide: const BorderSide(color: AppColor.grayLine),
                    borderRadius: BorderRadius.circular(5)),
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(5)),
                fillColor: Colors.white,
                filled: true,
              ),
              maxLines: 1,
              keyboardType: TextInputType.multiline,
              textInputAction: TextInputAction.done,
              controller: _address,
            ),
          ),
        ],
      ),
    );
  }

  //支付方式
  Widget _paidModeSwitchButton() {
    return Consumer<VisitGameProvider>(builder: (context, p, _) {
      return Container(
        margin: EdgeInsets.only(top: 3.h),
        padding: EdgeInsets.symmetric(horizontal: 5.w),
        width: Device.width,
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
                margin: EdgeInsets.symmetric(vertical: 2.h),
                child: Text(
                  S.of(context).paid_mode,
                  style:
                      TextStyle(fontSize: 18.sp, fontWeight: FontWeight.bold),
                )),
            Container(
              margin: EdgeInsets.only(bottom: 2.h),
              height: 8.h,
              decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: AppColor.grayLine, width: 1),
                  borderRadius: BorderRadius.circular(50)),
              child: Row(
                children: [
                  //探班
                  Expanded(
                      child: GestureDetector(
                    onTap: () => changePaidStatus(WhoPay.whoPaySelf),
                    child: Container(
                      alignment: Alignment.center,
                      margin: p.paidStatusMargin(_visitGameProvider!.whoWantsToPay,WhoPay.whoPaySelf),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50),
                          color: p.paidStatusButtonColor(_visitGameProvider!.whoWantsToPay,WhoPay.whoPaySelf)),
                      child: Text(
                        S.of(context).self_paid,
                        style: TextStyle(
                            fontSize: 18.sp,
                            color: p.paidStatusTextColor(_visitGameProvider!.whoWantsToPay,WhoPay.whoPaySelf)),
                      ),
                    ),
                  )),
                  SizedBox(
                    width: 1.w,
                  ),
                  //被探班
                  Expanded(
                      child: GestureDetector(
                    onTap: () => changePaidStatus(WhoPay.whoPayOther),
                    child: Container(
                      alignment: Alignment.center,
                      margin: p.paidStatusMargin(_visitGameProvider!.whoWantsToPay, WhoPay.whoPayOther),
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(50),
                          color: p.paidStatusButtonColor(_visitGameProvider!.whoWantsToPay, WhoPay.whoPayOther)),
                      child: Text(
                        S.of(context).opposite_paid,
                        style: TextStyle(
                            fontSize: 18.sp,
                            color: p.paidStatusTextColor(_visitGameProvider!.whoWantsToPay, WhoPay.whoPayOther)),
                      ),
                    ),
                  )),
                ],
              ),
            ),
          ],
        ),
      );
    });
  }

  //切換支付方式事件
  void changePaidStatus(WhoPay input) {
    if (_visitGameProvider!.whoWantsToPay == input) {
      return;
    }
    _visitGameProvider?.setWhoWantsToPay(input);
  }

  //送出
  Widget _sendButton() {
    return GestureDetector(
        onTap: () => _sendOnTap(),
        child: Container(
            width: 58.w,
            height: 7.h,
            margin: EdgeInsets.symmetric(vertical: 1.h),
            child:
                buttonWidget("${S.of(context).open}${S.of(context).visited}")));
  }

  //送出事件
  void _sendOnTap() {
    //字數過少
    if (_input.text.length < atLeastNumber) {
      EasyLoading.showToast(
          "${S.of(context).at_least} $atLeastNumber ${S.of(context).how_many_word}");
      return;
    }
    //字數過多
    if(_input.text.length > atMaxNumber){
      EasyLoading.showToast(
          "${S.of(context).wordLimit} $atMaxNumber");
      return;
    }
    if (_address.text.trim().isEmpty) {
      EasyLoading.showToast(S.of(context).please_enter_address);
      return;
    }
    debugPrint('建立探班還是被探班 ${_visitGameProvider!.userStatus}');
    if (_utilApiProvider!.visitButton == ButtonStatus.notClick) {
      //防止執行完之前又建立一單
      //建立被探班
      _utilApiProvider!
          .addBeingVisitRecord(
        _memberProvider!.memberModel.id!,
        _memberProvider!.memberModel.name!,
        _memberProvider!.memberModel.gender!,
        _memberProvider!.memberModel.nickname!,
        _memberProvider!.memberModel.birthday!,
        _input.text,
        _address.text,
        _positionProvider!.currentPosition,
        _visitGameProvider!.whoWantsToPay,
      ).then((value) {
        if (value) {
          _visitGameProvider!.changeUserStatus(VisitMode.visitModeBeingVisit);
          _utilApiProvider!.setVisitButton(status: ButtonStatus.notClick);
          delegate.popRoute();
        }else{
          _utilApiProvider!.setVisitButton(status: ButtonStatus.notClick);
        }
      });
    }
  }
}
