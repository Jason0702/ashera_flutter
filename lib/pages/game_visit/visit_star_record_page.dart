
import 'package:ashera_flutter/app.dart';
import 'package:ashera_flutter/provider/visit_game_provider.dart';
import 'package:ashera_flutter/routers/route_name.dart';
import 'package:ashera_flutter/utils/api.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../generated/l10n.dart';
import '../../provider/util_api_provider.dart';
import '../../utils/app_color.dart';
import '../../utils/app_image.dart';
import '../../utils/shared_preference.dart';
import '../../utils/util.dart';
import '../../widget/titlebar_widget.dart';

class VisitStarRecordPage extends StatefulWidget {
  const VisitStarRecordPage({Key? key}) : super(key: key);

  @override
  State createState() => _VisitStarRecordPageState();
}

class _VisitStarRecordPageState extends State<VisitStarRecordPage> {
  int? _id;
  UtilApiProvider? _utilApiProvider;
  VisitGameProvider? _visitGameProvider;
  bool _loadingData = true;

  _onLayoutDone(_) async {
    EasyLoading.show(status: 'Loading...');
    _loadingData = await _utilApiProvider!.getStarRecord(id: _id!);
    EasyLoading.dismiss();
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  Widget build(BuildContext context) {
    _utilApiProvider = Provider.of<UtilApiProvider>(context, listen: false);
    _visitGameProvider = Provider.of<VisitGameProvider>(context, listen: false);
    _id ??= ModalRoute.of(context)!.settings.arguments as int;
    return Scaffold(
      body: Stack(
        children: [
          Column(
            children: [
              //標題
              titleBarIntegrate([], S.of(context).star_rating, [closeWidget()]),
              //內容
              Expanded(child: _body()),
            ],
          ),
        ],
      ),
    );
  }

  //X
  Widget closeWidget() {
    return GestureDetector(
      onTap: () => _closeOnTap(),
      child: const Icon(
        Icons.clear,
        color: AppColor.appTitleBarTextColor,
        size: 30,
      ),
    );
  }

  //X點擊
  void _closeOnTap() {
    _utilApiProvider!.clearStarRecord();
    delegate.popRoute();
  }

  //內容
  Widget _body() {
    return Container(
      margin: EdgeInsets.only(top: 1.h),
      child: Consumer<UtilApiProvider>(builder: (context, record, _) {
        return record.visitStarRecordList.isEmpty
            ? SizedBox(
          width: Device.width,
          height: Device.height,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image(
                width: 20.w,
                height: 20.h,
                image: AssetImage(AppImage.imgWaiting),
              ),
              Text(
                S.of(context).not_rated_yet,
                style: TextStyle(
                    color: Colors.grey[300]!,
                    fontWeight: FontWeight.bold,
                    fontSize: 20.sp),
              )
            ],
          ),
        )
            : ListView.builder(
          itemBuilder: (context, index) => _buildListWidget(index: index),
          itemCount: record.visitStarRecordList.length,
        );
      }),
    );
  }
  ///列表內容
  Widget _buildListWidget({required int index}){
    return Consumer<UtilApiProvider>(builder: (context, order, _){
      return Container(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 10),
        decoration: const BoxDecoration(
            color: Colors.white,
            border: Border(
                bottom: BorderSide(color: AppColor.grayFrame, width: 1))),
        child: Row(
          children: [
            //頭像
            FutureBuilder(
                future: _visitGameProvider!.getMugshotMemberById(id: order.visitStarRecordList[index].memberId!, interactive: true),
                builder: (context, AsyncSnapshot<String> snapshot){
                  if(snapshot.connectionState == ConnectionState.done){
                    return Container(
                        width: 13.w,
                        height: 9.h,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                                fit: BoxFit.cover,
                                image: CachedNetworkImageProvider(
                                  Util().getMemberMugshotUrl(snapshot.data!),
                                  headers: {"authorization": "Bearer " + Api.accessToken},
                                ),
                                onError: (error, stackTrace) {
                                  debugPrint('Error: ${error.toString()}');
                                })));
                  }
                  return Container(
                    width: 13.w,
                    height: 9.h,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle, gradient: AppColor.appMainColor),
                    child: CircleAvatar(
                        backgroundColor: Colors.transparent,
                        child: Image.asset(AppImage.iconCat)
                    ),
                  ); /*const CircularProgressIndicator();*/
                }),
            //名稱&星星
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  //名稱
                  Container(
                    child: FittedBox(
                      child: Text(
                        order.visitStarRecordList[index]
                            .memberNickname!,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 18.sp,
                            color: Colors.black),
                      ),
                    ),
                    margin: const EdgeInsets.only(left: 10),
                  ),
                  //星星
                  Row(
                    children: [
                      Container(
                        margin: const EdgeInsets.only(left: 10, right: 5),
                        width: 5.w,
                        child: Image.asset(AppImage.iconStar),
                      ),
                      Text(
                        order.visitStarRecordList[index]
                            .memberScore!.toStringAsFixed(1),
                        style: const TextStyle(color: Colors.black),
                      ),
                    ],
                  )
                ],
              ),
            ),
            //內容
            Expanded(
                flex: 2,
                child: GestureDetector(
                  onTap: () => delegate.push(name: RouteName.visitStarRecordIndexPage, arguments: order.visitStarRecordList[index]),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      //內容title
                      Text(
                        '${S.of(context).content}:',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 18.sp,
                            color: Colors.black),
                      ),
                      //內容
                      Text(
                        order.visitStarRecordList[index].memberComment ?? '',
                        overflow: TextOverflow.ellipsis,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 18.sp,
                            color: Colors.black),
                      ),
                    ],
                  ),
                ))
          ],
        ),
      );
    },);
  }
}
