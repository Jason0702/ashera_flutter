import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:typed_data';

import 'package:ashera_flutter/app.dart';
import 'package:ashera_flutter/dialog/visit_ask_cancel_dialog.dart';
import 'package:ashera_flutter/generated/l10n.dart';
import 'package:ashera_flutter/models/add_unlock_payment_record_dto.dart';
import 'package:ashera_flutter/models/event_bus_model.dart';
import 'package:ashera_flutter/models/message_dto.dart';
import 'package:ashera_flutter/models/visit_record_dto.dart';
import 'package:ashera_flutter/provider/member_provider.dart';
import 'package:ashera_flutter/provider/position_provider.dart';
import 'package:ashera_flutter/provider/stomp_client_provider.dart';
import 'package:ashera_flutter/provider/unlock_mugshot_provider.dart';
import 'package:ashera_flutter/provider/util_api_provider.dart';
import 'package:ashera_flutter/widget/titlebar_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import '../../dialog/game_point_insufficient_dialog.dart';
import '../../dialog/rating_dialog.dart';
import '../../dialog/unblock_mugshot_dialog.dart';
import '../../enum/app_enum.dart';
import '../../models/get_chat_message_dto.dart';
import '../../models/member_model.dart';
import '../../models/message_record_model.dart';
import '../../models/visit_record_model.dart';
import '../../provider/visit_game_provider.dart';
import '../../routers/route_name.dart';
import '../../utils/api.dart';
import '../../utils/app_color.dart';
import '../../utils/app_image.dart';
import '../../utils/shared_preference.dart';
import '../../utils/util.dart';
import '../../widget/button_widget.dart';
import '../../widget/gender_icon.dart';
import '../../widget/image_shower.dart';
import '../../widget/mugshot_widget.dart';

class VisitProcessPage extends StatefulWidget {
  const VisitProcessPage({Key? key}) : super(key: key);

  @override
  _VisitProcessPageState createState() => _VisitProcessPageState();
}

class _VisitProcessPageState extends State<VisitProcessPage> {
  VisitGameProvider? _visitGameProvider;
  UtilApiProvider? _utilApiProvider;
  VisitRecordModel? _visitRecord;
  StompClientProvider? _stompClientProvider;
  MemberProvider? _memberProvider;
  PositionProvider? _positionProvider;
  UnlockMugshotProvider? _unlockMugshotProvider;
  final GlobalKey<_ShowTimeLeftState> _showTime = GlobalKey();

  //大頭針
  final Set<Marker> _markers = {};

  //初始化位置
  LatLng? _initialPosition;

  //倒數
  Timer? _timer;

  _onLayoutDone(_) async{
    GetLastVisitChatMessageDTO _dto = GetLastVisitChatMessageDTO(username: _memberProvider!.memberModel.name!, visitRecordId: _visitRecord!.id!);
    _stompClientProvider!.setVisitId(id: _visitRecord!.id!);
    _stompClientProvider!.sendMessage(Stomp.getVisitLastChatMessage, json.encode(_dto.toJson()));
    _timerStart();
    _visitGameProvider!.addListener(_timerStart);
    _utilApiProvider!.addListener(() {
      _timerStart();
      _addMark().then((value) {
        if(mounted){
          setState(() {});
        }
      });
    });
    _addMark().then((value) => setState(() {}));
  }

  void _timerStart() {
    _visitRecord = _utilApiProvider!.visitRecordModel; //替換成最新資料
    if (_visitGameProvider!.visitStatus == VisitStatus.processing) {
      if (_timeLeft() != '00:00:00') {
        _timer ??= Timer.periodic(const Duration(milliseconds: 1000), (timer) {
          _showTime.currentState?.onRefresh(_timeLeft());
        });
      } else {
        _showTime.currentState?.onRefresh(_timeLeft());
      }
    }
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  void dispose() {
    super.dispose();
    _visitGameProvider!.removeListener(_timerStart);
    _utilApiProvider!.removeListener(_timerStart);
    if (_timer != null) {
      _timer!.cancel();
      _timer = null;
    }
  }

  @override
  Widget build(BuildContext context) {
    _visitGameProvider = Provider.of<VisitGameProvider>(context);
    _stompClientProvider = Provider.of<StompClientProvider>(context);
    _memberProvider = Provider.of<MemberProvider>(context);
    _positionProvider = Provider.of<PositionProvider>(context);
    _utilApiProvider = Provider.of<UtilApiProvider>(context);
    _unlockMugshotProvider = Provider.of<UnlockMugshotProvider>(context);
    _visitRecord ??=
        ModalRoute.of(context)!.settings.arguments as VisitRecordModel;
    return Scaffold(
      body: Column(
        children: [
          //標題
          _titleBar(),
          //對方資訊
          otherSideInfo(),
          //地圖內容
          Expanded(child: _googleMap()),
          //探班資訊
          orderInfo(),
        ],
      ),
    );
  }

  Future<void> _addMark() async {
    _markers.clear();
    if (_visitGameProvider!.userStatus == VisitMode.visitModeVisit) {
      String makerId = '${_visitRecord!.id}';
      _markers.add(Marker(
        markerId: MarkerId(makerId),
        position:
            LatLng(_visitRecord!.endLatitude!, _visitRecord!.endLongitude!),
        icon: await _getMarkerIcon(
            id: _visitRecord!.targetMemberId!,
            gender: _visitRecord!.targetMemberGender!),
        onTap: () => unblockMemberMugshot(
            _memberProvider!.memberModel.id!,
            _visitRecord!.targetMemberNickname!,
            _visitRecord!.targetMemberGender!,
            _visitRecord!.targetMemberBirthday!,
            _visitRecord!.targetMemberId!),
      ));
    } else {
      String makerId = '${_visitRecord!.id}';
      _markers.add(Marker(
        markerId: MarkerId(makerId),
        position:
            LatLng(_visitRecord!.fromLatitude!, _visitRecord!.fromLongitude!),
        icon: await _getMarkerIcon(
            id: _visitRecord!.fromMemberId!,
            gender: _visitRecord!.fromMemberGender!),
        onTap: () => unblockMemberMugshot(
            _memberProvider!.memberModel.id!,
            _visitRecord!.fromMemberNickname!,
            _visitRecord!.fromMemberGender!,
            _visitRecord!.fromMemberBirthday!,
            _visitRecord!.fromMemberId!),
      ));
    }
  }

  //大頭針Icon
  Future<BitmapDescriptor> _getMarkerIcon(
      {required int id, required int gender}) async {
    Uint8List? markerIcon;
    if(_memberProvider!.memberModel.vip == VIPStatus.closure.index){
      String _iconUrl = await _visitGameProvider!.getMugshotMemberById(id: id, interactive: true);
      markerIcon = await Util().makeReceiptImage(
          avatar: Util().getMemberMugshotUrl(_iconUrl),
          marker: AppImage.imgUser);
    } else if (_isFriend(id: id)) {
      //如果是朋友了
      markerIcon = await Util().makeReceiptImage(avatar:
      Util().getMemberMugshotUrl(_stompClientProvider!.friendList
          .where((element) => element.id == id)
          .first
          .interactivePic!),
          marker: AppImage.imgUser
      );
    } else if (_isUnlock(id: id)) {
      //如果是解鎖了
      markerIcon = await Util().makeReceiptImage(avatar:
          Util().getMemberMugshotUrl(_unlockMugshotProvider!.memberMugshotList
              .where((element) => element.id == id)
              .first
              .interactivePic!),
          marker: AppImage.imgUser);
    } else {
      //都不是
      markerIcon = await Util().makeReceiptImage(avatar:
          gender == 0 ? AppImage.iconDog : AppImage.iconCat, marker: AppImage.imgUser);
    }
    return BitmapDescriptor.fromBytes(markerIcon);
  }

  //判斷是否是朋友
  bool _isFriend({required int id}) {
    return _stompClientProvider!.friendList
        .where((element) => element.id == id)
        .isNotEmpty;
  }

  //判斷此人是否有解鎖過
  bool _isUnlock({required int id}) {
    return _unlockMugshotProvider!.memberMugshotList
        .where((element) => element.id == id)
        .isNotEmpty;
  }

  //地圖內容
  Widget _googleMap() {
    return GoogleMap(
      markers: _markers,
      mapType: MapType.normal,
      zoomControlsEnabled: false,
      myLocationButtonEnabled: true,
      myLocationEnabled: true,
      mapToolbarEnabled: false,
      initialCameraPosition: CameraPosition(
          target: _initialPosition ??
              LatLng(_visitRecord!.endLatitude!, _visitRecord!.endLongitude!),
          zoom: 16.0),
    );
  }

  //標題
  Widget _titleBar() {
    return Consumer<VisitGameProvider>(builder: (context, p, _) {
      if (_visitGameProvider?.userStatus == VisitMode.visitModeVisit) {
        if (p.visitStatus.index == VisitStatus.processing.index) {
          return titleBarIntegrate(
              [back()], S.of(context).visit, [cancelVisit()]);
        }
        return titleBar(S.of(context).visit);
      } else {
        if (p.visitStatus.index == VisitStatus.processing.index) {
          return titleBarIntegrate(
              [back()], S.of(context).visited, [cancelVisit()]);
        }
        return titleBar(S.of(context).visited);
      }
    });
  }

  //返回
  Widget back() {
    return GestureDetector(
      onTap: () => {delegate.popRoute()},
      child: const Icon(
        Icons.arrow_back,
        color: AppColor.appTitleBarTextColor,
        size: 30,
      ),
    );
  }

  //取消探班
  Widget cancelVisit() {
    return Offstage(
      offstage: _canShowCancelVisit(),
      child: GestureDetector(
        onTap: () => cancelVisitOnTap(),
        child: FittedBox(
          child: Text(
            S.of(context).cancel_visit,
            style: TextStyle(color: Colors.black, fontSize: 18.sp),
          ),
        ),
      ),
    );
  }

  bool _canShowCancelVisit() {
    if (_visitGameProvider?.userStatus == VisitMode.visitModeVisit) {
      if (_visitRecord!.fromMemberScore != 0.0) {
        return true;
      }
    } else if (_visitGameProvider?.userStatus ==
        VisitMode.visitModeBeingVisit) {
      if (_visitRecord!.targetMemberScore != 0.0) {
        return true;
      }
    }
    if (_visitRecord?.status == VisitStatus.processing.index) {
      return false;
    }
    if(_visitRecord?.status == VisitStatus.pairing.index){
      return false;
    }
    return true;
  }

  //對方資訊
  Widget otherSideInfo() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      color: AppColor.grayFrame,
      child: Consumer<VisitGameProvider>(
        builder: (context, p, _) {
          return p.userStatus == VisitMode.visitModeVisit
              ? _targetMemberWidget()
              : _fromMemberWidget();
        },
      ),
    );
  }

  //探班顯示 被探班的人
  Widget _targetMemberWidget() {
    return Consumer<UtilApiProvider>(builder: (context, targetMember, _) {
      _visitRecord = targetMember.visitRecordModel;
      return Row(
        children: [
          //頭像
          MugshotWidget(
              fromMemberId: _memberProvider!.memberModel.id!,
              targetMemberId: _visitRecord!.targetMemberId!,
              nickName: _visitRecord!.targetMemberNickname!,
              gender: _visitRecord!.targetMemberGender!,
              birthday: _visitRecord!.targetMemberBirthday!,
              interactive: true,
          ),
          //名稱&星星&探班被探班標籤
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                //名稱
                Row(
                  children: [
                    //名稱
                    Container(
                      child: FittedBox(
                        child: Text(
                          _visitRecord!.targetMemberNickname!,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 18.sp,
                              color: Colors.black),
                        ),
                      ),
                      margin: const EdgeInsets.only(left: 10),
                    ),
                    //星星
                    GestureDetector(
                      onTap: () => _starOnTap(id: _visitRecord!.targetMemberId!),
                      child: Row(
                        children: [
                          Container(
                            margin: const EdgeInsets.only(left: 10, right: 5),
                            width: 5.w,
                            child: Image.asset(AppImage.iconStar),
                          ),
                          Text(
                            targetMember.otherStar.toStringAsFixed(1),
                            style: const TextStyle(color: Colors.black),
                          )
                        ],
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: 0.5.h,
                ),
                //性別年齡
                Container(
                  margin: const EdgeInsets.only(left: 10),
                  child: Row(
                    children: [
                      getGender(_visitRecord!.targetMemberGender!),
                      //年齡
                      Container(
                        margin: const EdgeInsets.only(left: 5),
                        child: Text(
                          Util().getAge(_visitRecord!.targetMemberBirthday!),
                          style: const TextStyle(color: Colors.black),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          //探班被探班標籤
          userStatusLabel(),
        ],
      );
    });
  }

  //被探班顯示 探班的人
  Widget _fromMemberWidget() {
    return Consumer<UtilApiProvider>(builder: (context, fromMember, _) {
      _visitRecord = fromMember.visitRecordModel;
      return Row(
        children: [
          //頭像
          MugshotWidget(
            fromMemberId: _memberProvider!.memberModel.id!,
            targetMemberId: _visitRecord!.fromMemberId!,
            nickName: _visitRecord!.fromMemberNickname!,
            gender: _visitRecord!.fromMemberGender!,
            birthday: _visitRecord!.fromMemberBirthday!,
            interactive: true,
          ),
          //名稱&星星&探班被探班標籤
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                //名稱
                Row(
                  children: [
                    //名稱
                    Container(
                      child: FittedBox(
                        child: Text(
                          _visitRecord!.fromMemberNickname!,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 18.sp,
                              color: Colors.black),
                        ),
                      ),
                      margin: const EdgeInsets.only(left: 10),
                    ),
                    //星星
                    GestureDetector(
                      onTap: () => _starOnTap(id: _visitRecord!.fromMemberId!),
                      child: Row(
                        children: [
                          Container(
                            margin: const EdgeInsets.only(left: 10, right: 5),
                            width: 5.w,
                            child: Image.asset(AppImage.iconStar),
                          ),
                          Text(
                            fromMember.otherStar.toStringAsFixed(1),
                            style: const TextStyle(color: Colors.black),
                          )
                        ],
                      ),
                    )
                  ],
                ),
                SizedBox(
                  height: 0.5.h,
                ),
                //性別年齡
                Container(
                  margin: const EdgeInsets.only(left: 10),
                  child: Row(
                    children: [
                      getGender(_visitRecord!.fromMemberGender!),
                      //年齡
                      Container(
                        margin: const EdgeInsets.only(left: 5),
                        child: Text(
                          Util().getAge(_visitRecord!.fromMemberBirthday!),
                          style: const TextStyle(color: Colors.black),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          //探班被探班標籤
          userStatusLabel(),
        ],
      );
    });
  }

  ///星星點擊
  /// * [id] 用戶ID
  void _starOnTap({required int id}){
    //log('點的到星星? $id');
    delegate.push(name: RouteName.visitStarRecordPage, arguments: id);
  }

  //解鎖大頭照
  Future<void> unblockMemberMugshot(int fromMemberId, String nickName,
      int gender, String birthday, int targetMemberId) async {
    if (_isFriend(id: targetMemberId)) {
      showImage(files: _stompClientProvider!.friendList.firstWhere((element) => element.id == targetMemberId).interactivePic!);
      return;
    } else if (_isUnlock(id: targetMemberId)) {
      showImage(files: _unlockMugshotProvider!.memberMugshotList.firstWhere((element) => element.id == targetMemberId).interactivePic!);
      return;
    } else {
      bool? result = await showGeneralDialog(
          context: context,
          pageBuilder: (context, anim1, anim2) {
            return UnblockMugshotDialog(
              key: UniqueKey(),
              nickName: nickName,
              gender: gender,
              birthday: birthday,
            );
          },
          barrierColor: Colors.black54,
          barrierDismissible: false,
          transitionDuration: const Duration(milliseconds: 100),
          transitionBuilder: (context, anim1, anim2, child) {
            return Transform.scale(
              scale: anim1.value,
              child: child,
            );
          });
      if (result != null) {
        if (result) {
          //想要解鎖
          if (_memberProvider!.memberPoint.point! >= _utilApiProvider!.systemSetting!.unlockMugshotPoints!) {
            //點數判斷夠不夠
            AddUnlockPaymentRecordDTO _addUnlockPaymentRecordDTO =
                AddUnlockPaymentRecordDTO(
                    fromMemberId: fromMemberId, targetMemberId: targetMemberId);
            _stompClientProvider!.sendUnlockMugshot(
                addUnlockPaymentRecordDTO: _addUnlockPaymentRecordDTO);
          } else {
            if(_utilApiProvider!.systemSetting!.addPointsStatus == PointsStatus.closure.index){
              EasyLoading.showToast(S.of(context).not_enough_points);
            }else{
              //點數不足
              _pointInsufficient();
            }
          }
        } else {
          //不解鎖

        }
      }
    }
  }

  //誰支付標籤
  Widget userStatusLabel() {
    return Padding(
      padding: const EdgeInsets.only(left: 5),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            padding: EdgeInsets.symmetric(horizontal: 1.5.w, vertical: 0.5.h),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30),
              border: Border.all(color: AppColor.buttonFrameColor, width: 1),
            ),
            child: Text(
              _getWhoPay(),
              style:
                  TextStyle(color: AppColor.buttonFrameColor, fontSize: 15.sp),
            ),
          ),
        ],
      ),
    );
  }

  String _getWhoPay() {
    if (_visitRecord!.mode == 0 && _visitRecord!.whoPay == 1) {
      return S.of(context).self_paid;
    } else if (_visitRecord!.mode == 0 && _visitRecord!.whoPay == 0) {
      return S.of(context).opposite_paid;
    } else if (_visitRecord!.mode == 1 && _visitRecord!.whoPay == 0) {
      return S.of(context).self_paid;
    } else {
      return S.of(context).opposite_paid;
    }
  }

  //探班資訊
  Widget orderInfo() {
    return Consumer<VisitGameProvider>(builder: (context, p, _) {
      return Container(
        padding: const EdgeInsets.symmetric(horizontal: 20),
        width: Device.width,
        color: AppColor.grayFrame,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            //位置icon
            Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Expanded(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        children: [
                          SizedBox(
                            width: 15.w,
                            child: Image.asset(AppImage.iconVisitLocation),
                          ),
                          SizedBox(
                            width: 1.w,
                          ),
                          Expanded(child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              FittedBox(
                                child: Text(
                                  S.of(context).location,
                                  style:
                                  TextStyle(fontSize: 18.sp, color: Colors.black),
                                ),
                              ),
                              //地址
                              FittedBox(
                                child: Text(
                                  _visitRecord!.address!,
                                  style: TextStyle(
                                      fontSize: 18.sp,
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold),
                                ),
                              ),
                              //距離
                              Row(
                                children: [
                                  SizedBox(
                                    width: 8.w,
                                    height: 8.w,
                                    child: Image.asset(AppImage.iconPosition),
                                  ),
                                  SizedBox(
                                    width: 2.w,
                                  ),
                                  Consumer<PositionProvider>(
                                    builder: (context, position, _) {
                                      LatLng _latLng = LatLng(
                                          _visitRecord!.endLatitude!,
                                          _visitRecord!.endLongitude!);
                                      return FittedBox(
                                        fit: BoxFit.scaleDown,
                                        child: RichText(
                                            text: TextSpan(children: [
                                              TextSpan(
                                                text:
                                                "${S.of(context).distance}${_getDistanceBetween(position.currentPosition, _latLng)}，${S.of(context).about}",
                                                style: TextStyle(
                                                    color: AppColor.grayText,
                                                    fontSize: 16.sp),
                                              ),
                                              TextSpan(
                                                text: _getMin(
                                                    position.currentPosition, _latLng),
                                                style: TextStyle(
                                                    color: Colors.black, fontSize: 16.sp),
                                              ),
                                              TextSpan(
                                                text: S.of(context).minute,
                                                style: TextStyle(
                                                    color: AppColor.grayText,
                                                    fontSize: 16.sp),
                                              ),
                                            ])),
                                      );
                                    },
                                  )
                                ],
                              ),
                            ],
                          )),
                        ],
                      ),
                      //需求icon
                      Row(
                        children: [
                          SizedBox(
                            width: 15.w,
                            child: Image.asset(AppImage.iconVisitRequest),
                          ),
                          SizedBox(
                            width: 1.w,
                          ),
                          Expanded(child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                S.of(context).needed,
                                style:
                                TextStyle(fontSize: 18.sp, color: Colors.black),
                              ),
                              //需求
                              Text(
                                _visitRecord!.requirement!,maxLines: 6,
                                style: TextStyle(
                                    fontSize: 18.sp,
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                              ),
                            ],
                          ))
                        ],
                      ),
                    ],
                  ),
                ),
                if (p.visitStatus.index == VisitStatus.processing.index)
                  SizedBox(
                    width: 50,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: [
                        Stack(
                          children: [
                            SizedBox(
                                width: 13.w,
                                height: 13.w,
                                child: GestureDetector(
                                  onTap: () => _goToChatRoom(),
                                  child: Image.asset(AppImage.iconChat),
                                )),
                            Positioned(
                                top: 0,
                                right: 0,
                                child: _lastUnreadMessageCount(
                                    _visitGameProvider!.userStatus ==
                                            VisitMode.visitModeVisit
                                        ? _visitRecord!.targetMemberName!
                                        : _visitRecord!.fromMemberName!))
                          ],
                        ),
                        FittedBox(
                          fit: BoxFit.scaleDown,
                          child: Text(
                            S.of(context).time_left,
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.black, fontSize: 16.sp),
                          ),
                        ),
                        ShowTimeLeft(key: _showTime,)
                      ],
                    ),
                  )
              ],
            ),
            //送出
            Center(child: _sendButton()),
          ],
        ),
      );
    });
  }

  //倒數時間計算
  String _timeLeft() {
    DateTime _nowTime = DateTime.now(); //現在時間
    DateTime _expireAt = DateTime.fromMillisecondsSinceEpoch(
        _visitRecord!.expireAt! * 1000,
        isUtc: false); //探班到期時間
    Duration timeLag = _nowTime.difference(_expireAt);
    Duration secLag = Duration(seconds: timeLag.inSeconds);
    List<String> parts = secLag.toString().split(':');
    List<String> _delDot = parts[2].toString().split('.');
    if (timeLag.inSeconds.isNegative) {
      return '${parts[0].padLeft(2, '0')}:${parts[1].padLeft(2, '0')}:${_delDot[0].padLeft(2, '0')}';
    }
    return '00:00:00';
  }

  //未讀訊息計算
  Widget _lastUnreadMessageCount(String _name) {
    return Consumer<StompClientProvider>(builder: (context, friend, _) {
      if (friend.visitMessageLastRecordDataList
          .where((element) => element.visitRecordId == _visitRecord!.id!)
          .isNotEmpty) {
        MessageLastRecordModel _messageRecordData = friend.visitMessageLastRecordDataList
            .where((element) => element.fromMember == _name)
            .first;
        if (_messageRecordData.unreadCnt != 0) {
          return Container(
              height: 5.w,
              width:
                  Util().redDotWidth(_messageRecordData.unreadCnt.toString()),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: AppColor.buttonFrameColor,
                  borderRadius: BorderRadius.circular(30)),
              child: Text(
                _messageRecordData.unreadCnt.toString(),
                textScaleFactor: 1,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 16.sp,
                    fontWeight: FontWeight.bold),
              ));
        } else {
          //0 就表示 沒有未讀消息
          return Container();
        }
      } else {
        //如果沒有訊息
        return Container();
      }
    });
  }

  //距離計算
  String _getDistanceBetween(LatLng _myLatLng, LatLng _visitLatLng) {
    String _value = '';
    double _distance = Geolocator.distanceBetween(_myLatLng.latitude,
        _myLatLng.longitude, _visitLatLng.latitude, _visitLatLng.longitude);
    if (_distance > 1000) {
      _value = '${(_distance / 1000).toStringAsFixed(2)}${S.of(context).km}';
    } else {
      _value = '${_distance.toStringAsFixed(2)}${S.of(context).m}';
    }
    return _value;
  }

  //分鐘數計算
  String _getMin(LatLng _myLatLng, LatLng _visitLatLng) {
    String _value = '';
    double _distance = Geolocator.distanceBetween(_myLatLng.latitude,
        _myLatLng.longitude, _visitLatLng.latitude, _visitLatLng.longitude);
    if (_distance >= 500) {
      String _min = (_distance / 500).ceil().toString();
      _value = _min;
    } else {
      _value = '00';
    }
    return _value;
  }

  //送出
  Widget _sendButton() {
    return GestureDetector(
        onTap: () => _sendOnTap(),
        child: Container(
            margin: EdgeInsets.only(top: 1.h, bottom: 1.h),
            height: 7.h,
            width: 58.w,
            child: _buttonWidget()));
  }

  //按鈕樣式
  Widget _buttonWidget() {
    if (_visitGameProvider!.visitStatus == VisitStatus.waitingApprove) {
      return buttonWidgetWaiting(S.of(context).apply_finish_waiting_accept);
    } else if (_visitGameProvider!.visitStatus == VisitStatus.processing) {
      return buttonWidget(S.of(context).finish_visit);
    } else if (_visitGameProvider!.visitStatus ==
        VisitStatus.cancelWaitingApprove) {
      return buttonWidget(S.of(context).wait_other_side_cancel_visit);
    } else if (_visitGameProvider!.visitStatus == VisitStatus.complete) {
      return buttonWidget(S.of(context).the_visit_has_been_completed);
    } else if (_visitGameProvider!.visitStatus == VisitStatus.pairing) {
      return buttonWidget(S.of(context).visit);
    } else {
      return Container();
    }
  }

  //送出事件
  void _sendOnTap() {
    //log('_visitGameProvider!.visitStatus ${_visitGameProvider!.visitStatus}');
    //log('_visitRecord!.status ${_visitRecord!.status}');
    switch (_visitGameProvider!.visitStatus) {
      case VisitStatus.pairing:
        if (_visitRecord!.status == VisitStatus.pairing.index) {
          WannaVisitDTO _wannaVisitDto = WannaVisitDTO(
              id: _visitRecord!.id,
              fromMemberId: _memberProvider!.memberModel.id,
              fromMemberName: _memberProvider!.memberModel.name,
              fromMemberNickname: _memberProvider!.memberModel.nickname,
              fromMemberGender: _memberProvider!.memberModel.gender,
              fromMemberBirthday: _memberProvider!.memberModel.birthday,
              fromLatitude: _positionProvider!.currentPosition.latitude,
              fromLongitude: _positionProvider!.currentPosition.longitude);
          _stompClientProvider!.sendWannaVisit(wannaVisitDto: _wannaVisitDto);
          _visitGameProvider!.changeVisitStatus(VisitStatus.waitingApprove);
          //過20秒重取紀錄
          if(mounted){
            Future.delayed(const Duration(seconds: 20), (){
              _recordStatusRefresh();//判斷狀態
            });
          }
        }
        break;
      case VisitStatus.processing:
        showRatingDialog();
        break;
      case VisitStatus.cancel:
        break;
      case VisitStatus.complete:
        delegate.popUtil();
        break;
      default:
        break;
    }
  }

  void _recordStatusRefresh(){
    _utilApiProvider!.getByFromMemberId(_memberProvider!.memberModel.id!, false).then((value) {
      if(value.where((element) => element.status == VisitStatus.pairing.index).isNotEmpty){
        VisitRecordModel _record = value.where((element) => element.status == VisitStatus.pairing.index).first;
        //log('相差20秒內嗎? ${Util().isWithinTwentySeconds(_record.wannaVisitAt!)}');
        if(!Util().isWithinTwentySeconds(_record.wannaVisitAt!)){ //時間超過就發送對方不同意
          _visitGameProvider!.changeVisitStatus(VisitStatus.pairing); //狀態改回等待
          visitBus.fire(VisitEventModel('selfCancel', null));
        }
      }
    });
  }

  //取消探班
  void cancelVisitOnTap() async {
    //詢問是否確定取消探班
    bool? _result = await showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return VisitAskCancelDialog(
            key: UniqueKey(),
            title: S.of(context).cancel_visit,
            content:
                '${S.of(context).are_you_sure}${S.of(context).cancel_visit}?',
            confirmButtonText: '${S.of(context).sure}${S.of(context).cancel}',
            haveClose: true,
          );
        },
        barrierColor: Colors.black54,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 100),
        transitionBuilder: (context, anim1, anim2, child) {
          return Transform.scale(
            scale: anim1.value,
            child: child,
          );
        });
    if (_result != null) {
      if (_result) {
        if(_visitRecord!.status == VisitStatus.processing.index){
          if (_visitGameProvider?.userStatus == VisitMode.visitModeVisit) {
            _stompClientProvider!.sendCancelRecordByFromMemberInProcessing(
                id: _visitRecord!.id!); //在進行中被 探班會員取消
          } else {
            _stompClientProvider!.sendCancelRecordByTargetMemberInProcessing(
                id: _visitRecord!.id!); //在進行中被 被探班會員取消
          }
        } else if(_visitRecord!.status == VisitStatus.pairing.index){
          _stompClientProvider!.sendCancelVisit(id: _visitRecord!.id!);
        }
      }
    }
  }

  //評分 評論
  void showRatingDialog() {
    //探班
    if (_visitGameProvider!.userStatus == VisitMode.visitModeVisit) {
      if (_visitRecord!.fromMemberScore != 0.0) {
        //用分數判斷防止重複評分探班
        EasyLoading.showToast(S.of(context).reviewed);
        return;
      }
      if(_utilApiProvider!.visitRecordModel.targetMemberScore == 0.0){
        //用分數判斷被探班方是否評分完成
        EasyLoading.showToast(S.of(context).the_opponent_has_not_rated_yet);
        return;
      }
      showGeneralDialog(
          context: context,
          pageBuilder: (context, anim1, anim2) {
            return RatingDialog(
              key: UniqueKey(),
              nickName: _visitRecord!.targetMemberNickname,
              birthday: _visitRecord!.targetMemberBirthday,
              gender: _visitRecord!.targetMemberGender,
              targetMemberId: _visitRecord!.targetMemberId,
            );
          },
          barrierColor: Colors.black54,
          barrierDismissible: false,
          transitionDuration: const Duration(milliseconds: 100),
          transitionBuilder: (context, anim1, anim2, child) {
            return Transform.scale(
              scale: anim1.value,
              child: child,
            );
          }).then((value) {
        Map<String, dynamic> _map = value as Map<String, dynamic>;
        //評分資料
        RateVisitRecordDTO _rateVisitRecord = RateVisitRecordDTO(
            id: _visitRecord!.id,
            memberId: _memberProvider!.memberModel.id,
            memberName: _memberProvider!.memberModel.name,
            score: _map['rating'],
            comment: _map['commit']);
        _stompClientProvider!.sendFromMemberRateVisit(rateVisitRecord: _rateVisitRecord);
        _visitGameProvider!.changeVisitStatus(VisitStatus.pairing);
        delegate.popUtil();
      });
    } else {
      if (_visitRecord!.targetMemberScore != 0.0) {
        //用分數判斷防止重複評分探班
        EasyLoading.showToast(S.of(context).reviewed);
        return;
      }
      //被探班
      showGeneralDialog(
          context: context,
          pageBuilder: (context, anim1, anim2) {
            return RatingDialog(
              key: UniqueKey(),
              nickName: _visitRecord!.fromMemberNickname,
              birthday: _visitRecord!.fromMemberBirthday,
              gender: _visitRecord!.fromMemberGender,
              targetMemberId: _visitRecord!.fromMemberId,
            );
          },
          barrierColor: Colors.black54,
          barrierDismissible: false,
          transitionDuration: const Duration(milliseconds: 100),
          transitionBuilder: (context, anim1, anim2, child) {
            return Transform.scale(
              scale: anim1.value,
              child: child,
            );
          }).then((value) {
        Map<String, dynamic> _map = value as Map<String, dynamic>;
        //評分資料
        RateVisitRecordDTO _rateVisitRecord = RateVisitRecordDTO(
            id: _visitRecord!.id,
            memberId: _memberProvider!.memberModel.id,
            memberName: _memberProvider!.memberModel.name,
            score: _map['rating'],
            comment: _map['commit']);
        _stompClientProvider!.sendTargetMemberRateVisit(rateVisitRecord: _rateVisitRecord);
        _visitGameProvider!.changeVisitStatus(VisitStatus.pairing);
        delegate.popUtil();
      });
    }
  }

  //進入聊天室
  void _goToChatRoom() {
    MemberModel _memberModel;
    //取得聊天紀錄
    if (_visitGameProvider!.userStatus == VisitMode.visitModeVisit) {
      _memberModel = MemberModel(
          id: _visitRecord!.targetMemberId,
          name: _visitRecord!.targetMemberName,
          nickname: _visitRecord!.targetMemberNickname,
          gender: _visitRecord!.targetMemberGender,
          height: 0.0,
          weight: 0.0,
          birthday: _visitRecord!.targetMemberBirthday,
          cellphone: _visitRecord!.targetMemberName,
          mugshot: _visitRecord!.targetMemberMugshot,
          zodiacSign: '',
          bloodType: '',
          aboutMe: '',
          job: '',
          asheraUid: '',
          updatedAt: '',
          latitude: 0.0,
          longitude: 0.0,
          faceId: '',
          verify: 0,
          facePic: '',
          s3Etag: '',
          vip: 0,
          activeStart: 0,
          activeEnd: 0,
          hideBirthday: 0,
          status: 0,
          initGame: 0,
          initGameAt: 0,
          interactivePic: ''
      );
    } else {
      _memberModel = MemberModel(
          id: _visitRecord!.fromMemberId,
          name: _visitRecord!.fromMemberName,
          nickname: _visitRecord!.fromMemberNickname,
          gender: _visitRecord!.fromMemberGender,
          height: 0.0,
          weight: 0.0,
          birthday: _visitRecord!.fromMemberBirthday,
          cellphone: _visitRecord!.fromMemberName,
          mugshot: _visitRecord!.fromMemberMugshot,
          zodiacSign: '',
          bloodType: '',
          aboutMe: '',
          job: '',
          asheraUid: '',
          updatedAt: '',
          latitude: 0.0,
          longitude: 0.0,
          faceId: '',
          verify: 0,
          facePic: '',
          s3Etag: '',
          vip: 0,
          activeStart: 0,
          activeEnd: 0,
          hideBirthday: 0,
          status: 0,
          initGame: 0,
          initGameAt: 0,
          interactivePic: ''
      );
    }
    _stompClientProvider!
        .sendMessage(Stomp.getVisitChatMessagePage, '${_visitRecord!.id!}');

    Map<String, dynamic> _map = {
      'member': _memberModel,
      'visitRecord': _visitRecord
    };
    delegate.push(name: RouteName.visitChatRoomPage, arguments: _map);
  }

  ///照片放大功能
  void showImage({required String files}){
    showGeneralDialog(
        context: context,
        pageBuilder: (context, animation, secondaryAnimation) =>
            ImageShower(key: UniqueKey(),data: [files], index: 0),
        transitionBuilder: (context, animation, secondaryAnimation, child) {
          const begin = Offset(0.0, 1.0);
          const end = Offset.zero;
          const curve = Curves.ease;
          var tween =
          Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
          return SlideTransition(
            position: animation.drive(tween),
            child: child,
          );
        });
  }

  ///點數不足統一方法
  void _pointInsufficient() async {
    bool? result = await _showPointInsufficient();
    if (result != null) {
      if (result) {
        //跳轉加值
        delegate.push(name: RouteName.myPointsPage);
      }
    }
  }

  ///點數不足
  Future<bool?> _showPointInsufficient() async {
    return await showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return GamePointInsufficientDialog(
              key: UniqueKey(),
              title: S.of(context).message,
              content: S.of(context).please_add_value,
              confirmButtonText: S.of(context).go_to_premium,
              cancelButtonText: S.of(context).cancel);
        },
        barrierColor: Colors.black54,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 100),
        transitionBuilder: (context, anim1, anim2, child) {
          return child;
        });
  }
}

class ShowTimeLeft extends StatefulWidget{
  const ShowTimeLeft({Key? key}): super(key: key);

  @override
  State createState() => _ShowTimeLeftState();
}

class _ShowTimeLeftState extends State<ShowTimeLeft>{
  String _showTimeLeft = '00:00:00';

  void onRefresh(String _time) {
    setState(() {
      _showTimeLeft = _time;
    });
  }

  @override
  Widget build(BuildContext context) {
    return FittedBox(fit: BoxFit.scaleDown ,child: Text(_showTimeLeft, style: TextStyle(color: Colors.black, fontSize: 16.sp),),);
  }
}