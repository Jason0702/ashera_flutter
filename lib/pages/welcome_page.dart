import 'dart:io';

import 'package:ashera_flutter/app.dart';
import 'package:ashera_flutter/provider/packages_info_provider.dart';
import 'package:ashera_flutter/utils/app_color.dart';
import 'package:ashera_flutter/utils/app_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:url_launcher/url_launcher.dart';

import '../dialog/visit_ask_cancel_dialog.dart';
import '../generated/l10n.dart';
import '../provider/util_api_provider.dart';
import '../routers/route_name.dart';
import '../utils/app_size.dart';
import '../utils/util.dart';

//註冊與登入選擇畫面
class WelcomePage extends StatefulWidget {
  const WelcomePage({Key? key}) : super(key: key);

  @override
  State createState() => _WelcomePageState();
}

class _WelcomePageState extends State<WelcomePage> {
  UtilApiProvider? _utilApiProvider;
  PackagesInfoProvider? _packagesInfoProvider;

  _onLayoutDone(_) {
    debugPrint('註冊與登入選擇畫面');
    Future.delayed(const Duration(milliseconds: 1000), _upDataApp);
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
    if (Platform.isIOS) {
      SystemChrome.setEnabledSystemUIMode(SystemUiMode.edgeToEdge,
          overlays: []);
      SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
        statusBarColor: Colors.black,
        systemNavigationBarColor: Colors.black,
        systemNavigationBarIconBrightness: Brightness.light,
        statusBarIconBrightness: Brightness.light,
        statusBarBrightness: Brightness.light,));
    }
  }

  @override
  Widget build(BuildContext context) {
    _utilApiProvider = Provider.of<UtilApiProvider>(context);
    _packagesInfoProvider = Provider.of<PackagesInfoProvider>(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.white,
      body: Container(
        decoration: const BoxDecoration(color: AppColor.appBackgroundColor),
        width: Device.boxConstraints.maxWidth,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            //AppIcon與名稱
            Flexible(flex: 2, child: _appNameAndIcon()),
            //中間圖像
            Expanded(flex: 3, child: _centerImage()),
            //底部按鈕
            Expanded(flex: 2, child: _bottomButton()),
          ],
        ),
      ),
    );
  }

  //region AppIcon與名稱
  Widget _appNameAndIcon() {
    return Image(
      height: 15.h,
      filterQuality: FilterQuality.medium,
      image: AssetImage(AppImage.logo),
    );
  }
  //endregion

  //region 中間圖像
  Widget _centerImage() {
    return Image(
      height: 20.h,
      filterQuality: FilterQuality.high,
      image: AssetImage(AppImage.imgLogin),
    );
  }
  //endregion

  //region 底部按鈕
  Widget _bottomButton() {
    return Column(
      children: [
        //登入按鈕
        GestureDetector(
          onTap: () => _loginOnTap(),
          child: Container(
            height: AppSize.buttonH,
            width: AppSize.buttonW,
            alignment: Alignment.center,
            padding: const EdgeInsets.all(5),
            margin: const EdgeInsets.all(10),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(AppSize.buttonCircular),
                gradient: AppColor.appMainColor),
            child: FittedBox(
              child: Text(
                S.of(context).login,
                style: TextStyle(
                    color: AppColor.buttonTextColor,
                    fontSize: 18.sp,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
        ),
        //註冊按鈕
        GestureDetector(
          onTap: () => _registerOnTap(),
          child: Container(
            height: AppSize.buttonH,
            width: AppSize.buttonW,
            alignment: Alignment.center,
            padding: const EdgeInsets.all(5),
            margin: const EdgeInsets.all(10),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(AppSize.buttonCircular),
                border: Border.all(color: AppColor.buttonFrameColor, width: 2)),
            child: FittedBox(
              child: Text(
                S.of(context).register,
                style: TextStyle(
                    color: AppColor.buttonFrameColor,
                    fontSize: 18.sp,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
        )
      ],
    );
  }

  void _loginOnTap() {
    debugPrint('進入登入');
    delegate.push(name: RouteName.loginPage);
  }

  void _registerOnTap() {
    debugPrint('進入註冊');
    delegate.push(name: RouteName.registerPage);
  }
  //endregion

  ///判斷是否有新版本
  void _upDataApp(){
    if(_utilApiProvider!.systemSetting == null){
      Future.delayed(const Duration(milliseconds: 1000), _upDataApp);
      return;
    }
    if (_utilApiProvider!.systemSetting!.appVersionNumber!.isNotEmpty) {
      //判斷是否有商店
      if (Platform.isAndroid) {
        // <- 安卓
        if (_utilApiProvider!.systemSetting!.playStoreAppUrl!.isNotEmpty) {
          if(_versionVerify()){
            //跳出更新顯示
            _showUpDataAppDialog(url: _utilApiProvider!.systemSetting!.playStoreAppUrl!);
          }
        }
      } else if (Platform.isIOS) {
        // <- IOS
        if (_utilApiProvider!.systemSetting!.appleStoreAppUrl!.isNotEmpty) {
          if(_versionVerify()){
            //跳出更新顯示
            _showUpDataAppDialog(
                url: _utilApiProvider!.systemSetting!.appleStoreAppUrl!);
          }

        }
      }
    }
  }

  bool _versionVerify(){
    String _localVersion = _packagesInfoProvider!.version;
    String _storeVersion = _utilApiProvider!.systemSetting!.appVersionNumber!;
    if(Util().getAppVersion(version: _localVersion) < Util().getAppVersion(version: _storeVersion)){
      return true;
    }
    return false;
  }

  ///顯示更新提示
  void _showUpDataAppDialog({required String url}) async {
    bool? _result = await showGeneralDialog(
      context: context,
      pageBuilder: (context, anim1, anim2) {
        return VisitAskCancelDialog(
          key: UniqueKey(),
          title: S.of(context).app_update,
          content: S.of(context).new_version,
          confirmButtonText: S.of(context).renew,
          haveClose: true,
        );
      },
        barrierColor: Colors.black54,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 100),
        transitionBuilder: (context, anim1, anim2, child) {
          return Transform.scale(
            scale: anim1.value,
            child: child,
          );
        }
    );
    if (_result != null) {
      if (_result) {
        // <- 要更新跳轉
        _launchAppStore(url: url);
      }
    }
  }

  ///跳轉商店
  void _launchAppStore({required String url}) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch appStoreLink';
    }
  }
}
