import 'package:ashera_flutter/app.dart';
import 'package:ashera_flutter/provider/member_provider.dart';
import 'package:ashera_flutter/utils/app_color.dart';
import 'package:ashera_flutter/utils/no_shadow_scroll_behavior.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../dialog/agreed_dialog.dart';
import '../enum/app_enum.dart';
import '../generated/l10n.dart';
import '../provider/auth_provider.dart';
import '../provider/member_black_list_provider.dart';
import '../provider/stomp_client_provider.dart';
import '../routers/route_name.dart';
import '../utils/api.dart';
import '../utils/app_image.dart';
import '../utils/app_size.dart';
import '../utils/shared_preference.dart';
import '../utils/util.dart';
import '../widget/titlebar_widget.dart';

//登入頁
class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  State createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  SharedPreferenceUtil sharedPreferenceUtil = SharedPreferenceUtil();
  //表單驗證
  final _formKey = GlobalKey<FormState>();
  //驗證狀態管理
  AuthProvider? _authProvider;
  MemberProvider? _memberProvider;
  StompClientProvider? _stompClientProvider;
  MemberBlackListProvider? _memberBlackListProvider;

  final TextEditingController _account = TextEditingController();
  final TextEditingController _password = TextEditingController();
  //是否顯示密碼
  bool isShowPassword = false;
  //是否同意條款
  bool isAgreedTerms = false;

  // 點擊控制密碼是否顯示
  void showPassWord() {
    setState(() {
      isShowPassword = !isShowPassword;
    });
  }

  _onLayoutDone(_) async {
    debugPrint('登入頁');
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _authProvider = Provider.of<AuthProvider>(context);
    _memberProvider = Provider.of<MemberProvider>(context);
    _stompClientProvider = Provider.of<StompClientProvider>(context);
    _memberBlackListProvider = Provider.of<MemberBlackListProvider>(context);
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: AppColor.appBackgroundColor,
      body: Column(
        children: [
          //返回 與 標題
          titleBar(S.of(context).login),
          //內容
          Expanded(flex: 10, child: _body())
        ],
      ),
    );
  }

  //內容
  Widget _body() {
    return Container(
      decoration: const BoxDecoration(
        color: Colors.transparent,
      ),
      child: ScrollConfiguration(
          behavior: NoShadowScrollBehavior(),
          child: SingleChildScrollView(
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  SizedBox(
                    height: 3.h,
                  ),
                  //AppIcon與名稱
                  _appNameAndIcon(),
                  //輸入框
                  _input(),
                  //條約與忘記密碼
                  _termsAndForgetPassword(),
                  //登入按鈕
                  SizedBox(
                    height: 4.h,
                  ),
                  _loginButton(),
                ],
              ),
            ),
          )),
    );
  }

  //region AppIcon與名稱
  Widget _appNameAndIcon() {
    return Image(
      height: 15.h,
      filterQuality: FilterQuality.medium,
      image: AssetImage(
        AppImage.logo,
      ),
    );
  }
  //endregion

  //region 輸入框
  Widget _input() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [_accountTextField(), _passwordTextField()],
    );
  }

  //帳號
  Widget _accountTextField() {
    return SizedBox(
      width: 80.w,
      height: 10.h,
      child: TextFormField(
        style: const TextStyle(color: Colors.black),
        decoration: InputDecoration(
          fillColor: Colors.transparent,
          filled: true,
          hintStyle: const TextStyle(color: Colors.grey),
          hintText: S.of(context).enter_a_phone_number,
        ),
        keyboardType: TextInputType.number,
        textInputAction: TextInputAction.next,
        controller: _account,
        validator: (value){
          if(value == null || value.isEmpty){
            return S.of(context).enter_a_phone_number;
          } else if(!Util().phoneVerification(value)){
            return S.of(context).mobile_number_entered_incorrectly;
          }
          return null;
        },
      ),
    );
  }

  //密碼
  Widget _passwordTextField() {
    return SizedBox(
      width: 80.w,
      height: 10.h,
      child: TextFormField(
        style: const TextStyle(color: Colors.black),
        decoration: InputDecoration(
          suffixIcon: Container(
            padding: const EdgeInsets.only(right: 8.0),
            child: IconButton(
              icon: Icon(
                isShowPassword ? Icons.visibility : Icons.visibility_off,
                color: Colors.grey[600],
                size: 25,
              ),
              onPressed: () {
                setState(() {
                  isShowPassword = !isShowPassword;
                });
              },
            ),
          ),
          fillColor: Colors.transparent,
          filled: true,
          hintStyle: const TextStyle(color: Colors.grey),
          hintText: S.of(context).enter_password,
        ),
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.done,
        controller: _password,
        obscureText: !isShowPassword,
        validator: (value){
          if(value == null || value.isEmpty){
            return S.of(context).enter_password;
          }else if(value.trim().length < 6 || value.trim().length > 12){
            return S.of(context).enter_alphanumeric_characters;
          }
          return null;
        },
      ),
    );
  }

  //endregion

  //region 條約與忘記密碼
  Widget _termsAndForgetPassword() {
    return SizedBox(
      width: 80.w,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          SizedBox(
            height: 3.h,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(
                  width: 6.w,
                  child: Checkbox(
                      fillColor: MaterialStateProperty.resolveWith(getColor),
                      value: isAgreedTerms,
                      onChanged: (value) {
                        setState(() {
                          isAgreedTerms = value!;
                        });
                      }),
                ),
                GestureDetector(
                  onTap: () => _agreedOnTap(),
                  child: FittedBox(
                    child: Text(
                      S.of(context).agreed_terms,
                      style: TextStyle(color: Colors.grey, fontSize: 16.sp),
                    ),
                  ),
                )
              ],
            ),
          ),
          GestureDetector(
            onTap: () => _forgetPasswordOnTap(),
            child: FittedBox(
              child: Text(
                '${S.of(context).forgot_password}?',
                style: TextStyle(
                    color: AppColor.buttonFrameColor, fontSize: 16.sp),
              ),
            ),
          )
        ],
      ),
    );
  }

  Color getColor(Set<MaterialState> states) {
    if (isAgreedTerms) {
      return Colors.red;
    } else {
      return Colors.black;
    }
  }

  void _forgetPasswordOnTap() {
    delegate.push(name: RouteName.forgetPasswordPage);
  }

  void _agreedOnTap(){
    showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return AgreedDialog(key: UniqueKey(),);
        },
        barrierColor: Colors.black54,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 100),
        transitionBuilder: (context, anim1, anim2, child) {
          return Transform.scale(
            scale: anim1.value,
            child: child,
          );
        });
  }

  //endregion

  //region 登入按鈕
  Widget _loginButton() {
    return GestureDetector(
      onTap: () => _loginOnTap(),
      child: Container(
        width: AppSize.buttonW,
        height: AppSize.buttonH,
        alignment: Alignment.center,
        padding: const EdgeInsets.all(5),
        margin: const EdgeInsets.all(10),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(AppSize.buttonCircular),
            gradient: AppColor.appMainColor),
        child: FittedBox(
          child: Text(
            S.of(context).login,
            style: TextStyle(
                color: AppColor.buttonTextColor,
                fontSize: 18.sp,
                fontWeight: FontWeight.bold),
          ),
        ),
      ),
    );
  }

  void _loginOnTap() {
    //驗證流程
    if(!_formKey.currentState!.validate()){
      if(!EasyLoading.isShow){
        EasyLoading.showToast(S.of(context).information_is_incomplete);
      }
    }else if(!isAgreedTerms){
      if(!EasyLoading.isShow){
        EasyLoading.showToast(S.of(context).agree_to_the_user_agreement);
      }
    } else {
      if(_authProvider!.loggedInStatus != AuthStatus.authenticating){
        EasyLoading.show(status: 'Loading...');
        _authProvider!.login(_account.text, _password.text).then((value) {
          if(value['status']){
            _stompClientProvider!.onCreation(_account.text, _password.text);//STOMP建立與連線
            EasyLoading.showSuccess(S.of(context).login_success);
            sharedPreferenceUtil.saveAccountAndPassword(_account.text, _password.text);
            Api.accessToken = value['user'].token;
            sharedPreferenceUtil.saveAccessToken(value['user'].token);
            sharedPreferenceUtil.saveMemberId(value['user'].id);
            sharedPreferenceUtil.saveRefreshTime();
            _memberProvider!.getMember(value['user'].id).then((status) {
              if(status){
                //跳轉
                delegate.replace(name: RouteName.appBarPage);
                //取得我封鎖了誰
                _memberBlackListProvider!.getMemberBlackListByFromMemberId(id: value['user'].id);
              }else{
                //取得失敗

              }
            });
          }else{
            EasyLoading.dismiss();
            EasyLoading.showToast(value['message'].toString().substring(3));
          }
        });
      }
    }
  }
  //endregion
}
