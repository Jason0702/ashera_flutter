import 'dart:developer';

import 'package:ashera_flutter/utils/app_image.dart';
import 'package:ashera_flutter/widget/titlebar_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../app.dart';
import '../generated/l10n.dart';
import '../utils/app_color.dart';
import 'package:flutter_dynamic_icon/flutter_dynamic_icon.dart';

class DynamicAppIconPage extends StatefulWidget {
  const DynamicAppIconPage({Key? key}) : super(key: key);

  @override
  State createState() => _DynamicAppIconPageState();
}

class _DynamicAppIconPageState extends State<DynamicAppIconPage> {
  String iconName = '';

  _onLayoutDone(_) {}

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  void dispose() {
    super.dispose();
  }

  //返回
  Widget _back() {
    return GestureDetector(
      onTap: () => {delegate.popRoute()},
      child: const Icon(
        Icons.arrow_back,
        color: AppColor.appTitleBarTextColor,
        size: 30,
      ),
    );
  }

  //確認
  Widget _confirm() {
    return GestureDetector(
      onTap: () => _confirmOnTap(),
      child: Text(
        S.of(context).confirm,
        style: const TextStyle(
            color: Colors.black, fontSize: 18, fontWeight: FontWeight.bold),
      ),
    );
  }

  void _confirmOnTap() async {
    try {
      if (await FlutterDynamicIcon.supportsAlternateIcons) {
        await FlutterDynamicIcon.setAlternateIconName(iconName);
        //log("App icon change successful");
        return;
      }
    } on PlatformException {
    } catch (e) {}
    //log("Failed to change app icon");
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: AppColor.appBackgroundColor,
      body: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          //標題
          titleBarIntegrate([_back()], S.of(context).change_icon, [_confirm()]),
          Expanded(
              child: Column(
            children: [
              //圖標設置
              Container(
                margin: const EdgeInsets.all(10),
                child: Text(S.of(context).change_icon),
              ),
              Row(
                children: [
                  Expanded(
                      child: GestureDetector(
                    onTap: () {
                      iconName = 'former';
                    },
                    child: Image(
                      image: AssetImage(AppImage.iconAshera),
                    ),
                  )),
                  Expanded(
                      child: GestureDetector(
                    onTap: () {
                      iconName = 'novel';
                    },
                    child: Image(
                      image: AssetImage(AppImage.iconNovel),
                    ),
                  )),
                  Expanded(
                    child: GestureDetector(
                        onTap: () {
                          iconName = 'twnews';
                        },
                        child: Image(
                          image: AssetImage(AppImage.iconTwNews),
                        )),
                  )
                ],
              )
            ],
          ))
        ],
      ),
    );
  }
}
