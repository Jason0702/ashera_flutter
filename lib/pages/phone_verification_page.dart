import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../app.dart';
import '../enum/app_enum.dart';
import '../generated/l10n.dart';
import '../models/member_model.dart';
import '../provider/auth_provider.dart';
import '../provider/member_provider.dart';
import '../provider/stomp_client_provider.dart';
import '../routers/route_name.dart';
import '../utils/api.dart';
import '../utils/app_color.dart';
import '../utils/app_image.dart';
import '../utils/app_size.dart';
import '../utils/no_shadow_scroll_behavior.dart';
import '../utils/shared_preference.dart';
import '../utils/util.dart';
/*
* 08/04
* */

class PhoneVerificationPage extends StatefulWidget{
  const PhoneVerificationPage({Key? key}): super(key: key);

  @override
  State createState() => _PhoneVerificationPageState();
}
class _PhoneVerificationPageState extends State<PhoneVerificationPage>{
  SharedPreferenceUtil sharedPreferenceUtil = SharedPreferenceUtil();

  //驗證狀態管理
  AuthProvider? _authProvider;
  MemberProvider? _memberProvider;
  StompClientProvider? _stompClientProvider;

  final TextEditingController _account = TextEditingController();
  final TextEditingController _verification = TextEditingController();

  //是否發送驗證碼
  bool isSendVerification = false;
  //發送倒數秒數
  Timer? _timer;
  int _countdownTime = 0;

  late MemberRegisterDTO _model;

  _onLayoutDone(_){
    _account.text = _model.cellphone;
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  Widget build(BuildContext context) {
    _authProvider = Provider.of<AuthProvider>(context);
    _stompClientProvider = Provider.of<StompClientProvider>(context);
    _memberProvider = Provider.of<MemberProvider>(context);
    _model = ModalRoute.of(context)!.settings.arguments as MemberRegisterDTO;
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: AppColor.appBackgroundColor,
      body: Column(
        children: [
          //返回 與 標題
          _titleBar(S.of(context).phone_verification),
          //內容
          Expanded(flex: 10, child: _body()),
          //註冊
          _registerButton(),
        ],
      ),
    );
  }

  //返回 與 標題
  Widget _titleBar(String _title) {
    return Container(
      height: AppSize.titleBarH,
      width: Device.boxConstraints.maxWidth,
      decoration: const BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
              color: Colors.grey,
              offset: Offset(0.0, 0.5), //陰影y軸偏移量
              blurRadius: 1, //陰影模糊程度
              spreadRadius: 1 //陰影擴散程度
          )
        ],
      ),
      child: Stack(
        alignment: Alignment.center,
        children: [
          //標題文字
          FittedBox(
            child: Text(
              _title,
              style: TextStyle(
                  color: AppColor.appTitleBarTextColor,
                  fontSize: 21.sp,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ],
      ),
    );
  }

  //內容
  Widget _body() {
    return Container(
      decoration: const BoxDecoration(
        color: Colors.transparent,
      ),
      child: ScrollConfiguration(
          behavior: NoShadowScrollBehavior(),
          child: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                SizedBox(height: 3.h,),
                //AppIcon與名稱
                _appNameAndIcon(),
                SizedBox(height: 10.h,),
                //手機號碼
                _accountTextField(),
                //簡訊驗證
                _smsTextField(),
              ],
            ),
          )),
    );
  }

  Widget _appNameAndIcon() {
    return Image(
      height: 15.h,
      filterQuality: FilterQuality.medium,
      image: AssetImage(AppImage.logo),
    );
  }

  //手機號碼
  Widget _accountTextField() {
    return SizedBox(
      width: 80.w,
      height: 8.h,
      child: TextFormField(
        readOnly: true,
        onTap: (){
          EasyLoading.showToast(S.of(context).need_to_change);
        },
        style: const TextStyle(color: Colors.black),
        decoration: InputDecoration(
          fillColor: Colors.transparent,
          filled: true,
          hintStyle: const TextStyle(color: Colors.grey),
          hintText: S.of(context).enter_a_phone_number,
        ),
        keyboardType: TextInputType.number,
        textInputAction: TextInputAction.next,
        controller: _account,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return S.of(context).enter_a_phone_number;
          } else if (!Util().phoneVerification(value)) {
            return S.of(context).mobile_number_entered_incorrectly;
          }
          return null;
        },
      ),
    );
  }
  //簡訊驗證
  Widget _smsTextField(){
    return SizedBox(
      width: 80.w,
      height: 8.h,
      child: Stack(
        alignment: Alignment.center,
        children: [
          TextFormField(
            style: const TextStyle(color: Colors.black),
            decoration: InputDecoration(
              fillColor: Colors.transparent,
              filled: true,
              hintStyle: const TextStyle(color: Colors.grey),
              hintText: S.of(context).enter_confirmation_code,
            ),
            keyboardType: TextInputType.number,
            textInputAction: TextInputAction.next,
            controller: _verification,
            validator: (value){
              if(value == null || value.isEmpty){
                return S.of(context).enter_confirmation_code;
              }
              return null;
            },
          ),
          Positioned(right: 0, child: _sendSms())
        ],
      ),
    );
  }

  Widget _sendSms(){
    return GestureDetector(
      onTap: () => _sendSmsOnTap(),
      child: Container(
        height: 5.h,
        width: 25.w,
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: AppColor.yellowButton,
            borderRadius: BorderRadius.circular(30)
        ),
        child: Text(
          isSendVerification
              ? '$_countdownTime s'
              : S.of(context).send_verification_code,
          style: TextStyle(color: AppColor.buttonTextColor, fontSize: 16.sp),
        ),
      ),
    );
  }

  void _sendSmsOnTap(){
    if(_account.text.trim().isNotEmpty){
      _reciprocal();
      _authProvider!.verificationCode(number: _account.text.trim()).then((value) {
        if(value){
          EasyLoading.showSuccess(S.of(context).send);
        }else{
          EasyLoading.showError(S.of(context).fail);
        }
      });
    } else {
      //帳號為空
      EasyLoading.showToast(S.of(context).enter_a_phone_number);
    }
  }

  void _reciprocal() {
    if(_countdownTime == 0 && !isSendVerification){
      _countdownTime = 60;
      isSendVerification = true;
      setState(() {});
      _startCountdownTimer();
    }
  }

  void _startCountdownTimer(){
    _timer = Timer.periodic(const Duration(milliseconds: 1000), (timer) {
      if(mounted){
        setState(() {
          if(_countdownTime < 1){
            _timer!.cancel();
            _timer = null;
            isSendVerification = false;
          }else{
            _countdownTime--;
          }
        });
      }
    });
  }

  //註冊按鈕
  Widget _registerButton(){
    return Container(
      margin: const EdgeInsets.only(left: 20, right: 20),
      child: Row(
        children: [
          //註冊
          Expanded(child: GestureDetector(
            onTap: () => _registerOnTap(),
            child: Container(
              height: AppSize.buttonH,
              width: AppSize.buttonW,
              alignment: Alignment.center,
              padding: const EdgeInsets.all(5),
              margin: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(AppSize.buttonCircular),
                  gradient: AppColor.appMainColor),
              child: FittedBox(
                child: Text(
                  S.of(context).next_step,
                  style: TextStyle(
                      color: AppColor.buttonTextColor,
                      fontSize: 18.sp,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          )),
          //取消
          Expanded(child: GestureDetector(
            onTap: () => delegate.popRoute(),
            child: Container(
              height: AppSize.buttonH,
              width: AppSize.buttonW,
              alignment: Alignment.center,
              padding: const EdgeInsets.all(5),
              margin: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(AppSize.buttonCircular),
                  border: Border.all(color: AppColor.buttonFrameColor, width: 2)),
              child: FittedBox(
                child: Text(
                  S.of(context).cancel,
                  style: TextStyle(
                      color: AppColor.buttonFrameColor,
                      fontSize: 18.sp,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ))
        ],
      ),
    );
  }

  void _registerOnTap() {
      //註冊
      if (_authProvider!.registeredInStatus != AuthStatus.authenticating) {
        EasyLoading.show(status: 'Loading...');
        _authProvider!
            .register(_model.nickname, _account.text, _model.password.trim(), _model.gender, _model.birthday, _verification.text.trim(), _model.zodiacSign)
            .then((value) {
          if (value['status']) {
            /*//判斷有沒有填人臉
            if (_localdetectPic.isNotEmpty) {
              EasyLoading.show(status: S.of(context).verifying);
              MemberModel _member = value['message'];
              Future.delayed(const Duration(milliseconds: 3000), (){
                _apiProvider!.setMemberFaceImage(_member.id!, _detectname).then((value) {
                  if(value){//有成功
                    _apiProvider!.putReview(id: _member.id!).then((value){
                      EasyLoading.showToast(S.of(context).finish);
                      //delegate.popRoute();//回到最前面
                      //自動登入
                      _login();
                    });
                  }else{//沒成功
                    EasyLoading.showToast(S.of(context).fail);
                    _showNoticeDialog();
                  }
                });
              });
            } else {
              _showNoticeDialog();
            }*/
            _login();
          } else {
            EasyLoading.dismiss();
            EasyLoading.showError(value['message']);
          }
        });
      }
  }

  void _login(){
    _authProvider!.login(_account.text, _model.password).then((value) {
      if(value['status']){
        _stompClientProvider!.onCreation(_account.text, _model.password);//STOMP建立與連線
        EasyLoading.showSuccess(S.of(context).registration_success);
        sharedPreferenceUtil.saveAccountAndPassword(_account.text, _model.password);
        Api.accessToken = value['user'].token;
        sharedPreferenceUtil.saveAccessToken(value['user'].token);
        sharedPreferenceUtil.saveMemberId(value['user'].id);
        sharedPreferenceUtil.saveRefreshTime();
        _memberProvider!.getMember(value['user'].id).then((value) {
          if(value){
            //跳轉
            delegate.push(name: RouteName.faceRegisterPage);
          }else{
            //取得失敗

          }
        });
      }else{
        EasyLoading.dismiss();
        EasyLoading.showToast(value['message'].toString().substring(3));
      }
    });
  }
}