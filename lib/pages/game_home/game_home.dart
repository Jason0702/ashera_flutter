import 'dart:developer';
import 'dart:io';

import 'package:ashera_flutter/dialog/game_point_insufficient_dialog.dart';
import 'package:ashera_flutter/dialog/game_shopping_dialog.dart';
import 'package:ashera_flutter/dialog/message_card_dialog.dart';
import 'package:ashera_flutter/models/game_dto.dart';
import 'package:ashera_flutter/models/member_model.dart';
import 'package:ashera_flutter/provider/game_home_provider.dart';
import 'package:ashera_flutter/provider/game_shopping_refresh_provider.dart';
import 'package:ashera_flutter/provider/game_street_provider.dart';
import 'package:ashera_flutter/provider/member_provider.dart';
import 'package:ashera_flutter/provider/stomp_client_provider.dart';
import 'package:ashera_flutter/provider/util_api_provider.dart';
import 'package:ashera_flutter/widget/mugshot_widget.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../app.dart';
import '../../dialog/pay_recover_times_dialog.dart';
import '../../dialog/pig_grows_dialog.dart';
import '../../dialog/purchases_dialog.dart';
import '../../dialog/setting_game_dialog.dart';
import '../../enum/app_enum.dart';
import '../../enum/app_position_enum.dart';
import '../../generated/l10n.dart';
import '../../models/dung_model.dart';
import '../../models/game_model.dart';
import '../../provider/position_provider.dart';
import '../../routers/route_name.dart';
import '../../utils/app_color.dart';
import '../../utils/app_image.dart';
import '../../utils/shared_preference.dart';
import '../../utils/util.dart';
import '../../widget/avatar_image.dart';
import '../../widget/gender_icon.dart';
import '../../widget/shopping_item_card_widget.dart';

class GameHome extends StatefulWidget {
  const GameHome({Key? key}) : super(key: key);

  @override
  _GameHomeState createState() => _GameHomeState();
}

class _GameHomeState extends State<GameHome> {
  //遊戲狀態管理
  GameHomeProvider? _gameHomeProvider;
  GameStreetProvider? _gameStreetProvider;
  GameShoppingRefreshProvider? _gameShoppingRefreshProvider;
  //STOMP
  StompClientProvider? _stompClientProvider;
  //Member
  MemberProvider? _memberProvider;
  UtilApiProvider? _utilApiProvider;
  PositionProvider? _positionProvider;

  //菜單高度
  final double _menuHeight = 15.h;

  //底部功能列按鈕寬度
  static final double _bottomWidth = 20.w;

  //左側功能列按鈕寬度
  static final double _leftWidth = 14.w;

  //家具高度
  final double _screenHeight = Device.height - 18.5.h;

  //編輯模式各區塊icon寬度
  final double _editIconWidth = 15.w;

  //視角操控器
  TransformationController transformationController =
      TransformationController();

  _onLayoutDone(_) async {
    _positionProvider!.setAppPosition(position: AppPosition.gameHome);
    await Future.wait([
      //用memberId取得會員家留言
      _gameHomeProvider!.getMemberHouseMessage(
          id: _gameHomeProvider!.memberHouse
              .where((element) => element.used == true)
              .first
              .id!),
      //用memberId取得會員家大便
      _gameHomeProvider!.getMemberHouseDung(
          id: _gameHomeProvider!.memberHouse
              .where((element) => element.used == true)
              .first
              .id!),
    ]);
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    EasyLoading.instance.maskColor = Colors.black;
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _positionProvider = Provider.of<PositionProvider>(context, listen: false);
    _stompClientProvider =
        Provider.of<StompClientProvider>(context, listen: false);
    _memberProvider = Provider.of<MemberProvider>(context, listen: false);
    _gameHomeProvider = Provider.of<GameHomeProvider>(context, listen: false);
    _utilApiProvider = Provider.of<UtilApiProvider>(context, listen: false);
    _gameStreetProvider =
        Provider.of<GameStreetProvider>(context, listen: false);
    _gameShoppingRefreshProvider =
        Provider.of<GameShoppingRefreshProvider>(context, listen: false);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: AppColor.appBackgroundColor,
      body: Stack(
        fit: StackFit.loose,
        children: [
          _yellowBackGround(),
          //街道
          Positioned(bottom: 0, child: _menuBackGround()),
          //家具內擺設
          _interior(),
          //底部功能按鈕
          Positioned(bottom: 0, child: _bottomBar()),
          //會員資訊
          Positioned(
            top: 0,
            left: 0,
            child: Consumer2<MemberProvider, GameHomeProvider>(
              builder: (context, member, game, _) {
                return _memberCard(
                    member: member.memberModel,
                    point: member.memberPoint.point!,
                    clean: member.memberPoint
                        .cleanTimes! /*game.memberCleaningTools.length*/,
                    dung: member
                        .memberPoint.shitTimes! /*game.memberDung.length*/);
              },
            ),
          ),
          //左側功能按鈕
          Positioned(left: 3.w, top: 18.h, child: _leftBar()),
          //編輯模式返回按鈕
          Positioned(right: 20, top: 10, child: _leaveEditMode()),
          //_messagePage(),
          Positioned(bottom: 0, child: mallWidget()),
        ],
      ),
    );
  }

  //編輯模式返回按鈕
  Widget _leaveEditMode() {
    return Consumer<GameHomeProvider>(
      builder: (context, home, _) {
        return Offstage(
          offstage: !home.editMode,
          child: GestureDetector(
            onTap: () {
              //關閉編輯模式
              home.setEditMode(value: false);
            },
            child: SizedBox(
              width: 15.w,
              child: Image.asset(AppImage.btnMallBack),
            ),
          ),
        );
      },
    );
  }

  //底部功能列按鈕
  Widget _bottomBar() {
    return Container(
      width: Device.width,
      padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 2.h),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          //房子
          GestureDetector(
            child: SizedBox(
              width: _bottomWidth,
              child: Image.asset(AppImage.btnHomeSelected),
            ),
          ),
          //街道
          GestureDetector(
            onTap: () => _streetOnTap(),
            child: SizedBox(
              width: _bottomWidth,
              child: Image.asset(AppImage.btnStreet),
            ),
          ),
          //設定
          GestureDetector(
            onTap: () => _showSettingDialog(),
            child: SizedBox(
              width: _bottomWidth,
              child: Image.asset(AppImage.btnSettings),
            ),
          ),
        ],
      ),
    );
  }

  //街道按鈕OnTap
  void _streetOnTap() {
    _positionProvider!.setAppPosition(position: AppPosition.game);
    delegate.popRoute();
  }

  //設定
  void _showSettingDialog() async {
    bool? result = await showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return SettingGameDialog(
            key: UniqueKey(),
          );
        });
    if (result != null) {
      if (result) {
        _gameStreetProvider!
            .setMemberSetting(id: _memberProvider!.memberModel.id!)
            .then((value) {
          _gameStreetProvider!.getNearMe(
              id: _memberProvider!.memberModel.id!,
              member: _memberProvider!.memberModelMe);
        });
      }
    }
  }

  //選單背景
  Widget _menuBackGround() {
    return SizedBox(
      height: _menuHeight,
      width: Device.width,
      child: Image(
        fit: BoxFit.fill,
        filterQuality: FilterQuality.medium,
        image: AssetImage(AppImage.imgBgMenu),
      ),
    );
  }

  //黃色背景
  Widget _yellowBackGround() {
    return SizedBox(
      width: Device.width,
      height: Device.height,
      child: Image(
        fit: BoxFit.fill,
        filterQuality: FilterQuality.medium,
        image: AssetImage(AppImage.imgBgHome),
      ),
    );
  }

  //region 自己人物
  ///會員資訊
  /// * [member] 大頭照
  /// * [clean]  清潔次數
  /// * [dung]  大便次數
  Widget _memberCard(
      {required member,
      required int point,
      required int clean,
      required int dung}) {
    return SizedBox(
      height: 15.h,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              //大頭照
              Container(
                  height: 9.h,
                  margin: EdgeInsets.only(left: 3.w),
                  alignment: Alignment.center,
                  decoration: const BoxDecoration(color: Colors.transparent),
                  child: meAvatarImage(member)),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  //個人資料
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 1.h),
                    child: Row(
                      children: [
                        //名稱
                        Container(
                          child: FittedBox(
                            child: Text(
                              member.nickname!,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18.sp,
                                  color: Colors.black),
                            ),
                          ),
                          margin: const EdgeInsets.only(left: 10),
                        ),
                        //性別年齡
                        Container(
                          margin: const EdgeInsets.only(left: 10),
                          child: Row(
                            children: [
                              getGender(member.gender!),
                              //年齡
                              Container(
                                margin: const EdgeInsets.only(left: 5),
                                child: Text(
                                  Util().getAge(member.birthday!),
                                  style: const TextStyle(
                                      color: Colors.grey, fontSize: 18),
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 1.w),
                      child: _pointWidget(point: point))
                ],
              ),
            ],
          ),
          SizedBox(
            height: 1.5.h,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              //清潔次數
              _cleanWidget(frequency: clean),
              //大便次數
              _dongWidget(frequency: dung),
            ],
          )
        ],
      ),
    );
  }

  //點數
  Widget _pointWidget({required int point}) {
    return Stack(
      children: [
        Container(
          margin: EdgeInsets.only(left: 2.8.w),
          padding: const EdgeInsets.all(2),
          width: 26.w,
          height: 3.7.h,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(30),
              border: Border.all(color: AppColor.pinkRedColor, width: 2)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              //空格
              SizedBox(
                width: 5.w,
              ),
              //數字
              Expanded(
                child: FittedBox(
                  child: Text(
                    '$point',
                    style: const TextStyle(
                        color: Colors.black, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              //+ icon
              if (_utilApiProvider!.systemSetting!.addPointsStatus !=
                  PointsStatus.closure.index)
                GestureDetector(
                  onTap: () => delegate.push(name: RouteName.myPointsPage),
                  child: SizedBox(child: Image.asset(AppImage.iconTopUp)),
                )
            ],
          ),
        ),
        //鑽石
        Positioned(
          left: 0,
          top: -0.2.h,
          child:
              SizedBox(height: 4.h, child: Image.asset(AppImage.iconDiamond)),
        ),
      ],
    );
  }

  ///清潔次數
  /// * [frequency] 次數
  Widget _cleanWidget({required int frequency}) {
    return Container(
      margin: EdgeInsets.only(left: 2.8.w),
      padding: const EdgeInsets.all(2),
      width: 22.w,
      height: 3.8.h,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(30),
          border: Border.all(color: AppColor.pinkRedColor, width: 2)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          //掃把
          Image(
            image: AssetImage(AppImage.iconClean),
          ),
          //數字
          Expanded(
            child: Consumer<UtilApiProvider>(
              builder: (context, util, _) {
                return FittedBox(
                  child: Text(
                    '$frequency/${util.gameSetting.cleanLimit}',
                    style: const TextStyle(
                        color: Colors.black, fontWeight: FontWeight.bold),
                  ),
                );
              },
            ),
          ),
          //+ icon
          GestureDetector(
            onTap: () => _addCleaningTools(),
            child: SizedBox(child: Image.asset(AppImage.iconTopUpCleanAndPoop)),
          )
        ],
      ),
    );
  }

  ///大便次數
  /// * [frequency] 次數
  Widget _dongWidget({required int frequency}) {
    return Container(
      margin: EdgeInsets.only(left: 2.8.w),
      padding: const EdgeInsets.all(2),
      width: 22.w,
      height: 3.7.h,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(30),
          border: Border.all(color: AppColor.pinkRedColor, width: 2)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          //大便
          Image(
            image: AssetImage(AppImage.iconPoop),
          ),
          //數字
          Expanded(
            child: Consumer<UtilApiProvider>(
              builder: (context, util, _) {
                return FittedBox(
                  child: Text(
                    '$frequency/${util.gameSetting.shitLimit}',
                    textAlign: TextAlign.center,
                    style: const TextStyle(
                        color: Colors.black, fontWeight: FontWeight.bold),
                  ),
                );
              },
            ),
          ),
          //+ icon
          GestureDetector(
            onTap: () => _addDong(),
            child: SizedBox(child: Image.asset(AppImage.iconTopUpCleanAndPoop)),
          )
        ],
      ),
    );
  }

  void _addDong() async {
    //恢復大便次數
    if (_memberProvider!.memberPoint.point! <
        _utilApiProvider!.gameSetting.recoverShitPoints!) {
      EasyLoading.showToast(S.of(context).please_add_value);
      return;
    }
    if(_memberProvider!.memberPoint.shitTimes! == 2){
      EasyLoading.showToast(S.of(context).the_limit_has_been_reached);
      return;
    }
    bool? result = await showGeneralDialog(
      context: context,
      pageBuilder: (context, anim1, anim2) {
        return PayRecoverTimersDialog(
          key: UniqueKey(),
          title: S.of(context).buy,
          recoverPoints: _utilApiProvider!.gameSetting.recoverShitPoints!,
          limit: _utilApiProvider!.gameSetting.shitLimit!,
          confirmButtonText: S.of(context).confirm,
          cancelButtonText: S.of(context).cancel,
        );
      },
    );
    if (result != null) {
      if (result) {
        EasyLoading.show(status: S.of(context).purchasing);
        bool r = await _gameHomeProvider!
            .postRecoverShitTimes(_memberProvider!.memberModel.id!);
        if (r) {
          await _memberProvider!
              .getMemberPoint(_memberProvider!.memberModel.id!);
          EasyLoading.dismiss();
        } else {
          EasyLoading.showError('${S.of(context).buy}${S.of(context).fail}');
        }
      }
    }
  }
  //endregion

  //region 左側功能列按鈕
  Widget _leftBar() {
    return Consumer<GameHomeProvider>(
      builder: (context, home, _) {
        return Offstage(
          offstage: home.editMode,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              //購物車
              _shoppingCart(),
              SizedBox(
                height: 2.h,
              ),
            ],
          ),
        );
      },
    );
  }

  //購物車按鈕
  Widget _shoppingCart() {
    return Consumer<GameHomeProvider>(
      builder: (context, home, _) {
        return GestureDetector(
          onTap: () {
            //開啟編輯模式
            home.setEditMode(value: true);
          },
          child: SizedBox(
            width: _leftWidth,
            child: Image.asset(AppImage.btnMall),
          ),
        );
      },
    );
  }
  //endregion

  //家具內擺設
  Widget _interior() {
    return SizedBox(
      height: _screenHeight,
      child: InteractiveViewer(
        minScale: 1,
        maxScale: 2,
        transformationController: transformationController,
        constrained: false,
        child: Stack(
          alignment: Alignment.bottomLeft,
          children: [
            //空白 撐stack用
            Container(
              height: _screenHeight,
            ),
            //背景
            _roomBg(),
            //櫃子
            Positioned(bottom: 20.h, right: 17.w, child: _cabinet()),
            //圍籬
            Positioned(bottom: 3.h, right: 13.w, child: _fence()),
            //狗屋
            Positioned(bottom: 25.h, left: 30.w, child: _doghouse()),
            //狗
            Positioned(bottom: 15.h, left: 22.w, child: _dog()),
            //椅子
            Positioned(
              bottom: 15.h,
              left: 77.w,
              child: _chair(),
            ),
            //踏墊
            Positioned(
              bottom: 2.h,
              left: 22.w,
              child: _mat(),
            ),
            //狗編輯icon
            Positioned(
                bottom: 20.h, left: 57.w, child: _editIconWidget(Interior.dog)),
            //狗屋編輯icon
            Positioned(
                bottom: 40.h,
                left: 47.w,
                child: _editIconWidget(Interior.doghouse)),
            //桌子編輯icon
            Positioned(
                bottom: 28.5.h,
                left: 115.w,
                child: _editIconWidget(Interior.table)),
            //椅子編輯icon
            Positioned(
                bottom: 26.h,
                right: 130.w,
                child: _editIconWidget(Interior.chair)),
            //圍籬編輯icon
            Positioned(
                bottom: 12.h,
                right: 32.w,
                child: _editIconWidget(Interior.fence)),
            //櫃子編輯icon
            Positioned(
                bottom: 37.h,
                right: 35.w,
                child: _editIconWidget(Interior.cabinet)),
            //背景編輯icon
            Positioned(
                bottom: 55.h,
                left: 99.w,
                child: _editIconWidget(Interior.wallpaper)),
            //踏墊編輯icon
            Positioned(
                bottom: 8.h, left: 60.w, child: _editIconWidget(Interior.mat)),
            //桌子
            Positioned(bottom: 15.5.h, left: 99.w, child: _table()),
            //便便1
            Positioned(
                bottom: 3.h,
                left: 30.w,
                child: Offstage(
                  offstage:
                      (_gameHomeProvider!.memberHouseDungModelList.isEmpty ||
                          _gameHomeProvider!.memberHouseDungModelList
                              .where((element) => element.posTop == 1)
                              .isEmpty),
                  child: poopWidget(position: 1),
                )),
            //便便2
            Positioned(
                bottom: 20.h,
                left: 55.w,
                child: Offstage(
                  offstage:
                      (_gameHomeProvider!.memberHouseDungModelList.isEmpty ||
                          _gameHomeProvider!.memberHouseDungModelList
                              .where((element) => element.posTop == 2)
                              .isEmpty),
                  child: poopWidget(position: 2),
                )),
            //便便4
            Positioned(
                bottom: 2.h,
                left: 97.w,
                child: Offstage(
                  offstage:
                      (_gameHomeProvider!.memberHouseDungModelList.isEmpty ||
                          _gameHomeProvider!.memberHouseDungModelList
                              .where((element) => element.posTop == 4)
                              .isEmpty),
                  child: poopWidget(position: 4),
                )),
            //便便5
            Positioned(
                bottom: 5.h,
                left: 130.w,
                child: Offstage(
                  offstage:
                      (_gameHomeProvider!.memberHouseDungModelList.isEmpty ||
                          _gameHomeProvider!.memberHouseDungModelList
                              .where((element) => element.posTop == 5)
                              .isEmpty),
                  child: poopWidget(position: 5),
                )),

            //便便3
            Positioned(
                bottom: 22.h,
                left: 110.w,
                child: Offstage(
                  offstage:
                      (_gameHomeProvider!.memberHouseDungModelList.isEmpty ||
                          _gameHomeProvider!.memberHouseDungModelList
                              .where((element) => element.posTop == 3)
                              .isEmpty),
                  child: poopWidget(position: 3),
                )),
            //便便6
            Positioned(
                bottom: 2.h,
                right: 120.w,
                child: Offstage(
                  offstage:
                      (_gameHomeProvider!.memberHouseDungModelList.isEmpty ||
                          _gameHomeProvider!.memberHouseDungModelList
                              .where((element) => element.posTop == 6)
                              .isEmpty),
                  child: poopWidget(position: 6),
                )),
            //便便8
            Positioned(
                bottom: 2.h,
                right: 54.w,
                child: Offstage(
                  offstage:
                      (_gameHomeProvider!.memberHouseDungModelList.isEmpty ||
                          _gameHomeProvider!.memberHouseDungModelList
                              .where((element) => element.posTop == 8)
                              .isEmpty),
                  child: poopWidget(position: 8),
                )),
            //便便7
            Positioned(
                bottom: 21.h,
                right: 65.w,
                child: Offstage(
                  offstage:
                      (_gameHomeProvider!.memberHouseDungModelList.isEmpty ||
                          _gameHomeProvider!.memberHouseDungModelList
                              .where((element) => element.posTop == 7)
                              .isEmpty),
                  child: poopWidget(position: 7),
                )),
            //便便9
            Positioned(
                bottom: 20.h,
                right: 110.w,
                child: Offstage(
                  offstage:
                      (_gameHomeProvider!.memberHouseDungModelList.isEmpty ||
                          _gameHomeProvider!.memberHouseDungModelList
                              .where((element) => element.posTop == 9)
                              .isEmpty),
                  child: poopWidget(position: 9),
                )),
            //豬按鈕
            Positioned(bottom: 10.h, right: 23.w, child: _pigButton()),
            //房子小圖
            Positioned(left: 3.w, bottom: 5.h, child: _homeSmallImage())
          ],
        ),
      ),
    );
  }

  //region 顯示家具

  //背景
  Widget _roomBg() {
    return SizedBox(
        height: _screenHeight - 10.h,
        child: Consumer<GameHomeProvider>(
          builder: (context, home, _) {
            if (home.editMode &&
                Interior.values[home.furnitureTypeIndex] ==
                    Interior.wallpaper) {
              return _builderWidget(Util().getMemberMugshotUrl(
                  home.showList[home.editModeWallpaper].pic!));
              /*FutureBuilder(
                  future: home.getImage(
                      url: Util().getMemberMugshotUrl(
                          home.showList[home.editModeWallpaper].pic!),
                      refresh: true),
                  builder: (context, AsyncSnapshot<String> snapshot) =>
                      _builderWidget(snapshot.data!));*/
            }
            if (home.editMode && home.editModeWallpaperImagePath.isNotEmpty) {
              if (home.editModeWallpaperImagePath.contains('/cache') ||
                  home.editModeWallpaperImagePath.contains('/Caches')) {
                return Image(
                  gaplessPlayback: true,
                  fit: BoxFit.contain,
                  image: FileImage(File(home.editModeWallpaperImagePath)),
                );
              }
              return CachedNetworkImage(
                imageUrl:
                    Util().getMemberMugshotUrl(home.editModeWallpaperImagePath),
                fit: BoxFit.contain,
              );
            } else if (!home.editMode && home.isUsedWallpaper) {
              return Image(
                gaplessPlayback: true,
                fit: BoxFit.contain,
                image: FileImage(File(home.usedWallpaperImagePath)),
              );
            } else {
              return Image.asset(
                AppImage.imgItemsRoomBg0,
                fit: BoxFit.fill,
              );
            }
          },
        ));
  }

  //狗
  Widget _dog() {
    return SizedBox(
        width: 40.w,
        child: Consumer<GameHomeProvider>(
          builder: (context, home, _) {
            if (home.editMode &&
                Interior.values[home.furnitureTypeIndex] == Interior.dog) {
              //log('載的圖');
              return _builderWidget(Util()
                  .getMemberMugshotUrl(home.showList[home.editModeDog].pic!));
            } else if (home.editMode && home.editModeDogImagePath.isNotEmpty) {
              //log('暫存圖');
              if (home.editModeDogImagePath.contains('/cache') ||
                  home.editModeDogImagePath.contains('/Caches')) {
                return Image(
                  gaplessPlayback: true,
                  fit: BoxFit.contain,
                  image: FileImage(File(home.editModeDogImagePath)),
                );
              }
              return CachedNetworkImage(
                imageUrl: Util().getMemberMugshotUrl(home.editModeDogImagePath),
                fit: BoxFit.contain,
              );
            } else if (!home.editMode && home.isUsedDog) {
              return Image(
                gaplessPlayback: true,
                fit: BoxFit.contain,
                image: FileImage(File(home.usedDogImagePath)),
              );
            } else {
              //log('本地圖');
              return Image.asset(
                AppImage.imgItemsDog0,
                fit: BoxFit.fill,
              );
            }
          },
        ));
  }

  //圍籬
  Widget _fence() {
    return SizedBox(
        width: 53.w,
        height: 21.h,
        child: Consumer<GameHomeProvider>(
          builder: (context, home, _) {
            if (home.editMode &&
                Interior.values[home.furnitureTypeIndex] == Interior.fence) {
              return _builderWidget(Util()
                  .getMemberMugshotUrl(home.showList[home.editModeFence].pic!));
            } else if (home.editMode &&
                home.editModeFenceImagePath.isNotEmpty) {
              if (home.editModeFenceImagePath.contains('/cache') ||
                  home.editModeFenceImagePath.contains('/Caches')) {
                return Image(
                  gaplessPlayback: true,
                  fit: BoxFit.contain,
                  image: FileImage(File(home.editModeFenceImagePath)),
                );
              }
              return CachedNetworkImage(
                imageUrl:
                    Util().getMemberMugshotUrl(home.editModeFenceImagePath),
                fit: BoxFit.contain,
              );
            } else if (!home.editMode && home.isUsedFence) {
              return Image(
                gaplessPlayback: true,
                fit: BoxFit.contain,
                image: FileImage(File(home.usedFenceImagePath)),
              );
            } else {
              return Image.asset(
                AppImage.imgItemsFence1,
                fit: BoxFit.fill,
              );
            }
          },
        ));
  }

  //櫃子
  Widget _cabinet() {
    return SizedBox(
        height: 40.h,
        child: Consumer<GameHomeProvider>(
          builder: (context, home, _) {
            if (home.editMode &&
                Interior.values[home.furnitureTypeIndex] == Interior.cabinet) {
              //是編輯模式但沒有換家具
              return _builderWidget(Util().getMemberMugshotUrl(
                  home.showList[home.editModeCabinet].pic!));
            } else if (home.editMode &&
                home.editModeCabinetImagePath.isNotEmpty) {
              if (home.editModeCabinetImagePath.contains('/cache') ||
                  home.editModeCabinetImagePath.contains('/Caches')) {
                return Image(
                  gaplessPlayback: true,
                  fit: BoxFit.contain,
                  image: FileImage(File(home.editModeCabinetImagePath)),
                );
              }
              //是編輯模式且換過家具
              return CachedNetworkImage(
                imageUrl:
                    Util().getMemberMugshotUrl(home.editModeCabinetImagePath),
                fit: BoxFit.contain,
              );
            } else if (!home.editMode && home.isUsedCabinet) {
              //不是編輯模式且有使用中的家具
              return Image(
                gaplessPlayback: true,
                fit: BoxFit.contain,
                image: FileImage(File(home.usedCabinetImagePath)),
              );
            } else {
              return Image.asset(
                AppImage.imgItemsCabinet0,
              );
            }
          },
        ));
  }

  //狗屋
  Widget _doghouse() {
    return SizedBox(
        width: 45.w,
        child: Consumer<GameHomeProvider>(
          builder: (context, home, _) {
            if (home.editMode &&
                Interior.values[home.furnitureTypeIndex] == Interior.doghouse) {
              return _builderWidget(Util().getMemberMugshotUrl(
                  home.showList[home.editModeDoghouse].pic!));
            } else if (home.editMode &&
                home.editModeDoghouseImagePath.isNotEmpty) {
              if (home.editModeDoghouseImagePath.contains('/cache') ||
                  home.editModeDoghouseImagePath.contains('/Caches')) {
                return Image(
                  gaplessPlayback: true,
                  fit: BoxFit.contain,
                  image: FileImage(File(home.editModeDoghouseImagePath)),
                );
              }
              //是編輯模式且換過家具
              return CachedNetworkImage(
                imageUrl:
                    Util().getMemberMugshotUrl(home.editModeDoghouseImagePath),
                fit: BoxFit.contain,
              );
            } else if (!home.editMode && home.isUsedDoghouse) {
              return Image(
                gaplessPlayback: true,
                fit: BoxFit.contain,
                image: FileImage(File(home.usedDoghouseImagePath)),
              );
            } else {
              return Container();
            }
          },
        ));
  }

  //踏墊
  Widget _mat() {
    return SizedBox(
      width: 45.w,
      child: Consumer<GameHomeProvider>(
        builder: (context, home, _) {
          if (home.editMode &&
              Interior.values[home.furnitureTypeIndex] == Interior.mat) {
            return _builderWidget(Util()
                .getMemberMugshotUrl(home.showList[home.editModeMat].pic!));
          } else if (home.editMode && home.editModeMatImagePath.isNotEmpty) {
            if (home.editModeMatImagePath.contains('/cache') ||
                home.editModeMatImagePath.contains('/Caches')) {
              return Image(
                gaplessPlayback: true,
                fit: BoxFit.contain,
                image: FileImage(File(home.editModeMatImagePath)),
              );
            }
            return CachedNetworkImage(
              imageUrl: Util().getMemberMugshotUrl(home.editModeMatImagePath),
              fit: BoxFit.contain,
            );
          } else if (!home.editMode && home.isUsedMat) {
            return Image(
              gaplessPlayback: true,
              fit: BoxFit.contain,
              image: FileImage(File(home.usedMatImagePath)),
            );
          } else {
            return Container();
          }
        },
      ),
    );
  }

  //椅子
  Widget _chair() {
    return SizedBox(
        width: 120.w,
        child: Consumer<GameHomeProvider>(
          builder: (context, home, _) {
            if (home.editMode &&
                Interior.values[home.furnitureTypeIndex] == Interior.chair) {
              return _builderWidget(Util()
                  .getMemberMugshotUrl(home.showList[home.editModeChair].pic!));
            } else if (home.editMode &&
                home.editModeChairImagePath.isNotEmpty) {
              if (home.editModeChairImagePath.contains('/cache') ||
                  home.editModeChairImagePath.contains('/Caches')) {
                return Image(
                  gaplessPlayback: true,
                  fit: BoxFit.contain,
                  image: FileImage(File(home.editModeChairImagePath)),
                );
              }
              return CachedNetworkImage(
                imageUrl:
                    Util().getMemberMugshotUrl(home.editModeChairImagePath),
                fit: BoxFit.contain,
              );
            } else if (!home.editMode && home.isUsedChair) {
              return Image(
                gaplessPlayback: true,
                fit: BoxFit.contain,
                image: FileImage(File(home.usedChairImagePath)),
              );
            } else {
              return Image.asset(
                AppImage.imgItemsChair0,
              );
            }
          },
        ));
  }

  //桌子
  Widget _table() {
    return SizedBox(
        width: 49.w,
        child: Consumer<GameHomeProvider>(
          builder: (context, home, _) {
            if (home.editMode &&
                Interior.values[home.furnitureTypeIndex] == Interior.table) {
              return _builderWidget(Util()
                  .getMemberMugshotUrl(home.showList[home.editModeTable].pic!));
            } else if (home.editMode &&
                home.editModeTableImagePath.isNotEmpty) {
              if (home.editModeTableImagePath.contains('/cache') ||
                  home.editModeTableImagePath.contains('/Caches')) {
                return Image(
                  gaplessPlayback: true,
                  fit: BoxFit.contain,
                  image: FileImage(File(home.editModeTableImagePath)),
                );
              }
              return CachedNetworkImage(
                imageUrl:
                    Util().getMemberMugshotUrl(home.editModeTableImagePath),
                fit: BoxFit.contain,
              );
            } else if (!home.editMode && home.isUsedTable) {
              return Image(
                gaplessPlayback: true,
                fit: BoxFit.contain,
                image: FileImage(File(home.usedTableImagePath)),
              );
            } else {
              return Image.asset(
                AppImage.imgItemsTable1,
              );
            }
          },
        ));
  }

  //房子小圖
  Widget _homeSmallImage() {
    return Consumer<GameHomeProvider>(
      builder: (context, home, _) {
        if (home.furnitureTypeIndex != Interior.house.index || !home.editMode) {
          return Container();
        }
        return Offstage(
          offstage: home.furnitureTypeIndex != Interior.house.index,
          child: Container(
            width: 40.w,
            height: 25.h,
            alignment: Alignment.bottomCenter,
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                border: Border.all(color: AppColor.brownColor, width: 4),
                color: AppColor.lightBlue),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                FittedBox(
                  child: Text(
                    S.of(context).current_look,
                    style: const TextStyle(
                        color: AppColor.buttonTextColor,
                        fontSize: 20,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                _builderWidget(Util().getMemberMugshotUrl(
                    home.showList[home.editModeHouse].pic!))
              ],
            ),
          ),
        );
      },
    );
  }

  //endregion

  //region 編輯家具紅色icon
  Widget _editIconWidget(Interior set) {
    return Consumer<GameHomeProvider>(
      builder: (context, home, _) {
        return GestureDetector(
          onTap: () {
            _changeTransformation(set);
            home.setFurnitureTypeIndex(value: set.index);
          },
          child: Offstage(
            offstage: !home.editMode,
            child: SizedBox(
                width: _editIconWidth,
                child: Image.asset(
                  home.furnitureTypeIndex == set.index
                      ? set.selectLabel
                      : set.label,
                  fit: BoxFit.fill,
                )),
          ),
        );
      },
    );
  }
  //endregion

  double getValues() {
    if (Device.pixelRatio > 3.0) {
      return -220.w;
    }
    if (Device.pixelRatio >= 3.0 && Device.aspectRatio > 0.5) {
      return -200.w;
    }
    return -250.w;
  }

  void _changeTransformation(Interior set) {
    switch (set) {
      case Interior.wallpaper:
        transformationController.value = Matrix4.translationValues(-50.w, 0, 0);
        break;
      case Interior.dog:
        transformationController.value = Matrix4.identity();
        break;
      case Interior.fence:
        transformationController.value =
            Matrix4.translationValues(getValues(), 0, 0);
        break;
      case Interior.cabinet:
        transformationController.value =
            Matrix4.translationValues(getValues(), 0, 0);
        break;
      case Interior.chair:
        transformationController.value = Matrix4.translationValues(-70.w, 0, 0);
        break;
      case Interior.table:
        transformationController.value = Matrix4.translationValues(-70.w, 0, 0);
        break;
      case Interior.doghouse:
        transformationController.value = Matrix4.identity();
        break;
      case Interior.mat:
        transformationController.value = Matrix4.identity();
        break;
      case Interior.house:
        transformationController.value = Matrix4.identity();
        break;
      default:
        break;
    }
  }

  ///便便widget
  /// * [position]  位置,
  Widget poopWidget({required int position}) {
    return Consumer<GameHomeProvider>(
      builder: (context, home, _) {
        if (home.memberHouseDungModelList.isNotEmpty &&
            home.memberHouseDungModelList
                .where((element) => element.posTop == position)
                .isNotEmpty) {
          MemberHouseDungModel _dung = home.memberHouseDungModelList
              .firstWhere((element) => element.posTop == position);
          if (home.memberHouseMessageList
              .where((element) => element.id == _dung.memberHouseMessageId)
              .isNotEmpty) {
            MemberHouseMessageModel _message = home.memberHouseMessageList
                .firstWhere(
                    (element) => element.id == _dung.memberHouseMessageId);
            return Offstage(
              offstage: _dung.clean! || home.editMode,
              child: SizedBox(
                width: 30.w,
                height: 18.h,
                child: Stack(
                  alignment: Alignment.center,
                  children: [
                    //大便
                    Positioned(
                      bottom: 0,
                      left: 0,
                      child: SizedBox(
                          width: 20.w,
                          child: GestureDetector(
                            onTap: () => _showMessageCardDialog(
                                message: _message, dung: _dung),
                            child: CachedNetworkImage(
                              imageUrl: _gameHomeProvider!
                                  .getDungImage(id: _dung.dungId!),
                              fit: BoxFit.contain,
                            ),
                          )),
                    ),
                    //頭像
                    Positioned(
                      top: 0,
                      left: 0,
                      child: Container(
                          width: 15.w,
                          margin: EdgeInsets.symmetric(horizontal: 4.w),
                          padding: EdgeInsets.symmetric(horizontal: 0.7.w),
                          alignment: Alignment.center,
                          decoration: const BoxDecoration(
                              color: Colors.white, shape: BoxShape.circle),
                          child: FutureBuilder(
                            initialData: MugshotWidget(
                              fromMemberId: _memberProvider!.memberModel.id!,
                              nickName: '',
                              gender: 1,
                              birthday: '',
                              targetMemberId: _dung.memberId!,
                            ),
                            future: _gameHomeProvider!
                                .getDungMember(id: _dung.memberId!),
                            builder:
                                (context, AsyncSnapshot<dynamic> snapshot) {
                              if (snapshot.connectionState ==
                                  ConnectionState.done) {
                                if (snapshot.data != null) {
                                  return MugshotWidget(
                                    fromMemberId:
                                        _memberProvider!.memberModel.id!,
                                    nickName: snapshot.data!.nickname,
                                    gender: snapshot.data!.gender,
                                    birthday: snapshot.data!.birthday,
                                    targetMemberId: snapshot.data!.id,
                                    inGame: true,
                                  );
                                } else {
                                  return MugshotWidget(
                                    fromMemberId:
                                        _memberProvider!.memberModel.id!,
                                    nickName: '',
                                    gender: 1,
                                    birthday: '',
                                    targetMemberId: _dung.memberId!,
                                  );
                                }
                              }
                              return MugshotWidget(
                                fromMemberId: _memberProvider!.memberModel.id!,
                                nickName: '',
                                gender: 1,
                                birthday: '',
                                targetMemberId: _dung.memberId!,
                              );
                            },
                          )),
                    ),
                    //時間
                    Positioned(
                        bottom: 7.h,
                        right: 0,
                        child: Container(
                          width: 17.w,
                          height: 4.h,
                          padding: EdgeInsets.only(
                              left: 2.w, right: 2.w, bottom: 0.7.h),
                          decoration: BoxDecoration(
                              image: DecorationImage(
                                  image: AssetImage(AppImage.imgDialog),
                                  fit: BoxFit.fill)),
                          child: FittedBox(
                              child:
                                  Text(Util().poopTime(_message.createdAt!))),
                        )),
                  ],
                ),
              ),
            );
          } else {
            return Container();
          }
        } else {
          return Container();
        }
      },
    );
  }

  ///確認訊息與清除大便
  /// * [message] 訊息
  /// * [dung] 大便
  void _showMessageCardDialog(
      {required MemberHouseMessageModel message,
      required MemberHouseDungModel dung}) async {
    EasyLoading.show(status: 'Loading...');
    MemberModel? _member =
        await _gameHomeProvider!.getDungMember(id: dung.memberId!);
    EasyLoading.dismiss();
    if (_member != null) {
      bool? _result = await showGeneralDialog(
          context: context,
          pageBuilder: (context, anim1, anim2) {
            return MessageCardDialog(
                key: UniqueKey(),
                message: message.message!,
                gender: _member.gender!,
                nickname: _member.nickname!,
                birthday: _member.birthday!,
                id: _member.id!,
                check: message.checked!);
          });
      if (_result != null) {
        if (_result) {
          if(_memberProvider!.memberPoint.cleanTimes == 0){
            EasyLoading.showToast(S.of(context).not_enough_cleaning);
            return;
          }
          if (message.checked == 0) {
            //需先確認是否已讀
            _stompClientProvider!.sendReadMemberHouseMessage(id: message.id!);
          }/* else {
            if (_gameHomeProvider!.memberCleaningTools.isNotEmpty) {
              //清除大便
              if (_gameHomeProvider!.memberCleaningTools
                  .where((element) => element.memberDefault == true)
                  .isNotEmpty) {
                //拿預設的來用
                ClearOneMemberHouseDungDTO _model = ClearOneMemberHouseDungDTO(
                    memberCleaningToolsId: _gameHomeProvider!
                        .memberCleaningTools
                        .where((element) => element.memberDefault == true)
                        .first
                        .id!,
                    memberId: _memberProvider!.memberModel.id!,
                    memberHouseId: dung.memberHouseId!,
                    memberHouseDungId: dung.id!);
                _gameShoppingRefreshProvider!
                    .setRefreshStatus(status: GameRefreshStatus.cleaning);
                _stompClientProvider!
                    .sendClearOneMemberHouseDung(model: _model);
              } else {
                ClearOneMemberHouseDungDTO _model = ClearOneMemberHouseDungDTO(
                    memberCleaningToolsId:
                        _gameHomeProvider!.memberCleaningTools.first.id!,
                    memberId: _memberProvider!.memberModel.id!,
                    memberHouseId: dung.memberHouseId!,
                    memberHouseDungId: dung.id!);
                _gameShoppingRefreshProvider!
                    .setRefreshStatus(status: GameRefreshStatus.cleaning);
                _stompClientProvider!
                    .sendClearOneMemberHouseDung(model: _model);
              }
            } else {
              EasyLoading.showToast(S.of(context).not_enough_cleaning);
            }
          }*/
        }
      }
    }
  }

  //養豬按鈕
  Widget _pigButton() {
    return Consumer<GameHomeProvider>(
      builder: (context, home, _) {
        return Offstage(
          offstage: home.editMode,
          child: GestureDetector(
            onTap: () => _showPigGrowsOnTap(),
            child: SizedBox(
                width: 23.w,
                child: Image.asset(
                  AppImage.btnPig,
                )),
          ),
        );
      },
    );
  }

  //豬隻成長
  void _showPigGrowsOnTap() {
    //檢查寵物狀態
    _stompClientProvider!.sendCheckPetsStatus();
    showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return PigGrowsDialog(
            key: UniqueKey(),
          );
        });
  }

  Widget mallWidget() {
    return Consumer<GameHomeProvider>(builder: (context, home, _) {
      //log('購買狀態: ${home.editMode}');
      return Offstage(
        offstage: !home.editMode,
        child: SizedBox(
          width: Device.width,
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.symmetric(vertical: 0.5.h),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    _buy(),
                    SizedBox(
                      width: 2.w,
                    ),
                    _check(),
                    SizedBox(width: 2.w),
                    _cancel(),
                    SizedBox(width: 2.w),
                  ],
                ),
              ),
              _interiorList(),
              _itemList(),
            ],
          ),
        ),
      );
    });
  }

  Widget _buy() {
    return Consumer<GameHomeProvider>(
      builder: (context, home, _) {
        return GestureDetector(
          onTap: () => _buyOnClick(
              home.showList, home.furnitureTypeIndex, home.itemIndex),
          child: SizedBox(
            width: 14.w,
            child: Image.asset(AppImage.btnBuy),
          ),
        );
      },
    );
  }

  ///購買點擊
  void _buyOnClick(_list, int _furnitureTypeIndex, int _itemIndex) async {
    if (EasyLoading.isShow) {
      return;
    }
    if (_list[_itemIndex].isBuy) {
      EasyLoading.showToast(S.of(context).bought);
      return;
    }
    switch (_furnitureTypeIndex) {
      case 0:
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
        //log('要購買的家具: ${_list[_itemIndex].toJson()}');
        //判斷點數是否足夠
        if (_memberProvider!.memberPoint.point! >= _list[_itemIndex].cost) {
          bool? result = await _showShoppingDialog(
              price: _list[_itemIndex].cost.toString());
          if (result != null) {
            //確認要買
            if (result) {
              UpdateMemberFurnitureDTO _updateMemberFurnitureDTO =
                  UpdateMemberFurnitureDTO(
                name: _list[_itemIndex].name,
                memberId: _memberProvider!.memberModel.id!,
                furnitureId: _list[_itemIndex].id,
                status: 1,
              );
              _gameShoppingRefreshProvider!
                  .setRefreshStatus(status: GameRefreshStatus.shopping);
              _gameShoppingRefreshProvider!
                  .setStuffItem(item: StuffItem.furniture);
              _stompClientProvider!.sendBuyFurniture(
                  updateMemberFurnitureDTO: _updateMemberFurnitureDTO);
            }
          }
        } else {
          if (_utilApiProvider!.systemSetting!.addPointsStatus ==
              PointsStatus.closure.index) {
            EasyLoading.showToast(S.of(context).not_enough_points);
          } else {
            //點數不足
            _pointInsufficient();
          }
        }
        break;
      //寵物
      case 1:
        //log('要購買的寵物: ${_list[_itemIndex].toJson()}');
        //判斷點數是否足夠
        if (_memberProvider!.memberPoint.point! >= _list[_itemIndex].cost) {
          bool? result = await _showShoppingDialog(
              price: _list[_itemIndex].cost.toString());
          if (result != null) {
            //確認要買
            if (result) {
              UpdateMemberPetsDTO _updateMemberPetsDTO = UpdateMemberPetsDTO(
                name: _list[_itemIndex].name,
                animalType: _list[_itemIndex].animalType,
                memberId: _memberProvider!.memberModel.id!,
                petsId: _list[_itemIndex].id,
                status: 1,
              );
              _gameShoppingRefreshProvider!
                  .setRefreshStatus(status: GameRefreshStatus.shopping);
              _gameShoppingRefreshProvider!.setStuffItem(item: StuffItem.pets);
              _stompClientProvider!
                  .sendBuyPets(buyPetsDTO: _updateMemberPetsDTO);
            }
          }
        } else {
          if (_utilApiProvider!.systemSetting!.addPointsStatus ==
              PointsStatus.closure.index) {
            EasyLoading.showToast(S.of(context).not_enough_points);
          } else {
            //點數不足
            _pointInsufficient();
          }
        }
        break;
      //房子
      case 8:
        //log('要購買的房子: ${_list[_itemIndex].toJson()}');
        //判斷點數是否足夠
        if (_memberProvider!.memberPoint.point! >= _list[_itemIndex].cost) {
          bool? result = await _showShoppingDialog(
              price: _list[_itemIndex].cost.toString());
          if (result != null) {
            //確認要買
            if (result) {
              UpdateMemberHouseStyleDTO _updateMemberHouseStyleDTO =
                  UpdateMemberHouseStyleDTO(
                name: _list[_itemIndex].name,
                memberId: _memberProvider!.memberModel.id!,
                houseStyleId: _list[_itemIndex].id,
                status: 1,
              );
              _gameShoppingRefreshProvider!
                  .setRefreshStatus(status: GameRefreshStatus.shopping);
              _gameShoppingRefreshProvider!.setStuffItem(item: StuffItem.house);
              _stompClientProvider!
                  .sendBuyHouse(buyHouseDTO: _updateMemberHouseStyleDTO);
            }
          }
        } else {
          if (_utilApiProvider!.systemSetting!.addPointsStatus ==
              PointsStatus.closure.index) {
            EasyLoading.showToast(S.of(context).not_enough_points);
          } else {
            //點數不足
            _pointInsufficient();
          }
        }
        break;
    }
  }

  ///點數不足統一方法
  void _pointInsufficient() async {
    bool? result = await _showPointInsufficient();
    if (result != null) {
      if (result) {
        //跳轉加值
        delegate.push(name: RouteName.myPointsPage);
      }
    }
  }

  ///確認購買對話框
  /// * [price] 價格
  Future<bool?> _showShoppingDialog({required String price}) async {
    return await showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return GameShoppingDialog(
              key: UniqueKey(),
              title: S.of(context).message,
              content: price,
              confirmButtonText: S.of(context).sure);
        },
        barrierColor: Colors.black54,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 100),
        transitionBuilder: (context, anim1, anim2, child) {
          return Transform.scale(
            scale: anim1.value,
            child: child,
          );
        });
  }

  ///點數不足
  Future<bool?> _showPointInsufficient() async {
    return await showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return GamePointInsufficientDialog(
              key: UniqueKey(),
              title: S.of(context).message,
              content: S.of(context).please_add_value,
              confirmButtonText: S.of(context).go_to_premium,
              cancelButtonText: S.of(context).cancel);
        },
        barrierColor: Colors.black54,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 100),
        transitionBuilder: (context, anim1, anim2, child) {
          return child;
        });
  }

  Widget _check() {
    return GestureDetector(
      onTap: () => _setInterior(),
      child: SizedBox(
        width: 9.w,
        child: Image.asset(AppImage.btnConfirm),
      ),
    );
  }

  void _showReplaceEasyLoading() {
    if (!EasyLoading.isShow) {
      _gameShoppingRefreshProvider!
          .setRefreshStatus(status: GameRefreshStatus.replace);
    }
  }

  ///更換家具
  void _setInterior() {
    if (EasyLoading.isShow) {
      return;
    }
    _gameHomeProvider!.setInterior().then((value) {
      if (!value['status']) {
        switch (value['value']) {
          case Interior.wallpaper:
            EasyLoading.showError(
                '${S.of(context).wallpaper}${S.of(context).cannot_be_replaced}');
            break;
          case Interior.dog:
            EasyLoading.showError(
                '${S.of(context).dog}${S.of(context).cannot_be_replaced}');
            break;
          case Interior.fence:
            EasyLoading.showError(
                '${S.of(context).fence}${S.of(context).cannot_be_replaced}');
            break;
          case Interior.cabinet:
            EasyLoading.showError(
                '${S.of(context).cabinet}${S.of(context).cannot_be_replaced}');
            break;
          case Interior.chair:
            EasyLoading.showError(
                '${S.of(context).chair}${S.of(context).cannot_be_replaced}');
            break;
          case Interior.table:
            EasyLoading.showError(
                '${S.of(context).table}${S.of(context).cannot_be_replaced}');
            break;
          case Interior.doghouse:
            EasyLoading.showError(
                '${S.of(context).doghouse}${S.of(context).cannot_be_replaced}');
            break;
          case Interior.mat:
            EasyLoading.showError(
                '${S.of(context).mat}${S.of(context).cannot_be_replaced}');
            break;
          case Interior.house:
            EasyLoading.showError(
                '${S.of(context).house}${S.of(context).cannot_be_replaced}');
            break;
        }
      } else {
        switch (Interior.values[_gameHomeProvider!.furnitureTypeIndex]) {
          case Interior.wallpaper:
            if (!_gameHomeProvider!
                .allWallpaper[_gameHomeProvider!.editModeWallpaper].isUsed!) {
              //壁紙未使用的話
              //log('壁紙更新 ${_gameHomeProvider!.allWallpaper[_gameHomeProvider!.editModeWallpaper].name}');
              int _fromItemId = _gameHomeProvider!.allWallpaper
                      .where((element) => element.isUsed == true)
                      .isNotEmpty
                  ? _gameHomeProvider!.memberFurniture
                      .where((element) =>
                          element.furnitureId ==
                          _gameHomeProvider!.allWallpaper
                              .where((element) => element.isUsed == true)
                              .first
                              .id!)
                      .first
                      .id!
                  : _gameHomeProvider!.memberFurniture
                      .where((element) =>
                          element.furnitureId ==
                          _gameHomeProvider!
                              .allWallpaper[
                                  _gameHomeProvider!.editModeWallpaper]
                              .id!)
                      .first
                      .id!;
              ChangeItemDTO _changeItemDTO = ChangeItemDTO(
                  fromItemId: _fromItemId,
                  targetItemId: _gameHomeProvider!.memberFurniture
                      .where((element) =>
                          element.furnitureId ==
                          _gameHomeProvider!
                              .allWallpaper[
                                  _gameHomeProvider!.editModeWallpaper]
                              .id!)
                      .first
                      .id!,
                  posTop: 0,
                  posLeft: 0);
              _showReplaceEasyLoading();
              _stompClientProvider!
                  .sendChangeMemberFurniture(changeItemDTO: _changeItemDTO);
            }
            break;
          case Interior.dog:
            if (!_gameHomeProvider!
                .noPigPetsList[_gameHomeProvider!.editModeDog].isUsed!) {
              //寵物未使用的話
              //log('寵物更新 ${_gameHomeProvider!.noPigPetsList[_gameHomeProvider!.editModeDog].name}');
              int _fromItemId = _gameHomeProvider!.noPigPetsList
                      .where((element) => element.isUsed == true)
                      .isNotEmpty
                  ? _gameHomeProvider!.memberPets
                      .where((element) =>
                          element.petsId ==
                          _gameHomeProvider!.noPigPetsList
                              .where((element) => element.isUsed == true)
                              .first
                              .id!)
                      .first
                      .id!
                  : _gameHomeProvider!.memberPets
                      .where((element) =>
                          element.petsId ==
                          _gameHomeProvider!
                              .noPigPetsList[_gameHomeProvider!.editModeDog]
                              .id!)
                      .first
                      .id!;
              ChangeItemDTO _changeItemDTO = ChangeItemDTO(
                  fromItemId: _fromItemId,
                  targetItemId: _gameHomeProvider!.memberPets
                      .where((element) =>
                          element.petsId ==
                          _gameHomeProvider!
                              .noPigPetsList[_gameHomeProvider!.editModeDog]
                              .id!)
                      .first
                      .id!,
                  posTop: 0,
                  posLeft: 0);
              _showReplaceEasyLoading();
              _stompClientProvider!
                  .sendChangeMemberPets(changeItemDTO: _changeItemDTO);
            }
            break;
          case Interior.fence:
            if (!_gameHomeProvider!
                .allFence[_gameHomeProvider!.editModeFence].isUsed!) {
              //圍籬未使用的話
              //log('圍籬更新 ${_gameHomeProvider!.allFence[_gameHomeProvider!.editModeFence].name}');
              int _fromItemId = _gameHomeProvider!.allFence
                      .where((element) => element.isUsed == true)
                      .isNotEmpty
                  ? _gameHomeProvider!.memberFurniture
                      .where((element) =>
                          element.furnitureId ==
                          _gameHomeProvider!.allFence
                              .where((element) => element.isUsed == true)
                              .first
                              .id!)
                      .first
                      .id!
                  : _gameHomeProvider!.memberFurniture
                      .where((element) =>
                          element.furnitureId ==
                          _gameHomeProvider!
                              .allFence[_gameHomeProvider!.editModeFence].id!)
                      .first
                      .id!;
              ChangeItemDTO _changeItemDTO = ChangeItemDTO(
                  fromItemId: _fromItemId,
                  targetItemId: _gameHomeProvider!.memberFurniture
                      .where((element) =>
                          element.furnitureId ==
                          _gameHomeProvider!
                              .allFence[_gameHomeProvider!.editModeFence].id!)
                      .first
                      .id!,
                  posTop: 0,
                  posLeft: 0);
              _showReplaceEasyLoading();
              _stompClientProvider!
                  .sendChangeMemberFurniture(changeItemDTO: _changeItemDTO);
            }
            break;
          case Interior.cabinet:
            if (!_gameHomeProvider!
                .allCabinet[_gameHomeProvider!.editModeCabinet].isUsed!) {
              //櫃子未使用的話
              //log('櫃子更新 ${_gameHomeProvider!.allCabinet[_gameHomeProvider!.editModeCabinet].name}');
              int _fromItemId = _gameHomeProvider!.allCabinet
                      .where((element) => element.isUsed == true)
                      .isNotEmpty
                  ? _gameHomeProvider!.memberFurniture
                      .where((element) =>
                          element.furnitureId ==
                          _gameHomeProvider!.allCabinet
                              .where((element) => element.isUsed == true)
                              .first
                              .id!)
                      .first
                      .id!
                  : _gameHomeProvider!.memberFurniture
                      .where((element) =>
                          element.furnitureId ==
                          _gameHomeProvider!
                              .allCabinet[_gameHomeProvider!.editModeCabinet]
                              .id!)
                      .first
                      .id!;
              ChangeItemDTO _changeItemDTO = ChangeItemDTO(
                  fromItemId: _fromItemId,
                  targetItemId: _gameHomeProvider!.memberFurniture
                      .where((element) =>
                          element.furnitureId ==
                          _gameHomeProvider!
                              .allCabinet[_gameHomeProvider!.editModeCabinet]
                              .id!)
                      .first
                      .id!,
                  posTop: 0,
                  posLeft: 0);
              _showReplaceEasyLoading();
              _stompClientProvider!
                  .sendChangeMemberFurniture(changeItemDTO: _changeItemDTO);
            }
            break;
          case Interior.chair:
            if (!_gameHomeProvider!
                .allChair[_gameHomeProvider!.editModeChair].isUsed!) {
              //椅子未使用的話
              //log('椅子更新 ${_gameHomeProvider!.allChair[_gameHomeProvider!.editModeChair].name}');
              int _fromItemId = _gameHomeProvider!.allChair
                      .where((element) => element.isUsed == true)
                      .isNotEmpty
                  ? _gameHomeProvider!.memberFurniture
                      .where((element) =>
                          element.furnitureId ==
                          _gameHomeProvider!.allChair
                              .where((element) => element.isUsed == true)
                              .first
                              .id!)
                      .first
                      .id!
                  : _gameHomeProvider!.memberFurniture
                      .where((element) =>
                          element.furnitureId ==
                          _gameHomeProvider!
                              .allChair[_gameHomeProvider!.editModeChair].id!)
                      .first
                      .id!;
              ChangeItemDTO _changeItemDTO = ChangeItemDTO(
                  fromItemId: _fromItemId,
                  targetItemId: _gameHomeProvider!.memberFurniture
                      .where((element) =>
                          element.furnitureId ==
                          _gameHomeProvider!
                              .allChair[_gameHomeProvider!.editModeChair].id!)
                      .first
                      .id!,
                  posTop: 0,
                  posLeft: 0);
              _showReplaceEasyLoading();
              _stompClientProvider!
                  .sendChangeMemberFurniture(changeItemDTO: _changeItemDTO);
            }
            break;
          case Interior.table:
            if (!_gameHomeProvider!
                .allTable[_gameHomeProvider!.editModeTable].isUsed!) {
              //桌子未使用的話
              //log('桌子更新 ${_gameHomeProvider!.allTable[_gameHomeProvider!.editModeTable].name}');
              int _fromItemId = _gameHomeProvider!.allTable
                      .where((element) => element.isUsed == true)
                      .isNotEmpty
                  ? _gameHomeProvider!.memberFurniture
                      .where((element) =>
                          element.furnitureId ==
                          _gameHomeProvider!.allTable
                              .where((element) => element.isUsed == true)
                              .first
                              .id!)
                      .first
                      .id!
                  : _gameHomeProvider!.memberFurniture
                      .where((element) =>
                          element.furnitureId ==
                          _gameHomeProvider!
                              .allTable[_gameHomeProvider!.editModeTable].id!)
                      .first
                      .id!;
              ChangeItemDTO _changeItemDTO = ChangeItemDTO(
                  fromItemId: _fromItemId,
                  targetItemId: _gameHomeProvider!.memberFurniture
                      .where((element) =>
                          element.furnitureId ==
                          _gameHomeProvider!
                              .allTable[_gameHomeProvider!.editModeTable].id!)
                      .first
                      .id!,
                  posTop: 12,
                  posLeft: 13);
              _showReplaceEasyLoading();
              _stompClientProvider!
                  .sendChangeMemberFurniture(changeItemDTO: _changeItemDTO);
            }
            break;
          case Interior.doghouse:
            if (!_gameHomeProvider!
                .allDoghouse[_gameHomeProvider!.editModeDoghouse].isUsed!) {
              //狗屋未使用的話
              //log('狗屋更新 ${_gameHomeProvider!.allDoghouse[_gameHomeProvider!.editModeDoghouse].name}');
              int _fromItemId = _gameHomeProvider!.allDoghouse
                      .where((element) => element.isUsed == true)
                      .isNotEmpty
                  ? _gameHomeProvider!.memberFurniture
                      .where((element) =>
                          element.furnitureId ==
                          _gameHomeProvider!.allDoghouse
                              .where((element) => element.isUsed == true)
                              .first
                              .id!)
                      .first
                      .id!
                  : _gameHomeProvider!.memberFurniture
                      .where((element) =>
                          element.furnitureId ==
                          _gameHomeProvider!
                              .allDoghouse[_gameHomeProvider!.editModeDoghouse]
                              .id!)
                      .first
                      .id!;
              ChangeItemDTO _changeItemDTO = ChangeItemDTO(
                  fromItemId: _fromItemId,
                  targetItemId: _gameHomeProvider!.memberFurniture
                      .where((element) =>
                          element.furnitureId ==
                          _gameHomeProvider!
                              .allDoghouse[_gameHomeProvider!.editModeDoghouse]
                              .id!)
                      .first
                      .id!,
                  posTop: 0,
                  posLeft: 0);
              _showReplaceEasyLoading();
              _stompClientProvider!
                  .sendChangeMemberFurniture(changeItemDTO: _changeItemDTO);
            }
            break;
          case Interior.mat:
            if (!_gameHomeProvider!
                .allMat[_gameHomeProvider!.editModeMat].isUsed!) {
              //踏墊未使用的話
              //log('踏墊更新 ${_gameHomeProvider!.allMat[_gameHomeProvider!.editModeMat].name}');
              int _fromItemId = _gameHomeProvider!.allMat
                      .where((element) => element.isUsed == true)
                      .isNotEmpty
                  ? _gameHomeProvider!.memberFurniture
                      .where((element) =>
                          element.furnitureId ==
                          _gameHomeProvider!.allMat
                              .where((element) => element.isUsed == true)
                              .first
                              .id!)
                      .first
                      .id!
                  : _gameHomeProvider!.memberFurniture
                      .where((element) =>
                          element.furnitureId ==
                          _gameHomeProvider!
                              .allMat[_gameHomeProvider!.editModeMat].id!)
                      .first
                      .id!;
              ChangeItemDTO _changeItemDTO = ChangeItemDTO(
                  fromItemId: _fromItemId,
                  targetItemId: _gameHomeProvider!.memberFurniture
                      .where((element) =>
                          element.furnitureId ==
                          _gameHomeProvider!
                              .allMat[_gameHomeProvider!.editModeMat].id!)
                      .first
                      .id!,
                  posTop: 0,
                  posLeft: 0);
              _showReplaceEasyLoading();
              _stompClientProvider!
                  .sendChangeMemberFurniture(changeItemDTO: _changeItemDTO);
            }
            break;
          case Interior.house:
            if (!_gameHomeProvider!
                .allHouseStyleList[_gameHomeProvider!.editModeHouse].isUsed!) {
              //房子未使用的話
              //log('房子更新 ${_gameHomeProvider!.allHouseStyleList[_gameHomeProvider!.editModeHouse].name}');
              int _fromItemId = _gameHomeProvider!.allHouseStyleList
                      .where((element) => element.isUsed == true)
                      .isNotEmpty
                  ? _gameHomeProvider!.memberHouseStyle
                      .where((element) =>
                          element.houseStyleId ==
                          _gameHomeProvider!.allHouseStyleList
                              .where((element) => element.isUsed == true)
                              .first
                              .id!)
                      .first
                      .id!
                  : _gameHomeProvider!.memberHouse
                      .where((element) =>
                          element.houseId ==
                          _gameHomeProvider!
                              .allHouseStyleList[
                                  _gameHomeProvider!.editModeHouse]
                              .id!)
                      .first
                      .id!;
              ChangeStyleDTO _changeStyleDTO = ChangeStyleDTO(
                  fromStyleId: _fromItemId,
                  targetStyleId: _gameHomeProvider!.memberHouseStyle
                      .where((element) =>
                          element.houseStyleId ==
                          _gameHomeProvider!
                              .allHouseStyleList[
                                  _gameHomeProvider!.editModeHouse]
                              .id!)
                      .first
                      .id!,
                  useToId: _gameHomeProvider!.memberHouse.first.id!);
              _showReplaceEasyLoading();
              _stompClientProvider!
                  .sendChangeMemberHouse(changeStyleDTO: _changeStyleDTO);
            }
            break;
        }
      }
    });
  }

  Widget _cancel() {
    return Consumer<GameHomeProvider>(
      builder: (context, home, _) {
        return GestureDetector(
          onTap: () {
            //關閉編輯模式
            home.setEditMode(value: false);
          },
          child: SizedBox(
            width: 9.w,
            child: Image.asset(AppImage.btnCancel),
          ),
        );
      },
    );
  }

  //家具類別
  Widget _interiorList() {
    return Container(
      height: 7.h,
      color: AppColor.orangeColor,
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: Interior.values.length,
          itemBuilder: (context, index) {
            return _interiorCard(set: Interior.values[index]);
          }),
    );
  }

  //家具類別卡
  Widget _interiorCard({required Interior set}) {
    return Consumer<GameHomeProvider>(builder: (context, home, _) {
      return GestureDetector(
        onTap: () {
          if (EasyLoading.isShow) {
            return;
          }
          if (home.furnitureTypeIndex != set.index) {
            _changeTransformation(set);
            home.setFurnitureTypeIndex(value: set.index);
          }
        },
        child: Container(
          width: 20.w,
          margin: EdgeInsets.symmetric(horizontal: 2.w, vertical: 1.h),
          padding: EdgeInsets.symmetric(horizontal: 3.w, vertical: 1.h),
          alignment: Alignment.center,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              border: Border.all(
                  color: home.furnitureTypeIndex == set.index
                      ? Colors.white
                      : AppColor.brownColor,
                  width: 2),
              color: home.furnitureTypeIndex == set.index
                  ? AppColor.yellowButton
                  : AppColor.beigeColor),
          child: Text(
            _textChange(data: set),
            style: TextStyle(
                color: home.furnitureTypeIndex == set.index
                    ? Colors.white
                    : AppColor.brownColor),
          ),
        ),
      );
    });
  }

  //品項類別
  Widget _itemList() {
    return Container(
      height: 25.w,
      color: AppColor.brownColor,
      child: Consumer<GameHomeProvider>(
        builder: (context, home, _) {
          return ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: home.showList.length,
              itemBuilder: (context, index) {
                return ShoppingItemCardWidget(index: index);
              });
        },
      ),
    );
  }

  ///字體返回
  ///* [data] 類別: 壁紙、狗、圍籬、櫃子、椅子、床、桌子、房子
  String _textChange({required Interior data}) {
    switch (data) {
      case Interior.wallpaper:
        return S.of(context).wallpaper;
      case Interior.dog:
        return S.of(context).dog;
      case Interior.fence:
        return S.of(context).fence;
      case Interior.cabinet:
        return S.of(context).cabinet;
      case Interior.chair:
        return S.of(context).chair;
      case Interior.table:
        return S.of(context).table;
      case Interior.house:
        return S.of(context).house;
      case Interior.doghouse:
        return S.of(context).doghouse;
      case Interior.mat:
        return S.of(context).mat;
    }
  }

  ///圖片加載判斷
  Widget _builderWidget(String snapshot) {
    if (snapshot.toString().contains('https')) {
      return CachedNetworkImage(
        imageUrl: snapshot.toString(),
        fit: BoxFit.contain,
      );
    } else {
      return Image(
        gaplessPlayback: true,
        fit: BoxFit.contain,
        image: FileImage(File(snapshot.toString())),
      );
    }
  }

  ///買清潔工具
  void _addCleaningTools() async {
    //購買清掃次數框
    /*Map<String, dynamic>? result = await showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return PurchasesDialog(
            key: UniqueKey(),
            title:
                '${S.of(context).buy}${S.of(context).clean_up}${S.of(context).frequency}',
            price: _utilApiProvider!.cleaningToolsList.first.cost!,
            confirmButtonText: S.of(context).confirm,
          );
        });
    if (result != null) {
      if (result['status']) {
        EasyLoading.show(status: S.of(context).purchasing);
        AddMemberItemDTO _addMemberItem = AddMemberItemDTO(
            memberId: _memberProvider!.memberModel.id!,
            itemId: _utilApiProvider!.cleaningToolsList.first.id!,
            qty: result['value']);
        _stompClientProvider!
            .sendAddMemberCleaningTools(addMemberItemDTO: _addMemberItem);
      }
    }*/
    //恢復清潔次數
    if (_memberProvider!.memberPoint.point! <
        _utilApiProvider!.gameSetting.recoverCleanPoints!) {
      EasyLoading.showToast(S.of(context).please_add_value);
      return;
    }
    if(_memberProvider!.memberPoint.cleanTimes! == 2){
      EasyLoading.showToast(S.of(context).the_limit_has_been_reached);
      return;
    }
    bool? result = await showGeneralDialog(
      context: context,
      pageBuilder: (context, anim1, anim2) {
        return PayRecoverTimersDialog(
          key: UniqueKey(),
          title: S.of(context).buy,
          recoverPoints: _utilApiProvider!.gameSetting.recoverCleanPoints!,
          limit: _utilApiProvider!.gameSetting.cleanLimit!,
          confirmButtonText: S.of(context).confirm,
          cancelButtonText: S.of(context).cancel,
        );
      },
    );
    if (result != null) {
      if (result) {
        EasyLoading.show(status: S.of(context).purchasing);
        bool r = await _gameHomeProvider!
            .postRecoverCleanTimes(_memberProvider!.memberModel.id!);
        if (r) {
          await _memberProvider!
              .getMemberPoint(_memberProvider!.memberModel.id!);
          EasyLoading.dismiss();
        } else {
          EasyLoading.showError('${S.of(context).buy}${S.of(context).fail}');
        }
      }
    }
  }
}
