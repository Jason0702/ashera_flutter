import 'dart:developer';
import 'dart:io';

import 'package:ashera_flutter/app.dart';
import 'package:ashera_flutter/dialog/pay_recover_times_dialog.dart';
import 'package:ashera_flutter/models/game_model.dart';
import 'package:ashera_flutter/provider/game_street_provider.dart';
import 'package:ashera_flutter/widget/mugshot_widget.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../dialog/game_shopping_dialog.dart';
import '../../dialog/member_house_message_dialog.dart';
import '../../dialog/purchases_dialog.dart';
import '../../dialog/setting_game_dialog.dart';
import '../../dialog/show_dog_dung_dialog.dart';
import '../../enum/app_enum.dart';
import '../../enum/app_position_enum.dart';
import '../../generated/l10n.dart';
import '../../models/game_dto.dart';
import '../../models/member_model.dart';
import '../../models/pets_model.dart';
import '../../provider/game_home_provider.dart';
import '../../provider/member_provider.dart';
import '../../provider/position_provider.dart';
import '../../provider/stomp_client_provider.dart';
import '../../provider/util_api_provider.dart';
import '../../routers/route_name.dart';
import '../../utils/app_color.dart';
import '../../utils/app_image.dart';
import '../../utils/util.dart';
import '../../widget/avatar_image.dart';
import '../../widget/gender_icon.dart';

class GameHomeOtherPeoplePage extends StatefulWidget {
  const GameHomeOtherPeoplePage({Key? key}) : super(key: key);

  @override
  State createState() => _GameHomeOtherPeoplePageState();
}

class _GameHomeOtherPeoplePageState extends State<GameHomeOtherPeoplePage> {
  GameHomeProvider? _gameHomeProvider;
  GameStreetProvider? _gameStreetProvider;
  //STOMP
  StompClientProvider? _stompClientProvider;
  //Member
  MemberProvider? _memberProvider;
  UtilApiProvider? _utilApiProvider;
  PositionProvider? _positionProvider;

  //菜單高度
  final double _menuHeight = 15.h;

  //底部功能列按鈕寬度
  static final double _bottomWidth = 20.w;

  //左側功能列按鈕寬度
  static final double _leftWidth = 14.w;

  //家具高度
  final double _screenHeight = Device.height - 18.5.h;

  //視角操控器
  TransformationController transformationController =
      TransformationController();

  MemberModel? _memberModel;

  int? _onTapPoopIndex = 0;

  _onLayoutDone(_) async {
    _positionProvider!.setAppPosition(position: AppPosition.gameHome);
    _gameHomeProvider!.tidyMemberDung(refresh: false);
    Future.delayed(const Duration(milliseconds: 1000), () {
      setState(() {});
    });

    if (_gameHomeProvider!.noPigPetsList
        .where((element) => element.isUsed == true)
        .isNotEmpty) {
      _gameHomeProvider!.noPigPetsList
          .where((element) => element.isUsed == true)
          .forEach((element) {
        //log('目前我正在使用的狗: ${element.name}');
      });
    }
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _positionProvider = Provider.of<PositionProvider>(context, listen: false);
    _gameHomeProvider = Provider.of<GameHomeProvider>(context, listen: false);
    _stompClientProvider =
        Provider.of<StompClientProvider>(context, listen: false);
    _memberProvider = Provider.of<MemberProvider>(context, listen: false);
    _utilApiProvider = Provider.of<UtilApiProvider>(context, listen: false);
    _gameStreetProvider =
        Provider.of<GameStreetProvider>(context, listen: false);
    _memberModel ??= ModalRoute.of(context)?.settings.arguments as MemberModel?;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: AppColor.appBackgroundColor,
      body: Stack(
        fit: StackFit.loose,
        children: [
          _yellowBackGround(),
          //街道
          Positioned(bottom: 0, child: _menuBackGround()),
          //家具內擺設
          _interior(),
          //底部功能按鈕
          Positioned(bottom: 0, child: _bottomBar()),
          //會員資訊
          Positioned(
            top: 0,
            left: 0,
            child: Consumer2<MemberProvider, GameHomeProvider>(
              builder: (context, member, game, _) {
                return _memberCard(
                    member: member.memberModel,
                    point: member.memberPoint.point!,
                    clean: member.memberPoint
                        .cleanTimes! /*game.memberCleaningTools.length*/,
                    dung: member
                        .memberPoint.shitTimes! /*game.memberDung.length*/);
              },
            ),
          ),
          //陌生人資訊
          Positioned(
              top: 0,
              right: 0,
              child: SizedBox(
                height: 10.h,
                width: 45.w,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    //大頭照
                    MugshotWidget(
                      fromMemberId: _memberProvider!.memberModel.id!,
                      nickName: _memberModel!.nickname!,
                      gender: _memberModel!.gender!,
                      birthday: _memberModel!.birthday!,
                      targetMemberId: _memberModel!.id!,
                    ),
                    Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        //名稱
                        Container(
                          child: FittedBox(
                            child: Text(
                              _memberModel!.nickname!,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18.sp,
                                  color: Colors.black),
                            ),
                          ),
                          margin: const EdgeInsets.only(left: 10),
                        ),
                        //性別年齡
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            getGender(_memberModel!.gender!),
                            //年齡
                            Container(
                              margin: const EdgeInsets.only(left: 5),
                              child: Text(
                                Util().getAge(_memberModel!.birthday!),
                                style: const TextStyle(
                                    color: Colors.grey, fontSize: 18),
                              ),
                            )
                          ],
                        ),
                      ],
                    )
                  ],
                ),
              )),
          //左側功能按鈕
          Positioned(left: 3.w, top: 18.h, child: _leftBar()),
          //購物區
          Positioned(bottom: 0, child: mallWidget()),
        ],
      ),
    );
  }

  //黃色背景
  Widget _yellowBackGround() {
    return SizedBox(
      width: Device.width,
      height: Device.height,
      child: Image(
        fit: BoxFit.fill,
        filterQuality: FilterQuality.medium,
        image: AssetImage(AppImage.imgBgHome),
      ),
    );
  }

  //選單背景
  Widget _menuBackGround() {
    return SizedBox(
      height: _menuHeight,
      width: Device.width,
      child: Image(
        fit: BoxFit.fill,
        filterQuality: FilterQuality.medium,
        image: AssetImage(AppImage.imgBgMenu),
      ),
    );
  }

  //左側門
  Widget _leftBar() {
    return Consumer<GameHomeProvider>(
      builder: (context, home, _) {
        return Offstage(
          offstage: home.editMode,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              //離開
              _leaveRoom(),
              SizedBox(
                height: 2.h,
              ),
            ],
          ),
        );
      },
    );
  }

  //離開房間
  Widget _leaveRoom() {
    return GestureDetector(
      onTap: () {
        if (delegate.pages
            .where((element) => element.name! == RouteName.gameHomePage)
            .isEmpty) {
          _positionProvider!.setAppPosition(position: AppPosition.game);
        }
        delegate.popRoute();
      },
      child: SizedBox(
        width: _leftWidth,
        child: Image.asset(AppImage.btnLeave),
      ),
    );
  }

  //家具內擺設
  Widget _interior() {
    return SizedBox(
      height: _screenHeight,
      child: InteractiveViewer(
        minScale: 1,
        maxScale: 2,
        transformationController: transformationController,
        constrained: false,
        child: Stack(
          alignment: Alignment.bottomLeft,
          children: [
            //空白 撐stack用
            Container(
              height: _screenHeight,
            ),
            //背景
            _roomBg(),
            //櫃子
            Positioned(bottom: 20.h, right: 17.w, child: _cabinet()),
            //圍籬
            Positioned(bottom: 3.h, right: 13.w, child: _fence()),
            //狗屋
            Positioned(bottom: 25.h, left: 30.w, child: _doghouse()),
            //狗
            Positioned(bottom: 15.h, left: 22.w, child: _dog()),
            //椅子
            Positioned(
              bottom: 15.h,
              left: 77.w,
              child: _chair(),
            ),
            //踏墊
            Positioned(
              bottom: 2.h,
              left: 22.w,
              child: _mat(),
            ),
            //桌子
            Positioned(bottom: 15.5.h, left: 99.w, child: _table()),
            //便便1
            Positioned(
                bottom: 3.h,
                left: 30.w,
                child: (_gameHomeProvider!
                            .memberStreetHouseDungModelList.isEmpty ||
                        _gameHomeProvider!.memberStreetHouseDungModelList
                            .where((element) => element.posTop == 1)
                            .isEmpty)
                    ? onPoopWidget(position: 1)
                    : poopWidget(position: 1)),
            //便便2
            Positioned(
                bottom: 20.h,
                left: 55.w,
                child: (_gameHomeProvider!
                            .memberStreetHouseDungModelList.isEmpty ||
                        _gameHomeProvider!.memberStreetHouseDungModelList
                            .where((element) => element.posTop == 2)
                            .isEmpty)
                    ? onPoopWidget(position: 2)
                    : poopWidget(position: 2)),
            //便便4
            Positioned(
                bottom: 2.h,
                left: 97.w,
                child: (_gameHomeProvider!
                            .memberStreetHouseDungModelList.isEmpty ||
                        _gameHomeProvider!.memberStreetHouseDungModelList
                            .where((element) => element.posTop == 4)
                            .isEmpty)
                    ? onPoopWidget(position: 4)
                    : poopWidget(position: 4)),
            //便便5
            Positioned(
                bottom: 5.h,
                left: 130.w,
                child: (_gameHomeProvider!
                            .memberStreetHouseDungModelList.isEmpty ||
                        _gameHomeProvider!.memberStreetHouseDungModelList
                            .where((element) => element.posTop == 5)
                            .isEmpty)
                    ? onPoopWidget(position: 5)
                    : poopWidget(position: 5)),

            //便便3
            Positioned(
                bottom: 22.h,
                left: 110.w,
                child: (_gameHomeProvider!
                            .memberStreetHouseDungModelList.isEmpty ||
                        _gameHomeProvider!.memberStreetHouseDungModelList
                            .where((element) => element.posTop == 3)
                            .isEmpty)
                    ? onPoopWidget(position: 3)
                    : poopWidget(position: 3)),
            //便便6
            Positioned(
                bottom: 2.h,
                right: 120.w,
                child: (_gameHomeProvider!
                            .memberStreetHouseDungModelList.isEmpty ||
                        _gameHomeProvider!.memberStreetHouseDungModelList
                            .where((element) => element.posTop == 6)
                            .isEmpty)
                    ? onPoopWidget(position: 6)
                    : poopWidget(position: 6)),
            //便便8
            Positioned(
                bottom: 2.h,
                right: 54.w,
                child: (_gameHomeProvider!
                            .memberStreetHouseDungModelList.isEmpty ||
                        _gameHomeProvider!.memberStreetHouseDungModelList
                            .where((element) => element.posTop == 8)
                            .isEmpty)
                    ? onPoopWidget(position: 8)
                    : poopWidget(position: 8)),
            //便便7
            Positioned(
                bottom: 21.h,
                right: 65.w,
                child: (_gameHomeProvider!
                            .memberStreetHouseDungModelList.isEmpty ||
                        _gameHomeProvider!.memberStreetHouseDungModelList
                            .where((element) => element.posTop == 7)
                            .isEmpty)
                    ? onPoopWidget(position: 7)
                    : poopWidget(position: 7)),
            //便便9
            Positioned(
                bottom: 20.h,
                right: 110.w,
                child: (_gameHomeProvider!
                            .memberStreetHouseDungModelList.isEmpty ||
                        _gameHomeProvider!.memberStreetHouseDungModelList
                            .where((element) => element.posTop == 9)
                            .isEmpty)
                    ? onPoopWidget(position: 9)
                    : poopWidget(position: 9)),
          ],
        ),
      ),
    );
  }

  //背景
  Widget _roomBg() {
    return SizedBox(
        height: _screenHeight - 10.h,
        child: Consumer<GameHomeProvider>(
          builder: (context, home, _) {
            if (home.streetMemberWallpaperPath != '') {
              if (home.streetMemberWallpaperPath.contains('https')) {
                return CachedNetworkImage(
                  fit: BoxFit.contain,
                  filterQuality: FilterQuality.medium,
                  imageUrl: home.streetMemberWallpaperPath,
                );
              } else {
                return Image.file(
                  File(home.streetMemberWallpaperPath),
                  fit: BoxFit.fill,
                );
              }
            }
            return Image.asset(
              AppImage.imgItemsRoomBg0,
              fit: BoxFit.fill,
            );
          },
        ));
  }

  //狗
  Widget _dog() {
    return SizedBox(
        width: 40.w,
        child: Consumer<GameHomeProvider>(
          builder: (context, home, _) {
            if (home.streetMemberPetsPath != '') {
              if (home.streetMemberPetsPath.contains('https')) {
                return CachedNetworkImage(
                  fit: BoxFit.contain,
                  filterQuality: FilterQuality.medium,
                  imageUrl: home.streetMemberPetsPath,
                );
              } else {
                return Image.file(
                  File(home.streetMemberPetsPath),
                  fit: BoxFit.fill,
                );
              }
            }
            return Image.asset(
              AppImage.imgItemsDog0,
              fit: BoxFit.fill,
            );
          },
        ));
  }

  //狗屋
  Widget _doghouse() {
    return SizedBox(
      width: 45.w,
      child: Consumer<GameHomeProvider>(
        builder: (context, home, _) {
          if (home.streetMemberDogHousePath != '') {
            if (home.streetMemberDogHousePath.contains('https')) {
              return CachedNetworkImage(
                fit: BoxFit.contain,
                filterQuality: FilterQuality.medium,
                imageUrl: home.streetMemberDogHousePath,
              );
            } else {
              return Image.file(
                File(home.streetMemberDogHousePath),
                fit: BoxFit.fill,
              );
            }
          }
          return Container();
        },
      ),
    );
  }

  //踏墊
  Widget _mat() {
    return SizedBox(
      width: 50.w,
      child: Consumer<GameHomeProvider>(
        builder: (context, home, _) {
          if (home.streetMemberMatPath != '') {
            if (home.streetMemberMatPath.contains('https')) {
              return CachedNetworkImage(
                fit: BoxFit.contain,
                filterQuality: FilterQuality.medium,
                imageUrl: home.streetMemberMatPath,
              );
            } else {
              return Image.file(
                File(home.streetMemberMatPath),
                fit: BoxFit.fill,
              );
            }
          }
          return Container();
        },
      ),
    );
  }

  //圍籬
  Widget _fence() {
    return SizedBox(
        width: 53.w,
        height: 21.h,
        child: Consumer<GameHomeProvider>(
          builder: (context, home, _) {
            if (home.streetMemberFencePath != '') {
              if (home.streetMemberFencePath.contains('https')) {
                return CachedNetworkImage(
                  fit: BoxFit.contain,
                  filterQuality: FilterQuality.medium,
                  imageUrl: home.streetMemberFencePath,
                );
              } else {
                return Image.file(
                  File(home.streetMemberFencePath),
                  fit: BoxFit.fill,
                );
              }
            }
            return Image.asset(
              AppImage.imgItemsFence1,
              fit: BoxFit.fill,
            );
          },
        ));
  }

  //櫃子
  Widget _cabinet() {
    return SizedBox(
        height: 40.h,
        child: Consumer<GameHomeProvider>(
          builder: (context, home, _) {
            if (home.streetMemberCabinetPath != '') {
              if (home.streetMemberCabinetPath.contains('https')) {
                return CachedNetworkImage(
                  fit: BoxFit.contain,
                  filterQuality: FilterQuality.medium,
                  imageUrl: home.streetMemberCabinetPath,
                );
              } else {
                return Image.file(
                  File(home.streetMemberCabinetPath),
                  fit: BoxFit.fill,
                );
              }
            }
            return Image.asset(
              AppImage.imgItemsCabinet0,
            );
          },
        ));
  }

  //椅子
  Widget _chair() {
    return SizedBox(
        width: 120.w,
        child: Consumer<GameHomeProvider>(
          builder: (context, home, _) {
            if (home.streetMemberChairPath != '') {
              if (home.streetMemberChairPath.contains('https')) {
                return CachedNetworkImage(
                  fit: BoxFit.contain,
                  filterQuality: FilterQuality.medium,
                  imageUrl: home.streetMemberChairPath,
                );
              } else {
                return Image.file(
                  File(home.streetMemberChairPath),
                  fit: BoxFit.fill,
                );
              }
            }
            return Image.asset(
              AppImage.imgItemsBed0,
            );
          },
        ));
  }

  //桌子
  Widget _table() {
    return SizedBox(
        width: 49.w,
        child: Consumer<GameHomeProvider>(
          builder: (context, home, _) {
            if (home.streetMemberTablePath != '') {
              if (home.streetMemberTablePath.contains('https')) {
                return CachedNetworkImage(
                  fit: BoxFit.contain,
                  filterQuality: FilterQuality.medium,
                  imageUrl: home.streetMemberTablePath,
                );
              } else {
                return Image.file(
                  File(home.streetMemberTablePath),
                  fit: BoxFit.fill,
                );
              }
            }
            return Image.asset(
              AppImage.imgItemsTable1,
            );
          },
        ));
  }

  //底部功能列按鈕
  Widget _bottomBar() {
    return Container(
      width: Device.width,
      padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 2.h),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          //房子
          GestureDetector(
            onTap: () => _houseOnTap(),
            child: SizedBox(
              width: _bottomWidth,
              child: Image.asset(AppImage.btnHomeSelected),
            ),
          ),
          //街道
          GestureDetector(
            onTap: () => _streetOnTap(),
            child: SizedBox(
              width: _bottomWidth,
              child: Image.asset(AppImage.btnStreet),
            ),
          ),
          //設定
          GestureDetector(
            onTap: () => _showSettingDialog(),
            child: SizedBox(
              width: _bottomWidth,
              child: Image.asset(AppImage.btnSettings),
            ),
          ),
        ],
      ),
    );
  }

  //房子按鈕OnTap
  void _houseOnTap() {
    _gameHomeProvider!.tidyMemberFurniture(refresh: true);
    _gameHomeProvider!.tidyMemberHouse(refresh: true);
    _gameHomeProvider!.tidyMemberPets(refresh: true);
    delegate.popAndPushRouter(name: RouteName.gameHomePage);
  }

  //街道按鈕OnTap
  void _streetOnTap() {
    _positionProvider!.setAppPosition(position: AppPosition.game);
    delegate.popRoute();
  }

  //設定按鈕OnTap
  void _showSettingDialog() async {
    bool? result = await showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return SettingGameDialog(
            key: UniqueKey(),
          );
        });
    if (result != null) {
      if (result) {
        _gameStreetProvider!
            .setMemberSetting(id: _memberProvider!.memberModel.id!)
            .then((value) {
          _gameStreetProvider!.getNearMe(
              id: _memberProvider!.memberModel.id!,
              member: _memberProvider!.memberModelMe);
        });
      }
    }
  }

  //region 自己人物
  ///會員資訊
  /// * [member] 大頭照
  /// * [clean]  清潔次數
  /// * [dung]  大便次數
  Widget _memberCard(
      {required member,
      required int point,
      required int clean,
      required int dung}) {
    return SizedBox(
      height: 15.h,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              //大頭照
              Container(
                  height: 9.h,
                  margin: EdgeInsets.only(left: 3.w),
                  alignment: Alignment.center,
                  decoration: const BoxDecoration(color: Colors.transparent),
                  child: meAvatarImage(member)),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  //個人資料
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 1.h),
                    child: Row(
                      children: [
                        //名稱
                        Container(
                          child: FittedBox(
                            child: Text(
                              member.nickname!,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18.sp,
                                  color: Colors.black),
                            ),
                          ),
                          margin: const EdgeInsets.only(left: 10),
                        ),
                        //性別年齡
                        Container(
                          margin: const EdgeInsets.only(left: 10),
                          child: Row(
                            children: [
                              getGender(member.gender!),
                              //年齡
                              Container(
                                margin: const EdgeInsets.only(left: 5),
                                child: Text(
                                  Util().getAge(member.birthday!),
                                  style: const TextStyle(
                                      color: Colors.grey, fontSize: 18),
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 1.w),
                      child: _pointWidget(point: point))
                ],
              ),
            ],
          ),
          SizedBox(
            height: 1.5.h,
          ),
          Row(
            children: [
              //清潔次數
              _cleanWidget(frequency: clean),
              //大便次數
              _dongWidget(frequency: dung)
            ],
          )
        ],
      ),
    );
  }

  //點數
  Widget _pointWidget({required int point}) {
    return Stack(
      children: [
        Container(
          margin: EdgeInsets.only(left: 2.8.w),
          padding: const EdgeInsets.all(2),
          width: 26.w,
          height: 3.7.h,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(30),
              border: Border.all(color: AppColor.pinkRedColor, width: 2)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              //空格
              SizedBox(
                width: 5.w,
              ),
              //數字
              Expanded(
                child: FittedBox(
                  child: Text(
                    '$point',
                    style: const TextStyle(
                        color: Colors.black, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              //+ icon
              if (_utilApiProvider!.systemSetting!.addPointsStatus !=
                  PointsStatus.closure.index)
                GestureDetector(
                  onTap: () => delegate.push(name: RouteName.myPointsPage),
                  child: SizedBox(child: Image.asset(AppImage.iconTopUp)),
                )
            ],
          ),
        ),
        //鑽石
        Positioned(
          left: 0,
          top: -0.2.h,
          child:
              SizedBox(height: 4.h, child: Image.asset(AppImage.iconDiamond)),
        ),
      ],
    );
  }

  ///清潔次數
  /// * [frequency] 次數
  Widget _cleanWidget({required int frequency}) {
    return Container(
      margin: EdgeInsets.only(left: 2.8.w),
      padding: const EdgeInsets.all(2),
      width: 22.w,
      height: 3.8.h,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(30),
          border: Border.all(color: AppColor.pinkRedColor, width: 2)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          //掃把
          Image(
            image: AssetImage(AppImage.iconClean),
          ),
          //數字
          Expanded(
            child: Consumer<UtilApiProvider>(
              builder: (context, util, _) {
                return FittedBox(
                  child: Text(
                    '$frequency/${util.gameSetting.cleanLimit}',
                    style: const TextStyle(
                        color: Colors.black, fontWeight: FontWeight.bold),
                  ),
                );
              },
            ),
          ),
          //+ icon
          GestureDetector(
            onTap: () => _addCleaningTools(),
            child: SizedBox(child: Image.asset(AppImage.iconTopUpCleanAndPoop)),
          )
        ],
      ),
    );
  }

  ///大便次數
  /// * [frequency] 次數
  Widget _dongWidget({required int frequency}) {
    return Container(
      margin: EdgeInsets.only(left: 2.8.w),
      padding: const EdgeInsets.all(2),
      width: 22.w,
      height: 3.7.h,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(30),
          border: Border.all(color: AppColor.pinkRedColor, width: 2)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          //大便
          Image(
            image: AssetImage(AppImage.iconPoop),
          ),
          //數字
          Expanded(
            child: Consumer<UtilApiProvider>(
              builder: (context, util, _) {
                return FittedBox(
                  child: Text(
                    '$frequency/${util.gameSetting.shitLimit}',
                    style: const TextStyle(
                        color: Colors.black, fontWeight: FontWeight.bold),
                  ),
                );
              },
            ),
          ),
          //+ icon
          GestureDetector(
            onTap: () => _addDong(),
            child: SizedBox(child: Image.asset(AppImage.iconTopUpCleanAndPoop)),
          )
        ],
      ),
    );
  }

  void _addDong() async {
    //恢復大便次數
    if (_memberProvider!.memberPoint.point! <
        _utilApiProvider!.gameSetting.recoverShitPoints!) {
      EasyLoading.showToast(S.of(context).please_add_value);
      return;
    }
    if (_memberProvider!.memberPoint.shitTimes! == 2) {
      EasyLoading.showToast(S.of(context).the_limit_has_been_reached);
      return;
    }
    bool? result = await showGeneralDialog(
      context: context,
      pageBuilder: (context, anim1, anim2) {
        return PayRecoverTimersDialog(
          key: UniqueKey(),
          title: S.of(context).buy,
          recoverPoints: _utilApiProvider!.gameSetting.recoverShitPoints!,
          limit: _utilApiProvider!.gameSetting.shitLimit!,
          confirmButtonText: S.of(context).confirm,
          cancelButtonText: S.of(context).cancel,
        );
      },
    );
    if (result != null) {
      if (result) {
        EasyLoading.show(status: S.of(context).purchasing);
        bool r = await _gameHomeProvider!
            .postRecoverShitTimes(_memberProvider!.memberModel.id!);
        if (r) {
          await _memberProvider!
              .getMemberPoint(_memberProvider!.memberModel.id!);
          EasyLoading.dismiss();
        } else {
          EasyLoading.showError('${S.of(context).buy}${S.of(context).fail}');
        }
      }
    }
  }

  ///買清潔工具
  void _addCleaningTools() async {
    /*//購買清掃次數框
    Map<String, dynamic>? result = await showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return PurchasesDialog(
            key: UniqueKey(),
            title:
                '${S.of(context).buy}${S.of(context).clean_up}${S.of(context).frequency}',
            price: _utilApiProvider!.cleaningToolsList.first.cost!,
            confirmButtonText: S.of(context).confirm,
          );
        });
    if (result != null) {
      if (result['status']) {
        EasyLoading.show(status: S.of(context).purchasing);
        AddMemberItemDTO _addMemberItem = AddMemberItemDTO(
            memberId: _memberProvider!.memberModel.id!,
            itemId: _utilApiProvider!.cleaningToolsList.first.id!,
            qty: result['value']);
        _stompClientProvider!
            .sendAddMemberCleaningTools(addMemberItemDTO: _addMemberItem);
      }
    }*/
    //恢復清潔次數
    if (_memberProvider!.memberPoint.point! <
        _utilApiProvider!.gameSetting.recoverCleanPoints!) {
      EasyLoading.showToast(S.of(context).please_add_value);
      return;
    }
    if (_memberProvider!.memberPoint.cleanTimes! == 2) {
      EasyLoading.showToast(S.of(context).the_limit_has_been_reached);
      return;
    }
    bool? result = await showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return PayRecoverTimersDialog(
              key: UniqueKey(),
              title: S.of(context).buy,
              recoverPoints: _utilApiProvider!.gameSetting.recoverCleanPoints!,
              limit: _utilApiProvider!.gameSetting.recoverShitPoints!,
              confirmButtonText: S.of(context).confirm,
              cancelButtonText: S.of(context).cancel);
        });
    if (result != null) {
      if (result) {
        EasyLoading.show(status: S.of(context).purchasing);
        bool r = await _gameHomeProvider!
            .postRecoverCleanTimes(_memberProvider!.memberModel.id!);
        if (r) {
          await _memberProvider!
              .getMemberPoint(_memberProvider!.memberModel.id!);
          EasyLoading.dismiss();
        } else {
          EasyLoading.showError('${S.of(context).buy}${S.of(context).fail}');
        }
      }
    }
  }
//endregion

  ///便便widget
  /// * [position] 位置,
  Widget poopWidget({required int position}) {
    return Consumer<GameHomeProvider>(
      builder: (context, home, _) {
        if (home.memberStreetHouseDungModelList
            .where((element) => element.posTop == position)
            .isNotEmpty) {
          MemberHouseDungModel _dung = home.memberStreetHouseDungModelList
              .firstWhere((element) => element.posTop == position);
          if (home.memberStreetHouseMessageList
              .where((element) => element.id == _dung.memberHouseMessageId)
              .isNotEmpty) {
            MemberHouseMessageModel _message = home.memberStreetHouseMessageList
                .firstWhere(
                    (element) => element.id == _dung.memberHouseMessageId);
            return SizedBox(
              width: 30.w,
              height: 18.h,
              child: Stack(
                alignment: Alignment.center,
                children: [
                  //大便
                  Positioned(
                    bottom: 0,
                    left: 0,
                    child: SizedBox(
                        width: 22.w,
                        child: CachedNetworkImage(
                          imageUrl: home.getDungImage(id: _dung.dungId!),
                          fit: BoxFit.contain,
                        )),
                  ),
                  //頭像
                  Positioned(
                    top: 0,
                    left: 0,
                    child: Container(
                        width: 15.w,
                        margin: EdgeInsets.symmetric(horizontal: 4.w),
                        padding: EdgeInsets.symmetric(horizontal: 0.7.w),
                        alignment: Alignment.center,
                        decoration: const BoxDecoration(
                            color: Colors.white, shape: BoxShape.circle),
                        child: MugshotWidget(
                          fromMemberId: _memberProvider!.memberModel.id!,
                          nickName: _dung.member!.nickname!,
                          gender: _dung.member!.gender!,
                          birthday: _dung.member!.birthday!,
                          targetMemberId: _dung.memberId!,
                        )),
                  ),
                  //時間
                  Positioned(
                      bottom: 7.h,
                      right: 0,
                      child: Container(
                        width: 17.w,
                        height: 4.h,
                        padding: EdgeInsets.only(
                            left: 2.w, right: 2.w, bottom: 0.7.h),
                        decoration: BoxDecoration(
                            image: DecorationImage(
                                image: AssetImage(AppImage.imgDialog),
                                fit: BoxFit.fill)),
                        child: FittedBox(
                            child: Text(Util().poopTime(_message.createdAt!))),
                      )),
                ],
              ),
            );
          } else {
            return Container();
          }
        } else {
          return Container();
        }
      },
    );
  }

  ///沒有便便Widget
  /// * [position] 位置
  Widget onPoopWidget({required position}) {
    return GestureDetector(
      onTap: () {
        if (_gameHomeProvider!.memberStreetHouseMessageList
            .where((element) =>
                element.memberId == _memberProvider!.memberModel.id!)
            .isEmpty) {
          if (_onTapPoopIndex != position) {
            _onTapPoopIndex = position;
            setState(() {});
            _gameHomeProvider!.setMessageMode(value: true);
          }
        } else {
          EasyLoading.showToast(
              S.of(context).you_have_had_a_good_time_at_this_member_house);
        }
      },
      child: Container(
        padding: const EdgeInsets.all(15),
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            color: _onTapPoopIndex != position
                ? Colors.white
                : AppColor.gooseYellowColor,
            border: Border.all(color: AppColor.brownColor, width: 5)),
        child:
            SizedBox(width: 15.w, child: Image.asset(AppImage.imgItemsShit0)),
      ),
    );
  }

  Widget mallWidget() {
    return Consumer<GameHomeProvider>(builder: (context, home, _) {
      return Offstage(
        offstage: !home.messageMode,
        child: SizedBox(
          width: Device.width,
          child: Column(
            children: [
              Container(
                margin: EdgeInsets.symmetric(vertical: 0.5.h),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    _buy(),
                    SizedBox(
                      width: 2.w,
                    ),
                    _check(),
                    SizedBox(width: 2.w),
                    _cancel(),
                    SizedBox(width: 2.w),
                  ],
                ),
              ),
              _interiorList(),
              _itemList(),
            ],
          ),
        ),
      );
    });
  }

  //大便選項
  Widget _interiorList() {
    return Container(
      height: 7.h,
      color: AppColor.orangeColor,
      alignment: Alignment.centerLeft,
      child: Container(
        width: 20.w,
        margin: EdgeInsets.symmetric(horizontal: 2.w, vertical: 1.h),
        padding: EdgeInsets.symmetric(horizontal: 3.w, vertical: 1.h),
        alignment: Alignment.center,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5),
            border: Border.all(color: AppColor.brownColor, width: 2),
            color: AppColor.beigeColor),
        child: Text(
          S.of(context).poop,
          style: const TextStyle(color: AppColor.brownColor),
        ),
      ),
    );
  }

  Widget _buy() {
    return Consumer<GameHomeProvider>(
      builder: (context, home, _) {
        return GestureDetector(
          onTap: () => _buyOnClick(home.allDungList, home.itemIndex),
          child: SizedBox(
            width: 14.w,
            child: Image.asset(AppImage.btnBuy),
          ),
        );
      },
    );
  }

  ///購買點擊
  void _buyOnClick(_list, int _itemIndex) async {
    //log('我要買的大便: ${_list[_itemIndex].name}');
    if (_list[_itemIndex].memberDefault) {
      EasyLoading.showToast(S.of(context).impossible_to_buy);
      return;
    }
    //購買大便次數框
    /*Map<String, dynamic>?*/ bool? result = await showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return GameShoppingDialog(
            key: UniqueKey(),
            title: '${S.of(context).buy}${S.of(context).poop}',
            content: _list[_itemIndex].cost.toString(),
            confirmButtonText: S.of(context).confirm,
          );
        });
    if (result != null) {
      if (result /*result['status']*/) {
        EasyLoading.show(status: S.of(context).purchasing);
        AddMemberItemDTO _addMemberItem = AddMemberItemDTO(
            memberId: _memberProvider!.memberModel.id!,
            itemId: _list[_itemIndex].id!,
            qty: 1 /*result['value']*/);
        _stompClientProvider!
            .sendAddMemberDung(addMemberItemDTO: _addMemberItem);
      }
    }
  }

  Widget _check() {
    return GestureDetector(
      onTap: () => _setInterior(),
      child: SizedBox(
        width: 9.w,
        child: Image.asset(AppImage.btnConfirm),
      ),
    );
  }

  ///留言
  void _setInterior() async {
    if (!_gameHomeProvider!.allDungList[_gameHomeProvider!.itemIndex].isBuy!) {
      EasyLoading.showToast(S.of(context).buy_this_poop_first);
      return;
    }
    Map<String, dynamic>? _result = await showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return MemberHouseMessageDialog(
            key: UniqueKey(),
            title: S.of(context).leave_comments,
            confirmButtonText: S.of(context).sure,
          );
        },
        barrierColor: Colors.black54,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 100),
        transitionBuilder: (context, anim1, anim2, child) {
          return child;
        });
    if (_result != null) {
      if (_result['status']) {
        if(_memberProvider!.memberPoint.shitTimes == 0){
          EasyLoading.showToast(S.of(context).not_enough_shit);
          return;
        }
        //寵物大便Show
        if (!util.isShowDogDungDialog) {
          PetsModel? _model;
          if (_gameHomeProvider!.noPigPetsList
              .where((element) => element.isUsed == true)
              .isNotEmpty) {
            _model = _gameHomeProvider!.noPigPetsList
                .where((element) => element.isUsed == true)
                .first;
          }
          showGeneralDialog(
              context: context,
              pageBuilder: (context, anim1, anim2) {
                return ShowDogDungDialog(
                  key: UniqueKey(),
                  gifPath: _model?.gif,
                );
              },
              barrierColor: Colors.black54,
              barrierDismissible: false,
              transitionDuration: const Duration(milliseconds: 100),
              transitionBuilder: (context, anim1, anim2, child) {
                return child;
              });
        }
        MemberHouseMessageDTO messageModel = MemberHouseMessageDTO(
          memberHouseId: _gameHomeProvider!.thisPeopleModel.id,
          memberId: _memberProvider!.memberModel.id!,
          message: _result['value'],
        );
        _gameHomeProvider!
            .addMemberHouseMessage(model: messageModel)
            .then((value) {
          if (value['status']) {
            MemberHouseDungModel dungModel;
            if (_gameHomeProvider!.memberDung
                .where((element) =>
                    element.dungId ==
                        _gameHomeProvider!
                            .allDungList[_gameHomeProvider!.itemIndex].id! &&
                    element.memberDefault == true)
                .isNotEmpty) {
              /*log('我使用的大便 ${_gameHomeProvider!.memberDung
                  .firstWhere((element) => element.dungId == _gameHomeProvider!.allDungList[_gameHomeProvider!.itemIndex].id! && element.memberDefault == true ).toJson()}');*/
              dungModel = MemberHouseDungModel(
                  memberDungId: _gameHomeProvider!.memberDung
                      .firstWhere((element) =>
                          element.dungId ==
                              _gameHomeProvider!
                                  .allDungList[_gameHomeProvider!.itemIndex]
                                  .id! &&
                          element.memberDefault == true)
                      .id!,
                  dungId: _gameHomeProvider!
                      .allDungList[_gameHomeProvider!.itemIndex].id!,
                  memberHouseMessageId: value['value'].id!,
                  memberHouseId: _gameHomeProvider!.thisPeopleModel.id,
                  memberId: _memberProvider!.memberModel.id!,
                  posTop: _onTapPoopIndex!,
                  posLeft: _onTapPoopIndex!);
            } else {
              /*log('我使用的大便 ${_gameHomeProvider!.memberDung
                  .firstWhere((element) => element.dungId == _gameHomeProvider!.allDungList[_gameHomeProvider!.itemIndex].id!).toJson()}');*/
              dungModel = MemberHouseDungModel(
                  memberDungId: _gameHomeProvider!.memberDung
                      .firstWhere((element) =>
                          element.dungId ==
                          _gameHomeProvider!
                              .allDungList[_gameHomeProvider!.itemIndex].id!)
                      .id!,
                  dungId: _gameHomeProvider!
                      .allDungList[_gameHomeProvider!.itemIndex].id!,
                  memberHouseMessageId: value['value'].id!,
                  memberHouseId: _gameHomeProvider!.thisPeopleModel.id,
                  memberId: _memberProvider!.memberModel.id!,
                  posTop: _onTapPoopIndex!,
                  posLeft: _onTapPoopIndex!);
            }

            _gameHomeProvider!
                .addMemberHouseDung(model: dungModel)
                .then((value) async {
              await Future.wait([
                _gameHomeProvider!.getMemberDungByMemberId(
                    id: _memberProvider!.memberModel.id!, refresh: true),
                //用memberId取得會員家留言
                _gameHomeProvider!.getStreetMemberHouseMessage(
                    id: _gameHomeProvider!.thisPeopleModel.id!),
                //用memberId取得會員家大便
                _gameHomeProvider!.getStreetMemberDungByMemberHouseId(
                    id: _gameHomeProvider!.thisPeopleModel.id!),

                _memberProvider!
                    .getMemberPoint(_memberProvider!.memberModel.id!),
              ]);

              if (util.isShowDogDungDialog) {
                Navigator.of(context).pop();
                util.isShowDogDungDialog = false;
              }
              setState(() {});
            });
          }
        });
      }
    }
  }

  //X
  Widget _cancel() {
    return Consumer<GameHomeProvider>(
      builder: (context, home, _) {
        return GestureDetector(
          onTap: () {
            //關閉大便模式
            home.setMessageMode(value: false);
            _onTapPoopIndex = 0;
            setState(() {});
          },
          child: SizedBox(
            width: 9.w,
            child: Image.asset(AppImage.btnCancel),
          ),
        );
      },
    );
  }

  //品項類別
  Widget _itemList() {
    return Container(
      height: 25.w,
      color: AppColor.brownColor,
      child: Consumer<GameHomeProvider>(
        builder: (context, home, _) {
          return ListView.builder(
              scrollDirection: Axis.horizontal,
              itemCount: home.allDungList.length,
              itemBuilder: (context, index) {
                return _itemCard(index: index);
              });
        },
      ),
    );
  }

  ///品項類別卡
  ///* [index] 選中的類別
  Widget _itemCard({required int index}) {
    return Consumer<GameHomeProvider>(builder: (context, home, _) {
      return GestureDetector(
        onTap: () {
          if (home.itemIndex != index) {
            home.setItemIndex(value: index);
          }
        },
        child: Container(
          height: 25.w,
          width: 25.w,
          margin: EdgeInsets.symmetric(horizontal: 2.w, vertical: 1.h),
          padding: EdgeInsets.symmetric(horizontal: 3.w),
          alignment: Alignment.center,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              border: Border.all(
                  color: home.itemIndex == index ? Colors.orange : Colors.white,
                  width: 4),
              color: Colors.white),
          child: Column(
            children: [
              SizedBox(
                  width: 17.w,
                  height: 7.h,
                  child: FutureBuilder(
                    future: home.getImage(
                        url: Util()
                            .getMemberMugshotUrl(home.allDungList[index].pic!),
                        refresh: false),
                    builder: (context, AsyncSnapshot<String> snapshot) =>
                        _builderWidget(snapshot),
                  )),
              Expanded(
                  child: Container(
                child: !home.allDungList[index].memberDefault!
                    ? home.allDungList[index].isBuy!
                        ? _furnitureIsUsed(
                            isUsed: home.allDungList[index].isBuy!)
                        : _diamondAndCost(cost: home.allDungList[index].cost!)
                    : _defaultDung(),
              ))
            ],
          ),
        ),
      );
    });
  }

  ///圖片加載判斷
  Widget _builderWidget(AsyncSnapshot<Object> snapshot) {
    if (snapshot.connectionState == ConnectionState.done) {
      if (snapshot.data!.toString().contains('https')) {
        return CachedNetworkImage(
          imageUrl: snapshot.data!.toString(),
          fit: BoxFit.contain,
        );
      } else {
        return Image(
          fit: BoxFit.contain,
          image: FileImage(File(snapshot.data!.toString())),
        );
      }
    }
    return const CircularProgressIndicator();
  }

  ///是否使用中
  /// * [isUsed] 使用
  Widget _furnitureIsUsed({required bool isUsed}) {
    if (isUsed) {
      //使用中
      return Image(
        width: 6.w,
        image: AssetImage(AppImage.iconBought),
      );
    } else {
      //未使用
      return Image(
        width: 6.w,
        image: AssetImage(AppImage.iconBoughtGray),
      );
    }
  }

  ///鑽石與價格
  /// * [cost] 價格
  Widget _diamondAndCost({required int cost}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
            width: 5.w,
            child: Image(
              image: AssetImage(AppImage.iconDiamond),
            )),
        Text(
          '$cost',
          style: const TextStyle(fontWeight: FontWeight.bold),
        ),
      ],
    );
  }

  ///預設大便
  Widget _defaultDung() {
    return Text(
      S.of(context).defaultDung,
      style: const TextStyle(fontWeight: FontWeight.bold),
    );
  }
}
