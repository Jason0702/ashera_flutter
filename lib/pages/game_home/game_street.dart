import 'dart:developer';

import 'package:ashera_flutter/app.dart';
import 'package:ashera_flutter/enum/app_enum.dart';
import 'package:ashera_flutter/enum/app_position_enum.dart';
import 'package:ashera_flutter/provider/game_home_provider.dart';
import 'package:ashera_flutter/provider/game_street_provider.dart';
import 'package:ashera_flutter/provider/member_provider.dart';
import 'package:ashera_flutter/provider/stomp_client_provider.dart';
import 'package:ashera_flutter/provider/util_api_provider.dart';
import 'package:ashera_flutter/utils/no_shadow_scroll_behavior.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../dialog/game_guidelines_dialog.dart';
import '../../dialog/setting_game_dialog.dart';
import '../../provider/position_provider.dart';
import '../../routers/route_name.dart';
import '../../utils/app_color.dart';
import '../../utils/app_image.dart';
import '../../utils/shared_preference.dart';
import '../../widget/house_widget.dart';

class GamePage extends StatefulWidget {
  const GamePage({Key? key}) : super(key: key);

  @override
  _GamePageState createState() => _GamePageState();
}

class _GamePageState extends State<GamePage>{
  MemberProvider? _memberProvider;
  StompClientProvider? _stompClientProvider;
  GameStreetProvider? _gameStreetProvider;
  UtilApiProvider? _utilApiProvider;
  PositionProvider? _positionProvider;
  GameHomeProvider? _gameHomeProvider;

  //街道高度
  static final double _streetHeight = 33.h;

  //功能列按鈕寬度
  static final double _bottomWidth = 20.w;

  _onLayoutDone(_) async {
    _positionProvider!.setAppPosition(position: AppPosition.game);
    //會員沒有初始化就初始化
    if (_memberProvider!.memberModel.initGame != 1) {
      _stompClientProvider!.sendMemberInitGame();
      //打開遊戲指引
      _showGameGuidelines();
    }else{
      //檢查大便狀態
      _stompClientProvider!.sendCheckMemberDefaultDung();
      //檢查清潔工具狀態
      _stompClientProvider!.sendCheckMemberDefaultCleaningTools();
    }
    if(_memberProvider!.memberModel.longitude == null || _memberProvider!.memberModel.latitude == null){

    }
  }

  void _showGameGuidelines(){
    showGeneralDialog(context: context,
        pageBuilder: (context, anim1, anim2){
          return GameGuidelinesDialog(
            key: UniqueKey(),
          );
        });
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  Widget build(BuildContext context) {
    _positionProvider = Provider.of<PositionProvider>(context, listen: false);
    _memberProvider = Provider.of<MemberProvider>(context, listen: false);
    _stompClientProvider = Provider.of<StompClientProvider>(context, listen: false);
    _utilApiProvider = Provider.of<UtilApiProvider>(context, listen: false);
    _gameStreetProvider = Provider.of<GameStreetProvider>(context, listen: false);
    _gameHomeProvider = Provider.of<GameHomeProvider>(context, listen: false);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: AppColor.appBackgroundColor,
      body: Stack(
        fit: StackFit.loose,
        children: [
          Container(
            height: MediaQuery.of(context).size.height,
          ),
          //天空
          _skyBackGround(),
          //街道
          Positioned(bottom: 0, child: _streetBackGround()),
          //房子
          Positioned(bottom: _streetHeight, child: _houseList()),
          //功能按鈕
          Positioned(bottom: 0, child: _bottomBar()),
          //離開按鈕
          Positioned(top: 10, right: 20, child: _leaveButton()),
          //點數
          Container(
              margin: EdgeInsets.only(top: 1.h),
              width: Device.width,
              alignment: Alignment.topCenter,
              child: _pointWidget()),
        ],
      ),
    );
  }

  //天空背景
  Widget _skyBackGround() {
    return SizedBox(
      child: Image(
        fit: BoxFit.fill,
        filterQuality: FilterQuality.medium,
        image: AssetImage(AppImage.imgBgSky),
      ),
    );
  }

  //街道背景
  Widget _streetBackGround() {
    return SizedBox(
      height: _streetHeight,
      width: Device.width,
      child: Image(
        fit: BoxFit.fill,
        filterQuality: FilterQuality.medium,
        image: AssetImage(AppImage.imgBgStreet),
      ),
    );
  }

  //房間列表
  Widget _houseList() {
    return SizedBox(
      width: Device.width,
      height: Device.height,
      child: ScrollConfiguration(
        behavior: NoShadowScrollBehavior(),
        child: Consumer<GameStreetProvider>(builder: (context, game, _){
          return ListView.builder(
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              addAutomaticKeepAlives: true,
              itemCount: game.nearMeList.length,
              itemBuilder: (context, index) {
                return HouseWidget(
                  memberModel: game.nearMeList[index],
                  index: index,
                );
              });
        },),
      ),
    );
  }

  //功能列按鈕
  Widget _bottomBar() {
    return Container(
      width: Device.width,
      padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 2.h),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          //房子
          GestureDetector(
            onTap: () => _houseOnTap(),
            child: SizedBox(
              width: _bottomWidth,
              child: Image.asset(AppImage.btnHome),
            ),
          ),
          //街道
          GestureDetector(
            child: SizedBox(
              width: _bottomWidth,
              child: Image.asset(AppImage.btnStreetSelected),
            ),
          ),
          //設定
          GestureDetector(
            onTap: () => _showSettingDialog(),
            child: SizedBox(
              width: _bottomWidth,
              child: Image.asset(AppImage.btnSettings),
            ),
          ),
        ],
      ),
    );
  }

  //設定按鈕OnTap
  void _showSettingDialog() async {
    bool? result = await showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return SettingGameDialog(key: UniqueKey(),);
        });
    if(result != null){
      if(result){
        _gameStreetProvider!.setMemberSetting(id: _memberProvider!.memberModel.id!).then((value) {
          _gameStreetProvider!.getNearMe(id: _memberProvider!.memberModel.id!, member: _memberProvider!.memberModelMe);
        });
      }
    }
  }

  //房子按鈕OnTap
  void _houseOnTap() async {
    _gameHomeProvider!.tidyMemberFurniture(refresh: true);
    _gameHomeProvider!.tidyMemberHouse(refresh: true);
    _gameHomeProvider!.tidyMemberPets(refresh: true);
    delegate.push(name: RouteName.gameHomePage);
  }

  //離開按鈕
  Widget _leaveButton() {
    return GestureDetector(
      onTap: () {
        _positionProvider!.setAppPosition(position: AppPosition.home);
        Navigator.pop(context);
      },
      child: const Icon(
        Icons.close,
        color: Colors.grey,
        size: 40,
      ),
    );
  }

  //點數
  Widget _pointWidget() {
    return Stack(
      children: [
        Container(
          margin: EdgeInsets.only(left: 3.w),
          padding: const EdgeInsets.all(2),
          width: 32.w,
          height: 4.5.h,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(30),
              border: Border.all(color: AppColor.pinkRedColor, width: 2)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              //空格
              SizedBox(
                width: 8.w,
              ),
              //數字
              Expanded(
                child: FittedBox(
                  child: Consumer<MemberProvider>(builder: (context, member, _){
                    return Text(
                      '${member.memberPoint.point!}',
                      style: const TextStyle(
                          color: Colors.black, fontWeight: FontWeight.bold),
                    );
                  },),
                ),
              ),
              //+ icon
              if(_utilApiProvider!.systemSetting!.addPointsStatus != PointsStatus.closure.index)
              GestureDetector(
                onTap: () => delegate.push(name: RouteName.myPointsPage),
                child: SizedBox(child: Image.asset(AppImage.iconTopUp)),
              )
            ],
          ),
        ),
        //鑽石
        Positioned(
          left: 0,
          top: -0.3.h,
          child:
              SizedBox(height: 4.8.h, child: Image.asset(AppImage.iconDiamond)),
        ),
      ],
    );
  }

}
