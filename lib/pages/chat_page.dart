import 'dart:convert';
import 'dart:developer';

import 'package:ashera_flutter/models/member_model.dart';
import 'package:ashera_flutter/models/message_record_model.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_font_icons/flutter_font_icons.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../app.dart';
import '../enum/app_enum.dart';
import '../generated/l10n.dart';
import '../models/get_chat_message_dto.dart';
import '../provider/member_black_list_provider.dart';
import '../provider/member_provider.dart';
import '../provider/menu_status_provider.dart';
import '../provider/stomp_client_provider.dart';
import '../routers/route_name.dart';
import '../utils/api.dart';
import '../utils/app_color.dart';
import '../utils/app_image.dart';
import '../utils/no_shadow_scroll_behavior.dart';
import '../utils/util.dart';
import '../dialog/menu_dialog.dart';
import '../widget/avatar_image.dart';
import '../widget/titlebar_widget.dart';

class ChatPage extends StatefulWidget {
  const ChatPage({Key? key}) : super(key: key);

  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  StompClientProvider? _stompClientProvider;
  MemberProvider? _memberProvider;
  MemberBlackListProvider? _memberBlackListProvider;

  final TextEditingController _searchText = TextEditingController();
  MenuStatusProvider? _menuStatusProvider;
  //好友
  final List<MemberModel> _friendList = [];

  //顯示用
  List<MemberModel> items = [];

  @override
  Widget build(BuildContext context) {
    _stompClientProvider = Provider.of<StompClientProvider>(context);
    _memberProvider = Provider.of<MemberProvider>(context);
    _memberBlackListProvider = Provider.of<MemberBlackListProvider>(context);
    _refreshFriend();
    return Consumer<MenuStatusProvider>(builder: (context, menuStatus, _) {
      _menuStatusProvider = menuStatus;
      return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: AppColor.appBackgroundColor,
        body: menuStatus.menuStatus == MenuStatus.menu
            ? //選單
            MenuWidget(key: UniqueKey(),)
            : //聊天
            Column(
                children: [
                  //選項 與 標題
                  titleBarIntegrate([addFriend()], S.of(context).chat, []),
                  //搜尋
                  Container(
                    width: 90.w,
                    margin: const EdgeInsets.only(top: 13, bottom: 13),
                    decoration: const BoxDecoration(color: Colors.transparent),
                    child: TextField(
                      style: const TextStyle(color: Colors.black),
                      onChanged: (value) {
                        _filterSearchResults(value);
                      },
                      controller: _searchText,
                      decoration: InputDecoration(
                        contentPadding: const EdgeInsets.only(
                            left: 20.0, right: 10.0, top: 10.0, bottom: 10.0),
                        enabledBorder: OutlineInputBorder(
                            borderSide: const BorderSide(color: Colors.grey),
                            borderRadius: BorderRadius.circular(25.0)),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(25.0),
                          borderSide: const BorderSide(color: Colors.grey),
                        ),
                        focusedBorder: OutlineInputBorder(
                            borderSide: const BorderSide(color: Colors.grey),
                            borderRadius: BorderRadius.circular(25.0)),
                        prefixIcon: const Icon(Icons.search),
                        fillColor: Colors.transparent,
                        filled: true,
                        hintStyle: const TextStyle(color: Colors.grey),
                        hintText: S.of(context).search,
                      ),
                    ),
                  ),
                  //內容
                  Expanded(flex: 8, child: _bodyFriend())
                ],
              ),
      );
    });
  }

  //選項
  Widget addFriend() {
    return GestureDetector(
      onTap: () => _barsOnTap(),
      child: const Icon(
        FontAwesome.bars,
        color: AppColor.appTitleBarTextColor,
        size: 25,
      ),
    );
  }

  Widget _bodyFriend() {
    return Container(
      decoration: const BoxDecoration(color: Colors.white),
      child: Column(
        children: [
          //聊天列表
          Expanded(
              child: ScrollConfiguration(
            behavior: NoShadowScrollBehavior(),
            child: items.isEmpty ? SizedBox(
              width: Device.width,
              height: Device.height,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image(
                    width: 20.w,
                    height: 20.h,
                    image: AssetImage(AppImage.imgWaiting),
                  ),
                  Text(
                    S.of(context).there_is_currently_no_chat_history,
                    style: TextStyle(
                        color: Colors.grey[300]!,
                        fontWeight: FontWeight.bold,
                        fontSize: 20.sp),
                  )
                ],
              ),
            ) : ListView.builder(
                shrinkWrap: true,
                itemCount: items.length,
                itemBuilder: (context, index) {
                  return chatCard(index);
                }),
          ))
        ],
      ),
    );
  }

  //聊天卡片
  Widget chatCard(int index) {
    return GestureDetector(
      onTap: () => chatListOnTap(index),
      child: Container(
        padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
        decoration: const BoxDecoration(
            border:
                Border(bottom: BorderSide(width: 1, color: AppColor.grayLine))),
        child: Row(
          children: [
            //頭貼
            Consumer<StompClientProvider>(builder: (context, friend, _) {
              return friend.friendList
                          .where((element) => element.name == items[index].name)
                          .first
                          .mugshot ==
                      null
                  ? noAvatarImage() //沒照片
                  : friend.friendList
                  .where((element) => element.name == items[index].name)
                  .first
                  .mugshot!.isEmpty ? noAvatarImage()
                  : Container(
                      width: 16.w,
                      height: 8.h,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                            fit: BoxFit.cover,
                            image: CachedNetworkImageProvider(
                              Util().getMemberMugshotUrl(friend.friendList
                                  .where((element) =>
                                      element.name == items[index].name)
                                  .first
                                  .mugshot!),
                              headers: {"authorization": "Bearer " + Api.accessToken},
                            ),
                            onError: (error, stackTrace) {
                              debugPrint('Error: ${error.toString()}');
                              if (error
                                  .toString()
                                  .contains('statusCode: 404')) {
                                _stompClientProvider!.setFriendAvatarNull(friend
                                    .friendList
                                    .where((element) =>
                                        element.name == items[index].name)
                                    .first
                                    .name!);
                              }
                            }),
                      ),
                    ); //有照片
            }),
            Expanded(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  //名稱
                  Container(
                    child: Consumer<StompClientProvider>(
                      builder: (context, friend, _) {
                        return Text(
                          friend.friendList
                              .where((element) =>
                          element.name == items[index].name)
                              .first
                              .nickname!,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 18.sp,
                              color: Colors.black),
                        );
                      },
                    ),
                    margin: const EdgeInsets.only(left: 10),
                  ),
                  SizedBox(
                    height: 0.5.h,
                  ),
                  //最後訊息
                  _lastMessage(items[index].name!),
                ],
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                //最後訊息時間
                _lastTime(items[index].name!),
                SizedBox(
                  height: 0.5.h,
                ),
                //紅點
                _lastUnreadMessageCount(items[index].name!)
              ],
            )
          ],
        ),
      ),
    );
  }

  //最後一筆訊息
  Widget _lastMessage(String _name) {
    return Consumer<StompClientProvider>(builder: (context, friend, _) {
      if (friend.messageRecordList
          .where((element) =>
              element.fromMember == _name || element.targetMember == _name)
          .isNotEmpty) {
        List<MessageLastRecordModel> _messageRecordData = friend
            .messageRecordList
            .where((element) =>
                element.fromMember == _name || element.targetMember == _name)
            .toList();
        _messageRecordData
            .sort((first, last) => last.updatedAt!.compareTo(first.updatedAt!));
        if (Util.messageType[_messageRecordData.first.type] == MessageType.TEXT) {
          //如果是文字
          return Container(
            child: Text(
              _messageRecordData.first.content!,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
              style: TextStyle(fontSize: 16.sp, color: AppColor.grayText),
            ),
            margin: const EdgeInsets.only(left: 10),
          );
        } else if (Util.messageType[_messageRecordData.first.type] ==
            MessageType.PIC) {
          //如果是圖片
          return Container(
            child: FittedBox(
              child: Text(
                S.of(context).sent_a_picture,
                style: TextStyle(fontSize: 16.sp, color: AppColor.grayText),
              ),
            ),
            margin: const EdgeInsets.only(left: 10),
          );
        } else if (Util.messageType[_messageRecordData.first.type] ==
            MessageType.VIDEO) {
          //如果是影片
          return Container(
            child: FittedBox(
              child: Text(
                S.of(context).sent_a_video,
                style: TextStyle(fontSize: 16.sp, color: AppColor.grayText),
              ),
            ),
            margin: const EdgeInsets.only(left: 10),
          );
        } else {
          //如果沒有Type
          return Container();
        }
      } else {
        //如果沒有訊息
        return Container();
      }
    });
  }

  //最後一筆訊息時間
  Widget _lastTime(String _name) {
    return Consumer<StompClientProvider>(
      builder: (context, friend, _) {
        if (friend.messageRecordList
            .where((element) =>
                element.fromMember == _name || element.targetMember == _name)
            .isNotEmpty) {
          //如果有消息 就有時間
          List<MessageLastRecordModel> _messageRecordData = friend
              .messageRecordList
              .where((element) =>
                  element.fromMember == _name || element.targetMember == _name)
              .toList();
          _messageRecordData.sort(
              (first, last) => last.updatedAt!.compareTo(first.updatedAt!));
          return Container(
            width: 15.w,
            alignment: Alignment.center,
            child: FittedBox(
              child: Text(
                Util().lastMessageTime(_messageRecordData.first.updatedAt!),
                style: TextStyle(fontSize: 18.sp, color: AppColor.grayText),
              ),
            ),
          );
        } else {
          //如果沒有訊息
          return Container();
        }
      },
    );
  }

  //未讀消息數量
  Widget _lastUnreadMessageCount(String _name) {
    return Consumer<StompClientProvider>(builder: (context, friend, _) {
      if (friend.messageRecordList
          .where((element) => element.fromMember == _name)
          .isNotEmpty) {
        MessageLastRecordModel _messageRecordData = friend.messageRecordList
            .where((element) => element.fromMember == _name)
            .first;
        if (_messageRecordData.unreadCnt != 0) {
          return Container(
              height: 5.w,
              width:
                  Util().redDotWidth(_messageRecordData.unreadCnt.toString()),
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: AppColor.buttonFrameColor,
                  borderRadius: BorderRadius.circular(30)),
              child: Text(
                _messageRecordData.unreadCnt.toString(),
                textScaleFactor: 1,
                style: TextStyle(
                    color: Colors.white,
                    fontSize: 16.sp,
                    fontWeight: FontWeight.bold),
              ));
        } else {
          //0 就表示 沒有未讀消息
          return Container();
        }
      } else {
        //如果沒有訊息
        return Container();
      }
    });
  }

  //點擊列表事件
  void chatListOnTap(int index) {
    //取得聊天紀錄
    GetChatMessageDTO _getChatMessageDTO = GetChatMessageDTO(
        fromMember: _memberProvider!.memberModel.name!,
        targetMember: items[index].name,
        date: '',
        page: 0,
        size: 10,
        sortBy: 'created_at');
    _stompClientProvider!
        .sendMessage(Stomp.getChatMessagePage, json.encode(_getChatMessageDTO));
    delegate.push(name: RouteName.chatRoomPage, arguments: items[index]);
  }

  //開啟選單
  void _barsOnTap() {
    _menuStatusProvider!.openMenu();
  }

  void _refreshFriend() {
    if (_searchText.text.isEmpty) {
      items.clear();
      _friendList.clear();
      //沒有紀錄的就不放進去
      for (var _value in _stompClientProvider!.friendList) {
        if (_stompClientProvider!.messageRecordList
            .where((element) =>
                element.fromMember == _value.name ||
                element.targetMember == _value.name)
            .isNotEmpty) {
          _friendList.add(_value);
        }
      }
      if(_memberBlackListProvider!.blackListByFromMember.isNotEmpty){
        //log('黑名單過濾');
        for (var element in _memberBlackListProvider!.blackListByFromMember) {
          _friendList.removeWhere((value) => value.id == element.targetMemberId);
        }
      }
      _friendList
          .sort((first, last) => last.updatedAt!.compareTo(first.updatedAt!));
      items = List.from(_friendList);
    } else {
      _friendList.clear();
      //沒有紀錄的就不放進去
      for (var _value in _stompClientProvider!.friendList) {
        if (_stompClientProvider!.messageRecordList
            .where((element) =>
                element.fromMember == _value.name ||
                element.targetMember == _value.name)
            .isNotEmpty) {
          _friendList.add(_value);
        }
      }
      _friendList
          .sort((first, last) => last.updatedAt!.compareTo(first.updatedAt!));
    }
  }

  //查詢
  void _filterSearchResults(String _query) {
    List<MemberModel> _dummySearchList = [];
    _dummySearchList.addAll(_friendList);
    if (_query.isNotEmpty) {
      List<MemberModel> _dummyListData = [];
      for (var item in _dummySearchList) {
        if (item.nickname!.contains(_query)) {
          _dummyListData.add(item);
        }
      }
      items.clear();
      items.addAll(_dummyListData);
      items.sort((first, last) => last.updatedAt!.compareTo(first.updatedAt!));
      setState(() {});
      return;
    } else {
      items.clear();
      items.addAll(_friendList);
      items.sort((first, last) => last.updatedAt!.compareTo(first.updatedAt!));
      setState(() {});
    }
  }
}
