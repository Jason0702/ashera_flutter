
import 'dart:developer';

import 'package:ashera_flutter/generated/l10n.dart';
import 'package:ashera_flutter/provider/member_provider.dart';
import 'package:ashera_flutter/provider/stomp_client_provider.dart';
import 'package:ashera_flutter/utils/api.dart';
import 'package:ashera_flutter/widget/titlebar_widget.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../app.dart';
import '../enum/app_enum.dart';
import '../models/follower_model.dart';
import '../models/member_model.dart';
import '../provider/util_api_provider.dart';
import '../routers/route_name.dart';
import '../utils/app_color.dart';
import '../utils/app_image.dart';
import '../utils/no_shadow_scroll_behavior.dart';
import '../utils/shared_preference.dart';
import '../utils/util.dart';
import '../widget/avatar_image.dart';

class AddFriendPage extends StatefulWidget {
  const AddFriendPage({Key? key}) : super(key: key);

  @override
  _AddFriendPageState createState() => _AddFriendPageState();
}

class _AddFriendPageState extends State<AddFriendPage> {
  UtilApiProvider? _utilApiProvider;
  MemberProvider? _memberProvider;
  StompClientProvider? _stompClientProvider;

  final TextEditingController _searchText = TextEditingController();


  _onLayoutDone(_) async {
    _stompClientProvider!.getRequestMyFollower();
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }



  @override
  Widget build(BuildContext context) {
    _utilApiProvider = Provider.of<UtilApiProvider>(context);
    _memberProvider = Provider.of<MemberProvider>(context);
    _stompClientProvider = Provider.of<StompClientProvider>(context);
    return Scaffold(
      backgroundColor: AppColor.appBackgroundColor,
      body: GestureDetector(
        onTap: () => searchAsheraId(),
        child: Column(
          children: [
            titleBarIntegrate([_backButtonWidget()], S.of(context).add_friends,
                [_showMyQrcodeWidget()]),
            //請輸入Ashera ID
            Container(
              width: 90.w,
              margin: const EdgeInsets.only(top: 13, bottom: 13),
              child: TextField(
                style: const TextStyle(color: Colors.black),
                controller: _searchText,
                decoration: InputDecoration(
                  contentPadding: const EdgeInsets.only(
                      left: 20.0, right: 10.0, top: 10.0, bottom: 10.0),
                  enabledBorder: OutlineInputBorder(
                      borderSide: const BorderSide(color: Colors.grey),
                      borderRadius: BorderRadius.circular(25.0)),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(25.0),
                    borderSide: const BorderSide(color: Colors.grey),
                  ),
                  focusedBorder: OutlineInputBorder(
                      borderSide: const BorderSide(color: Colors.grey),
                      borderRadius: BorderRadius.circular(25.0)),
                  prefixIcon: const Icon(Icons.search),
                  fillColor: Colors.transparent,
                  filled: true,
                  hintStyle: const TextStyle(color: Colors.grey),
                  hintText: S.of(context).please_enter_ashera_id,
                ),
                onEditingComplete: () {
                  SystemChannels.textInput.invokeMethod('TextInput.hide');
                  searchAsheraId();
                },
              ),
            ),
            //找尋結果
            Consumer<UtilApiProvider>(builder: (context, member, _){
              return member.memberModel != null ? _searchResultCard() : Container();
            }),
            //待加好友
            Container(
                padding: const EdgeInsets.only(left: 20, top: 0),
                alignment: Alignment.centerLeft,
                color: Colors.white,
                height: 3.h,
                child: Consumer<StompClientProvider>(builder: (context, follower, _){
                  return Text(
                    "${S.of(context).waiting_add_friends} ${follower.followerList.length}",
                    style: TextStyle(
                        fontSize: 18.sp,
                        fontWeight: FontWeight.bold,
                        color: AppColor.buttonFrameColor),
                  );
                },)),
            //好友列表
            Expanded(
                child: Container(
              color: Colors.white,
              child: Container(
                margin: const EdgeInsets.only(left: 10, top: 10),
                child: ScrollConfiguration(
                  behavior: NoShadowScrollBehavior(),
                  child: Consumer<StompClientProvider>(
                    builder: (context, follower, _){
                      return follower.followerList.isEmpty ? SizedBox(
                        width: Device.width,
                        height: Device.height,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image(
                              width: 20.w,
                              height: 20.h,
                              image: AssetImage(AppImage.imgWaiting),
                            ),
                            Text(
                              S.of(context).no_friends_to_add,
                              style: TextStyle(
                                  color: Colors.grey[300]!,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20.sp),
                            )
                          ],
                        ),
                      ) :
                      ListView.builder(
                          shrinkWrap: true,
                          itemCount: follower.followerList.length,
                          itemBuilder: (context, index) {
                            return friendCard(index);
                          });
                    },
                  ),
                ),
              ),
            ))
          ],
        ),
      ),
    );
  }

  //返回按鈕
  Widget _backButtonWidget() {
    return GestureDetector(
      onTap: () {
        _utilApiProvider!.setUidSearchMemberClear();
        delegate.popRoute();
      },
      child: const Icon(
        Icons.arrow_back,
        color: AppColor.appTitleBarTextColor,
        size: 30,
      ),
    );
  }

  //我的QRCode
  Widget _showMyQrcodeWidget() {
    return GestureDetector(
      onTap: () => _qrCodeOnTap(),
      child: const Icon(
        Icons.qr_code,
        size: 30,
      ),
    );
  }

  //列表單一樣式
  Widget friendCard(int index) {
    return Container(
      margin: const EdgeInsets.only(top: 0, bottom: 5),
      padding: EdgeInsets.symmetric(horizontal: 5.w),
      child: Consumer<StompClientProvider>(builder: (context, follower, _){
        return follower.followerList[index].followerModel != null
            ? Row(
          children: [
            //大頭照

              followerAvatarImage(index, follower),
            Expanded(
              child: Container(
                child: Text(
                  follower.followerList[index].followerModel!.nickname!,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18.sp,
                      color: Colors.black),
                ),
                margin: const EdgeInsets.only(left: 15),
              ),
            ),
            Container(
              child: FittedBox(
                child: Text(
                  S.of(context).waiting_add_friends_confirm,
                  style: TextStyle(fontSize: 17.sp, color: AppColor.grayText),
                ),
              ),
              margin: const EdgeInsets.only(left: 10),
            ),
          ],
        )
            : Container();
      },),
    );
  }

  Widget _searchResultCard() {
    return Consumer2<UtilApiProvider, StompClientProvider>(builder: (context, member, friend, _) {
      return Container(
        padding: EdgeInsets.symmetric(horizontal: 5.w, vertical: 5),
        decoration: const BoxDecoration(
            color: Colors.white,
            border:
                Border(bottom: BorderSide(color: AppColor.grayLine, width: 1))),
        child: Row(
          children: [
            //大頭照
            member.memberModel!.mugshot == null
                ? noAvatarImage()
                : Container(
              width: 16.w,
              height: 8.h,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                image: DecorationImage(
                    fit: BoxFit.cover,
                    image: CachedNetworkImageProvider(
                      Util().getMemberMugshotUrl(member.memberModel!.mugshot!),
                      headers: {"authorization": "Bearer " + Api.accessToken},),
                    onError: (error, stackTrace){
                      debugPrint('Error: ${error.toString()}');
                      if(error.toString().contains('statusCode: 404')){

                      }
                    }
                ),
              ),
            ),
            Expanded(
              child: Container(
                child: Text(
                  member.memberModel!.nickname!,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18.sp,
                      color: Colors.black),
                ),
                margin: const EdgeInsets.only(left: 15),
              ),
            ),
            //判斷加過還是能加
            _isAddOrCanAdd(member.memberModel!, friend.friendList, friend.followerList),
          ],
        ),
      );
    });
  }

  Widget _isAddOrCanAdd(MeModel memberModel, List<MemberModel> friendList, List<FollowerModel> followerList){
    if(friendList.where((element) => element.asheraUid == memberModel.asheraUid).isEmpty && followerList.where((element) => element.followerModel!.asheraUid == memberModel.asheraUid).isEmpty){
      return GestureDetector(
        onTap: () => _addFriendOnTap(),
        child: FittedBox(
          child: Icon(
            Icons.person_add_alt_1,
            color: AppColor.femaleColor,
            size: 7.w,
          ),
        ),
      );
    } else {
      if(friendList.where((element) => element.asheraUid == memberModel.asheraUid).isNotEmpty){
        return FittedBox(
          child: Text(
            S.of(context).added_friend,
            style: const TextStyle(color: AppColor.femaleColor),
          ),
        );
      } else {
        return FittedBox(
          child: Text(
            S.of(context).waiting_add_friends,
            style: const TextStyle(color: AppColor.femaleColor),
          ),
        );
      }
    }
  }

  //加好友
  void _addFriendOnTap(){
    _stompClientProvider!.sendFollowerRequest(_memberProvider!.memberModel.id!, _utilApiProvider!.memberModel!.id!);
    _utilApiProvider!.setUidSearchMemberClear();//清除
  }

  void searchAsheraId() {
    SystemChannels.textInput.invokeMethod('TextInput.hide');
    if (_searchText.text.isNotEmpty && _searchText.text.trim() != _memberProvider!.memberModel.asheraUid) {
      _utilApiProvider!.getUidSearchMember(_searchText.text);
    } else if(_searchText.text.trim() == _memberProvider!.memberModel.asheraUid){
      EasyLoading.showToast(S.of(context).do_not_search_for_your_own_UID);
    } else{
      //沒輸入文字
    }
  }

  //QRCode按鈕事件
  void _qrCodeOnTap() {
    delegate.push(
        name: RouteName.cameraPage,
        arguments: {'type': PhotographOrScanning.scanning});
  }
}
