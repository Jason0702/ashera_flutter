import 'package:ashera_flutter/utils/api.dart';
import 'package:ashera_flutter/utils/app_color.dart';
import 'package:ashera_flutter/utils/shared_preference.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_font_icons/flutter_font_icons.dart';
import 'package:image_pickers/image_pickers.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../app.dart';
import '../dialog/register_face_notice_dialog.dart';
import '../enum/app_enum.dart';
import '../generated/l10n.dart';
import '../provider/member_provider.dart';
import '../provider/util_api_provider.dart';
import '../routers/route_name.dart';
import '../utils/app_size.dart';
import '../utils/util.dart';

class FaceRegisterPage extends StatefulWidget {
  const FaceRegisterPage({Key? key}) : super(key: key);

  @override
  State createState() => _FaceRegisterPageState();
}

class _FaceRegisterPageState extends State<FaceRegisterPage> {
  //原本的頭像
  String _filename = '';
  MemberProvider? _memberProvider;
  UtilApiProvider? _apiProvider;
  PhotoType _photoType = PhotoType.tmp;

  _onLayoutDone(_) async {
    if (_memberProvider!.memberModel.facePic != null) {
      _filename = _memberProvider!.memberModel.facePic!;
    }
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  Widget build(BuildContext context) {
    _memberProvider = Provider.of<MemberProvider>(context);
    _apiProvider = Provider.of<UtilApiProvider>(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: AppColor.appBackgroundColor,
      body: Column(
        children: [
          //返回 與 標題
          _titleBar(S.of(context).face_recognition),
          //內容
          Expanded(flex: 10, child: _body()),
          //上傳或略過
          _button()
        ],
      ),
    );
  }

  //返回 與 標題
  Widget _titleBar(String _title) {
    return Container(
      height: AppSize.titleBarH,
      width: Device.boxConstraints.maxWidth,
      decoration: const BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
              color: Colors.grey,
              offset: Offset(0.0, 0.5), //陰影y軸偏移量
              blurRadius: 1, //陰影模糊程度
              spreadRadius: 1 //陰影擴散程度
              )
        ],
      ),
      child: Stack(
        alignment: Alignment.center,
        children: [
          //標題文字
          FittedBox(
            child: Text(
              _title,
              style: TextStyle(
                  color: AppColor.appTitleBarTextColor,
                  fontSize: 21.sp,
                  fontWeight: FontWeight.bold),
            ),
          ),
        ],
      ),
    );
  }

  //內容
  Widget _body() {
    return Container(
      width: Device.width,
      margin: EdgeInsets.only(top: 2.h),
      decoration: const BoxDecoration(
        color: Colors.white,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(
            height: 5.h,
          ),
          //人臉辨識
          _avatarImage(),
          //請上傳一張五官清晰、正面照供人臉辨識用 / 已驗證
          SizedBox(
            height: 5.h,
          ),
          Container(
            margin: const EdgeInsets.only(right: 40, left: 40),
            child: Text(
              S.of(context).need_to_complete_the_login,
              maxLines: 2,
              textAlign: TextAlign.center,
              style: const TextStyle(color: Colors.black,
                  fontSize: 20,
                  fontWeight: FontWeight.bold),
            ),
          ),
          SizedBox(
            height: 5.h,
          ),
          //※若審核成功後需更改人臉辨識照片， 請聯繫客服
          Text(
            S.of(context).click_the_circle_for_face_recognition,
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.grey[400]!, fontSize: 18),
          ),
        ],
      ),
    );
  }

  //大頭照
  Widget _avatarImage() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          height: 2.h,
        ),
        SizedBox(
          width: 25.w * 2,
          height: 10.h * 2,
          child: Consumer<MemberProvider>(
            builder: (context, member, _) {
              return Stack(
                alignment: Alignment.center,
                children: [
                  _filename.isNotEmpty //未驗證過
                      ? GestureDetector(
                          onTap: () => _changePhoto(),
                          child: Container(
                            width: 20.w * 2,
                            height: 10.h * 2,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                  fit: BoxFit.cover,
                                  image: CachedNetworkImageProvider(
                                      Util().getMemberMugshotUrl(_filename),
                                      headers: {"authorization": "Bearer " + Api.accessToken}
                                  ),
                              ),
                            ),
                          ),
                        )
                      : GestureDetector(
                          onTap: () => _changePhoto(),
                          child: Container(
                            width: 20.w * 2,
                            height: 10.h * 2,
                            decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                color: Colors.white,
                                border: Border.all(color: AppColor.buttonFrameColor, width: 3)
                            ),
                            child: const CircleAvatar(
                              backgroundColor: Colors.transparent,
                              child: Icon(
                                FontAwesome5Solid.user,
                                size: 40 * 2,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                ],
              );
            },
          ),
        ),
      ],
    );
  }

  //修改照片
  void _changePhoto() {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return _chooseImage();
        });
  }

  //拍照或相簿選擇框
  Widget _chooseImage() {
    return Container(
      height: 15.h,
      padding: const EdgeInsets.only(top: 10),
      child: Column(
        children: [
          SizedBox(
            height: 2.h,
          ),
          //拍照
          GestureDetector(
            onTap: () => _takePhoto(),
            child: Container(
              width: 70.w,
              height: 9.h,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: Colors.white,
                  border:
                      Border.all(color: AppColor.buttonFrameColor, width: 1),
                  borderRadius: BorderRadius.circular(30)),
              child: Text(
                S.of(context).photograph,
                style: TextStyle(
                    color: AppColor.buttonFrameColor, fontSize: 20.sp),
              ),
            ),
          ),
          SizedBox(
            height: 2.h,
          ),
        ],
      ),
    );
  }

  //打開相機
  void _takePhoto() {
    Navigator.of(context).pop();
    ImagePickers.openCamera(
            cameraMimeType: CameraMimeType.photo,
            cropConfig: CropConfig(enableCrop: true, width: 2, height: 3))
        .then((media) async {
      if (media != null) {
        //上傳圖片並顯示成功或失敗
        EasyLoading.show(status: S.of(context).uploading);
        _photoType = PhotoType.face;
        _apiProvider!
            .upDataImage(
                _memberProvider!.memberModel.name!, media.path!, _photoType)
            .then((value) {
          debugPrint('拍照成功或失敗: ${value['status']}');
          if (value['status']) {
            _filename = value['filename'];
            setState(() {});
            EasyLoading.showToast(S.of(context).finish);
          } else {
            EasyLoading.showToast(S.of(context).fail);
          }
        });
      }
    });
  }

  //上傳或略過
  Widget _button() {
    return Container(
      padding: const EdgeInsets.only(left: 20, right: 20),
      decoration: const BoxDecoration(
        color: Colors.white
      ),
      child: Row(
        children: [
          //上傳
          Expanded(
              child: GestureDetector(
            onTap: () => _uploadImage(),
            child: Container(
              height: AppSize.buttonH,
              width: AppSize.buttonW,
              alignment: Alignment.center,
              padding: const EdgeInsets.all(5),
              margin: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(AppSize.buttonCircular),
                  gradient: AppColor.appMainColor),
              child: FittedBox(
                child: Text(
                  S.of(context).next_step,
                  style: TextStyle(
                      color: AppColor.buttonTextColor,
                      fontSize: 18.sp,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          )),
          //略過
          Expanded(
              child: GestureDetector(
            onTap: () => _skip(),
            child: Container(
              height: AppSize.buttonH,
              width: AppSize.buttonW,
              alignment: Alignment.center,
              padding: const EdgeInsets.all(5),
              margin: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(AppSize.buttonCircular),
                  border:
                      Border.all(color: AppColor.buttonFrameColor, width: 2)),
              child: FittedBox(
                child: Text(
                  S.of(context).skip,
                  style: TextStyle(
                      color: AppColor.buttonFrameColor,
                      fontSize: 18.sp,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ))
        ],
      ),
    );
  }
  //上傳
  void _uploadImage(){
    if (_filename.isNotEmpty && !EasyLoading.isShow) {
      EasyLoading.show(status: S.of(context).verifying);
      _apiProvider!
          .setMemberFaceImage(_memberProvider!.memberModel.id!, _filename)
          .then((value) {
        if (value) {
          _apiProvider!.putReview(id: _memberProvider!.memberModel.id!).then((value) {
            _memberProvider!
                .getMember(_memberProvider!.memberModel.id!)
                .then((value) {
              EasyLoading.showToast(S.of(context).finish);
              //跳轉
              delegate.replace(name: RouteName.appBarPage);
            });
          });
        } else {
          EasyLoading.showToast(S.of(context).failed_to_upload_again);
        }
      });
    } else {
      EasyLoading.showToast(S.of(context).please_select_an_identifying_photo);
    }
  }
  //略過
  void _skip() async {
    var result = await showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return RegisterFaceNoticeDialog(key: UniqueKey(),);
        },
        barrierColor: Colors.transparent,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 200),
        transitionBuilder: (context, anim1, anim2, child) {
          return Transform.scale(
            scale: anim1.value,
            child: child,
          );
        });
    if(result != null){
      //跳轉
      delegate.replace(name: RouteName.appBarPage);
    }
  }
}
