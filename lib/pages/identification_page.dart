
import 'package:ashera_flutter/provider/identification_provider.dart';
import 'package:ashera_flutter/provider/util_api_provider.dart';
import 'package:ashera_flutter/utils/no_shadow_scroll_behavior.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../enum/app_enum.dart';
import '../generated/l10n.dart';
import '../utils/app_color.dart';
import '../utils/app_image.dart';
import '../widget/identification_listview_card.dart';
import '../widget/titlebar_widget.dart';

class IdentificationPage extends StatefulWidget {
  const IdentificationPage({Key? key}) : super(key: key);

  @override
  State createState() => _IdentificationPageState();
}

class _IdentificationPageState extends State<IdentificationPage> {
  IdentificationProvider? _identificationProvider;

  _onLayoutDone(_) async {}

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  Widget build(BuildContext context) {
    _identificationProvider = Provider.of<IdentificationProvider>(context);
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: AppColor.appBackgroundColor,
      body: Column(
        children: [
          //返回 與 標題
          titleBar(S.of(context).identification_record),
          //內容
          Expanded(flex: 10, child: _body())
        ],
      ),
    );
  }

  //內容
  Widget _body() {
    return Column(
      children: [
        //選擇辯識過我的 還是我辯識過的
        facesSwitchButton(),
        //底下
        Expanded(
            child: ScrollConfiguration(
          behavior: NoShadowScrollBehavior(),
          child: Consumer<IdentificationProvider>(
            builder: (context, record, _) {
              if (record.facesMember == FacesMember.facesTargetMember) {
                return record.facesTargetMemberDetectHistoryRecord.isEmpty
                    ? _noDataWidget()
                    : ListView.builder(
                        addAutomaticKeepAlives: true,
                        itemCount:
                            record.facesTargetMemberDetectHistoryRecord.length,
                        itemBuilder: (context, index) {
                          return IdentificationListViewCard(
                              data: record
                                  .facesTargetMemberDetectHistoryRecord[index]);
                        });
              } else if (record.facesMember == FacesMember.facesFromMember) {
                return record.facesFromMemberDetectHistoryRecord.isEmpty
                    ? _noDataWidget()
                    : ListView.builder(
                        addAutomaticKeepAlives: true,
                        itemCount:
                            record.facesFromMemberDetectHistoryRecord.length,
                        itemBuilder: (context, index) {
                          return IdentificationListViewCard(
                              data: record
                                  .facesFromMemberDetectHistoryRecord[index]);
                        });
              } else {
                return Container();
              }
            },
          ),
        ))
      ],
    );
  }

  //選項
  Widget facesSwitchButton() {
    return Consumer<UtilApiProvider>(builder: (context, record, _) {
      return Container(
        margin: EdgeInsets.only(top: 2.h),
        padding: EdgeInsets.symmetric(horizontal: 5.w),
        width: Device.width,
        color: Colors.white,
        child: Container(
          margin: EdgeInsets.only(bottom: 2.h, top: 2.h),
          height: 8.h,
          decoration: BoxDecoration(
              color: Colors.white,
              border: Border.all(color: AppColor.grayLine, width: 1),
              borderRadius: BorderRadius.circular(30)),
          child: Row(
            children: [
              //看過我的
              Expanded(
                  child: GestureDetector(
                onTap: () => changeUserStatus(FacesMember.facesTargetMember),
                child: Container(
                  alignment: Alignment.center,
                  margin: IdentificationUtil.userStatusMargin(
                      _identificationProvider!.facesMember,
                      FacesMember.facesTargetMember),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      color: IdentificationUtil.userStatusButtonColor(
                          _identificationProvider!.facesMember,
                          FacesMember.facesTargetMember)),
                  child: Text(
                    S.of(context).saw_mine,
                    style: TextStyle(
                        fontSize: 19.sp,
                        color: IdentificationUtil.userStatusTextColor(
                            _identificationProvider!.facesMember,
                            FacesMember.facesTargetMember)),
                  ),
                ),
              )),
              SizedBox(
                width: 1.w,
              ),
              //我看過的
              Expanded(
                  child: GestureDetector(
                onTap: () => changeUserStatus(FacesMember.facesFromMember),
                child: Container(
                  alignment: Alignment.center,
                  margin: IdentificationUtil.userStatusMargin(
                      _identificationProvider!.facesMember,
                      FacesMember.facesFromMember),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      color: IdentificationUtil.userStatusButtonColor(
                          _identificationProvider!.facesMember,
                          FacesMember.facesFromMember)),
                  child: Text(
                    S.of(context).i_have_seen,
                    style: TextStyle(
                        fontSize: 19.sp,
                        color: IdentificationUtil.userStatusTextColor(
                            _identificationProvider!.facesMember,
                            FacesMember.facesFromMember)),
                  ),
                ),
              )),
            ],
          ),
        ),
      );
    });
  }

  //切換事件
  void changeUserStatus(FacesMember input) {
    _identificationProvider!.setFacesMember(faces: input);
  }

  Widget _noDataWidget() {
    return SizedBox(
      width: Device.width,
      height: Device.height,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Image(
            width: 20.w,
            height: 20.h,
            image: AssetImage(AppImage.imgWaiting),
          ),
          Text(
            S.of(context).there_are_currently_no_records,
            style: TextStyle(
                color: Colors.grey[300]!,
                fontWeight: FontWeight.bold,
                fontSize: 20.sp),
          )
        ],
      ),
    );
  }
}
