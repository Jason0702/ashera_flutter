import 'package:ashera_flutter/utils/app_color.dart';
import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../utils/app_image.dart';

class SplashPage extends StatefulWidget{
  const SplashPage({Key? key}): super(key: key);

  @override
  State createState() => _SplashPageState();
}

class _SplashPageState extends State<SplashPage>{

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: AppColor.appBackgroundColor,
      body: SizedBox(
        child: Stack(
          alignment: Alignment.center,
          children: [
            Container(
              decoration: BoxDecoration(
                gradient: AppColor.appMainColor
              ),
            ),
            Image(
              width: 25.w,
              height: 23.h,
              gaplessPlayback: true,
              filterQuality: FilterQuality.medium,
              image: AssetImage(AppImage.logoWhite),
            )
          ],
        ),
      ),
    );
  }
}