import 'dart:developer';

import 'package:ashera_flutter/enum/app_position_enum.dart';
import 'package:ashera_flutter/provider/game_street_provider.dart';
import 'package:ashera_flutter/provider/peekaboo_game_provider.dart';
import 'package:ashera_flutter/provider/stomp_client_provider.dart';
import 'package:ashera_flutter/provider/unlock_mugshot_provider.dart';
import 'package:ashera_flutter/utils/app_image.dart';
import 'package:ashera_flutter/utils/no_shadow_scroll_behavior.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:image_pickers/image_pickers.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../app.dart';
import '../dialog/verify_dialog.dart';
import '../dialog/visit_dialog.dart';
import '../enum/app_enum.dart';
import '../generated/l10n.dart';
import '../models/visit_record_dto.dart';
import '../provider/member_provider.dart';
import '../provider/position_provider.dart';
import '../provider/util_api_provider.dart';
import '../provider/visit_game_provider.dart';
import '../routers/route_name.dart';
import '../utils/app_color.dart';
import '../utils/shared_preference.dart';
import '../utils/util.dart';
import '../widget/avatar_image.dart';
import '../widget/gender_icon.dart';
import '../widget/titlebar_widget.dart';

class ColorfulSocialPage extends StatefulWidget {
  const ColorfulSocialPage({Key? key}) : super(key: key);

  @override
  State createState() => _ColorfulSocialPageState();
}

class _ColorfulSocialPageState extends State<ColorfulSocialPage> {
  PositionProvider? _positionProvider;
  VisitGameProvider? _visitGameProvider;
  StompClientProvider? _stompClientProvider;
  PeekabooGameProvider? _peekabooGameProvider;
  GameStreetProvider? _gameStreetProvider;

  //API
  UtilApiProvider? _utilApiProvider;
  UnlockMugshotProvider? _unlockMugshotProvider;

  //self
  MemberProvider? _memberProvider;

  final PhotoType _photoType = PhotoType.interactive;

  _onLayoutDone(_)async{
    Future.delayed(const Duration(milliseconds: 3000), (){
      _unlockMugshotProvider!.getUnlockPaymentRecordByFromMemberId(id: _memberProvider!.memberModel.id!, expiration: _utilApiProvider!.systemSetting!.unlockMugshotExpireDay!);
    });
    if(_memberProvider!.memberModel.initGame! == 1){
      _utilApiProvider!.getGameSetting();
      _gameStreetProvider!.getMemberGameSetting(id: _memberProvider!.memberModel.id!);
    }
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  Widget build(BuildContext context) {
    _positionProvider = Provider.of<PositionProvider>(context, listen: false);
    _visitGameProvider = Provider.of<VisitGameProvider>(context, listen: false);
    _utilApiProvider = Provider.of<UtilApiProvider>(context, listen: false);
    _memberProvider = Provider.of<MemberProvider>(context, listen: false);
    _stompClientProvider = Provider.of<StompClientProvider>(context, listen: false);
    _peekabooGameProvider = Provider.of<PeekabooGameProvider>(context, listen: false);
    _gameStreetProvider = Provider.of<GameStreetProvider>(context, listen: false);
    _unlockMugshotProvider = Provider.of<UnlockMugshotProvider>(context, listen: false);
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: AppColor.appBackgroundColor,
      body: Column(
        children: [
          //返回 與 標題
          titleBarIntegrate([],S.of(context).interactive,[]),
          //內容
          Expanded(flex: 10, child: _body())
        ],
      ),
    );
  }

  //內容
  Widget _body() {
    return Container(
      margin: EdgeInsets.only(top: 1.h),
      color: AppColor.appBackgroundColor,
      child: ScrollConfiguration(
        behavior: NoShadowScrollBehavior(),
        child: SingleChildScrollView(
          child: Column(
            children: [
              //個人資料
              Consumer<MemberProvider>(builder: (context, member, _){
                return Container(
                  margin: EdgeInsets.only(left: 5.w, bottom: 1.h, right: 5.w),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          //大頭照
                          Consumer<MemberProvider>(builder: (context, member, _) {
                            return meAvatarImage(member.memberModel);
                          }),
                          //名稱&星星&探班被探班標籤
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              //名稱 & 星星
                              Row(
                                children: [
                                  //名稱
                                  Container(
                                    child: FittedBox(
                                      child: Text(
                                        member.memberModel.nickname!,
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 18.sp,
                                            color: Colors.black),
                                      ),
                                    ),
                                    margin: const EdgeInsets.only(left: 10),
                                  ),
                                  //星星
                                  GestureDetector(
                                    onTap: () => _starOnTap(id: _memberProvider!.memberModel.id!),
                                    child: Row(
                                      children: [
                                        Container(
                                          margin: const EdgeInsets.only(left: 10, right: 5),
                                          width: 5.w,
                                          child: Image.asset(AppImage.iconStar),
                                        ),
                                        Consumer<UtilApiProvider>(
                                          builder: (context, star, _) {
                                            return Text(
                                              star.memberStar.toStringAsFixed(1),
                                              style: const TextStyle(color: Colors.black),
                                            );
                                          },
                                        )
                                      ],
                                    ),
                                  )
                                ],
                              ),
                              SizedBox(
                                height: 0.5.h,
                              ),
                              //性別年齡
                              Container(
                                margin: const EdgeInsets.only(left: 10),
                                child: Row(
                                  children: [
                                    getGender(member.memberModel.gender!),
                                    //年齡
                                    Container(
                                      margin: const EdgeInsets.only(left: 5),
                                      child: Text(
                                        Util().getAge(member.memberModel.birthday!),
                                        style: const TextStyle(color: Colors.black),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          //互動用大頭照
                          Consumer<MemberProvider>(builder: (context, member, _) {
                            return GestureDetector(
                              onTap: () => _changePhoto(),
                              child: interactiveAvatarImage(member.memberModel, member.interactiveAvatarVer),
                            );
                          }),
                          //互動照片認證
                          FittedBox(
                            child: Text(S.of(context).interactive_photo_authentication),
                          )
                        ],
                      )
                    ],
                  ),
                );
              }),
              //互動
              Container(
                width: Device.width,
                decoration: const BoxDecoration(
                    color: Colors.white
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    //互動
                    SizedBox(
                      width: 85.w,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          //互動
                          Container(
                            height: 50,
                            padding: const EdgeInsets.only(bottom: 10, top: 10),
                            alignment: Alignment.centerLeft,
                            child: FittedBox(
                              child: Text(S.of(context).interactive, style: const TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 24),),
                            ),
                          ),
                          //認證
                          Container(
                            height: 50,
                            padding: const EdgeInsets.only(bottom: 10, top: 10),
                            alignment: Alignment.centerLeft,
                            child: FittedBox(
                              child: Text(S.of(context).certification, style: const TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 18),),
                            ),
                          ),
                        ],
                      ),
                    ),
                    //探班
                    SizedBox(
                      width: 85.w,
                      child: GestureDetector(
                        onTap: () => visitOnTap(),
                        child: Container(
                          height: 30.w,
                          width: 75.w,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(
                              color: AppColor.femaleColor,
                            ),
                            borderRadius: BorderRadius.circular(10),
                            boxShadow: const [
                              BoxShadow(
                                  color: Colors.grey,
                                  offset: Offset(0.0, 0.5), //陰影y軸偏移量
                                  blurRadius: 1, //陰影模糊程度
                                  spreadRadius: 1 //陰影擴散程度
                              )
                            ],
                          ),
                          child: Stack(
                            children: [
                              Row(
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(right: 5.w),
                                    height: 31.w,
                                    width: 30.w,
                                    child: Image.asset(
                                      AppImage.imgVisit,
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                  Expanded(child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      //探班title
                                      FittedBox(
                                        child: Text(
                                          S.of(context).visit,
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold, fontSize: 20.sp),
                                        ),
                                      ),
                                      const SizedBox(
                                        height: 10,
                                      ),
                                      //探班content
                                      FittedBox(
                                        child: Text(
                                          S.of(context).visit_content,
                                          style: TextStyle(fontSize: 16.sp),
                                        ),
                                      ),
                                    ],
                                  )),
                                ],
                              ),
                              //認證
                              Positioned(
                                  right: 0,
                                  top: 0,
                                  child: Consumer<MemberProvider>(builder: (context, member, _){
                                    return Container(
                                      decoration: BoxDecoration(
                                          color: member.memberModel.interactivePic!.isNotEmpty
                                              ? AppColor.femaleColor
                                              : Colors.grey[400]!,
                                          borderRadius: const BorderRadius.only(topRight: Radius.circular(9), bottomLeft: Radius.circular(10))
                                      ),
                                      padding: const EdgeInsets.all(3),
                                      child: const Icon(
                                        Icons.check,
                                        color: Colors.white,
                                        size: 20,
                                      ),
                                    );
                                  },))
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 3.h,
                    ),
                    //躲貓貓
                    SizedBox(
                      width: 85.w,
                      child: GestureDetector(
                        onTap: () => peekabooOnTap(),
                        child: Container(
                          height: 30.w,
                          width: 75.w,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(
                              color: AppColor.femaleColor,
                            ),
                            borderRadius: BorderRadius.circular(10),
                            boxShadow: const [
                              BoxShadow(
                                  color: Colors.grey,
                                  offset: Offset(0.0, 0.5), //陰影y軸偏移量
                                  blurRadius: 1, //陰影模糊程度
                                  spreadRadius: 1 //陰影擴散程度
                              )
                            ],
                          ),
                          child: Stack(
                            children: [
                              Row(
                                children: [
                                  Container(
                                    margin: EdgeInsets.only(right: 5.w),
                                    height: 31.w,
                                    width: 30.w,
                                    child: Image.asset(
                                      AppImage.imgPeekaboo,
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                  Expanded(child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      //躲貓貓title
                                      FittedBox(
                                        child: Text(
                                          S.of(context).peekaboo,
                                          style: TextStyle(
                                              fontWeight: FontWeight.bold, fontSize: 20.sp),
                                        ),
                                      ),
                                      const SizedBox(height: 10,),
                                      //躲貓貓content
                                      FittedBox(
                                        child: Text(
                                          S.of(context).peekaboo_content,
                                          style: TextStyle(fontSize: 16.sp),
                                        ),
                                      ),
                                    ],
                                  ))
                                ],
                              ),
                              //認證
                              Positioned(
                                  right: 0,
                                  top: 0,
                                  child: Consumer<MemberProvider>(builder: (context, member, _){
                                    return Container(
                                      decoration: BoxDecoration(
                                          color: member.memberModel.interactivePic!.isNotEmpty
                                              ? AppColor.femaleColor
                                              : Colors.grey[400]!,
                                          borderRadius: const BorderRadius.only(topRight: Radius.circular(9), bottomLeft: Radius.circular(10))
                                      ),
                                      padding: const EdgeInsets.all(3),
                                      child: const Icon(
                                        Icons.check,
                                        color: Colors.white,
                                        size: 20,
                                      ),
                                    );
                                  },))
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 3.h,)
                  ],
                ),
              ),
              SizedBox(height: 1.h,),
              //遊戲
              Container(
                width: Device.width,
                decoration: const BoxDecoration(
                    color: Colors.white
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    //遊戲
                    Container(
                      width: 85.w,
                      height: 50,
                      padding: const EdgeInsets.only(bottom: 10, top: 10),
                      alignment: Alignment.centerLeft,
                      child: FittedBox(
                        child: Text(S.of(context).game, style: const TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 24),),
                      ),
                    ),
                    //遊戲
                    SizedBox(
                      width: 85.w,
                      child: GestureDetector(
                        onTap: () {
                          _gameStreetProvider!.getNearMe(id: _memberProvider!.memberModel.id!, member: _memberProvider!.memberModelMe);
                          delegate.push(name: RouteName.gamePage);
                        },
                        child: Container(
                          height: 30.w,
                          width: 75.w,
                          decoration: BoxDecoration(
                            color: Colors.white,
                            border: Border.all(
                              color: AppColor.femaleColor,
                            ),
                            borderRadius: BorderRadius.circular(10),
                            boxShadow: const [
                              BoxShadow(
                                  color: Colors.grey,
                                  offset: Offset(0.0, 0.5), //陰影y軸偏移量
                                  blurRadius: 1, //陰影模糊程度
                                  spreadRadius: 1 //陰影擴散程度
                              )
                            ],
                          ),
                          child: Row(
                            children: [
                              Container(
                                margin: EdgeInsets.only(right: 5.w),
                                height: 31.w,
                                width: 31.w,
                                child: Image.asset(
                                  AppImage.imgGame,
                                  fit: BoxFit.fill,
                                ),
                              ),
                              Expanded(child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  //小遊戲title
                                  FittedBox(
                                    child: Text(
                                      'Dirty Social',
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold, fontSize: 20.sp),
                                    ),
                                  ),
                                  //小遊戲content
                                  FittedBox(
                                    child: Text(
                                      S.of(context).game_content,
                                      style: TextStyle(fontSize: 16.sp),
                                    ),
                                  ),
                                ],
                              )),
                            ],
                          ),
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 3.h,
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  ///星星點擊
  /// * [id] 用戶ID
  void _starOnTap({required int id}){
    //log('點的到星星? $id');
    delegate.push(name: RouteName.visitStarRecordPage, arguments: id);
  }

  //按探班事件
  void visitOnTap() {
    if (!_positionProvider!.checkInitialPosition()) {
      return;
    }
    //未審核人臉無法進入
    if(_memberProvider!.memberModel.verify != VerifyType.yes.index && _memberProvider!.memberModel.verify != VerifyType.review.index){
      _showVerifyDialog();
    }else{
      if(_memberProvider!.memberModel.interactivePic!.isNotEmpty){
        _visitEntrance();
        _positionProvider!.setAppPosition(position: AppPosition.visit);
      } else {
        //互動專用照片未上傳
        EasyLoading.showToast(S.of(context).interactive_only_photos_not_uploaded);
      }
    }
  }

  //判斷入口
  void _visitEntrance() async {
    if(_utilApiProvider!.visitButton == ButtonStatus.notClick){
      EasyLoading.show(status: 'Loading...');
      _utilApiProvider!.getFindPairingAndProcessingRecordByBeingVisit(name: _memberProvider!.memberModel.name!).then((value) {
        EasyLoading.dismiss();
        if(value.isNotEmpty){
          switch(value.last.status){
          //配對中
            case 0:
              //log('是被探班');
              //判斷是否是被探班
              _visitGameProvider!.changeUserStatus(VisitMode.visitModeBeingVisit);
              _visitGameProvider!.changeVisitStatus(VisitStatus.pairing);
              _utilApiProvider!.setVisitRecordModel(value.last);
              _utilApiProvider!.clearVisitProcessingRecordList();
              delegate.push(name: RouteName.visitPage);
              break;
          //已完成
            case 1:
              break;
          //進行中
            case 2:
              if(value.last.targetMemberId == _memberProvider!.memberModel.id){
                //需回到畫面
                _utilApiProvider!.getOtherStar(value.last.fromMemberId!);
                _utilApiProvider!.setVisitRecordModel(value.last);
                _visitGameProvider!.changeUserStatus(VisitMode.visitModeBeingVisit);
                _visitGameProvider!.changeVisitStatus(VisitStatus.processing);
                delegate.push(
                    name: RouteName.visitProcessPage,
                    arguments: value.last);
              } else {
                //需回到畫面
                _utilApiProvider!.getOtherStar(value.last.targetMemberId!);
                _utilApiProvider!.setVisitRecordModel(value.last);
                _visitGameProvider!.changeUserStatus(VisitMode.visitModeVisit);
                _visitGameProvider!.changeVisitStatus(VisitStatus.processing);
                delegate.push(
                    name: RouteName.visitProcessPage,
                    arguments: value.last);
              }
              break;
          //已取消
            case 3:

              break;
          //取消等待同意
            case 4:
              if(value.last.targetMemberId == _memberProvider!.memberModel.id){
                if(value.last.cancelBy == WhoSide.target.index){
                  //log('取消我發的 lastTarget');
                } else {
                  //log('取消對方發的 lastTarget');
                  //需彈出對話框
                  Future.delayed(const Duration(milliseconds: 1000), () => _showCancelVisitDialog(_utilApiProvider!.lastTargetVisitRecord!.id!));
                }
                //需回到畫面
                _utilApiProvider!.getOtherStar(value.last.fromMemberId!);
                _utilApiProvider!.setVisitRecordModel(value.last);
                _visitGameProvider!.changeUserStatus(VisitMode.visitModeBeingVisit);
                _visitGameProvider!.changeVisitStatus(VisitStatus.cancelWaitingApprove);
                delegate.push(
                    name: RouteName.visitProcessPage,
                    arguments: value.last);
              } else {
                //需回到畫面
                if(value.last.cancelBy == WhoSide.from.index){
                  //log('取消我發的 lastFrom');
                } else {
                  //log('取消對方發的 lastFrom');
                  //需彈出對話框
                  Future.delayed(const Duration(milliseconds: 1000), () => _showCancelVisitDialog(_utilApiProvider!.lastFromVisitRecord!.id!));
                }
                _utilApiProvider!.getOtherStar(value.last.targetMemberId!);
                _utilApiProvider!.setVisitRecordModel(value.last);
                _visitGameProvider!.changeUserStatus(VisitMode.visitModeVisit);
                _visitGameProvider!.changeVisitStatus(VisitStatus.cancelWaitingApprove);
                delegate.push(
                    name: RouteName.visitProcessPage,
                    arguments: value.last);
              }
              break;
          }
        } else {
          delegate.push(name: RouteName.visitPage);
        }
      });
    }

  }

  //被取消探班同意或不同意
  void _showCancelVisitDialog(int _id) async {
    bool? result = await showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return VisitDialog(
            key: UniqueKey(),
            title: S.of(context).cancel_visit,
            content: S.of(context).do_you_agree_with_the_other_party_to_cancel_the_visit,
            confirmButtonText: '${S.of(context).agree}${S.of(context).cancel}',
            cancelButtonText: '${S.of(context).disagree}${S.of(context).cancel}',
          );
        },
        barrierColor: Colors.black54,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 100),
        transitionBuilder: (context, anim1, anim2, child) {
          return child;
        });
    if (result != null) {
      ApproveVisitRecordDTO _approveVisitRecord = ApproveVisitRecordDTO(
          id: _id, approve: result);
      if(_visitGameProvider!.userStatus == VisitMode.visitModeVisit){
        //探班
        _stompClientProvider!.sendAgreeToCancelByFromMember(approveVisitRecord: _approveVisitRecord);
      }else{
        //被探班
        _stompClientProvider!.sendAgreeToCancelByTargetMember(approveVisitRecord: _approveVisitRecord);
      }
    }
  }

  //按躲貓貓事件
  void peekabooOnTap() {
    if (!_positionProvider!.checkInitialPosition()) {
      return;
    }
    //未審核人臉無法進入
    if(_memberProvider!.memberModel.verify != VerifyType.yes.index && _memberProvider!.memberModel.verify != VerifyType.review.index){
      _showVerifyDialog();
    }else{
      if(_memberProvider!.memberModel.interactivePic!.isNotEmpty){
        _utilApiProvider!.getHideAndSeekRecordByStatus();
        _utilApiProvider!.getHideAndSeekRecordByTargetMemberId(id: _memberProvider!.memberModel.id!).then((value) {
          if(_utilApiProvider!.peekabooRecordTargetMemberList.isNotEmpty){
            switch(_utilApiProvider!.peekabooRecordTargetMemberList.last.status){
              case 0:
                _peekabooGameProvider!.setCatchStatus(status: CatchStatus.catchStatusWaiting);
                break;
              case 1:
                _peekabooGameProvider!.setCatchStatus(status: CatchStatus.catchStatusComplete);
                break;
              case 2:
                _peekabooGameProvider!.setCatchStatus(status: CatchStatus.catchStatusProcessing);
                break;
              case 3:
                _peekabooGameProvider!.setCatchStatus(status: CatchStatus.catchStatusCancel);
                break;
            }
            if(_utilApiProvider!.peekabooRecordTargetMemberList.last.status == CatchStatus.catchStatusWaiting.index || _utilApiProvider!.peekabooRecordTargetMemberList.last.status == CatchStatus.catchStatusProcessing.index){
              _peekabooGameProvider!.setPeekabooUserStatus(status: PeekabooUserStatus.sought);
            }
          }

        });
        _utilApiProvider!.getHideAndSeekRecordByFromMemberId(id: _memberProvider!.memberModel.id!);
        delegate.push(name: RouteName.peekabooPage);
      } else {

      }
    }

  }

  //顯示驗證提示框
  void _showVerifyDialog() {
    showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return VerifyDialog(key: UniqueKey(),);
        },
        barrierColor: Colors.black54,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 100),
        transitionBuilder: (context, anim1, anim2, child) {
          return Transform.scale(
            scale: anim1.value,
            child: child,
          );
        });
  }

  //修改照片
  void _changePhoto() {
    if(_memberProvider!.memberModel.facePic!.isEmpty){
      EasyLoading.showToast(S.of(context).not_yet_verified);
      return;
    }
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return _chooseImage();
        });
  }
  //拍照或相簿選擇框
  Widget _chooseImage() {
    return Container(
      height: 30.h,
      padding: const EdgeInsets.only(top: 10),
      child: Column(
        children: [
          //相片選擇
          Text(
            S.of(context).photo_selection,
            style: TextStyle(
                color: AppColor.appTitleBarTextColor, fontSize: 18.sp),
          ),
          SizedBox(
            height: 2.h,
          ),
          //拍照
          GestureDetector(
            onTap: () => _takePhoto(),
            child: Container(
              width: 70.w,
              height: 9.h,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: Colors.white,
                  border:
                      Border.all(color: AppColor.buttonFrameColor, width: 1),
                  borderRadius: BorderRadius.circular(30)),
              child: Text(
                S.of(context).photograph,
                style: TextStyle(
                    color: AppColor.buttonFrameColor, fontSize: 20.sp),
              ),
            ),
          ),
          SizedBox(
            height: 2.h,
          ),
          //相簿
          GestureDetector(
            onTap: () => _selectImage(),
            child: Container(
              width: 70.w,
              height: 9.h,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: Colors.white,
                  border:
                      Border.all(color: AppColor.buttonFrameColor, width: 1),
                  borderRadius: BorderRadius.circular(30)),
              child: Text(
                S.of(context).album,
                style: TextStyle(
                    color: AppColor.buttonFrameColor, fontSize: 20.sp),
              ),
            ),
          )
        ],
      ),
    );
  }

  //打開相機
  void _takePhoto() {
    ImagePickers.openCamera(
            cameraMimeType: CameraMimeType.photo,
            cropConfig: CropConfig(enableCrop: true, width: 2, height: 3))
        .then((media) {
      if (media != null) {
        Navigator.pop(context);
        //上傳圖片並顯示成功或失敗
        EasyLoading.show(status: S.of(context).uploading);
        _utilApiProvider!
            .upDataImage(_memberProvider!.memberModel.name!, media.path!, _photoType)
            .then((value) async{
          debugPrint('拍照成功或失敗: ${value['status']}');
          if (value['status']) {
            //先判斷是否是同一位用戶
            bool _isMember = await _utilApiProvider!.getMemberByFaceImageIsMember(value['filename'], _memberProvider!.memberModel.name!);
            if(_isMember){
              //相片更新是否成功
              bool _picUpload = await _utilApiProvider!.setInteractivePic(_memberProvider!.memberModel.id!, value['filename']);
              if(_picUpload){
                _memberProvider!.setInteractiveAvatarVer();
                _memberProvider!.getMember(_memberProvider!.memberModel.id!).then((value){
                  if(value){
                    EasyLoading.showToast(S.of(context).finish);
                  }else{
                    EasyLoading.showToast(S.of(context).fail);
                  }
                });
              }else{
                EasyLoading.showToast(S.of(context).fail);
              }
            } else {
              EasyLoading.showToast(S.of(context).fail);
            }
          } else {
            EasyLoading.showToast(S.of(context).fail);
          }
        });
      }
    });
  }

  //打開相簿
  void _selectImage() async {
    ImagePickers.pickerPaths(
            galleryMode: GalleryMode.image,
            showGif: false,
            selectCount: 1,
            showCamera: false,
            cropConfig: CropConfig(enableCrop: true, height: 1, width: 1),
            compressSize: 500,
            uiConfig: UIConfig(uiThemeColor: AppColor.buttonFrameColor))
        .then((media) {
      if (media.first.path != null) {
        Navigator.pop(context);
        //上傳圖片並顯示成功或失敗
        EasyLoading.show(status: S.of(context).uploading);
        _utilApiProvider!
            .upDataImage(_memberProvider!.memberModel.name!, media.first.path!, _photoType)
            .then((value) async{
          debugPrint('拍照成功或失敗: ${value['status']}');
          if (value['status']) {
            //先判斷是否是同一位用戶
            bool _isMember = await _utilApiProvider!.getMemberByFaceImageIsMember(value['filename'], _memberProvider!.memberModel.name!);
            if(_isMember){
              //相片更新是否成功
              bool _picUpload = await _utilApiProvider!.setInteractivePic(_memberProvider!.memberModel.id!, value['filename']);
              if(_picUpload){
                _memberProvider!.setInteractiveAvatarVer();
                _memberProvider!.getMember(_memberProvider!.memberModel.id!).then((value){
                  if(value){
                    EasyLoading.showToast(S.of(context).finish);
                  }else{
                    EasyLoading.showToast(S.of(context).fail);
                  }
                });
              }else{
                EasyLoading.showToast(S.of(context).fail);
              }
            } else {
              EasyLoading.showToast(S.of(context).fail);
            }
          } else {
            EasyLoading.showToast(S.of(context).fail);
          }
        });
      }
    });
  }
}
