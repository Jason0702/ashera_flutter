import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:ashera_flutter/app.dart';
import 'package:ashera_flutter/dialog/recharge_done_dialog.dart';
import 'package:ashera_flutter/enum/app_position_enum.dart';
import 'package:ashera_flutter/models/event_bus_model.dart';
import 'package:ashera_flutter/models/peekaboo_record_model.dart';
import 'package:ashera_flutter/models/visit_record_dto.dart';
import 'package:ashera_flutter/provider/game_home_provider.dart';
import 'package:ashera_flutter/provider/game_street_provider.dart';
import 'package:ashera_flutter/provider/identification_provider.dart';
import 'package:ashera_flutter/provider/in_app_purchase_provider.dart';
import 'package:ashera_flutter/provider/member_provider.dart';
import 'package:ashera_flutter/provider/network_status_provider.dart';
import 'package:ashera_flutter/provider/peekaboo_game_provider.dart';
import 'package:ashera_flutter/provider/position_provider.dart';
import 'package:ashera_flutter/provider/stomp_client_provider.dart';
import 'package:ashera_flutter/provider/unlock_mugshot_provider.dart';
import 'package:ashera_flutter/utils/app_image.dart';
import 'package:badges/badges.dart' as bg;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:geolocator/geolocator.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:url_launcher/url_launcher.dart';

import '../config.dart';
import '../dialog/visit_ask_cancel_dialog.dart';
import '../dialog/visit_confirm_dialog.dart';
import '../dialog/visit_dialog.dart';
import '../enum/app_enum.dart';
import '../generated/l10n.dart';
import '../main.dart';
import '../models/dung_model.dart';
import '../models/game_model.dart';
import '../models/recharge_record_model.dart';
import '../models/update_member_location_dto.dart';
import '../models/visit_record_model.dart';
import '../provider/game_shopping_refresh_provider.dart';
import '../provider/menu_status_provider.dart';
import '../provider/packages_info_provider.dart';
import '../provider/util_api_provider.dart';
import '../provider/visit_game_provider.dart';
import '../routers/route_name.dart';
import '../utils/api.dart';
import '../utils/app_color.dart';
import '../utils/app_size.dart';
import '../utils/shared_preference.dart';
import '../utils/util.dart';
import '../widget/gps_request_permission_dialpg.dart';
import 'chat_page.dart';
import 'colorful_social_page.dart';
import 'friend_page.dart';
import 'me_page.dart';

class AppBarPage extends StatefulWidget {
  const AppBarPage({Key? key}) : super(key: key);

  @override
  State createState() => _AppBarPageState();
}

class _AppBarPageState extends State<AppBarPage> {
  SharedPreferenceUtil sharedPreferenceUtil = SharedPreferenceUtil();

  //選到的項目
  BottomNavigationBarStatus _bottomNavigationBarIndex =
      BottomNavigationBarStatus.friend;
  MenuStatusProvider? _menuStatusProvider;
  PositionProvider? _positionProvider;
  UtilApiProvider? _utilApiProvider;
  NetworkStatusProvider? _networkStatusProvider;
  StompClientProvider? _stompClientProvider;
  MemberProvider? _memberProvider;
  VisitGameProvider? _visitGameProvider;
  PeekabooGameProvider? _peekabooGameProvider;
  GameHomeProvider? _gameHomeProvider;
  PackagesInfoProvider? _packagesInfoProvider;
  GameStreetProvider? _gameStreetProvider;
  UnlockMugshotProvider? _unlockMugshotProvider;
  GameShoppingRefreshProvider? _gameShoppingRefreshProvider;
  InAppPurchaseProvider? _appPurchaseProvider;
  IdentificationProvider? _identificationProvider;

  //會員Token刷新時間判斷
  Timer? _tokenExpiredTimer;

  Timer? _customerTimer;

  //商店更新
  Timer? _gameHomeCommodityTimer;

  //好友取得 假刷新
  Timer? _friendTimer;

  final List<Widget> _children = [
    const FriendPage(),
    const ChatPage(),
    const FriendPage(),
    const ColorfulSocialPage(),
    const MePage()
  ];

  _onLayoutDone(_) async {
    //取得客服
    _memberProvider!.getCustomerService(7150);
    //_memberProvider!.getCustomerService(3023);

    //取得辨識紀錄
    _identificationProvider!.getFacesDetectHistoryByTargetMemberIdAndFromMemberId(_memberProvider!.memberModel.id!);
    _upDataApp(); // <- APP更新判斷
    debugPrint('AppBar頁');
    //應用內購初始化
    _appPurchaseProvider!.appPurchaseInit();
    //刷新點數加值紀錄
    _memberProvider!.getMemberPointHistory();

    if (_memberProvider!.memberModel.age == null) {
      _memberProvider!.putAge(
          age:
              int.parse(Util().getAge(_memberProvider!.memberModel.birthday!)));
    }
    //取得關於我們
    _utilApiProvider!.getAboutUs();
    //取得備註
    EasyLoading.show(status: '');
    await _utilApiProvider!.getFriendNameByFromMemberId(id: _memberProvider!.memberModel.id!).then((value) {
      if (value != null) {
        _stompClientProvider!.setFriendName(friendName: value);
      }else{
        _stompClientProvider!.getFriends(); //取得本地所有好友
      }
    });
    EasyLoading.dismiss();
    message.getToken().then((value) => _utilApiProvider!
        .putFcmToken(id: _memberProvider!.memberModel.id!, token: value!));
    _tokenRefreshTime(); //判斷是否需要重刷Token
    _customerRefreshTime();//重新取得客服上線狀態
    _positionProvider!.checkPosition().then((value) {
      if (!value) {
        //_gPSDenied();
      }
    });
    _positionProvider!.addListener(_myListener);

    _networkStatusProvider!.init(); //開始網路狀態監聽

    _visitEventBus(); //探班監聽
    _rechargeEventBus(); //充值監聽
    _friendEventBus(); //好友監聽
    _memberUpgradeEventBus(); //用戶監聽
    _memberShoppingEventBus(); //小遊戲購物監聽

    //取得探班與被探班全部紀錄
    _utilApiProvider!
        .getAllVisitRecord(_memberProvider!.memberModel.id!)
        .then((value) {
      //log('我有多少星星: ${_utilApiProvider!.memberStar}');
    });

    //取得躲貓貓捕捉紀錄
    _utilApiProvider!.getHideAndSeekRecordByFromMemberId(id: _memberProvider!.memberModel.id!);


    //取得清潔工具列表
    _utilApiProvider!.getCleaningTools();
    //取得全部便便
    _gameHomeProvider!.getDung();
    //取全部動物類型
    _gameHomeProvider!.getAnimalType();
    //取全部家具
    _gameHomeProvider!.getFurniture();
    //取全部家具類型
    _gameHomeProvider!.getFurnitureType();
    //取全部寵物
    _gameHomeProvider!.getPets();
    //取全部房子
    _gameHomeProvider!.getHouse();
    //取全部房子風格
    _gameHomeProvider!.getHouseStyle();
    //用memberId取得會員的家
    _gameHomeProvider!
        .getMemberHouseByMemberId(id: _memberProvider!.memberModel.id!);
    if (_memberProvider!.memberModel.initGame! == 1) {
      //用memberId取得會員的便便
      _gameHomeProvider!.getMemberDungByMemberId(
          id: _memberProvider!.memberModel.id!, refresh: false);
      //用memberId取得會員的清理工具
      _gameHomeProvider!.getMemberCleaningToolsByMemberId(
          id: _memberProvider!.memberModel.id!, refresh: false);
      //用memberId取得會員的寵物
      _gameHomeProvider!.getMemberPetsByMemberId(
          id: _memberProvider!.memberModel.id!, refresh: false);
      //用memberId取得會員的家
      _gameHomeProvider!
          .getMemberHouseByMemberId(id: _memberProvider!.memberModel.id!)
          .then((value) {
        //用memberId取得會員家留言
        _gameHomeProvider!.getMemberHouseMessage(
            id: _gameHomeProvider!.memberHouse
                .where((element) => element.used == true)
                .first
                .id!);
        //用memberId取得會員家大便
        _gameHomeProvider!.getMemberHouseDung(
            id: _gameHomeProvider!.memberHouse
                .where((element) => element.used == true)
                .first
                .id!);
      });
      _gameHomeProvider!.getMemberHouseStyleByMemberId(
          id: _memberProvider!.memberModel.id!, refresh: false);
      //用memberId取得會員家具
      _gameHomeProvider!
          .getMemberFurnitureByMemberId(id: _memberProvider!.memberModel.id!);
    }

    //如果星座不對
    if(_memberProvider!.memberModel.zodiacSign! != Util().dateToHoroscope(_memberProvider!.memberModel.birthday!.replaceAll('-', '/')).name){
      _memberProvider!.changeZodiacSign(Util().dateToHoroscope(_memberProvider!.memberModel.birthday!.replaceAll('-', '/')).name);
      await Future.delayed(const Duration(milliseconds: 300));
      await _memberProvider!.putMember(_memberProvider!.memberModel.id!);
    }
  }

  void _myListener() {
    //更新用戶位置
    if(_stompClientProvider!.stompStatus == STOMPStatus.onConnect){
      _stompClientProvider!.sendUpdateMemberLocation(
          location: UpdateMemberLocationDTO(
              latitude: _positionProvider!.currentPosition.latitude,
              longitude: _positionProvider!.currentPosition.longitude));
    }
  }

  //若拒絕位置權限  開啟視窗導至設定
  void _gPSDenied() {
    showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return Container();
        },
        barrierColor: Colors.transparent,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 100),
        transitionBuilder: (context, anim1, anim2, child) {
          return Transform.scale(
            scale: anim1.value,
            child: const GPSRequestPermissionDialog(),
          );
        }).then((value) {
      if (value != null) {
        Geolocator.openAppSettings();
      }
    });
  }

  @override
  void initState() {
    super.initState();
    if (Platform.isIOS) {
      SystemChrome.setEnabledSystemUIMode(SystemUiMode.edgeToEdge,
          overlays: []);
      SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
        statusBarColor: Colors.black,
        systemNavigationBarColor: Colors.black,
        systemNavigationBarIconBrightness: Brightness.light,
        statusBarIconBrightness: Brightness.light,
        statusBarBrightness: Brightness.light,
      ));
    }
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  void dispose() {
    super.dispose();
    _positionProvider!.removeListener(_myListener);
    _positionProvider!.cancelStream();
    if(_customerTimer != null){
      _customerTimer!.cancel();
      _customerTimer = null;
    }
    if (_friendTimer != null) {
      _friendTimer!.cancel();
      _friendTimer = null;
    }
    if (_tokenExpiredTimer != null) {
      _tokenExpiredTimer!.cancel();
      _tokenExpiredTimer = null;
    }
    if (_gameHomeCommodityTimer != null) {
      _gameHomeCommodityTimer!.cancel();
      _gameHomeCommodityTimer = null;
    }
  }

  @override
  Widget build(BuildContext context) {
    _menuStatusProvider = Provider.of<MenuStatusProvider>(context);
    _positionProvider = Provider.of<PositionProvider>(context);
    _utilApiProvider = Provider.of<UtilApiProvider>(context);
    _networkStatusProvider = Provider.of<NetworkStatusProvider>(context);
    _stompClientProvider = Provider.of<StompClientProvider>(context);
    _memberProvider = Provider.of<MemberProvider>(context);
    _visitGameProvider = Provider.of<VisitGameProvider>(context);
    _peekabooGameProvider = Provider.of<PeekabooGameProvider>(context);
    _gameHomeProvider = Provider.of<GameHomeProvider>(context);
    _packagesInfoProvider = Provider.of<PackagesInfoProvider>(context);
    _gameStreetProvider = Provider.of<GameStreetProvider>(context);
    _unlockMugshotProvider = Provider.of<UnlockMugshotProvider>(context);
    _gameShoppingRefreshProvider =
        Provider.of<GameShoppingRefreshProvider>(context);
    _appPurchaseProvider = Provider.of<InAppPurchaseProvider>(context);
    _identificationProvider = Provider.of<IdentificationProvider>(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: AppColor.appBackgroundColor,
      floatingActionButton: Container(
        width: 20.w,
        height: 10.h,
        margin: EdgeInsets.only(top: 5.h),
        child: FloatingActionButton(
          onPressed: () => _scanOnTap(),
          child: Container(
            width: 20.w,
            height: 10.h,
            child: Image(
              image: AssetImage(AppImage.iconScan),
              filterQuality: FilterQuality.medium,
            ),
            decoration: const BoxDecoration(
              shape: BoxShape.circle,
              color: Colors.white,
            ),
          ),
        ),
      ),
      body: SafeArea(
        child: IndexedStack(
          children: _children,
          index: _bottomNavigationBarIndex.index,
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
        color: Colors.white,
        child: _tabs(),
      ),
    );
  }

  Widget _tabs() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        //好友
        GestureDetector(
          onTap: () => _friendOnTap(),
          child: SizedBox(
            height: AppSize.iconSizeH,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image(
                  gaplessPlayback: true,
                  width: AppSize.iconSizeW,
                  image: getAssetImage(BottomNavigationBarStatus.friend),
                ),
                Text(
                  S.of(context).friend,
                  style: TextStyle(
                      color: getTextColor(BottomNavigationBarStatus.friend)),
                )
              ],
            ),
          ),
        ),
        //聊天
        GestureDetector(
          onTap: () => _chatOnTap(),
          child: SizedBox(
            height: AppSize.iconSizeH,
            child: Consumer<StompClientProvider>(
              builder: (context, cunt, _) {
                return Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    bg.Badge(
                      badgeContent: Text(
                        '${cunt.allUnreadCunt}',
                        style: const TextStyle(
                            color: Colors.white, fontWeight: FontWeight.bold),
                      ),
                      child: Image(
                        gaplessPlayback: true,
                        width: AppSize.iconSizeW,
                        image: getAssetImage(BottomNavigationBarStatus.chat),
                      ),
                      showBadge: cunt.allUnreadCunt != 0 ? true : false,
                      animationType: bg.BadgeAnimationType.fade,
                    ),
                    Text(
                      S.of(context).chat,
                      style: TextStyle(
                          color: getTextColor(BottomNavigationBarStatus.chat)),
                    )
                  ],
                );
              },
            ),
          ),
        ),
        //空格
        SizedBox(
          width: 13.w,
          height: 1.h,
        ),
        //遊戲
        GestureDetector(
          onTap: () => _gameOnTap(),
          child: SizedBox(
            height: AppSize.iconSizeH,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image(
                  gaplessPlayback: true,
                  width: AppSize.iconSizeW,
                  image: getAssetImage(BottomNavigationBarStatus.game),
                ),
                Text(
                  S.of(context).interactive,
                  style: TextStyle(
                      color: getTextColor(BottomNavigationBarStatus.game)),
                )
              ],
            ),
          ),
        ),
        //我
        GestureDetector(
          onTap: () => _meOnTap(),
          child: SizedBox(
            height: AppSize.iconSizeH,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image(
                  gaplessPlayback: true,
                  width: AppSize.iconSizeW,
                  image: getAssetImage(BottomNavigationBarStatus.me),
                ),
                Text(
                  S.of(context).me,
                  style: TextStyle(
                      color: getTextColor(BottomNavigationBarStatus.me)),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }

  //選相機
  void _scanOnTap() {
    //跳轉相機畫面 拍照模式
    delegate.push(
        name: RouteName.cameraPage,
        arguments: {'type': PhotographOrScanning.photograph});
  }

  //選好友
  void _friendOnTap() {
    _bottomNavigationBarIndex = BottomNavigationBarStatus.friend;
    _menuStatusProvider!.closeMenu();
    setState(() {});
  }

  //選聊天
  void _chatOnTap() {
    _bottomNavigationBarIndex = BottomNavigationBarStatus.chat;
    _menuStatusProvider!.closeMenu();
    setState(() {});
  }

  //選遊戲
  void _gameOnTap() {
    //delegate.push(name: '/GamePage');
    _bottomNavigationBarIndex = BottomNavigationBarStatus.game;
    _menuStatusProvider!.closeMenu();
    setState(() {});
  }

  //選我
  void _meOnTap() {
    _bottomNavigationBarIndex = BottomNavigationBarStatus.me;
    _menuStatusProvider!.closeMenu();
    setState(() {});
  }

  Color getTextColor(BottomNavigationBarStatus _tabIndex) {
    if (_tabIndex == _bottomNavigationBarIndex) {
      return AppColor.buttonFrameColor;
    }
    return Colors.grey[400]!;
  }

  AssetImage getAssetImage(BottomNavigationBarStatus _tabIndex) {
    switch (_tabIndex) {
      case BottomNavigationBarStatus.friend:
        if (_tabIndex == _bottomNavigationBarIndex) {
          return AssetImage(AppImage.iconFriendPressed);
        } else {
          return AssetImage(AppImage.iconFriendNormal);
        }
      case BottomNavigationBarStatus.chat:
        if (_tabIndex == _bottomNavigationBarIndex) {
          return AssetImage(AppImage.iconChatPressed);
        } else {
          return AssetImage(AppImage.iconChatNormal);
        }
      case BottomNavigationBarStatus.game:
        if (_tabIndex == _bottomNavigationBarIndex) {
          return AssetImage(AppImage.iconGamePressed);
        } else {
          return AssetImage(AppImage.iconGameNormal);
        }
      case BottomNavigationBarStatus.me:
        if (_tabIndex == _bottomNavigationBarIndex) {
          return AssetImage(AppImage.iconMePressed);
        } else {
          return AssetImage(AppImage.iconMeNormal);
        }
      default:
        return AssetImage(AppImage.iconFriendNormal);
    }
  }

  //刷新Token判斷
  void _tokenRefreshTime() {
    _tokenExpiredTimer = Timer.periodic(
        const Duration(minutes: 5), (Timer t) => _tokenRefresh());
  }

  void _customerRefreshTime(){
    _customerTimer = Timer.periodic(
        const Duration(seconds: 30),
            (timer) {
              //取得客服
              _memberProvider!.getCustomerService(7150);
              //_memberProvider!.getCustomerService(3023);
            });
  }

  //刷新方法
  void _tokenRefresh() {
    sharedPreferenceUtil.getRefreshTime().then((value) {
      //取得上一次儲存Token的時間
      //log('相差時間: ${DateTime.now().difference(value)}');
      if (DateTime.now().difference(value) >=
          Duration(minutes: Util().tokenExpired)) {
        //是否需要刷新Token
        //刷新Token
        refreshToken();
      }
    });
  }

  //好友廣播監聽
  void _friendEventBus() {
    friend.on<String>().listen((event) {
      if (event == 'refreshFriend') {
        _stompClientProvider!.sendMessage(Stomp.getFollowMe, '');
      }
      if (event == 'deleteSuccess') {
        EasyLoading.showSuccess(
            '${S.of(context).delete}${S.of(context).finish}');
      }
      if (event == 'deleteFail') {
        EasyLoading.showSuccess('${S.of(context).delete}${S.of(context).fail}');
      }
      if (event == Config.refreshFacesDetectHistory){
        _identificationProvider!.getFacesDetectHistoryByTargetMemberIdAndFromMemberId(_memberProvider!.memberModel.id!);
      }
    });
  }

  //用戶升級廣播監聽
  void _memberUpgradeEventBus() {
    memberUpgrade.on<String>().listen((event) {
      if (event == 'refresh') {
        if (_memberProvider!.status == ApiStatus.idle) {
          _memberProvider!.getMember(_memberProvider!.memberModel.id!);
        }
        if (_memberProvider!.memberModel.initGame == 1 &&
            _positionProvider!.appPosition == AppPosition.game) {
          //用memberId取得會員的便便
          _gameHomeProvider!.getMemberDungByMemberId(
              id: _memberProvider!.memberModel.id!, refresh: false);
          //用memberId取得會員的清理工具
          _gameHomeProvider!.getMemberCleaningToolsByMemberId(
              id: _memberProvider!.memberModel.id!, refresh: false);
          //用memberId取得會員的寵物
          _gameHomeProvider!.getMemberPetsByMemberId(
              id: _memberProvider!.memberModel.id!, refresh: false);
          //用memberId取得會員家風格
          _gameHomeProvider!.getMemberHouseStyleByMemberId(
              id: _memberProvider!.memberModel.id!, refresh: false);
          //用memberId取得會員家具
          _gameHomeProvider!.getMemberFurnitureByMemberId(
              id: _memberProvider!.memberModel.id!);
          //用memberId取得會員遊戲設定
          _gameStreetProvider!
              .getMemberGameSetting(id: _memberProvider!.memberModel.id!)
              .then((value) {
            //用memberId取得附近會員
            _gameStreetProvider!.getNearMe(
                id: _memberProvider!.memberModel.id!,
                member: _memberProvider!.memberModelMe);
          });
        }
      }
    });
  }

  //小遊戲購物監聽
  void _memberShoppingEventBus() {
    gameShopping.on<String>().listen((event) async {
      if (_positionProvider!.appPosition == AppPosition.gameHome) {
        switch (event) {
          case 'memberShoppingRefresh':
          //購買物品
            if (_gameShoppingRefreshProvider!.refreshStatus ==
                GameRefreshStatus.shopping) {
              //log('購買物品');
              switch (_gameShoppingRefreshProvider!.stuffItem) {
                case StuffItem.none:
                  await Future.wait([
                    //刷新會員點數
                    _memberProvider!
                        .getMemberPoint(_memberProvider!.memberModel.id!),
                    //用memberId取得會員的清理工具
                    _gameHomeProvider!.getMemberCleaningToolsByMemberId(
                        id: _memberProvider!.memberModel.id!, refresh: true),
                    //用memberId取得會員的寵物
                    _gameHomeProvider!.getMemberPetsByMemberId(
                        id: _memberProvider!.memberModel.id!, refresh: true),
                    //用memberId取得會員家的風格
                    _gameHomeProvider!.getMemberHouseStyleByMemberId(
                        id: _memberProvider!.memberModel.id!, refresh: true),
                    //用memberId取得會員家具
                    _gameHomeProvider!.getMemberFurnitureByMemberId(
                        id: _memberProvider!.memberModel.id!),
                  ]);
                  _shoppingDelayEasyLoadingDismiss();
                  break;
                case StuffItem.house:
                  await Future.wait([
                    //刷新會員點數
                    _memberProvider!
                        .getMemberPoint(_memberProvider!.memberModel.id!),
                    //用memberId取得會員家的風格
                    _gameHomeProvider!.getMemberHouseStyleByMemberId(
                        id: _memberProvider!.memberModel.id!, refresh: true),
                  ]);
                  _shoppingDelayEasyLoadingDismiss();
                  break;
                case StuffItem.furniture:
                  await Future.wait([
                    //刷新會員點數
                    _memberProvider!
                        .getMemberPoint(_memberProvider!.memberModel.id!),
                    //用memberId取得會員家具
                    _gameHomeProvider!.getMemberFurnitureByMemberId(
                        id: _memberProvider!.memberModel.id!),
                  ]);
                  _shoppingDelayEasyLoadingDismiss();
                  break;
                case StuffItem.pets:
                  await Future.wait([
                    //刷新會員點數
                    _memberProvider!
                        .getMemberPoint(_memberProvider!.memberModel.id!),
                    //用memberId取得會員的寵物
                    _gameHomeProvider!.getMemberPetsByMemberId(
                        id: _memberProvider!.memberModel.id!, refresh: true),
                  ]);
                  _shoppingDelayEasyLoadingDismiss();
                  break;
                case StuffItem.dung:
                  break;
              }
            }

            //物品更換
            if (_gameShoppingRefreshProvider!.refreshStatus ==
                GameRefreshStatus.replace) {
              log('物品更換');
              _gameStreetProvider!.setHome(_memberProvider!.memberModel.id!, _gameHomeProvider!.allHouseStyleList);
              await Future.wait([
                //用memberId取得會員的寵物
                _gameHomeProvider!.getMemberPetsByMemberId(
                    id: _memberProvider!.memberModel.id!, refresh: true),
                //用memberId取得會員家的風格
                _gameHomeProvider!.getMemberHouseStyleByMemberId(
                    id: _memberProvider!.memberModel.id!, refresh: true),
                //用memberId取得會員家具
                _gameHomeProvider!.getMemberFurnitureByMemberId(
                    id: _memberProvider!.memberModel.id!),
              ]);
              _replaceDelayEasyLoadingDismiss(time: 2500);
            }

            //清理大便
            if (_gameShoppingRefreshProvider!.refreshStatus ==
                GameRefreshStatus.cleaning) {
              //用memberId取得會員家留言
              _gameHomeProvider!.getMemberHouseMessage(
                  id: _gameHomeProvider!.memberHouse
                      .where((element) => element.used == true)
                      .first
                      .id!);
              //用memberId取得會員家大便
              _gameHomeProvider!.getMemberHouseDung(
                  id: _gameHomeProvider!.memberHouse
                      .where((element) => element.used == true)
                      .first
                      .id!);
              _memberProvider!.getMemberPoint(_memberProvider!.memberModel.id!);
            }

            break;
          case 'checkMemberDefault':
            if (_gameHomeProvider!
                .memberHouse
                .where((element) => element.used == true)
                .isNotEmpty) {
              //用memberId取得會員家留言
              _gameHomeProvider!.getMemberHouseMessage(
                  id: _gameHomeProvider!
                      .memberHouse
                      .where((element) => element.used == true)
                      .first
                      .id!);
              //用memberId取得會員家大便
              _gameHomeProvider!.getMemberHouseDung(
                  id: _gameHomeProvider!
                      .memberHouse
                      .where((element) => element.used == true)
                      .first
                      .id!);
            }
            await Future.wait([
              //刷新會員點數
              _memberProvider!.getMemberPoint(_memberProvider!.memberModel.id!),
              //用memberId取得會員的清理工具
              _gameHomeProvider!.getMemberCleaningToolsByMemberId(
                  id: _memberProvider!.memberModel.id!, refresh: true),
              //用memberId取得會員的便便
              _gameHomeProvider!.getMemberDungByMemberId(
                  id: _memberProvider!.memberModel.id!, refresh: true),
            ]);
            Future.delayed(const Duration(milliseconds: 1000), () {
              EasyLoading.dismiss();
            });
            break;
        }
      }
    });

    checkPetsStatus.on<RechargeEventModel>().listen((event) {
      log('檢查大便與清潔次數');
      if (event.title == 'message') {
        MemberHouseMessageModel data = MemberHouseMessageModel.fromJson(json.decode(event.frame!.body!));

        if(data.checked == 1){
          if (_gameHomeProvider!.memberCleaningTools.isNotEmpty) {
            log('清除大便');
            //清除大便
              ClearOneMemberHouseDungDTO _model = ClearOneMemberHouseDungDTO(
                  memberCleaningToolsId:
                  _gameHomeProvider!.memberCleaningTools.first.id!,
                  memberId: _memberProvider!.memberModel.id!,
                  memberHouseId: data.memberHouseId!,
                  memberHouseDungId: _gameHomeProvider!.memberHouseDungModelList.firstWhere(
                          (element) => element.memberHouseMessageId == data.id).id!);
              _gameShoppingRefreshProvider!
                  .setRefreshStatus(status: GameRefreshStatus.cleaning);
              _stompClientProvider!
                  .sendClearOneMemberHouseDung(model: _model);
          } else {
            EasyLoading.showToast(S.of(context).not_enough_cleaning);
          }
        }

      } else if (event.title == 'pets') {
        //log('檢查寵物: ${event.title} ${json.decode(event.frame!.body!)}');
        _gameHomeProvider!.setMemberPig(event.frame!.body!);
        if (EasyLoading.isShow) {
          EasyLoading.dismiss();
        }
      }
    });
  }

  void _shoppingDelayEasyLoadingDismiss(){
    Future.delayed(const Duration(milliseconds: 2500),(){
      if(EasyLoading.isShow){
        EasyLoading.dismiss();
      }
    });
  }

  void _replaceDelayEasyLoadingDismiss({required int time}){
    Future.delayed(Duration(milliseconds: time),(){
      if(EasyLoading.isShow){
        EasyLoading.dismiss();
        _gameShoppingRefreshProvider!.deleteStuffItemList();
      }
    });
  }

  bool _isShow = false;
  //充值廣播監聽
  void _rechargeEventBus() {
    recharge.on<RechargeEventModel>().listen((event) {
      /*RechargeRecordModel _rechargeRecordModel =
          RechargeRecordModel.fromJson(json.decode(event.frame!.body!));
      //充值完成
      if (delegate.pages
          .where((element) => element.name == RouteName.bonusDialog)
          .isNotEmpty) {
        //如果 BonusDialog 還在就關閉
        delegate.popRoute();
      }
      Future.delayed(const Duration(milliseconds: 500), () {
        if (_isShow) {
          return;
        }
        //跳出加值完成
        _showRechargeDoneDialog(rechargeRecordModel: _rechargeRecordModel);
      });*/
      //刷新點數
      _memberProvider!.getMemberPoint(_memberProvider!.memberModel.id!);
      //刷新紀錄
      _memberProvider!.getMemberPointHistory();
    });
  }

  //探班廣播監聽
  void _visitEventBus() {
    visitBus.on<VisitEventModel>().listen((event) {
      //log('廣播收到?${event.title}');
      VisitRecordModel? _visitRecordModel;
      PeekabooRecordModel? _peekabooRecordModel;
      if (event.frame != null) {
        if (event.title == 'hideAndSeek' ||
            event.title == 'doCatchHideAndSeek') {
          _peekabooRecordModel =
              PeekabooRecordModel.fromJson(json.decode(event.frame!.body!));
        } else {
          _visitRecordModel =
              VisitRecordModel.fromJson(json.decode(event.frame!.body!));
        }
      }

      switch (event.title) {
        // 想要探班
        case 'wannaVisit':
          //log('-----------------想要探班!--------------');
          if (!util.dialogIsOpen &&
              _visitRecordModel!.targetMemberName ==
                  _memberProvider!.memberModel.name) {
            _utilApiProvider!.getFindLastByTargetMemberName(
                _memberProvider!.memberModel.name!); //取得被探班資料
            _showVisitDialog(_visitRecordModel.id!, _visitRecordModel);
          }
          break;
        //同意探班
        case 'approveVisit':
          if (_visitRecordModel!.targetMemberName ==
              _memberProvider!.memberModel.name) {
            //被探班
            if (_visitRecordModel.status == VisitStatus.processing.index) {
              //沒點同意就不跳轉
              if (delegate.pages
                  .where((element) => element.name == RouteName.visitProcessPage)
                  .isEmpty) {
                _utilApiProvider!.setVisitRecordModel(_visitRecordModel);
                _visitGameProvider!
                    .changeUserStatus(VisitMode.visitModeBeingVisit);
                _visitGameProvider!.changeVisitStatus(VisitStatus.processing);
                delegate.push(
                    name: RouteName.visitProcessPage, arguments: _visitRecordModel);
              } else {
                _utilApiProvider!.setVisitRecordModel(_visitRecordModel);
                _visitGameProvider!
                    .changeUserStatus(VisitMode.visitModeBeingVisit);
                _visitGameProvider!.changeVisitStatus(VisitStatus.processing);
              }
            } else {
              _utilApiProvider!.getFindProcessingRecordByBeingVisit(
                  name: _memberProvider!.memberModel.name!); //取得進行中且未完成的探班記錄
            }
          } else {
            //探班
            if (_visitRecordModel.status == VisitStatus.processing.index) {
              //對方同意
              _utilApiProvider!.setVisitRecordModel(_visitRecordModel);
              _visitGameProvider!.changeUserStatus(VisitMode.visitModeVisit);
              _visitGameProvider!.changeVisitStatus(VisitStatus.processing);
            } else {
              //對方不同意
              _visitGameProvider!.changeVisitStatus(VisitStatus.pairing);
              _showRefusalToVisitDialog();
            }
          }
          break;
        //探班評分
        case 'rateVisit':
          if (_visitRecordModel!.status == VisitStatus.complete.index) {
            _visitGameProvider!.changeUserStatus(VisitMode.visitModeVisit);
            _visitGameProvider!.changeVisitStatus(VisitStatus.pairing);
            if (delegate.pages
                .where((element) => element.name == RouteName.visitProcessPage)
                .isNotEmpty) {
              delegate.popUtil();
            }
            _utilApiProvider!.clearVisitProcessingRecordList();
          } else if (_visitRecordModel.status == VisitStatus.processing.index) {
            _utilApiProvider!.setVisitRecordModel(_visitRecordModel);
          }
          break;
        //取消探班
        case 'cancelVisit':
          _utilApiProvider!.clearVisitRecordModel();
          _visitGameProvider!.changeUserStatus(VisitMode.visitModeVisit);
          EasyLoading.showSuccess(
              '${S.of(context).cancel}${S.of(context).finish}');
          if (delegate.pages
              .where((element) => element.name == RouteName.visitPage)
              .isNotEmpty) {
            delegate.popRoute();
          }
          break;
        //重配對探班
        case 'rePairingVisit':
          break;
        //進行中探班會員取消探班資料通道
        case 'cancelByFromMemberInProcessingVisit':
          _utilApiProvider!.setVisitRecordModel(_visitRecordModel!);
          if (_visitRecordModel.cancelBy == WhoSide.from.index) {
            //探班的人發起的
            if (_visitRecordModel.fromMemberName !=
                _memberProvider!.memberModel.name) {
              _showCancelVisitDialog(_visitRecordModel.id!);
            } else {
              //跳出
              if (delegate.pages
                  .where((element) => element.name == RouteName.cancelVisitDialog)
                  .isEmpty) {
                //先找路徑是否還在
                delegate.push(
                    name: RouteName.cancelVisitDialog, arguments: _visitRecordModel);
              }
            }
          } else {
            //被探班的人發起的
            if (_visitRecordModel.targetMemberName !=
                _memberProvider!.memberModel.name) {
              _showCancelVisitDialog(_visitRecordModel.id!);
            } else {
              //跳出
              if (delegate.pages
                  .where((element) => element.name == RouteName.cancelVisitDialog)
                  .isEmpty) {
                //先找路徑是否還在
                delegate.push(
                    name: RouteName.cancelVisitDialog, arguments: _visitRecordModel);
              }
            }
          }
          break;
        //同意取消探班資料通道
        case 'agreeToCancelVisit':
          _utilApiProvider!.setVisitRecordModel(_visitRecordModel!);
          //如果路徑還在 但探班同意取消
          if (delegate.pages
              .where((element) => element.name == RouteName.cancelVisitDialog)
              .isNotEmpty) {
            //先找路徑是否還在
            delegate.popRoute(); //還在就關閉
          }
          Future.delayed(const Duration(milliseconds: 500), () {
            //延遲0.5秒 否則提示框會無法跳出
            //判斷 探班是否被取消
            if (_visitRecordModel!.status == VisitStatus.cancel.index) {
              _utilApiProvider!.clearVisitProcessingRecordList();
              //狀態等於被取消時
              _showCancelSuccessDialog(); //成功取消
              _visitGameProvider!.changeVisitStatus(VisitStatus.pairing);
              _visitGameProvider!.changeUserStatus(VisitMode.visitModeVisit);
            } else {
              _showCancelFallDialog();
            }
          });
          break;
        //探班會員請求取消探班資料通道
        case 'cancelBackInWaitingCancel':
          _utilApiProvider!.setVisitRecordModel(_visitRecordModel!);
          if (util.dialogIsOpen) {
            Navigator.of(context).pop();
          }
          break;
        //解鎖
        case 'unlock':
          _unlockMugshotProvider!.getUnlockPaymentRecordByFromMemberId(
              id: _memberProvider!.memberModel.id!, expiration: _utilApiProvider!.systemSetting!.unlockMugshotExpireDay!);
          _memberProvider!.getMemberPoint(_memberProvider!.memberModel.id!);
          break;
        //躲貓貓資料通道
        case 'hideAndSeek':
          Future.delayed(const Duration(milliseconds: 3000), () {
            _utilApiProvider!.setHideAndSeekButtonStatus();
          });
          switch (_peekabooRecordModel!.status) {
            case 0:
              _peekabooGameProvider!
                  .setCatchStatus(status: CatchStatus.catchStatusWaiting);
              break;
            case 1:
              _peekabooGameProvider!
                  .setCatchStatus(status: CatchStatus.catchStatusComplete);
              break;
            case 2:
              _peekabooGameProvider!
                  .setCatchStatus(status: CatchStatus.catchStatusProcessing);
              if (_utilApiProvider!.peekabooRecordTargetMemberList.isNotEmpty) {
                if (_utilApiProvider!.peekabooRecordTargetMemberList
                    .where((element) => element.id != _peekabooRecordModel!.id!)
                    .isNotEmpty) {
                  _utilApiProvider!.setPeekabooRecordTargetMemberList(
                      record: _peekabooRecordModel);
                }
              } else {
                _utilApiProvider!.getHideAndSeekRecordByTargetMemberId(
                    id: _memberProvider!.memberModel.id!);
              }
              break;
            case 3:
              _peekabooGameProvider!
                  .setCatchStatus(status: CatchStatus.catchStatusCancel);
              _utilApiProvider!.setPeekabooRecordTargetMemberList(
                  record: _peekabooRecordModel);
              break;
          }
          break;
        //躲貓貓抓取資料通道
        case 'doCatchHideAndSeek': //被捕捉
          if (_peekabooGameProvider!.userStatus == PeekabooUserStatus.sought) {
            //被捕方
            //跳出被捕提示
            _showQuickSuccess();
            //狀態更改
            switch (_peekabooRecordModel!.status) {
              case 0:
                _peekabooGameProvider!
                    .setCatchStatus(status: CatchStatus.catchStatusWaiting);
                break;
              case 1:
                _peekabooGameProvider!
                    .setCatchStatus(status: CatchStatus.catchStatusComplete);
                break;
              case 2:
                _peekabooGameProvider!
                    .setCatchStatus(status: CatchStatus.catchStatusProcessing);
                break;
              case 3:
                _peekabooGameProvider!
                    .setCatchStatus(status: CatchStatus.catchStatusCancel);
                break;
            }
            //刷新資料
            _utilApiProvider!.getHideAndSeekRecordByTargetMemberId(
                id: _memberProvider!.memberModel.id!);
          }
          break;
        case 'selfCancel':
          _visitGameProvider!.changeVisitStatus(VisitStatus.pairing);
          _showRefusalToVisitDialog();
          break;
      }
    });
  }

  //探班 同意或不同意
  void _showVisitDialog(int _id, VisitRecordModel _visitRecordModel) async {
    bool? result = await showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return VisitConfirmDialog(
            /*key: UniqueKey(),*/
            title: S.of(context).received_an_invitation_to_visit,
            content: _visitRecordModel,
            confirmButtonText: S.of(context).agree,
            cancelButtonText: S.of(context).disagree,
          );
        },
        barrierColor: Colors.black54,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 100),
        transitionBuilder: (context, anim1, anim2, child) {
          return child;
        });
    if (result != null) {
      if (Util().isWithinTwentySeconds(_visitRecordModel.wannaVisitAt!)) {
        ApproveVisitRecordDTO _approveVisitRecord =
            ApproveVisitRecordDTO(id: _id, approve: result);
        _stompClientProvider!
            .sendApprove(approveVisitRecord: _approveVisitRecord);
      } else {
        EasyLoading.showToast(S.of(context).timed_out);
        _utilApiProvider!.setCleanFindLastByTargetMemberName(); //清除顯示
      }
    }
  }

  //被取消探班同意或不同意
  void _showCancelVisitDialog(int _id) async {
    bool? result = await showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return VisitDialog(
            key: UniqueKey(),
            title: S.of(context).cancel_visit,
            content: S
                .of(context)
                .do_you_agree_with_the_other_party_to_cancel_the_visit,
            confirmButtonText: '${S.of(context).agree}${S.of(context).cancel}',
            cancelButtonText:
                '${S.of(context).disagree}${S.of(context).cancel}',
          );
        },
        barrierColor: Colors.black54,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 100),
        transitionBuilder: (context, anim1, anim2, child) {
          return child;
        });
    if (result != null) {
      ApproveVisitRecordDTO _approveVisitRecord =
          ApproveVisitRecordDTO(id: _id, approve: result);
      if (_visitGameProvider!.userStatus == VisitMode.visitModeVisit) {
        //探班
        _stompClientProvider!.sendAgreeToCancelByFromMember(
            approveVisitRecord: _approveVisitRecord);
      } else {
        //被探班
        _stompClientProvider!.sendAgreeToCancelByTargetMember(
            approveVisitRecord: _approveVisitRecord);
      }
    }
  }

  //取消探班成功確認
  void _showCancelSuccessDialog() async {
    showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return VisitAskCancelDialog(
            key: UniqueKey(),
            title: S.of(context).message,
            content: '${S.of(context).already}${S.of(context).cancel_visit}',
            confirmButtonText: S.of(context).sure,
            haveClose: false,
          );
        },
        barrierColor: Colors.black54,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 100),
        transitionBuilder: (context, anim1, anim2, child) {
          return Transform.scale(
            scale: anim1.value,
            child: child,
          );
        }).then((value) {
      delegate.popUtil();
    }); //回到第一頁
  }

  //取消探班失敗確認
  void _showCancelFallDialog() async {
    showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return VisitAskCancelDialog(
            key: UniqueKey(),
            title: S.of(context).message,
            content: '${S.of(context).cancel_visit}${S.of(context).fail}',
            confirmButtonText: S.of(context).sure,
            haveClose: false,
          );
        },
        barrierColor: Colors.black54,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 100),
        transitionBuilder: (context, anim1, anim2, child) {
          return Transform.scale(
            scale: anim1.value,
            child: child,
          );
        }).then((value) {
      //將訂單狀態刷新成進行中
      _visitGameProvider!.changeVisitStatus(VisitStatus.processing);
    });
  }

  //被探班方不同意你探班
  void _showRefusalToVisitDialog() async {
    showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return VisitAskCancelDialog(
              key: UniqueKey(),
              title: S.of(context).message,
              content:
                  S.of(context).the_other_party_does_not_agree_to_your_visit,
              confirmButtonText: S.of(context).sure,
              haveClose: false);
        },
        barrierColor: Colors.black54,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 100),
        transitionBuilder: (context, anim1, anim2, child) {
          return Transform.scale(
            scale: anim1.value,
            child: child,
          );
        });
  }

  //被捕捉提示框
  void _showQuickSuccess() async {
    showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return VisitAskCancelDialog(
              key: UniqueKey(),
              title: '',
              content: S.of(context).you_have_been_caught,
              confirmButtonText: S.of(context).confirm,
              haveClose: false);
        },
        barrierColor: Colors.black54,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 100),
        transitionBuilder: (context, anim1, anim2, child) {
          return Transform.scale(
            scale: anim1.value,
            child: child,
          );
        });
  }

  ///充值完成或失敗對話框
  void _showRechargeDoneDialog(
      {required RechargeRecordModel rechargeRecordModel}) {
    _isShow = true;
    showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return RechargeDoneDialog(
            key: UniqueKey(),
            title: S.of(context).recharge,
            image:
                _getRechargeDoneDialogImage(type: rechargeRecordModel.payment!),
            content: _getRechargeDoneDialogContent(
              type: rechargeRecordModel.payment!,
              amount: rechargeRecordModel.amount!,
            ),
            confirmButtonText: S.of(context).confirm,
          );
        },
        barrierColor: Colors.black54,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 100),
        transitionBuilder: (context, anim1, anim2, child) {
          return Transform.scale(
            scale: anim1.value,
            child: child,
          );
        }).then((value) {
      _isShow = false;
      //刷新點數
      _memberProvider!.getMemberPoint(_memberProvider!.memberModel.id!);
      //刷新紀錄
      _memberProvider!.getMemberPointHistory();
    });
  }

  /// 對話框中的圖片
  /// * [type] 是否充值成功
  String _getRechargeDoneDialogImage({required bool type}) {
    if (type) {
      return AppImage.iconOrderComplete;
    }
    return AppImage.iconOrderFailed;
  }

  /// 對話框中的文字
  /// * [type] 是否充值成功
  /// * [amount] 花了多少$
  Widget _getRechargeDoneDialogContent(
      {required bool type, required int amount}) {
    if (type) {
      return RichText(
          text: TextSpan(
              text: 'NT\$$amount ',
              style: const TextStyle(
                  color: Colors.red, fontWeight: FontWeight.bold, fontSize: 20),
              children: [
            TextSpan(
              text: S.of(context).successful_recharge,
              style: const TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 20),
            )
          ]));
    }
    return Text(
      S.of(context).recharge_failed_please_recharge,
      style: const TextStyle(
          color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
    );
  }

  ///判斷是否有新版本
  void _upDataApp() {
    if (_utilApiProvider!.systemSetting == null) {
      Future.delayed(const Duration(milliseconds: 5000), _upDataApp);
      return;
    }
    if (_utilApiProvider!.systemSetting!.appVersionNumber!.isNotEmpty) {
      //判斷是否有商店
      if (Platform.isAndroid) {
        // <- 安卓
        if (_utilApiProvider!.systemSetting!.playStoreAppUrl!.isNotEmpty) {
          if (_versionVerify()) {
            //跳出更新顯示
            Future.delayed(const Duration(milliseconds: 500), () {
              _showUpDataAppDialog(
                  url: _utilApiProvider!.systemSetting!.playStoreAppUrl!);
            });
          }
        }
      } else if (Platform.isIOS) {
        // <- IOS
        if (_utilApiProvider!.systemSetting!.appleStoreAppUrl!.isNotEmpty) {
          if (_versionVerify()) {
            //跳出更新顯示
            Future.delayed(const Duration(milliseconds: 500), () {
              _showUpDataAppDialog(
                  url: _utilApiProvider!.systemSetting!.appleStoreAppUrl!);
            });
          }
        }
      }
    }
  }

  bool _versionVerify() {
    String _localVersion = _packagesInfoProvider!.version;
    String _storeVersion = _utilApiProvider!.systemSetting!.appVersionNumber!;
    if (Util().getAppVersion(version: _localVersion) <
        Util().getAppVersion(version: _storeVersion)) {
      return true;
    }
    return false;
  }

  ///顯示更新提示
  void _showUpDataAppDialog({required String url}) async {
    bool? _result = await showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return VisitAskCancelDialog(
            key: UniqueKey(),
            title: S.of(context).app_update,
            content: S.of(context).new_version,
            confirmButtonText: S.of(context).renew,
            haveClose: true,
          );
        },
        barrierColor: Colors.black54,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 100),
        transitionBuilder: (context, anim1, anim2, child) {
          return Transform.scale(
            scale: anim1.value,
            child: child,
          );
        });
    if (_result != null) {
      if (_result) {
        // <- 要更新跳轉
        _launchAppStore(url: url);
      }
    }
  }

  ///跳轉商店
  void _launchAppStore({required String url}) async {
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch appStoreLink';
    }
  }
}
