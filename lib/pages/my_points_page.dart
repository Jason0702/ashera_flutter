import 'dart:developer';
import 'dart:io';

import 'package:ashera_flutter/app.dart';
import 'package:ashera_flutter/generated/l10n.dart';
import 'package:ashera_flutter/provider/in_app_purchase_provider.dart';
import 'package:ashera_flutter/provider/member_provider.dart';
import 'package:ashera_flutter/provider/position_provider.dart';
import 'package:ashera_flutter/provider/util_api_provider.dart';
import 'package:ashera_flutter/utils/app_color.dart';
import 'package:ashera_flutter/utils/app_image.dart';
import 'package:ashera_flutter/utils/no_shadow_scroll_behavior.dart';
import 'package:ashera_flutter/widget/button_widget.dart';
import 'package:ashera_flutter/widget/titlebar_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../enum/app_enum.dart';
import '../models/recharge_record_model.dart';
import '../routers/route_name.dart';
import '../utils/util.dart';

class MyPointsPage extends StatefulWidget {
  const MyPointsPage({Key? key}) : super(key: key);

  @override
  State createState() => _MyPointsPageState();
}

class _MyPointsPageState extends State<MyPointsPage> {
  MemberProvider? _memberProvider;
  UtilApiProvider? _utilApiProvider;
  PositionProvider? _positionProvider;
  InAppPurchaseProvider? _appPurchaseProvider;

  //按鈕狀態
  ButtonStatus _rechargeButtonStatus = ButtonStatus.click;
  ButtonStatus _rechargeRecordButtonStatus = ButtonStatus.notClick;
  //頁面切換狀態
  RechargeOrRechargeRecord _pageStatus = RechargeOrRechargeRecord.recharge;

  final List<DropdownMenuItem<TopUpPlanModel>> _topUpAmountItem = [];
  TopUpPlanModel? _topUpAmountValue;

  final List<DropdownMenuItem<String>> _paymentMethodItem = [];
  String _paymentMethodValue = '信用卡付款';

  List<TopUpPlanModel> _topUpPlans = [];

  _onLayoutDone(_) async {
    Map<String, dynamic> result = await _utilApiProvider!.getTopUpPlan();
    if (result['status']) {
      _topUpPlans = List.from(result['value']);
      Set<String> _kIds = {};
      _kIds.addAll(_topUpPlans
          .where((element) => element.iapProductId != null)
          .map((e) => e.iapProductId!));
      if (_kIds.isNotEmpty) {
        _appPurchaseProvider!.appPurchaseStoreInfoInit(_kIds);
      }
    }
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);

    _paymentMethodItem.add(const DropdownMenuItem(
      child: Text('信用卡付款'),
      value: '信用卡付款',
    ));
  }

  @override
  Widget build(BuildContext context) {
    _memberProvider = Provider.of<MemberProvider>(context);
    _utilApiProvider = Provider.of<UtilApiProvider>(context);
    _positionProvider = Provider.of<PositionProvider>(context);
    _appPurchaseProvider = Provider.of<InAppPurchaseProvider>(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Column(
        children: [
          //返回與標題
          titleBar(S.of(context).my_points),
          //內容
          Expanded(child: _body())
        ],
      ),
    );
  }

  //內容
  Widget _body() {
    return SizedBox(
      width: Device.boxConstraints.maxWidth,
      child: Column(
        children: [
          SizedBox(
            height: 2.h,
          ),
          //現有點數
          Container(
            height: 12.h,
            alignment: Alignment.center,
            margin: EdgeInsets.only(left: 10.w, right: 10.w),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                border: Border.all(color: AppColor.buttonFrameColor, width: 2),
                color: Colors.white),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                FittedBox(
                  child: Text(
                    S.of(context).point,
                    style: TextStyle(
                        color: AppColor.buttonFrameColor, fontSize: 18.sp),
                  ),
                ),
                SizedBox(
                  width: 2.w,
                ),
                Image(
                  width: 10.w,
                  image: AssetImage(AppImage.iconDiamond),
                ),
                SizedBox(
                  width: 2.w,
                ),
                FittedBox(
                  child: Consumer<MemberProvider>(
                    builder: (context, member, _) {
                      return Text(
                        '${member.memberPoint.point}',
                        style: TextStyle(
                            color: AppColor.buttonFrameColor,
                            fontSize: 22.sp,
                            fontWeight: FontWeight.bold),
                      );
                    },
                  ),
                ),
              ],
            ),
          ),
          //充值與充值紀錄 按鈕
          Container(
            margin: EdgeInsets.only(left: 3.w, right: 3.w),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                //充值
                _yellowButton(0, _rechargeButtonStatus, S.of(context).recharge,
                    _rechargeOnTap),
                //充值紀錄
                _yellowButton(1, _rechargeRecordButtonStatus,
                    S.of(context).recharge_record, _rechargeRecordOnTap),
              ],
            ),
          ),
          Container(
            height: 0.5.h,
            color: AppColor.grayLine,
          ),
          //選單 充值或記錄
          Expanded(child: _menuWidget())
        ],
      ),
    );
  }

  //選單 充值或記錄
  Widget _menuWidget() {
    return _pageStatus == RechargeOrRechargeRecord.recharge
        ? _rechargeWidget()
        : _rechargeRecordWidget();
  }

  //充值畫面
  Widget _rechargeWidget() {
    return Container(
      width: Device.boxConstraints.maxWidth,
      decoration: const BoxDecoration(
        color: Colors.white,
      ),
      child: Consumer<InAppPurchaseProvider>(
        builder: (context, value, _) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(
                height: 2.h,
              ),
              Expanded(
                  child: ScrollConfiguration(
                    behavior: NoShadowScrollBehavior(),
                    child: GridView.builder(
                      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 2, childAspectRatio: 1.5),
                      itemCount: value.products.length,
                      itemBuilder: (context, index) {
                        return _moneyItemWidget(value, index);
                      },
                    ),
                  ))
            ],
          );
        },
      ),
    );
  }

  Widget _moneyItemWidget(InAppPurchaseProvider value, int index){
    return GestureDetector(
      onTapDown: (TapDownDetails tap){
        log('onTapDown');
        value.setBorderColor(AppColor.buttonFrameColor, index);
        value.setBackgroundColor(AppColor.fauxSnow, index);
      },
      onTapUp: (TapUpDetails tap){
        log('onTapUp');
        value.setBorderColor(Colors.grey[400]!, index);
        value.setBackgroundColor(Colors.white, index);
      },
      onTapCancel: (){
        log('onTapCancel');
        value.setBorderColor(Colors.grey[400]!, index);
        value.setBackgroundColor(Colors.white, index);
      },
      onTap: () => _makingPurchase(value.products[index]),
      child: Container(
        margin: const EdgeInsets.symmetric(horizontal: 5),
        child: Stack(
          alignment: Alignment.centerLeft,
          children: [
            Container(
              margin:
              const EdgeInsets.symmetric(vertical: 5, horizontal: 25),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(12),
                border: Border.all(color: value.borderColorList[index], width: 2),
                color: value.backgroundColorList[index],
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  //title
                  Expanded(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Image(
                            width: 10.w,
                            image: AssetImage(AppImage.iconDiamond),
                          ),
                          SizedBox(
                            width: 1.w,
                          ),
                          FittedBox(
                            fit: BoxFit.scaleDown,
                            child: Text(
                              '${_cutPoints(value.products[index].title)}',
                              style: const TextStyle(
                                  color: Colors.black,
                                  fontSize: 20,
                                  height: 1.1,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ],
                      )),
                  //金額
                  Container(
                    height: 30,
                    alignment: Alignment.center,
                    decoration: const BoxDecoration(
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(10),
                            bottomRight: Radius.circular(10)),
                        color: AppColor.buttonFrameColor),
                    child: FittedBox(
                      fit: BoxFit.scaleDown,
                      child: Text(
                        '\$${value.products[index].rawPrice.ceil()}',
                        style: const TextStyle(
                            color: Colors.white,
                            fontSize: 18,
                            height: 1.1,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  )
                ],
              ),
            ),
            if(_getProportionImage(value.products[index].rawPrice.ceil()).isNotEmpty)
              Positioned(
                  top: 0,
                  right: 0,
                  child: Image(
                    width: 50,
                    image: AssetImage(_getProportionImage(value.products[index].rawPrice.ceil())),
                  )
              )
          ],
        ),
      ),
    );
  }
  //標籤
  String _getProportionImage(int price){
    switch(price){
      case 100:
        return AppImage.img3proportion;
      case 270:
        return AppImage.img5proportion;
      case 490:
        return AppImage.img7proportion;
      case 790:
        return AppImage.img9proportion;
      default:
        return '';
    }
  }

  Widget _itemWidget(ProductDetails productDetails) {
    return GestureDetector(
      onTap: () => _makingPurchase(productDetails),
      child: Container(
        margin: const EdgeInsets.symmetric(vertical: 5, horizontal: 20),
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(12),
            border: Border.all(color: AppColor.femaleColor, width: 1)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            //title
            FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                productDetails.description,
                style: const TextStyle(
                    color: Colors.black, fontSize: 18, height: 1.1),
              ),
            ),
            //金額
            FittedBox(
              fit: BoxFit.scaleDown,
              child: Text(
                '金額: \$${productDetails.rawPrice.ceil()}',
                style: const TextStyle(
                    color: Colors.black, fontSize: 18, height: 1.1),
              ),
            )
          ],
        ),
      ),
    );
  }

  //充值
  void _makingPurchase(ProductDetails productDetails) async {
    if (EasyLoading.isShow) {
      return;
    }
    EasyLoading.show();
    final PurchaseParam purchaseParam =
        PurchaseParam(productDetails: productDetails);

    AddPointsHistoryDTO _addPointsHistory = AddPointsHistoryDTO(
        amount: _topUpPlans
            .firstWhere((element) => element.iapProductId == productDetails.id)
            .amount!,
        memberId: _memberProvider!.memberModel.id!,
        paymentType: Platform.isIOS
            ? PaymentType.APPLE_IAP.index
            : PaymentType.GOOGLE_IAP.index,
        points: _cutPoints(productDetails.title),
        topUpPlanId: _topUpPlans
            .firstWhere((element) => element.iapProductId == productDetails.id)
            .id!);

    Map<String, dynamic>? value = await _utilApiProvider!
        .postMemberPointHistory(addPointsHistoryDTO: _addPointsHistory);
    if (value != null) {
      if (value['status']) {
        RechargeRecordModel rechargeRecordModel = value['value'];
        _appPurchaseProvider!
            .setStoreOrderNumber(rechargeRecordModel.orderNumber!);
        Future.delayed(const Duration(milliseconds: 300),
            () => _appPurchaseProvider!.byConsumable(purchaseParam));
        EasyLoading.dismiss();
      } else {
        EasyLoading.dismiss();
      }
    } else {
      EasyLoading.dismiss();
    }
  }

  //切割取點數
  int _cutPoints(String _title) {
    String _amount = '0';
    if (_title.contains('(')) {
      List<String> _value = _title.split('(');
      _amount = _value[0].trim();
    } else {
      _amount = _title.trim();
    }
    return int.parse(_amount);
  }

  //加值點數
  Widget _topUpAmount() {
    return Container(
      decoration: BoxDecoration(
          border: Border.all(color: AppColor.grayLine, width: 1),
          borderRadius: BorderRadius.circular(5)),
      width: 80.w,
      height: 6.h,
      child: ButtonTheme(
        alignedDropdown: true,
        child: DropdownButtonHideUnderline(
          child: DropdownButton(
            alignment: Alignment.center,
            dropdownColor: Colors.white,
            icon: const Icon(Icons.keyboard_arrow_down_rounded),
            items: _topUpAmountItem,
            value: _topUpAmountValue,
            onChanged: (TopUpPlanModel? _value) {
              setState(() {
                _topUpAmountValue = _value;
              });
            },
            isExpanded: true,
            iconSize: 40,
          ),
        ),
      ),
    );
  }

  //付款方式
  Widget _paymentMethod() {
    return Container(
      decoration: BoxDecoration(
          border: Border.all(color: AppColor.grayLine, width: 1),
          borderRadius: BorderRadius.circular(5)),
      width: 80.w,
      height: 6.h,
      child: ButtonTheme(
        alignedDropdown: true,
        child: DropdownButtonHideUnderline(
          child: DropdownButton(
            alignment: Alignment.center,
            dropdownColor: Colors.white,
            icon: const Icon(Icons.keyboard_arrow_down_rounded),
            items: _paymentMethodItem,
            value: _paymentMethodValue,
            onChanged: (String? _value) {
              setState(() {
                _paymentMethodValue = _value!;
              });
            },
            isExpanded: true,
            iconSize: 40,
          ),
        ),
      ),
    );
  }

  //支付金額
  Widget _paymentAmount() {
    return Container(
      alignment: Alignment.centerLeft,
      margin: EdgeInsets.only(left: 10.w),
      child: FittedBox(
        child: RichText(
          text: TextSpan(
              text: _topUpAmountValue == null
                  ? '\$0'
                  : '\$${_topUpAmountValue!.amount}',
              style: TextStyle(
                  color: Colors.red,
                  fontSize: 25.sp,
                  fontWeight: FontWeight.bold),
              children: [
                TextSpan(
                    text: '  ${S.of(context).yuan}',
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 18.sp,
                    ))
              ]),
        ),
      ),
    );
  }

  //確認充值
  Widget _confirmRechargeButton() {
    return GestureDetector(
      onTap: () => _confirmRechargeOnTap(),
      child: Container(
        height: 7.h,
        width: 58.w,
        margin: EdgeInsets.symmetric(vertical: 5.h),
        child: buttonWidget(S.of(context).confirm_recharge),
      ),
    );
  }

  void _confirmRechargeOnTap() {
    EasyLoading.show(status: S.of(context).order_is_being_created);
    AddPointsHistoryDTO _addPointsHistory = AddPointsHistoryDTO(
        amount: _topUpAmountValue!.amount!,
        memberId: _memberProvider!.memberModel.id!,
        paymentType: PaymentType.ATM.index,
        points: _topUpAmountValue!.point!,
        topUpPlanId: _topUpAmountValue!.id!);
    _utilApiProvider!
        .postMemberPointHistory(addPointsHistoryDTO: _addPointsHistory)
        .then((value) {
      ///跳轉
      if (value != null) {
        if (value['status']) {
          RechargeRecordModel rechargeRecordModel = value['value'];
          YauYangPaymentReqDTO yangPaymentReqDTO = YauYangPaymentReqDTO(
              custOrderNo: rechargeRecordModel.orderNumber!,
              memberId: rechargeRecordModel.memberId!,
              custId: '${rechargeRecordModel.id!}',
              orderAmount: '${rechargeRecordModel.amount!}',
              orderDetail: _topUpAmountValue!.name!,
              productName: '加值${_topUpAmountValue!.name!}',
              payerName: _memberProvider!.memberModel.nickname!,
              payerAddress: _positionProvider!.address,
              payerMobile: _memberProvider!.memberModel.cellphone!,
              payerEmail: '123@gmail.com',
              successURL: '123');
          _utilApiProvider!
              .postYauYangPayment(yangPaymentReqDTO: yangPaymentReqDTO)
              .then((value) {
            if (value['status']) {
              //關閉Loading框
              if (EasyLoading.isShow) {
                EasyLoading.dismiss();
              }
              //開啟WebView
              _showBonusDialog(paymentResDTO: value['value']);
            } else {
              EasyLoading.showToast(S.of(context).recharge_failed);
            }
          });
        } else {
          EasyLoading.showToast(S.of(context).recharge_failed);
        }
      } else {
        EasyLoading.showToast(S.of(context).recharge_failed);
      }
    });
  }

  ///充值webView
  void _showBonusDialog({required PaymentResDTO paymentResDTO}) {
    delegate.push(name: RouteName.bonusDialog, arguments: paymentResDTO.url);
  }

  //充值紀錄
  Widget _rechargeRecordWidget() {
    return Container(
      width: Device.boxConstraints.maxWidth,
      decoration: const BoxDecoration(
        color: Colors.white,
      ),
      child: ScrollConfiguration(
        behavior: NoShadowScrollBehavior(),
        child: Consumer<MemberProvider>(
          builder: (context, member, _) {
            return ListView.builder(
                itemCount: member.memberPointHistory.length,
                itemBuilder: (context, index) {
                  return _rechargeRecordCard(index);
                });
          },
        ),
      ),
    );
  }

  Widget _rechargeRecordCard(int index) {
    return Container(
      alignment: Alignment.center,
      child: Consumer<MemberProvider>(
        builder: (context, member, _) {
          return Column(
            children: [
              _rechargeRecordIndex(
                  S.of(context).transaction_number,
                  member.memberPointHistory[index].orderNumber.toString(),
                  Colors.black),
              _rechargeRecordIndex(
                  S.of(context).transaction_date,
                  _getRecordTime(
                      member.memberPointHistory[index].createdAt!.ceil()),
                  Colors.black),
              _rechargeRecordIndex(
                  S.of(context).payment_method,
                  S.of(context).paymentType(_getPaymentTypeName(
                      type: member.memberPointHistory[index].paymentType!)),
                  Colors.black),
              _rechargeRecordIndex(
                  S.of(context).amount_of_the_transaction,
                  _getAmountOfTheTransaction(
                      member.memberPointHistory[index].amount!),
                  Colors.red),
              _rechargeRecordIndex(
                  S.of(context).record_points,
                  member.memberPointHistory[index].points!.toString(),
                  Colors.black),
              _rechargeRecordIndex(
                  S.of(context).payment_status,
                  member.memberPointHistory[index].payment!
                      ? S.of(context).finish
                      : S.of(context).fail,
                  Colors.red),
              if (index != member.memberPointHistory.length - 1)
                Container(
                  color: AppColor.grayLine,
                  height: 0.3.h,
                ),
            ],
          );
        },
      ),
    );
  }

  String _getRecordTime(int _time) {
    DateTime _createdAt =
        DateTime.fromMillisecondsSinceEpoch(_time * 1000, isUtc: false);
    return Util().visitRecordTime(_createdAt);
  }

  String _getPaymentTypeName({required int type}) {
    return PaymentType.values[type].name;
  }

  String _getAmountOfTheTransaction(int _money) {
    return 'NT\$$_money';
  }

  Widget _rechargeRecordIndex(String _title, String _text, Color _textColor) {
    return Container(
      margin: EdgeInsets.only(left: 6.w),
      child: Row(
        children: [
          //前面 Title
          FittedBox(
            child: Text(
              '$_title :',
              style: TextStyle(color: Colors.black, fontSize: 18.sp),
            ),
          ),
          //後面 內容與顏色
          FittedBox(
            child: Text(
              ' $_text',
              style: TextStyle(color: _textColor, fontSize: 18.sp),
            ),
          )
        ],
      ),
    );
  }

  //充值
  void _rechargeOnTap() {
    _rechargeButtonStatus = ButtonStatus.click;
    _rechargeRecordButtonStatus = ButtonStatus.notClick;
    _pageStatus = RechargeOrRechargeRecord.recharge;
    setState(() {});
  }

  //充值記錄
  void _rechargeRecordOnTap() {
    _rechargeButtonStatus = ButtonStatus.notClick;
    _rechargeRecordButtonStatus = ButtonStatus.click;
    _pageStatus = RechargeOrRechargeRecord.rechargeRecord;
    setState(() {});
  }

  Widget _yellowButton(
      int _id, ButtonStatus _status, String _text, VoidCallback _click) {
    return GestureDetector(
      onTap: () => _click(),
      child: Container(
        height: 5.h,
        width: 30.w,
        margin: EdgeInsets.only(top: 2.h, bottom: 2.h),
        padding: EdgeInsets.only(top: 0.5.h, bottom: 0.5.h),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30),
            border: Border.all(color: AppColor.yellowButton, width: 2),
            color: _status == ButtonStatus.click
                ? AppColor.yellowButton
                : Colors.white),
        child: FittedBox(
          child: Text(
            _text,
            style: TextStyle(
                color: _status == ButtonStatus.click
                    ? Colors.white
                    : AppColor.yellowButton,
                fontSize: 15.sp,
                fontWeight: FontWeight.bold),
          ),
        ),
      ),
    );
  }
}
