import 'package:ashera_flutter/provider/stomp_client_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../generated/l10n.dart';
import '../utils/app_color.dart';
import '../utils/app_image.dart';
import '../utils/no_shadow_scroll_behavior.dart';
import '../utils/shared_preference.dart';
import '../widget/avatar_image.dart';
import '../widget/titlebar_widget.dart';

//通知
class NoticePage extends StatefulWidget{
  const NoticePage({Key? key}): super(key: key);

  @override
  State createState() => _NoticePageState();
}

class _NoticePageState extends State<NoticePage> {
  StompClientProvider? _stompClientProvider;

  _onLayoutDone(_) async {
    debugPrint('通知');
    _stompClientProvider!.getRequestFollowerMe();
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  Widget build(BuildContext context) {
    _stompClientProvider = Provider.of<StompClientProvider>(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: AppColor.appBackgroundColor,
      body: Column(
        children: [
          //返回 與 標題
          titleBar(S.of(context).notice),
          SizedBox(height: 5.h,
          child: Container(
            alignment: Alignment.centerLeft,
            margin: EdgeInsets.only(left: 6.w),
            child: FittedBox(
              child: Text(S.of(context).friend_invitation, style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18.sp),),
            ),
          )
          ),
          //內容
          Expanded(flex: 10, child: _body())
        ],
      ),
    );
  }

  //內容
  Widget _body() {
    return Container(
      decoration: const BoxDecoration(
        color: Colors.white
      ),
      child: Container(
        margin: const EdgeInsets.only(left: 0, top: 0),
        child: ScrollConfiguration(
          behavior: NoShadowScrollBehavior(),
          child: Consumer<StompClientProvider>(builder: (context, notice, _){
            return notice.wantAddMeList.isEmpty ? SizedBox(
              width: Device.width,
              height: Device.height,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image(
                    width: 20.w,
                    height: 20.h,
                    image: AssetImage(AppImage.imgWaiting),
                  ),
                  Text(
                    S.of(context).there_are_currently_no_records,
                    style: TextStyle(
                        color: Colors.grey[300]!,
                        fontWeight: FontWeight.bold,
                        fontSize: 20.sp),
                  )
                ],
              ),
            ) : ListView.builder(
                shrinkWrap: true,
                itemCount: notice.wantAddMeList.length,
                itemBuilder: (context, index) {
                  return friendCard(index);
                });
          },),
        ),
      ),
    );
  }

  Widget friendCard(int index){
    return Column(
      children: [
        Container(
          margin: EdgeInsets.only(top: 5, bottom: 5, right: 6.w, left: 6.w),
          child: Consumer<StompClientProvider>(builder: (context, notice, _){
            return notice.wantAddMeList[index].memberModel != null ? Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                //頭像
                wantAddMeAvatarImage(index, notice),
                //名稱與日期
                Expanded(child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      child: FittedBox(
                        child: Text(
                          notice.wantAddMeList[index].memberModel!.nickname!,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 18.sp,
                              color: Colors.black),
                        ),
                      ),
                      margin: const EdgeInsets.only(left: 10),
                    ),
                  ],
                )),
                //按鈕
                Container(
                  margin: const EdgeInsets.only(left: 10),
                  child: Row(
                    children: [
                      //確認按鈕
                      GestureDetector(
                        onTap: () => _confirmOnTap(notice.wantAddMeList[index].memberId!, notice.wantAddMeList[index].followerId!),
                        child: Container(
                          width: 15.w,
                          height: 4.h,
                          decoration: BoxDecoration(
                              gradient: AppColor.appMainColor,
                              borderRadius: BorderRadius.circular(30)
                          ),
                          padding: EdgeInsets.only(left: 0.5.w, right: 0.5.w, top: 0.5.h, bottom: 0.5.h),
                          child: FittedBox(
                            child: Text(S.of(context).confirm, style: TextStyle(color: AppColor.buttonTextColor, fontSize: 16.sp),),
                          ),
                        ),
                      ),
                      SizedBox(width: 2.w,),
                      //拒絕按鈕
                      GestureDetector(
                        onTap: () => _refuseOnTap(notice.wantAddMeList[index].memberId!, notice.wantAddMeList[index].followerId!),
                        child: Container(
                          width: 16.w,
                          height: 4.h,
                          decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(30),
                              border: Border.all(color: AppColor.buttonFrameColor, width: 2)
                          ),
                          padding: EdgeInsets.only(left: 0.3.w, right: 0.3.w, top: 0.3.h, bottom: 0.3.h),
                          child: FittedBox(
                            child: Text(S.of(context).refuse, style: TextStyle(color: AppColor.buttonFrameColor, fontSize: 16.sp),),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ) : Container();
          },),
        ),
        Container(
          color: Colors.grey,
          height: 0.1.h,
        )
      ],
    );
  }

  //確認按鈕
  void _confirmOnTap(int _memberId,int _followerId){
    _stompClientProvider!.sendFollowerRequestAccept(_memberId, _followerId, true);
  }
  //拒絕按鈕
  void _refuseOnTap(int _memberId,int _followerId){
    _stompClientProvider!.sendFollowerRequestAccept(_memberId, _followerId, false);
  }
}