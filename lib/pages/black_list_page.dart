import 'package:ashera_flutter/provider/member_black_list_provider.dart';
import 'package:ashera_flutter/provider/member_provider.dart';
import 'package:ashera_flutter/provider/stomp_client_provider.dart';
import 'package:ashera_flutter/utils/app_color.dart';
import 'package:ashera_flutter/utils/no_shadow_scroll_behavior.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../generated/l10n.dart';
import '../utils/api.dart';
import '../utils/app_size.dart';
import '../utils/shared_preference.dart';
import '../utils/util.dart';
import '../widget/avatar_image.dart';
import '../widget/titlebar_widget.dart';

class BlackListPage extends StatefulWidget{
  const BlackListPage({Key? key}): super(key: key);

  @override
  State createState() => _BlackListPageStatus();
}

class _BlackListPageStatus extends State<BlackListPage>{
  MemberBlackListProvider? _memberBlackListProvider;
  MemberProvider? _memberProvider;

  @override
  Widget build(BuildContext context) {
    _memberBlackListProvider = Provider.of<MemberBlackListProvider>(context);
    _memberProvider = Provider.of<MemberProvider>(context);
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: AppColor.appBackgroundColor,
      body: Column(
        children: [
          //返回 與 標題
          titleBar(S.of(context).block_list),
          SizedBox(height: 1.h,),
          Expanded(flex: 10, child: _body())
        ],
      ),
    );
  }

  //內容
  Widget _body() {
    return Container(
      width: Device.boxConstraints.maxWidth,
      decoration: const BoxDecoration(
          color: Colors.white
      ),
      child: ScrollConfiguration(
        behavior: NoShadowScrollBehavior(),
        child: Consumer<MemberBlackListProvider>(builder: (context, blacklist, _){
          return ListView.builder(
              shrinkWrap: true,
              itemBuilder: (context, index){
                return _blacklist(index);
              },
              itemCount: blacklist.blackListByFromMember.length,
          );
        },),
      ),
    );
  }

  Widget _blacklist(int index){
    return Consumer2<StompClientProvider , MemberBlackListProvider>(builder: (context, friend, blacklist, _){
      return Container(
        decoration: const BoxDecoration(
            border: Border(bottom: BorderSide(width: 1, color: AppColor.grayLine))),
        child: Container(
          margin: const EdgeInsets.only(top: 5, bottom: 5, right: 25, left: 25),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  //大頭照
                  _avatarImage(friend, blacklist.blackListByFromMember[index].targetMemberId!),
                  //暱稱
                  Container(
                    child: FittedBox(
                      child: Text(
                        friend.friendList.firstWhere((element) => element.id == blacklist.blackListByFromMember[index].targetMemberId).nickname!,
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 18.sp,
                            color: Colors.black),
                      ),
                    ),
                    margin: const EdgeInsets.only(left: 10),
                  ),
                ],
              ),
              //解除封鎖按鈕
              _unFollowWidget(blacklist.blackListByFromMember[index].id!)
            ],
          ),
        ),
      );
    });
  }

  //好友
  Widget _avatarImage(StompClientProvider friend, int targetMemberId) {
    return friend.friendList.firstWhere((element) => element.id == targetMemberId).mugshot == null
        ?  _noAvatarImage()//沒照片
        : Container(
      width: 16.w,
      height: 8.h,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        image: DecorationImage(
            fit: BoxFit.cover,
            image: CachedNetworkImageProvider(
              Util().getMemberMugshotUrl(friend.friendList.firstWhere((element) => element.id == targetMemberId).mugshot!),
              headers: {"authorization": "Bearer " + Api.accessToken},
            ),
            onError: (error, stackTrace) {
              debugPrint('Error: ${error.toString()}');
              if (error.toString().contains('statusCode: 404')) {
                friend.setFriendAvatarNull(friend.friendList.firstWhere((element) => element.id == targetMemberId).name!);
              }
            }),
      ),
    ); //有照片
  }

  //沒有大頭照UI統一
  Widget _noAvatarImage(){
    return noAvatarImage();
  }

  //解除按鈕
  Widget _unFollowWidget(int id) {
    return GestureDetector(
      onTap: ()=> _unblockOnTap(id),
      child: Container(
          padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 5),
          decoration: BoxDecoration(
              border: Border.all(color: AppColor.pinkRedColor, width: 1),
              borderRadius: BorderRadius.circular(AppSize.buttonCircular)),
          child: Text(
            S.of(context).lift,
            style: const TextStyle(
                color: AppColor.pinkRedColor,
                fontWeight: FontWeight.bold,
                fontSize: 18),
          )),
    );
  }

  void _unblockOnTap(int id){
    EasyLoading.show(status: 'Loading...');
    _memberBlackListProvider!.deleteMemberBlackList(id: id).then((value) {
      if(value){
        _memberBlackListProvider!.getMemberBlackListByFromMemberId(id: _memberProvider!.memberModel.id!);
        EasyLoading.showSuccess(S.of(context).finish);
      } else {
        EasyLoading.showError(S.of(context).fail);
      }
    });
  }
}