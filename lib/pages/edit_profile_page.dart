import 'dart:developer';
import 'dart:io';

import 'package:ashera_flutter/provider/member_provider.dart';
import 'package:ashera_flutter/utils/api.dart';
import 'package:ashera_flutter/utils/app_color.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:image_pickers/image_pickers.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../app.dart';
import '../enum/app_enum.dart';
import '../generated/l10n.dart';
import '../provider/util_api_provider.dart';
import '../utils/app_image.dart';
import '../utils/no_shadow_scroll_behavior.dart';
import '../utils/util.dart';
import '../widget/titlebar_widget.dart';

class EditProfilePage extends StatefulWidget {
  const EditProfilePage({Key? key}) : super(key: key);

  @override
  State createState() => _EditProfilePageStatus();
}

class _EditProfilePageStatus extends State<EditProfilePage> {
  MemberProvider? _memberProvider;
  UtilApiProvider? _apiProvider;
  final PhotoType _photoType = PhotoType.tmp;
  //關於我
  final TextEditingController _aboutMe = TextEditingController();
  //暱稱
  final TextEditingController _nickName = TextEditingController();
  //性別
  final TextEditingController _gender = TextEditingController();
  //生日
  final TextEditingController _birthday = TextEditingController();
  //身高
  final TextEditingController _height = TextEditingController();
  //體重
  final TextEditingController _weight = TextEditingController();
  //星座
  final TextEditingController _constellation = TextEditingController();
  //血型
  final TextEditingController _bloodType = TextEditingController();
  //職業
  final TextEditingController _profession = TextEditingController();
  //名片自介
  final TextEditingController _businessCard = TextEditingController();

  //是否填寫職業細項
  bool _professionDetail = false;

  //性別
  Gender _genderValue = Gender.female;
  final List<DropdownMenuItem<Gender>> _genderItem = [];

  //星座
  String _constellationValue = Horoscope.aries.name;
  final List<DropdownMenuItem<String>> _constellationItem = [];
  //血型
  String _bloodTypeValue = 'O';
  final List<DropdownMenuItem<String>> _bloodTypeItem = [];
  //職業
  String _professionValue = '民意代表、主管及經理人員';
  final List<DropdownMenuItem<String>> _professionItem = [];


  //用來存生日
  Map<String, String> _birthDayMap = {};
  //原本的頭像
  //String? _filename;
  File? newPhoto;

  //表單驗證
  final _formKey = GlobalKey<FormState>();
  //隱藏生日
  ValueChanged<dynamic> _hideBirthdayValueChange() {
    return (value) {
      _memberProvider!.changeHideBirthday(value);
    };
  }
  //隱藏體重
  ValueChanged<dynamic> _hideWeightValueChange(){
    return (value) {
      _memberProvider!.changeHideWeight(value);
    };
  }
  //隱藏職業
  ValueChanged<dynamic> _hideJobValueChange(){
    return (value) {
      _memberProvider!.changeHideJob(value);
    };
  }

  _onLayoutDone(_) async {
    //性別
    _genderItem.add(DropdownMenuItem(
      child: Text(S.of(context).male),
      value: Gender.male,
    ));
    _genderItem.add(DropdownMenuItem(
      child: Text(S.of(context).female),
      value: Gender.female,
    ));
    //星座
    for (var element in Horoscope.values) {
      _constellationItem.add(DropdownMenuItem(
        child: Text(S.of(context).horoscope(element.name)),
        value: element.name,
      ));
    }
    //血型
    _bloodTypeItem.add(const DropdownMenuItem(
      child: Text('O'),
      value: 'O',
    ));
    _bloodTypeItem.add(const DropdownMenuItem(
      child: Text('A'),
      value: 'A',
    ));
    _bloodTypeItem.add(const DropdownMenuItem(
      child: Text('B'),
      value: 'B',
    ));
    _bloodTypeItem.add(const DropdownMenuItem(
      child: Text('AB'),
      value: 'AB',
    ));
    _bloodTypeItem.add(const DropdownMenuItem(
      child: Text('Rh'),
      value: 'Rh',
    ));
    //職業
    _professionItem.add(const DropdownMenuItem(
      child: Text('民意代表、主管及經理人員'),
      value: '民意代表、主管及經理人員',
    ));
    _professionItem.add(const DropdownMenuItem(
      child: Text('專業人員'),
      value: '專業人員',
    ));
    _professionItem.add(const DropdownMenuItem(
      child: Text('技術員及助理專業人員'),
      value: '技術員及助理專業人員',
    ));
    _professionItem.add(const DropdownMenuItem(
      child: Text('事務支援人員'),
      value: '事務支援人員',
    ));
    _professionItem.add(const DropdownMenuItem(
      child: Text('服務及銷售工作人員'),
      value: '服務及銷售工作人員',
    ));
    _professionItem.add(const DropdownMenuItem(
      child: Text('農、林、漁、牧業生產人員'),
      value: '農、林、漁、牧業生產人員',
    ));
    _professionItem.add(const DropdownMenuItem(
      child: Text('技藝有關工作人員'),
      value: '技藝有關工作人員',
    ));
    _professionItem.add(const DropdownMenuItem(
      child: Text('機械設備操作及組裝人員'),
      value: '機械設備操作及組裝人員',
    ));
    _professionItem.add(const DropdownMenuItem(
      child: Text('基層技術工及勞力工'),
      value: '基層技術工及勞力工',
    ));
    _professionItem.add(const DropdownMenuItem(
      child: Text('軍人'),
      value: '軍人',
    ));
    _professionItem.add(const DropdownMenuItem(
      child: Text('其他'),
      value: '其他',
    ));
    //載入會員資料
    _memberInfo();
  }

  //初始化
  void _memberInfo() {
    _memberProvider!.copyMemberModel(); //複製model
    _aboutMe.text = _memberProvider!.memberModel.aboutMe ?? '';
    _nickName.text = _memberProvider!.memberModel.nickname ?? '';
    if (_memberProvider!.memberModel.gender == Gender.male.index) {
      _genderValue = Gender.male;
    } else {
      _genderValue = Gender.female;
    }
    _birthday.text =
        Util().getBirthDayFormat(_memberProvider!.memberModel.birthday!);
    _height.text = _memberProvider!.memberModel.height != 0.0
        ? _memberProvider!.memberModel.height.toString()
        : '';
    _weight.text = _memberProvider!.memberModel.weight != 0.0
        ? _memberProvider!.memberModel.weight.toString()
        : '';

    _constellationValue =
        _memberProvider!.memberModel.zodiacSign ?? Horoscope.aries.name;

    _constellation.text = S.of(context).horoscope(_constellationValue);

    _bloodTypeValue = _memberProvider!.memberModel.bloodType ?? 'O';
    if(_memberProvider!.memberModel.job != null){
      if(_memberProvider!.memberModel.job!.isEmpty){
        _professionValue = '民意代表、主管及經理人員';
      }else{
        _professionValue = _memberProvider!.memberModel.job!;
      }
    }else{
      _professionValue = '民意代表、主管及經理人員';
    }

    _businessCard.text = _memberProvider!.memberModel.aboutMe2!;

    setState(() {});
  }

  //更新資料
  void _setMemberInfo() async {
    _memberProvider!.changeBusinessCard(_businessCard.text);
    _memberProvider!.changeAboutMe(_aboutMe.text);
    _memberProvider!.changeNickname(_nickName.text);
    _memberProvider!.changeGender(_genderValue.index);
    _memberProvider!.changeBirthday(
        _birthDayMap['apiText'] ?? Util().upLoadBirthDayFormat(_birthday.text));

    _memberProvider!.changeZodiacSign(_constellationValue);

    _memberProvider!.changeBloodType(_bloodTypeValue);
    _memberProvider!.changeJob(_profession.text);

    if (_height.text.isNotEmpty) {
      _memberProvider!
          .changeHeight(double.parse(_height.text.replaceAll(' cm', '')));
    }
    if (_weight.text.isNotEmpty) {
      _memberProvider!
          .changeWeight(double.parse(_weight.text.replaceAll(' kg', '')));
    }
    //上傳更新
    EasyLoading.show(status: S.of(context).updating_information);
    if (newPhoto != null) {
      if (await _updatedImage()) {
      } else {
        EasyLoading.showError(S.of(context).fail);
      }
    }

    _memberProvider!.putMember(_memberProvider!.memberModel.id!).then((value) {
      if (value) {
        _memberProvider!.putAge(age: int.parse(Util().getAge(_memberProvider!.memberModel.birthday!)));
        EasyLoading.showSuccess(S.of(context).finish).whenComplete(() {
          Future.delayed(const Duration(milliseconds: 2500), (){
            delegate.popRoute();
          });
        });
      } else {
        EasyLoading.showError(S.of(context).fail);
      }
    });
  }

  Future<bool> _updatedImage() async {
    //上傳圖片並顯示成功或失敗
    Map<String, dynamic> value = await _apiProvider!.upDataImage(_memberProvider!.memberModel.name!, newPhoto!.path, PhotoType.mugshot);
    if (value['status']) {
      String _filename = value['filename'];
      _memberProvider!.changeMugshot('$_filename?v=${DateTime.now().millisecondsSinceEpoch}');
      //上傳頭像
      return await _apiProvider!.setMemberMugshot(_memberProvider!.memberModel.id!, _filename);
    } else {
      //上傳頭像
      EasyLoading.showToast("上傳頭像失敗");
      return false;
    }
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  Widget build(BuildContext context) {
    _apiProvider = Provider.of<UtilApiProvider>(context);
    _memberProvider = Provider.of<MemberProvider>(context, listen: false);
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: AppColor.appBackgroundColor,
      body: Column(
        children: [
          //返回 與 標題
          titleBarIntegrate([back()], S.of(context).edit_profile, [save()]),
          //內容
          Expanded(flex: 10, child: _body())
        ],
      ),
    );
  }

  //返回
  Widget back() {
    return GestureDetector(
      onTap: () => _backOnTap(),
      child: const Icon(
        Icons.arrow_back,
        color: AppColor.appTitleBarTextColor,
        size: 30,
      ),
    );
  }

  //儲存
  Widget save() {
    return GestureDetector(
      onTap: () => _saveOnTap(),
      child: FittedBox(
        child: Text(
          S.of(context).save,
          style: TextStyle(color: Colors.black, fontSize: 18.sp),
        ),
      ),
    );
  }

  //返回點擊
  void _backOnTap() {
    if(!EasyLoading.isShow){
      debugPrint('返回');
      _memberProvider!.reductionMemberModel();
      delegate.popRoute();
    }
  }

  //儲存點擊
  void _saveOnTap() {
    debugPrint('儲存');
    if (!_formKey.currentState!.validate()) {
      //資料未填或有誤
      if (!EasyLoading.isShow) {
        EasyLoading.showToast(S.of(context).information_is_incomplete);
      }
      return;
    }
    _setMemberInfo();
  }

  Widget _body() {
    return Container(
      decoration: const BoxDecoration(color: Colors.transparent),
      child: ScrollConfiguration(
        behavior: NoShadowScrollBehavior(),
        child: SingleChildScrollView(
          child: Form(
            key: _formKey,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                //大頭照
                _avatarImage(),
                //關於我
                _aboutMeTextField(),
                //暱稱
                _nickNameTextField(),
                //性別
                _genderTextField(),
                //生日
                _birthdayTextField(),
                //身高
                _heightTextField(),
                //體重
                _weightTextField(),
                //星座
                _constellationTextField(),
                //血型
                _bloodTypeTextField(),
                //職業
                _professionTextField(),
                //職業細項
                if(_professionDetail)
                  _professionDetailTextField(),
                //名片自介
                _businessCardTextField()
              ],
            ),
          ),
        ),
      ),
    );
  }

  //大頭照
  Widget _avatarImage() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          height: 2.h,
        ),
        SizedBox(
          width: 25.w,
          height: 10.h,
          child: Stack(
            alignment: Alignment.center,
            children: [
              //大頭照
              Selector<MemberProvider, String>(
                selector: (context, data) => data.memberModel.mugshot!,
                shouldRebuild: (pre, old) => pre != old,
                builder: (context, mugshot, _){
                  return mugshot.isEmpty && newPhoto == null
                    ? Container(
                    width: 20.w,
                    height: 10.h,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        gradient: AppColor.appMainColor),
                    child: CircleAvatar(
                      backgroundColor: Colors.transparent,
                      child: Image(
                        image: AssetImage(AppImage.iconCat),
                      ),
                    ),
                  )//本地線上沒照片
                      : newPhoto == null
                      ? SizedBox(
                    width: 20.w,
                    height: 10.h,
                    child: CachedNetworkImage(
                      imageUrl: Util()
                          .getMemberMugshotUrl(
                          mugshot) + '?v=${DateTime.now().millisecondsSinceEpoch}',
                        httpHeaders: {"authorization": "Bearer ${Api.accessToken}"},
                      imageBuilder: (context, image){
                        return Container(
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                                fit: BoxFit.cover,
                                image: image
                            )
                          ),
                        );
                      },
                      progressIndicatorBuilder: (context, _, __){
                        return Container(
                          alignment: Alignment.center,
                          padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
                          child: const CircularProgressIndicator(color: AppColor.meColor,),
                        );
                      },

                    ),
                  )///線上有照片
                      : Container(
                    width: 20.w,
                    height: 10.h,
                    decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          fit: BoxFit.cover,
                          image: FileImage(newPhoto!)),

                      ///加millisecondsSinceEpoch防止圖片路徑相同不刷新
                    ),
                  )//本地有照片
                  ;
                },
              ),
              /*Consumer<MemberProvider>(builder: (context, member, _) {
                return member.memberModel.mugshot!.isEmpty && newPhoto == null
                    ? Container(
                        width: 20.w,
                        height: 10.h,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            gradient: AppColor.appMainColor),
                        child: CircleAvatar(
                          backgroundColor: Colors.transparent,
                          child: Image(
                            image: AssetImage(AppImage.iconCat),
                          ),
                        ),
                      ) //本地線上沒照片
                    : newPhoto == null
                        ? Container(
                            width: 20.w,
                            height: 10.h,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                  fit: BoxFit.cover,
                                  image: CachedNetworkImageProvider(Util()
                                          .getMemberMugshotUrl(
                                              member.memberModel.mugshot!) + '?v=${DateTime.now().millisecondsSinceEpoch}',
                                    headers: {"authorization": "Bearer " + Api.accessToken},
                                  ),
                                ),

                              ///加millisecondsSinceEpoch防止圖片路徑相同不刷新
                            ),
                          ) //線上有照片
                        : Container(
                            width: 20.w,
                            height: 10.h,
                            decoration: BoxDecoration(
                              shape: BoxShape.circle,
                              image: DecorationImage(
                                  fit: BoxFit.cover,
                                  image: FileImage(newPhoto!)),

                              ///加millisecondsSinceEpoch防止圖片路徑相同不刷新
                            ),
                          ); //本地有照片
              }),*/
              //+號
              Positioned(
                  right: 0,
                  bottom: 0,
                  child: GestureDetector(
                    onTap: () => _changePhoto(),
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Positioned.fill(
                            child: Container(
                          margin: const EdgeInsets.all(10),
                          color: Colors.white,
                        )),
                        const Icon(
                          Icons.add_circle,
                          color: AppColor.buttonFrameColor,
                          size: 35,
                        )
                      ],
                    ),
                  ))
            ],
          ),
        ),
        //名子
        SizedBox(
          height: 3.5.h,
          child: Consumer<MemberProvider>(
            builder: (context, member, _) {
              return Text(
                '${member.memberModel.nickname}',
                style: TextStyle(color: Colors.black, fontSize: 18.sp),
              );
            },
          ),
        ),
        SizedBox(
          height: 1.h,
        )
      ],
    );
  }

  //修改照片
  void _changePhoto() {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return _chooseImage();
        });
  }

  //拍照或相簿選擇框
  Widget _chooseImage() {
    return Container(
      height: 30.h,
      padding: const EdgeInsets.only(top: 10),
      child: Column(
        children: [
          //相片選擇
          Text(
            S.of(context).photo_selection,
            style: TextStyle(
                color: AppColor.appTitleBarTextColor, fontSize: 18.sp),
          ),
          SizedBox(
            height: 2.h,
          ),
          //拍照
          GestureDetector(
            onTap: () => _takePhoto(),
            child: Container(
              width: 70.w,
              height: 9.h,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: Colors.white,
                  border:
                      Border.all(color: AppColor.buttonFrameColor, width: 1),
                  borderRadius: BorderRadius.circular(50)),
              child: Text(
                S.of(context).photograph,
                style: TextStyle(
                    color: AppColor.buttonFrameColor, fontSize: 20.sp),
              ),
            ),
          ),
          SizedBox(
            height: 2.h,
          ),
          //相簿
          GestureDetector(
            onTap: () => _selectImage(),
            child: Container(
              width: 70.w,
              height: 9.h,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: Colors.white,
                  border:
                      Border.all(color: AppColor.buttonFrameColor, width: 1),
                  borderRadius: BorderRadius.circular(50)),
              child: Text(
                S.of(context).album,
                style: TextStyle(
                    color: AppColor.buttonFrameColor, fontSize: 20.sp),
              ),
            ),
          )
        ],
      ),
    );
  }

  //打開相機
  void _takePhoto() {
    Navigator.of(context).pop();
    ImagePickers.openCamera(
            cameraMimeType: CameraMimeType.photo,
            cropConfig: CropConfig(enableCrop: true, width: 2, height: 3))
        .then((media) async {
      if (media != null) {
        newPhoto = File(media.path!);
        setState(() {});
      }
    });
  }

  //打開相簿
  void _selectImage() async {
    Navigator.of(context).pop();
    ImagePickers.pickerPaths(
            galleryMode: GalleryMode.image,
            showGif: false,
            selectCount: 1,
            showCamera: false,
            cropConfig: CropConfig(enableCrop: true, height: 1, width: 1),
            compressSize: 500,
            uiConfig: UIConfig(uiThemeColor: AppColor.buttonFrameColor))
        .then((media) async {
      if (media.isNotEmpty) {
        newPhoto = File(media.first.path!);
        setState(() {});
      }
    });
  }

  //關於我
  Widget _aboutMeTextField() {
    return Container(
      width: Device.boxConstraints.maxWidth,
      decoration: const BoxDecoration(color: Colors.white),
      child: Container(
        margin: EdgeInsets.only(left: 8.w, right: 8.w),
        alignment: Alignment.centerLeft,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              S.of(context).about_me,
              style: TextStyle(color: AppColor.grayText, fontSize: 16.sp),
            ),
            TextField(
              style: const TextStyle(color: Colors.black),
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.all(5),
                enabledBorder: OutlineInputBorder(
                    borderSide: const BorderSide(color: AppColor.grayLine),
                    borderRadius: BorderRadius.circular(5)),
                border:
                    OutlineInputBorder(borderRadius: BorderRadius.circular(5)),
                fillColor: Colors.white,
                hintText: S.of(context).about_me,
                filled: true,
              ),
              maxLines: 4,
              keyboardType: TextInputType.multiline,
              textInputAction: TextInputAction.newline,
              controller: _aboutMe,
            )
          ],
        ),
      ),
    );
  }

  //暱稱
  Widget _nickNameTextField() {
    return Container(
      width: Device.boxConstraints.maxWidth,
      decoration: const BoxDecoration(color: Colors.white),
      child: Container(
        margin: EdgeInsets.only(left: 8.w, right: 8.w),
        alignment: Alignment.centerLeft,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              S.of(context).nick_name,
              style: TextStyle(color: AppColor.grayText, fontSize: 16.sp),
            ),
            SizedBox(
              width: 80.w,
              height: 8.h,
              child: TextFormField(
                style: const TextStyle(color: Colors.black),
                decoration: InputDecoration(
                  fillColor: Colors.transparent,
                  filled: true,
                  hintStyle: const TextStyle(color: Colors.grey),
                  hintText: S.of(context).enter_a_nickname,
                ),
                keyboardType: TextInputType.name,
                textInputAction: TextInputAction.next,
                controller: _nickName,
                validator: (value) {
                  if (value == null || value.isEmpty) {
                    return S.of(context).enter_a_nickname;
                  }
                  if (value.length > Util.nicknameLen) {
                    return '${S.of(context).wordLimit}${Util.nicknameLen}';
                  }
                  return null;
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  //性別
  Widget _genderTextField() {
    return Container(
      width: Device.boxConstraints.maxWidth,
      decoration: const BoxDecoration(color: Colors.white),
      child: Container(
        margin: EdgeInsets.only(left: 8.w, right: 8.w),
        alignment: Alignment.centerLeft,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              S.of(context).gender,
              style: TextStyle(color: AppColor.grayText, fontSize: 16.sp),
            ),
            SizedBox(
              width: 80.w,
              height: 8.h,
              child: ButtonTheme(
                alignedDropdown: true,
                child: DropdownButton(
                  hint: Text(S.of(context).select_gender),
                  underline: Container(
                    height: 1.0,
                    decoration: const BoxDecoration(
                      border: Border(
                        bottom: BorderSide(
                          color: Color(0xFF8E8E8E),
                        ),
                      ),
                    ),
                  ),
                  dropdownColor: Colors.white,
                  icon: const Icon(Icons.arrow_drop_down_sharp),
                  items: _genderItem,
                  value: _genderValue,
                  onChanged: (Gender? _value) {
                    setState(() {
                      _genderValue = _value!;
                      if (_genderValue == Gender.male) {
                        _gender.text = S.of(context).male;
                      } else {
                        _gender.text = S.of(context).female;
                      }
                    });
                  },
                  isExpanded: true,
                  iconSize: 40,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  //生日
  Widget _birthdayTextField() {
    return Container(
      width: Device.boxConstraints.maxWidth,
      decoration: const BoxDecoration(color: Colors.white),
      child: Container(
        margin: EdgeInsets.only(left: 8.w, right: 8.w),
        alignment: Alignment.centerLeft,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 4.h,
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Positioned(left: 0, child: Text(
                    S.of(context).birthday,
                    style: TextStyle(color: AppColor.grayText, fontSize: 16.sp),
                  )),
                  Positioned(
                    right: 0,
                    child: Consumer<MemberProvider>(builder: (context, member, _){
                      return _switchButton(text: S.of(context).hide, isTurnOn: member.hideBirthday ? true : false, valueChanged: _hideBirthdayValueChange());
                    }),)
                ],
              ),
            ),
            SizedBox(
              width: 80.w,
              height: 8.h,
              child: Stack(
                alignment: Alignment.center,
                children: [
                  TextField(
                    readOnly: true,
                    onTap: () => _selectBirthDay(),
                    style: const TextStyle(color: Colors.black),
                    decoration: InputDecoration(
                      fillColor: Colors.transparent,
                      filled: true,
                      hintStyle: const TextStyle(color: Colors.grey),
                      hintText: S.of(context).select_a_birthday,
                    ),
                    keyboardType: TextInputType.number,
                    textInputAction: TextInputAction.next,
                    controller: _birthday,
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }

  //身高
  Widget _heightTextField() {
    return Container(
      width: Device.boxConstraints.maxWidth,
      decoration: const BoxDecoration(color: Colors.white),
      child: Container(
        margin: EdgeInsets.only(left: 8.w, right: 8.w),
        alignment: Alignment.centerLeft,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              S.of(context).height,
              style: TextStyle(color: AppColor.grayText, fontSize: 16.sp),
            ),
            SizedBox(
              width: 80.w,
              height: 8.h,
              child: Focus(
                canRequestFocus: false,
                onFocusChange: (hasFocus) {
                  if (_height.text.isNotEmpty) {
                    _height.text = _height.text.replaceAll(' cm', '');
                    if (!hasFocus) {
                      _height.text = '${_height.text} cm';
                    }
                  }
                },
                child: TextFormField(
                  style: const TextStyle(color: Colors.black),
                  decoration: const InputDecoration(
                    fillColor: Colors.transparent,
                    filled: true,
                    hintStyle: TextStyle(color: Colors.grey),
                    hintText: '',
                  ),
                  inputFormatters: [
                    FilteringTextInputFormatter.allow(RegExp(r"[0-9.cm]")),
                  ],
                  keyboardType: const TextInputType.numberWithOptions(
                      decimal: true, signed: false),
                  textInputAction: TextInputAction.next,
                  controller: _height,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  //體重
  Widget _weightTextField() {
    return Container(
      width: Device.boxConstraints.maxWidth,
      decoration: const BoxDecoration(color: Colors.white),
      child: Container(
        margin: EdgeInsets.only(left: 8.w, right: 8.w),
        alignment: Alignment.centerLeft,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 4.h,
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Positioned(left: 0, child: SizedBox(
                    width: 60.w,
                    child: Text(
                      S.of(context).weight,
                      style: TextStyle(color: AppColor.grayText, fontSize: 16.sp, height: 1.1),
                    ),
                  )),
                  Positioned(right: 0, child: Consumer<MemberProvider>(builder: (context, member, _){
                    return _switchButton(text: S.of(context).hide, isTurnOn: member.hideWeight ? true : false, valueChanged: _hideWeightValueChange());
                  }))
                ],
              ),
            ),
            SizedBox(
            width: 80.w,
            height: 8.h,
            child: Focus(
              canRequestFocus: false,
              onFocusChange: (hasFocus) {
                if (_weight.text.isNotEmpty) {
                  _weight.text = _weight.text.replaceAll(' kg', '');
                  if (!hasFocus) {
                    _weight.text = '${_weight.text} kg';
                  }
                }
              },
              child: TextFormField(
                style: const TextStyle(color: Colors.black),
                decoration: const InputDecoration(
                  fillColor: Colors.transparent,
                  filled: true,
                  hintStyle: TextStyle(color: Colors.grey),
                  hintText: '',
                ),
                inputFormatters: [
                  FilteringTextInputFormatter.allow(RegExp(r"[0-9.kg]")),
                ],
                keyboardType: const TextInputType.numberWithOptions(
                    decimal: true, signed: false),
                textInputAction: TextInputAction.next,
                controller: _weight,
              ),
            ),
          )
          ],
        ),
      ),
    );
  }

  //星座
  Widget _constellationTextField() {
    return Container(
      width: Device.boxConstraints.maxWidth,
      decoration: const BoxDecoration(color: Colors.white),
      child: Container(
        margin: EdgeInsets.only(left: 8.w, right: 8.w),
        alignment: Alignment.centerLeft,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              S.of(context).constellation,
              style: TextStyle(color: AppColor.grayText, fontSize: 16.sp),
            ),
            SizedBox(
              width: 80.w,
              height: 8.h,
              child: TextFormField(
                readOnly: true,
                style: const TextStyle(color: Colors.black),
                decoration: const InputDecoration(
                  fillColor: Colors.transparent,
                  filled: true,
                  hintStyle: TextStyle(color: Colors.grey),
                ),
                keyboardType: TextInputType.name,
                textInputAction: TextInputAction.next,
                controller: _constellation,
              ),
            )
          ],
        ),
      ),
    );
  }

  //血型
  Widget _bloodTypeTextField() {
    return Container(
      width: Device.boxConstraints.maxWidth,
      decoration: const BoxDecoration(color: Colors.white),
      child: Container(
        margin: EdgeInsets.only(left: 8.w, right: 8.w),
        alignment: Alignment.centerLeft,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              S.of(context).blood_type,
              style: TextStyle(color: AppColor.grayText, fontSize: 16.sp),
            ),
            SizedBox(
              width: 80.w,
              height: 8.h,
              child: ButtonTheme(
                alignedDropdown: true,
                child: DropdownButton(
                  hint: Text(S.of(context).not_selected),
                  underline: Container(
                    height: 1.0,
                    decoration: const BoxDecoration(
                      border: Border(
                        bottom: BorderSide(
                          color: Color(0xFF8E8E8E),
                        ),
                      ),
                    ),
                  ),
                  dropdownColor: Colors.white,
                  icon: const Icon(Icons.arrow_drop_down_sharp),
                  items: _bloodTypeItem,
                  value: _bloodTypeValue,
                  onChanged: (String? _value) {
                    setState(() {
                      _bloodTypeValue = _value!;
                      _bloodType.text = _bloodTypeValue;
                    });
                  },
                  isExpanded: true,
                  iconSize: 40,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  //職業
  Widget _professionTextField() {
    return Container(
      width: Device.boxConstraints.maxWidth,
      decoration: const BoxDecoration(color: Colors.white),
      child: Container(
        margin: EdgeInsets.only(left: 8.w, right: 8.w),
        alignment: Alignment.centerLeft,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(
              height: 4.h,
              child: Stack(
                alignment: Alignment.center,
                children: [
                  Positioned(left: 0, child: Text(
                    S.of(context).profession,
                    style: TextStyle(color: AppColor.grayText, fontSize: 16.sp),
                  )),
                  Positioned(right: 0, child: Consumer<MemberProvider>(builder: (context, member, _){
                    return _switchButton(text: S.of(context).hide, isTurnOn: member.hideJob ? true : false, valueChanged: _hideJobValueChange());
                  }))

                ],
              ),
            ),
            SizedBox(
              width: 80.w,
              height: 8.h,
              child: ButtonTheme(
                alignedDropdown: true,
                child: DropdownButton(
                  hint: Text(S.of(context).not_selected),
                  underline: Container(
                    height: 1.0,
                    decoration: const BoxDecoration(
                      border: Border(
                        bottom: BorderSide(
                          color: Color(0xFF8E8E8E),
                        ),
                      ),
                    ),
                  ),
                  dropdownColor: Colors.white,
                  icon: const Icon(Icons.arrow_drop_down_sharp),
                  items: _professionItem,
                  value: _professionValue,
                  onChanged: (String? _value) {
                    setState(() {
                      _professionValue = _value!;
                      if(_professionValue == '其他'){
                        _profession.text = '';
                        _professionDetail = true;
                      }else{
                        _profession.text = _professionValue;
                        _professionDetail = false;
                      }
                    });
                  },
                  isExpanded: true,
                  iconSize: 40,
                ),
              ),
            ),
            if(!_professionDetail)
            SizedBox(
              height: 2.h,
            )
          ],
        ),
      ),
    );
  }

  //職業細項
  Widget _professionDetailTextField(){
    return Container(
      width: Device.boxConstraints.maxWidth,
      decoration: const BoxDecoration(color: Colors.white),
      child: Container(
        margin: EdgeInsets.only(left: 8.w, right: 8.w),
        alignment: Alignment.centerLeft,
        child: SizedBox(
          width: 80.w,
          height: 8.h,
          child: TextFormField(
            style: const TextStyle(color: Colors.black),
            decoration: InputDecoration(
              fillColor: Colors.transparent,
              filled: true,
              hintStyle: const TextStyle(color: Colors.grey),
              hintText: S.of(context).enter_a_profession,
            ),
            keyboardType: TextInputType.multiline,
            textInputAction: TextInputAction.done,
            controller: _profession,
            validator: (value) {
              if (value == null || value.isEmpty) {
                return S.of(context).enter_a_profession;
              }
              return null;
            },
          ),
        ),
      ),
    );
  }

  //名片自介
  Widget _businessCardTextField(){
    return Container(
      width: Device.boxConstraints.maxWidth,
      decoration: const BoxDecoration(color: Colors.white),
      child: Container(
        margin: EdgeInsets.only(left: 8.w, right: 8.w, bottom: 3.h),
        alignment: Alignment.centerLeft,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              S.of(context).business_card_introduction,
              style: TextStyle(color: AppColor.grayText, fontSize: 16.sp),
            ),
            TextField(
              style: const TextStyle(color: Colors.black),
              decoration: InputDecoration(
                contentPadding: const EdgeInsets.all(5),
                enabledBorder: OutlineInputBorder(
                    borderSide: const BorderSide(color: AppColor.grayLine),
                    borderRadius: BorderRadius.circular(5)),
                border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(5)),
                fillColor: Colors.white,
                hintText: S.of(context).business_card_introduction,
                filled: true,
              ),
              maxLines: 4,
              keyboardType: TextInputType.multiline,
              textInputAction: TextInputAction.newline,
              controller: _businessCard,
            )
          ],
        ),
      ),
    );
  }

  //選擇日期
  void _selectBirthDay() {
    DateTime _now = DateTime.now();
    DatePicker.showDatePicker(context,
        showTitleActions: true,
        minTime: DateTime(1922, 1, 1),
        maxTime: DateTime(_now.year - 18, _now.month, _now.day),
        onChanged: (date) {
      debugPrint('change $date');
      _birthDayMap = Util().getBirthDay(date);
      _birthday.text = _birthDayMap['showText']!;
      setState(() {});
    }, onConfirm: (date) {
      debugPrint('confirm $date');
      _birthDayMap = Util().getBirthDay(date);
      _birthday.text = _birthDayMap['showText']!;
      _constellationValue =
          Util().dateToHoroscope(_birthDayMap['showText']!).name;
      _constellation.text = S
          .of(context)
          .horoscope(Util().dateToHoroscope(_birthDayMap['showText']!).name);
      setState(() {});
    }, onCancel: () {
      _birthDayMap = {};
      _birthday.text = '';
      _constellation.text = S.of(context).horoscope(Horoscope.aries.name);
      _constellationValue = Horoscope.aries.name;
      setState(() {});
    }, locale: LocaleType.tw);
  }

  /// 隱藏開關
  /// * [text] 文字
  /// * [isTurnOn] 開關狀態
  /// * [valueChanged] 狀態改變監聽
  Widget _switchButton({
    required String text,
    required bool isTurnOn,
    required ValueChanged valueChanged
  }){
    return Container(
      padding: const EdgeInsets.fromLTRB(20, 20, 0, 20),
      child: Row(
        children: [
          Text(
            text,
            style: const TextStyle(fontSize: 16, color: AppColor.grayText),
          ),
          CupertinoSwitch(
              value: isTurnOn,
              activeColor: AppColor.pinkRedColor,
              onChanged: valueChanged)
        ],
      ),
    );
  }
}
