import 'dart:developer';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_font_icons/flutter_font_icons.dart';
import 'package:image_pickers/image_pickers.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../enum/app_enum.dart';
import '../generated/l10n.dart';
import '../provider/member_provider.dart';
import '../provider/util_api_provider.dart';
import '../utils/api.dart';
import '../utils/app_color.dart';
import '../utils/shared_preference.dart';
import '../utils/util.dart';
import '../widget/button_widget.dart';
import '../widget/titlebar_widget.dart';

class FaceRecognitionPage extends StatefulWidget {
  const FaceRecognitionPage({Key? key}) : super(key: key);

  @override
  State createState() => _FaceRecognitionPageState();
}

class _FaceRecognitionPageState extends State<FaceRecognitionPage> {
  MemberProvider? _memberProvider;
  UtilApiProvider? _apiProvider;
  PhotoType _photoType = PhotoType.tmp;

  //原本的頭像
  String _filename = '';

  _onLayoutDone(_) async {
    if(_memberProvider!.memberModel.facePic != null){
      _filename = _memberProvider!.memberModel.facePic!;
    }
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  Widget build(BuildContext context) {
    _apiProvider = Provider.of<UtilApiProvider>(context);
    _memberProvider = Provider.of<MemberProvider>(context);
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: AppColor.appBackgroundColor,
      body: Column(
        children: [
          //返回 與 標題
          titleBar(S.of(context).face_recognition),
          //內容
          Expanded(flex: 10, child: _body())
        ],
      ),
    );
  }

  //內容
  Widget _body() {
    return Container(
      width: Device.width,
      margin: EdgeInsets.only(top: 2.h),
      decoration: const BoxDecoration(
        color: Colors.white,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          //人臉辨識
          _avatarImage(),
          //請上傳一張五官清晰、正面照供人臉辨識用 / 已驗證
          SizedBox(
            height: 5.h,
          ),
          Consumer<MemberProvider>(builder: (context, member, _) {
            return member.memberModel.verify == VerifyType.yes.index
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      const Icon(
                        FontAwesome5Solid.check_circle,
                        color: AppColor.buttonFrameColor,
                      ),
                      SizedBox(
                        width: 1.w,
                      ),
                      Text(
                        S.of(context).verified,
                        style: const TextStyle(
                            color: AppColor.buttonFrameColor, fontSize: 18),
                      )
                    ],
                  )
                : Container(
                    margin: const EdgeInsets.only(right: 40, left: 40),
                    child: FittedBox(
                      child: RichText(
                        text: TextSpan(
                            text: S.of(context).please_upload_one,
                            style: const TextStyle(
                                color: Colors.black,
                                fontSize: 20,
                                fontWeight: FontWeight.bold),
                            children: [
                              TextSpan(
                                text: S
                                    .of(context)
                                    .clear_facial_features_frontal_photo,
                                style: const TextStyle(
                                    color: AppColor.buttonFrameColor,
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold),
                              ),
                              TextSpan(
                                text: S.of(context).for_face_recognition,
                                style: const TextStyle(
                                    color: Colors.black,
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold),
                              )
                            ]),
                      ),
                    ),
                  );
          }),
          SizedBox(
            height: 5.h,
          ),
          //※若審核成功後需更改人臉辨識照片， 請聯繫客服
          Text(
            S.of(context).face_recognition_warning,
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.grey[400]!, fontSize: 18),
          ),
          //確認上傳
          SizedBox(
            height: 5.h,
          ),
          _upLoadImageButton()
        ],
      ),
    );
  }

  //大頭照
  Widget _avatarImage() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
          height: 2.h,
        ),
        SizedBox(
          width: 25.w * 2,
          height: 10.h * 2,
          child: Consumer<MemberProvider>(
            builder: (context, member, _) {
              return Stack(
                alignment: Alignment.center,
                children: [
                  //大頭照 已驗證 需重新驗證 驗證中
                  member.memberModel.verify == VerifyType.yes.index || member.memberModel.verify == VerifyType.reVerify.index || member.memberModel.verify == VerifyType.review.index
                      ? Container(
                          width: 20.w * 2,
                          height: 10.h * 2,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                                fit: BoxFit.cover,
                                image: CachedNetworkImageProvider(Util()
                                    .getMemberMugshotUrl(
                                        member.memberModel.facePic!) + '?v=${DateTime.now().millisecondsSinceEpoch}',
                                  headers: {"authorization": "Bearer " + Api.accessToken},),
                                onError: (error, stackTrace) {
                                  debugPrint('Error: ${error.toString()}');
                                  if(error.toString().contains('401,')){
                                    setState(() {});
                                  }
                                }),
                          ),
                        ) //線上有照片
                      : _filename.isNotEmpty //未驗證過
                          ? Container(
                              width: 20.w * 2,
                              height: 10.h * 2,
                              decoration: BoxDecoration(
                                shape: BoxShape.circle,
                                image: DecorationImage(
                                    fit: BoxFit.cover,
                                    image: CachedNetworkImageProvider(Util()
                                        .getMemberMugshotUrl(_filename)  + '?v=${DateTime.now().millisecondsSinceEpoch}',
                                      headers: {"authorization": "Bearer " + Api.accessToken},),
                                    onError: (error, stackTrace) {
                                      debugPrint('Error: ${error.toString()}');
                                      if(error.toString().contains('401,')){
                                        setState(() {});
                                      }
                                    }
                                ),
                              ),
                            )
                          : Container(
                              width: 20.w * 2,
                              height: 10.h * 2,
                              decoration: const BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: AppColor.appFaceBackgroundColor),
                              child: const CircleAvatar(
                                backgroundColor: Colors.transparent,
                                child: Icon(
                                  FontAwesome5Solid.user,
                                  size: 40 * 2,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                  //+號 未驗證 需重新驗證
                  if (member.memberModel.verify == VerifyType.no.index || member.memberModel.verify == VerifyType.reVerify.index)
                    Positioned(
                        right: 0,
                        bottom: 0,
                        child: GestureDetector(
                          onTap: () => _changePhoto(),
                          child: Stack(
                            alignment: Alignment.center,
                            children: [
                              Positioned.fill(
                                  child: Container(
                                margin: const EdgeInsets.all(15),
                                color: Colors.white,
                              )),
                              const Icon(
                                Icons.add_circle,
                                color: AppColor.buttonFrameColor,
                                size: 60,
                              )
                            ],
                          ),
                        )),
                  if (member.memberModel.verify == VerifyType.yes.index)
                    Positioned(
                        right: 5,
                        bottom: 5,
                        child: GestureDetector(
                          onTap: () => _changePhoto(),
                          child: Stack(
                            alignment: Alignment.center,
                            children: [
                              Positioned.fill(
                                  child: Container(
                                    margin: const EdgeInsets.all(15),
                                    color: Colors.transparent,
                                  )),
                              Container(
                                height: 50,
                                width: 50,
                                decoration: const BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: AppColor.buttonFrameColor
                                ),
                              ),
                              const Icon(
                                FontAwesome5Solid.pen,
                                color: Colors.white,
                                size: 25,
                              )
                            ],
                          ),
                        ))
                ],
              );
            },
          ),
        ),
      ],
    );
  }

  //修改照片
  void _changePhoto() {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext context) {
          return _chooseImage();
        });
  }

  //拍照或相簿選擇框
  Widget _chooseImage() {
    return Container(
      height: 15.h,
      padding: const EdgeInsets.only(top: 10),
      child: Column(
        children: [
          SizedBox(
            height: 2.h,
          ),
          //拍照
          GestureDetector(
            onTap: () => _takePhoto(),
            child: Container(
              width: 70.w,
              height: 9.h,
              alignment: Alignment.center,
              decoration: BoxDecoration(
                  color: Colors.white,
                  border:
                      Border.all(color: AppColor.buttonFrameColor, width: 1),
                  borderRadius: BorderRadius.circular(30)),
              child: Text(
                S.of(context).photograph,
                style: TextStyle(
                    color: AppColor.buttonFrameColor, fontSize: 20.sp),
              ),
            ),
          ),
          SizedBox(
            height: 2.h,
          ),
        ],
      ),
    );
  }

  //打開相機
  void _takePhoto() {
    Navigator.of(context).pop();
    ImagePickers.openCamera(
            cameraMimeType: CameraMimeType.photo,
            cropConfig: CropConfig(enableCrop: true, width: 2, height: 3))
        .then((media) async {
      if (media != null) {
        //上傳圖片並顯示成功或失敗
        EasyLoading.show(status: S.of(context).uploading);
        _photoType = PhotoType.face;
        _apiProvider!
            .upDataImage(
                _memberProvider!.memberModel.name!, media.path!, _photoType)
            .then((value) {
          debugPrint('拍照成功或失敗: ${value['status']}');
          if (value['status']) {
            _filename = value['filename'];
            setState(() {});
            EasyLoading.showToast(S.of(context).finish);
          } else {
            EasyLoading.showToast(S.of(context).fail);
          }
        });
      }
    });
  }

  //打開相簿
  void _selectImage() async {
    Navigator.of(context).pop();
    ImagePickers.pickerPaths(
            galleryMode: GalleryMode.image,
            showGif: false,
            selectCount: 1,
            showCamera: false,
            cropConfig: CropConfig(enableCrop: false, height: 1, width: 1),
            compressSize: 500,
            uiConfig: UIConfig(uiThemeColor: AppColor.buttonFrameColor))
        .then((media) {
      if (media.isNotEmpty) {
        EasyLoading.show(status: S.of(context).uploading);
        _photoType = PhotoType.face;
        //上傳圖片並顯示成功或失敗
        _apiProvider!
            .upDataImage(_memberProvider!.memberModel.name!, media.first.path!,
                _photoType)
            .then((value) {
          debugPrint('相簿成功或失敗: ${value['status']}');
          if (value['status']) {
            _filename = value['filename'];
            setState(() {});
            EasyLoading.showToast(S.of(context).finish);
          } else {
            EasyLoading.showToast(S.of(context).fail);
          }
        });
      }
    });
  }

  //確認上傳
  Widget _upLoadImageButton() {
    return Consumer<MemberProvider>(builder: (context, member, _){
      return GestureDetector(
          onTap: () => (member.memberModel.verify == VerifyType.yes.index && _filename.isEmpty) || member.memberModel.verify == VerifyType.review.index ? null : _upLoadImageOnTap(),
          child: Container(
              width: 58.w,
              height: 7.h,
              margin: EdgeInsets.symmetric(vertical: 5.h),
              child: (member.memberModel.verify == VerifyType.yes.index && _filename.isEmpty) || member.memberModel.verify == VerifyType.review.index
                  ? buttonWidgetDispose(S.of(context).confirm_upload)
                  : buttonWidget(S.of(context).confirm_upload)));
    });
  }

  void _upLoadImageOnTap() {
    if (_filename.isNotEmpty && !EasyLoading.isShow) {
      EasyLoading.show(status: S.of(context).verifying);
      _apiProvider!
          .setMemberFaceImage(_memberProvider!.memberModel.id!, _filename)
          .then((value) {
        if (value) {
            _apiProvider!.putReview(id: _memberProvider!.memberModel.id!).then((value) {
              _memberProvider!
                  .getMember(_memberProvider!.memberModel.id!)
                  .then((value) {
                EasyLoading.showToast(S.of(context).finish);

              });
            });
        } else {
          EasyLoading.showToast(S.of(context).failed_to_upload_again);
        }
      });
    } else {
      EasyLoading.showToast(S.of(context).please_select_an_identifying_photo);
    }
  }
}
