import 'dart:developer';
import 'dart:io';

import 'package:ashera_flutter/dialog/identify_friends_dialog.dart';
import 'package:ashera_flutter/dialog/qr_check_dialog.dart';
import 'package:ashera_flutter/models/faces_detect_history_model.dart';
import 'package:ashera_flutter/provider/member_provider.dart';
import 'package:ashera_flutter/provider/position_provider.dart';
import 'package:ashera_flutter/provider/stomp_client_provider.dart';
import 'package:ashera_flutter/provider/util_api_provider.dart';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_font_icons/flutter_font_icons.dart';
import 'package:flutter_switch/flutter_switch.dart';
import 'package:provider/provider.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart' as qr_code;
import 'package:responsive_sizer/responsive_sizer.dart';

import '../app.dart';
import '../dialog/my_qr_code_dialog.dart';
import '../enum/app_enum.dart';
import '../generated/l10n.dart';
import '../routers/route_name.dart';
import '../utils/app_color.dart';
import '../widget/scan_ui.dart';

class CameraPage extends StatefulWidget {
  const CameraPage({Key? key}) : super(key: key);

  @override
  State createState() => _CameraPageState();
}

class _CameraPageState extends State<CameraPage>
    with TickerProviderStateMixin, WidgetsBindingObserver {
  final GlobalKey _key = GlobalKey();
  UtilApiProvider? _utilApiProvider;
  MemberProvider? _memberProvider;
  StompClientProvider? _stompClientProvider;
  PositionProvider? _positionProvider;

  final GlobalKey qrKey = GlobalKey();
  //掃描到的內容
  qr_code.Barcode? result;
  //QR控制器
  qr_code.QRViewController? _qrViewController;
  //狀態
  PhotographOrScanning? photographOrScanning;
  //相機控制器
  CameraController? _cameraController;
  //拍攝的照片
  List<CameraDescription> _cameras = [];
  XFile? imageFile; //檔案

  bool _btnStatus = false; //防止切換太快造成崩潰
  double x = 0;
  double y = 0;
  bool _showFocusCircle = false; //手動對焦的框

  double _zoom = 1.0;
  double _scaleFactor = 1.0;



  void _getCameras() async {
    _cameras = await availableCameras();
    _cameraController =
        CameraController(_cameras[0], ResolutionPreset.max, imageFormatGroup: ImageFormatGroup.yuv420, enableAudio: false,);
    _cameraController!.setFlashMode(FlashMode.off);
    _cameraController!.initialize().then((value) {
      if (!mounted) {
        return;
      }
      _btnStatus = false;
      log("74");
      setState(() {});
    });
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addObserver(this);
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    final CameraController? cameraController = _cameraController;

    // App state changed before we got the chance to initialize.
    if (cameraController == null || !cameraController.value.isInitialized) {
      return;
    }

    if (state == AppLifecycleState.inactive) {
      cameraController.dispose();
    } else if (state == AppLifecycleState.resumed) {
      onNewCameraSelected(cameraController.description);
    }
  }

  Future<void> onNewCameraSelected(CameraDescription cameraDescription) async {
    if (_cameraController != null) {
      await _cameraController!.dispose();
    }

    final CameraController cameraController = CameraController(
      cameraDescription,
      ResolutionPreset.max,
      enableAudio: false,
      imageFormatGroup: ImageFormatGroup.yuv420,
    );
    cameraController.setFlashMode(FlashMode.off);

    _cameraController = cameraController;

    // If the controller is updated then update the UI.
    /*cameraController.addListener(() {
      if (mounted) {
        log("118");
        setState(() {});
      }
      if (cameraController.value.hasError) {
        log('Camera error ${cameraController.value.errorDescription}');
      }
    });*/

    try {
      await cameraController.initialize();
      _btnStatus = false;
    } on CameraException catch (e) {
      log('CameraException ${e.toString()}');
    }

    if (mounted) {
      log("133");
      setState(() {});
    }
  }

  @override
  void reassemble() {
    super.reassemble();
    if (Platform.isAndroid) {
      _qrViewController?.pauseCamera();
    } else if (Platform.isIOS) {
      _qrViewController!.resumeCamera();
    }
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    _qrViewController?.dispose();
    _cameraController?.dispose();
    super.dispose();
  }

  void _onQRViewCreated(qr_code.QRViewController _controller) {
    _qrViewController = _controller;
    _qrViewController!.resumeCamera();
    _controller.scannedDataStream.listen((scanData) {
      log('scanData ${scanData.code}');
      if (result == null) {
        result = scanData;
        _utilApiProvider!.getUidSearchMember(result!.code!).then((value) {
          if (value) {
            _showQrCheckDialog();
          } else {
            result = null;
          }
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    _utilApiProvider = Provider.of<UtilApiProvider>(context, listen: false);
    _memberProvider = Provider.of<MemberProvider>(context, listen: false);
    _stompClientProvider = Provider.of<StompClientProvider>(context, listen: false);
    _positionProvider = Provider.of<PositionProvider>(context, listen: false);
    dynamic obj = ModalRoute.of(context)?.settings.arguments;
    if (photographOrScanning == null) {
      photographOrScanning = obj['type'];
      if (photographOrScanning == PhotographOrScanning.photograph) {
        _getCameras();
      }
    }
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: AppColor.appBackgroundColor,
      body: Stack(
        fit: StackFit.expand,
        alignment: Alignment.center,
        children: [
          photographOrScanning == PhotographOrScanning.scanning
              ? qr_code.QRView(
                  key: qrKey,
                  onQRViewCreated: _onQRViewCreated,
                )
              : _cameraPreviewWidget(),
          Positioned(
              top: 40.h,
              child: CustomPaint(
                painter: ScanFramePainter(),
              )),
          //關閉
          Positioned(
              right: 5.w,
              top: 3.h,
              child: GestureDetector(
                onTap: () => _close(),
                child: const Icon(
                  Icons.clear,
                  size: 30,
                  color: Colors.white,
                ),
              )),
          //提示文字
          Positioned(
              bottom: 20.h,
              child: FittedBox(
                child: Text(
                  photographOrScanning == PhotographOrScanning.scanning
                      ? S.of(context).scan_the_QR_CODE
                      : S.of(context).scan_your_face_for_identification,
                  style: TextStyle(color: Colors.white, fontSize: 20.sp),
                ),
              )),
          //按鈕
          Positioned(
              bottom: 10.h,
              child: GestureDetector(
                onTap: () => _action(),
                child: Container(
                  alignment: Alignment.center,
                  height: 5.h,
                  width: photographOrScanning == PhotographOrScanning.scanning
                      ? 35.w
                      : 20.w,
                  padding: EdgeInsets.only(right: 2.w, left: 2.w),
                  decoration: BoxDecoration(
                      color: Colors.black54,
                      borderRadius: BorderRadius.circular(30)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Icon(
                        photographOrScanning == PhotographOrScanning.scanning
                            ? FontAwesome5Solid.qrcode
                            : Icons.camera_alt,
                        color: Colors.white,
                        size: 25,
                      ),
                      SizedBox(
                        width: 2.w,
                      ),
                      Expanded(
                          child: FittedBox(
                        child: Text(
                          photographOrScanning == PhotographOrScanning.scanning
                              ? S.of(context).show_mobile_barcode
                              : S.of(context).photograph,
                          style:
                              TextStyle(color: Colors.white, fontSize: 16.sp),
                        ),
                      ))
                    ],
                  ),
                ),
              )),
          //選擇QR或拍照
          Positioned(
              left: 5.w,
              top: 3.h,
              child: FlutterSwitch(
                width: 20.w,
                height: 5.h,
                toggleSize: 30.0,
                value: photographOrScanning == PhotographOrScanning.scanning,
                borderRadius: 20.0,
                padding: 1.0,
                activeToggleColor: Colors.white,
                inactiveToggleColor: Colors.white,
                activeSwitchBorder: Border.all(
                  color: Colors.white70,
                  width: 6.0,
                ),
                inactiveSwitchBorder: Border.all(
                  color: Colors.white70,
                  width: 6.0,
                ),
                activeColor: Colors.grey[400]!,
                inactiveColor: Colors.grey[400]!,
                activeIcon: const Icon(FontAwesome5Solid.qrcode),
                inactiveIcon: const Icon(Icons.camera_alt),
                onToggle: (val) => _onToggle(val),
              )),
          //相機縮放
          if(photographOrScanning == PhotographOrScanning.photograph)
          GestureDetector(
            behavior: HitTestBehavior.translucent,
            onScaleStart: (details){
              _zoom = _scaleFactor;
            },
            onScaleUpdate: (details){
              _scaleFactor = _zoom * details.scale;
              if(_scaleFactor >= 1.0 && _scaleFactor <= 8.0){
                _cameraController!.setZoomLevel(_scaleFactor);
              }
            },
          )
        ],
      ),
    );
  }

  void _onToggle(_value) {
    log('onToggle $_value btnStatus $_btnStatus');
    if (!_btnStatus) {
      _btnStatus = true;
      final CameraController? cameraController = _cameraController;
      if (_value) {
        photographOrScanning = PhotographOrScanning.scanning;
        cameraController?.dispose();
        _btnStatus = false;
        log("325");
        setState(() {});
      } else {
        photographOrScanning = PhotographOrScanning.photograph;
        if (cameraController == null) {
          _getCameras();
        } else {
          onNewCameraSelected(cameraController.description);
        }
      }
    }
  }

  //關閉
  void _close() {
    delegate.popRoute();
  }

  //動作
  void _action() {
    if (photographOrScanning == PhotographOrScanning.scanning) {
      //我的QRCode
      _myQRCodeOnTap();
    } else {
      //拍照
      onTakePicturePressed();
    }
  }

  //我的QRCode
  void _myQRCodeOnTap() {
    /*showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return Container();
        },
        barrierDismissible: false,
        barrierColor: Colors.transparent,
        transitionDuration: const Duration(milliseconds: 200),
        transitionBuilder: (context, anim1, anim2, child) {
          return Transform.scale(
            scale: anim1.value,
            child: MyQRCodeDialog(key: UniqueKey(),),
          );
        });*/
    delegate.push(name: RouteName.myQrCodePage);
  }

  void _showQrCheckDialog() async {
    bool? result = await showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return QRCheckDialog(
            key: UniqueKey(),
          );
        },
        barrierColor: Colors.transparent,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 200),
        transitionBuilder: (context, anim1, anim2, child) {
          return Transform.scale(
            scale: anim1.value,
            child: child,
          );
        });
    if (result != null) {
      if (result) {
        _stompClientProvider!.sendFollowerRequest(
            _memberProvider!.memberModel.id!,
            _utilApiProvider!.memberModel!.id!);
        _utilApiProvider!.setUidSearchMemberClear();
        //關閉
        delegate.popRoute();
      } else {
        //清除搜群道的使用者
        _utilApiProvider!.setUidSearchMemberClear();
        //關閉
        delegate.popRoute();
      }
    }
  }

  //拍照
  void onTakePicturePressed() {
    takePicture().then((XFile? file) {
      if (mounted) {
        EasyLoading.show(status: S.of(context).uploading_comparison);
        //imageFile = file; //<- 拍照結果
        PhotoType _photoType = PhotoType.tmp;
        _utilApiProvider!
            .upDataImage(
                _memberProvider!.memberModel.name!, file!.path, _photoType)
            .then((value) {
          //先上傳照片
          EasyLoading.dismiss();
          if (value['status']) {
            //上傳成功
            EasyLoading.show(status: S.of(context).photo_comparison);
            String _fileName = value['filename'];
            _utilApiProvider!.getMemberByFaceImage(_fileName).then((value) {
              if (value) {
                if (_utilApiProvider!.faceImageList.isNotEmpty) {
                  //有找到人
                  EasyLoading.dismiss();
                  //From 是我 Target是對方
                  AddFacesDetectHistoryModel _addFacesDetectHistoryModel =
                      AddFacesDetectHistoryModel(
                          fromMemberId: _memberProvider!.memberModel.id,
                          fromMemberName: _memberProvider!.memberModel.name,
                          fromMemberNickname:
                              _memberProvider!.memberModel.nickname,
                          fromMemberGender: _memberProvider!.memberModel.gender,
                          targetMemberId:
                              _utilApiProvider!.faceImageList.first.id,
                          targetMemberName:
                              _utilApiProvider!.faceImageList.first.name,
                          targetMemberNickname:
                              _utilApiProvider!.faceImageList.first.nickname,
                          targetMemberGender:
                              _utilApiProvider!.faceImageList.first.gender,
                          latitude: _positionProvider!.currentPosition.latitude,
                          longitude:
                              _positionProvider!.currentPosition.longitude,
                          address: _positionProvider!.address,
                          detectType: DetectType.followFriend.index);
                  _utilApiProvider!
                      .postFacesDetectHistory(
                          addFacesDetectHistoryModel:
                              _addFacesDetectHistoryModel)
                      .then((value) {
                    //跳出 是否加為好友
                    log('開啟對話框');
                    _showSearchFaceMember();
                  });
                } else {
                  //沒找到人
                  EasyLoading.dismiss();
                  EasyLoading.showToast(
                      S.of(context).no_profile_for_this_person);
                }
              } else {
                //沒找到人
                EasyLoading.dismiss();
                EasyLoading.showToast(S.of(context).no_profile_for_this_person);
              }
            });
          } else {
            //沒上傳成功
            EasyLoading.dismiss();
            EasyLoading.showToast(S.of(context).fail);
          }
        });
      }
      if (file != null) {
        log('Picture saved to ${file.path}');
      }
    });
  }

  //拍照結果
  Future<XFile?> takePicture() async {
    final CameraController? cameraController = _cameraController;
    if (cameraController == null || !cameraController.value.isInitialized) {
      log('Error: select a camera first.');
      return null;
    }

    if (cameraController.value.isTakingPicture) {
      // A capture is already pending, do nothing.
      return null;
    }

    try {
      final XFile file = await cameraController.takePicture();
      return file;
    } on CameraException catch (e) {
      log('CameraException ${e.toString()}');
      return null;
    }
  }

  //拍照畫面
  Widget _cameraPreviewWidget() {
    final CameraController? cameraController = _cameraController;
    final size = MediaQuery.of(context).size;
    double? scale;
    if (cameraController != null && cameraController.value.isInitialized) {
      scale = size.aspectRatio * _cameraController!.value.aspectRatio;
      if (scale < 1) scale = 1 / scale;
    }

    if (cameraController == null || !cameraController.value.isInitialized) {
      return Container(
        width: Device.boxConstraints.maxWidth,
        height: Device.boxConstraints.maxHeight,
        color: Colors.black,
      );
    } else {
      cameraController.setFlashMode(FlashMode.off);
      return Transform.scale(
        scale: scale,
        alignment: Alignment.topCenter,
        child: GestureDetector(
          onTapUp: (details) => _onTap(details),
          child: Stack(
            children: [
              CameraPreview(
                _cameraController!,
              ),
              if (_showFocusCircle)
                Positioned(
                  top: y - 20,
                  left: x - 20,
                  child: Container(
                    height: 40,
                    width: 40,
                    decoration: BoxDecoration(
                        shape: BoxShape.rectangle,
                        border: Border.all(color: Colors.white, width: 1.5)),
                  ),
                )
            ],
          ),
        ),
      );
    }
  }
  void _showSearchFaceMember() async {
    log('這地方? ${delegate.pages.last.name}');
    bool? result = await showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return IdentifyFriendsDialog(
            key: _key,
            isGame: false,
          );
        },
        barrierColor: Colors.transparent,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 200),
        transitionBuilder: (context, anim1, anim2, child) {
          return Transform.scale(
            scale: anim1.value,
            child: child,
          );
        });
    if (result != null) {
      _utilApiProvider!.clearFaceImageList();
      //關閉
      delegate.popRoute();
    }
  }

  Future<void> _onTap(TapUpDetails details) async {
    if (_cameraController != null) {
      if (_cameraController!.value.isInitialized) {
        _showFocusCircle = true;
        x = details.localPosition.dx;
        y = details.localPosition.dy;

        double fullWidth = MediaQuery.of(context).size.width;
        double cameraHeight = fullWidth * _cameraController!.value.aspectRatio;

        double xp = x / fullWidth;
        double yp = y / cameraHeight;

        Offset point = Offset(xp, yp);
        log("point : $point");

        // Manually focus
        await _cameraController!.setFocusPoint(point);
        log("594");
        setState(() {
          Future.delayed(const Duration(seconds: 2)).whenComplete(() {
            log("597");
            setState(() {
              _showFocusCircle = false;
            });
          });
        });
      }
    }
  }
}
