// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class S {
  S();

  static S? _current;

  static S get current {
    assert(_current != null,
        'No instance of S was loaded. Try to initialize the S delegate before accessing S.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = S();
      S._current = instance;

      return instance;
    });
  }

  static S of(BuildContext context) {
    final instance = S.maybeOf(context);
    assert(instance != null,
        'No instance of S present in the widget tree. Did you add S.delegate in localizationsDelegates?');
    return instance!;
  }

  static S? maybeOf(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `login`
  String get login {
    return Intl.message(
      'login',
      name: 'login',
      desc: '',
      args: [],
    );
  }

  /// `register`
  String get register {
    return Intl.message(
      'register',
      name: 'register',
      desc: '',
      args: [],
    );
  }

  /// `profile picture`
  String get profile_picture {
    return Intl.message(
      'profile picture',
      name: 'profile_picture',
      desc: '',
      args: [],
    );
  }

  /// `Please enter a nickname`
  String get enter_a_nickname {
    return Intl.message(
      'Please enter a nickname',
      name: 'enter_a_nickname',
      desc: '',
      args: [],
    );
  }

  /// `Please enter the phone number`
  String get enter_a_phone_number {
    return Intl.message(
      'Please enter the phone number',
      name: 'enter_a_phone_number',
      desc: '',
      args: [],
    );
  }

  /// `Please enter password`
  String get enter_password {
    return Intl.message(
      'Please enter password',
      name: 'enter_password',
      desc: '',
      args: [],
    );
  }

  /// `Please enter the password again`
  String get enter_password_again {
    return Intl.message(
      'Please enter the password again',
      name: 'enter_password_again',
      desc: '',
      args: [],
    );
  }

  /// `Please select gender`
  String get select_gender {
    return Intl.message(
      'Please select gender',
      name: 'select_gender',
      desc: '',
      args: [],
    );
  }

  /// `Please select a birthday`
  String get select_a_birthday {
    return Intl.message(
      'Please select a birthday',
      name: 'select_a_birthday',
      desc: '',
      args: [],
    );
  }

  /// `Agree the user agreement`
  String get agreed_terms {
    return Intl.message(
      'Agree the user agreement',
      name: 'agreed_terms',
      desc: '',
      args: [],
    );
  }

  /// `Forgot password`
  String get forgot_password {
    return Intl.message(
      'Forgot password',
      name: 'forgot_password',
      desc: '',
      args: [],
    );
  }

  /// `Enter confirmation code`
  String get enter_confirmation_code {
    return Intl.message(
      'Enter confirmation code',
      name: 'enter_confirmation_code',
      desc: '',
      args: [],
    );
  }

  /// `Send the verification code`
  String get send_verification_code {
    return Intl.message(
      'Send the verification code',
      name: 'send_verification_code',
      desc: '',
      args: [],
    );
  }

  /// `Send`
  String get send {
    return Intl.message(
      'Send',
      name: 'send',
      desc: '',
      args: [],
    );
  }

  /// `friend`
  String get friend {
    return Intl.message(
      'friend',
      name: 'friend',
      desc: '',
      args: [],
    );
  }

  /// `chat`
  String get chat {
    return Intl.message(
      'chat',
      name: 'chat',
      desc: '',
      args: [],
    );
  }

  /// `game`
  String get game {
    return Intl.message(
      'game',
      name: 'game',
      desc: '',
      args: [],
    );
  }

  /// `me`
  String get me {
    return Intl.message(
      'me',
      name: 'me',
      desc: '',
      args: [],
    );
  }

  /// `search`
  String get search {
    return Intl.message(
      'search',
      name: 'search',
      desc: '',
      args: [],
    );
  }

  /// `Add friends`
  String get add_friends {
    return Intl.message(
      'Add friends',
      name: 'add_friends',
      desc: '',
      args: [],
    );
  }

  /// `Scan QRCode`
  String get scan_qr {
    return Intl.message(
      'Scan QRCode',
      name: 'scan_qr',
      desc: '',
      args: [],
    );
  }

  /// `Male`
  String get male {
    return Intl.message(
      'Male',
      name: 'male',
      desc: '',
      args: [],
    );
  }

  /// `Female`
  String get female {
    return Intl.message(
      'Female',
      name: 'female',
      desc: '',
      args: [],
    );
  }

  /// `Photo selection`
  String get photo_selection {
    return Intl.message(
      'Photo selection',
      name: 'photo_selection',
      desc: '',
      args: [],
    );
  }

  /// `Photograph`
  String get photograph {
    return Intl.message(
      'Photograph',
      name: 'photograph',
      desc: '',
      args: [],
    );
  }

  /// `Album`
  String get album {
    return Intl.message(
      'Album',
      name: 'album',
      desc: '',
      args: [],
    );
  }

  /// `Menu`
  String get menu {
    return Intl.message(
      'Menu',
      name: 'menu',
      desc: '',
      args: [],
    );
  }

  /// `IdentificationRecord`
  String get identification_record {
    return Intl.message(
      'IdentificationRecord',
      name: 'identification_record',
      desc: '',
      args: [],
    );
  }

  /// `ColorfulSocial`
  String get colorful_social {
    return Intl.message(
      'ColorfulSocial',
      name: 'colorful_social',
      desc: '',
      args: [],
    );
  }

  /// `Comments and Questions Feedback`
  String get opinion {
    return Intl.message(
      'Comments and Questions Feedback',
      name: 'opinion',
      desc: '',
      args: [],
    );
  }

  /// `Change Password`
  String get change_password {
    return Intl.message(
      'Change Password',
      name: 'change_password',
      desc: '',
      args: [],
    );
  }

  /// `About Ashera`
  String get about_ashera {
    return Intl.message(
      'About Ashera',
      name: 'about_ashera',
      desc: '',
      args: [],
    );
  }

  /// `Sign out`
  String get sign_out {
    return Intl.message(
      'Sign out',
      name: 'sign_out',
      desc: '',
      args: [],
    );
  }

  /// `Bonus`
  String get bonus {
    return Intl.message(
      'Bonus',
      name: 'bonus',
      desc: '',
      args: [],
    );
  }

  /// `Edit profile`
  String get edit_profile {
    return Intl.message(
      'Edit profile',
      name: 'edit_profile',
      desc: '',
      args: [],
    );
  }

  /// `Video`
  String get video {
    return Intl.message(
      'Video',
      name: 'video',
      desc: '',
      args: [],
    );
  }

  /// `Notice`
  String get notice {
    return Intl.message(
      'Notice',
      name: 'notice',
      desc: '',
      args: [],
    );
  }

  /// `confirm`
  String get confirm {
    return Intl.message(
      'confirm',
      name: 'confirm',
      desc: '',
      args: [],
    );
  }

  /// `Please enter old password`
  String get enter_old_password {
    return Intl.message(
      'Please enter old password',
      name: 'enter_old_password',
      desc: '',
      args: [],
    );
  }

  /// `Please enter a new password`
  String get enter_new_password {
    return Intl.message(
      'Please enter a new password',
      name: 'enter_new_password',
      desc: '',
      args: [],
    );
  }

  /// `Please enter new password again`
  String get enter_new_password_again {
    return Intl.message(
      'Please enter new password again',
      name: 'enter_new_password_again',
      desc: '',
      args: [],
    );
  }

  /// `Confirm changes`
  String get confirm_changes {
    return Intl.message(
      'Confirm changes',
      name: 'confirm_changes',
      desc: '',
      args: [],
    );
  }

  /// `Visit`
  String get visit {
    return Intl.message(
      'Visit',
      name: 'visit',
      desc: '',
      args: [],
    );
  }

  /// `Visited`
  String get visited {
    return Intl.message(
      'Visited',
      name: 'visited',
      desc: '',
      args: [],
    );
  }

  /// `Let one thing have \ninfinite possibilities.`
  String get visit_content {
    return Intl.message(
      'Let one thing have \ninfinite possibilities.',
      name: 'visit_content',
      desc: '',
      args: [],
    );
  }

  /// `Peekaboo`
  String get peekaboo {
    return Intl.message(
      'Peekaboo',
      name: 'peekaboo',
      desc: '',
      args: [],
    );
  }

  /// `Fate is no longer \na chance encounter.`
  String get peekaboo_content {
    return Intl.message(
      'Fate is no longer \na chance encounter.',
      name: 'peekaboo_content',
      desc: '',
      args: [],
    );
  }

  /// `About Me`
  String get about_me {
    return Intl.message(
      'About Me',
      name: 'about_me',
      desc: '',
      args: [],
    );
  }

  /// `NickName`
  String get nick_name {
    return Intl.message(
      'NickName',
      name: 'nick_name',
      desc: '',
      args: [],
    );
  }

  /// `Gender`
  String get gender {
    return Intl.message(
      'Gender',
      name: 'gender',
      desc: '',
      args: [],
    );
  }

  /// `Birthday`
  String get birthday {
    return Intl.message(
      'Birthday',
      name: 'birthday',
      desc: '',
      args: [],
    );
  }

  /// `Height`
  String get height {
    return Intl.message(
      'Height',
      name: 'height',
      desc: '',
      args: [],
    );
  }

  /// `Weight`
  String get weight {
    return Intl.message(
      'Weight',
      name: 'weight',
      desc: '',
      args: [],
    );
  }

  /// `Constellation`
  String get constellation {
    return Intl.message(
      'Constellation',
      name: 'constellation',
      desc: '',
      args: [],
    );
  }

  /// `Blood Type`
  String get blood_type {
    return Intl.message(
      'Blood Type',
      name: 'blood_type',
      desc: '',
      args: [],
    );
  }

  /// `Profession`
  String get profession {
    return Intl.message(
      'Profession',
      name: 'profession',
      desc: '',
      args: [],
    );
  }

  /// `Enter Message`
  String get enter_message {
    return Intl.message(
      'Enter Message',
      name: 'enter_message',
      desc: '',
      args: [],
    );
  }

  /// `Select Question Type`
  String get select_question_type {
    return Intl.message(
      'Select Question Type',
      name: 'select_question_type',
      desc: '',
      args: [],
    );
  }

  /// `Question Type`
  String get question_type {
    return Intl.message(
      'Question Type',
      name: 'question_type',
      desc: '',
      args: [],
    );
  }

  /// `Please enter comments and feedback`
  String get opinion_content {
    return Intl.message(
      'Please enter comments and feedback',
      name: 'opinion_content',
      desc: '',
      args: [],
    );
  }

  /// `Not selected`
  String get not_selected {
    return Intl.message(
      'Not selected',
      name: 'not_selected',
      desc: '',
      args: [],
    );
  }

  /// `Save`
  String get save {
    return Intl.message(
      'Save',
      name: 'save',
      desc: '',
      args: [],
    );
  }

  /// `My QR CODE`
  String get my_QR_code {
    return Intl.message(
      'My QR CODE',
      name: 'my_QR_code',
      desc: '',
      args: [],
    );
  }

  /// `Please scan the QR CODE and add me as a friend`
  String get scan_qr_code_add_me {
    return Intl.message(
      'Please scan the QR CODE and add me as a friend',
      name: 'scan_qr_code_add_me',
      desc: '',
      args: [],
    );
  }

  /// `NowStatus: {status, select, none{none} seek{Seek} other{Sought}}`
  String nowPeekabooStatus(Object status) {
    return Intl.message(
      'NowStatus: ${Intl.select(status, {
            'none': 'none',
            'seek': 'Seek',
            'other': 'Sought'
          })}',
      name: 'nowPeekabooStatus',
      desc: '',
      args: [status],
    );
  }

  /// `Today Gotcha`
  String get today_gotcha {
    return Intl.message(
      'Today Gotcha',
      name: 'today_gotcha',
      desc: '',
      args: [],
    );
  }

  /// `cats`
  String get how_many_cat {
    return Intl.message(
      'cats',
      name: 'how_many_cat',
      desc: '',
      args: [],
    );
  }

  /// `Today Be Gotcha`
  String get today_be_gotcha {
    return Intl.message(
      'Today Be Gotcha',
      name: 'today_be_gotcha',
      desc: '',
      args: [],
    );
  }

  /// `times`
  String get how_many_times {
    return Intl.message(
      'times',
      name: 'how_many_times',
      desc: '',
      args: [],
    );
  }

  /// `Record`
  String get record {
    return Intl.message(
      'Record',
      name: 'record',
      desc: '',
      args: [],
    );
  }

  /// `My Points`
  String get my_points {
    return Intl.message(
      'My Points',
      name: 'my_points',
      desc: '',
      args: [],
    );
  }

  /// `Point`
  String get point {
    return Intl.message(
      'Point',
      name: 'point',
      desc: '',
      args: [],
    );
  }

  /// `Recharge`
  String get recharge {
    return Intl.message(
      'Recharge',
      name: 'recharge',
      desc: '',
      args: [],
    );
  }

  /// `Recharge Record`
  String get recharge_record {
    return Intl.message(
      'Recharge Record',
      name: 'recharge_record',
      desc: '',
      args: [],
    );
  }

  /// `Gotcha`
  String get gotcha {
    return Intl.message(
      'Gotcha',
      name: 'gotcha',
      desc: '',
      args: [],
    );
  }

  /// `BeGotcha`
  String get be_gotcha {
    return Intl.message(
      'BeGotcha',
      name: 'be_gotcha',
      desc: '',
      args: [],
    );
  }

  /// `Closing`
  String get closing {
    return Intl.message(
      'Closing',
      name: 'closing',
      desc: '',
      args: [],
    );
  }

  /// `Opening`
  String get opening {
    return Intl.message(
      'Opening',
      name: 'opening',
      desc: '',
      args: [],
    );
  }

  /// `Top-up Point`
  String get top_up_amount {
    return Intl.message(
      'Top-up Point',
      name: 'top_up_amount',
      desc: '',
      args: [],
    );
  }

  /// `Payment Method`
  String get payment_method {
    return Intl.message(
      'Payment Method',
      name: 'payment_method',
      desc: '',
      args: [],
    );
  }

  /// `Payment Amount`
  String get payment_amount {
    return Intl.message(
      'Payment Amount',
      name: 'payment_amount',
      desc: '',
      args: [],
    );
  }

  /// `Yuan`
  String get yuan {
    return Intl.message(
      'Yuan',
      name: 'yuan',
      desc: '',
      args: [],
    );
  }

  /// `Confirm Recharge`
  String get confirm_recharge {
    return Intl.message(
      'Confirm Recharge',
      name: 'confirm_recharge',
      desc: '',
      args: [],
    );
  }

  /// `Self Paid`
  String get self_paid {
    return Intl.message(
      'Self Paid',
      name: 'self_paid',
      desc: '',
      args: [],
    );
  }

  /// `Opposite Paid`
  String get opposite_paid {
    return Intl.message(
      'Opposite Paid',
      name: 'opposite_paid',
      desc: '',
      args: [],
    );
  }

  /// `Needed`
  String get needed {
    return Intl.message(
      'Needed',
      name: 'needed',
      desc: '',
      args: [],
    );
  }

  /// `At Least`
  String get at_least {
    return Intl.message(
      'At Least',
      name: 'at_least',
      desc: '',
      args: [],
    );
  }

  /// `words`
  String get how_many_word {
    return Intl.message(
      'words',
      name: 'how_many_word',
      desc: '',
      args: [],
    );
  }

  /// `Open `
  String get open {
    return Intl.message(
      'Open ',
      name: 'open',
      desc: '',
      args: [],
    );
  }

  /// `Close `
  String get close {
    return Intl.message(
      'Close ',
      name: 'close',
      desc: '',
      args: [],
    );
  }

  /// `Seek`
  String get seek {
    return Intl.message(
      'Seek',
      name: 'seek',
      desc: '',
      args: [],
    );
  }

  /// `Map`
  String get map {
    return Intl.message(
      'Map',
      name: 'map',
      desc: '',
      args: [],
    );
  }

  /// `List`
  String get list {
    return Intl.message(
      'List',
      name: 'list',
      desc: '',
      args: [],
    );
  }

  /// `Distance`
  String get distance {
    return Intl.message(
      'Distance',
      name: 'distance',
      desc: '',
      args: [],
    );
  }

  /// `m`
  String get m {
    return Intl.message(
      'm',
      name: 'm',
      desc: '',
      args: [],
    );
  }

  /// `km`
  String get km {
    return Intl.message(
      'km',
      name: 'km',
      desc: '',
      args: [],
    );
  }

  /// `Unblock`
  String get unblock {
    return Intl.message(
      'Unblock',
      name: 'unblock',
      desc: '',
      args: [],
    );
  }

  /// `Location`
  String get location {
    return Intl.message(
      'Location',
      name: 'location',
      desc: '',
      args: [],
    );
  }

  /// `Mode`
  String get mode {
    return Intl.message(
      'Mode',
      name: 'mode',
      desc: '',
      args: [],
    );
  }

  /// `Paid Mode`
  String get paid_mode {
    return Intl.message(
      'Paid Mode',
      name: 'paid_mode',
      desc: '',
      args: [],
    );
  }

  /// `Transaction Number`
  String get transaction_number {
    return Intl.message(
      'Transaction Number',
      name: 'transaction_number',
      desc: '',
      args: [],
    );
  }

  /// `Transaction Date`
  String get transaction_date {
    return Intl.message(
      'Transaction Date',
      name: 'transaction_date',
      desc: '',
      args: [],
    );
  }

  /// `Amount Of The Transaction`
  String get amount_of_the_transaction {
    return Intl.message(
      'Amount Of The Transaction',
      name: 'amount_of_the_transaction',
      desc: '',
      args: [],
    );
  }

  /// `Points`
  String get record_points {
    return Intl.message(
      'Points',
      name: 'record_points',
      desc: '',
      args: [],
    );
  }

  /// `Count Down`
  String get count_down {
    return Intl.message(
      'Count Down',
      name: 'count_down',
      desc: '',
      args: [],
    );
  }

  /// `Identify Friends`
  String get identify_friends {
    return Intl.message(
      'Identify Friends',
      name: 'identify_friends',
      desc: '',
      args: [],
    );
  }

  /// `About`
  String get about {
    return Intl.message(
      'About',
      name: 'about',
      desc: '',
      args: [],
    );
  }

  /// `minutes`
  String get minute {
    return Intl.message(
      'minutes',
      name: 'minute',
      desc: '',
      args: [],
    );
  }

  /// `hours`
  String get hour {
    return Intl.message(
      'hours',
      name: 'hour',
      desc: '',
      args: [],
    );
  }

  /// `Wait Cancel Visit`
  String get wait_other_side_cancel_visit {
    return Intl.message(
      'Wait Cancel Visit',
      name: 'wait_other_side_cancel_visit',
      desc: '',
      args: [],
    );
  }

  /// `Wait Time`
  String get wait_time {
    return Intl.message(
      'Wait Time',
      name: 'wait_time',
      desc: '',
      args: [],
    );
  }

  /// `Cancel`
  String get cancel {
    return Intl.message(
      'Cancel',
      name: 'cancel',
      desc: '',
      args: [],
    );
  }

  /// `Cancelled`
  String get cancelled {
    return Intl.message(
      'Cancelled',
      name: 'cancelled',
      desc: '',
      args: [],
    );
  }

  /// `Rating`
  String get rating_bar {
    return Intl.message(
      'Rating',
      name: 'rating_bar',
      desc: '',
      args: [],
    );
  }

  /// `Please Enter Your Commit`
  String get please_enter_your_commit {
    return Intl.message(
      'Please Enter Your Commit',
      name: 'please_enter_your_commit',
      desc: '',
      args: [],
    );
  }

  /// `Send Commit`
  String get send_commit {
    return Intl.message(
      'Send Commit',
      name: 'send_commit',
      desc: '',
      args: [],
    );
  }

  /// `Please scan the QR CODE`
  String get scan_the_QR_CODE {
    return Intl.message(
      'Please scan the QR CODE',
      name: 'scan_the_QR_CODE',
      desc: '',
      args: [],
    );
  }

  /// `Show mobile barcode`
  String get show_mobile_barcode {
    return Intl.message(
      'Show mobile barcode',
      name: 'show_mobile_barcode',
      desc: '',
      args: [],
    );
  }

  /// `Please scan your face for identification`
  String get scan_your_face_for_identification {
    return Intl.message(
      'Please scan your face for identification',
      name: 'scan_your_face_for_identification',
      desc: '',
      args: [],
    );
  }

  /// `Message`
  String get message {
    return Intl.message(
      'Message',
      name: 'message',
      desc: '',
      args: [],
    );
  }

  /// `Are You Sure `
  String get are_you_sure {
    return Intl.message(
      'Are You Sure ',
      name: 'are_you_sure',
      desc: '',
      args: [],
    );
  }

  /// `Already`
  String get already {
    return Intl.message(
      'Already',
      name: 'already',
      desc: '',
      args: [],
    );
  }

  /// `Get Face In This Week For`
  String get get_face_in_this_week {
    return Intl.message(
      'Get Face In This Week For',
      name: 'get_face_in_this_week',
      desc: '',
      args: [],
    );
  }

  /// `Recognition failed, please select a friend to add`
  String get recognition_failed {
    return Intl.message(
      'Recognition failed, please select a friend to add',
      name: 'recognition_failed',
      desc: '',
      args: [],
    );
  }

  /// `Identify Percent`
  String get identify_percent {
    return Intl.message(
      'Identify Percent',
      name: 'identify_percent',
      desc: '',
      args: [],
    );
  }

  /// `Smile Percent`
  String get smile_percent {
    return Intl.message(
      'Smile Percent',
      name: 'smile_percent',
      desc: '',
      args: [],
    );
  }

  /// `Time UP ! Do You Want Make Friend?`
  String get identify_time_over_content {
    return Intl.message(
      'Time UP ! Do You Want Make Friend?',
      name: 'identify_time_over_content',
      desc: '',
      args: [],
    );
  }

  /// `Please Enter Ashera ID`
  String get please_enter_ashera_id {
    return Intl.message(
      'Please Enter Ashera ID',
      name: 'please_enter_ashera_id',
      desc: '',
      args: [],
    );
  }

  /// `Waiting Add Friends`
  String get waiting_add_friends {
    return Intl.message(
      'Waiting Add Friends',
      name: 'waiting_add_friends',
      desc: '',
      args: [],
    );
  }

  /// `Waiting Add Friends Confirm`
  String get waiting_add_friends_confirm {
    return Intl.message(
      'Waiting Add Friends Confirm',
      name: 'waiting_add_friends_confirm',
      desc: '',
      args: [],
    );
  }

  /// `Unblock Mugshot ?`
  String get unblock_member_mugshot {
    return Intl.message(
      'Unblock Mugshot ?',
      name: 'unblock_member_mugshot',
      desc: '',
      args: [],
    );
  }

  /// `Apply Finish\nWaiting Accept...`
  String get apply_finish_waiting_accept {
    return Intl.message(
      'Apply Finish\nWaiting Accept...',
      name: 'apply_finish_waiting_accept',
      desc: '',
      args: [],
    );
  }

  /// `Finish Visit`
  String get finish_visit {
    return Intl.message(
      'Finish Visit',
      name: 'finish_visit',
      desc: '',
      args: [],
    );
  }

  /// `Cancel Visit`
  String get cancel_visit {
    return Intl.message(
      'Cancel Visit',
      name: 'cancel_visit',
      desc: '',
      args: [],
    );
  }

  /// `Cancel The Visit`
  String get cancel_the_visit {
    return Intl.message(
      'Cancel The Visit',
      name: 'cancel_the_visit',
      desc: '',
      args: [],
    );
  }

  /// `No Headshots Taken`
  String get no_headshots_taken {
    return Intl.message(
      'No Headshots Taken',
      name: 'no_headshots_taken',
      desc: '',
      args: [],
    );
  }

  /// `Please enter 6~12 alphanumeric characters`
  String get enter_alphanumeric_characters {
    return Intl.message(
      'Please enter 6~12 alphanumeric characters',
      name: 'enter_alphanumeric_characters',
      desc: '',
      args: [],
    );
  }

  /// `Mobile number entered incorrectly`
  String get mobile_number_entered_incorrectly {
    return Intl.message(
      'Mobile number entered incorrectly',
      name: 'mobile_number_entered_incorrectly',
      desc: '',
      args: [],
    );
  }

  /// `Information is incomplete`
  String get information_is_incomplete {
    return Intl.message(
      'Information is incomplete',
      name: 'information_is_incomplete',
      desc: '',
      args: [],
    );
  }

  /// `Registration Success!`
  String get registration_success {
    return Intl.message(
      'Registration Success!',
      name: 'registration_success',
      desc: '',
      args: [],
    );
  }

  /// `Login Success!`
  String get login_success {
    return Intl.message(
      'Login Success!',
      name: 'login_success',
      desc: '',
      args: [],
    );
  }

  /// `Refuse`
  String get refuse {
    return Intl.message(
      'Refuse',
      name: 'refuse',
      desc: '',
      args: [],
    );
  }

  /// `Different Passwords Entered Twice`
  String get different_passwords_entered_twice {
    return Intl.message(
      'Different Passwords Entered Twice',
      name: 'different_passwords_entered_twice',
      desc: '',
      args: [],
    );
  }

  /// `Friend Invitation`
  String get friend_invitation {
    return Intl.message(
      'Friend Invitation',
      name: 'friend_invitation',
      desc: '',
      args: [],
    );
  }

  /// `Registering`
  String get registering {
    return Intl.message(
      'Registering',
      name: 'registering',
      desc: '',
      args: [],
    );
  }

  /// `Please agree to the User Agreement`
  String get agree_to_the_user_agreement {
    return Intl.message(
      'Please agree to the User Agreement',
      name: 'agree_to_the_user_agreement',
      desc: '',
      args: [],
    );
  }

  /// `Updating Information`
  String get updating_information {
    return Intl.message(
      'Updating Information',
      name: 'updating_information',
      desc: '',
      args: [],
    );
  }

  /// `Finish`
  String get finish {
    return Intl.message(
      'Finish',
      name: 'finish',
      desc: '',
      args: [],
    );
  }

  /// `Fail`
  String get fail {
    return Intl.message(
      'Fail',
      name: 'fail',
      desc: '',
      args: [],
    );
  }

  /// `{string, select, aries{Aries} taurus{Taurus} gemini{Gemini} cancer{Cancer} leo{Leo} virgo{Virgo} libra{Libra} scorpio{Scorpio} sagittarius{Sagittarius} capricorn{Capricorn} aquarius{Aquarius} pisces{Pisces} other{none}}`
  String horoscope(Object string) {
    return Intl.select(
      string,
      {
        'aries': 'Aries',
        'taurus': 'Taurus',
        'gemini': 'Gemini',
        'cancer': 'Cancer',
        'leo': 'Leo',
        'virgo': 'Virgo',
        'libra': 'Libra',
        'scorpio': 'Scorpio',
        'sagittarius': 'Sagittarius',
        'capricorn': 'Capricorn',
        'aquarius': 'Aquarius',
        'pisces': 'Pisces',
        'other': 'none',
      },
      name: 'horoscope',
      desc: '',
      args: [string],
    );
  }

  /// `Friendship invitation has been sent`
  String get friendship_invitation_has_been_sent {
    return Intl.message(
      'Friendship invitation has been sent',
      name: 'friendship_invitation_has_been_sent',
      desc: '',
      args: [],
    );
  }

  /// `User Agreement`
  String get user_agreement {
    return Intl.message(
      'User Agreement',
      name: 'user_agreement',
      desc: '',
      args: [],
    );
  }

  /// `uploading...`
  String get uploading {
    return Intl.message(
      'uploading...',
      name: 'uploading',
      desc: '',
      args: [],
    );
  }

  /// `Add Friend`
  String get add_friend {
    return Intl.message(
      'Add Friend',
      name: 'add_friend',
      desc: '',
      args: [],
    );
  }

  /// `Add`
  String get add {
    return Intl.message(
      'Add',
      name: 'add',
      desc: '',
      args: [],
    );
  }

  /// `Word's length can't bigger than`
  String get wordLimit {
    return Intl.message(
      'Word\'s length can\'t bigger than',
      name: 'wordLimit',
      desc: '',
      args: [],
    );
  }

  /// `Do not Search for your own UID`
  String get do_not_search_for_your_own_UID {
    return Intl.message(
      'Do not Search for your own UID',
      name: 'do_not_search_for_your_own_UID',
      desc: '',
      args: [],
    );
  }

  /// `Added friend`
  String get added_friend {
    return Intl.message(
      'Added friend',
      name: 'added_friend',
      desc: '',
      args: [],
    );
  }

  /// `sent a picture`
  String get sent_a_picture {
    return Intl.message(
      'sent a picture',
      name: 'sent_a_picture',
      desc: '',
      args: [],
    );
  }

  /// `sent a video`
  String get sent_a_video {
    return Intl.message(
      'sent a video',
      name: 'sent_a_video',
      desc: '',
      args: [],
    );
  }

  /// `Duplicate registered`
  String get duplicate_registered {
    return Intl.message(
      'Duplicate registered',
      name: 'duplicate_registered',
      desc: '',
      args: [],
    );
  }

  /// `Face recognition`
  String get face_recognition {
    return Intl.message(
      'Face recognition',
      name: 'face_recognition',
      desc: '',
      args: [],
    );
  }

  /// `Verified`
  String get verified {
    return Intl.message(
      'Verified',
      name: 'verified',
      desc: '',
      args: [],
    );
  }

  /// `Unverified`
  String get unverified {
    return Intl.message(
      'Unverified',
      name: 'unverified',
      desc: '',
      args: [],
    );
  }

  /// `Please upload one`
  String get please_upload_one {
    return Intl.message(
      'Please upload one',
      name: 'please_upload_one',
      desc: '',
      args: [],
    );
  }

  /// `Clear facial features, frontal photo`
  String get clear_facial_features_frontal_photo {
    return Intl.message(
      'Clear facial features, frontal photo',
      name: 'clear_facial_features_frontal_photo',
      desc: '',
      args: [],
    );
  }

  /// `For face recognition`
  String get for_face_recognition {
    return Intl.message(
      'For face recognition',
      name: 'for_face_recognition',
      desc: '',
      args: [],
    );
  }

  /// `※If the face recognition photo needs to be changed after the review is successful,\nPlease contact customer service`
  String get face_recognition_warning {
    return Intl.message(
      '※If the face recognition photo needs to be changed after the review is successful,\nPlease contact customer service',
      name: 'face_recognition_warning',
      desc: '',
      args: [],
    );
  }

  /// `Confirm upload`
  String get confirm_upload {
    return Intl.message(
      'Confirm upload',
      name: 'confirm_upload',
      desc: '',
      args: [],
    );
  }

  /// `Face recognition has not been verified, \nplease go to personal settings to verify`
  String get not_yet_verified {
    return Intl.message(
      'Face recognition has not been verified, \nplease go to personal settings to verify',
      name: 'not_yet_verified',
      desc: '',
      args: [],
    );
  }

  /// `Verifying....`
  String get verifying {
    return Intl.message(
      'Verifying....',
      name: 'verifying',
      desc: '',
      args: [],
    );
  }

  /// `Please select an identifying photo`
  String get please_select_an_identifying_photo {
    return Intl.message(
      'Please select an identifying photo',
      name: 'please_select_an_identifying_photo',
      desc: '',
      args: [],
    );
  }

  /// `Failed! Please change the identification avatar`
  String get failed_to_upload_again {
    return Intl.message(
      'Failed! Please change the identification avatar',
      name: 'failed_to_upload_again',
      desc: '',
      args: [],
    );
  }

  /// `uploading comparison`
  String get uploading_comparison {
    return Intl.message(
      'uploading comparison',
      name: 'uploading_comparison',
      desc: '',
      args: [],
    );
  }

  /// `photo comparison`
  String get photo_comparison {
    return Intl.message(
      'photo comparison',
      name: 'photo_comparison',
      desc: '',
      args: [],
    );
  }

  /// `No profile for this person`
  String get no_profile_for_this_person {
    return Intl.message(
      'No profile for this person',
      name: 'no_profile_for_this_person',
      desc: '',
      args: [],
    );
  }

  /// `Registration is complete Face recognition has not been completed`
  String get registration_is_complete_face_recognition_has_not_been_completed {
    return Intl.message(
      'Registration is complete Face recognition has not been completed',
      name: 'registration_is_complete_face_recognition_has_not_been_completed',
      desc: '',
      args: [],
    );
  }

  /// `Please log in to the app to complete the verification`
  String get please_log_in_to_the_app_to_complete_the_verification {
    return Intl.message(
      'Please log in to the app to complete the verification',
      name: 'please_log_in_to_the_app_to_complete_the_verification',
      desc: '',
      args: [],
    );
  }

  /// `Visiting class notice`
  String get visiting_class_notice {
    return Intl.message(
      'Visiting class notice',
      name: 'visiting_class_notice',
      desc: '',
      args: [],
    );
  }

  /// `received an invitation to visit`
  String get received_an_invitation_to_visit {
    return Intl.message(
      'received an invitation to visit',
      name: 'received_an_invitation_to_visit',
      desc: '',
      args: [],
    );
  }

  /// `agree`
  String get agree {
    return Intl.message(
      'agree',
      name: 'agree',
      desc: '',
      args: [],
    );
  }

  /// `disagree`
  String get disagree {
    return Intl.message(
      'disagree',
      name: 'disagree',
      desc: '',
      args: [],
    );
  }

  /// `Those who have been visited have not completed the class, and they are not allowed to visit the class.`
  String get those_who_have_been_visited_have_not_completed_the_class {
    return Intl.message(
      'Those who have been visited have not completed the class, and they are not allowed to visit the class.',
      name: 'those_who_have_been_visited_have_not_completed_the_class',
      desc: '',
      args: [],
    );
  }

  /// `time left`
  String get time_left {
    return Intl.message(
      'time left',
      name: 'time_left',
      desc: '',
      args: [],
    );
  }

  /// `Sure`
  String get sure {
    return Intl.message(
      'Sure',
      name: 'sure',
      desc: '',
      args: [],
    );
  }

  /// `Do you agree with the other party to cancel the visit?`
  String get do_you_agree_with_the_other_party_to_cancel_the_visit {
    return Intl.message(
      'Do you agree with the other party to cancel the visit?',
      name: 'do_you_agree_with_the_other_party_to_cancel_the_visit',
      desc: '',
      args: [],
    );
  }

  /// `Reviewed`
  String get reviewed {
    return Intl.message(
      'Reviewed',
      name: 'reviewed',
      desc: '',
      args: [],
    );
  }

  /// `The visit has been completed`
  String get the_visit_has_been_completed {
    return Intl.message(
      'The visit has been completed',
      name: 'the_visit_has_been_completed',
      desc: '',
      args: [],
    );
  }

  /// `The other party does not agree to your visit`
  String get the_other_party_does_not_agree_to_your_visit {
    return Intl.message(
      'The other party does not agree to your visit',
      name: 'the_other_party_does_not_agree_to_your_visit',
      desc: '',
      args: [],
    );
  }

  /// `closing time not yet`
  String get closing_time_not_yet {
    return Intl.message(
      'closing time not yet',
      name: 'closing_time_not_yet',
      desc: '',
      args: [],
    );
  }

  /// `It's not time to switch`
  String get It_s_not_time_to_switch {
    return Intl.message(
      'It\'s not time to switch',
      name: 'It_s_not_time_to_switch',
      desc: '',
      args: [],
    );
  }

  /// `capture completed`
  String get capture_completed {
    return Intl.message(
      'capture completed',
      name: 'capture_completed',
      desc: '',
      args: [],
    );
  }

  /// `you have been caught`
  String get you_have_been_caught {
    return Intl.message(
      'you have been caught',
      name: 'you_have_been_caught',
      desc: '',
      args: [],
    );
  }

  /// `Sales record`
  String get sales_record {
    return Intl.message(
      'Sales record',
      name: 'sales_record',
      desc: '',
      args: [],
    );
  }

  /// `Upgrade VIP Membership`
  String get upgrade_VIP_membership {
    return Intl.message(
      'Upgrade VIP Membership',
      name: 'upgrade_VIP_membership',
      desc: '',
      args: [],
    );
  }

  /// `hunger`
  String get hunger {
    return Intl.message(
      'hunger',
      name: 'hunger',
      desc: '',
      args: [],
    );
  }

  /// `Please close gotcha status first`
  String get please_close_status {
    return Intl.message(
      'Please close gotcha status first',
      name: 'please_close_status',
      desc: '',
      args: [],
    );
  }

  /// `{string, select, childhood{Childhood} growingUp{GrowingUp} Mature{Mature} die{Die} other{none}}`
  String growingPhase(Object string) {
    return Intl.select(
      string,
      {
        'childhood': 'Childhood',
        'growingUp': 'GrowingUp',
        'Mature': 'Mature',
        'die': 'Die',
        'other': 'none',
      },
      name: 'growingPhase',
      desc: '',
      args: [string],
    );
  }

  /// `sell`
  String get sell {
    return Intl.message(
      'sell',
      name: 'sell',
      desc: '',
      args: [],
    );
  }

  /// `feeding`
  String get feeding {
    return Intl.message(
      'feeding',
      name: 'feeding',
      desc: '',
      args: [],
    );
  }

  /// `Leave Message`
  String get leave_message {
    return Intl.message(
      'Leave Message',
      name: 'leave_message',
      desc: '',
      args: [],
    );
  }

  /// `I Know`
  String get i_know {
    return Intl.message(
      'I Know',
      name: 'i_know',
      desc: '',
      args: [],
    );
  }

  /// `Setting`
  String get setting {
    return Intl.message(
      'Setting',
      name: 'setting',
      desc: '',
      args: [],
    );
  }

  /// `Screening Condition`
  String get screening_condition {
    return Intl.message(
      'Screening Condition',
      name: 'screening_condition',
      desc: '',
      args: [],
    );
  }

  /// `filter`
  String get filter {
    return Intl.message(
      'filter',
      name: 'filter',
      desc: '',
      args: [],
    );
  }

  /// `none`
  String get none_gender {
    return Intl.message(
      'none',
      name: 'none_gender',
      desc: '',
      args: [],
    );
  }

  /// `Age`
  String get age {
    return Intl.message(
      'Age',
      name: 'age',
      desc: '',
      args: [],
    );
  }

  /// `need to re-authenticate`
  String get need_to_re_authenticate {
    return Intl.message(
      'need to re-authenticate',
      name: 'need_to_re_authenticate',
      desc: '',
      args: [],
    );
  }

  /// `Wallpaper`
  String get wallpaper {
    return Intl.message(
      'Wallpaper',
      name: 'wallpaper',
      desc: '',
      args: [],
    );
  }

  /// `Dog`
  String get dog {
    return Intl.message(
      'Dog',
      name: 'dog',
      desc: '',
      args: [],
    );
  }

  /// `Cat`
  String get cat {
    return Intl.message(
      'Cat',
      name: 'cat',
      desc: '',
      args: [],
    );
  }

  /// `Fence`
  String get fence {
    return Intl.message(
      'Fence',
      name: 'fence',
      desc: '',
      args: [],
    );
  }

  /// `Cabinet`
  String get cabinet {
    return Intl.message(
      'Cabinet',
      name: 'cabinet',
      desc: '',
      args: [],
    );
  }

  /// `Chair`
  String get chair {
    return Intl.message(
      'Chair',
      name: 'chair',
      desc: '',
      args: [],
    );
  }

  /// `Bed`
  String get bed {
    return Intl.message(
      'Bed',
      name: 'bed',
      desc: '',
      args: [],
    );
  }

  /// `Table`
  String get table {
    return Intl.message(
      'Table',
      name: 'table',
      desc: '',
      args: [],
    );
  }

  /// `House`
  String get house {
    return Intl.message(
      'House',
      name: 'house',
      desc: '',
      args: [],
    );
  }

  /// `Doghouse`
  String get doghouse {
    return Intl.message(
      'Doghouse',
      name: 'doghouse',
      desc: '',
      args: [],
    );
  }

  /// `Mat`
  String get mat {
    return Intl.message(
      'Mat',
      name: 'mat',
      desc: '',
      args: [],
    );
  }

  /// `Waiting`
  String get waiting {
    return Intl.message(
      'Waiting',
      name: 'waiting',
      desc: '',
      args: [],
    );
  }

  /// `Upload file too large`
  String get upload_file_too_large {
    return Intl.message(
      'Upload file too large',
      name: 'upload_file_too_large',
      desc: '',
      args: [],
    );
  }

  /// `Unblock Need {value} Point`
  String unblockNeedPoint(Object value) {
    return Intl.message(
      'Unblock Need $value Point',
      name: 'unblockNeedPoint',
      desc: '',
      args: [value],
    );
  }

  /// `One Week Be GotCha`
  String get oneWeekBeGotChaTime {
    return Intl.message(
      'One Week Be GotCha',
      name: 'oneWeekBeGotChaTime',
      desc: '',
      args: [],
    );
  }

  /// `timed out`
  String get timed_out {
    return Intl.message(
      'timed out',
      name: 'timed_out',
      desc: '',
      args: [],
    );
  }

  /// `review`
  String get review {
    return Intl.message(
      'review',
      name: 'review',
      desc: '',
      args: [],
    );
  }

  /// `VIP Member`
  String get vip_member {
    return Intl.message(
      'VIP Member',
      name: 'vip_member',
      desc: '',
      args: [],
    );
  }

  /// `At Large Membership`
  String get at_large_membership {
    return Intl.message(
      'At Large Membership',
      name: 'at_large_membership',
      desc: '',
      args: [],
    );
  }

  /// `current look`
  String get current_look {
    return Intl.message(
      'current look',
      name: 'current_look',
      desc: '',
      args: [],
    );
  }

  /// `again`
  String get again {
    return Intl.message(
      'again',
      name: 'again',
      desc: '',
      args: [],
    );
  }

  /// `Ok to Use`
  String get ok_to_use {
    return Intl.message(
      'Ok to Use',
      name: 'ok_to_use',
      desc: '',
      args: [],
    );
  }

  /// `Buy`
  String get buy {
    return Intl.message(
      'Buy',
      name: 'buy',
      desc: '',
      args: [],
    );
  }

  /// `go to Premium`
  String get go_to_premium {
    return Intl.message(
      'go to Premium',
      name: 'go_to_premium',
      desc: '',
      args: [],
    );
  }

  /// `Insufficient points, please add value`
  String get please_add_value {
    return Intl.message(
      'Insufficient points, please add value',
      name: 'please_add_value',
      desc: '',
      args: [],
    );
  }

  /// `Order is being Created...`
  String get order_is_being_created {
    return Intl.message(
      'Order is being Created...',
      name: 'order_is_being_created',
      desc: '',
      args: [],
    );
  }

  /// `Recharge failed`
  String get recharge_failed {
    return Intl.message(
      'Recharge failed',
      name: 'recharge_failed',
      desc: '',
      args: [],
    );
  }

  /// `Recharge failed, please recharge`
  String get recharge_failed_please_recharge {
    return Intl.message(
      'Recharge failed, please recharge',
      name: 'recharge_failed_please_recharge',
      desc: '',
      args: [],
    );
  }

  /// `successful recharge`
  String get successful_recharge {
    return Intl.message(
      'successful recharge',
      name: 'successful_recharge',
      desc: '',
      args: [],
    );
  }

  /// `{string, select, CREDIT_CARD{creditCard} GOOGLE_IAP{googleIap} APPLE_IAP{appleIap} other{creditCard}}`
  String paymentType(Object string) {
    return Intl.select(
      string,
      {
        'CREDIT_CARD': 'creditCard',
        'GOOGLE_IAP': 'googleIap',
        'APPLE_IAP': 'appleIap',
        'other': 'creditCard',
      },
      name: 'paymentType',
      desc: '',
      args: [string],
    );
  }

  /// `Saw mine`
  String get saw_mine {
    return Intl.message(
      'Saw mine',
      name: 'saw_mine',
      desc: '',
      args: [],
    );
  }

  /// `I have seen`
  String get i_have_seen {
    return Intl.message(
      'I have seen',
      name: 'i_have_seen',
      desc: '',
      args: [],
    );
  }

  /// `Change friend name`
  String get edit_remarks {
    return Intl.message(
      'Change friend name',
      name: 'edit_remarks',
      desc: '',
      args: [],
    );
  }

  /// `Does it cost`
  String get does_it_cost {
    return Intl.message(
      'Does it cost',
      name: 'does_it_cost',
      desc: '',
      args: [],
    );
  }

  /// `Upgrade VIP`
  String get upgrade_vip {
    return Intl.message(
      'Upgrade VIP',
      name: 'upgrade_vip',
      desc: '',
      args: [],
    );
  }

  /// `bought`
  String get bought {
    return Intl.message(
      'bought',
      name: 'bought',
      desc: '',
      args: [],
    );
  }

  /// `interactive`
  String get interactive {
    return Intl.message(
      'interactive',
      name: 'interactive',
      desc: '',
      args: [],
    );
  }

  /// `change Ashera app icon`
  String get change_icon {
    return Intl.message(
      'change Ashera app icon',
      name: 'change_icon',
      desc: '',
      args: [],
    );
  }

  /// `hide`
  String get hide {
    return Intl.message(
      'hide',
      name: 'hide',
      desc: '',
      args: [],
    );
  }

  /// `secret`
  String get secret {
    return Intl.message(
      'secret',
      name: 'secret',
      desc: '',
      args: [],
    );
  }

  /// `delete`
  String get delete {
    return Intl.message(
      'delete',
      name: 'delete',
      desc: '',
      args: [],
    );
  }

  /// `blockade`
  String get blockade {
    return Intl.message(
      'blockade',
      name: 'blockade',
      desc: '',
      args: [],
    );
  }

  /// `Not purchased and cannot be replaced`
  String get cannot_be_replaced {
    return Intl.message(
      'Not purchased and cannot be replaced',
      name: 'cannot_be_replaced',
      desc: '',
      args: [],
    );
  }

  /// `Block list`
  String get block_list {
    return Intl.message(
      'Block list',
      name: 'block_list',
      desc: '',
      args: [],
    );
  }

  /// `lift`
  String get lift {
    return Intl.message(
      'lift',
      name: 'lift',
      desc: '',
      args: [],
    );
  }

  /// `Ambiguous, starting from hurting each other`
  String get game_content {
    return Intl.message(
      'Ambiguous, starting from hurting each other',
      name: 'game_content',
      desc: '',
      args: [],
    );
  }

  /// `frequency`
  String get frequency {
    return Intl.message(
      'frequency',
      name: 'frequency',
      desc: '',
      args: [],
    );
  }

  /// `clean up`
  String get clean_up {
    return Intl.message(
      'clean up',
      name: 'clean_up',
      desc: '',
      args: [],
    );
  }

  /// `poop`
  String get poop {
    return Intl.message(
      'poop',
      name: 'poop',
      desc: '',
      args: [],
    );
  }

  /// `The room is being arranged`
  String get the_room_is_being_arranged {
    return Intl.message(
      'The room is being arranged',
      name: 'the_room_is_being_arranged',
      desc: '',
      args: [],
    );
  }

  /// `Purchasing...`
  String get purchasing {
    return Intl.message(
      'Purchasing...',
      name: 'purchasing',
      desc: '',
      args: [],
    );
  }

  /// `Please enter a message, at least 8 characters`
  String get enter_a_message {
    return Intl.message(
      'Please enter a message, at least 8 characters',
      name: 'enter_a_message',
      desc: '',
      args: [],
    );
  }

  /// `leave comments`
  String get leave_comments {
    return Intl.message(
      'leave comments',
      name: 'leave_comments',
      desc: '',
      args: [],
    );
  }

  /// `Please buy this poop first`
  String get buy_this_poop_first {
    return Intl.message(
      'Please buy this poop first',
      name: 'buy_this_poop_first',
      desc: '',
      args: [],
    );
  }

  /// `feeding...`
  String get pig_eating {
    return Intl.message(
      'feeding...',
      name: 'pig_eating',
      desc: '',
      args: [],
    );
  }

  /// `start the game`
  String get start_the_game {
    return Intl.message(
      'start the game',
      name: 'start_the_game',
      desc: '',
      args: [],
    );
  }

  /// `default`
  String get defaultDung {
    return Intl.message(
      'default',
      name: 'defaultDung',
      desc: '',
      args: [],
    );
  }

  /// `This item is preset and cannot be purchased`
  String get impossible_to_buy {
    return Intl.message(
      'This item is preset and cannot be purchased',
      name: 'impossible_to_buy',
      desc: '',
      args: [],
    );
  }

  /// `is read all message`
  String get is_read_all_message {
    return Intl.message(
      'is read all message',
      name: 'is_read_all_message',
      desc: '',
      args: [],
    );
  }

  /// `serial number`
  String get serial_number {
    return Intl.message(
      'serial number',
      name: 'serial_number',
      desc: '',
      args: [],
    );
  }

  /// `growth time`
  String get growth_time {
    return Intl.message(
      'growth time',
      name: 'growth_time',
      desc: '',
      args: [],
    );
  }

  /// `deduct points`
  String get deduct_points {
    return Intl.message(
      'deduct points',
      name: 'deduct_points',
      desc: '',
      args: [],
    );
  }

  /// `point of sale`
  String get point_of_sale {
    return Intl.message(
      'point of sale',
      name: 'point_of_sale',
      desc: '',
      args: [],
    );
  }

  /// `account deletion`
  String get logout {
    return Intl.message(
      'account deletion',
      name: 'logout',
      desc: '',
      args: [],
    );
  }

  /// `Not Enough Points`
  String get not_enough_points {
    return Intl.message(
      'Not Enough Points',
      name: 'not_enough_points',
      desc: '',
      args: [],
    );
  }

  /// `report`
  String get report {
    return Intl.message(
      'report',
      name: 'report',
      desc: '',
      args: [],
    );
  }

  /// `Phone verification`
  String get phone_verification {
    return Intl.message(
      'Phone verification',
      name: 'phone_verification',
      desc: '',
      args: [],
    );
  }

  /// `If you need to change, please go to the previous page`
  String get need_to_change {
    return Intl.message(
      'If you need to change, please go to the previous page',
      name: 'need_to_change',
      desc: '',
      args: [],
    );
  }

  /// `Next step`
  String get next_step {
    return Intl.message(
      'Next step',
      name: 'next_step',
      desc: '',
      args: [],
    );
  }

  /// `skip`
  String get skip {
    return Intl.message(
      'skip',
      name: 'skip',
      desc: '',
      args: [],
    );
  }

  /// `To allow you to experience a more complete Ashera service, you need to complete the login of face recognition.`
  String get need_to_complete_the_login {
    return Intl.message(
      'To allow you to experience a more complete Ashera service, you need to complete the login of face recognition.',
      name: 'need_to_complete_the_login',
      desc: '',
      args: [],
    );
  }

  /// `Please click the circle for face recognition`
  String get click_the_circle_for_face_recognition {
    return Intl.message(
      'Please click the circle for face recognition',
      name: 'click_the_circle_for_face_recognition',
      desc: '',
      args: [],
    );
  }

  /// `Certification`
  String get certification {
    return Intl.message(
      'Certification',
      name: 'certification',
      desc: '',
      args: [],
    );
  }

  /// `Interactive Photo Authentication`
  String get interactive_photo_authentication {
    return Intl.message(
      'Interactive Photo Authentication',
      name: 'interactive_photo_authentication',
      desc: '',
      args: [],
    );
  }

  /// `Interactive-only photos not uploaded`
  String get interactive_only_photos_not_uploaded {
    return Intl.message(
      'Interactive-only photos not uploaded',
      name: 'interactive_only_photos_not_uploaded',
      desc: '',
      args: [],
    );
  }

  /// `hunt list`
  String get hunt_list {
    return Intl.message(
      'hunt list',
      name: 'hunt_list',
      desc: '',
      args: [],
    );
  }

  /// `Not yet open time`
  String get not_yet_open_time {
    return Intl.message(
      'Not yet open time',
      name: 'not_yet_open_time',
      desc: '',
      args: [],
    );
  }

  /// `release requirements`
  String get release_requirements {
    return Intl.message(
      'release requirements',
      name: 'release_requirements',
      desc: '',
      args: [],
    );
  }

  /// `You can post your requirements here`
  String get you_can_post_your_requirements_here {
    return Intl.message(
      'You can post your requirements here',
      name: 'you_can_post_your_requirements_here',
      desc: '',
      args: [],
    );
  }

  /// `address`
  String get address {
    return Intl.message(
      'address',
      name: 'address',
      desc: '',
      args: [],
    );
  }

  /// `Please enter address`
  String get please_enter_address {
    return Intl.message(
      'Please enter address',
      name: 'please_enter_address',
      desc: '',
      args: [],
    );
  }

  /// `star rating`
  String get star_rating {
    return Intl.message(
      'star rating',
      name: 'star_rating',
      desc: '',
      args: [],
    );
  }

  /// `content`
  String get content {
    return Intl.message(
      'content',
      name: 'content',
      desc: '',
      args: [],
    );
  }

  /// `You have had a good time at this member's house`
  String get you_have_had_a_good_time_at_this_member_house {
    return Intl.message(
      'You have had a good time at this member\'s house',
      name: 'you_have_had_a_good_time_at_this_member_house',
      desc: '',
      args: [],
    );
  }

  /// `App Update`
  String get app_update {
    return Intl.message(
      'App Update',
      name: 'app_update',
      desc: '',
      args: [],
    );
  }

  /// `There is a new version`
  String get new_version {
    return Intl.message(
      'There is a new version',
      name: 'new_version',
      desc: '',
      args: [],
    );
  }

  /// `renew`
  String get renew {
    return Intl.message(
      'renew',
      name: 'renew',
      desc: '',
      args: [],
    );
  }

  /// `Not enough cleaning`
  String get not_enough_cleaning {
    return Intl.message(
      'Not enough cleaning',
      name: 'not_enough_cleaning',
      desc: '',
      args: [],
    );
  }

  /// `Not enough shit`
  String get not_enough_shit {
    return Intl.message(
      'Not enough shit',
      name: 'not_enough_shit',
      desc: '',
      args: [],
    );
  }

  /// `Please enter occupation`
  String get enter_a_profession {
    return Intl.message(
      'Please enter occupation',
      name: 'enter_a_profession',
      desc: '',
      args: [],
    );
  }

  /// `The opponent has not rated yet`
  String get the_opponent_has_not_rated_yet {
    return Intl.message(
      'The opponent has not rated yet',
      name: 'the_opponent_has_not_rated_yet',
      desc: '',
      args: [],
    );
  }

  /// `Not rated yet`
  String get not_rated_yet {
    return Intl.message(
      'Not rated yet',
      name: 'not_rated_yet',
      desc: '',
      args: [],
    );
  }

  /// `member profile`
  String get member_profile {
    return Intl.message(
      'member profile',
      name: 'member_profile',
      desc: '',
      args: [],
    );
  }

  /// `payment status`
  String get payment_status {
    return Intl.message(
      'payment status',
      name: 'payment_status',
      desc: '',
      args: [],
    );
  }

  /// `currently have no friends`
  String get currently_have_no_friends {
    return Intl.message(
      'currently have no friends',
      name: 'currently_have_no_friends',
      desc: '',
      args: [],
    );
  }

  /// `There are currently no records`
  String get there_are_currently_no_records {
    return Intl.message(
      'There are currently no records',
      name: 'there_are_currently_no_records',
      desc: '',
      args: [],
    );
  }

  /// `There is currently no chat history`
  String get there_is_currently_no_chat_history {
    return Intl.message(
      'There is currently no chat history',
      name: 'there_is_currently_no_chat_history',
      desc: '',
      args: [],
    );
  }

  /// `no friends to add`
  String get no_friends_to_add {
    return Intl.message(
      'no friends to add',
      name: 'no_friends_to_add',
      desc: '',
      args: [],
    );
  }

  /// `total`
  String get total {
    return Intl.message(
      'total',
      name: 'total',
      desc: '',
      args: [],
    );
  }

  /// `ThisWeekTotal`
  String get this_week {
    return Intl.message(
      'ThisWeekTotal',
      name: 'this_week',
      desc: '',
      args: [],
    );
  }

  /// `The word count exceeds the limit of 50 characters`
  String get the_word_count_exceeds_the_limit_of_50_characters {
    return Intl.message(
      'The word count exceeds the limit of 50 characters',
      name: 'the_word_count_exceeds_the_limit_of_50_characters',
      desc: '',
      args: [],
    );
  }

  /// `all`
  String get all {
    return Intl.message(
      'all',
      name: 'all',
      desc: '',
      args: [],
    );
  }

  /// `Handle it for you now`
  String get handle_it_for_you_now {
    return Intl.message(
      'Handle it for you now',
      name: 'handle_it_for_you_now',
      desc: '',
      args: [],
    );
  }

  /// `Overdue automatic cancellation`
  String get overdue_automatic_cancellation {
    return Intl.message(
      'Overdue automatic cancellation',
      name: 'overdue_automatic_cancellation',
      desc: '',
      args: [],
    );
  }

  /// `business Card Introduction`
  String get business_card_introduction {
    return Intl.message(
      'business Card Introduction',
      name: 'business_card_introduction',
      desc: '',
      args: [],
    );
  }

  /// `The limit has been reached`
  String get the_limit_has_been_reached {
    return Intl.message(
      'The limit has been reached',
      name: 'the_limit_has_been_reached',
      desc: '',
      args: [],
    );
  }

  /// `unsubscribe`
  String get unsubscribe {
    return Intl.message(
      'unsubscribe',
      name: 'unsubscribe',
      desc: '',
      args: [],
    );
  }

  /// `are you sure unsubscribe?`
  String get are_you_sure_unsubscribe {
    return Intl.message(
      'are you sure unsubscribe?',
      name: 'are_you_sure_unsubscribe',
      desc: '',
      args: [],
    );
  }

  /// `little helper`
  String get little_helper {
    return Intl.message(
      'little helper',
      name: 'little_helper',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'en'),
      Locale.fromSubtags(languageCode: 'zh', scriptCode: 'Hant'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
