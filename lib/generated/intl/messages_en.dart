// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en';

  static String m0(string) => "${Intl.select(string, {
            'childhood': 'Childhood',
            'growingUp': 'GrowingUp',
            'Mature': 'Mature',
            'die': 'Die',
            'other': 'none',
          })}";

  static String m1(string) => "${Intl.select(string, {
            'aries': 'Aries',
            'taurus': 'Taurus',
            'gemini': 'Gemini',
            'cancer': 'Cancer',
            'leo': 'Leo',
            'virgo': 'Virgo',
            'libra': 'Libra',
            'scorpio': 'Scorpio',
            'sagittarius': 'Sagittarius',
            'capricorn': 'Capricorn',
            'aquarius': 'Aquarius',
            'pisces': 'Pisces',
            'other': 'none',
          })}";

  static String m2(status) => "NowStatus: ${Intl.select(status, {
            'none': 'none',
            'seek': 'Seek',
            'other': 'Sought',
          })}";

  static String m3(string) => "${Intl.select(string, {
            'CREDIT_CARD': 'creditCard',
            'GOOGLE_IAP': 'googleIap',
            'APPLE_IAP': 'appleIap',
            'other': 'creditCard',
          })}";

  static String m4(value) => "Unblock Need ${value} Point";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "It_s_not_time_to_switch":
            MessageLookupByLibrary.simpleMessage("It\'s not time to switch"),
        "about": MessageLookupByLibrary.simpleMessage("About"),
        "about_ashera": MessageLookupByLibrary.simpleMessage("About Ashera"),
        "about_me": MessageLookupByLibrary.simpleMessage("About Me"),
        "add": MessageLookupByLibrary.simpleMessage("Add"),
        "add_friend": MessageLookupByLibrary.simpleMessage("Add Friend"),
        "add_friends": MessageLookupByLibrary.simpleMessage("Add friends"),
        "added_friend": MessageLookupByLibrary.simpleMessage("Added friend"),
        "address": MessageLookupByLibrary.simpleMessage("address"),
        "again": MessageLookupByLibrary.simpleMessage("again"),
        "age": MessageLookupByLibrary.simpleMessage("Age"),
        "agree": MessageLookupByLibrary.simpleMessage("agree"),
        "agree_to_the_user_agreement": MessageLookupByLibrary.simpleMessage(
            "Please agree to the User Agreement"),
        "agreed_terms":
            MessageLookupByLibrary.simpleMessage("Agree the user agreement"),
        "album": MessageLookupByLibrary.simpleMessage("Album"),
        "all": MessageLookupByLibrary.simpleMessage("all"),
        "already": MessageLookupByLibrary.simpleMessage("Already"),
        "amount_of_the_transaction":
            MessageLookupByLibrary.simpleMessage("Amount Of The Transaction"),
        "app_update": MessageLookupByLibrary.simpleMessage("App Update"),
        "apply_finish_waiting_accept": MessageLookupByLibrary.simpleMessage(
            "Apply Finish\nWaiting Accept..."),
        "are_you_sure": MessageLookupByLibrary.simpleMessage("Are You Sure "),
        "are_you_sure_unsubscribe":
            MessageLookupByLibrary.simpleMessage("are you sure unsubscribe?"),
        "at_large_membership":
            MessageLookupByLibrary.simpleMessage("At Large Membership"),
        "at_least": MessageLookupByLibrary.simpleMessage("At Least"),
        "be_gotcha": MessageLookupByLibrary.simpleMessage("BeGotcha"),
        "bed": MessageLookupByLibrary.simpleMessage("Bed"),
        "birthday": MessageLookupByLibrary.simpleMessage("Birthday"),
        "block_list": MessageLookupByLibrary.simpleMessage("Block list"),
        "blockade": MessageLookupByLibrary.simpleMessage("blockade"),
        "blood_type": MessageLookupByLibrary.simpleMessage("Blood Type"),
        "bonus": MessageLookupByLibrary.simpleMessage("Bonus"),
        "bought": MessageLookupByLibrary.simpleMessage("bought"),
        "business_card_introduction":
            MessageLookupByLibrary.simpleMessage("business Card Introduction"),
        "buy": MessageLookupByLibrary.simpleMessage("Buy"),
        "buy_this_poop_first":
            MessageLookupByLibrary.simpleMessage("Please buy this poop first"),
        "cabinet": MessageLookupByLibrary.simpleMessage("Cabinet"),
        "cancel": MessageLookupByLibrary.simpleMessage("Cancel"),
        "cancel_the_visit":
            MessageLookupByLibrary.simpleMessage("Cancel The Visit"),
        "cancel_visit": MessageLookupByLibrary.simpleMessage("Cancel Visit"),
        "cancelled": MessageLookupByLibrary.simpleMessage("Cancelled"),
        "cannot_be_replaced": MessageLookupByLibrary.simpleMessage(
            "Not purchased and cannot be replaced"),
        "capture_completed":
            MessageLookupByLibrary.simpleMessage("capture completed"),
        "cat": MessageLookupByLibrary.simpleMessage("Cat"),
        "certification": MessageLookupByLibrary.simpleMessage("Certification"),
        "chair": MessageLookupByLibrary.simpleMessage("Chair"),
        "change_icon":
            MessageLookupByLibrary.simpleMessage("change Ashera app icon"),
        "change_password":
            MessageLookupByLibrary.simpleMessage("Change Password"),
        "chat": MessageLookupByLibrary.simpleMessage("chat"),
        "clean_up": MessageLookupByLibrary.simpleMessage("clean up"),
        "clear_facial_features_frontal_photo":
            MessageLookupByLibrary.simpleMessage(
                "Clear facial features, frontal photo"),
        "click_the_circle_for_face_recognition":
            MessageLookupByLibrary.simpleMessage(
                "Please click the circle for face recognition"),
        "close": MessageLookupByLibrary.simpleMessage("Close "),
        "closing": MessageLookupByLibrary.simpleMessage("Closing"),
        "closing_time_not_yet":
            MessageLookupByLibrary.simpleMessage("closing time not yet"),
        "colorful_social":
            MessageLookupByLibrary.simpleMessage("ColorfulSocial"),
        "confirm": MessageLookupByLibrary.simpleMessage("confirm"),
        "confirm_changes":
            MessageLookupByLibrary.simpleMessage("Confirm changes"),
        "confirm_recharge":
            MessageLookupByLibrary.simpleMessage("Confirm Recharge"),
        "confirm_upload":
            MessageLookupByLibrary.simpleMessage("Confirm upload"),
        "constellation": MessageLookupByLibrary.simpleMessage("Constellation"),
        "content": MessageLookupByLibrary.simpleMessage("content"),
        "count_down": MessageLookupByLibrary.simpleMessage("Count Down"),
        "current_look": MessageLookupByLibrary.simpleMessage("current look"),
        "currently_have_no_friends":
            MessageLookupByLibrary.simpleMessage("currently have no friends"),
        "deduct_points": MessageLookupByLibrary.simpleMessage("deduct points"),
        "defaultDung": MessageLookupByLibrary.simpleMessage("default"),
        "delete": MessageLookupByLibrary.simpleMessage("delete"),
        "different_passwords_entered_twice":
            MessageLookupByLibrary.simpleMessage(
                "Different Passwords Entered Twice"),
        "disagree": MessageLookupByLibrary.simpleMessage("disagree"),
        "distance": MessageLookupByLibrary.simpleMessage("Distance"),
        "do_not_search_for_your_own_UID": MessageLookupByLibrary.simpleMessage(
            "Do not Search for your own UID"),
        "do_you_agree_with_the_other_party_to_cancel_the_visit":
            MessageLookupByLibrary.simpleMessage(
                "Do you agree with the other party to cancel the visit?"),
        "does_it_cost": MessageLookupByLibrary.simpleMessage("Does it cost"),
        "dog": MessageLookupByLibrary.simpleMessage("Dog"),
        "doghouse": MessageLookupByLibrary.simpleMessage("Doghouse"),
        "duplicate_registered":
            MessageLookupByLibrary.simpleMessage("Duplicate registered"),
        "edit_profile": MessageLookupByLibrary.simpleMessage("Edit profile"),
        "edit_remarks":
            MessageLookupByLibrary.simpleMessage("Change friend name"),
        "enter_a_message": MessageLookupByLibrary.simpleMessage(
            "Please enter a message, at least 8 characters"),
        "enter_a_nickname":
            MessageLookupByLibrary.simpleMessage("Please enter a nickname"),
        "enter_a_phone_number": MessageLookupByLibrary.simpleMessage(
            "Please enter the phone number"),
        "enter_a_profession":
            MessageLookupByLibrary.simpleMessage("Please enter occupation"),
        "enter_alphanumeric_characters": MessageLookupByLibrary.simpleMessage(
            "Please enter 6~12 alphanumeric characters"),
        "enter_confirmation_code":
            MessageLookupByLibrary.simpleMessage("Enter confirmation code"),
        "enter_message": MessageLookupByLibrary.simpleMessage("Enter Message"),
        "enter_new_password":
            MessageLookupByLibrary.simpleMessage("Please enter a new password"),
        "enter_new_password_again": MessageLookupByLibrary.simpleMessage(
            "Please enter new password again"),
        "enter_old_password":
            MessageLookupByLibrary.simpleMessage("Please enter old password"),
        "enter_password":
            MessageLookupByLibrary.simpleMessage("Please enter password"),
        "enter_password_again": MessageLookupByLibrary.simpleMessage(
            "Please enter the password again"),
        "face_recognition":
            MessageLookupByLibrary.simpleMessage("Face recognition"),
        "face_recognition_warning": MessageLookupByLibrary.simpleMessage(
            "※If the face recognition photo needs to be changed after the review is successful,\nPlease contact customer service"),
        "fail": MessageLookupByLibrary.simpleMessage("Fail"),
        "failed_to_upload_again": MessageLookupByLibrary.simpleMessage(
            "Failed! Please change the identification avatar"),
        "feeding": MessageLookupByLibrary.simpleMessage("feeding"),
        "female": MessageLookupByLibrary.simpleMessage("Female"),
        "fence": MessageLookupByLibrary.simpleMessage("Fence"),
        "filter": MessageLookupByLibrary.simpleMessage("filter"),
        "finish": MessageLookupByLibrary.simpleMessage("Finish"),
        "finish_visit": MessageLookupByLibrary.simpleMessage("Finish Visit"),
        "for_face_recognition":
            MessageLookupByLibrary.simpleMessage("For face recognition"),
        "forgot_password":
            MessageLookupByLibrary.simpleMessage("Forgot password"),
        "frequency": MessageLookupByLibrary.simpleMessage("frequency"),
        "friend": MessageLookupByLibrary.simpleMessage("friend"),
        "friend_invitation":
            MessageLookupByLibrary.simpleMessage("Friend Invitation"),
        "friendship_invitation_has_been_sent":
            MessageLookupByLibrary.simpleMessage(
                "Friendship invitation has been sent"),
        "game": MessageLookupByLibrary.simpleMessage("game"),
        "game_content": MessageLookupByLibrary.simpleMessage(
            "Ambiguous, starting from hurting each other"),
        "gender": MessageLookupByLibrary.simpleMessage("Gender"),
        "get_face_in_this_week":
            MessageLookupByLibrary.simpleMessage("Get Face In This Week For"),
        "go_to_premium": MessageLookupByLibrary.simpleMessage("go to Premium"),
        "gotcha": MessageLookupByLibrary.simpleMessage("Gotcha"),
        "growingPhase": m0,
        "growth_time": MessageLookupByLibrary.simpleMessage("growth time"),
        "handle_it_for_you_now":
            MessageLookupByLibrary.simpleMessage("Handle it for you now"),
        "height": MessageLookupByLibrary.simpleMessage("Height"),
        "hide": MessageLookupByLibrary.simpleMessage("hide"),
        "horoscope": m1,
        "hour": MessageLookupByLibrary.simpleMessage("hours"),
        "house": MessageLookupByLibrary.simpleMessage("House"),
        "how_many_cat": MessageLookupByLibrary.simpleMessage("cats"),
        "how_many_times": MessageLookupByLibrary.simpleMessage("times"),
        "how_many_word": MessageLookupByLibrary.simpleMessage("words"),
        "hunger": MessageLookupByLibrary.simpleMessage("hunger"),
        "hunt_list": MessageLookupByLibrary.simpleMessage("hunt list"),
        "i_have_seen": MessageLookupByLibrary.simpleMessage("I have seen"),
        "i_know": MessageLookupByLibrary.simpleMessage("I Know"),
        "identification_record":
            MessageLookupByLibrary.simpleMessage("IdentificationRecord"),
        "identify_friends":
            MessageLookupByLibrary.simpleMessage("Identify Friends"),
        "identify_percent":
            MessageLookupByLibrary.simpleMessage("Identify Percent"),
        "identify_time_over_content": MessageLookupByLibrary.simpleMessage(
            "Time UP ! Do You Want Make Friend?"),
        "impossible_to_buy": MessageLookupByLibrary.simpleMessage(
            "This item is preset and cannot be purchased"),
        "information_is_incomplete":
            MessageLookupByLibrary.simpleMessage("Information is incomplete"),
        "interactive": MessageLookupByLibrary.simpleMessage("interactive"),
        "interactive_only_photos_not_uploaded":
            MessageLookupByLibrary.simpleMessage(
                "Interactive-only photos not uploaded"),
        "interactive_photo_authentication":
            MessageLookupByLibrary.simpleMessage(
                "Interactive Photo Authentication"),
        "is_read_all_message":
            MessageLookupByLibrary.simpleMessage("is read all message"),
        "km": MessageLookupByLibrary.simpleMessage("km"),
        "leave_comments":
            MessageLookupByLibrary.simpleMessage("leave comments"),
        "leave_message": MessageLookupByLibrary.simpleMessage("Leave Message"),
        "lift": MessageLookupByLibrary.simpleMessage("lift"),
        "list": MessageLookupByLibrary.simpleMessage("List"),
        "little_helper": MessageLookupByLibrary.simpleMessage("little helper"),
        "location": MessageLookupByLibrary.simpleMessage("Location"),
        "login": MessageLookupByLibrary.simpleMessage("login"),
        "login_success": MessageLookupByLibrary.simpleMessage("Login Success!"),
        "logout": MessageLookupByLibrary.simpleMessage("account deletion"),
        "m": MessageLookupByLibrary.simpleMessage("m"),
        "male": MessageLookupByLibrary.simpleMessage("Male"),
        "map": MessageLookupByLibrary.simpleMessage("Map"),
        "mat": MessageLookupByLibrary.simpleMessage("Mat"),
        "me": MessageLookupByLibrary.simpleMessage("me"),
        "member_profile":
            MessageLookupByLibrary.simpleMessage("member profile"),
        "menu": MessageLookupByLibrary.simpleMessage("Menu"),
        "message": MessageLookupByLibrary.simpleMessage("Message"),
        "minute": MessageLookupByLibrary.simpleMessage("minutes"),
        "mobile_number_entered_incorrectly":
            MessageLookupByLibrary.simpleMessage(
                "Mobile number entered incorrectly"),
        "mode": MessageLookupByLibrary.simpleMessage("Mode"),
        "my_QR_code": MessageLookupByLibrary.simpleMessage("My QR CODE"),
        "my_points": MessageLookupByLibrary.simpleMessage("My Points"),
        "need_to_change": MessageLookupByLibrary.simpleMessage(
            "If you need to change, please go to the previous page"),
        "need_to_complete_the_login": MessageLookupByLibrary.simpleMessage(
            "To allow you to experience a more complete Ashera service, you need to complete the login of face recognition."),
        "need_to_re_authenticate":
            MessageLookupByLibrary.simpleMessage("need to re-authenticate"),
        "needed": MessageLookupByLibrary.simpleMessage("Needed"),
        "new_version":
            MessageLookupByLibrary.simpleMessage("There is a new version"),
        "next_step": MessageLookupByLibrary.simpleMessage("Next step"),
        "nick_name": MessageLookupByLibrary.simpleMessage("NickName"),
        "no_friends_to_add":
            MessageLookupByLibrary.simpleMessage("no friends to add"),
        "no_headshots_taken":
            MessageLookupByLibrary.simpleMessage("No Headshots Taken"),
        "no_profile_for_this_person":
            MessageLookupByLibrary.simpleMessage("No profile for this person"),
        "none_gender": MessageLookupByLibrary.simpleMessage("none"),
        "not_enough_cleaning":
            MessageLookupByLibrary.simpleMessage("Not enough cleaning"),
        "not_enough_points":
            MessageLookupByLibrary.simpleMessage("Not Enough Points"),
        "not_enough_shit":
            MessageLookupByLibrary.simpleMessage("Not enough shit"),
        "not_rated_yet": MessageLookupByLibrary.simpleMessage("Not rated yet"),
        "not_selected": MessageLookupByLibrary.simpleMessage("Not selected"),
        "not_yet_open_time":
            MessageLookupByLibrary.simpleMessage("Not yet open time"),
        "not_yet_verified": MessageLookupByLibrary.simpleMessage(
            "Face recognition has not been verified, \nplease go to personal settings to verify"),
        "notice": MessageLookupByLibrary.simpleMessage("Notice"),
        "nowPeekabooStatus": m2,
        "ok_to_use": MessageLookupByLibrary.simpleMessage("Ok to Use"),
        "oneWeekBeGotChaTime":
            MessageLookupByLibrary.simpleMessage("One Week Be GotCha"),
        "open": MessageLookupByLibrary.simpleMessage("Open "),
        "opening": MessageLookupByLibrary.simpleMessage("Opening"),
        "opinion": MessageLookupByLibrary.simpleMessage(
            "Comments and Questions Feedback"),
        "opinion_content": MessageLookupByLibrary.simpleMessage(
            "Please enter comments and feedback"),
        "opposite_paid": MessageLookupByLibrary.simpleMessage("Opposite Paid"),
        "order_is_being_created":
            MessageLookupByLibrary.simpleMessage("Order is being Created..."),
        "overdue_automatic_cancellation": MessageLookupByLibrary.simpleMessage(
            "Overdue automatic cancellation"),
        "paid_mode": MessageLookupByLibrary.simpleMessage("Paid Mode"),
        "paymentType": m3,
        "payment_amount":
            MessageLookupByLibrary.simpleMessage("Payment Amount"),
        "payment_method":
            MessageLookupByLibrary.simpleMessage("Payment Method"),
        "payment_status":
            MessageLookupByLibrary.simpleMessage("payment status"),
        "peekaboo": MessageLookupByLibrary.simpleMessage("Peekaboo"),
        "peekaboo_content": MessageLookupByLibrary.simpleMessage(
            "Fate is no longer \na chance encounter."),
        "phone_verification":
            MessageLookupByLibrary.simpleMessage("Phone verification"),
        "photo_comparison":
            MessageLookupByLibrary.simpleMessage("photo comparison"),
        "photo_selection":
            MessageLookupByLibrary.simpleMessage("Photo selection"),
        "photograph": MessageLookupByLibrary.simpleMessage("Photograph"),
        "pig_eating": MessageLookupByLibrary.simpleMessage("feeding..."),
        "please_add_value": MessageLookupByLibrary.simpleMessage(
            "Insufficient points, please add value"),
        "please_close_status": MessageLookupByLibrary.simpleMessage(
            "Please close gotcha status first"),
        "please_enter_address":
            MessageLookupByLibrary.simpleMessage("Please enter address"),
        "please_enter_ashera_id":
            MessageLookupByLibrary.simpleMessage("Please Enter Ashera ID"),
        "please_enter_your_commit":
            MessageLookupByLibrary.simpleMessage("Please Enter Your Commit"),
        "please_log_in_to_the_app_to_complete_the_verification":
            MessageLookupByLibrary.simpleMessage(
                "Please log in to the app to complete the verification"),
        "please_select_an_identifying_photo":
            MessageLookupByLibrary.simpleMessage(
                "Please select an identifying photo"),
        "please_upload_one":
            MessageLookupByLibrary.simpleMessage("Please upload one"),
        "point": MessageLookupByLibrary.simpleMessage("Point"),
        "point_of_sale": MessageLookupByLibrary.simpleMessage("point of sale"),
        "poop": MessageLookupByLibrary.simpleMessage("poop"),
        "profession": MessageLookupByLibrary.simpleMessage("Profession"),
        "profile_picture":
            MessageLookupByLibrary.simpleMessage("profile picture"),
        "purchasing": MessageLookupByLibrary.simpleMessage("Purchasing..."),
        "question_type": MessageLookupByLibrary.simpleMessage("Question Type"),
        "rating_bar": MessageLookupByLibrary.simpleMessage("Rating"),
        "received_an_invitation_to_visit": MessageLookupByLibrary.simpleMessage(
            "received an invitation to visit"),
        "recharge": MessageLookupByLibrary.simpleMessage("Recharge"),
        "recharge_failed":
            MessageLookupByLibrary.simpleMessage("Recharge failed"),
        "recharge_failed_please_recharge": MessageLookupByLibrary.simpleMessage(
            "Recharge failed, please recharge"),
        "recharge_record":
            MessageLookupByLibrary.simpleMessage("Recharge Record"),
        "recognition_failed": MessageLookupByLibrary.simpleMessage(
            "Recognition failed, please select a friend to add"),
        "record": MessageLookupByLibrary.simpleMessage("Record"),
        "record_points": MessageLookupByLibrary.simpleMessage("Points"),
        "refuse": MessageLookupByLibrary.simpleMessage("Refuse"),
        "register": MessageLookupByLibrary.simpleMessage("register"),
        "registering": MessageLookupByLibrary.simpleMessage("Registering"),
        "registration_is_complete_face_recognition_has_not_been_completed":
            MessageLookupByLibrary.simpleMessage(
                "Registration is complete Face recognition has not been completed"),
        "registration_success":
            MessageLookupByLibrary.simpleMessage("Registration Success!"),
        "release_requirements":
            MessageLookupByLibrary.simpleMessage("release requirements"),
        "renew": MessageLookupByLibrary.simpleMessage("renew"),
        "report": MessageLookupByLibrary.simpleMessage("report"),
        "review": MessageLookupByLibrary.simpleMessage("review"),
        "reviewed": MessageLookupByLibrary.simpleMessage("Reviewed"),
        "sales_record": MessageLookupByLibrary.simpleMessage("Sales record"),
        "save": MessageLookupByLibrary.simpleMessage("Save"),
        "saw_mine": MessageLookupByLibrary.simpleMessage("Saw mine"),
        "scan_qr": MessageLookupByLibrary.simpleMessage("Scan QRCode"),
        "scan_qr_code_add_me": MessageLookupByLibrary.simpleMessage(
            "Please scan the QR CODE and add me as a friend"),
        "scan_the_QR_CODE":
            MessageLookupByLibrary.simpleMessage("Please scan the QR CODE"),
        "scan_your_face_for_identification":
            MessageLookupByLibrary.simpleMessage(
                "Please scan your face for identification"),
        "screening_condition":
            MessageLookupByLibrary.simpleMessage("Screening Condition"),
        "search": MessageLookupByLibrary.simpleMessage("search"),
        "secret": MessageLookupByLibrary.simpleMessage("secret"),
        "seek": MessageLookupByLibrary.simpleMessage("Seek"),
        "select_a_birthday":
            MessageLookupByLibrary.simpleMessage("Please select a birthday"),
        "select_gender":
            MessageLookupByLibrary.simpleMessage("Please select gender"),
        "select_question_type":
            MessageLookupByLibrary.simpleMessage("Select Question Type"),
        "self_paid": MessageLookupByLibrary.simpleMessage("Self Paid"),
        "sell": MessageLookupByLibrary.simpleMessage("sell"),
        "send": MessageLookupByLibrary.simpleMessage("Send"),
        "send_commit": MessageLookupByLibrary.simpleMessage("Send Commit"),
        "send_verification_code":
            MessageLookupByLibrary.simpleMessage("Send the verification code"),
        "sent_a_picture":
            MessageLookupByLibrary.simpleMessage("sent a picture"),
        "sent_a_video": MessageLookupByLibrary.simpleMessage("sent a video"),
        "serial_number": MessageLookupByLibrary.simpleMessage("serial number"),
        "setting": MessageLookupByLibrary.simpleMessage("Setting"),
        "show_mobile_barcode":
            MessageLookupByLibrary.simpleMessage("Show mobile barcode"),
        "sign_out": MessageLookupByLibrary.simpleMessage("Sign out"),
        "skip": MessageLookupByLibrary.simpleMessage("skip"),
        "smile_percent": MessageLookupByLibrary.simpleMessage("Smile Percent"),
        "star_rating": MessageLookupByLibrary.simpleMessage("star rating"),
        "start_the_game":
            MessageLookupByLibrary.simpleMessage("start the game"),
        "successful_recharge":
            MessageLookupByLibrary.simpleMessage("successful recharge"),
        "sure": MessageLookupByLibrary.simpleMessage("Sure"),
        "table": MessageLookupByLibrary.simpleMessage("Table"),
        "the_limit_has_been_reached":
            MessageLookupByLibrary.simpleMessage("The limit has been reached"),
        "the_opponent_has_not_rated_yet": MessageLookupByLibrary.simpleMessage(
            "The opponent has not rated yet"),
        "the_other_party_does_not_agree_to_your_visit":
            MessageLookupByLibrary.simpleMessage(
                "The other party does not agree to your visit"),
        "the_room_is_being_arranged":
            MessageLookupByLibrary.simpleMessage("The room is being arranged"),
        "the_visit_has_been_completed": MessageLookupByLibrary.simpleMessage(
            "The visit has been completed"),
        "the_word_count_exceeds_the_limit_of_50_characters":
            MessageLookupByLibrary.simpleMessage(
                "The word count exceeds the limit of 50 characters"),
        "there_are_currently_no_records": MessageLookupByLibrary.simpleMessage(
            "There are currently no records"),
        "there_is_currently_no_chat_history":
            MessageLookupByLibrary.simpleMessage(
                "There is currently no chat history"),
        "this_week": MessageLookupByLibrary.simpleMessage("ThisWeekTotal"),
        "those_who_have_been_visited_have_not_completed_the_class":
            MessageLookupByLibrary.simpleMessage(
                "Those who have been visited have not completed the class, and they are not allowed to visit the class."),
        "time_left": MessageLookupByLibrary.simpleMessage("time left"),
        "timed_out": MessageLookupByLibrary.simpleMessage("timed out"),
        "today_be_gotcha":
            MessageLookupByLibrary.simpleMessage("Today Be Gotcha"),
        "today_gotcha": MessageLookupByLibrary.simpleMessage("Today Gotcha"),
        "top_up_amount": MessageLookupByLibrary.simpleMessage("Top-up Point"),
        "total": MessageLookupByLibrary.simpleMessage("total"),
        "transaction_date":
            MessageLookupByLibrary.simpleMessage("Transaction Date"),
        "transaction_number":
            MessageLookupByLibrary.simpleMessage("Transaction Number"),
        "unblock": MessageLookupByLibrary.simpleMessage("Unblock"),
        "unblockNeedPoint": m4,
        "unblock_member_mugshot":
            MessageLookupByLibrary.simpleMessage("Unblock Mugshot ?"),
        "unsubscribe": MessageLookupByLibrary.simpleMessage("unsubscribe"),
        "unverified": MessageLookupByLibrary.simpleMessage("Unverified"),
        "updating_information":
            MessageLookupByLibrary.simpleMessage("Updating Information"),
        "upgrade_VIP_membership":
            MessageLookupByLibrary.simpleMessage("Upgrade VIP Membership"),
        "upgrade_vip": MessageLookupByLibrary.simpleMessage("Upgrade VIP"),
        "upload_file_too_large":
            MessageLookupByLibrary.simpleMessage("Upload file too large"),
        "uploading": MessageLookupByLibrary.simpleMessage("uploading..."),
        "uploading_comparison":
            MessageLookupByLibrary.simpleMessage("uploading comparison"),
        "user_agreement":
            MessageLookupByLibrary.simpleMessage("User Agreement"),
        "verified": MessageLookupByLibrary.simpleMessage("Verified"),
        "verifying": MessageLookupByLibrary.simpleMessage("Verifying...."),
        "video": MessageLookupByLibrary.simpleMessage("Video"),
        "vip_member": MessageLookupByLibrary.simpleMessage("VIP Member"),
        "visit": MessageLookupByLibrary.simpleMessage("Visit"),
        "visit_content": MessageLookupByLibrary.simpleMessage(
            "Let one thing have \ninfinite possibilities."),
        "visited": MessageLookupByLibrary.simpleMessage("Visited"),
        "visiting_class_notice":
            MessageLookupByLibrary.simpleMessage("Visiting class notice"),
        "wait_other_side_cancel_visit":
            MessageLookupByLibrary.simpleMessage("Wait Cancel Visit"),
        "wait_time": MessageLookupByLibrary.simpleMessage("Wait Time"),
        "waiting": MessageLookupByLibrary.simpleMessage("Waiting"),
        "waiting_add_friends":
            MessageLookupByLibrary.simpleMessage("Waiting Add Friends"),
        "waiting_add_friends_confirm":
            MessageLookupByLibrary.simpleMessage("Waiting Add Friends Confirm"),
        "wallpaper": MessageLookupByLibrary.simpleMessage("Wallpaper"),
        "weight": MessageLookupByLibrary.simpleMessage("Weight"),
        "wordLimit": MessageLookupByLibrary.simpleMessage(
            "Word\'s length can\'t bigger than"),
        "you_can_post_your_requirements_here":
            MessageLookupByLibrary.simpleMessage(
                "You can post your requirements here"),
        "you_have_been_caught":
            MessageLookupByLibrary.simpleMessage("you have been caught"),
        "you_have_had_a_good_time_at_this_member_house":
            MessageLookupByLibrary.simpleMessage(
                "You have had a good time at this member\'s house"),
        "yuan": MessageLookupByLibrary.simpleMessage("Yuan")
      };
}
