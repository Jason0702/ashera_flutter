// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a zh_Hant locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'zh_Hant';

  static String m0(string) => "${Intl.select(string, {
            'childhood': '幼年',
            'growingUp': '成長',
            'mature': '成熟',
            'die': '死亡',
            'other': 'none',
          })}";

  static String m1(string) => "${Intl.select(string, {
            'aries': '牡羊座',
            'taurus': '金牛座',
            'gemini': '雙子座',
            'cancer': '巨蟹座',
            'leo': '獅子座',
            'virgo': '處女座',
            'libra': '天秤座',
            'scorpio': '天蠍座',
            'sagittarius': '射手座',
            'capricorn': '摩羯座',
            'aquarius': '水瓶座',
            'pisces': '雙魚座',
            'other': 'none',
          })}";

  static String m2(status) => "現在狀態: ${Intl.select(status, {
            'none': '無',
            'seek': '狗狗方',
            'other': '貓貓方',
          })}";

  static String m3(string) => "${Intl.select(string, {
            'CREDIT_CARD': '信用卡付款',
            'GOOGLE_IAP': 'Google 應用程式內購',
            'APPLE_IAP': 'Apple 應用程式內購',
            'other': '信用卡付款',
          })}";

  static String m4(value) => "解鎖需要花費${value}點";

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "It_s_not_time_to_switch":
            MessageLookupByLibrary.simpleMessage("還不到切換時間"),
        "about": MessageLookupByLibrary.simpleMessage("約"),
        "about_ashera": MessageLookupByLibrary.simpleMessage("關於 Ashera"),
        "about_me": MessageLookupByLibrary.simpleMessage("關於我"),
        "add": MessageLookupByLibrary.simpleMessage("加入"),
        "add_friend": MessageLookupByLibrary.simpleMessage("加入好友"),
        "add_friends": MessageLookupByLibrary.simpleMessage("新增好友"),
        "added_friend": MessageLookupByLibrary.simpleMessage("已加入好友"),
        "address": MessageLookupByLibrary.simpleMessage("地址"),
        "again": MessageLookupByLibrary.simpleMessage("重養"),
        "age": MessageLookupByLibrary.simpleMessage("年齡"),
        "agree": MessageLookupByLibrary.simpleMessage("同意"),
        "agree_to_the_user_agreement":
            MessageLookupByLibrary.simpleMessage("請同意用戶條約"),
        "agreed_terms": MessageLookupByLibrary.simpleMessage("同意用戶約定條款"),
        "album": MessageLookupByLibrary.simpleMessage("相簿"),
        "all": MessageLookupByLibrary.simpleMessage("全部"),
        "already": MessageLookupByLibrary.simpleMessage("已成功"),
        "amount_of_the_transaction":
            MessageLookupByLibrary.simpleMessage("交易金額"),
        "app_update": MessageLookupByLibrary.simpleMessage("應用更新"),
        "apply_finish_waiting_accept":
            MessageLookupByLibrary.simpleMessage("申請完成\n等待對方確認..."),
        "are_you_sure": MessageLookupByLibrary.simpleMessage("是否確定"),
        "are_you_sure_unsubscribe":
            MessageLookupByLibrary.simpleMessage("確定要取消訂閱嗎?"),
        "at_large_membership": MessageLookupByLibrary.simpleMessage("一般會員"),
        "at_least": MessageLookupByLibrary.simpleMessage("至少"),
        "be_gotcha": MessageLookupByLibrary.simpleMessage("被獵取"),
        "bed": MessageLookupByLibrary.simpleMessage("床"),
        "birthday": MessageLookupByLibrary.simpleMessage("生日"),
        "block_list": MessageLookupByLibrary.simpleMessage("封鎖名單"),
        "blockade": MessageLookupByLibrary.simpleMessage("封鎖"),
        "blood_type": MessageLookupByLibrary.simpleMessage("血型"),
        "bonus": MessageLookupByLibrary.simpleMessage("加值"),
        "bought": MessageLookupByLibrary.simpleMessage("已購買"),
        "business_card_introduction":
            MessageLookupByLibrary.simpleMessage("名片自介"),
        "buy": MessageLookupByLibrary.simpleMessage("購買"),
        "buy_this_poop_first": MessageLookupByLibrary.simpleMessage("請先購買此大便"),
        "cabinet": MessageLookupByLibrary.simpleMessage("櫃子"),
        "cancel": MessageLookupByLibrary.simpleMessage("取消"),
        "cancel_the_visit": MessageLookupByLibrary.simpleMessage("取消被探班"),
        "cancel_visit": MessageLookupByLibrary.simpleMessage("取消探班"),
        "cancelled": MessageLookupByLibrary.simpleMessage("已取消"),
        "cannot_be_replaced": MessageLookupByLibrary.simpleMessage("未購買，無法更換"),
        "capture_completed": MessageLookupByLibrary.simpleMessage("獵取完成"),
        "cat": MessageLookupByLibrary.simpleMessage("貓"),
        "certification": MessageLookupByLibrary.simpleMessage("認證"),
        "chair": MessageLookupByLibrary.simpleMessage("椅子"),
        "change_icon": MessageLookupByLibrary.simpleMessage("更換 Ashera app 圖標"),
        "change_password": MessageLookupByLibrary.simpleMessage("變更密碼"),
        "chat": MessageLookupByLibrary.simpleMessage("聊天"),
        "clean_up": MessageLookupByLibrary.simpleMessage("清掃"),
        "clear_facial_features_frontal_photo":
            MessageLookupByLibrary.simpleMessage("五官清晰、正面照"),
        "click_the_circle_for_face_recognition":
            MessageLookupByLibrary.simpleMessage("請點選圓框進行人臉辨識"),
        "close": MessageLookupByLibrary.simpleMessage("關閉"),
        "closing": MessageLookupByLibrary.simpleMessage("關閉中"),
        "closing_time_not_yet": MessageLookupByLibrary.simpleMessage("還未到關閉時間"),
        "colorful_social": MessageLookupByLibrary.simpleMessage("繽紛社交"),
        "confirm": MessageLookupByLibrary.simpleMessage("確認"),
        "confirm_changes": MessageLookupByLibrary.simpleMessage("確認變更"),
        "confirm_recharge": MessageLookupByLibrary.simpleMessage("確認充值"),
        "confirm_upload": MessageLookupByLibrary.simpleMessage("確認上傳"),
        "constellation": MessageLookupByLibrary.simpleMessage("星座"),
        "content": MessageLookupByLibrary.simpleMessage("內容"),
        "count_down": MessageLookupByLibrary.simpleMessage("倒數"),
        "current_look": MessageLookupByLibrary.simpleMessage("當前造型"),
        "currently_have_no_friends":
            MessageLookupByLibrary.simpleMessage("目前沒有朋友"),
        "deduct_points": MessageLookupByLibrary.simpleMessage("扣除點數"),
        "defaultDung": MessageLookupByLibrary.simpleMessage("預設"),
        "delete": MessageLookupByLibrary.simpleMessage("刪除"),
        "different_passwords_entered_twice":
            MessageLookupByLibrary.simpleMessage("兩次輸入密碼不同"),
        "disagree": MessageLookupByLibrary.simpleMessage("不同意"),
        "distance": MessageLookupByLibrary.simpleMessage("距離"),
        "do_not_search_for_your_own_UID":
            MessageLookupByLibrary.simpleMessage("請勿搜尋自己的UID"),
        "do_you_agree_with_the_other_party_to_cancel_the_visit":
            MessageLookupByLibrary.simpleMessage("是否同意對方取消探班?"),
        "does_it_cost": MessageLookupByLibrary.simpleMessage("是否花費"),
        "dog": MessageLookupByLibrary.simpleMessage("狗"),
        "doghouse": MessageLookupByLibrary.simpleMessage("狗屋"),
        "duplicate_registered": MessageLookupByLibrary.simpleMessage("帳號已重複"),
        "edit_profile": MessageLookupByLibrary.simpleMessage("編輯個人檔案"),
        "edit_remarks": MessageLookupByLibrary.simpleMessage("變更好友名稱"),
        "enter_a_message": MessageLookupByLibrary.simpleMessage("請輸入留言，至少8個字"),
        "enter_a_nickname": MessageLookupByLibrary.simpleMessage("請輸入暱稱"),
        "enter_a_phone_number": MessageLookupByLibrary.simpleMessage("請輸入手機號碼"),
        "enter_a_profession": MessageLookupByLibrary.simpleMessage("請輸入職業"),
        "enter_alphanumeric_characters":
            MessageLookupByLibrary.simpleMessage("請輸入6~12位英數字"),
        "enter_confirmation_code":
            MessageLookupByLibrary.simpleMessage("請輸入驗證碼"),
        "enter_message": MessageLookupByLibrary.simpleMessage("請輸入訊息"),
        "enter_new_password": MessageLookupByLibrary.simpleMessage("請輸入新密碼"),
        "enter_new_password_again":
            MessageLookupByLibrary.simpleMessage("請再次輸入新密碼"),
        "enter_old_password": MessageLookupByLibrary.simpleMessage("請輸入舊密碼"),
        "enter_password": MessageLookupByLibrary.simpleMessage("請輸入密碼"),
        "enter_password_again": MessageLookupByLibrary.simpleMessage("請再次輸入密碼"),
        "face_recognition": MessageLookupByLibrary.simpleMessage("人臉辨識"),
        "face_recognition_warning":
            MessageLookupByLibrary.simpleMessage("※若審核成功後需更改人臉辨識照片，\n請聯繫客服"),
        "fail": MessageLookupByLibrary.simpleMessage("失敗"),
        "failed_to_upload_again":
            MessageLookupByLibrary.simpleMessage("失敗!\n請更換辨識頭像"),
        "feeding": MessageLookupByLibrary.simpleMessage("餵食"),
        "female": MessageLookupByLibrary.simpleMessage("女生"),
        "fence": MessageLookupByLibrary.simpleMessage("圍籬"),
        "filter": MessageLookupByLibrary.simpleMessage("篩選"),
        "finish": MessageLookupByLibrary.simpleMessage("完成"),
        "finish_visit": MessageLookupByLibrary.simpleMessage("完成探班"),
        "for_face_recognition": MessageLookupByLibrary.simpleMessage("供人臉辨識用"),
        "forgot_password": MessageLookupByLibrary.simpleMessage("忘記密碼"),
        "frequency": MessageLookupByLibrary.simpleMessage("次數"),
        "friend": MessageLookupByLibrary.simpleMessage("好友"),
        "friend_invitation": MessageLookupByLibrary.simpleMessage("好友邀請"),
        "friendship_invitation_has_been_sent":
            MessageLookupByLibrary.simpleMessage("交友邀請已送出"),
        "game": MessageLookupByLibrary.simpleMessage("遊戲"),
        "game_content": MessageLookupByLibrary.simpleMessage("曖昧，從互相傷害開始"),
        "gender": MessageLookupByLibrary.simpleMessage("性別"),
        "get_face_in_this_week": MessageLookupByLibrary.simpleMessage("一周內被刷臉"),
        "go_to_premium": MessageLookupByLibrary.simpleMessage("前往加值"),
        "gotcha": MessageLookupByLibrary.simpleMessage("獵取"),
        "growingPhase": m0,
        "growth_time": MessageLookupByLibrary.simpleMessage("成長時間"),
        "handle_it_for_you_now": MessageLookupByLibrary.simpleMessage("馬上為您處理"),
        "height": MessageLookupByLibrary.simpleMessage("身高"),
        "hide": MessageLookupByLibrary.simpleMessage("隱藏"),
        "horoscope": m1,
        "hour": MessageLookupByLibrary.simpleMessage("小時"),
        "house": MessageLookupByLibrary.simpleMessage("房子"),
        "how_many_cat": MessageLookupByLibrary.simpleMessage("隻貓"),
        "how_many_times": MessageLookupByLibrary.simpleMessage("次"),
        "how_many_word": MessageLookupByLibrary.simpleMessage("個字"),
        "hunger": MessageLookupByLibrary.simpleMessage("飢餓度"),
        "hunt_list": MessageLookupByLibrary.simpleMessage("獵取名單"),
        "i_have_seen": MessageLookupByLibrary.simpleMessage("我看過的"),
        "i_know": MessageLookupByLibrary.simpleMessage("我知道了"),
        "identification_record": MessageLookupByLibrary.simpleMessage("辨識紀錄"),
        "identify_friends": MessageLookupByLibrary.simpleMessage("辨識好友"),
        "identify_percent": MessageLookupByLibrary.simpleMessage("辨識度"),
        "identify_time_over_content":
            MessageLookupByLibrary.simpleMessage("請問你想要跟對方成為朋友嗎?"),
        "impossible_to_buy":
            MessageLookupByLibrary.simpleMessage("此物品為預設，無法購買"),
        "information_is_incomplete":
            MessageLookupByLibrary.simpleMessage("請填寫資料"),
        "interactive": MessageLookupByLibrary.simpleMessage("互動"),
        "interactive_only_photos_not_uploaded":
            MessageLookupByLibrary.simpleMessage("互動專用照片未上傳"),
        "interactive_photo_authentication":
            MessageLookupByLibrary.simpleMessage("互動照片認證"),
        "is_read_all_message": MessageLookupByLibrary.simpleMessage("已閱讀完所有訊息"),
        "km": MessageLookupByLibrary.simpleMessage("公里"),
        "leave_comments": MessageLookupByLibrary.simpleMessage("留言"),
        "leave_message": MessageLookupByLibrary.simpleMessage("留言訊息"),
        "lift": MessageLookupByLibrary.simpleMessage("解除"),
        "list": MessageLookupByLibrary.simpleMessage("列表"),
        "little_helper": MessageLookupByLibrary.simpleMessage("小幫手"),
        "location": MessageLookupByLibrary.simpleMessage("位置"),
        "login": MessageLookupByLibrary.simpleMessage("登入"),
        "login_success": MessageLookupByLibrary.simpleMessage("登入成功!"),
        "logout": MessageLookupByLibrary.simpleMessage("註銷"),
        "m": MessageLookupByLibrary.simpleMessage("公尺"),
        "male": MessageLookupByLibrary.simpleMessage("男生"),
        "map": MessageLookupByLibrary.simpleMessage("地圖"),
        "mat": MessageLookupByLibrary.simpleMessage("踏墊"),
        "me": MessageLookupByLibrary.simpleMessage("我"),
        "member_profile": MessageLookupByLibrary.simpleMessage("會員資料"),
        "menu": MessageLookupByLibrary.simpleMessage("選單"),
        "message": MessageLookupByLibrary.simpleMessage("訊息"),
        "minute": MessageLookupByLibrary.simpleMessage("分鐘"),
        "mobile_number_entered_incorrectly":
            MessageLookupByLibrary.simpleMessage("手機號碼輸入錯誤"),
        "mode": MessageLookupByLibrary.simpleMessage("模式"),
        "my_QR_code": MessageLookupByLibrary.simpleMessage("我的QR CODE"),
        "my_points": MessageLookupByLibrary.simpleMessage("我的點數"),
        "need_to_change": MessageLookupByLibrary.simpleMessage("如需變更請前往上一頁"),
        "need_to_complete_the_login": MessageLookupByLibrary.simpleMessage(
            "讓您體驗更完整的Ashera服務 需完成人臉辨識的登入。"),
        "need_to_re_authenticate":
            MessageLookupByLibrary.simpleMessage("需重新驗證"),
        "needed": MessageLookupByLibrary.simpleMessage("需求"),
        "new_version": MessageLookupByLibrary.simpleMessage("有新版本囉"),
        "next_step": MessageLookupByLibrary.simpleMessage("下一步"),
        "nick_name": MessageLookupByLibrary.simpleMessage("暱稱"),
        "no_friends_to_add": MessageLookupByLibrary.simpleMessage("沒有待加好友"),
        "no_headshots_taken": MessageLookupByLibrary.simpleMessage("未拍攝大頭照"),
        "no_profile_for_this_person":
            MessageLookupByLibrary.simpleMessage("無此人資料"),
        "none_gender": MessageLookupByLibrary.simpleMessage("不限"),
        "not_enough_cleaning": MessageLookupByLibrary.simpleMessage("清潔次數不夠囉"),
        "not_enough_points": MessageLookupByLibrary.simpleMessage("點數不足"),
        "not_enough_shit": MessageLookupByLibrary.simpleMessage("大便次數不夠囉"),
        "not_rated_yet": MessageLookupByLibrary.simpleMessage("尚未獲得評分"),
        "not_selected": MessageLookupByLibrary.simpleMessage("未選擇"),
        "not_yet_open_time": MessageLookupByLibrary.simpleMessage("還未到開啟時間"),
        "not_yet_verified":
            MessageLookupByLibrary.simpleMessage("人臉辨識尚未驗證，\n請先至個人設定驗證"),
        "notice": MessageLookupByLibrary.simpleMessage("通知"),
        "nowPeekabooStatus": m2,
        "ok_to_use": MessageLookupByLibrary.simpleMessage("確定使用"),
        "open": MessageLookupByLibrary.simpleMessage("開啟"),
        "opening": MessageLookupByLibrary.simpleMessage("開啟中"),
        "opinion": MessageLookupByLibrary.simpleMessage("意見和問題反饋"),
        "opinion_content": MessageLookupByLibrary.simpleMessage("請輸入意見以及問題反饋"),
        "opposite_paid": MessageLookupByLibrary.simpleMessage("對方支付"),
        "order_is_being_created":
            MessageLookupByLibrary.simpleMessage("訂單建立中..."),
        "overdue_automatic_cancellation":
            MessageLookupByLibrary.simpleMessage("逾期自動取消"),
        "paid_mode": MessageLookupByLibrary.simpleMessage("支付方式"),
        "paymentType": m3,
        "payment_amount": MessageLookupByLibrary.simpleMessage("支付金額"),
        "payment_method": MessageLookupByLibrary.simpleMessage("付款方式"),
        "payment_status": MessageLookupByLibrary.simpleMessage("付款狀態"),
        "peekaboo": MessageLookupByLibrary.simpleMessage("躲貓貓"),
        "peekaboo_content": MessageLookupByLibrary.simpleMessage("緣分不再是不期而遇。"),
        "phone_verification": MessageLookupByLibrary.simpleMessage("手機驗證"),
        "photo_comparison": MessageLookupByLibrary.simpleMessage("照片比對中"),
        "photo_selection": MessageLookupByLibrary.simpleMessage("相片選擇"),
        "photograph": MessageLookupByLibrary.simpleMessage("拍照"),
        "pig_eating": MessageLookupByLibrary.simpleMessage("餵食中..."),
        "please_add_value": MessageLookupByLibrary.simpleMessage("點數不足，請加值"),
        "please_close_status":
            MessageLookupByLibrary.simpleMessage("請先關閉被獵取狀態"),
        "please_enter_address": MessageLookupByLibrary.simpleMessage("請輸入地址"),
        "please_enter_ashera_id":
            MessageLookupByLibrary.simpleMessage("請輸入Ashera ID"),
        "please_enter_your_commit":
            MessageLookupByLibrary.simpleMessage("請輸入您的評語"),
        "please_log_in_to_the_app_to_complete_the_verification":
            MessageLookupByLibrary.simpleMessage("請登入app後完成驗證"),
        "please_select_an_identifying_photo":
            MessageLookupByLibrary.simpleMessage("請選擇辨識照片"),
        "please_upload_one": MessageLookupByLibrary.simpleMessage("請上傳一張"),
        "point": MessageLookupByLibrary.simpleMessage("點數"),
        "point_of_sale": MessageLookupByLibrary.simpleMessage("販售點數"),
        "poop": MessageLookupByLibrary.simpleMessage("大便"),
        "profession": MessageLookupByLibrary.simpleMessage("職業"),
        "profile_picture": MessageLookupByLibrary.simpleMessage("個人頭像"),
        "purchasing": MessageLookupByLibrary.simpleMessage("購買中..."),
        "question_type": MessageLookupByLibrary.simpleMessage("問題類型"),
        "rating_bar": MessageLookupByLibrary.simpleMessage("評分欄"),
        "received_an_invitation_to_visit":
            MessageLookupByLibrary.simpleMessage("收到一則探班邀請"),
        "recharge": MessageLookupByLibrary.simpleMessage("充值"),
        "recharge_failed": MessageLookupByLibrary.simpleMessage("充值失敗"),
        "recharge_failed_please_recharge":
            MessageLookupByLibrary.simpleMessage("充值失敗，請重新充值"),
        "recharge_record": MessageLookupByLibrary.simpleMessage("充值記錄"),
        "recognition_failed":
            MessageLookupByLibrary.simpleMessage("辨識失敗，請選擇要加的好友"),
        "record": MessageLookupByLibrary.simpleMessage("紀錄"),
        "record_points": MessageLookupByLibrary.simpleMessage("點    數"),
        "refuse": MessageLookupByLibrary.simpleMessage("拒絕"),
        "register": MessageLookupByLibrary.simpleMessage("註冊"),
        "registering": MessageLookupByLibrary.simpleMessage("註冊中"),
        "registration_is_complete_face_recognition_has_not_been_completed":
            MessageLookupByLibrary.simpleMessage("已註冊完成"),
        "registration_success": MessageLookupByLibrary.simpleMessage("註冊成功!"),
        "release_requirements": MessageLookupByLibrary.simpleMessage("發佈需求"),
        "renew": MessageLookupByLibrary.simpleMessage("更新"),
        "report": MessageLookupByLibrary.simpleMessage("檢舉"),
        "review": MessageLookupByLibrary.simpleMessage("審核中"),
        "reviewed": MessageLookupByLibrary.simpleMessage("您已評價過此探班"),
        "sales_record": MessageLookupByLibrary.simpleMessage("販售記錄"),
        "save": MessageLookupByLibrary.simpleMessage("儲存"),
        "saw_mine": MessageLookupByLibrary.simpleMessage("看過我的"),
        "scan_qr": MessageLookupByLibrary.simpleMessage("掃描QRCode"),
        "scan_qr_code_add_me":
            MessageLookupByLibrary.simpleMessage("請掃描QR CODE，加我好友"),
        "scan_the_QR_CODE": MessageLookupByLibrary.simpleMessage("請掃描QR CODE"),
        "scan_your_face_for_identification":
            MessageLookupByLibrary.simpleMessage("請掃描臉部，以便進行辨識"),
        "screening_condition": MessageLookupByLibrary.simpleMessage("篩選條件"),
        "search": MessageLookupByLibrary.simpleMessage("搜尋"),
        "secret": MessageLookupByLibrary.simpleMessage("秘密"),
        "seek": MessageLookupByLibrary.simpleMessage("尋找"),
        "select_a_birthday": MessageLookupByLibrary.simpleMessage("請選擇生日"),
        "select_gender": MessageLookupByLibrary.simpleMessage("請選擇性別"),
        "select_question_type": MessageLookupByLibrary.simpleMessage("請選擇問題類型"),
        "self_paid": MessageLookupByLibrary.simpleMessage("己方支付"),
        "sell": MessageLookupByLibrary.simpleMessage("販售"),
        "send": MessageLookupByLibrary.simpleMessage("送出"),
        "send_commit": MessageLookupByLibrary.simpleMessage("送出評價"),
        "send_verification_code": MessageLookupByLibrary.simpleMessage("發送驗證碼"),
        "sent_a_picture": MessageLookupByLibrary.simpleMessage("傳送了圖片"),
        "sent_a_video": MessageLookupByLibrary.simpleMessage("傳送了影片"),
        "serial_number": MessageLookupByLibrary.simpleMessage("序號"),
        "setting": MessageLookupByLibrary.simpleMessage("設定"),
        "show_mobile_barcode": MessageLookupByLibrary.simpleMessage("顯示行動條碼"),
        "sign_out": MessageLookupByLibrary.simpleMessage("登出"),
        "skip": MessageLookupByLibrary.simpleMessage("略過"),
        "smile_percent": MessageLookupByLibrary.simpleMessage("微笑度"),
        "star_rating": MessageLookupByLibrary.simpleMessage("星級評價"),
        "start_the_game": MessageLookupByLibrary.simpleMessage("開始遊戲"),
        "successful_recharge": MessageLookupByLibrary.simpleMessage("充值成功"),
        "sure": MessageLookupByLibrary.simpleMessage("確定"),
        "table": MessageLookupByLibrary.simpleMessage("桌子"),
        "the_limit_has_been_reached":
            MessageLookupByLibrary.simpleMessage("次數已達上限"),
        "the_opponent_has_not_rated_yet":
            MessageLookupByLibrary.simpleMessage("對方尚未評分"),
        "the_other_party_does_not_agree_to_your_visit":
            MessageLookupByLibrary.simpleMessage("對方不同意你探班"),
        "the_room_is_being_arranged":
            MessageLookupByLibrary.simpleMessage("房間佈置中"),
        "the_visit_has_been_completed":
            MessageLookupByLibrary.simpleMessage("探班已完成"),
        "the_word_count_exceeds_the_limit_of_50_characters":
            MessageLookupByLibrary.simpleMessage("字數超過限制50字"),
        "there_are_currently_no_records":
            MessageLookupByLibrary.simpleMessage("目前沒有紀錄"),
        "there_is_currently_no_chat_history":
            MessageLookupByLibrary.simpleMessage("目前沒有聊天紀錄"),
        "this_week": MessageLookupByLibrary.simpleMessage("週次數"),
        "those_who_have_been_visited_have_not_completed_the_class":
            MessageLookupByLibrary.simpleMessage("有被探班未完成，不可探班"),
        "time_left": MessageLookupByLibrary.simpleMessage("剩餘時間"),
        "timed_out": MessageLookupByLibrary.simpleMessage("已逾時"),
        "today_be_gotcha": MessageLookupByLibrary.simpleMessage("今天已被獵取"),
        "today_gotcha": MessageLookupByLibrary.simpleMessage("今天已獵取"),
        "top_up_amount": MessageLookupByLibrary.simpleMessage("加值點數"),
        "total": MessageLookupByLibrary.simpleMessage("總次數"),
        "transaction_date": MessageLookupByLibrary.simpleMessage("交易日期"),
        "transaction_number": MessageLookupByLibrary.simpleMessage("交易編號"),
        "unblock": MessageLookupByLibrary.simpleMessage("解鎖"),
        "unblockNeedPoint": m4,
        "unblock_member_mugshot":
            MessageLookupByLibrary.simpleMessage("是否解鎖大頭照"),
        "unsubscribe": MessageLookupByLibrary.simpleMessage("取消訂閱"),
        "unverified": MessageLookupByLibrary.simpleMessage("未驗證"),
        "updating_information": MessageLookupByLibrary.simpleMessage("資料更新中"),
        "upgrade_VIP_membership":
            MessageLookupByLibrary.simpleMessage("升級VIP會員"),
        "upgrade_vip": MessageLookupByLibrary.simpleMessage("升級VIP"),
        "upload_file_too_large": MessageLookupByLibrary.simpleMessage("上傳檔案過大"),
        "uploading": MessageLookupByLibrary.simpleMessage("上傳中..."),
        "uploading_comparison": MessageLookupByLibrary.simpleMessage("上傳比對中"),
        "user_agreement": MessageLookupByLibrary.simpleMessage("用戶約定條款"),
        "verified": MessageLookupByLibrary.simpleMessage("已驗證"),
        "verifying": MessageLookupByLibrary.simpleMessage("驗證中...."),
        "video": MessageLookupByLibrary.simpleMessage("影片"),
        "vip_member": MessageLookupByLibrary.simpleMessage("VIP會員"),
        "visit": MessageLookupByLibrary.simpleMessage("探班"),
        "visit_content": MessageLookupByLibrary.simpleMessage("讓一樣事物擁有無限可能。"),
        "visited": MessageLookupByLibrary.simpleMessage("被探班"),
        "visiting_class_notice": MessageLookupByLibrary.simpleMessage("探班通知"),
        "wait_other_side_cancel_visit":
            MessageLookupByLibrary.simpleMessage("等待對方同意取消探班"),
        "wait_time": MessageLookupByLibrary.simpleMessage("等待時間"),
        "waiting": MessageLookupByLibrary.simpleMessage("等待中"),
        "waiting_add_friends": MessageLookupByLibrary.simpleMessage("待加好友"),
        "waiting_add_friends_confirm":
            MessageLookupByLibrary.simpleMessage("待好友確認"),
        "wallpaper": MessageLookupByLibrary.simpleMessage("壁紙"),
        "weight": MessageLookupByLibrary.simpleMessage("體重"),
        "wordLimit": MessageLookupByLibrary.simpleMessage("字數不可大於"),
        "you_can_post_your_requirements_here":
            MessageLookupByLibrary.simpleMessage("您可在此發佈需求"),
        "you_have_been_caught": MessageLookupByLibrary.simpleMessage("你已被獵取"),
        "you_have_had_a_good_time_at_this_member_house":
            MessageLookupByLibrary.simpleMessage("您已在此會員家大過便了"),
        "yuan": MessageLookupByLibrary.simpleMessage("元")
      };
}
