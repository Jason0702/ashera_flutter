import 'package:ashera_flutter/db/app_db.dart';
import 'package:ashera_flutter/routers/delegate.dart';
import 'package:ashera_flutter/utils/firebase_message.dart';
import 'package:ashera_flutter/utils/util.dart';
import 'package:event_bus/event_bus.dart';

import 'enum/app_enum.dart';

AsheraRouterDelegate delegate = AsheraRouterDelegate();
AppDb appDb = AppDb();
MenuStatus friendOrMenuStatus = MenuStatus.none;
Util util = Util();
EventBus visitBus = EventBus();//廣播
EventBus recharge = EventBus();//充值通道廣播
EventBus friend = EventBus();//好友廣播
EventBus memberUpgrade = EventBus();//用戶升級廣播
EventBus gameShopping = EventBus();//家具購買廣播
EventBus checkPetsStatus = EventBus();//寵物狀態
FirebaseMessage message = FirebaseMessage();