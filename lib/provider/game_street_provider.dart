import 'dart:convert';
import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:dio_http_package/dio_http_package.dart';
import 'package:flutter/material.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';

import '../models/game_model.dart';
import '../models/house_model.dart';
import '../models/member_model.dart';
import '../utils/api.dart';
import '../utils/app_image.dart';
import '../utils/parse_json.dart';
import '../utils/util.dart';

class GameStreetProvider with ChangeNotifier{
  final DefaultCacheManager _manager = DefaultCacheManager();

  //附近會員
  List<MemberModel> _nearMeList = [];
  List<MemberModel> get nearMeList => _nearMeList;

  //遊戲設定
  MemberGameSettingModel? _memberGameSetting;
  MemberGameSettingModel get memberGameSetting => _memberGameSetting!;

  ///用memberId取得會員遊戲設定
  /// * [id] 會員ID
  Future<void> getMemberGameSetting({required int id}) async {
    try {
      dynamic response = await HttpUtils.get(
        '${Api.memberGameSetting}/byMemberId/$id',
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      //log('用memberId取得會員遊戲設定: $response');
      _memberGameSetting =
          MemberGameSettingModel.fromJson(json.decode(json.encode(response)));
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      debugPrint('Error: ${_map['message']}');
    }
  }

  ///設定性別
  /// * [gender] -1 不限 0男 1女
  void setMemberGameSettingGender({required int gender}) {
    _memberGameSetting!.filterGender = gender;
  }

  ///設定範圍
  /// * [range] 範圍
  void setMemberGameSettingRange({required RangeValues range}) {
    _memberGameSetting!.olderThenAge = range.start.ceil();
    _memberGameSetting!.youngerThenAge = range.end.ceil();
  }

  ///設定距離
  /// * [] 距離
  void setMemberGameSettingDistance({required int distance}) {
    _memberGameSetting!.filterDistance = distance;
  }

  void setHome(int id, List<HouseModel> house){
    log('房屋刷新');
    getStreetMemberHouseByMemberId(id: id, house: house);
    notifyListeners();
  }

  ///更新設定值
  /// * [id] 會員ID
  Future<void> setMemberSetting({required int id}) async {
    try {
      dynamic response = await HttpUtils.put(
        '${Api.memberGameSetting}/my/$id',
        data: _memberGameSetting!.toJson(),
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      //log('更新遊戲設定值: $response');
      _memberGameSetting =
          MemberGameSettingModel.fromJson(json.decode(json.encode(response)));
      //log('刷新 setMemberSetting');
      notifyListeners();
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      debugPrint('更新遊戲設定值Error: ${_map['message']}');
    }
  }

  ///取附近會員資料
  Future<void> getNearMe({required int id, required member}) async {
    try {
      dynamic response = await HttpUtils.get(
        '${Api.memberFaceImage}/nearMeForGame/$id',
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      //log('附近會員資料: $response');
      List<dynamic> _list = json.decode(json.encode(response));
      _nearMeList = _list
          .map((e) => MemberModel.fromJson(e))
          .where((element) => element.initGame == 1)
          .toList();
      if(_nearMeList.where((element) => element.id == member.id).isEmpty){
        _nearMeList.insert(0, member);
      }
      _nearMeList.toSet();
      //log('刷新 getNearMe');
      notifyListeners();
    } on DioError catch (e) {
      //log('附近會員資料Error: ${e.response}');
    }
  }

  ///街道取得會員的家
  ///* [id] 會員ID
  Future<String> getStreetMemberHouseByMemberId({required int id, required List<HouseModel> house}) async {
    try {
      dynamic response = await HttpUtils.get(
        '${Api.memberHomeStyle}/byMemberId/$id',
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      log('街道取得會員的家: $response');
      List<MemberHouseModel> _memberHouse = parseMemberHouseModel(json.encode(response));
      MemberHouseModel _model =
      _memberHouse.firstWhere((element) => element.used == true);
      String _pic = house.firstWhere((element) => element.id == _model.houseStyleId).pic!;

      return await getImage(
          url: Util().getMemberMugshotUrl(_pic),);
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      debugPrint('Error: ${_map['message']}');
      return '';
    }
  }

  ///街道煙霧
  /// * [id] 家ID
  Future<String> getStreetMemberHousePoopByMemberHouseId({required int id}) async {
    try {
      dynamic response = await HttpUtils.get(
        '${Api.memberHome}/byMemberId/$id',
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      //log('街道煙霧: $response');
      List<MemberHouseModel> _memberHouse =
      parseMemberHouseModel(json.encode(response));
      MemberHouseModel _thisPeopleModel = _memberHouse.firstWhere((element) => element.used == true);
      return await _returnStreet(id: _thisPeopleModel.id!);
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      debugPrint('Error: ${_map['message']}');
      return '';
    }
  }

  Future<String> _returnStreet({required int id}) async {
    try {
      dynamic _response = await HttpUtils.get(
        '${Api.houseDung}/byMemberHouseId/$id',
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      //log('街道煙霧: $_response');
      List<dynamic> _list = json.decode(json.encode(_response));
      if(_list.length >= 5){
        return AppImage.imgSmoke3;
      } else if (_list.length >= 4) {
        return AppImage.imgSmoke2;
      } else if (_list.length >= 3) {
        return AppImage.imgSmoke1;
      } else if (_list.length >= 2) {
        return AppImage.imgSmoke;
      } else {
        return '';
      }
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      debugPrint('Error: ${_map['message']}');
      return '';
    }
  }

  ///取得暫存是否有這張圖
  /// * [url] 圖片網址
  Future<String> getImage({required String url}) async {
    FileInfo? _cacheFile = await _manager.getFileFromCache(url);
    if (_cacheFile != null) {
      return _cacheFile.file.path;
    } else {
      return url;
    }
  }
}