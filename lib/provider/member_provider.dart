import 'dart:convert';
import 'dart:developer';

import 'package:ashera_flutter/enum/app_enum.dart';
import 'package:ashera_flutter/models/member_model.dart';
import 'package:dio/dio.dart';
import 'package:dio_http_package/dio_http_package.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import '../models/member_point_model.dart';
import '../models/recharge_record_model.dart';
import '../utils/api.dart';
import '../utils/shared_preference.dart';

class MemberProvider with ChangeNotifier{
  SharedPreferenceUtil sharedPreferenceUtil = SharedPreferenceUtil();
  MeModel? _memberModel;
  MeModel get memberModel => _memberModel!;
  MeModel? _copyMemberModel;
  MemberModel? _memberModelMe;
  MemberModel get memberModelMe => _memberModelMe!;

  MemberPointModel? _memberPoint;
  MemberPointModel get memberPoint => _memberPoint!;

  List<RechargeRecordModel> _memberPointHistory = [];
  List<RechargeRecordModel> get memberPointHistory => _memberPointHistory;

  bool? _hideBirthday;
  bool get hideBirthday => _hideBirthday!;

  bool? _hideWeight;
  bool get hideWeight => _hideWeight!;

  bool? _hideJob;
  bool get hideJob => _hideJob!;

  ApiStatus _status = ApiStatus.idle;
  ApiStatus get status => _status;

  int interactiveAvatarVer = 0;

  MemberModel? _customerService;
  MemberModel? get customerService => _customerService;

  void setInteractiveAvatarVer(){
    interactiveAvatarVer++;
    notifyListeners();
  }
  

  //取得
  Future<bool> getMember(int _id) async{
    bool _result = false;
    _status = ApiStatus.inExecution;
    try{
      dynamic response = await HttpUtils.get(
        '${Api.member}/id/$_id',
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer " + await sharedPreferenceUtil.getAccessToken()}),
      );
      log('取得會員資料response: $response');
      await getMemberPoint(_id);//取得會員點數
      _memberModel = MeModel.fromJson(json.decode(json.encode(response)));
      _memberModelMe = MemberModel.fromJson(json.decode(json.encode(response)));
      _hideBirthday = _memberModel!.hideColumn!.where((element) => element == HideColumn.birthday.name).isNotEmpty ? true : false;
      _hideWeight = _memberModel!.hideColumn!.where((element) => element == HideColumn.weight.name).isNotEmpty ? true : false;
      _hideJob  = _memberModel!.hideColumn!.where((element) => element == HideColumn.job.name).isNotEmpty ? true : false;
      _result = true;
      notifyListeners();
    } on DioError catch (e) {
      //log('取得會員資料Error: ${e.response!.statusCode}');
      _result = false;
      _status = ApiStatus.idle;
    }
    return _result;
  }
  //待加好友資料
  Future<MeModel?> getMemberById({required int id}) async {
    try{
      dynamic response = await HttpUtils.get(
        '${Api.member}/id/$id',
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer " + await sharedPreferenceUtil.getAccessToken()}),
      );
      return MeModel.fromJson(json.decode(json.encode(response)));
    } on DioError catch (e){
      //log('待加好友資料Error: ${e.response!.statusCode}');
      return null;
    }
  }
  //跳轉房間用
  Future<MemberModel?> getMemberByIdToMemberModel({required int id}) async {
    try{
      dynamic response = await HttpUtils.get(
        '${Api.member}/id/$id',
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer " + await sharedPreferenceUtil.getAccessToken()}),
      );
      return MemberModel.fromJson(json.decode(json.encode(response)));
    } on DioError catch (e){
      //log('待加好友資料Error: ${e.response!.statusCode}');
      return null;
    }
  }
  /*//更新隱藏生日
  Future<Map<String, dynamic>> putHideBirthday(bool value) async {
    log('更新隱藏生日: ${Api.member}/${_memberModel!.id!}/hideBirthday/$value $value');
    _status = ApiStatus.inExecution;
    Map<String, dynamic> _map = {
      'hideBirthday': value
    };
    try{
      dynamic response = await HttpUtils.put(
        '${Api.member}/${_memberModel!.id!}/hideBirthday/$value',
        data: _map,
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer " + await sharedPreferenceUtil.getAccessToken()}),
      );
      log('更新隱藏生日 $response');
      _hideBirthday = value;
      _status = ApiStatus.idle;
      notifyListeners();
      return {'status': true, 'value': value};
    } on DioError catch(e) {
      log('更新隱藏生日Error: ${e.response!.statusCode}');
      _hideBirthday = !value;
      notifyListeners();
      _status = ApiStatus.idle;
      return {'status': false, 'value': !value};
    }
  }*/

  //更新
  Future<bool> putMember(int _id) async {
    bool _result = false;
    _status = ApiStatus.inExecution;
    Map<String, dynamic> _map = {
      'name': _memberModel!.name,
      'cellphone': _memberModel!.name,
      'nickname': _memberModel!.nickname,
      'gender': _memberModel!.gender,
      'height': _memberModel!.height,
      'weight': _memberModel!.weight,
      'birthday': _memberModel!.birthday,
      'mugshot': _memberModel!.mugshot?.split('?').first,
      'zodiacSign': _memberModel!.zodiacSign,
      'bloodType': _memberModel!.bloodType,
      'aboutMe': _memberModel!.aboutMe,
      'aboutMe2': _memberModel!.aboutMe2,
      'job': _memberModel!.job,
      'hideColumn': json.encode(_memberModel!.hideColumn)
    };
    //log('上傳參數: $_map');
    try{
      dynamic response = await HttpUtils.put(
        '${Api.member}/$_id',
        data: _map,
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer " + await sharedPreferenceUtil.getAccessToken()}),
      );
      //log('取得會員資料response: $response');
      _memberModel = MeModel.fromJson(json.decode(json.encode(response)));
      _result = true;
      _status = ApiStatus.idle;
    } on DioError catch (e) {
      //log('更新會員資料Error: ${e.response!.statusCode}');
      _result = false;
      _status = ApiStatus.idle;
    }
    return _result;
  }
  ///更新年齡
  Future<bool> putAge({required int age}) async {
    bool _result = false;
    _status = ApiStatus.inExecution;
    Map<String, dynamic> _map = {
      'age': age
    };
    try{
      dynamic response = await HttpUtils.put(
        '${Api.member}/${memberModel.id!}/age/$age',
        data: _map,
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer " + await sharedPreferenceUtil.getAccessToken()}),
      );
      //log('更新年齡: $response');
      _result = true;
      _status = ApiStatus.idle;
    } on DioError catch (e) {
      //log('更新年齡Error: ${e.response!.statusCode}');
      _result = false;
      _status = ApiStatus.idle;
    }
    return _result;
  }

  //修改密碼
  Future<bool> putMemberPassword(int _id, String _newPassword, String _oldPassword) async {
    bool _result = false;
    _status = ApiStatus.inExecution;
    Map<String, dynamic> _map = {
      'newPassword': _newPassword,
      'oldPassword': _oldPassword
    };
    //log('上傳參數: $_map');
    try{
      dynamic response = await HttpUtils.put(
        '${Api.member}/$_id/password',
        data: _map,
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer " + await sharedPreferenceUtil.getAccessToken()}),
      );
      //log('修改密碼: $response');
      _result = true;
      _status = ApiStatus.idle;
    } on DioError catch (e) {
      //log('修改密碼Error: ${e.response!.data}');
      EasyLoading.showToast(e.response!.data['message']!);
      _result = false;
      _status = ApiStatus.idle;
    }
    return _result;
  }

  //會員點數
  Future<bool> getMemberPoint(int _id) async {
    bool _result = false;
    try{
      dynamic response = await HttpUtils.get(
        '${Api.memberPoint}/my/$_id',
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer " + await sharedPreferenceUtil.getAccessToken()}),
      );
      log('會員點數: $response');
      _memberPoint = MemberPointModel.fromJson(json.decode(json.encode(response)));
      _result = true;
      _status = ApiStatus.idle;
      notifyListeners();
    } on DioError catch (e){
      //log('會員點數Error: ${e.response!.data}');
      _result = false;
      _status = ApiStatus.idle;
    }
    return _result;
  }

  //會員加值紀錄
  Future<bool> getMemberPointHistory() async {
    bool _result = false;
    _status = ApiStatus.inExecution;
    try{
      dynamic response = await HttpUtils.get(
        '${Api.addPointHistory}/byMemberId/${_memberModel!.id}',
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer " + await sharedPreferenceUtil.getAccessToken()}),
      );
      //log('會員加值紀錄: $response');
      final List list = json.decode(json.encode(response));
      _memberPointHistory = list.map((e) => RechargeRecordModel.fromJson(e)).toList();
      _memberPointHistory.sort((left, right) => right.createdAt!.compareTo(left.createdAt!));
      _result = true;
      _status = ApiStatus.idle;
      notifyListeners();
    } on DioError catch(e) {
      //log('會員點數Error: ${e.response!.data}');
      _result = false;
      _status = ApiStatus.idle;
    }
    return _result;
  }
  //會員取消訂閱
  Future<bool> vipUnSubscription() async {
    bool _result = false;
    try{
      dynamic response = await HttpUtils.put(
        '${Api.vipUnSubscription}/${memberModel.id}/vipUnSubscription',
        data: {'id': memberModel.id},
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer " + await sharedPreferenceUtil.getAccessToken()}),
      );
      log('會員取消訂閱: $response');
      if(response){
        try{
          dynamic response = await HttpUtils.get(
            '${Api.member}/id/${memberModel.id}',
            options: Options(contentType: 'application/json', headers: {"authorization": "Bearer " + await sharedPreferenceUtil.getAccessToken()}),
          );
          log('取得會員資料response: $response');
          _memberModel = MeModel.fromJson(json.decode(json.encode(response)));
          notifyListeners();
        } on DioError catch (e) {
          log('取得會員資料 Error: ${e.response}');
        }
      }
      _result = true;
      notifyListeners();
    } on DioError catch(e){
      log('會員取消訂閱Error: ${e.response!.data}');
      _result = false;
    }
    return _result;
  }

  //複製
  void copyMemberModel(){
    _copyMemberModel = MeModel.fromJson(_memberModel!.toJson());
  }

  //還原
  void reductionMemberModel(){
    _memberModel = MeModel.fromJson(_copyMemberModel!.toJson());
    notifyListeners();
  }

  //更變細項
  //大頭照
  void changeMugshot(String _mugshot){
    _memberModel!.mugshot = _mugshot;
    notifyListeners();
  }
  //暱稱
  void changeNickname(String _nickName){
    _memberModel!.nickname = _nickName;
    notifyListeners();
  }
  //性別
  void changeGender(int _gender){
    _memberModel!.gender = _gender;
    notifyListeners();
  }
  //身高
  void changeHeight(double _height){
    _memberModel!.height = _height;
    notifyListeners();
  }
  //體重
  void changeWeight(double _weight){
    _memberModel!.weight = _weight;
    notifyListeners();
  }
  //生日
  void changeBirthday(String _birthday){
    _memberModel!.birthday = _birthday;
    notifyListeners();
  }
  //星座
  void changeZodiacSign(String _zodiacSign){
    _memberModel!.zodiacSign = _zodiacSign;
    notifyListeners();
  }
  //血型
  void changeBloodType(String _bloodType){
    _memberModel!.bloodType = _bloodType;
    notifyListeners();
  }
  //關於我
  void changeAboutMe(String _aboutMe){
    _memberModel!.aboutMe = _aboutMe;
    notifyListeners();
  }
  void changeBusinessCard(String _businessCard){
    _memberModel!.aboutMe2 = _businessCard;
    notifyListeners();
  }
  //職位
  void changeJob(String _job){
    _memberModel!.job = _job;
    notifyListeners();
  }
  //隱藏生日
  void changeHideBirthday(bool value){
    if(value){
      _memberModel!.hideColumn!.add(HideColumn.birthday.name);
    } else {
      _memberModel!.hideColumn!.remove(HideColumn.birthday.name);
    }
    _hideBirthday = value;
    notifyListeners();
  }
  //隱藏體重
  void changeHideWeight(bool value){
    if(value){
      _memberModel!.hideColumn!.add(HideColumn.weight.name);
    } else {
      _memberModel!.hideColumn!.remove(HideColumn.weight.name);
    }
    _hideWeight = value;
    notifyListeners();
  }
  //隱藏職業
  void changeHideJob(bool value){
    if(value){
      _memberModel!.hideColumn!.add(HideColumn.job.name);
    } else {
      _memberModel!.hideColumn!.remove(HideColumn.job.name);
    }
    _hideJob = value;
    notifyListeners();
  }

  //取得客服
  void getCustomerService(int _id) async {
    try{
      dynamic response = await HttpUtils.get(
        '${Api.member}/id/$_id',
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer " + await sharedPreferenceUtil.getAccessToken()}),
      );
      log('取得客服資料response: $response');
      _customerService = MemberModel.fromJson(json.decode(json.encode(response)));
      notifyListeners();
    } on DioError catch (e) {
      log('取得客服資料Error: ${e.response!.statusCode}');
    }
  }
}