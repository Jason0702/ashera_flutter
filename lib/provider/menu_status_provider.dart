import 'package:flutter/cupertino.dart';

import '../enum/app_enum.dart';

class MenuStatusProvider with ChangeNotifier{
  MenuStatus menuStatus = MenuStatus.none;
  void openMenu(){
    menuStatus = MenuStatus.menu;
    notifyListeners();
  }
  void closeMenu(){
    menuStatus = MenuStatus.none;
    notifyListeners();
  }
}