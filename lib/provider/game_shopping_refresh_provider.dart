import 'package:ashera_flutter/enum/app_enum.dart';
import 'package:flutter/cupertino.dart';

class GameShoppingRefreshProvider with ChangeNotifier{
  //狀態是?
  GameRefreshStatus _refreshStatus = GameRefreshStatus.shopping;
  GameRefreshStatus get refreshStatus => _refreshStatus;

  //物品是?
  StuffItem _stuffItem = StuffItem.none;
  StuffItem get stuffItem => _stuffItem;

  //更換物品種類
  List<StuffItem> _stuffItemList = [];
  List<StuffItem> get stuffItemList => _stuffItemList;

  //設定狀態
  void setRefreshStatus({required GameRefreshStatus status}){
    _refreshStatus = status;
  }

  //設定購買物品
  void setStuffItem({required StuffItem item}){
    _stuffItem = item;
  }

  //設定更換物品種類
  void setStuffItemList({required StuffItem item}){
    _stuffItemList.add(item);
  }
  //清除更換物品種類
  void deleteStuffItemList(){
    _stuffItemList.clear();
  }
}