import 'dart:async';
import 'dart:convert';
import 'dart:developer';

import 'package:ashera_flutter/utils/shared_preference.dart';
import 'package:dio/dio.dart';
import 'package:dio_http_package/dio_http_package.dart';
import 'package:flutter/material.dart';
import '../enum/app_enum.dart';
import '../models/member_mugshot_model.dart';
import '../utils/api.dart';
import '../utils/app_color.dart';

class VisitGameProvider with ChangeNotifier{
  SharedPreferenceUtil sharedPreferenceUtil = SharedPreferenceUtil();
  VisitMode userStatus = VisitMode.visitModeVisit;
  WhoPay paidStatus = WhoPay.all;
  WhoPay whoWantsToPay = WhoPay.whoPaySelf;
  GenderStatus genderStatus= GenderStatus.girl;
  VisitStatus visitStatus = VisitStatus.pairing;

  Timer? _timer;
  int _sec = 20;
  int get sec => _sec;


  int _distance = 6;
  int get distance => _distance;

  int _minAge = 18;
  int get minAge => _minAge;

  int _maxAge = 100;
  int get maxAge => _maxAge;

  void changeUserStatus(VisitMode change){
    userStatus = change;
    notifyListeners();
  }
  void changePaidStatus(WhoPay change){
    paidStatus = change;
    notifyListeners();
  }

  void changeVisitStatus(VisitStatus change){
    visitStatus = change;
    notifyListeners();
  }

  void changeSeekGenderStatus(GenderStatus change){
    genderStatus = change;
    notifyListeners();
  }
  //想要誰付
  void setWhoWantsToPay(WhoPay change){
    whoWantsToPay = change;
    notifyListeners();
  }

  //判斷paidStatus回傳邊距
  EdgeInsets paidStatusMargin(WhoPay nowStatus,WhoPay set) {
    if (nowStatus == set) {
      return const EdgeInsets.all(3);
    } else {
      return const EdgeInsets.all(0);
    }
  }

  //判斷paidStatus回傳文字顏色
  Color paidStatusTextColor(WhoPay nowStatus,WhoPay set) {
    if (nowStatus == set) {
      return AppColor.buttonTextColor;
    } else {
      return AppColor.grayText;
    }
  }

  //判斷paidStatus回傳按鈕顏色
  Color paidStatusButtonColor(WhoPay nowStatus,WhoPay set) {
    if (nowStatus == set) {
      return AppColor.yellowButton;
    } else {
      return Colors.transparent;
    }
  }

  //判斷UserStatus回傳邊距
  EdgeInsets userStatusMargin(VisitMode set) {
    if (userStatus == set) {
      return const EdgeInsets.all(3);
    } else {
      return const EdgeInsets.all(0);
    }
  }

  //判斷UserStatus回傳文字顏色
  Color userStatusTextColor(VisitMode set) {
    if ( userStatus == set) {
      return AppColor.buttonTextColor;
    } else {
      return AppColor.grayText;
    }
  }

  //判斷UserStatus回傳按鈕顏色
  Color userStatusButtonColor(VisitMode set) {
    if ( userStatus == set) {
      return AppColor.yellowButton;
    } else {
      return Colors.transparent;
    }
  }

  //判斷seekGenderStatus回傳邊距
  EdgeInsets seekGenderStatusMargin(GenderStatus set) {
    if (genderStatus == set) {
      return const EdgeInsets.all(3);
    } else {
      return const EdgeInsets.all(0);
    }
  }

  //判斷seekGenderStatus回傳文字顏色
  Color seekGenderStatusTextColor(GenderStatus set) {
    if ( genderStatus == set) {
      return AppColor.buttonTextColor;
    } else {
      return AppColor.grayText;
    }
  }

  //判斷seekGenderStatus回傳按鈕顏色
  Color seekGenderStatusButtonColor(GenderStatus set) {
    if ( genderStatus == set) {
      if(genderStatus == GenderStatus.girl){
        return AppColor.femaleColor;
      }
      return AppColor.maleColor;
    } else {
      return Colors.transparent;
    }
  }

  ///設定距離
  /// * [value]
  void setSettingDistance({required int value}){
    _distance = value;
  }

  ///設定年齡
  /// * [min] 最小
  /// * [max] 最大
  void setSettingAge({required int min, required int max}){
    _minAge = min;
    _maxAge = max;
  }

  ///辨識紀錄 用ID取得大頭照路徑
  Future<String> getMugshotMemberById({required int id, required bool interactive}) async {
    String _value = '';
    try {
      dynamic response = await HttpUtils.get(
        '${Api.member}/id/$id',
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer " + await sharedPreferenceUtil.getAccessToken()}),
      );
      //log('用ID取得大頭照路徑: $response');
      MemberMugshotModel _memberMugshot = MemberMugshotModel.fromJson(json.decode(json.encode(response)));
      if(interactive){
        _value = _memberMugshot.interactivePic ?? '';
      } else {
        _value = _memberMugshot.mugshot ?? '';
      }
    } on DioError catch (e) {
      //log('用ID取得大頭照路徑: ${e.response!.statusCode}');
      _value = '';
    }
    return _value;
  }

  //開始計時
  void startTimer() {
    _timer ??= Timer.periodic(const Duration(milliseconds: 950), (timer) {
      if (_sec > 0) {
        _sec--;
        notifyListeners();
      }
    });
  }

  //停止計時
  void stopTimer() {
    _sec = 20;
    if (_timer != null) {
      _timer!.cancel();
      _timer = null;
    }
  }
}