import 'dart:async';
import 'dart:convert';
import 'dart:developer';

import 'package:ashera_flutter/enum/app_enum.dart';
import 'package:ashera_flutter/models/about_us_model.dart';
import 'package:ashera_flutter/models/member_local_model.dart';
import 'package:ashera_flutter/models/peekaboo_record_dto.dart';
import 'package:ashera_flutter/models/peekaboo_record_model.dart';
import 'package:ashera_flutter/models/system_setting_model.dart';
import 'package:ashera_flutter/models/visit_record_dto.dart';
import 'package:ashera_flutter/models/detec_face_dto.dart';
import 'package:ashera_flutter/models/follower_model.dart';
import 'package:ashera_flutter/models/member_model.dart';
import 'package:ashera_flutter/models/visit_record_model.dart';
import 'package:dio/dio.dart';
import 'package:dio_http_package/dio_http_package.dart';
import 'package:flutter/foundation.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:heic_to_jpg/heic_to_jpg.dart';

import '../models/complaint_record_dto.dart';
import '../models/face_image_model.dart';
import '../models/faces_detect_history_model.dart';
import '../models/friend_name_model.dart';
import '../models/game_model.dart';
import '../models/game_setting_model.dart';
import '../models/recharge_record_model.dart';
import '../utils/api.dart';
import '../utils/parse_json.dart';
import '../utils/shared_preference.dart';
import '../utils/util.dart';

class UtilApiProvider with ChangeNotifier {
  //是否可以點擊被探班按鈕
  ButtonStatus _visitButton = ButtonStatus.notClick;
  ButtonStatus get visitButton => _visitButton;

  UpLoadingStatus _upLoadingStatus = UpLoadingStatus.notUpLoading;
  UpLoadingStatus get upLoadingStatus => _upLoadingStatus;

  ButtonStatus _hideAndSeekButtonStatus = ButtonStatus.notClick;
  ButtonStatus get hideAndSeekButtonStatus => _hideAndSeekButtonStatus;

  ButtonStatus _addPointsHistoryButton = ButtonStatus.notClick;

  //搜尋到的使用者
  MeModel? _memberModel;
  MeModel? get memberModel => _memberModel;
  //通知 想加我的
  final List<FollowerModel> _wantAddMeList = [];
  List<FollowerModel> get wantAddMeList => _wantAddMeList;
  //待加好友
  List<FollowerModel> _followerList = [];
  List<FollowerModel> get followerList => _followerList;

  //辨識出來的人
  List<FaceImageModel> _faceImageList = [];
  List<FaceImageModel> get faceImageList => _faceImageList;

  //進行中的探班紀錄
  List<VisitRecordModel> _visitProcessingRecordList = [];
  List<VisitRecordModel> get visitProcessingRecordList =>
      _visitProcessingRecordList;

  //目前進行中的
  VisitRecordModel? _visitRecordModel;
  VisitRecordModel get visitRecordModel => _visitRecordModel!;

  //被探班最後一筆紀錄
  VisitRecordModel? _lastTargetVisitRecord;
  VisitRecordModel? get lastTargetVisitRecord => _lastTargetVisitRecord;
  //探班最後一筆紀錄
  VisitRecordModel? _lastFromVisitRecord;
  VisitRecordModel? get lastFromVisitRecord => _lastFromVisitRecord;

  //探班全部紀錄
  final List<VisitRecordModel> _fromMemberAllVisitRecord = [];
  List<VisitRecordModel> get fromMemberAllVisitRecord =>
      _fromMemberAllVisitRecord;
  //被探班全部紀錄
  final List<VisitRecordModel> _targetMemberAllVisitRecord = [];
  List<VisitRecordModel> get targetMemberAllVisitRecord =>
      _targetMemberAllVisitRecord;

  //探班與被探班全部紀錄
  final List<VisitRecordModel> _allVisitRecord = [];
  List<VisitRecordModel> get allVisitRecord => _allVisitRecord;

  //進行中且未完成與配對中且未完成的訂單
  final List<VisitRecordModel> _makeAndMatchRecord = [];
  List<VisitRecordModel> get makeAndMatchRecord => _makeAndMatchRecord;

  //辨識紀錄
  List<FacesDetectHistoryModel> _facesDetectHistoryRecord = [];
  List<FacesDetectHistoryModel> get facesDetectHistoryRecord =>
      _facesDetectHistoryRecord;

  //星星數量
  double _memberStar = 0.0;
  double get memberStar => _memberStar;

  //對方星星數量
  double _otherStar = 0.0;
  double get otherStar => _otherStar;

  //關於
  AboutUsModel? _aboutUsModel;
  AboutUsModel get aboutUsModel => _aboutUsModel!;

  //躲貓貓 被捕捉 紀錄
  List<PeekabooRecordModel> _peekabooRecordTargetMemberCompleteList =
      []; //已完成紀錄
  List<PeekabooRecordModel> get peekabooRecordTargetMemberCompleteList =>
      _peekabooRecordTargetMemberCompleteList;
  List<PeekabooRecordModel> _peekabooRecordTargetMemberList = []; //全紀錄
  List<PeekabooRecordModel> get peekabooRecordTargetMemberList =>
      _peekabooRecordTargetMemberList;
  //本周獵取紀錄 被捕捉
  List<PeekabooRecordModel> _thisWeekPeekabooRecordTargetMemberCompleteList = [];
  List<PeekabooRecordModel> get thisWeekPeekabooRecordTargetMemberCompleteList =>
      _thisWeekPeekabooRecordTargetMemberCompleteList;
  //本日獵取紀錄 被捕捉
  List<PeekabooRecordModel> _toDayPeekabooRecordTargetMemberCompleteList = [];
  List<PeekabooRecordModel> get toDayPeekabooRecordTargetMemberCompleteList =>
      _toDayPeekabooRecordTargetMemberCompleteList;

  //躲貓貓 捕捉 紀錄
  List<PeekabooRecordModel> _peekabooRecordFromMemberCompleteList = []; //已完成紀錄
  List<PeekabooRecordModel> get peekabooRecordFromMemberCompleteList =>
      _peekabooRecordFromMemberCompleteList;
  List<PeekabooRecordModel> _peekabooRecordFromMemberList = []; //全紀錄
  List<PeekabooRecordModel> get peekabooRecordFromMemberList =>
      _peekabooRecordFromMemberList;
  //本周獵取紀錄 捕捉
  List<PeekabooRecordModel> _thisWeekPeekabooRecordFromMemberCompleteList = [];
  List<PeekabooRecordModel> get thisWeekPeekabooRecordFromMemberCompleteList => _thisWeekPeekabooRecordFromMemberCompleteList;
  //本日獵取紀錄 捕捉
  List<PeekabooRecordModel> _toDayPeekabooRecordFromMemberCompleteList = [];
  List<PeekabooRecordModel> get toDayPeekabooRecordFromMemberCompleteList =>
      _toDayPeekabooRecordFromMemberCompleteList;

  //躲貓貓進行中的人
  List<PeekabooRecordModel> _peekabooRecordProcessingList = [];
  List<PeekabooRecordModel> get peekabooRecordProcessingList =>
      _peekabooRecordProcessingList;

  //躲貓貓 捕捉次數
  int _isCatchCount = 0;
  int get isCatchCount => _isCatchCount;
  //躲貓貓 被捕捉次數
  int _beCaughtCount = 0;
  int get beCaughtCount => _beCaughtCount;

  //掃臉次數
  int _faceScanTime = 0;
  int get faceScanTime => _faceScanTime;

  //捕捉玩家的位置
  List<MemberLocalModel> _memberLocalList = [];
  List<MemberLocalModel> get memberLocalList => _memberLocalList;

  SystemSettingModel? _systemSetting;
  SystemSettingModel? get systemSetting => _systemSetting;

  List<MemberFriendNameModel> _friendNameList = [];
  List<MemberFriendNameModel> get friendNameList => _friendNameList;

  List<CleaningToolsModel> _cleaningToolsList = [];
  List<CleaningToolsModel> get cleaningToolsList => _cleaningToolsList;

  final List<VisitStarRecordModel> _visitStarRecordList = [];
  List<VisitStarRecordModel> get visitStarRecordList => _visitStarRecordList;
  //附近會員
  List<MemberModel> _meDistanceList = [];
  List<MemberModel> get meDistanceList => _meDistanceList;

  GameSettingModel? _gameSetting;
  GameSettingModel get gameSetting => _gameSetting!;

  //上傳 個人大頭照的話 檔名 就以 會員帳號_mugshot.png  暫存圖 存放名稱: <會員帳號>_tmp.png 辨識 照片 命名為 <會員帳號>_face.png 互動照片命名為<會員帳號>_interactive.png
  Future<Map<String, dynamic>> upDataImage(
      String _account, String _path, PhotoType _photoType) async {
    //上傳中
    _upLoadingStatus = UpLoadingStatus.upLoading;
    notifyListeners();

    String _fileName;
    Map<String, dynamic> result = {};
    if (_path.contains('.heic')) {
      //log('path: $_path');
      _fileName =
          '${_account}_${_photoType.name}.${_path.substring(_path.lastIndexOf(".") + 1, _path.length)}';
      _path = (await HeicToJpg.convert(_path).catchError((e) {
        //log('catchError $e');
      }))!;
    } else {
      _fileName =
          '${_account}_${_photoType.name}.${_path.substring(_path.lastIndexOf(".") + 1, _path.length)}';
    }
    FormData data = FormData.fromMap({
      "file": await MultipartFile.fromFile(_path, filename: _fileName)
          .catchError((e) {
        //log('catchError $e');
      })
    });
    //log('fileName: $_fileName');
    //上傳
    try {
      dynamic response = await HttpUtils.upload(
        Api.fileUpload,
        data: data,
        options: Options(contentType: 'application/json', headers: {
          "authorization":
              "Bearer ${Api.accessToken}"
        }),
      );
      //log('上傳成功: $response');
      _upLoadingStatus = UpLoadingStatus.uploadCompleted;
      notifyListeners();
      result = {'status': true, 'filename': _fileName};
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      //log('Error: ${_map['message']}');
      _upLoadingStatus = UpLoadingStatus.uploadFailed;
      notifyListeners();
      result = {'status': false, 'filename': _fileName};
    }
    return result;
  }

  //獲取我的追隨請求者列表
  Future<bool> getRequestMyFollower(int _id) async {
    bool _result = false;
    try {
      dynamic response = await HttpUtils.get(
        '${Api.followerRequest}/myFollower/$_id',
        options: Options(contentType: 'application/json', headers: {
          "authorization":
              "Bearer ${Api.accessToken}"
        }),
      );
      //log('獲取我的追隨請求者列表: $response');
      _followerList = await compute(parseFollower, json.encode(response));
      _result = true;
      notifyListeners();
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      //log('Error: ${_map['message']}');
      _result = false;
    }
    return _result;
  }

  //使用Uid獲取使用者資料
  Future<bool> getUidSearchMember(String _uid) async {
    bool _result = false;
    try {
      dynamic response = await HttpUtils.get(
        '${Api.member}/uid/$_uid',
        options: Options(contentType: 'application/json', headers: {
          "authorization":
              "Bearer ${Api.accessToken}"
        }),
      );
      if (response.toString().isNotEmpty) {
        //log('使用Uid獲取使用者資料: $response');
        _memberModel = MeModel.fromJson(json.decode(json.encode(response)));
        dynamic time = await getLengthFacesDetectHistoryByTargetMemberId(
            id: _memberModel!.id!);
        _faceScanTime = time;
        _result = true;
        notifyListeners();
      }
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      //log('Error: ${_map['message']}');
      _result = false;
    }
    return _result;
  }

  //Uid搜尋的使用者清除
  void setUidSearchMemberClear() {
    _memberModel = null;
    notifyListeners();
  }

  //意見
  Future<bool> postSuggestion(int _id, String _suggest, String _type) async {
    bool _result = false;
    Map<String, dynamic> _map = {
      "memberId": _id,
      "suggest": _suggest,
      "type": _type
    };
    //log('上傳參數: $_map');
    try {
      dynamic response = await HttpUtils.post(
        Api.suggestion,
        data: _map,
        options: Options(contentType: 'application/json', headers: {
          "authorization":
              "Bearer ${Api.accessToken}"
        }),
      );
      //log('意見: $response');
      _result = true;
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      //log('Error: ${_map['message']}');
      _result = false;
    }
    return _result;
  }

  //關於
  Future<bool> getAboutUs() async {
    bool _result = false;
    try {
      dynamic response = await HttpUtils.get(
        Api.aboutUs,
        options: Options(contentType: 'application/json', headers: {
          "authorization":
              "Bearer ${Api.accessToken}"
        }),
      );
      //log('關於: $response');
      _aboutUsModel = AboutUsModel.fromJson(json.decode(json.encode(response)));
      _result = true;
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      //log('Error: ${_map['message']}');
      _result = false;
    }
    return _result;
  }

  //設定大頭照
  Future<bool> setMemberMugshot(int _id, String? _mugshot) async {
    bool _result = false;
    Map<String, dynamic> _map = {'1': 1};
    try {
      dynamic response = await HttpUtils.put(
        '${Api.member}/$_id/mugshot/$_mugshot',
        data: _map,
        options: Options(contentType: 'application/json', headers: {
          "authorization":
              "Bearer ${Api.accessToken}"
        }),
      );
      //log('更新大頭照完成: $response');
      _result = true;
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      //log('Error: ${_map['message']}');
      _result = false;
    }
    return _result;
  }

  //設定互動照片
  Future<bool> setInteractivePic(int _id, String? _interactivePic) async {
    bool _result = false;
    Map<String, dynamic> _map = {'1': 1};
    try {
      dynamic response = await HttpUtils.put(
        '${Api.member}/$_id/interactivePic/$_interactivePic',
        data: _map,
        options: Options(contentType: 'application/json', headers: {
          "authorization":
              "Bearer ${Api.accessToken}"
        }),
      );
      //log('更新互動照片完成: $response');
      _result = true;
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      //log('Error: ${_map['message']}');
      _result = false;
    }
    return _result;
  }

  //判斷帳號是否重複
  Future<bool> memberNameExist(String _account) async {
    bool _result = false;
    try {
      dynamic response = await HttpUtils.get(
        '${Api.isMemberNameExist}$_account',
        options: Options(contentType: 'application/json'),
      );
      //log('判斷帳號是否重複: $response');
      if (response) {
        _result = false;
      } else {
        _result = true;
      }
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      //log('判斷帳號是否重複Error: ${e.response!.realUri} ${_map['message']}');
      _result = false;
    }
    return _result;
  }

  //照片檢測
  Future<bool> rekognitionFaces() async {
    bool _result = false;
    Map<String, dynamic> _map = {
      'sourceImage':
          'https://cdn.bella.tw/index_image/GBF6darQGPM5kb567Y8mZwfeUVOKczvqHFBsJjcE.jpeg',
      'gender': "MALE",
      'local': false
    };
    try {
      dynamic response = await HttpUtils.post(
        Api.rekognitionFaces,
        data: _map,
        options: Options(contentType: 'application/json', headers: {
          "authorization":
              "Bearer ${Api.accessToken}"
        }),
      );
      //log('照片檢測: $response');
      _result = true;
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      //log('照片檢測Error: ${e.response!.realUri} ${_map['message']}');
      _result = false;
    }
    return _result;
  }

  //更新會員辨識圖片
  Future<bool> setMemberFaceImage(int _id, String _fileName) async {
    bool _result = false;
    DetectFaceDTO _detectFace =
        DetectFaceDTO(filePath: '/upload', imageName: _fileName);
    //log('上傳的資料: ${_detectFace.toJson()}');
    try {
      dynamic response = await HttpUtils.put(
        '${Api.member}/$_id/facePic',
        data: _detectFace.toJson(),
        options: Options(contentType: 'application/json', headers: {
          "authorization":
              "Bearer ${Api.accessToken}"
        }),
      );
      //log('更新辨識圖片完成: $response');
      _result = response;
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      //log('辨識圖片Error: ${_map['message']}');
      _result = false;
    }
    return _result;
  }

  //上傳圖片辨識會員
  Future<bool> getMemberByFaceImage(String? _faceImage) async {
    bool _result = false;
    DetectFaceDTO _detectFace =
        DetectFaceDTO(filePath: '/upload', imageName: _faceImage);
    //log('detectFace: ${_detectFace.toJson()} faceImage: $_faceImage');
    try {
      dynamic response = await HttpUtils.post(
        '${Api.memberFaceImage}/searchMemberByPic',
        data: _detectFace.toJson(),
        options: Options(contentType: 'application/json', headers: {
          "authorization":
              "Bearer ${Api.accessToken}"
        }),
      );
      //log('上傳圖片辨識會員完成: $response');
      _faceImageList =
          await compute(parseMemberByFaceImage, json.encode(response));
      _result = true;
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      //log('上傳圖片辨識會員Error: ${_map['message']}');
      _result = false;
    }
    return _result;
  }

  //清除找到的使用者
  void clearFaceImageList() {
    _faceImageList.clear();
  }

  //上傳圖片辨識會員且判斷是否是此會員
  Future<bool> getMemberByFaceImageIsMember(
      String? _faceImage, String? _name) async {
    bool _result = false;
    DetectFaceDTO _detectFace =
        DetectFaceDTO(filePath: '/upload', imageName: _faceImage);
    //log('detectFace: ${_detectFace.toJson()} faceImage: $_faceImage');
    try {
      dynamic response = await HttpUtils.post(
        '${Api.memberFaceImage}/searchMemberByPic',
        data: _detectFace.toJson(),
        options: Options(contentType: 'application/json', headers: {
          "authorization":
              "Bearer ${Api.accessToken}"
        }),
      );
      //log('上傳圖片辨識會員完成: $response');
      List<FaceImageModel> _list =
          await compute(parseMemberByFaceImage, json.encode(response));
      if (_list.isNotEmpty && _list.first.name == _name) {
        _result = true;
      } else {
        _result = false;
      }
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      //log('上傳圖片辨識會員Error: ${_map['message']}');
      _result = false;
    }
    return _result;
  }

  //建立探班紀錄
  Future<bool> addVisitRecord(
      int _fromMemberId,
      String _fromMemberName,
      int _targetMemberId,
      String _targetMemberName,
      String _address,
      LatLng _fromLatLng,
      LatLng _endLatLng,
      WhoPay _whoPay) async {
    bool _result = false;
    AddVisitRecordDTO _addVisitRecord = AddVisitRecordDTO(
      fromMemberId: _fromMemberId,
      fromMemberName: _fromMemberName,
      targetMemberId: _targetMemberId,
      targetMemberName: _targetMemberName,
      address: _address,
      fromLatitude: _fromLatLng.latitude,
      fromLongitude: _fromLatLng.longitude,
      endLatitude: _endLatLng.latitude,
      endLongitude: _endLatLng.longitude,
      whoPay: _whoPay.index,
    );

    //log('建立探班紀錄上傳值: ${_addVisitRecord.toJson()}');

    try {
      dynamic response = await HttpUtils.post(
        '${Api.visitRecord}/addVisitRecord',
        data: _addVisitRecord.toJson(),
        options: Options(contentType: 'application/json', headers: {
          "authorization":
              "Bearer ${Api.accessToken}"
        }),
      );
      //log('建立探班紀錄完成: $response');
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      //log('建立探班紀錄Error: ${_map['message']}');
      _result = false;
    }
    return _result;
  }

  //建立被探班紀錄
  Future<bool> addBeingVisitRecord(
      int _targetMemberId,
      String _targetMemberName,
      int _targetMemberGender,
      String _targetMemberNickname,
      String _targetMemberBirthday,
      String _requirement,
      String _address,
      LatLng _endLatLng,
      WhoPay _whoPay) async {
    bool _result = false;
    _visitButton = ButtonStatus.click;
    notifyListeners();
    AddBeingVisitRecordDTO _addVisitRecord = AddBeingVisitRecordDTO(
      targetMemberId: _targetMemberId,
      targetMemberName: _targetMemberName,
      address: _address,
      endLatitude: _endLatLng.latitude,
      endLongitude: _endLatLng.longitude,
      whoPay: _whoPay.index,
      requirement: _requirement,
      targetMemberGender: _targetMemberGender,
      targetMemberNickname: _targetMemberNickname,
      targetMemberBirthday: _targetMemberBirthday,
    );

    //log('建立被探班紀錄上傳值: ${_addVisitRecord.toJson()}');

    try {
      dynamic response = await HttpUtils.post(
        '${Api.visitRecord}/addBeingVisitRecord',
        data: _addVisitRecord.toJson(),
        options: Options(contentType: 'application/json', headers: {
          "authorization":
              "Bearer ${Api.accessToken}"
        }),
      );
      //log('建立被探班紀錄完成: $response');
      _result = true;
      _visitRecordModel =
          VisitRecordModel.fromJson(json.decode(json.encode(response)));
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      //log('建立被探班紀錄Error: ${_map['message']}');
      _result = false;
    }
    return _result;
  }

  ///設定探班按鈕狀態
  void setVisitButton({required ButtonStatus status}) {
    _visitButton = status;
    notifyListeners();
  }

  //建立躲貓貓紀錄
  Future<bool> addHideAndSeekRecord({
    required int targetMemberId,
    required String targetMemberName,
    required String targetMemberNickname,
    required int targetMemberGender,
    required String targetMemberBirthday,
    required String address,
    required LatLng latLng,
    required int catchType,
  }) async {
    bool _result = false;
    _hideAndSeekButtonStatus = ButtonStatus.click; //進行中
    notifyListeners();
    AddBeingHideAndSeekRecordDTO _addHideAndSeekRecord =
        AddBeingHideAndSeekRecordDTO(
      targetMemberId: targetMemberId,
      targetMemberName: targetMemberName,
      targetMemberNickname: targetMemberNickname,
      targetMemberGender: targetMemberGender,
      targetMemberBirthday: targetMemberBirthday,
      address: address,
      latitude: latLng.latitude,
      longitude: latLng.longitude,
      catchType: catchType,
    );
    //log('建立躲貓貓紀錄上傳值: ${_addHideAndSeekRecord.toJson()}');
    try {
      dynamic response = await HttpUtils.post(
        Api.hideAndSeekRecord,
        data: _addHideAndSeekRecord.toJson(),
        options: Options(contentType: 'application/json', headers: {
          "authorization":
              "Bearer ${Api.accessToken}"
        }),
      );
      //log('建立躲貓貓紀錄完成: $response');
      PeekabooRecordModel _recordModel =
          PeekabooRecordModel.fromJson(json.decode(json.encode(response)));
      _peekabooRecordTargetMemberList.add(_recordModel);
      _result = true;
      //_hideAndSeekButtonStatus = ButtonStatus.notClick; //停止
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      //log('建立躲貓貓紀錄Error: ${_map['message']}');
      _result = false;
      //_hideAndSeekButtonStatus = ButtonStatus.notClick; //停止
    }
    return _result;
  }

  void setHideAndSeekButtonStatus() {
    _hideAndSeekButtonStatus = ButtonStatus.notClick;
    notifyListeners();
  }

  ///取得配對中且未完成的探班記錄
  Future<List<VisitRecordModel>> getFindPairingRecordByBeingVisit(
      {required int? gender,
      required int? whoPay,
      required int? minAge,
      required int? maxAge,
      required String? name}) async {
    _visitProcessingRecordList.clear();
    List<VisitRecordModel> list = [];
    try {
      dynamic response = await HttpUtils.get(
        '${Api.visitRecord}/findPairingRecordByBeingVisit',
        options: Options(contentType: 'application/json', headers: {
          "authorization":
              "Bearer ${Api.accessToken}"
        }),
      );
      //log('配對中且未完成的探班記錄: $response');
      final List<dynamic> _list = json.decode(json.encode(response));
      if (whoPay != null && minAge != null && maxAge != null && name == null) {
        //所有條件
        //log('所有條件');
        _visitProcessingRecordList = _list
            .map((e) => VisitRecordModel.fromJson(e))
            .where((element) =>
                element.targetMemberGender == gender &&
                element.whoPay == whoPay &&
                int.parse(Util().getAge(element.targetMemberBirthday!)) >=
                    minAge &&
                int.parse(Util().getAge(element.targetMemberBirthday!)) <=
                    maxAge &&
                Util().isToDay(element.createdAt!.ceil()))
            .toList();
        List<VisitRecordModel> _deleteOrder = [];
        for (var value in _visitProcessingRecordList) {
          //<- 進行中的訂單
          if (_meDistanceList
              .where((element) => element.id == value.targetMemberId)
              .isEmpty) {
            //<- 附近的人
            //待刪除訂單
            _deleteOrder.add(value);
          }
        }
        for (var element in _deleteOrder) {
          _visitProcessingRecordList.remove(element);
        }
      } else {
        //log('只做男女判斷');
        //<- 取男或女
        _visitProcessingRecordList = _list
            .map((e) => VisitRecordModel.fromJson(e))
            .where((element) =>
                element.targetMemberGender == gender &&
                Util().isToDay(element.createdAt!.ceil()))
            .toList();
      }
      if (name == null) {
        list = _list.map((e) => VisitRecordModel.fromJson(e)).toList();
      } else {
        list = _list
            .map((e) => VisitRecordModel.fromJson(e))
            .where((element) => element.targetMemberName == name)
            .toList();
      }
      //log('取得配對中且未完成的探班記錄 刷新');
      notifyListeners();
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      //log('配對中且未完成的探班記錄Error: ${_map['message']}');
    }
    return list;
  }

  //取得進行中且未完成的探班記錄
  Future<List<VisitRecordModel>> getFindProcessingRecordByBeingVisit(
      {required String name}) async {
    _visitProcessingRecordList.clear();
    List<VisitRecordModel> list = [];
    try {
      dynamic response = await HttpUtils.get(
        '${Api.visitRecord}/findProcessingRecordByBeingVisit',
        options: Options(contentType: 'application/json', headers: {
          "authorization":
              "Bearer ${Api.accessToken}"
        }),
      );
      //log('進行中且未完成的探班記錄: $response');
      final List<dynamic> _list = json.decode(json.encode(response));
      _visitProcessingRecordList = _list
          .map((e) => VisitRecordModel.fromJson(e))
          .where((element) =>
              element.fromMemberName == name ||
              element.targetMemberName == name)
          .toList();
      list = List.from(_visitProcessingRecordList);
      notifyListeners();
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      //log('進行中且未完成的探班記錄Error: ${_map['message']}');
    }
    return list;
  }

  //進行中且未完成與配對中且未完成的訂單
  Future<List<VisitRecordModel>> getFindPairingAndProcessingRecordByBeingVisit(
      {required String name}) async {
    _visitButton = ButtonStatus.click; //表示點擊
    List<VisitRecordModel> list = [];
    var result = await Future.wait([
      getFindProcessingRecordByBeingVisit(name: name),
      getFindPairingRecordByBeingVisit(
          gender: null, whoPay: null, minAge: null, maxAge: null, name: name)
    ]);
    if (result[0].isNotEmpty) {
      list.addAll(result[0]);
    }
    if (result[1].isNotEmpty) {
      list.addAll(result[1]);
    }
    list.sort((left, right) => left.createdAt!.compareTo(right.createdAt!));
    Future.delayed(const Duration(milliseconds: 500),
        () => _visitButton = ButtonStatus.notClick); // 0.5秒後解除
    return list;
  }

  //使用 TargetMemberName 獲取最後一筆探班記錄
  Future<bool> getFindLastByTargetMemberName(String _name) async {
    bool _result = false;
    try {
      dynamic response = await HttpUtils.get(
        '${Api.visitRecord}/findLastByTargetMemberName/$_name',
        options: Options(contentType: 'application/json', headers: {
          "authorization":
              "Bearer ${Api.accessToken}"
        }),
      );
      if (response.runtimeType != String) {
        //log('使用 TargetMemberName 獲取最後一筆探班記錄: $response');
        _visitProcessingRecordList.clear();
        _lastTargetVisitRecord =
            VisitRecordModel.fromJson(json.decode(json.encode(response)));
        if (_lastTargetVisitRecord != null) {
          if (_lastTargetVisitRecord!.status != VisitStatus.complete.index &&
              _lastTargetVisitRecord!.status != VisitStatus.cancel.index &&
              _lastTargetVisitRecord!.fromMemberId != null) {
            _visitProcessingRecordList.add(_lastTargetVisitRecord!);
          }
        }
        _result = true;
        notifyListeners();
      }
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      //log('使用 TargetMemberName 獲取最後一筆探班記錄Error: ${_map['message']}');
      _result = false;
    }
    return _result;
  }

  void setCleanFindLastByTargetMemberName() {
    _visitProcessingRecordList.clear();
    notifyListeners();
  }

  //使用 FromMemberName 獲取最後一筆探班記錄
  Future<bool> getFindLastByFromMemberName(String _name) async {
    bool _result = false;
    try {
      dynamic response = await HttpUtils.get(
        '${Api.visitRecord}/findLastByFromMemberName/$_name',
        options: Options(contentType: 'application/json', headers: {
          "authorization":
              "Bearer ${Api.accessToken}"
        }),
      );
      if (response.runtimeType != String) {
        //log('使用 FromMemberName 獲取最後一筆探班記錄: $response');
        _lastFromVisitRecord =
            VisitRecordModel.fromJson(json.decode(json.encode(response)));
        _result = true;
        notifyListeners();
      }
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      //log('使用 FromMemberName 獲取最後一筆探班記錄Error: ${_map['message']}');
      _result = false;
    }
    return _result;
  }

  //使用 FromMemberId 獲取探班記錄
  Future<List<VisitRecordModel>> getByFromMemberId(
      int _id, bool _isCanClear) async {
    List<VisitRecordModel> _visitRecord = [];
    if (_isCanClear) {
      _fromMemberAllVisitRecord.clear();
    }
    try {
      dynamic response = await HttpUtils.get(
        '${Api.visitRecord}/byFromMemberId/$_id',
        options: Options(contentType: 'application/json', headers: {
          "authorization":
              "Bearer ${Api.accessToken}"
        }),
      );
      if (response.runtimeType != String) {
        //log('使用 FromMemberId 獲取探班記錄: $response');
        _visitRecord =
            await compute(parseVisitRecordModel, json.encode(response));
        if (_isCanClear) {
          for (var element in _visitRecord) {
            if (element.status == VisitStatus.complete.index) {
              _fromMemberAllVisitRecord.add(element);
            }
          }
        }
        _fromMemberAllVisitRecord.toSet();
      }
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      //log('使用 FromMemberId 獲取探班記錄Error: ${_map['message']}');
    }
    return _visitRecord;
  }

  //使用 TargetMemberId 獲取被探班記錄
  Future<List<VisitRecordModel>> getByTargetMemberId(
      int _id, bool _isCanClear) async {
    _targetMemberAllVisitRecord.clear();
    List<VisitRecordModel> _visitRecord = [];
    try {
      dynamic response = await HttpUtils.get(
        '${Api.visitRecord}/byTargetMemberId/$_id',
        options: Options(contentType: 'application/json', headers: {
          "authorization":
              "Bearer ${Api.accessToken}"
        }),
      );
      if (response.runtimeType != String) {
        //log('使用 TargetMemberId 獲取被探班記錄: $response');
        _visitRecord =
            await compute(parseVisitRecordModel, json.encode(response));
        if (_isCanClear) {
          for (var element in _visitRecord) {
            if (element.status == VisitStatus.complete.index) {
              _targetMemberAllVisitRecord.add(element);
            }
          }
        }
        _targetMemberAllVisitRecord.toSet();
      }
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      //log('使用 TargetMemberId 獲取探班記錄Error: ${_map['message']}');
    }
    return _visitRecord;
  }

  //使用 ID 取全部探班資料
  Future<void> getAllVisitRecord(int _id) async {
    double _memberStarTem = 0.0;
    int _frequency = 0;
    _allVisitRecord.clear();
    var result = await Future.wait(
        [getByFromMemberId(_id, true), getByTargetMemberId(_id, true)]);
    _allVisitRecord.addAll(result[0]);
    _allVisitRecord.addAll(result[1]);

    for (var element in _allVisitRecord) {
      if (element.fromMemberId == _id &&
          element.status == VisitStatus.complete.index) {
        _memberStarTem += element.targetMemberScore!;
        _frequency++;
      } else if (element.targetMemberId == _id &&
          element.status == VisitStatus.complete.index) {
        _memberStarTem += element.fromMemberScore!;
        _frequency++;
      }
    }
    if (_frequency == 0) {
      _memberStar = 0;
    } else {
      _memberStar = _memberStarTem / _frequency;
    }
    notifyListeners();
  }

  //用ID取得探班全部記錄計算星星數
  Future<void> getOtherStar(int _id) async {
    //log('ID: $_id');
    double _memberStarTem = 0.0;
    int _frequency = 0;
    var result = await Future.wait(
        [getByFromMemberId(_id, false), getByTargetMemberId(_id, false)]);
    List<VisitRecordModel> _allRecord = [];
    _allRecord.addAll(result[0]);
    _allRecord.addAll(result[1]);
    for (var element in _allRecord) {
      if (element.fromMemberId == _id &&
          element.status == VisitStatus.complete.index) {
        _memberStarTem += element.targetMemberScore!;
        _frequency++;
      } else if (element.targetMemberId == _id &&
          element.status == VisitStatus.complete.index) {
        _memberStarTem += element.fromMemberScore!;
        _frequency++;
      }
    }
    if (_frequency == 0) {
      _otherStar = 0;
    } else {
      _otherStar = _memberStarTem / _frequency;
      //notifyListeners();
      //log('1.對方的星星數 $_otherStar');
    }
  }

  //列表星星
  Future<double> getListOtherStar({required int id}) async {
    //log('getListOtherStar: $id');
    double _result = 0.0;
    double _memberStarTem = 0.0;
    int _frequency = 0;
    var result = await Future.wait(
        [getByFromMemberId(id, false), getByTargetMemberId(id, false)]);
    List<VisitRecordModel> _allRecord = [];
    _allRecord.addAll(result[0]);
    _allRecord.addAll(result[1]);
    for (var element in _allRecord) {
      if ((element.fromMemberId == id) &&
          element.status == VisitStatus.complete.index) {
        _memberStarTem += element.targetMemberScore!;
        _frequency++;
      } else if (element.targetMemberId == id &&
          element.status == VisitStatus.complete.index) {
        _memberStarTem += element.fromMemberScore!;
        _frequency++;
      }
    }
    if (_frequency == 0) {
      _result = 0.0;
    } else {
      _result = _memberStarTem / _frequency;
    }
    return _result;
  }

  //星星評分紀錄
  Future<bool> getStarRecord({required int id}) async {
    bool _result = false;
    var result = await Future.wait(
        [getByFromMemberId(id, false), getByTargetMemberId(id, false)]);
    List<VisitRecordModel> _allRecord = [];
    _allRecord.addAll(result[0]
        .where((element) => element.status == VisitStatus.complete.index)
        .toList());
    _allRecord.addAll(result[1]
        .where((element) => element.status == VisitStatus.complete.index)
        .toList());
    for (var element in _allRecord) {
      if ((element.fromMemberId == id) &&
          element.status == VisitStatus.complete.index) {
        _visitStarRecordList
            .add(VisitStarRecordModel.targetMemberJson(element.toJson()));
        _visitStarRecordList.sort((left, right) => (right.completeAt!.compareTo(left.completeAt!)));
      } else if (element.targetMemberId == id &&
          element.status == VisitStatus.complete.index) {
        _visitStarRecordList
            .add(VisitStarRecordModel.fromMemberJson(element.toJson()));
        _visitStarRecordList.sort((left, right) => (right.completeAt!.compareTo(left.completeAt!)));
      }
    }
    return _result;
  }

  void clearStarRecord() {
    _visitStarRecordList.clear();
  }

  //如果已接單 進行中 直接替換
  void setVisitRecordModel(VisitRecordModel _record) {
    _visitRecordModel = _record;
    notifyListeners();
  }

  //使用 TargetMemberId 獲取躲貓貓紀錄 被捕捉紀錄
  Future<bool> getHideAndSeekRecordByTargetMemberId({required int id}) async {
    bool _result = false;
    try {
      dynamic response = await HttpUtils.get(
        '${Api.hideAndSeekRecord}/byTargetMemberId/$id',
        options: Options(contentType: 'application/json', headers: {
          "authorization":
              "Bearer ${Api.accessToken}"
        }),
      );
      //log('使用 TargetMemberId 獲取躲貓貓紀錄: $response');
      final List<dynamic> _list = json.decode(json.encode(response));
      _peekabooRecordTargetMemberList =
          _list.map((e) => PeekabooRecordModel.fromJson(e)).toList();
      _peekabooRecordTargetMemberCompleteList = _list
          .map((e) => PeekabooRecordModel.fromJson(e))
          .where((element) =>
              element.status == CatchStatus.catchStatusComplete.index)
          .toList();

      //本周
      _thisWeekPeekabooRecordTargetMemberCompleteList = _peekabooRecordTargetMemberCompleteList
          .where((element) => Util().isWeek(element.createdAt!.ceil(), isUtc: false))
          .toList();
      _thisWeekPeekabooRecordTargetMemberCompleteList.sort((left, right) => right.createdAt!.compareTo(left.createdAt!));

      //本日
      _toDayPeekabooRecordTargetMemberCompleteList =
          _peekabooRecordTargetMemberCompleteList
              .where((element) => Util().isToDay(element.createdAt!.ceil()))
              .toList();
      _toDayPeekabooRecordTargetMemberCompleteList
          .sort((left, right) => right.createdAt!.compareTo(left.createdAt!));
      _peekabooRecordTargetMemberCompleteList
          .sort((left, right) => right.createdAt!.compareTo(left.createdAt!));
      List<PeekabooRecordModel> _count = _peekabooRecordTargetMemberCompleteList
          .where((element) => Util().isToDay(element.createdAt!.ceil()))
          .toList();
      _beCaughtCount = _count.length;
      _result = true;
      notifyListeners();
    } on DioError catch (e) {
      _result = false;
      //log('使用 TargetMemberId 獲取躲貓貓紀錄Error: ${e.message}');
    }
    return _result;
  }

  //使用 FromMemberId 獲取躲貓貓記錄 捕捉紀錄
  Future<bool> getHideAndSeekRecordByFromMemberId({required int id}) async {
    bool _result = false;
    _isCatchCount = 0;
    try {
      dynamic response = await HttpUtils.get(
        '${Api.hideAndSeekRecord}/byFromMemberId/$id',
        options: Options(contentType: 'application/json', headers: {
          "authorization":
              "Bearer ${Api.accessToken}"
        }),
      );
      //log('使用 FromMemberId 獲取躲貓貓記錄: $response');
      final List<dynamic> _list = json.decode(json.encode(response));
      _peekabooRecordFromMemberList =
          _list.map((e) => PeekabooRecordModel.fromJson(e)).toList();
      _peekabooRecordFromMemberCompleteList = _list
          .map((e) => PeekabooRecordModel.fromJson(e))
          .where((element) =>
              element.status == CatchStatus.catchStatusComplete.index)
          .toList();
      //本周
      _thisWeekPeekabooRecordFromMemberCompleteList = _peekabooRecordFromMemberCompleteList
          .where((element) => Util().isWeek(element.createdAt!.ceil(), isUtc: false))
          .toList();
      _thisWeekPeekabooRecordFromMemberCompleteList.sort((left, right) => right.createdAt!.compareTo(left.createdAt!));

      //本日
      _toDayPeekabooRecordFromMemberCompleteList =
          _peekabooRecordFromMemberCompleteList
              .where((element) => Util().isToDay(element.createdAt!.ceil()))
              .toList();
      _toDayPeekabooRecordFromMemberCompleteList
          .sort((left, right) => right.createdAt!.compareTo(left.createdAt!));
      _peekabooRecordFromMemberCompleteList
          .sort((left, right) => right.createdAt!.compareTo(left.createdAt!));
      List<PeekabooRecordModel> _count = _peekabooRecordFromMemberCompleteList
          .where((element) => Util().isToDay(element.createdAt!.ceil()))
          .toList();
      _isCatchCount = _count.length;
      _result = true;
      notifyListeners();
    } on DioError catch (e) {
      _result = false;
      //log('使用 FromMemberId 獲取躲貓貓記錄Error: ${e.message}');
    }
    return _result;
  }

  //使用 Status 獲取躲貓貓紀錄
  Future<bool> getHideAndSeekRecordByStatus() async {
    bool _result = false;
    try {
      dynamic response = await HttpUtils.get(
        '${Api.hideAndSeekRecord}/findByStatus/${CatchStatus.catchStatusProcessing.index}',
        options: Options(contentType: 'application/json', headers: {
          "authorization":
              "Bearer ${Api.accessToken}"
        }),
      );
      //log('使用 Status 獲取躲貓貓紀錄: $response');
      

      _peekabooRecordProcessingList =
          await compute(parseHideAndSeekRecordByStatus, json.encode(response));

      //假資料
      //_peekabooRecordProcessingList = await compute(parseHideAndSeekRecordByStatus, json.encode([TestPeekaboo().p1, TestPeekaboo().p2, TestPeekaboo().p3, TestPeekaboo().p4, TestPeekaboo().p5, TestPeekaboo().p6, TestPeekaboo().p7, TestPeekaboo().p8, TestPeekaboo().p9, TestPeekaboo().p10 ]));
    } on DioError catch (e) {
      _result = false;
      //log('使用 Status 獲取躲貓貓紀錄Error: ${e.message}');
    }
    return _result;
  }

  //替換內容
  void setPeekabooRecordTargetMemberList(
      {required PeekabooRecordModel record}) {
    PeekabooRecordModel _model = record;
    _peekabooRecordTargetMemberList.remove(record);
    _peekabooRecordTargetMemberList.insert(
        _peekabooRecordTargetMemberList.length, _model);
    notifyListeners();
  }

  void clearVisitRecordModel() {
    _visitRecordModel = null;
    notifyListeners();
  }

  //清除資料
  void clearVisitProcessingRecordList() {
    _visitProcessingRecordList.clear();
    notifyListeners();
  }

  //使用 hideStatus 和 hideType 獲取會員資料
  Future<bool> getPeekabooLocal() async {
    bool _result = false;
    try {
      dynamic response = await HttpUtils.get(
        '${Api.memberFaceImage}/hideStatus/${OnlineStatus.onlineStatusOnline.index}/hideType/${Catch.isCatch.index}',
        options: Options(contentType: 'application/json', headers: {
          "authorization":
              "Bearer ${Api.accessToken}"
        }),
      );
      log('使用 hideStatus 和 hideType 獲取會員資料: $response');
      _memberLocalList = await compute(parseMemberLocal, json.encode(response));
      _result = true;
      notifyListeners();
    } on DioError catch (e) {
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      //log('使用 hideStatus 和 hideType 獲取會員資料Error: ${_map['message']}');
      _result = false;
    }
    return _result;
  }

  //使用 TargetMemberName 獲取躲貓貓記錄
  Future<PeekabooRecordModel?> getPeekabooRecordByTargetMemberName(
      {required String name, required int id}) async {
    List<PeekabooRecordModel> _member = [];
    try {
      dynamic response = await HttpUtils.get(
        '${Api.hideAndSeekRecord}/byTargetMemberName/$name',
        options: Options(contentType: 'application/json', headers: {
          "authorization":
              "Bearer ${Api.accessToken}"
        }),
      );
      //log('使用 TargetMemberName 獲取躲貓貓記錄: $response');
      _member = await compute(
          parsePeekabooRecordByTargetMemberName, json.encode(response));
      //_member = List.from(_member.where((element) => element.id))
    } on DioError catch (e) {
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      //log('使用 TargetMemberName 獲取躲貓貓記錄Error: ${_map['message']}');
    }
    if (_member.isEmpty) {
      return null;
    }
    return _member.first;
  }

  ///上傳辨識紀錄
  /// * [addFacesDetectHistoryModel] AddFacesDetectHistoryModel
  Future<bool> postFacesDetectHistory(
      {required AddFacesDetectHistoryModel addFacesDetectHistoryModel}) async {
    bool _result = false;
    //log('上傳資料: ${addFacesDetectHistoryModel.toJson()}');
    try {
      dynamic response = await HttpUtils.post(
        Api.faceDetectHistory,
        data: addFacesDetectHistoryModel.toJson(),
        options: Options(contentType: 'application/json', headers: {
          "authorization":
              "Bearer ${Api.accessToken}"
        }),
      );
      //log('上傳辨識紀錄: $response');
      _result = true;
    } on DioError catch (e) {
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      //log('上傳辨識紀錄Error: ${_map['message']}');
      _result = false;
    }
    return _result;
  }

  ///使用 TargetMemberId 獲取辨識紀錄次數
  /// * [id] 自己的ID
  Future<int> getLengthFacesDetectHistoryByTargetMemberId(
      {required int id}) async {
    try {
      dynamic response = await HttpUtils.get(
        '${Api.faceDetectHistory}/byTargetMemberId/$id',
        options: Options(contentType: 'application/json', headers: {
          "authorization":
              "Bearer ${Api.accessToken}"
        }),
      );
      //log('使用 TargetMemberId 獲取辨識紀錄: $response');
      List<FacesDetectHistoryModel> list =
          await compute(parseFacesDetectHistoryModel, json.encode(response));
      _facesDetectHistoryRecord = List.from(list.where((element) => Util().isWeek(element.createdAt!.ceil(), isUtc: false)).toList());
      return _facesDetectHistoryRecord.length;
    } on DioError catch (e) {
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      //log('使用 TargetMemberId 獲取辨識紀錄Error: ${_map['message']}');
      return 0;
    }
  }

  ///
  /// 重新驗證改狀態
  /// [id] 自己的
  Future<void> putReview({required int id}) async {
    try {
      dynamic response = await HttpUtils.put(
        '${Api.member}/$id/review',
        data: {'reVerify': id},
        options: Options(contentType: 'application/json', headers: {
          "authorization":
              "Bearer ${Api.accessToken}"
        }),
      );
      //log('重新驗證改狀態: $response');
    } on DioError catch (e) {
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      //log('重新驗證改狀態Error: ${_map['message']}');
    }
  }

  ///更新用戶Token
  /// * [id] 自己的
  /// * [token] FirebaseToken
  Future<void> putFcmToken({required int id, required String token}) async {
    //log('用戶Token: $token');
    try {
      dynamic response = await HttpUtils.put('${Api.member}/$id/fcmToken',
          data: {'fcmToken': token},
          options: Options(contentType: 'application/json', headers: {
            "authorization":
                "Bearer ${Api.accessToken}"
          }));
      //log('更新用戶Token $response');
    } on DioError catch (e) {
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      //log('更新用戶TokenError: ${_map['message']}');
    }
  }

  ///註銷
  /// * [id] 自己的
  Future<bool> deleteMember({required int id}) async {
    bool _result = false;
    try {
      dynamic response = await HttpUtils.delete('${Api.memberFaceImage}/$id',
          options: Options(contentType: 'application/json', headers: {
            "authorization":
                "Bearer ${Api.accessToken}"
          }));
      //log('註銷 $response');
      _result = true;
    } on DioError catch (e) {
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      //log('註銷Error: ${_map['message']}');
      _result = false;
    }
    return _result;
  }

  ///加值方案
  Future<Map<String, dynamic>> getTopUpPlan() async {
    try {
      dynamic response = await HttpUtils.get(
        Api.topUpPlan,
        options: Options(contentType: 'application/json', headers: {
          "authorization":
              "Bearer ${Api.accessToken}"
        }),
      );
      //log('加值方案: $response');
      final List list = json.decode(json.encode(response));
      List<TopUpPlanModel> _topUpPlanList = list.map((e) => TopUpPlanModel.fromJson(e)).where((element) => element.status == 1).toList();
      return {'status': true, 'value': _topUpPlanList};
    } on DioError catch (e) {
      //log('會員點數Error: ${e.response!.data}');
      return {'status': false};
    }
  }

  ///新建會員加值紀錄
  /// * [addPointsHistoryDTO] AddPointsHistoryDTO 基本資料
  Future<Map<String, dynamic>?> postMemberPointHistory(
      {required AddPointsHistoryDTO addPointsHistoryDTO}) async {
    //log('上傳資料: ${addPointsHistoryDTO.toJson()}');
    if (_addPointsHistoryButton == ButtonStatus.notClick) {
      _addPointsHistoryButton = ButtonStatus.click;
      notifyListeners();
      try {
        dynamic response = await HttpUtils.post(
          Api.addPointHistory,
          options: Options(contentType: 'application/json', headers: {
            "authorization":
                "Bearer ${Api.accessToken}"
          }),
          data: addPointsHistoryDTO.toJson(),
        );
        //log('新建會員加值紀錄: $response');
        RechargeRecordModel _rechargeRecord =
            RechargeRecordModel.fromJson(json.decode(json.encode(response)));
        Future.delayed(const Duration(milliseconds: 500), () {
          _addPointsHistoryButton = ButtonStatus.notClick;
        });
        return {'status': true, 'value': _rechargeRecord};
      } on DioError catch (e) {
        //log('新建會員加值紀錄Error: ${e.response!.data}');
        Future.delayed(const Duration(milliseconds: 500), () {
          _addPointsHistoryButton = ButtonStatus.notClick;
        });
        return {'status': false};
      }
    } else {
      return null;
    }
  }

  ///加值
  /// * [yangPaymentReqDTO] 加值Model
  Future<Map<String, dynamic>> postYauYangPayment(
      {required YauYangPaymentReqDTO yangPaymentReqDTO}) async {
    //log('yangPaymentReqDTO: ${yangPaymentReqDTO.toJson()}');
    try {
      dynamic response = await HttpUtils.post(
        Api.yauyangPayment,
        options: Options(contentType: 'application/json', headers: {
          "authorization":
              "Bearer ${Api.accessToken}"
        }),
        data: yangPaymentReqDTO.toJson(),
      );
      //log('加值: $response');
      PaymentResDTO _paymentRes =
          PaymentResDTO.fromJson(json.decode(json.encode(response)));
      return {'status': true, 'value': _paymentRes};
    } on DioError catch (e) {
      //log('加值Error: ${e.response}');
      return {'status': false};
    }
  }

  ///備註
  /// * [id]
  Future<List<MemberFriendNameModel>?> getFriendNameByFromMemberId(
      {required int id}) async {
    try {
      dynamic response = await HttpUtils.get(
        '${Api.friendName}/byFromMemberId/$id',
        options: Options(contentType: 'application/json', headers: {
          "authorization":
              "Bearer ${Api.accessToken}"
        }),
      );
      log('備註: $response');
      _friendNameList =
          await compute(parseMemberFriendNameModel, json.encode(response));
      return _friendNameList;
    } on DioError catch (e) {
      //log('備註Error: ${e.response}');
      return null;
    }
  }

  ///更改備註
  /// * [memberFriendNameDTO] 更新資料
  Future<bool> putFriendName(
      {required MemberFriendNameDTO memberFriendNameDTO}) async {
    bool _result = false;
    try {
      dynamic response = await HttpUtils.put(Api.friendName,
          options: Options(contentType: 'application/json', headers: {
            "authorization":
                "Bearer ${Api.accessToken}"
          }),
          data: memberFriendNameDTO.toJson());
      //log('更改備註: $response');
      _result = true;
    } on DioError catch (e) {
      //log('更改備註Error: ${e.response}');
      _result = false;
    }
    return _result;
  }

  ///系統設定
  Future<void> getSystemSetting() async {
    try {
      dynamic response = await HttpUtils.get(
        Api.systemSetting,
        options: Options(contentType: 'application/json'),
      );
      //log('系統設定: $response');
      _systemSetting = SystemSettingModel.fromJson(json.decode(json.encode(response)));
      notifyListeners();
    } on DioError catch (e) {
      //log('系統設定Error: ${e.response}');
    }
  }

  ///取得清潔工具列表
  Future<void> getCleaningTools() async {
    try {
      dynamic response = await HttpUtils.get(
        Api.cleaningTools,
        options: Options(contentType: 'application/json', headers: {
          "authorization":
              "Bearer ${Api.accessToken}"
        }),
      );
      //log('清潔工具列表: $response');
      _cleaningToolsList =
          await compute(parseCleaningToolsModel, json.encode(response));
    } on DioError catch (e) {
      //log('清潔工具列表Error: ${e.response}');
    }
  }

  ///更改探班地址
  Future<void> putVisitRecordAddress(
      {required int id, required ChangeVisitRecordAddressDTO address}) async {
    try {
      dynamic response = await HttpUtils.put(
        '${Api.visitRecord}/changeAddress/$id',
        data: address.toJson(),
        options: Options(contentType: 'application/json', headers: {
          "authorization":
              "Bearer ${Api.accessToken}"
        }),
      );
      //log('更改探班地址: $response');
    } on DioError catch (e) {
      //log('更改探班地址Error: ${e.response}');
    }
  }

  ///獲取附近會員資料
  /// * [memberId] 會員Id
  /// * [distance] 距離
  Future<void> getNearMeDistance(
      {required int memberId, required int distance}) async {
    try {
      dynamic response = await HttpUtils.get(
        '${Api.memberFaceImage}/nearMe/$memberId/distance/$distance',
        options: Options(contentType: 'application/json', headers: {
          "authorization":
              "Bearer ${Api.accessToken}"
        }),
      );
      //log('獲取附近會員資料: $response');
      List _list = json.decode(json.encode(response));
      _meDistanceList = _list
          .map((e) => MemberModel.fromJson(e))
          .where((element) => element.id != memberId)
          .toList();
    } on DioError catch (e) {
      //log('獲取附近會員資料Error: ${e.response}');
    }
  }

  ///檢舉紀錄
  Future<bool> postComplaintRecord({required ComplaintRecordDTO dto}) async {
    bool _result = false;
    try {
      dynamic response = await HttpUtils.post(
        Api.complaintRecord,
        data: dto.toJson(),
        options: Options(contentType: 'application/json', headers: {
          "authorization":
              "Bearer ${Api.accessToken}"
        }),
      );
      //log('檢舉紀錄: $response');
      _result = true;
    } on DioError catch (e) {
      //log('檢舉紀錄Error: ${e.response}');
      _result = false;
    }
    return _result;
  }

  ///取得遊戲設定
  Future<bool> getGameSetting() async {
    bool _result = false;
    try{
      dynamic response = await HttpUtils.get(
        '${Api.gameSetting}/1',
        options: Options(contentType: 'application/json', headers: {
          "authorization":
          "Bearer ${Api.accessToken}"
        }),
      );
      log('取得遊戲設定: $response');
      _gameSetting = GameSettingModel.fromJson(json.decode(json.encode(response)));
      _result = true;
    } on DioError catch(e){
      log('取得遊戲設定Error: ${e.response}');
      _result = false;
    }
    return _result;
  }
}
