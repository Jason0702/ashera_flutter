import 'dart:convert';
import 'dart:developer';

import 'package:ashera_flutter/models/member_model.dart';
import 'package:dio/dio.dart';
import 'package:dio_http_package/dio_http_package.dart';
import 'package:flutter/cupertino.dart';

import '../enum/app_enum.dart';
import '../models/user_model.dart';
import '../utils/api.dart';
import '../utils/util.dart';

class AuthProvider with ChangeNotifier{
  AuthStatus _loggedInStatus = AuthStatus.notLoggedIn;
  AuthStatus _registeredInStatus = AuthStatus.notRegistered;

  AuthStatus get loggedInStatus => _loggedInStatus;
  AuthStatus get registeredInStatus => _registeredInStatus;
  //登入驗證
  Future<Map<String, dynamic>> login(String _account, String _password) async{
    Map<String, dynamic> result = {};
    final Map<String, dynamic> _map = {
      'name': _account,
      'password': _password
    };
    //log('上傳參數: $_map');
    _loggedInStatus = AuthStatus.authenticating;
    notifyListeners();
    try{
      dynamic response = await HttpUtils.post(
        Api.memberLogin,
        data: _map,
        options: Options(contentType: 'application/json'),
      );
      debugPrint('登入 $response');
      User _user = User.fromJson(json.decode(json.encode(response)));
      result = {'status': true, 'message': 'Successful', 'user': _user};
      _loggedInStatus = AuthStatus.loggedIn;
      notifyListeners();
    } on DioError catch (e) {
      //錯誤
      //log('登入 ${e.message}');
      result = {'status': false, 'message': e.message};
      _loggedInStatus = AuthStatus.notLoggedIn;
      notifyListeners();
    }
    return result;
  }

  ///獲取驗證碼資料
  /// * [number] 手機號碼
  Future<bool> verificationCode({required String number}) async {
    bool _result = false;
    try{
      dynamic response = await HttpUtils.get(
        '${Api.verificationCode}/number/$number',
        options: Options(contentType: 'application/json'),
      );
      //log('獲取驗證碼資料 $response');
      _result = true;
    }on DioError catch(e){
      _result = false;
      //log('獲取驗證碼資料Error ${e.message}');
    }
    return _result;
  }

  ///註冊
  Future<Map<String, dynamic>> register(String _nickName, String _account, String _password, int _gender, String _birthday, String verificationCode, String zodiacSign) async {
    Map<String, dynamic> result = {};
    final Map<String, dynamic> _map = {
      'name': _account,
      'password': _password,
      'nickname': _nickName,
      'cellphone': _account,
      'gender': _gender,
      'birthday': _birthday,
      'mugshot': '',
      'age': Util().getRegionAge(_birthday),
      'verificationCode': verificationCode,
      'zodiacSign': zodiacSign
    };
    log('上傳參數: $_map');
    _registeredInStatus = AuthStatus.authenticating;
    notifyListeners();
    try {
      dynamic response = await HttpUtils.post(
        Api.memberRegister,
        data: _map,
        options: Options(contentType: 'application/json'),
      );
      //log('註冊 $response');
      MemberModel _member = MemberModel.fromJson(json.decode(json.encode(response)));
      result = {'status': true, 'message': _member};
      _registeredInStatus = AuthStatus.registered;
      notifyListeners();
    } on DioError catch (e) {
      //錯誤
      //log('註冊 ${e.message}');
      result = {'status': false, 'message': e.message};
      _registeredInStatus = AuthStatus.notRegistered;
      notifyListeners();
    }
    return result;
  }
  ///發送驗證碼
  /// * [phone] 手機號碼
  Future<bool> forgotPasswordVerificationCode({required String phone}) async {
    bool _result = false;
    try{
      dynamic response = await HttpUtils.get(
        '${Api.forgotPasswordVerificationCode}$phone',
        options: Options(contentType: 'application/json'),
      );
      debugPrint('發送驗證碼 $response');
      _result = true;
    }on DioError catch (e){
      //錯誤
      //log('發送驗證碼 ${e.message}');
      _result = false;
    }
    return _result;
  }

  ///忘記密碼
  /// * [memberForgotPasswordDTO] MemberForgotPasswordDTO
  Future<bool> forgotPassword({required MemberForgotPasswordDTO memberForgotPasswordDTO}) async {
    bool _result = false;
    try{
      dynamic response = await HttpUtils.post(
        Api.forgotPassword,
        options: Options(contentType: 'application/json'),
        data: memberForgotPasswordDTO.toJson()
      );
      debugPrint('忘記密碼 $response');
      _result = true;
    }on DioError catch (e){
      //錯誤
      //log('忘記密碼 ${e.message}');
      _result = false;
    }
    return _result;
  }
}