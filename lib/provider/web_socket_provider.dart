import 'dart:convert';
import 'dart:developer';
import 'dart:typed_data';

import 'package:ashera_flutter/utils/shared_preference.dart';
import 'package:flutter/cupertino.dart';
import 'package:web_socket_channel/io.dart';
import 'package:web_socket_channel/status.dart' as status;


import '../enum/app_enum.dart';
import '../utils/api.dart';

//WebSocket 檔案上傳
class WebSocketProvider with ChangeNotifier{
  UpLoadingStatus _uploadFileStatus = UpLoadingStatus.notUpLoading;
  UpLoadingStatus get uploadFileStatus => _uploadFileStatus;

  MessageType _messageType = MessageType.PIC;
  MessageType get messageType => _messageType;

  String _thisFileName = '';
  String get fileName => _thisFileName;

  final SharedPreferenceUtil _sharedPreferenceUtil = SharedPreferenceUtil();

  void uploadFile(String _fileName, Uint8List _file) async {
    _sharedPreferenceUtil.getAccountAndPassword().then((value) {
      //log('WebSocketProvider 連線');
      final channel = IOWebSocketChannel.connect('${Stomp.webSocketBinary}?name=${base64.encode(utf8.encode(_fileName))}',
          headers: {"username": value['name'], "password": value['password']});
      channel.sink.add(_file);
      _uploadFileStatus = UpLoadingStatus.upLoading;
      notifyListeners();

      channel.stream.listen((message) {
        //log('WebSocketProvider $message');
        if(message == 'upload success'){
          channel.sink.close(status.goingAway);
        }
      }, onDone: (){
        //log('WebSocketProvider OnDone');
        _thisFileName = _fileName;
        _uploadFileStatus = UpLoadingStatus.uploadCompleted;
        notifyListeners();
      }, onError: (err, stack) {
        //log('WebSocketProvider OnError: ${err.runtimeType}');
        //log('WebSocketProvider OnError: $err');
        _thisFileName = '';
        _uploadFileStatus = UpLoadingStatus.uploadFailed;
        notifyListeners();
      },
          cancelOnError: true);
    });
  }

  void setFileUploadState(){
    _thisFileName = '';
    _messageType = MessageType.PIC;
    _uploadFileStatus = UpLoadingStatus.notUpLoading;
  }

  void setMessageTypeToPIC(){
    _messageType = MessageType.PIC;
  }

  void setMessageTypeToVideo(){
    _messageType = MessageType.VIDEO;
  }
}