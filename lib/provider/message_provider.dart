
import 'package:flutter/cupertino.dart';
import '../models/message_model.dart';


class MessageProvider with ChangeNotifier {
  List<MessageModel> messages = [
  ];
  //發送訊息
  Future<void> sentMessage(MessageModel data) async {
    messages.add(data);
    notifyListeners();
  }
  //取得訊息
  void getMessage(List data) async{
    if(data.isEmpty){
      return ;
    }
    messages.clear();
    messages = data.map((e) => MessageModel.fromJson(e)).toList();
    notifyListeners();
  }

  void deleteMessage(){
    messages.clear();
    notifyListeners();
  }
}
