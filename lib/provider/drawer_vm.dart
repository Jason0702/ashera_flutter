
import 'package:flutter/cupertino.dart';

class DrawerVm extends ChangeNotifier{
  double _bodyHeight = 0.0;
  double get bodyHeight => _bodyHeight;

  void drawerBodyHeight(double value){
    _bodyHeight = value;
    notifyListeners();
  }
}