import 'package:flutter/cupertino.dart';

import '../enum/app_enum.dart';
import '../models/place.dart';

class PeekabooGameProvider<T> with ChangeNotifier{
  //貓或狗狀態
  PeekabooUserStatus _userStatus = PeekabooUserStatus.seek;
  PeekabooUserStatus get userStatus => _userStatus;
  //紀錄捕捉或被捕捉狀態
  PeekabooUserStatus _recordStatus = PeekabooUserStatus.seek;
  PeekabooUserStatus get recordStatus => _recordStatus;
  //獵取名單狗或貓狀態
  PeekabooUserStatus _huntRecordStatus = PeekabooUserStatus.seek;
  PeekabooUserStatus get huntRecordStatus => _huntRecordStatus;

  //抓取狀態
  CatchStatus _catchStatus = CatchStatus.catchStatusComplete;
  CatchStatus get catchStatus => _catchStatus;

  Iterable<Place> _items = [];
  Iterable<Place> get items => _items;

  //設定貓或狗狀態
  void setPeekabooUserStatus({required PeekabooUserStatus status}){
    _userStatus = status;
    notifyListeners();
  }

  //設定紀錄捕捉或被捕捉狀態
  void setPeekabooRecordStatus({required PeekabooUserStatus status}){
    _recordStatus = status;
    notifyListeners();
  }

  //設定獵取名單狗或貓狀態
  void setHuntRecordStatus({required PeekabooUserStatus status}){
    _huntRecordStatus = status;
    notifyListeners();
  }

  //設定抓取狀態
  void setCatchStatus({required CatchStatus status}){
    _catchStatus = status;
    notifyListeners();
  }

  void setItem(Iterable<Place> value){
    _items = value;
    notifyListeners();
  }
}