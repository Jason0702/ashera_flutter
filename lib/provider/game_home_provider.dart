import 'dart:convert';
import 'dart:developer';

import 'package:ashera_flutter/models/animal_type_model.dart';
import 'package:ashera_flutter/models/dung_model.dart';
import 'package:ashera_flutter/models/house_model.dart';
import 'package:ashera_flutter/models/member_model.dart';
import 'package:ashera_flutter/models/pets_model.dart';
import 'package:ashera_flutter/utils/util.dart';
import 'package:dio/dio.dart';
import 'package:dio_http_package/dio_http_package.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_cache_manager/flutter_cache_manager.dart';

import '../enum/app_enum.dart';
import '../models/furniture_model.dart';
import '../models/furniture_type_model.dart';
import '../models/game_model.dart';
import '../utils/api.dart';
import '../utils/parse_json.dart';
import '../utils/shared_preference.dart';

class GameHomeProvider with ChangeNotifier {
  final DefaultCacheManager _manager = DefaultCacheManager();

  ///furnitureTypeIndex
  int _furnitureTypeIndex = 0;
  int get furnitureTypeIndex => _furnitureTypeIndex;

  ///編輯模式
  bool _editMode = false;
  bool get editMode => _editMode;

  ///留言模式
  bool _messageMode = false;
  bool get messageMode => _messageMode;

  ///itemIndex
  int _itemIndex = 0;
  int get itemIndex => _itemIndex;

  List _showList = [];
  List get showList => _showList;

  //全部便便
  List<DungModel> _allDungList = [];
  List<DungModel> get allDungList => _allDungList;
  //全部動物類型
  List<AnimalTypeModel> _allAnimalTypeList = [];
  List<AnimalTypeModel> get allAnimalTypeList => _allAnimalTypeList;
  //全部家具
  List<FurnitureModel> _allFurnitureList = [];
  List<FurnitureModel> get allFurnitureList => _allFurnitureList;
  //全部家具類型
  List<FurnitureTypeModel> _allFurnitureTypeList = [];
  List<FurnitureTypeModel> get allFurnitureTypeList => _allFurnitureTypeList;
  //全部寵物
  List<PetsModel> _allPetsList = [];
  List<PetsModel> get allPetsList => _allPetsList;
  List<PetsModel> _noPigPetsList = [];
  List<PetsModel> get noPigPetsList => _noPigPetsList;
  List<PetsModel> _justPigPetsList = [];
  List<PetsModel> get justPigPetsList => _justPigPetsList;
  //全部房子
  List<HouseModel> _allHouseList = [];
  List<HouseModel> get allHouseList => _allHouseList;
  List<HouseModel> _allHouseStyleList = [];
  List<HouseModel> get allHouseStyleList => _allHouseStyleList;

  ///編輯時的物件更換
  //壁紙
  int _editModeWallpaper = 0;
  int get editModeWallpaper => _editModeWallpaper;
  String _editModeWallpaperImagePath = '';
  String get editModeWallpaperImagePath => _editModeWallpaperImagePath;
  //狗
  int _editModeDog = 0;
  int get editModeDog => _editModeDog;
  String _editModeDogImagePath = '';
  String get editModeDogImagePath => _editModeDogImagePath;
  //圍籬
  int _editModeFence = 0;
  int get editModeFence => _editModeFence;
  String _editModeFenceImagePath = '';
  String get editModeFenceImagePath => _editModeFenceImagePath;
  //櫃子
  int _editModeCabinet = 0;
  int get editModeCabinet => _editModeCabinet;
  String _editModeCabinetImagePath = '';
  String get editModeCabinetImagePath => _editModeCabinetImagePath;
  //椅子
  int _editModeChair = 0;
  int get editModeChair => _editModeChair;
  String _editModeChairImagePath = '';
  String get editModeChairImagePath => _editModeChairImagePath;
  //桌子
  int _editModeTable = 0;
  int get editModeTable => _editModeTable;
  String _editModeTableImagePath = '';
  String get editModeTableImagePath => _editModeTableImagePath;
  //房子
  int _editModeHouse = 0;
  int get editModeHouse => _editModeHouse;
  String _editModeHouseImagePath = '';
  String get editModeHouseImagePath => _editModeHouseImagePath;
  //狗屋
  int _editModeDoghouse = 0;
  int get editModeDoghouse => _editModeDoghouse;
  String _editModeDoghouseImagePath = '';
  String get editModeDoghouseImagePath => _editModeDoghouseImagePath;
  //踏墊
  int _editModeMat = 0;
  int get editModeMat => _editModeMat;
  String _editModeMatImagePath = '';
  String get editModeMatImagePath => _editModeMatImagePath;

  ///底下是需細分的家具項目
  //壁紙
  List<FurnitureModel> _allWallpaper = [];
  List<FurnitureModel> get allWallpaper => _allWallpaper;
  //圍籬
  List<FurnitureModel> _allFence = [];
  List<FurnitureModel> get allFence => _allFence;
  //櫃子
  List<FurnitureModel> _allCabinet = [];
  List<FurnitureModel> get allCabinet => _allCabinet;
  //椅子
  List<FurnitureModel> _allChair = [];
  List<FurnitureModel> get allChair => _allChair;
  //桌子
  List<FurnitureModel> _allTable = [];
  List<FurnitureModel> get allTable => _allTable;
  //狗屋
  List<FurnitureModel> _allDoghouse = [];
  List<FurnitureModel> get allDoghouse => _allDoghouse;
  //踏墊
  List<FurnitureModel> _allMat = [];
  List<FurnitureModel> get allMat => _allMat;
  //會員已擁有家具
  List<MemberFurnitureModel> _memberFurniture = [];
  List<MemberFurnitureModel> get memberFurniture => _memberFurniture;
  //會員已擁有寵物
  List<MemberPetsModel> _memberPets = [];
  List<MemberPetsModel> get memberPets => _memberPets;
  //會員自己的豬
  List<MemberPetsModel> _memberPig = [];
  List<MemberPetsModel> get memberPig => _memberPig;
  //會員已擁有房子
  List<MemberHouseModel> _memberHouse = [];
  List<MemberHouseModel> get memberHouse => _memberHouse;
  //會員已擁有房子風格
  List<MemberHouseStyleModel> _memberHouseStyle = [];
  List<MemberHouseStyleModel> get memberHouseStyle => _memberHouseStyle;
  //會員的便便
  List<MemberDungModel> _memberDung = [];
  List<MemberDungModel> get memberDung => _memberDung;
  //會員的清理工具
  List<MemberCleaningToolsModel> _memberCleaningTools = [];
  List<MemberCleaningToolsModel> get memberCleaningTools =>
      _memberCleaningTools;

  //會員豬隻販售紀錄
  List<PetsSellRecordModel> _petsSellRecord = [];
  List<PetsSellRecordModel> get petsSellRecord => _petsSellRecord;

  //使用中的壁紙
  bool _isUsedWallpaper = false;
  bool get isUsedWallpaper => _isUsedWallpaper;
  String _usedWallpaperImagePath = '';
  String get usedWallpaperImagePath => _usedWallpaperImagePath;
  //使用中的狗
  bool _isUsedDog = false;
  bool get isUsedDog => _isUsedDog;
  String _usedDogImagePath = '';
  String get usedDogImagePath => _usedDogImagePath;
  //使用中的圍籬
  bool _isUsedFence = false;
  bool get isUsedFence => _isUsedFence;
  String _usedFenceImagePath = '';
  String get usedFenceImagePath => _usedFenceImagePath;
  //使用中的櫃子
  bool _isUsedCabinet = false;
  bool get isUsedCabinet => _isUsedCabinet;
  String _usedCabinetImagePath = '';
  String get usedCabinetImagePath => _usedCabinetImagePath;
  //使用中的椅子
  bool _isUsedChair = false;
  bool get isUsedChair => _isUsedChair;
  String _usedChairImagePath = '';
  String get usedChairImagePath => _usedChairImagePath;
  //使用中的桌子
  bool _isUsedTable = false;
  bool get isUsedTable => _isUsedTable;
  String _usedTableImagePath = '';
  String get usedTableImagePath => _usedTableImagePath;
  //使用中的房子
  bool _isUsedHouse = false;
  bool get isUsedHouse => _isUsedHouse;
  String _usedHouseImagePath = '';
  String get usedHouseImagePath => _usedHouseImagePath;
  //使用中的狗屋
  bool _isUsedDoghouse = false;
  bool get isUsedDoghouse => _isUsedDoghouse;
  String _usedDoghouseImagePath = '';
  String get usedDoghouseImagePath => _usedDoghouseImagePath;
  //使用中的踏墊
  bool _isUsedMat = false;
  bool get isUsedMat => _isUsedMat;
  String _usedMatImagePath = '';
  String get usedMatImagePath => _usedMatImagePath;

  //會員家的留言
  List<MemberHouseMessageModel> _memberHouseMessageList = [];
  List<MemberHouseMessageModel> get memberHouseMessageList =>
      _memberHouseMessageList;
  //會員家的大便
  List<MemberHouseDungModel> _memberHouseDungModelList = [];
  List<MemberHouseDungModel> get memberHouseDungModelList => _memberHouseDungModelList;

  //陌生人套組
  //使用中的寵物
  String _streetMemberPetsPath = '';
  String get streetMemberPetsPath => _streetMemberPetsPath;
  //使用中的壁紙
  String _streetMemberWallpaperPath = '';
  String get streetMemberWallpaperPath => _streetMemberWallpaperPath;
  //使用中的圍籬
  String _streetMemberFencePath = '';
  String get streetMemberFencePath => _streetMemberFencePath;
  //使用中的櫃子
  String _streetMemberCabinetPath = '';
  String get streetMemberCabinetPath => _streetMemberCabinetPath;
  //使用中的椅子
  String _streetMemberChairPath = '';
  String get streetMemberChairPath => _streetMemberChairPath;
  //使用中的桌子
  String _streetMemberTablePath = '';
  String get streetMemberTablePath => _streetMemberTablePath;
  //使用中的狗屋
  String _streetMemberDogHousePath = '';
  String get streetMemberDogHousePath => _streetMemberDogHousePath;
  //使用中的踏墊
  String _streetMemberMatPath = '';
  String get streetMemberMatPath => _streetMemberMatPath;

  //陌生人
  MemberHouseModel? _thisPeopleModel;
  MemberHouseModel get thisPeopleModel => _thisPeopleModel!;
  //陌生人家的留言
  List<MemberHouseMessageModel> _memberStreetHouseMessageList = [];
  List<MemberHouseMessageModel> get memberStreetHouseMessageList =>
      _memberStreetHouseMessageList;
  //陌生人家的大便
  List<MemberHouseDungModel> _memberStreetHouseDungModelList = [];
  List<MemberHouseDungModel> get memberStreetHouseDungModelList =>
      _memberStreetHouseDungModelList;

  ///取全部便便 <- 需暫存
  Future<void> getDung() async {
    try {
      dynamic response = await HttpUtils.get(
        Api.dung,
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      //log('全部便便: $response');
      _allDungList = await compute(parseDungModel, json.encode(response));
      if (_allDungList.isNotEmpty) {
        for (var element in _allDungList) {
          await _saveImage(url: Util().getMemberMugshotUrl(element.pic!));
        }
      }
      tidyMemberDung(refresh: false);
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      debugPrint('Error: ${_map['message']}');
    }
  }

  ///取全部動物類型
  Future<void> getAnimalType() async {
    try {
      dynamic response = await HttpUtils.get(
        Api.animalType,
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      //log('全部動物類型: $response');
      _allAnimalTypeList =
          await compute(parseAnimalTypeModel, json.encode(response));
      _setAllAnimalType();
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      debugPrint('Error: ${_map['message']}');
    }
  }

  ///取全部家具 <- 需暫存
  Future<void> getFurniture() async {
    try {
      dynamic response = await HttpUtils.get(
        Api.furniture,
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      //log('全部家具: $response');
      _allFurnitureList =
          await compute(parseFurnitureModel, json.encode(response));
      if (_allFurnitureList.isNotEmpty) {
        for (var element in _allFurnitureList) {
          await _saveImage(url: Util().getMemberMugshotUrl(element.pic!));
        }
      }
      _setAllFurniture(init: true);
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      debugPrint('Error: ${_map['message']}');
    }
  }

  ///取全部家具類型
  Future<void> getFurnitureType() async {
    try {
      dynamic response = await HttpUtils.get(
        Api.furnitureType,
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      //log('全部家具類型: $response');
      _allFurnitureTypeList =
          await compute(parseFurnitureTypeModel, json.encode(response));
      _setAllFurniture(init: true);
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      debugPrint('Error: ${_map['message']}');
    }
  }

  ///取全部寵物 <- 需暫存
  Future<void> getPets() async {
    try {
      dynamic response = await HttpUtils.get(
        Api.pets,
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      //log('全部寵物: $response');
      _allPetsList = await compute(parsePetsModel, json.encode(response));
      if (_allPetsList.isNotEmpty) {
        for (var element in _allPetsList) {
          await _saveImage(url: Util().getMemberMugshotUrl(element.pic!));
        }
        _setAllAnimalType();
      }
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      debugPrint('Error: ${_map['message']}');
    }
  }

  ///取全部房子 <- 需暫存
  Future<void> getHouse() async {
    try {
      dynamic response = await HttpUtils.get(
        Api.house,
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      //log('全部房子: $response');
      _allHouseList = await compute(parseHouseModel, json.encode(response));
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      debugPrint('Error: ${_map['message']}');
    }
  }

  ///取全部房子風格
  Future<void> getHouseStyle() async {
    try{
      dynamic response = await HttpUtils.get(
        Api.houseStyle,
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      //log('全部房子風格: $response');
      _allHouseStyleList = await compute(parseHouseModel, json.encode(response));
      if (_allHouseStyleList.isNotEmpty) {
        for (var element in _allHouseStyleList) {
          await _saveImage(url: Util().getMemberMugshotUrl(element.pic!));
        }
      }
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      debugPrint('Error: ${_map['message']}');
    }
  }

  ///用memberId取得會員的便便
  /// * [id] 會員ID
  Future<void> getMemberDungByMemberId({required int id, required bool refresh}) async {
    try {
      dynamic response = await HttpUtils.get(
        '${Api.memberDung}/byMemberId/$id',
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      log('用memberId取得會員的便便: $response');
      _memberDung = await compute(parseMemberDungModel, json.encode(response));
      tidyMemberDung(refresh: refresh);
      if(refresh){
        //log('刷新 getMemberDungByMemberId');
        notifyListeners();
      }
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      debugPrint('Error: ${_map['message']}');
    }
  }

  ///用memberId取得會員的清理工具
  /// * [id] 會員ID
  Future<void> getMemberCleaningToolsByMemberId({required int id, required bool refresh}) async {
    try {
      dynamic response = await HttpUtils.get(
        '${Api.memberCleaningTools}/byMemberId/$id',
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      //log('用memberId取得會員的清理工具: $response');
      _memberCleaningTools =
          await compute(parseMemberCleaningToolsModel, json.encode(response));
      if(refresh){
        //log('刷新 getMemberCleaningToolsByMemberId');
        notifyListeners();
      }
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      debugPrint('Error: ${_map['message']}');
    }
  }

  ///用memberId取得會員的寵物
  ///* [id] 會員ID
  Future<void> getMemberPetsByMemberId({required int id, required bool refresh}) async {
    try {
      dynamic response = await HttpUtils.get(
        '${Api.memberPets}/byMemberId/$id',
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      //log('用memberId取得會員的寵物: $response');
      _memberPets = await compute(parseMemberPetsModel, json.encode(response));
      _memberPig = _memberPets.where((element) => element.animalType == 1).toList();
      tidyMemberPets(refresh: refresh);
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      debugPrint('Error: ${_map['message']}');
    }
  }

  ///用memberId取得會員的家
  ///* [id] 會員ID
  Future<void> getMemberHouseByMemberId({required int id}) async {
    try {
      dynamic response = await HttpUtils.get(
        '${Api.memberHome}/byMemberId/$id',
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      //log('用memberId取得會員的家: $response');
      _memberHouse =
          await compute(parseMemberHouseModel, json.encode(response));
      //tidyMemberHouse();
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      debugPrint('Error: ${_map['message']}');
    }
  }

  ///用memberId取得會員家的風格
  ///* [id] 會員ID
  Future<void> getMemberHouseStyleByMemberId({required int id, required bool refresh}) async {
    try{
      dynamic response = await HttpUtils.get(
        '${Api.memberHomeStyle}/byMemberId/$id',
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      //log('用memberId取得會員家的風格: $response');
      _memberHouseStyle = await compute(parseMemberHouseStyleModel, json.encode(response));
      tidyMemberHouse(refresh: refresh);
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      debugPrint('Error: ${_map['message']}');
    }
  }

  ///用memberId取得會員家具
  ///* [id] 會員ID
  Future<void> getMemberFurnitureByMemberId({required int id}) async {
    try {
      dynamic response = await HttpUtils.get(
        '${Api.memberFurniture}/byMemberId/$id',
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      //log('用memberId取得會員家具: $response');
      _memberFurniture =
          await compute(parseMemberFurnitureModel, json.encode(response));
      tidyMemberFurniture(refresh: true);
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      debugPrint('Error: ${_map['message']}');
    }
  }

  ///會員家的留言
  /// * [id] 家的ID
  Future<void> getMemberHouseMessage({required int id}) async {
    try {
      dynamic response = await HttpUtils.get(
        '${Api.houseMessage}/byMemberHouseId/$id',
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      //log('會員家的留言: $response');
      _memberHouseMessageList = await compute(parseMemberHouseMessageModel, json.encode(response));
      notifyListeners();
    } on DioError catch (e) {
      //log('會員家的留言Error: ${e.response}');
    }
  }

  ///會員家的大便
  /// * [id] 家的ID
  Future<void> getMemberHouseDung({required int id}) async {
    try {
      dynamic response = await HttpUtils.get(
        '${Api.houseDung}/byMemberHouseId/$id',
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      //log('會員家的大便: $response');
      _memberHouseDungModelList =
          await compute(parseMemberHouseDungModel, json.encode(response));
    } on DioError catch (e) {
      //log('會員家的大便Error: ${e.response}');
    }
  }

  ///暫存圖片
  /// * [url] 圖片網址
  Future<void> _saveImage({required String url}) async {
    FileInfo? _cacheFile = await _manager.getFileFromCache(url);

    //log('_cacheFile: ${_cacheFile?.file.path}');
    //判斷是否存在緩存中
    if (_cacheFile != null) {
      if (_cacheFile.validTill.isBefore(DateTime.now())) {
        //在 但過期了，下載
        _downloadImage(url: url);
      }
    } else {
      //不在就下載
      _downloadImage(url: url);
    }
  }

  ///下載圖片
  /// * [url] 圖片網址
  void _downloadImage({required String url}) async{
    //下載並暫存
    Stream<FileResponse> _fileStream =
        _manager.getFileStream(url, withProgress: true, headers: {"authorization": "Bearer ${Api.accessToken}"});
    _fileStream.listen((event) {
      //log('暫存下載: ${event.originalUrl}');
    }, onDone: () {
      //log('暫存下載完成');
    }, onError: (e) {
      //log('暫存下載錯誤:${e.toString()}');
    });
  }

  ///點擊到的index
  /// * [value] 家具類型
  void setFurnitureTypeIndex({required int value}) {
    _furnitureTypeIndex = value;
    _itemIndex = _getFurnitureEditIndex(data: Interior.values[value]); //<-每次換項目就歸零
    List _list = _setShowList(value: Interior.values[value]);
    if (_list.isNotEmpty) {
      _showList = _getModel(list: _list, data: Interior.values[value]);
    } else {
      _showList = [];
    }
    //log('刷新 setFurnitureTypeIndex');
    notifyListeners();
  }
  //回傳index
  int _getFurnitureEditIndex({required Interior data}){
    switch (data) {
      case Interior.wallpaper:
        return _editModeWallpaper;
      case Interior.dog:
        return _editModeDog;
      case Interior.fence:
        return _editModeFence;
      case Interior.cabinet:
        return _editModeCabinet;
      case Interior.chair:
        return _editModeChair;
      case Interior.table:
        return _editModeTable;
      case Interior.doghouse:
        return _editModeDoghouse;
      case Interior.mat:
        return _editModeMat;
      case Interior.house:
        return _editModeHouse;
    }
  }

  ///選擇了哪一項
  /// * [value] 項目
  void setItemIndex({required int value}) async {
    _itemIndex = value;
    _setEditModeFurnitureIndex(
        value: value, data: Interior.values[_furnitureTypeIndex]);
    _setEditModeFurnitureImageFilePath(data: Interior.values[_furnitureTypeIndex], path: await getImage(url: _showList[value].pic!, refresh: false));
    //log('刷新 setItemIndex');
    notifyListeners();
  }

  ///設定編輯模式每個類別的Index
  /// * [value] 要設定的index
  /// * [data] 類別
  void _setEditModeFurnitureIndex(
      {required int value, required Interior data}) {
    switch (data) {
      case Interior.wallpaper:
        _editModeWallpaper = value;
        break;
      case Interior.dog:
        _editModeDog = value;
        break;
      case Interior.fence:
        _editModeFence = value;
        break;
      case Interior.cabinet:
        _editModeCabinet = value;
        break;
      case Interior.chair:
        _editModeChair = value;
        break;
      case Interior.table:
        _editModeTable = value;
        break;
      case Interior.doghouse:
        _editModeDoghouse = value;
        break;
      case Interior.mat:
        _editModeMat = value;
        break;
      case Interior.house:
        _editModeHouse = value;
        break;
    }
  }

  ///編輯模式設定
  /// * [value] 購物狀態
  void setEditMode({required bool value}) {
    _editMode = value;
    if (value) {
      _furnitureTypeIndex = 0;
      _itemIndex = 0;
      List _list = _setShowList(value: Interior.wallpaper);
      if (_list.isNotEmpty) {
        _showList = _getModel(list: _list, data: Interior.wallpaper);
      } else {
        _showList = [];
      }
      //log('刷新 setEditMode');
      notifyListeners();
    } else {
      //如果是點返回將現有更換的都換回去
      _editModeWallpaperImagePath = _usedWallpaperImagePath;
      _editModeDogImagePath = _usedDogImagePath;
      _editModeFenceImagePath = _usedFenceImagePath;
      _editModeCabinetImagePath = _usedCabinetImagePath;
      _editModeChairImagePath = _usedChairImagePath;
      _editModeTableImagePath = _usedTableImagePath;
      _editModeHouseImagePath = _usedHouseImagePath;
      _editModeDoghouseImagePath = _usedDoghouseImagePath;
      _editModeMatImagePath = _usedMatImagePath;
      //log('刷新 setEditMode');
      notifyListeners();
    }
  }

  ///留言模式
  /// * [value] 留言狀態
  void setMessageMode({required bool value}) {
    _messageMode = value;
    //log('刷新 setMessageMode');
    notifyListeners();
  }

  ///依照家具類型更換顯示的List
  List _setShowList({required Interior value}) {
    switch (value) {
      case Interior.wallpaper:
        return _allWallpaper;
      case Interior.dog:
        return _noPigPetsList;
      case Interior.fence:
        return _allFence;
      case Interior.cabinet:
        return _allCabinet;
      case Interior.chair:
        return _allChair;
      case Interior.table:
        return _allTable;
      case Interior.doghouse:
        return _allDoghouse;
      case Interior.mat:
        return _allMat;
      case Interior.house:
        return _allHouseStyleList;
      default:
        return _allWallpaper;
    }
  }

  ///整理家具List
  /// * [init] 初始化
  void _setAllFurniture({required bool init}) {
    if (_allFurnitureTypeList.isNotEmpty && _allFurnitureList.isNotEmpty) {
      for (var element in _allFurnitureTypeList) {
        switch (element.name) {
          case 'table': //桌子
            List _list = _allFurnitureList
                .where((value) => value.furnitureType == element.id && value.status == 1)
                .toList();
            if (_list.isNotEmpty) {
              _allTable = List.from(_list);
            }
            break;
          case 'chair': //椅子
            List _list = _allFurnitureList
                .where((value) => value.furnitureType == element.id && value.status == 1)
                .toList();
            if (_list.isNotEmpty) {
              _allChair = List.from(_list);
            }
            break;
          case 'cabinet': //櫃子
            List _list = _allFurnitureList
                .where((value) => value.furnitureType == element.id && value.status == 1)
                .toList();
            if (_list.isNotEmpty) {
              _allCabinet = List.from(_list);
            }
            break;
          case 'fence': //圍籬
            List _list = _allFurnitureList
                .where((value) => value.furnitureType == element.id && value.status == 1)
                .toList();
            if (_list.isNotEmpty) {
              _allFence = List.from(_list);
            }
            break;
          case 'wallpaper': //壁紙
            List _list = _allFurnitureList
                .where((value) => value.furnitureType == element.id && value.status == 1)
                .toList();
            if (_list.isNotEmpty) {
              if (init) {
                _showList = List.from(_list); //初始化
              }
              _allWallpaper = List.from(_list);
            }
            break;
          case 'doghouse': //狗屋
            List _list = _allFurnitureList
                .where((value) => value.furnitureType == element.id && value.status == 1)
                .toList();
            if (_list.isNotEmpty) {
              _allDoghouse = List.from(_list);
            }
            break;
          case 'mat': //踏墊
            List _list = _allFurnitureList
                .where((value) => value.furnitureType == element.id && value.status == 1)
                .toList();
            if (_list.isNotEmpty) {
              _allMat = List.from(_list);
            }
            break;
        }
      }
    }
  }

  ///整理寵物List <-過濾你的豬
  void _setAllAnimalType() {
    if (_allPetsList.isNotEmpty && _allAnimalTypeList.isNotEmpty) {
      for (var element in _allAnimalTypeList) {
        switch (element.name) {
          case 'pig':
            List _list = _allPetsList
                .where((value) => value.animalType != element.id)
                .toList();
            List _pig = _allPetsList
                .where((value) => value.animalType == element.id)
                .toList();
            _noPigPetsList = List.from(_list);
            _justPigPetsList = List.from(_pig);
            break;
        }
      }
    }
  }

  ///改變List<dynamic>內容
  /// * [list] 陣列
  /// * [data] 要改變的Model
  List _getModel({required List list, required Interior data}) {
    switch (data) {
      case Interior.wallpaper:
        return list.cast<FurnitureModel>();
      case Interior.dog:
        return list.cast<PetsModel>();
      case Interior.fence:
        return list.cast<FurnitureModel>();
      case Interior.cabinet:
        return list.cast<FurnitureModel>();
      case Interior.chair:
        return list.cast<FurnitureModel>();
      case Interior.table:
        return list.cast<FurnitureModel>();
      case Interior.doghouse:
        return list.cast<FurnitureModel>();
      case Interior.mat:
        return list.cast<FurnitureModel>();
      case Interior.house:
        return list.cast<HouseModel>();
    }
  }

  ///取得暫存是否有這張圖
  /// * [url] 圖片網址
  /// * [refresh] 是否刷新 <- 很重要! 會影響呈現結果
  Future<String> getImage({required String url, required bool refresh}) async {
    FileInfo? _cacheFile = await _manager.getFileFromCache(url);
    if (_cacheFile != null) {
      return _cacheFile.file.path;
    } else {
      return url;
    }
  }

  ///設定編輯模式圖片路徑
  void _setEditModeFurnitureImageFilePath(
      {required Interior data, required String path}) {
    //log('設定編輯模式圖片路徑: ${data.name} $path');
    switch (data) {
      case Interior.wallpaper:
        _editModeWallpaperImagePath = path;
        break;
      case Interior.dog:
        _editModeDogImagePath = path;
        break;
      case Interior.fence:
        _editModeFenceImagePath = path;
        break;
      case Interior.cabinet:
        _editModeCabinetImagePath = path;
        break;
      case Interior.chair:
        _editModeChairImagePath = path;
        break;
      case Interior.table:
        _editModeTableImagePath = path;
        break;
      case Interior.doghouse:
        _editModeDoghouseImagePath = path;
        break;
      case Interior.mat:
        _editModeMatImagePath = path;
        break;
      case Interior.house:
        _editModeHouseImagePath = path;
        break;
    }
  }

  ///整理使用者已擁有的家具
  void tidyMemberFurniture({required bool refresh}) async {
    if (_allFurnitureList.isNotEmpty &&
        _memberFurniture.isNotEmpty &&
        _allFurnitureTypeList.isNotEmpty) {
      for (var element in _allFurnitureList) {
        if (_memberFurniture
            .where((value) => element.id == value.furnitureId)
            .isNotEmpty) {
          element.isBuy = true;
          bool _value = _memberFurniture
              .firstWhere((value) => element.id == value.furnitureId)
              .used!;
          element.isUsed = _value;
          if (_value) {
            _setUsedFurnitureImageFilePath(
                data: _getInterior(furnitureType: element.furnitureType!),
                path: await getImage(
                    url: Util().getMemberMugshotUrl(element.pic!),
                    refresh: false));
          }
        }
      }
      _setAllFurniture(init: false);
      if(refresh){
        //log('刷新 tidyMemberFurniture');
        notifyListeners();
      }

    } else {
      Future.delayed(Duration.zero, () {
        tidyMemberFurniture(refresh: refresh);
      });
    }
  }

  ///整理使用者已擁有寵物
  void tidyMemberPets({required bool refresh}) async {
    if (_noPigPetsList.isNotEmpty && _memberPets.isNotEmpty) {
      for (var element in _noPigPetsList) {
        if (_memberPets
            .where((value) => element.id == value.petsId)
            .isNotEmpty) {
          element.isBuy = true;
          bool _value = _memberPets
              .firstWhere((value) => element.id == value.petsId)
              .used!;
          element.isUsed = _value;
          if (_value) {
            _setUsedFurnitureImageFilePath(
                data: Interior.dog,
                path: await getImage(
                    url: Util().getMemberMugshotUrl(element.pic!),
                    refresh: false));
          }
        }
      }
      _setAllAnimalType();
      if(refresh){
        //log('刷新 tidyMemberPets');
        notifyListeners();
      }

    } else if (_allPetsList.isEmpty) {
      Future.delayed(Duration.zero, (){
        tidyMemberPets(refresh: refresh);
      });
    }
  }

  ///整理使用者已擁有房子
  void tidyMemberHouse({required bool refresh}) async {
    if (_allHouseStyleList.isNotEmpty && _memberHouseStyle.isNotEmpty) {
      for (var element in _allHouseStyleList) {
        if (_memberHouseStyle
            .where((value) => element.id == value.houseStyleId)
            .isNotEmpty) {
          element.isBuy = true;
          bool _value = _memberHouseStyle
              .firstWhere((value) => element.id == value.houseStyleId)
              .used!;
          element.isUsed = _value;
          if (_value) {
            _setUsedFurnitureImageFilePath(
                data: Interior.house,
                path: await getImage(
                    url: Util().getMemberMugshotUrl(element.pic!),
                    refresh: false));
          }
        }
      }
      if(refresh){
        //log('刷新 tidyMemberHouse');
        notifyListeners();
      }
    } else {
      Future.delayed(Duration.zero, () {
        tidyMemberHouse(refresh: refresh);
      });
    }
  }

  ///整理使用者已擁有大便
  void tidyMemberDung({required bool refresh}) async {
    if (_allDungList.isNotEmpty) {
      if(_memberDung.isEmpty){
        for(var element in _allDungList){
          if(element.isBuy! || element.isUsed!){
            element.isUsed = false;
            element.isBuy = false;
          }
        }
      } else {
        for (var element in _allDungList) {
          if (_memberDung.where((value) => element.id == value.dungId).isNotEmpty) {
            element.isBuy = true;
            bool _value = _memberDung.firstWhere((value) => element.id == value.dungId).used!;
            element.isUsed = _value;
            if (_value) {
              await getImage(url: Util().getMemberMugshotUrl(element.pic!), refresh: false);
            }
          } else {
            //log('這大便沒了: ${element.name}');
            element.isBuy = false;
            element.isUsed = false;
          }
        }
      }
      if(refresh){
        //log('刷新 tidyMemberDung');
        notifyListeners();
      }
    } else {
      Future.delayed(Duration.zero, () {
        tidyMemberDung(refresh: refresh);
      });
    }
  }

  ///取得使用者使用中的家具
  void _setUsedFurnitureImageFilePath(
      {required Interior? data, required String path}) {
    switch (data) {
      case Interior.wallpaper:
        _isUsedWallpaper = true;
        _editModeWallpaperImagePath = path;
        _usedWallpaperImagePath = path;
        break;
      case Interior.dog:
        _isUsedDog = true;
        _editModeDogImagePath = path;
        _usedDogImagePath = path;
        break;
      case Interior.fence:
        _isUsedFence = true;
        _editModeFenceImagePath = path;
        _usedFenceImagePath = path;
        break;
      case Interior.cabinet:
        _isUsedCabinet = true;
        _editModeCabinetImagePath = path;
        _usedCabinetImagePath = path;
        break;
      case Interior.chair:
        _isUsedChair = true;
        _editModeChairImagePath = path;
        _usedChairImagePath = path;
        break;
      case Interior.table:
        _isUsedTable = true;
        _editModeTableImagePath = path;
        _usedTableImagePath = path;
        break;
      case Interior.doghouse:
        _isUsedDoghouse = true;
        _editModeDoghouseImagePath = path;
        _usedDoghouseImagePath = path;
        break;
      case Interior.mat:
        _isUsedMat = true;
        _editModeMatImagePath = path;
        _usedMatImagePath = path;
        break;
      case Interior.house:
        _isUsedHouse = true;
        _editModeHouseImagePath = path;
        _usedHouseImagePath = path;
        break;
      default:
        break;
    }
  }

  ///取得家具類型
  Interior? _getInterior({required int furnitureType}) {
    if (_allFurnitureTypeList
        .where((element) => element.id == furnitureType)
        .isNotEmpty) {
      String _name = _allFurnitureTypeList
          .firstWhere((element) => element.id == furnitureType)
          .name!;
      switch (_name) {
        case 'table': //桌子
          return Interior.table;
        case 'chair': //椅子
          return Interior.chair;
        case 'cabinet': //櫃子
          return Interior.cabinet;
        case 'fence': //圍籬
          return Interior.fence;
        case 'wallpaper': //壁紙
          return Interior.wallpaper;
        /*case 'bed': //床
          return Interior.bed;*/
        case 'doghouse': //狗屋
          return Interior.doghouse;
        case 'mat': //踏墊
          return Interior.mat;
      }
    }
    return null;
  }

  ///打勾後 更換現在的家具擺設
  Future<Map<String, dynamic>> setInterior() async {
    //先判斷是否有未購買項目
    if (!_getItemNotPurchased()['status']) {
      //有東西未買齊
      return {'status': false, 'value': _getItemNotPurchased()['value']};
    } else {
      return {'status': true};
    }
  }

  ///判斷是否有未購買項目
  Map<String, dynamic> _getItemNotPurchased() {
    //log('目前是? ${Interior.values[_furnitureTypeIndex].name}');
    //改成單個判斷
    switch(Interior.values[_furnitureTypeIndex]){
      case Interior.wallpaper:
        if (!_allWallpaper[_editModeWallpaper].isBuy!) {
          //log('壁紙沒買');
          return {'status': false, 'value': Interior.wallpaper};
        }
        return {'status': true};
      case Interior.dog:
        if (!_noPigPetsList[_editModeDog].isBuy!) {
          //log('寵物沒買');
          return {'status': false, 'value': Interior.dog};
        }
        return {'status': true};
      case Interior.fence:
        if (!_allFence[_editModeFence].isBuy!) {
          //log('圍籬沒買');
          return {'status': false, 'value': Interior.fence};
        }
        return {'status': true};
      case Interior.cabinet:
        if (!_allCabinet[_editModeCabinet].isBuy!) {
          //log('櫃子沒買');
          return {'status': false, 'value': Interior.cabinet};
        }
        return {'status': true};
      case Interior.chair:
        if (!_allChair[_editModeChair].isBuy!) {
          //log('椅子沒買');
          return {'status': false, 'value': Interior.chair};
        }
        return {'status': true};
      case Interior.table:
        if (!_allTable[_editModeTable].isBuy!) {
          //log('桌子沒買');
          return {'status': false, 'value': Interior.table};
        }
        return {'status': true};
      case Interior.doghouse:
        if(!_allDoghouse[_editModeDoghouse].isBuy!){
          //log('狗屋沒買');
          return {'status': false, 'value': Interior.doghouse};
        }
        return {'status': true};
      case Interior.mat:
        if (!_allMat[_editModeMat].isBuy!){
          //log('踏墊沒買');
          return {'status': false, 'value': Interior.mat};
        }
        return {'status': true};
      case Interior.house:
        if (!_allHouseStyleList[_editModeHouse].isBuy!) {
          //log('房子沒買');
          return {'status': false, 'value': Interior.house};
        }
        return {'status': true};
    }
    //region舊
    /*if (!_allWallpaper[_editModeWallpaper].isBuy!) {
      log('壁紙沒買');
      return {'status': false, 'value': Interior.wallpaper};
    } else if (!_noPigPetsList[_editModeDog].isBuy!) {
      log('寵物沒買');
      return {'status': false, 'value': Interior.dog};
    } else if (!_allFence[_editModeFence].isBuy!) {
      log('圍籬沒買');
      return {'status': false, 'value': Interior.fence};
    } else if (!_allCabinet[_editModeCabinet].isBuy!) {
      log('櫃子沒買');
      return {'status': false, 'value': Interior.cabinet};
    } else if (!_allChair[_editModeChair].isBuy!) {
      log('狗屋沒買');
      return {'status': false, 'value': Interior.chair};
    } else if (!_allTable[_editModeTable].isBuy!) {
      log('桌子沒買');
      return {'status': false, 'value': Interior.table};
    } else if (!_allHouseStyleList[_editModeHouse].isBuy!) {
      log('房子沒買');
      return {'status': false, 'value': Interior.house};
    } else if(!_allDoghouse[_editModeDoghouse].isBuy!){
      log('狗屋沒買');
      return {'status': false, 'value': Interior.doghouse};
    } else if (!_allMat[_editModeMat].isBuy!){
      log('踏墊沒買');
      return {'status': false, 'value': Interior.mat};
    } else {
      return {'status': true};
    }*/
    //endregion
  }

  ///陌生人家裡的家具
  /// * [id] 會員ID
  Future<void> getStreetMemberFurnitureByMemberId({required int id, required bool refresh}) async {
    try {
      dynamic response = await HttpUtils.get(
        '${Api.memberFurniture}/byMemberId/$id',
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      //log('陌生人家裡的家具: $response');
      List<dynamic> _list = json.decode(json.encode(response));
      List<MemberFurnitureModel> _memberFurniture = _list
          .map((e) => MemberFurnitureModel.formJson(e))
          .where((element) => element.used == true)
          .toList();
      _setMemberFurniture(memberFurniture: _memberFurniture, refresh: refresh);
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      debugPrint('Error: ${_map['message']}');
    }
  }

  void _setMemberFurniture(
      {required List<MemberFurnitureModel> memberFurniture, required bool refresh}) async {
    for (var element in memberFurniture) {
      String? _name = '';
      if(_allFurnitureTypeList.where((type) => type.id == _allFurnitureList.firstWhere((furniture) => furniture.id == element.furnitureId).furnitureType).isNotEmpty){
        _name = _allFurnitureTypeList.where((type) => type.id == _allFurnitureList.firstWhere((furniture) => furniture.id == element.furnitureId).furnitureType).first.name;
      }
      switch (_name) {
        case 'table': //桌子
          String _pic = _allFurnitureList
              .firstWhere((furniture) => furniture.id == element.furnitureId)
              .pic!;
          _streetMemberTablePath = await getImage(
              url: Util().getMemberMugshotUrl(_pic), refresh: false);
          break;
        case 'chair': //椅子
          String _pic = _allFurnitureList
              .firstWhere((furniture) => furniture.id == element.furnitureId)
              .pic!;
          _streetMemberChairPath = await getImage(
              url: Util().getMemberMugshotUrl(_pic), refresh: false);
          break;
        case 'cabinet': //櫃子
          String _pic = _allFurnitureList
              .firstWhere((furniture) => furniture.id == element.furnitureId)
              .pic!;
          _streetMemberCabinetPath = await getImage(
              url: Util().getMemberMugshotUrl(_pic), refresh: false);
          break;
        case 'fence': //圍籬
          String _pic = _allFurnitureList
              .firstWhere((furniture) => furniture.id == element.furnitureId)
              .pic!;
          _streetMemberFencePath = await getImage(
              url: Util().getMemberMugshotUrl(_pic), refresh: false);
          break;
        case 'wallpaper': //壁紙
          String _pic = _allFurnitureList
              .firstWhere((furniture) => furniture.id == element.furnitureId)
              .pic!;
          _streetMemberWallpaperPath = await getImage(
              url: Util().getMemberMugshotUrl(_pic), refresh: false);
          break;
        case 'doghouse': //狗屋
          String _pic = _allFurnitureList.firstWhere((furniture) => furniture.id == element.furnitureId).pic!;
          _streetMemberDogHousePath = await getImage(url: Util().getMemberMugshotUrl(_pic), refresh: false);
          break;
        case 'mat': //踏墊
          String _pic = _allFurnitureList.firstWhere((furniture) => furniture.id == element.furnitureId).pic!;
          _streetMemberMatPath = await getImage(url: Util().getMemberMugshotUrl(_pic), refresh: false);
          break;
        default:
          break;
      }
    }
    if(refresh){
      //log('刷新 _setMemberFurniture');
      notifyListeners();
    }
  }

  ///陌生人家裡的寵物
  /// * [id] 會員ID
  Future<void> getStreetMemberPetsByMemberId({required int id, required bool refresh}) async {
    try {
      dynamic response = await HttpUtils.get(
        '${Api.memberPets}/byMemberId/$id',
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      //log('陌生人家裡的寵物: $response');
      List<MemberPetsModel> _memberPets =
          parseMemberPetsModel(json.encode(response));
      int _streetMemberPetsId = _memberPets
          .firstWhere(
              (element) => !element.name!.contains('豬') && element.used == true)
          .petsId!;
      String _streetMemberPetsPic;
      if(_allPetsList.where((element) => element.id == _streetMemberPetsId).isNotEmpty){
        _streetMemberPetsPic = _allPetsList
            .firstWhere((element) => element.id == _streetMemberPetsId)
            .pic!;
      }else{
        _streetMemberPetsPic = _noPigPetsList.first.pic!;
      }

      _streetMemberPetsPath = await getImage(
          url: Util().getMemberMugshotUrl(_streetMemberPetsPic),
          refresh: false);
      if(refresh){
        //log('刷新 getStreetMemberPetsByMemberId');
        notifyListeners();
      }
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      debugPrint('Error: ${_map['message']}');
    }
  }

  ///陌生人家裡的便便
  /// * [id] 會員ID
  Future<void> getStreetMemberDungByMemberHouseId({required int id}) async {
    try {
      dynamic response = await HttpUtils.get(
        '${Api.houseDung}/byMemberHouseId/$id',
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      log('陌生人家裡的便便: $response');
      _memberStreetHouseDungModelList = await compute(parseMemberHouseDungModel, json.encode(response));
      notifyListeners();
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      debugPrint('Error: ${_map['message']}');
    }
  }

  ///陌生人家裡的留言
  /// * [id] 家的ID
  Future<void> getStreetMemberHouseMessage({required int id}) async {
    try {
      dynamic response = await HttpUtils.get(
        '${Api.houseMessage}/byMemberHouseId/$id',
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      log('陌生人家的留言: $response');
      _memberStreetHouseMessageList = await compute(parseMemberHouseMessageModel, json.encode(response));
    } on DioError catch (e) {
      //log('陌生人家裡的留言Error: ${e.response}');
    }
  }

  ///陌生人家裡的HouseId
  ///* [id] 會員ID
  Future<void> getThisPeopleHouseByMemberId({required int id}) async {
    try {
      dynamic response = await HttpUtils.get(
        '${Api.memberHome}/byMemberId/$id',
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      //log('陌生人家裡的HouseId: $response');
      List<MemberHouseModel> _memberHouse =
          parseMemberHouseModel(json.encode(response));
      _thisPeopleModel =
          _memberHouse.firstWhere((element) => element.used == true);
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      debugPrint('Error: ${_map['message']}');
    }
  }

  ///新增會員家訊息
  /// * [model] 新增訊息
  Future<Map<String, dynamic>> addMemberHouseMessage(
      {required MemberHouseMessageDTO model}) async {
    //log('新增會員家訊息上傳值: ${model.toJson()}');
    try {
      dynamic response = await HttpUtils.post(
        Api.houseMessage,
        data: model.toJson(),
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      //log('新增會員家訊息: $response');
      MemberHouseMessageModel _model =
          MemberHouseMessageModel.fromJson(json.decode(json.encode(response)));
      return {'status': true, 'value': _model};
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      debugPrint('Error: ${_map['message']}');
      return {'status': false};
    }
  }

  ///新增會員家大便
  /// * [model] 新增大便
  Future<bool> addMemberHouseDung({required MemberHouseDungModel model}) async {
    bool _result = false;
    try {
      dynamic response = await HttpUtils.post(
        Api.houseDung,
        data: model.toJson(),
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      //log('新增會員家大便: $response');
      _result = true;
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      debugPrint('Error: ${_map['message']}');
      _result = false;
    }
    return _result;
  }

  ///取得留言者詳細資訊
  /// * [id] 對方ID
  Future<MemberModel?> getDungMember({required int id}) async {
    try{
      dynamic response = await HttpUtils.get(
        '${Api.member}/id/$id',
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      //log('取得會員資料response: $response');
      return MemberModel.fromJson(json.decode(json.encode(response)));
    } on DioError catch (e) {
      //log('取得會員資料Error: ${e.response!.statusCode}');
      return null;
    }
  }
  ///設定豬列表
  void setMemberPig(String response){
    List<MemberPetsModel> _memberPets = parseMemberPetsModel(response);
    _memberPig = _memberPets.where((element) => element.animalType == 1).toList();
    //log('刷新 setMemberPig');
    notifyListeners();
  }
  ///取得大便照片
  /// * [id] 大便id
  String getDungImage({required int id}) {
    String _path = _allDungList.firstWhere((element) => element.id == id).pic!;
    String url = Util().getMemberMugshotUrl(_path);
    /*FileInfo? _cacheFile = await _manager.getFileFromCache(url);
    if(_cacheFile != null){
      return _cacheFile.file.path;
    } else {*/
      return url;
    //}
  }
  ///取得寵物出售紀錄
  /// * [id] 會員ID
  Future<bool> getPetsSellRecord({required int id}) async {
    bool _result = false;
    try{
      dynamic response = await HttpUtils.get(
        '${Api.pestSellRecord}/byMemberId/$id',
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer ${Api.accessToken}"}),
      );
      //log('取得寵物出售紀錄response: $response');
      _petsSellRecord = await compute(parsePetsSellRecordModel, json.encode(response));
      _petsSellRecord.sort((left, right) => right.createdAt!.compareTo(left.createdAt!));
      _result = true;
      notifyListeners();
    } on DioError catch (e) {
      //log('取得寵物出售紀錄Error: ${e.response!.statusCode}');
      _result = false;
    }
    return _result;
  }
  //恢復大便次數
  Future<bool> postRecoverShitTimes(int id) async {
    bool _result = false;
    log('恢復大便次數: ${Api.recoverShitTimes}/$id');
    try{
      dynamic response = await HttpUtils.post(
        '${Api.recoverShitTimes}/$id',
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer " + Api.accessToken}),
      );
      log('恢復大便次數response: $response');
      _result = true;
    } on DioError catch (e){
      //log('恢復大便次數Error: ${e.response!.statusCode}');
      _result = false;
    }
    return _result;
  }

  //恢復清理次數
  Future<bool> postRecoverCleanTimes(int id) async {
    bool _result = false;
    try{
      dynamic response = await HttpUtils.post(
          '${Api.recoverCleanTimes}/$id',
          options: Options(contentType: 'application/json', headers: {"authorization": "Bearer " + Api.accessToken}),
      );
      log('恢復清理次數response: $response');
      _result = true;
    } on DioError catch (e){
      //log('恢復清理次數Error: ${e.response!.statusCode}');
      _result = false;
    }
    return _result;
  }
}
