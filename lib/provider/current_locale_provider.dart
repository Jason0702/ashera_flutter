import 'package:flutter/cupertino.dart';

class CurrentLocaleProvider with ChangeNotifier{
  //預設語系
  Locale _locale = const Locale('zh', 'Hant');
  //取得語系
  Locale get value => _locale;
  //設定語系
  void setLocale(Locale locale){
    _locale = locale;
    notifyListeners();
  }
}