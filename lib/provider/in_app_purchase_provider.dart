import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';

import 'package:ashera_flutter/enum/app_enum.dart';
import 'package:dio/dio.dart';
import 'package:dio_http_package/dio_http_package.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:in_app_purchase_storekit/store_kit_wrappers.dart';

import '../app.dart';
import '../models/event_bus_model.dart';
import '../models/google_iap.dart';
import '../models/member_model.dart';
import '../utils/api.dart';

class InAppPurchaseProvider with ChangeNotifier{

  final Set<String> _kIds = <String>{'com.sj.ashera_flutter.mycoin100','com.sj.ashera_flutter.mycoin'};

  final InAppPurchase _inAppPurchase = InAppPurchase.instance;
  late StreamSubscription<List<PurchaseDetails>> _subscription;

  List<ProductDetails> _products = [];
  List<ProductDetails> get products => _products;

  List<Color> _borderColorList = [];
  List<Color> get borderColorList => _borderColorList;
  List<Color> _backgroundColorList = [];
  List<Color> get backgroundColorList => _backgroundColorList;

  String _storeOrderNumber = '';

  void appPurchaseInit(){
    final Stream<List<PurchaseDetails>> purchaseUpdated = _inAppPurchase.purchaseStream;
    _subscription = purchaseUpdated.listen((List<PurchaseDetails> purchaseDetailsList) {
      _listenToPurchaseUpdated(purchaseDetailsList);
    }, onDone: (){
      _subscription.cancel();
    }, onError: (error){
      //handle error here
      debugPrint('Handle error here: $error');
    });
  }

  void appPurchaseStoreInfoInit(Set<String> kIds) async {
    final bool isAvailable = await _inAppPurchase.isAvailable();
    if(isAvailable){
      final ProductDetailsResponse response = await _inAppPurchase.queryProductDetails(kIds);
      if(response.notFoundIDs.isNotEmpty){
        //Handle the error
        debugPrint('Handle the error: response.notFoundIDs');
      }
      _products = response.productDetails;
      _products.sort((left, right) => left.rawPrice.compareTo(right.rawPrice));
      _borderColorList = List.generate(_products.length, (index) => Colors.grey[400]!);
      _backgroundColorList = List.generate(_products.length, (index) => Colors.white);
      notifyListeners();
    } else {
      //Show placeholder UI
      debugPrint('Unfortunately store is not available');
    }
  }

  ///狀態判斷
  void _listenToPurchaseUpdated(List<PurchaseDetails> purchaseDetailsList)async{
    for (var purchaseDetails in purchaseDetailsList) {
      if(purchaseDetails.status == PurchaseStatus.pending){
        debugPrint('App Purchase pending UI');
      } else {
        if(purchaseDetails.status == PurchaseStatus.error){
          //錯誤
          debugPrint('App Purchase show error UI and handle errors');
        } else if(purchaseDetails.status == PurchaseStatus.purchased ||
            purchaseDetails.status == PurchaseStatus.restored){
          bool valid = await _verifyPurchase(purchaseDetails);
          if(valid){
            //購買成功
            debugPrint('App Purchase deliver products');
            recharge.fire(RechargeEventModel('IAP', null));
          } else {
            //
            debugPrint('App Purchase show invalid purchase UI and invalid purchases');
            recharge.fire(RechargeEventModel('IAP', null));
            continue;
          }
          if (purchaseDetails.pendingCompletePurchase) {
            await InAppPurchase.instance
              .completePurchase(purchaseDetails);
          }
        }
      }
    }
  }

  Future<bool> _verifyPurchase(PurchaseDetails purchaseDetails){
    //Verify the purchase in your backed, and return true if valid.
    //訂單編號
    debugPrint(purchaseDetails.purchaseID);
    debugPrint(purchaseDetails.productID);
    debugPrint(purchaseDetails.transactionDate);
    //驗證Token
    debugPrint(purchaseDetails.verificationData.serverVerificationData);
    debugPrint(purchaseDetails.verificationData.source);
    if(Platform.isIOS){
      IosPayDTO _dto = IosPayDTO(
          priceId: int.parse(purchaseDetails.purchaseID!),
          transactionId: purchaseDetails.purchaseID,
          payload: purchaseDetails.verificationData.serverVerificationData,
          storeOrderNumber: _storeOrderNumber,
          storeOrderType: StoreOrderType.ADD_POINTS.index,
      );
      return Future.value(_applePay(_dto));
    }else if(Platform.isAndroid){
      PurchaseGetProductDTO _data = PurchaseGetProductDTO(
        packageName: 'com.sj.ashera_flutter',
        productId: purchaseDetails.productID,
        token: purchaseDetails.verificationData.serverVerificationData,
        storeOrderNumber: _storeOrderNumber,
        storeOrderType: StoreOrderType.ADD_POINTS.index,
      );
      return Future.value(_postGetProduct(_data));
    }
    return Future.value(false);
  }

  //購買
  void byConsumable(PurchaseParam purchaseParam) async {
    if(Platform.isIOS){
      final queueWrapper = SKPaymentQueueWrapper();
      final transactions = await queueWrapper.transactions();
      await Future.wait(transactions.map((e) => queueWrapper.finishTransaction(e)));
    }
    _inAppPurchase.buyConsumable(purchaseParam: purchaseParam);
  }

  void setStoreOrderNumber(String value){
    _storeOrderNumber = value;
  }

  //Google內購驗證
  Future<bool> _postGetProduct(PurchaseGetProductDTO data) async {
    try{
      dynamic response = await HttpUtils.post(
        Api.getProduct,
        data: data.toJson(),
        options: Options(contentType: 'application/json', headers: {
          "authorization": "Bearer " + Api.accessToken
        }),
      );
      //log('Google內購驗證: $response');
      GoogleIAPModel _model = GoogleIAPModel.fromJson(json.decode(json.encode(response)));
      if(_model.consumptionState == PurchaseStatus.purchased.index){
        return true;
      }
      return false;
    }on DioError catch (e) {
      //log('Google內購驗證Error: ${e.response}');
      return false;
    }
  }
  //Apple內購驗證
  Future<bool> _applePay(IosPayDTO _dto) async {
    //log('upLoad: ${_dto.toJson()}');
    try{
      dynamic response = await HttpUtils.post(
        Api.iosPay,
        data: _dto.toJson(),
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer " + Api.accessToken}),
      );
      //log('Apple內購驗證: $response');
      return true;
    } on DioError catch (e) {
      //log('Apple內購驗證Error: ${e.response!.statusCode}');
      return false;
    }
  }

  void setBorderColor(Color color,int index){
    _borderColorList[index] = color;
    notifyListeners();
  }

  void setBackgroundColor(Color color, int index){
    _backgroundColorList[index] = color;
    notifyListeners();
  }
}