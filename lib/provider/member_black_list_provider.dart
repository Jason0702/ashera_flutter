import 'dart:convert';
import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:dio_http_package/dio_http_package.dart';
import 'package:flutter/cupertino.dart';

import '../app.dart';
import '../models/member_model.dart';
import '../utils/api.dart';
import '../utils/shared_preference.dart';

class MemberBlackListProvider with ChangeNotifier{
  SharedPreferenceUtil sharedPreferenceUtil = SharedPreferenceUtil();

  ///黑名單列表 我封鎖的
  List<MemberBlackModel> _blackListByFromMember = [];
  List<MemberBlackModel> get blackListByFromMember => _blackListByFromMember;
  ///黑名單列表 封鎖我的
  List<MemberBlackModel> _blackListByTargetMember = [];
  List<MemberBlackModel> get blackListByTargetMember => _blackListByTargetMember;

  ///新增會員黑名單
  /// * [memberBlacklistDTO] MemberBlacklistDTO
  Future<bool> addMemberBlackList({required MemberBlacklistDTO memberBlacklistDTO}) async {
    bool _result = false;
    try{
      dynamic response = await HttpUtils.post(
        Api.memberBlacklist,
        data: memberBlacklistDTO.toJson(),
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer " + await sharedPreferenceUtil.getAccessToken()}),
      );
      //log('新增黑名單: $response');
      _result = true;
    } on DioError catch (e){
      //log('新增黑名單Error: ${e.response}');
      _result = false;
    }
    return _result;
  }

  ///刪除會員黑名單
  /// * [id] 解除封鎖用資料的id
  Future<bool> deleteMemberBlackList({required int id}) async {
    bool _result = false;
    try{
      dynamic response = await HttpUtils.delete(
        '${Api.memberBlacklist}/$id',
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer " + await sharedPreferenceUtil.getAccessToken()}),
      );
      //log('刪除會員黑名單: $response');
      if(json.decode(json.encode(response))){
        appDb.deleteBlacklistMember(id);
      }
      _result = true;
    } on DioError catch(e) {
      //log('刪除會員黑名單Error: ${e.response}');
      _result = false;
    }
    return _result;
  }

  ///誰封鎖了我
  /// * [id] 自己的id
  Future<void> getMemberBlackListByTargetMemberId({required int id}) async {
    try{
      dynamic response = await HttpUtils.get(
        '${Api.memberBlacklist}/byTargetMemberId/$id',
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer " + await sharedPreferenceUtil.getAccessToken()}),
      );
      //log('誰封鎖了我: $response');
      List _list = json.decode(json.encode(response));
      _blackListByTargetMember = List.from(_list.map((e) => MemberBlackModel.fromJson(e)).toList());
    } on DioError catch (e) {
      //log('誰封鎖了我Error: ${e.response}');
    }
  }

  ///我封鎖了誰
  /// * [id] 自己的id
  Future<void> getMemberBlackListByFromMemberId({required int id}) async {
    try{
      dynamic response = await HttpUtils.get(
        '${Api.memberBlacklist}/byFromMemberId/$id',
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer " + await sharedPreferenceUtil.getAccessToken()}),
      );
      //log('我封鎖了誰: $response');
      List _list = json.decode(json.encode(response));
      List<MemberBlackModel> _blacklist = List.from(_list.map((e) => MemberBlackModel.fromJson(e)).toList());
      if(_blacklist.isEmpty){
        await appDb.deleteBlacklist();
      } else {
        for(var element in _blacklist.toSet()){
          if(await _isAddBlacklistToDb(element)){
            await appDb.insertBlacklist(element);
          }
        }
      }

      getBlacklist();
    } on DioError catch (e){
      //log('我封鎖了誰Error: ${e.response}');
    }
  }

  Future<bool> _isAddBlacklistToDb(MemberBlackModel model) async {
    List<MemberBlackModel> _list = await appDb.queryBlacklist(model.id!);
    if(_list.isEmpty){
      return true;
    } else {
      return false;
    }
  }

  Future<void> getBlacklist() async {
    List<MemberBlackModel> _list = await appDb.getBlacklist();
    _blackListByFromMember = List.from(_list);
    notifyListeners();
  }
}