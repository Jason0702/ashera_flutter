import 'dart:async';
import 'dart:convert';
import 'dart:developer';

import 'package:ashera_flutter/enum/app_enum.dart';
import 'package:ashera_flutter/models/add_unlock_payment_record_dto.dart';
import 'package:ashera_flutter/models/chat_message_model.dart';
import 'package:ashera_flutter/models/event_bus_model.dart';
import 'package:ashera_flutter/models/message_page_record_model.dart';
import 'package:ashera_flutter/models/peekaboo_record_dto.dart';
import 'package:ashera_flutter/models/update_member_location_dto.dart';
import 'package:ashera_flutter/models/visit_record_dto.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_app_badger/flutter_app_badger.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

import 'package:stomp_dart_client/stomp.dart';
import 'package:stomp_dart_client/stomp_config.dart';
import 'package:stomp_dart_client/stomp_frame.dart';

import '../app.dart';
import '../models/add_follower_request_dto.dart';
import '../models/dung_model.dart';
import '../models/follower_model.dart';
import '../models/friend_name_model.dart';
import '../models/game_dto.dart';
import '../models/get_chat_message_dto.dart';
import '../models/member_model.dart';
import '../models/message_dto.dart';
import '../models/message_record_model.dart';
import '../utils/api.dart';
import '../utils/shared_preference.dart';

class StompClientProvider with ChangeNotifier{
  final SharedPreferenceUtil _sharedPreferenceUtil = SharedPreferenceUtil();

  StompClient? stompClient;
  String username = '';

  STOMPStatus _stompStatus = STOMPStatus.onDisconnect;
  STOMPStatus get stompStatus => _stompStatus;

  List<ChatMessageModel> _chatMessageList = [];
  List<ChatMessageModel> get chatMessageList => _chatMessageList;

  List<VisitChatMessageModel> _visitChatRecordList = [];
  List<VisitChatMessageModel> get visitChatRecordList => _visitChatRecordList;

  bool _isRoomNewMessage = false;
  bool get isRoomNewMessage => _isRoomNewMessage;

  List<MemberModel> _friendList = [];
  List<MemberModel> get friendList => _friendList;

  List<MessageLastRecordModel> _messageRecordList = [];
  List<MessageLastRecordModel> get messageRecordList => _messageRecordList;

  List<VisitMessageLastRecordModel> _visitMessageLastRecordDataList = [];
  List<VisitMessageLastRecordModel> get visitMessageLastRecordDataList => _visitMessageLastRecordDataList;

  int _allUnreadCunt = 0;
  int get allUnreadCunt => _allUnreadCunt;

  bool _isContinue = false;
  bool get isContinue => _isContinue;

  //通知 想加我的
  List<FollowerModel> _wantAddMeList = [];
  List<FollowerModel> get wantAddMeList => _wantAddMeList;

  //待加好友
  List<FollowerModel> _followerList = [];
  List<FollowerModel> get followerList => _followerList;

  FollowerType _followerType = FollowerType.wantAddMe;
  FollowerType get followerType => _followerType;

  List<MemberFriendNameModel> _friendNameList = [];

  bool _isMePushDeleteFriend = false;

  int _visitId = 0;
  int get visitId => _visitId;


  void onCreation(String _username, String _password){
    username = _username;
    stompClient = StompClient(
        config: StompConfig.SockJS(
            url: Stomp.baseUrl, //您要連接的服務器的網址（必填）
            reconnectDelay: const Duration(milliseconds: 5000), //重新連接嘗試之間的持續時間。如果您不想自動重新連接，請設置為 0 ms
            heartbeatOutgoing: const Duration(milliseconds: 5000), //傳出心跳消息之間的持續時間。設置為 0 ms 不發送任何心跳
            heartbeatIncoming: const Duration(milliseconds: 0), //傳入心跳消息之間的持續時間。設置為 0 ms 不接收任何心跳
            connectionTimeout: const Duration(milliseconds: 5000), //等待連接嘗試中止的持續時間
            onConnect: onConnect, //當客戶端成功連接到服務器時要調用的函數。
            onDisconnect: onDisconnect, //客戶端預期斷開連接時調用的函數
            onStompError: onStompError, //當 stomp 服務器發送錯誤幀時要調用的函數
            //beforeConnect: beforeConnect, //在建立連接之前將等待的異步函數。
            onWebSocketError: onWebSocketError,//WebSocketError
            stompConnectHeaders: {"username":_username, "password":_password}, //將在 STOMP 連接幀上使用的可選標頭值
            webSocketConnectHeaders: {"username":_username, "password":_password}, //連接到底層 WebSocket 時將使用的可選標頭值（Web 中不支持）
            onUnhandledFrame: onUnhandledFrame, //服務器發送無法識別的幀時調用的函數
            onUnhandledMessage: onUnhandledMessage, //當訂閱消息沒有處理程序時要調用的函數
            onUnhandledReceipt: onUnhandledReceipt, //當接收消息沒有註冊觀察者時調用的函數
            onWebSocketDone: onWebSocketDone, //當底層 WebSocket 完成/斷開連接時要調用的函數
            onDebugMessage: onDebugMessage, //為內部消息處理程序生成的調試消息調用的函數
        )
    );
    _activate();
  }

  //當客戶端成功連接到服務器時要調用的函數。
  void onConnect(StompFrame frame){
    log('onConnect');
    _stompStatus = STOMPStatus.onConnect;
    //點對點訊息通道
    _subscribe(Stomp.p2pMsg);
    //點對點
    _subscribe(Stomp.p2pMsgResp);
    //點對點會員資料通道
    _subscribe(Stomp.p2pMember);
    //點對點發送消息，將資料發送到指定用戶
    _subscribe(Stomp.followMe);
    //點對點聊天紀錄
    _subscribe(Stomp.p2pGetChatMsgResp);
    //取得最後聊天訊息回傳
    _subscribe(Stomp.p2pGetLastChatMsgResp);
    //監聽請求好友資料
    _subscribe(Stomp.p2pFollowRequest);
    //監聽允許好友資料
    _subscribe(Stomp.p2pAcceptFollowRequest);
    //更新未讀聊天訊息回傳
    _subscribe(Stomp.p2pUpdateUnreadChatMsgResp);

    //想要探班
    _subscribe(Stomp.p2pWannaVisit);
    //探班評分資料通道
    _subscribe(Stomp.p2pRateVisit);
    //同意探班
    _subscribe(Stomp.p2pApproveVisit);
    //取消探班
    _subscribe(Stomp.p2pCancelVisit);
    //重新配對
    _subscribe(Stomp.p2pRePairingVisit);

    //同意取消探班資料通道
    _subscribe(Stomp.p2pAgreeToCancelVisit);
    //探班會員取消探班資料通道
    _subscribe(Stomp.p2pCancelByFromMemberVisit);
    //進行中探班會員取消探班資料通道
    _subscribe(Stomp.p2pCancelByFromMemberInProcessingVisit);
    //探班會員請求取消探班資料通道
    _subscribe(Stomp.p2pCancelBackInWaitingCancel);

    //躲貓貓資料通道
    _subscribe(Stomp.p2pHideAndSeek);
    //躲貓貓抓資料通道
    _subscribe(Stomp.p2pDoCatchHideAndSeek);

    //解鎖
    _subscribe(Stomp.p2pUnlock);

    //錯誤訊息
    _subscribe(Stomp.p2pError);

    //小遊戲
    _subscribe(Stomp.p2pShopping);
    _subscribe(Stomp.p2pStuff);
    _subscribe(Stomp.p2pClear);

    //充值
    _subscribe(Stomp.p2pPayment);

    //發送探班聊天
    _subscribe(Stomp.p2pVisitChatMsgResp);
    //取得聊天紀錄
    _subscribe(Stomp.p2pGetVisitChatMsgResp);
    //對方探班新訊息監聽
    _subscribe(Stomp.p2pVisitMsg);
    //取得最後一筆
    _subscribe(Stomp.p2pGetVisitLastChatMsgResp);
    //更新已讀
    _subscribe(Stomp.p2pUpdateUnreadVisitChatMsgResp);

    //連線後取得好友
    sendMessage(Stomp.getFollowMe, '');
    sendMessage(Stomp.getLastChatMessage, username);
    getRequestFollowerMe();
    notifyListeners();
  }

  //客戶端預期斷開連接時調用的函數
  void onDisconnect(StompFrame frame){
    log('onDisconnect: ${frame.body}');
    _stompStatus = STOMPStatus.onDisconnect;
    notifyListeners();
  }

  //當 stomp 服務器發送錯誤幀時要調用的函數
  void onStompError(StompFrame frame){
    log('onStompError: ${frame.body}');
    _stompStatus = STOMPStatus.onDisconnect;
    notifyListeners();
  }

  //服務器發送無法識別的幀時調用的函數
  void onUnhandledFrame(StompFrame frame){
    log('onUnhandledFrame: ${frame.body}');
  }

  //當訂閱消息沒有處理程序時要調用的函數
  void onUnhandledMessage(StompFrame frame){
    log('onUnhandledMessage: ${frame.body}');
  }

  //當接收消息沒有註冊觀察者時調用的函數
  void onUnhandledReceipt(StompFrame frame){
    log('onUnhandledReceipt: ${frame.body}');
  }

  //當底層 WebSocket 完成/斷開連接時要調用的函數
  void onWebSocketDone(){
    log('onWebSocketDone');
    _stompStatus = STOMPStatus.onDisconnect;
    notifyListeners();
  }

  //為內部消息處理程序生成的調試消息調用的函數
  void onDebugMessage(String value){
    log('onDebugMessage: $value');
  }

  //WebSocketError
  void onWebSocketError(dynamic error){
    log('onWebSocketError ${error.toString()}');
    deactivate();
    Future.delayed(const Duration(milliseconds: 2000), () async {
      onCreation(await _sharedPreferenceUtil.getAccount(), await _sharedPreferenceUtil.getPassword());
    });
  }


  //連接
  void _activate(){
    if(!stompClient!.isActive){
      stompClient!.activate();
    }
  }
  //斷線
  void deactivate(){
    if(stompClient!.isActive){
      stompClient!.deactivate();
    }
  }

  //傳送訊息
  void sendMessage(String _url, String _value) {
    if(stompClient!.isActive){
      stompClient!.send(destination: _url, body: _value);
    }
  }

  //訂閱
  void _subscribe(String _url){
    stompClient!.subscribe(destination: _url, callback: (frame) {
      switch(_url){
        case Stomp.p2pMsg: //如果是聊天 <- 對方傳給我的
          _messageData(frame); //聊天訊息處理
          break;
        case Stomp.p2pMsgResp: //如果是聊天 <- 自己的
          _messageData(frame); //聊天訊息處理
          break;
        case Stomp.p2pGetChatMsgResp:
          _messageRecordData(frame); //聊天訊息處理
          break;
        case Stomp.followMe://如果是好友
          _friendData(frame);
          break;
        case Stomp.p2pGetLastChatMsgResp://取得最後聊天訊息回傳
          _messageLastRecordData(frame);
          break;
        case Stomp.p2pFollowRequest: //請求好友資料通道
          _followRequest(frame);
          break;
        case Stomp.p2pAcceptFollowRequest: //允許好友資料通道
          _acceptFollowerRequest(frame.body!);
          break;
        case Stomp.p2pUpdateUnreadChatMsgResp: //更新未讀聊天訊息回傳
          sendMessage(Stomp.getLastChatMessage, username);//取得未讀
          break;
        case Stomp.p2pServerPushMsg://服務器推送訊息
          log('p2pServerPushMsg: ${frame.body}');
          break;
        case Stomp.p2pError:
          log('p2pError: ${frame.body}');
          if(EasyLoading.isShow){
            EasyLoading.dismiss();
          }
          break;
        case Stomp.p2pMember:
          log('p2pMember: ${frame.body}');
          _memberInfo(frame);
          break;
        case Stomp.p2pWannaVisit: // 想要探班資料通道
          _wannaVisit(frame);
          break;
        case Stomp.p2pRateVisit: // 探班評分資料通道
          log('p2pRateVisit: ${frame.body}');
          _rateVisit(frame);
          break;
        case Stomp.p2pApproveVisit: // 同意探班資料通道
          _approveVisit(frame);
          break;
        case Stomp.p2pCancelVisit: // 取消探班資料通道
          log('p2pCancelVisit: ${frame.body}');
          _cancelVisit(frame);
          break;
        case Stomp.p2pRePairingVisit: // 重配對探班資料通道
          log('p2pRePairingVisit: ${frame.body}');
          _rePairingVisit(frame);
          break;
        case Stomp.p2pAgreeToCancelVisit: // 同意取消探班資料通道
          log('p2pAgreeToCancelVisit: ${frame.body}');
          _agreeToCancelVisit(frame);
          break;
        case Stomp.p2pCancelByFromMemberVisit: // 探班會員取消探班資料通道
          log('p2pCancelByFromMemberVisit: ${frame.body}');
          break;
        case Stomp.p2pCancelByFromMemberInProcessingVisit: // 進行中探班會員取消探班資料通道
          log('p2pCancelByFromMemberInProcessingVisit: ${frame.body}');
          _cancelByFromMemberInProcessingVisit(frame);
          break;
        case Stomp.p2pCancelBackInWaitingCancel://探班會員請求取消探班資料通道
          log('p2pCancelBackInWaitingCancel: ${frame.body}');
          _cancelBackInWaitingCancel(frame);
          break;
        case Stomp.p2pUnlock: //解鎖
          log('p2pUnlock: ${frame.body}');
          _unlock(frame);
          break;
        case Stomp.p2pHideAndSeek://躲貓貓資料通道
          log('p2pHideAndSeek: ${frame.body}');
          _hideAndSeek(frame);
          break;
        case Stomp.p2pDoCatchHideAndSeek://躲貓貓抓資料通道
          log('p2pDoCatchHideAndSeek: ${frame.body}');
          _doCatchHideAndSeek(frame);
          break;
        case Stomp.p2pShopping://小遊戲購買
          _shopping(frame);
          break;
        case Stomp.p2pStuff://小遊戲更換
          log('runType: ${json.decode(frame.body!).runtimeType}');
          if(json.decode(frame.body!).runtimeType != List){
            _shopping(frame);
          } else {
            _checkPetsStatus(frame);
          }
          break;
        case Stomp.p2pClear://大便清理
          _clear(frame);
          break;
        case Stomp.p2pPayment://充值通到
          log('p2pPayment: ${frame.body}');
          _recharge(frame);
          break;
        case Stomp.p2pVisitChatMsgResp:
          _visitMessageData(frame);
          break;
        case Stomp.p2pVisitMsg: //<-對方的
          if(_visitId != 0){
            GetLastVisitChatMessageDTO _dto = GetLastVisitChatMessageDTO(username: username, visitRecordId: _visitId);
            sendMessage(Stomp.getVisitLastChatMessage, json.encode(_dto.toJson()));
          }
          _visitMessageData(frame);
          break;
        case Stomp.p2pGetVisitChatMsgResp:
          _visitChatRecordData(frame);//探班聊天紀錄
          break;
        case Stomp.p2pGetVisitLastChatMsgResp:
          _visitMessageLastRecordData(frame);
          break;
        case Stomp.p2pUpdateUnreadVisitChatMsgResp:
          if(_visitId != 0){
            GetLastVisitChatMessageDTO _dto = GetLastVisitChatMessageDTO(username: username, visitRecordId: _visitId);
            sendMessage(Stomp.getVisitLastChatMessage, json.encode(_dto.toJson()));
          }
          break;
      }
    });
  }
  //刷新使用者資訊
  void _memberInfo(StompFrame frame) async {
    memberUpgrade.fire('refresh');
  }

  //聊天處理
  void _messageData(StompFrame frame) async {
    String _otherSide = '';//好友的名子
    Map<String, dynamic> _map = Map.from(json.decode(frame.body!));
    ChatMessageModel _chatMessageModel = ChatMessageModel.fromJson(_map);
    log('解析後: ${_chatMessageModel.toJson().toString()}');
    await appDb.insertMessage(_chatMessageModel);
    if(_chatMessageModel.targetMember == username){
      _otherSide = _chatMessageModel.fromMember!;
    }else{
      _otherSide = _chatMessageModel.targetMember!;
    }
    getMessage(_otherSide);//取得訊息
    sendMessage(Stomp.getLastChatMessage, username);//取得未讀
  }

  //聊天紀錄處理
  void _messageRecordData(StompFrame frame) async{
    if(frame.body.runtimeType == String){
      String _otherSide = '';//好友的名子
      Map<String, dynamic> _map = Map.from(json.decode(frame.body!));
      MessagePageRecordModel _messagePageRecord = MessagePageRecordModel.fromJson(_map);
      log('messageRecordData: ${_messagePageRecord.toJson()}');
      if(_messagePageRecord.content.isNotEmpty){
        if(_messagePageRecord.content.first.targetMember == username){
          _otherSide = _messagePageRecord.content.first.fromMember!;
        }else{
          _otherSide = _messagePageRecord.content.first.targetMember!;
        }

        for (var element in _messagePageRecord.content) {
          if(await _isAddMessageToDb(element)){
            await appDb.insertMessage(element);
          }
        }

        if(_messagePageRecord.pageable!.pageNumber! == 0){ //第一次進入才已讀
          //updateIsRead(_messagePageRecord.content.first);
          _isRoomNewMessage = true;
          Future.delayed(const Duration(milliseconds: 300), (){ //刷新畫面
            getMessage(_otherSide);//取得訊息
          });
          notifyListeners();
        }else{
          Future.delayed(const Duration(milliseconds: 300), (){ //刷新畫面
            getMessage(_otherSide);//取得訊息
          });
        }
        if(_messagePageRecord.totalPages! != 0 && _isContinue){ //還有之前的紀錄
          //取之前的紀錄
          GetChatMessageDTO _getChatMessageDTO = GetChatMessageDTO(
              fromMember: username,
              targetMember: _otherSide,
              date: '', page: _messagePageRecord.pageable!.pageNumber! + 1, size: 10, sortBy: 'created_at');
          sendMessage(Stomp.getChatMessagePage, json.encode(_getChatMessageDTO));
        }
      }

    }
  }

  //聊天頁未讀筆數
  void _messageLastRecordData(StompFrame frame){
    List<dynamic> _list = List.from(json.decode(frame.body!));
    List<MessageLastRecordModel> _messageLastRecordData = _list.map((e) => MessageLastRecordModel.fromJson(e)).where((element) => element.targetMember != username || (element.fromMember != username && !element.block!)).toList();//解析
    _messageLastRecordData.sort((first, last) => first.updatedAt!.compareTo(last.updatedAt!));
    _messageRecordList = List.from(_messageLastRecordData);
    if(_friendList.isNotEmpty){
      _messageAndFriendSort();//訊息排序
    }
  }

  //訊息時間更改
  void _messageAndFriendSort() async{
    for (var message in _messageRecordList) {
      String _otherSide = '';
      if(message.fromMember != username){
        _otherSide = message.fromMember!;
      }else{
        _otherSide = message.targetMember!;
      }
      if(_friendList.where((element) => element.name == _otherSide).isNotEmpty){
        MemberModel _member = _friendList.where((element) => element.name == _otherSide).first;
        _member.updatedAt = message.updatedAt;
        await appDb.updateFriendUpdateAt(_member);//更新時間
      }
    }
    getAllUnreadCunt();
  }

  //新信息狀態變更
  void setRoomNewMessage(){
    _isRoomNewMessage = false;
    notifyListeners();
  }

  //好友處理
  void _friendData(StompFrame frame) async{
    log('好友處理');
    if(json.decode(frame.body!).runtimeType != bool){
      final List<dynamic> list = List.from(json.decode(frame.body!));
      List<FollowerModel> _followerMeList = list.map((e) => FollowerModel.fromJson(e)).toList();
      for (var element in _followerMeList.toSet()){
        MemberModel member = MemberModel.fromJson(json.decode(json.encode(element.memberModel!)));
        //新增好友進入DB
        if(await _isAddFriendToDb(member)){
          await appDb.insertFriend(member);
        }
      }

      getFriends();
      
      _refreshFriend(_followerMeList);
    } else {
      if(!json.decode(frame.body!)){
        friend.fire('deleteFail');
      } else {
        if(_isMePushDeleteFriend){
          friend.fire('deleteSuccess');
          _isMePushDeleteFriend = false;
        }
        //重新取得好友
        sendMessage(Stomp.getFollowMe, '');
      }
    }
  }
  void _refreshFriend(List<FollowerModel> _followerMeList) async {
    if(_friendList.isNotEmpty && _followerMeList.isNotEmpty){
      for (var element in _friendList) {
        if(_followerMeList.where((value) => element.id == value.memberModel!.id!).isEmpty){
          await appDb.deleteFriend(element.id!).then((value) => getFriends());
        }
      }
    } else if(_followerMeList.isEmpty){
      await appDb.deleteAllFriend().then((value) => getFriends());
    }
  }
  
  //從DB拿好友
  void getFriends() async{
    List<MemberModel> _list = await appDb.getDbFriends();
    _friendList = List.from(_list.toSet());
    //訊息排序
    if(_messageRecordList.isNotEmpty){
      _messageAndFriendSort();
    }
    notifyListeners();
  }

  //判斷好友是否能加入DB
  Future<bool> _isAddFriendToDb(MemberModel _friend) async{
    List<MemberModel> _list = await appDb.queryFriend(_friend.id!);
    if(_list.isEmpty){//如果回來是空就加
      return true;
    }else{//如果不是空
      List<MemberModel> _copyFriend = [];
      _copyFriend.add(_friend);
      if(!listEquals(_list, _copyFriend) && _friendNameList.where((element) => element.targetMemberId == _friend.id).isNotEmpty){//如果不同 就更新
        if(_friendNameList.firstWhere((element) => element.targetMemberId == _friend.id).targetMemberName != _friendList.firstWhere((element) => element.id == _friend.id).nickname){
          log('需更改的好友${_friend.toJson()}');
          await appDb.updateFriend(_friend);
        }else if(_friendList.isEmpty){
          //不用考慮備註
          await appDb.updateFriend(_friend);
        }
      }
    }
    return false;
  }

  //清除好友
  void clearFriend(){
    if(_friendList.isNotEmpty){
      _friendList.clear();
      notifyListeners();
    }
  }

  //清除聊天訊息
  void clearChatMessage(){
    if(_chatMessageList.isNotEmpty){
      _chatMessageList.clear();
      notifyListeners();
    }
  }

  //設定好友頭像為空
  void setFriendAvatarNull(String _name){
    _friendList.where((element) => element.name == _name).first.mugshot = null;
    notifyListeners();
  }

  //取得總未讀數
  void getAllUnreadCunt(){
    _allUnreadCunt = 0;
    for (var element in _messageRecordList) {
      if(element.fromMember != username && friendList.where((value) => value.name == element.fromMember || value.name == element.targetMember).isNotEmpty){
        _allUnreadCunt += element.unreadCnt!;
      }
    }
    FlutterAppBadger.updateBadgeCount(_allUnreadCunt);
    debugPrint('總訊息數: $_allUnreadCunt');
    FlutterAppBadger.isAppBadgeSupported().then((value) => log('總訊息數: $value'));
    notifyListeners();
  }

  //已讀狀態變更
  void updateIsRead(ChatMessageModel _data){
    sendMessage(Stomp.updateChatMessageIsRead, json.encode(_data));
    FlutterAppBadger.removeBadge();
  }

  //判斷訊息是否能加入DB
  Future<bool> _isAddMessageToDb(ChatMessageModel _chatMessage) async {
    String _otherSide = '';
    if(_chatMessage.targetMember == username){
      _otherSide = _chatMessage.fromMember!;
    }else{
      _otherSide = _chatMessage.targetMember!;
    }
    if(_chatMessage.fromMember != username && _chatMessage.block == 1){
      return false;
    }
    List<ChatMessageModel> _list = await appDb.queryMessage(_otherSide);
    if(_list.isEmpty){//如果回空就加
      return true;
    }else{
      if(_list.where((element) => element == _chatMessage).isEmpty){
        return true;
      }
    }
    return false;
  }

  //從DB拿訊息
  void getMessage(String _friendName) async {
      List<ChatMessageModel> _list = await appDb.queryMessage(_friendName);
      _chatMessageList = List.from(_list);
      _chatMessageList.sort((first, last) => last.createdAt!.compareTo(first.createdAt!));
      notifyListeners();
  }

  void setContinue(bool _value){
    _isContinue = _value;
    if(_value){
      notifyListeners();
    }
  }

  //接受好友
  void sendFollowerRequestAccept(int _myId, int _followerId, bool _approve) async {
    AcceptFollowerRequestDTO _addFollowerRequestDTO = AcceptFollowerRequestDTO(
        followerId: _followerId,
        memberId: _myId,
        approve: _approve,
    );
    sendMessage(Stomp.getAcceptFollowerRequest, json.encode(_addFollowerRequestDTO.toJson()));
  }

  //發送好友
  void sendFollowerRequest(int _myId, int _followerId) async {
    AddFollowerRequestDTO _addFollowerRequestDTO = AddFollowerRequestDTO(
        followerId: _followerId,
        memberId: _myId);
    sendMessage(Stomp.getAddFollowerRequest, json.encode(_addFollowerRequestDTO.toJson()));
  }

  //好友更新返回
  void _acceptFollowerRequest(String _value){
    if(_value == 'true'){
      sendMessage(Stomp.getFollowMe, '');
      getRequestFollowerMe();
    }
  }

  //請求好友資料通道
  void _followRequest(StompFrame frame){
    if(frame.body != 'true'){
      if(_followerType == FollowerType.wantAddMe){
        _setWantAddMeList(frame.body!);
      }else{
        _setFollowerList(frame.body!);
      }
    } else {
      _acceptFollowerRequest(frame.body!);
    }
  }

  //通知頁 誰想加我
  void getRequestFollowerMe(){
    _followerType = FollowerType.wantAddMe;
    sendMessage(Stomp.getFollowerMe, '');
  }

  //獲取我的追隨請求者列表
  void getRequestMyFollower(){
    _followerType = FollowerType.follower;
    sendMessage(Stomp.getMyFollower, '');
  }
  //獲取我的追隨請求者列表 資料新增
  void _setFollowerList(String _value){
    if(json.decode(_value).runtimeType == List){
      final List<dynamic> list = json.decode(_value);
      _followerList = list.map((e) => FollowerModel.fromJson(e)).toList();
      notifyListeners();
    } else {
      sendMessage(Stomp.getMyFollower, '');
    }
  }
  //通知頁 誰想加我 資料新增
  void _setWantAddMeList(String _value){
    if(json.decode(_value).runtimeType == List){
      final List<dynamic> list = json.decode(_value);
      _wantAddMeList = list.map((e) => FollowerModel.fromJson(e)).toList();
      notifyListeners();
    } else {
      sendMessage(Stomp.getFollowerMe, ''); //獲取誰想加我
    }
  }

  ///設定備註
  /// * [friendName] 好友名稱
  Future<void> setFriendName({required List<MemberFriendNameModel> friendName}) async{
    if(friendName.isNotEmpty){
      _friendNameList = List.from(friendName);
      for (var element in friendName) {
        List<MemberModel> _memberList = await appDb.queryFriend(element.targetMemberId!);
        if(_memberList.isNotEmpty){
          MemberModel _member = _memberList.first;
          _member.nickname = element.targetMemberName;
          await appDb.updateFriendNickName(_member);
        }
      }
      //重新取得好友列表
      List<MemberModel> _list = await appDb.getDbFriends();
      _friendList = List.from(_list.toSet());
      notifyListeners();
    }
  }


  ///發送 我要探班
  /// * [wannaVisitDto] 想探班DTO
  void sendWannaVisit({required WannaVisitDTO wannaVisitDto}){
    log('我要探班 ${wannaVisitDto.toJson()}');
    sendMessage(Stomp.wannaVisit, json.encode(wannaVisitDto.toJson()));
  }
  ///發送 同意探班
  /// * [approveVisitRecord] 意或不同意DTO
  void sendApprove({required ApproveVisitRecordDTO approveVisitRecord}){
    log('同意探班: ${approveVisitRecord.toJson()}');
    sendMessage(Stomp.approveVisit, json.encode(approveVisitRecord));
  }
  ///發送 取消探班
  /// * [id]
  void sendCancelVisit({required int id}){
    sendMessage(Stomp.cancelVisit, '$id');
  }
  ///發送 探班會員取消
  /// * [id]
  void sendCancelByFromMember({required int id}){
    sendMessage(Stomp.cancelByFromMember, '$id');
  }
  ///發送 在進行中被 探班會員取消
  /// * [id] 訂單的id
  void sendCancelRecordByFromMemberInProcessing({required int id}){
    sendMessage(Stomp.cancelRecordByFromMemberInProcessing, '$id');
  }
  ///發送 在進行中被 被探班會員取消
  /// * [id] 訂單的id
  void sendCancelRecordByTargetMemberInProcessing({required int id}){
    sendMessage(Stomp.cancelRecordByTargetMemberInProcessing, '$id');
  }

  ///發送 同意取消探班
  /// * [approveVisitRecord] 同意或不同意DTO
  void sendAgreeToCancel({required ApproveVisitRecordDTO approveVisitRecord}){
    sendMessage(Stomp.agreeToCancelVisit, json.encode(approveVisitRecord));
  }

  ///發送 探班會員同意取消探班
  /// * [approveVisitRecord] 同意或不同意DTO
  void sendAgreeToCancelByFromMember({required ApproveVisitRecordDTO approveVisitRecord}){
    sendMessage(Stomp.agreeToCancelByFromMember, json.encode(approveVisitRecord));
  }

  ///發送 被探班會員同意取消
  /// * [approveVisitRecord] 同意或不同意DTO
  void sendAgreeToCancelByTargetMember({required ApproveVisitRecordDTO approveVisitRecord}){
    sendMessage(Stomp.agreeToCancelByTargetMember, json.encode(approveVisitRecord));
  }

  ///發送 重新配對
  /// * [id] 自己的id
  void sendRePairing({required int id}){
    sendMessage(Stomp.reParingVisit, '$id');
  }
  ///發送 探班會員評分
  /// * [rateVisitRecord] 評分DTO
  void sendFromMemberRateVisit({required RateVisitRecordDTO rateVisitRecord}){
    sendMessage(Stomp.fromMemberRateVisit, json.encode(rateVisitRecord.toJson()));
  }
  ///發送 被探班會員評分
  /// * [rateVisitRecord] 評分DTO
  void sendTargetMemberRateVisit({required RateVisitRecordDTO rateVisitRecord}){
    sendMessage(Stomp.targetMemberRateVisit, json.encode(rateVisitRecord.toJson()));
  }
  ///發送 探班會員 請求取消
  /// * [id] 自己的id
  void sendCancelBackByFromMember({required int id}){
    sendMessage(Stomp.cancelBackByFromMember, '$id');
  }
  ///發送 被探班會員 請求取消
  /// * [id] 自己的id
  void sendCancelBackByTargetMember({required int id}){
    sendMessage(Stomp.cancelBackByTargetMember, '$id');
  }

  ///發送 解鎖大頭照
  /// * [addUnlockPaymentRecordDTO] 解鎖大頭照DTO
  void sendUnlockMugshot({required AddUnlockPaymentRecordDTO addUnlockPaymentRecordDTO}){
    sendMessage(Stomp.unlockMugshot, json.encode(addUnlockPaymentRecordDTO));
  }

  ///發送 躲貓貓 開始
  /// * [id] 自己的id
  void sendStartHideAndSeek({required int id}){
    sendMessage(Stomp.startHideAndSeek, '$id');
  }

  ///發送 躲貓貓 取消
  /// * [id] 自己的id
  void sendCancelHideAndSeek({required int id}){
    sendMessage(Stomp.cancelHideAndSeek, '$id');
  }

  ///發送 躲貓貓 抓取
  /// * [recordDTO] CatchHideAndSeekRecordDTO
  void sendDoCatchHideAndSeek({required CatchHideAndSeekRecordDTO recordDTO}){
    sendMessage(Stomp.doCatchHideAndSeek, json.encode(recordDTO));
  }

  ///發送 設定躲貓貓上線狀態
  /// * [status] 上線狀態
  void sendUpdateHideStatus({required OnlineStatus status}){
    sendMessage(Stomp.updateHideStatus, '${status.index}');
  }

  ///發送 設定躲貓貓抓取類型
  /// * [status] 抓取類型
  void sendUpdateHideType({required Catch status}){
    sendMessage(Stomp.updateHideType, '${status.index}');
  }

  ///發送 更新會員位置
  /// * [location] 用戶位置
  void sendUpdateMemberLocation({required UpdateMemberLocationDTO location}){
    sendMessage(Stomp.updateMemberLocation, json.encode(location));
  }

  ///發送 購買房子
  /// * [buyHouseDTO] UpdateMemberHouseDTO
  void sendBuyHouse({required UpdateMemberHouseStyleDTO buyHouseDTO}){
    sendMessage(Stomp.buyHouseStyle, json.encode(buyHouseDTO));
  }
  ///發送 購買家具
  /// * [buyFurnitureDTO] UpdateMemberFurnitureDTO
  void sendBuyFurniture({required UpdateMemberFurnitureDTO updateMemberFurnitureDTO}){
    log('購買家具發送內容: ${updateMemberFurnitureDTO.toJson()}');
    sendMessage(Stomp.buyFurniture, json.encode(updateMemberFurnitureDTO));
  }
  ///發送 購買寵物
  /// * [buyPetsDTO] UpdateMemberPetsDTO
  void sendBuyPets({required UpdateMemberPetsDTO buyPetsDTO}){
    sendMessage(Stomp.buyPets, json.encode(buyPetsDTO));
  }

  ///發送 購買VIP
  void sendMemberUpgrade(){
    sendMessage(Stomp.memberUpgrade, '');
  }

  ///發送 刪除好友
  void sendDeleteFriend({required AddFollowerRequestDTO addFollowerRequestDTO}){
    _isMePushDeleteFriend = true;
    sendMessage(Stomp.deleteFollowerByMemberIdAndFollowerId, json.encode(addFollowerRequestDTO));
  }

  ///發送 遊戲初始化
  void sendMemberInitGame(){
    sendMessage(Stomp.memberInitGame, '');
  }
  ///發送 改變家具
  void sendChangeMemberFurniture({required ChangeItemDTO changeItemDTO}){
    sendMessage(Stomp.changeMemberFurniture, json.encode(changeItemDTO));
  }
  ///發送 改變寵物
  void sendChangeMemberPets({required ChangeItemDTO changeItemDTO}){
    sendMessage(Stomp.changeMemberPets, json.encode(changeItemDTO));
  }
  ///發送 改變房子
  void sendChangeMemberHouse({required ChangeStyleDTO changeStyleDTO}){
    sendMessage(Stomp.changeMemberHouseStyle, json.encode(changeStyleDTO));
  }
  ///發送 購買清理工具
  void sendAddMemberCleaningTools({required AddMemberItemDTO addMemberItemDTO}){
    sendMessage(Stomp.addMemberCleaningTools, json.encode(addMemberItemDTO));
  }
  ///發送 購買大便
  void sendAddMemberDung({required AddMemberItemDTO addMemberItemDTO}){
    sendMessage(Stomp.addMemberDung, json.encode(addMemberItemDTO));
  }

  ///發送 使用清潔工具
 /* void sendUseMemberCleaningTools({required UseItemDTOForMemberHouse useItemDTOForMemberHouse}){
    sendMessage(Stomp.useMemberCleaningToolsForMemberHouseDung, json.encode(useItemDTOForMemberHouse));
  }*/
  ///發送 檢查動物狀態
  void sendCheckPetsStatus(){
    sendMessage(Stomp.checkPetsStatus, '');
  }
  ///發送 出售寵物
  void sendSellPets({required int id}){
    sendMessage(Stomp.sellPets, '$id');
  }
  ///發送 檢查大便狀態
  void sendCheckMemberDefaultDung(){
    sendMessage(Stomp.checkMemberDefaultDung, '');
  }
  ///發送 讀取會員家中訊息
  /// * [id] 訊息id
  void sendReadMemberHouseMessage({required int id}){
    sendMessage(Stomp.readMemberHouseMessage, '$id');
  }
  ///發送 檢查清潔工具狀態
  void sendCheckMemberDefaultCleaningTools(){
    sendMessage(Stomp.checkMemberDefaultCleaningTools, '');
  }
  ///發送 使用大便
  void sendUseMemberDung({required UseItemDTO model}){
    sendMessage(Stomp.useMemberDung, json.encode(model));
  }

  ///清除單一大便
  /// * [model] ClearOneMemberHouseDungDTO
  void sendClearOneMemberHouseDung({required ClearOneMemberHouseDungDTO model}){
    sendMessage(Stomp.useMemberCleaningToolsForMemberHouseDung, json.encode(model));
}

  ///發送 加血
  void sendAddMemberPetsHp({required UpdateMemberPetsHpDTO updateMemberPetsHpDTO}){
    sendMessage(Stomp.addMemberPetsHp, json.encode(updateMemberPetsHpDTO));
  }

  ///接收 想要探班
  void _wannaVisit(StompFrame frame){
    visitBus.fire(VisitEventModel('wannaVisit', frame));
  }
  ///接收 同意探班
  void _approveVisit(StompFrame frame){
    visitBus.fire(VisitEventModel('approveVisit', frame));
  }
  ///接收 探班評分
  void _rateVisit(StompFrame frame){
    visitBus.fire(VisitEventModel('rateVisit', frame));
  }
  ///接收 取消探班
  void _cancelVisit(StompFrame frame){
    visitBus.fire(VisitEventModel('cancelVisit', frame));
  }
  ///接收 重配對探班
  void _rePairingVisit(StompFrame frame){
    visitBus.fire(VisitEventModel('rePairingVisit', frame));
  }
  ///接收 進行中探班會員取消探班資料通道
  void _cancelByFromMemberInProcessingVisit(StompFrame frame){
    visitBus.fire(VisitEventModel('cancelByFromMemberInProcessingVisit', frame));
  }
  ///接收 同意取消探班資料通道
  void _agreeToCancelVisit(StompFrame frame){
    visitBus.fire(VisitEventModel('agreeToCancelVisit', frame));
  }
  ///接收 探班會員請求取消探班資料通道
  void _cancelBackInWaitingCancel(StompFrame frame){
    visitBus.fire(VisitEventModel('cancelBackInWaitingCancel', frame));
  }
  ///接收 解鎖大頭照
  void _unlock(StompFrame frame){
    visitBus.fire(VisitEventModel('unlock', frame));
  }

  ///接收 躲貓貓資料通道
  void _hideAndSeek(StompFrame frame){
    visitBus.fire(VisitEventModel('hideAndSeek', frame));
  }
  ///接收 躲貓貓抓資料通道
  void _doCatchHideAndSeek(StompFrame frame){
    visitBus.fire(VisitEventModel('doCatchHideAndSeek', frame));
  }

  //接收 購買資料通道
  void _shopping(StompFrame frame){
    log('購買資料通道: ${frame.body}');
    if(json.decode(frame.body!).runtimeType == bool){
      if(json.decode(frame.body!)){
        //刷新
        log('p2pStuff 刷新');
        gameShopping.fire('checkMemberDefault');
      }else{
        //不刷新
        log('p2pStuff 不刷新');
      }
    }else{
      log('p2pStuff 跑哪?');
      log('物品購買 物品更換 ');
      gameShopping.fire('memberShoppingRefresh');
    }
  }

  //接收 豬隻資料
  void _checkPetsStatus(StompFrame frame){
    log('檢查寵物狀態');
    checkPetsStatus.fire(RechargeEventModel('pets', frame));
  }

  //接收 大便清除
  void _clear(StompFrame frame){
    log('大便是否能清除');
    checkPetsStatus.fire(RechargeEventModel('message', frame));
  }
  //接收 充值資料通道
  void _recharge(StompFrame frame){
    recharge.fire(RechargeEventModel('Payment', frame));
  }

  //探班聊天處理
  void _visitMessageData(StompFrame frame) {
    VisitChatMessageModel _chatMessageModel = VisitChatMessageModel.fromJson(json.decode(frame.body!));
    _visitChatRecordList.add(_chatMessageModel);
    _visitChatRecordList.sort((first, last) => last.createdAt!.compareTo(first.createdAt!));
    notifyListeners();
  }

  //探班聊天紀錄處理
  void _visitChatRecordData(StompFrame frame){
    List _list = json.decode(frame.body!);
    _visitChatRecordList = _list.map((e) => VisitChatMessageModel.fromJson(e)).toList();
    _visitChatRecordList.sort((first, last) => last.createdAt!.compareTo(first.createdAt!));
    notifyListeners();
  }
  //探班已讀狀態變更
  void updateVisitIsRead(ChatMessageModel _data){
    sendMessage(Stomp.updateVisitChatMessageIsRead, json.encode(_data));
    FlutterAppBadger.removeBadge();
  }
  //探班聊天頁未讀筆數
  void _visitMessageLastRecordData(StompFrame frame){
    List _list = List.from(json.decode(frame.body!));
    _visitMessageLastRecordDataList = _list.map((e) => VisitMessageLastRecordModel.fromJson(e)).where((element) => element.targetMember != username || (element.fromMember != username && !element.block!)).toList();//解析
    _visitMessageLastRecordDataList.sort((first, last) => first.updatedAt!.compareTo(last.updatedAt!));
    notifyListeners();
  }

  void setVisitId({required int id}){
    _visitId = id;
  }
}