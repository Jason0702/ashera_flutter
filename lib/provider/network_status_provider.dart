import 'dart:async';
import 'dart:developer';

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/cupertino.dart';

class NetworkStatusProvider with ChangeNotifier{
  ConnectivityResult _connectivityResult = ConnectivityResult.none;
  ConnectivityResult get connectivityResult => _connectivityResult;

  late StreamSubscription _connectivitySubscription;

  void init(){
    _connectivitySubscription = Connectivity().onConnectivityChanged.listen((result) {
      resultHandler(result);
    });
  }

  void resultHandler(ConnectivityResult result){
    _connectivityResult = result;
    log('Currently connected: $result');
    notifyListeners();
  }

  void cancel(){
    _connectivitySubscription.cancel();
  }
}