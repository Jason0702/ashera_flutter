import 'dart:convert';
import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:dio_http_package/dio_http_package.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

import '../enum/app_enum.dart';
import '../models/faces_detect_history_model.dart';
import '../models/member_mugshot_model.dart';
import '../utils/api.dart';
import '../utils/app_color.dart';
import '../utils/parse_json.dart';
import '../utils/util.dart';

class IdentificationProvider with ChangeNotifier{
  //辯識過我的還是我辯識過的
  FacesMember _facesMember = FacesMember.facesTargetMember;
  FacesMember get facesMember => _facesMember;

  //辨識紀錄 總和
  final List<FacesDetectHistoryModel> _facesDetectHistoryRecord = [];
  List<FacesDetectHistoryModel> get facesDetectHistoryRecord => _facesDetectHistoryRecord;

  //辨識紀錄 本周
  List<FacesDetectHistoryModel> _facesDetectHistoryWeekRecord = [];
  List<FacesDetectHistoryModel> get facesDetectHistoryWeekRecord => _facesDetectHistoryWeekRecord;

  //辯識紀錄 辯識過我的
  List<FacesDetectHistoryModel> _facesTargetMemberDetectHistoryRecord = [];
  List<FacesDetectHistoryModel> get facesTargetMemberDetectHistoryRecord =>
      _facesTargetMemberDetectHistoryRecord;

  //辨識紀錄 我辯識過的
  List<FacesDetectHistoryModel> _facesFromMemberDetectHistoryRecord = [];
  List<FacesDetectHistoryModel> get facesFromMemberDetectHistoryRecord =>
      _facesFromMemberDetectHistoryRecord;

  //設定顯示 辯識過我的還是我辯識過的
  void setFacesMember({required FacesMember faces}){
    if (_facesMember == faces) {
      return;
    }
    _facesMember = faces;
    notifyListeners();
  }



  ///辨識紀錄 用ID取得大頭照路徑
  Future<String> getIdentificationMemberById({required int id}) async {
    String _value = '';
    try {
      dynamic response = await HttpUtils.get(
        '${Api.member}/id/$id',
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer " + Api.accessToken}),
      );
      //log('用ID取得大頭照路徑: $response');
      MemberMugshotModel _memberMugshot =
      MemberMugshotModel.fromJson(json.decode(json.encode(response)));
      _value = _memberMugshot.mugshot ?? '';
    } on DioError catch (e) {
      //log('用ID取得大頭照路徑: ${e.response!.statusCode}');
      _value = '';
    }
    return _value;
  }

  ///使用 TargetMemberId 獲取辨識紀錄
  /// * [id] ID
  Future<bool> getFacesDetectHistoryByTargetMemberId({required int id}) async {
    bool _result = false;
    try {
      dynamic response = await HttpUtils.get(
        '${Api.faceDetectHistory}/byTargetMemberId/$id',
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer " + Api.accessToken}),
      );
      //log('使用 TargetMemberId 獲取辨識紀錄: $response');
      _facesTargetMemberDetectHistoryRecord =
      await compute(parseFacesDetectHistoryModel, json.encode(response));
      _facesTargetMemberDetectHistoryRecord.sort((left, right) => right.createdAt!.compareTo(left.createdAt!));
      _result = true;
    } on DioError catch (e) {
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      //log('使用 TargetMemberId 獲取辨識紀錄Error: ${_map['message']}');
      _result = false;
    }
    return _result;
  }

  ///使用 FromMemberId 獲取辨識紀錄
  /// * [id] ID
  Future<bool> getFacesDetectHistoryByFromMemberId({required int id}) async {
    bool _result = false;
    try {
      dynamic response = await HttpUtils.get(
        '${Api.faceDetectHistory}/byFromMemberId/$id',
        options: Options(contentType: 'application/json', headers: {"authorization": "Bearer " + Api.accessToken}),
      );
      //log('使用 FromMemberId 獲取辨識紀錄: $response');
      _facesFromMemberDetectHistoryRecord =
      await compute(parseFacesDetectHistoryModel, json.encode(response));
      _facesFromMemberDetectHistoryRecord.sort((left, right) => right.createdAt!.compareTo(left.createdAt!));
      _result = true;
    } on DioError catch (e) {
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      //log('使用 FromMemberId 獲取辨識紀錄Error: ${_map['message']}');
      _result = false;
    }
    return _result;
  }
  //取得全部
  Future<bool> getFacesDetectHistoryByTargetMemberIdAndFromMemberId(int id) async {
    _facesDetectHistoryRecord.clear();
    List<bool> _r = await Future.wait([
      getFacesDetectHistoryByFromMemberId(id: id),
      getFacesDetectHistoryByTargetMemberId(id: id)
    ]);
    if(_r[0] && _r[1]){
      _facesDetectHistoryRecord.addAll(_facesFromMemberDetectHistoryRecord);
      _facesDetectHistoryRecord.addAll(_facesTargetMemberDetectHistoryRecord);
      _facesDetectHistoryRecord.shuffle();
      List<FacesDetectHistoryModel> _recordList = List.from(_facesTargetMemberDetectHistoryRecord.where((element) => Util().isWeek(element.createdAt!.ceil(), isUtc: false)).toList());
      //為了周次數同人不重複
      if(_recordList.isNotEmpty){
        for (var record in _recordList) {
          if(_facesDetectHistoryWeekRecord.where((element) => element.targetMemberId == record.targetMemberId && element.fromMemberId == record.fromMemberId).isEmpty){
            _facesDetectHistoryWeekRecord.add(record);
          }
        }
      }
      _facesDetectHistoryWeekRecord.sort((first, last) => last.createdAt!.compareTo(first.createdAt!));
      notifyListeners();
      return true;
    }
    return false;
  }
}

class IdentificationUtil{
  static EdgeInsets userStatusMargin(FacesMember _facesMember, FacesMember set) {
    if (_facesMember == set) {
      return const EdgeInsets.all(3);
    } else {
      return const EdgeInsets.all(0);
    }
  }

  static Color userStatusTextColor(FacesMember _facesMember, FacesMember set) {
    if (_facesMember == set) {
      return AppColor.buttonTextColor;
    } else {
      return AppColor.grayText;
    }
  }

  static Color userStatusButtonColor(FacesMember _facesMember, FacesMember set) {
    if (_facesMember == set) {
      return AppColor.yellowButton;
    } else {
      return Colors.transparent;
    }
  }
}