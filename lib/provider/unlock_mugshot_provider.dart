import 'dart:convert';
import 'dart:developer';

import 'package:ashera_flutter/utils/shared_preference.dart';
import 'package:dio/dio.dart';
import 'package:dio_http_package/dio_http_package.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';

import '../models/member_mugshot_model.dart';
import '../models/unlock_payment_record_model.dart';
import '../utils/api.dart';
import '../utils/parse_json.dart';
import '../utils/util.dart';

class UnlockMugshotProvider with ChangeNotifier {
  SharedPreferenceUtil sharedPreferenceUtil = SharedPreferenceUtil();

  //我付費解鎖了哪些人
  List<UnlockPaymentRecordModel> _unlockPaymentRecord = [];
  List<UnlockPaymentRecordModel> get unlockPaymentRecord =>
      _unlockPaymentRecord;

  //解鎖的用戶大頭照
  MemberMugshotModel? _memberMugshot;
  final List<MemberMugshotModel> _memberMugshotList = [];
  List<MemberMugshotModel> get memberMugshotList => _memberMugshotList;

  //使用 fromMemberId 取得解鎖付費紀錄
  Future<bool> getUnlockPaymentRecordByFromMemberId(
      {required int id, required int expiration}) async {
    bool _result = false;
    try {
      dynamic response = await HttpUtils.get(
        '${Api.unlockPaymentRecord}/byFromMemberId/$id',
        options: Options(contentType: 'application/json', headers: {
          "authorization":
              "Bearer " + await sharedPreferenceUtil.getAccessToken()
        }),
      );
      log('使用 fromMemberId 取得解鎖付費紀錄: $response');
      final List<dynamic> _list = json.decode(json.encode(response));
      _unlockPaymentRecord = _list
          .map((e) => UnlockPaymentRecordModel.fromJson(e))
          .where((element) => Util().getDifference(element.createdAt!.ceil()) < expiration)
          .toList();
      _memberMugshotList.clear();
      if (_unlockPaymentRecord.isNotEmpty) {
        for (var element in _unlockPaymentRecord) {
          await getMemberById(id: element.targetMemberId!);
        }
      }
      _result = true;
    } on DioError catch (e) {
      //錯誤
      Map<String, dynamic> _map = {};
      _map = json.decode(e.response.toString());
      //log('使用 fromMemberId 取得解所付費紀錄Error: ${_map['message']}');
      _result = false;
    }
    return _result;
  }

  //解鎖 用ID取得大頭照路徑
  Future<bool> getMemberById({required int id}) async {
    bool _result = false;
    try {
      dynamic response = await HttpUtils.get(
        '${Api.member}/id/$id',
        options: Options(contentType: 'application/json', headers: {
          "authorization":
              "Bearer " + await sharedPreferenceUtil.getAccessToken()
        }),
      );
      log('解鎖 用ID取得大頭照路徑: $response');
      _memberMugshot =
          MemberMugshotModel.fromJson(json.decode(json.encode(response)));
      _memberMugshotList.add(_memberMugshot!);
      _result = true;
      notifyListeners();
    } on DioError catch (e) {
      //log('解鎖 用ID取得大頭照路徑: ${e.response!.statusCode}');
      _result = false;
    }
    return _result;
  }
}
