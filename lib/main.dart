import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:io';
import 'dart:ui';

import 'package:ashera_flutter/provider/auth_provider.dart';
import 'package:ashera_flutter/provider/current_locale_provider.dart';
import 'package:ashera_flutter/provider/drawer_vm.dart';
import 'package:ashera_flutter/provider/game_shopping_refresh_provider.dart';
import 'package:ashera_flutter/provider/game_street_provider.dart';
import 'package:ashera_flutter/provider/identification_provider.dart';
import 'package:ashera_flutter/provider/in_app_purchase_provider.dart';
import 'package:ashera_flutter/provider/member_black_list_provider.dart';
import 'package:ashera_flutter/provider/member_provider.dart';
import 'package:ashera_flutter/provider/menu_status_provider.dart';
import 'package:ashera_flutter/provider/message_provider.dart';
import 'package:ashera_flutter/provider/network_status_provider.dart';
import 'package:ashera_flutter/provider/packages_info_provider.dart';
import 'package:ashera_flutter/provider/peekaboo_game_provider.dart';
import 'package:ashera_flutter/provider/position_provider.dart';
import 'package:ashera_flutter/provider/stomp_client_provider.dart';
import 'package:ashera_flutter/provider/unlock_mugshot_provider.dart';
import 'package:ashera_flutter/provider/util_api_provider.dart';
import 'package:ashera_flutter/provider/visit_game_provider.dart';
import 'package:ashera_flutter/routers/parser.dart';
import 'package:ashera_flutter/routers/route_name.dart';
import 'package:ashera_flutter/utils/api.dart';
import 'package:ashera_flutter/utils/firebase_message.dart';
import 'package:ashera_flutter/utils/shared_preference.dart';
import 'package:ashera_flutter/utils/util.dart';
import 'package:dio/dio.dart';
import 'package:dio_http_package/dio_http_package.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:in_app_purchase/in_app_purchase.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

import 'app.dart';
import 'generated/l10n.dart';
import 'models/user_model.dart';
import 'provider/game_home_provider.dart';
import 'provider/web_socket_provider.dart';

/**
 * android 相片裁剪 圓型框
 * 進入 image_pickers 套件
 * class ImageCropEngine 下
 * onStartCrop 方法內
 * 加入 options.setCircleDimmedLayer(true)
 * options.setShowCropFrame; 改false
 * options.setShowCropGrid; 改false
 *
 * IOS 相片裁剪 圓形框
 * 進入 pods -> Development Pods -> image_pickers
 * .. -> Classes -> imagePickersPlugin
 * configuration.editImageConfiguration.clipRatios其中的 isCircle 改 true
 *
 * 直接進入裁剪畫面也需更改套件內容，但這就不一一說明
 *
 * android 相簿無法顯示問題解決
 * 作者寫的相片預覽無法使用
 * 所以改使用系統相簿預覽
 * PictureSelector.create(this).openSystemGallery(SelectMimeType.ofImage())
    .setCropEngine((selectCount.intValue() == 1 && enableCrop) ?
    new ImageCropEngine(this, buildOptions(selectorStyle), width.intValue(), height.intValue()) : null)
    .setCompressEngine(new ImageCompressEngine(compressSize.intValue()))
    .setSandboxFileEngine(new MeSandboxFileEngine())
    .setSelectionMode(selectCount.intValue() == 1 ? SelectModeConfig.SINGLE : SelectModeConfig.MULTIPLE)// 多选 or 单选 PictureConfig.MULTIPLE or PictureConfig.SINGLE
    .setSkipCropMimeType(new String[]{PictureMimeType.ofGIF(), PictureMimeType.ofWEBP()}).forSystemResult(new OnResultCallbackListener<LocalMedia>() {
    @Override
    public void onResult(ArrayList<LocalMedia> result) {
    handlerResult(result);
    }

    @Override
    public void onCancel() {
    Intent intent = new Intent();
    intent.putExtra(COMPRESS_PATHS, new ArrayList<>());
    setResult(RESULT_OK, intent);
    finish();
    }
    });
 * */

///Firebase Background
@pragma('vm:entry-point')
Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();

  debugPrint("Handling a background message: ${message.messageId}");
  flutterLocalNotificationsPlugin
      .resolvePlatformSpecificImplementation<
      AndroidFlutterLocalNotificationsPlugin>()
      ?.getNotificationChannels().then((value) {
    //log('Firebase Background ${value!.length.toString()}');
  });
}

//入口
void main() {
  WidgetsFlutterBinding.ensureInitialized();
  ///Firebase 背景監聽
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  //讀取本地資料庫
  appDb.initDatabase();
  //Firebase
  message.fireBaseInit().then((value) {
    if(Platform.isIOS){ //判斷是IOS才需要
      message.iosSetting();
    }
  });

  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]).then((value) {
    initStore();
    runApp(MyApp());
  });
}


Future<void> refreshToken() async {
  SharedPreferenceUtil sharedPreferenceUtil = SharedPreferenceUtil();
  final refreshToken = await sharedPreferenceUtil.getAccessToken();
  try{
    dynamic response = await HttpUtils.get(
        Api.authRefreshToken,
        options: Options(headers: {
          'authorization': 'Bearer $refreshToken'
        })
    );
    final Map<String, dynamic> responseData = json.decode(json.encode(response));
    User _user = User.fromJsonRefresh(responseData);
    Api.accessToken = _user.token!;
    sharedPreferenceUtil.saveAccessToken(_user.token!);
  } on DioError catch(e){
    //錯誤
    //log('Error ${e.message}');
  }
}

void initStore() {
  //初始化
  HttpUtils.init(baseUrl: Api.baseUrl);
}

class MyApp extends StatefulWidget {
  MyApp({Key? key}) : super(key: key) {
    //跳轉
    delegate.replace(name: RouteName.splashPage);
  }

  @override
  State createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {

  SharedPreferenceUtil sharedPreferenceUtil = SharedPreferenceUtil();
  final List<SingleChildWidget> _providers = [];
  //獲取系統設定語言
  final List<Locale> systemLocales = window.locales;
  CurrentLocaleProvider? currentLocaleProvider;
  MemberProvider? _memberProvider;
  AuthProvider? _authProvider;
  StompClientProvider? _stompClientProvider;
  PackagesInfoProvider? _packagesInfoProvider;
  UtilApiProvider? _utilApiProvider;
  MemberBlackListProvider? _memberBlackListProvider;

  _onLayoutDone(_) async {
    //取得系統設定
    await _utilApiProvider!.getSystemSetting();
    _autoLogin();
    //取得系統預設
    currentLocaleProvider!.setLocale(systemLocales.first);
    //取得APP資訊
    _packagesInfoProvider!.getPackageInfo();
  }

  @override
  void initState() {
    super.initState();
    initProviders();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
    if(Platform.isIOS){
      SystemChrome.setEnabledSystemUIMode(SystemUiMode.leanBack, overlays: []);
      SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
          statusBarColor: Colors.black,
          systemNavigationBarColor: Colors.black,
          systemNavigationBarIconBrightness: Brightness.dark
      ));
    }
  }

  void _autoLogin(){
    Future.delayed(const Duration(milliseconds: 1600), (){
      sharedPreferenceUtil.getAccountAndPassword().then((value) {//先判斷是否有登入過
        String _name = value['name'];
        String _password = value['password'];
        //log('是否有登入過: $_name');
        if(_name.isNotEmpty){ //有存在帳號
          sharedPreferenceUtil.getRefreshTime().then((value) { //取得上一次儲存Token的時間
            //log('相差時間: ${DateTime.now().difference(value)}');
            if(DateTime.now().difference(value) >= Duration(minutes: Util().tokenExpired)){ //是否需要刷新Token
              _memberLogin(name: _name, password: _password);
            }else{
              sharedPreferenceUtil.getAccessToken().then((value) {
                Api.accessToken = value;
                sharedPreferenceUtil.getMemberId().then((value) {//取得會員ID
                  if(value != 0){
                    _memberProvider!.getMember(value).then((value) async{//取得會員資料
                      if(value){
                        if(_memberProvider!.memberModel.status == 1){//是否被禁用
                          //取得我封鎖了誰
                          _memberBlackListProvider!.getMemberBlackListByFromMemberId(id: _memberProvider!.memberModel.id!);
                          _memberBlackListProvider!.getBlacklist();
                          //STOMP建立與連線
                          _stompClientProvider!.onCreation(_name, _password);
                          //跳轉
                          delegate.replace(name: RouteName.appBarPage);
                        } else {
                          //失敗
                          await appDb.deleteAllFriend();
                          await appDb.deleteAllMessage();
                          await appDb.deleteBlacklist();
                          delegate.replace(name: RouteName.welcomePage);
                        }
                      }else{
                        //失敗
                        await appDb.deleteAllFriend();
                        await appDb.deleteAllMessage();
                        await appDb.deleteBlacklist();
                        delegate.replace(name: RouteName.welcomePage);
                      }
                    });
                  }
                });
              });
            }
          });
        } else { //沒有帳號存在過
          //跳轉
          delegate.replace(name: RouteName.welcomePage);
        }
      });
    });
  }
  ///
  /// 會員登入
  ///
  /// [name] 帳號
  /// [password] 密碼
  ///
  void _memberLogin({required String name,required String password}){
    _authProvider!.login(name, password).then((user) {
      if(user['status']){
        Api.accessToken = user['user'].token;
        sharedPreferenceUtil.saveAccessToken(user['user'].token);
        sharedPreferenceUtil.saveRefreshTime();
        //取得會員資料
        _memberProvider!.getMember(user['user'].id).then((value) {
          if(value){
            //跳轉
            _stompClientProvider!.onCreation(name, password);//STOMP建立與連線
            //取得我封鎖了誰
            _memberBlackListProvider!.getMemberBlackListByFromMemberId(id: user['user'].id);
            _memberBlackListProvider!.getBlacklist();
            delegate.replace(name: RouteName.appBarPage);
          }else{
            //失敗
            delegate.replace(name: RouteName.welcomePage);
          }
        });
      }else{
        delegate.replace(name: RouteName.welcomePage);
        EasyLoading.showToast(user['message'].toString().substring(3));
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: _providers,
      child: Consumer<CurrentLocaleProvider>(builder: (context, currentLocale, _){
        _memberProvider = Provider.of<MemberProvider>(context);
        _authProvider = Provider.of<AuthProvider>(context);
        _stompClientProvider = Provider.of<StompClientProvider>(context);
        _packagesInfoProvider = Provider.of<PackagesInfoProvider>(context);
        _utilApiProvider = Provider.of<UtilApiProvider>(context);
        _memberBlackListProvider = Provider.of<MemberBlackListProvider>(context);
        currentLocaleProvider = currentLocale;
        return MaterialApp.router(
          localizationsDelegates: const [
            GlobalMaterialLocalizations.delegate,
            GlobalWidgetsLocalizations.delegate,
            GlobalCupertinoLocalizations.delegate,
            S.delegate
          ],
          supportedLocales: const [
            Locale('zh', 'Hant'),
            Locale('en'),
          ],
          locale: currentLocale.value,
          debugShowCheckedModeBanner: false,
          routeInformationParser: const AsheraRouteInformationParser(),
          routerDelegate: delegate,
          builder: (context, child) {
            final MediaQueryData data = MediaQuery.of(context);
            child = EasyLoading.init()(context, child);
            child = Scaffold(
              resizeToAvoidBottomInset: false,
              body: MediaQuery(
                data: data.copyWith(textScaleFactor: 1, boldText: false),
                child: GestureDetector(
                    onTap: () {
                      SystemChannels.textInput.invokeMethod('TextInput.hide');
                    },
                    child: child),
              ),
            );
            return child;
          },
        );
      },),
    );
  }

  //狀態管理初始化
  void initProviders() {
    _providers.add(ChangeNotifierProvider(create: (_) => InAppPurchaseProvider()));
    _providers
        .add(ChangeNotifierProvider(create: (_) => CurrentLocaleProvider()));
    _providers.add(ChangeNotifierProvider(create: (_) => AuthProvider()));
    _providers.add(ChangeNotifierProvider(create: (_) => UtilApiProvider()));
    _providers.add(ChangeNotifierProvider(create: (_) => MenuStatusProvider()));
    _providers.add(ChangeNotifierProvider(create: (_) => MessageProvider()));
    _providers.add(ChangeNotifierProvider(create: (_) => VisitGameProvider()));
    _providers.add(ChangeNotifierProvider(create: (_) => PositionProvider()));
    _providers.add(ChangeNotifierProvider(create: (_) => MemberProvider()));
    _providers.add(ChangeNotifierProvider(create: (_) => StompClientProvider()));
    _providers.add(ChangeNotifierProvider(create: (_) => NetworkStatusProvider()));
    _providers.add(ChangeNotifierProvider(create: (_) => WebSocketProvider()));
    _providers.add(ChangeNotifierProvider(create: (_) => PeekabooGameProvider()));
    _providers.add(ChangeNotifierProvider(create: (_) => GameHomeProvider()));
    _providers.add(ChangeNotifierProvider(create: (_) => PackagesInfoProvider()));
    _providers.add(ChangeNotifierProvider(create: (_) => MemberBlackListProvider()));
    _providers.add(ChangeNotifierProvider(create: (_) => GameStreetProvider()));
    _providers.add(ChangeNotifierProvider(create: (_) => UnlockMugshotProvider()));
    _providers.add(ChangeNotifierProvider(create: (_) => GameShoppingRefreshProvider()));
    _providers.add(ChangeNotifierProvider(create: (_) => IdentificationProvider()));
    _providers.add(ChangeNotifierProvider(create: (_) => DrawerVm()));
  }
}