import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../generated/l10n.dart';
import '../utils/app_color.dart';
import '../widget/button_widget.dart';

class PeekabooDialog extends StatefulWidget{
  const PeekabooDialog({Key? key}):super(key: key);

  @override
  State createState() => _PeekabooDialogState();
}

class _PeekabooDialogState extends State<PeekabooDialog>{

  @override
  Widget build(BuildContext context) {
    return BackButtonListener(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.black54,
          body: Center(
            child: Container(
              width: Device.width - 60,
              height: 40.h,
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(20)),
              child: Column(
                children: [
                  //title
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 1.5.h),
                    child: Text(
                      '',
                      style: TextStyle(
                          fontSize: 23.sp,
                          color: Colors.black,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  //灰線
                  Container(
                    height: 0.3.h,
                    width: Device.width,
                    color: AppColor.grayLine,
                  ),
                  //內容
                  Expanded(
                    child: Container(
                      alignment: Alignment.center,
                      child: Text(
                        S.of(context).capture_completed,
                        style: const TextStyle(color: Colors.black, fontSize: 20,fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  //按鈕
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop(true);
                    },
                    child: Container(
                      margin: EdgeInsets.only(bottom: 5.h),
                      width: 58.w,
                      height: 7.h,
                      child: buttonWidget(S.of(context).confirm),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
        onBackButtonPressed: () => pop());
  }

  Future<bool> pop() async{
    Navigator.of(context).pop(false);
    return true;
  }
}