
import 'dart:developer';

import 'package:ashera_flutter/dialog/pig_sales_record_dialog.dart';
import 'package:ashera_flutter/provider/game_home_provider.dart';
import 'package:ashera_flutter/provider/member_provider.dart';
import 'package:ashera_flutter/provider/stomp_client_provider.dart';
import 'package:ashera_flutter/utils/app_color.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../app.dart';
import '../enum/app_enum.dart';
import '../generated/l10n.dart';
import '../models/game_dto.dart';
import '../provider/util_api_provider.dart';
import '../routers/route_name.dart';
import '../utils/app_image.dart';
import '../utils/util.dart';
import 'game_point_insufficient_dialog.dart';
import 'member_upgrade_vip_dialog.dart';

class PigGrowsDialog extends StatefulWidget {
  const PigGrowsDialog({
    Key? key,
  }) : super(key: key);

  @override
  State createState() => _PigGrowsDialogState();
}

class _PigGrowsDialogState extends State<PigGrowsDialog> {
  UtilApiProvider? _utilApiProvider;
  StompClientProvider? _stompClientProvider;
  GameHomeProvider? _gameHomeProvider;
  MemberProvider? _memberProvider;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _utilApiProvider = Provider.of<UtilApiProvider>(context);
    _stompClientProvider = Provider.of<StompClientProvider>(context);
    _gameHomeProvider = Provider.of<GameHomeProvider>(context);
    _memberProvider = Provider.of<MemberProvider>(context);
    return BackButtonListener(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.black12,
          body: Center(
            child: Container(
              width: Device.width - 40,
              height: Device.height - 200,
              decoration: BoxDecoration(
                  color: AppColor.brownColor,
                  borderRadius: BorderRadius.circular(20)),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  //販售紀錄 X
                  Flexible(
                    child: _titleRecordAndClose(),
                  ),
                  //豬列表
                  Expanded(flex: 8, child: _pigListWidget()),
                ],
              ),
            ),
          ),
        ),
        onBackButtonPressed: () => pop());
  }

  //販售紀錄與X
  Widget _titleRecordAndClose() {
    return SizedBox(
      width: Device.width,
      child: Stack(
        children: [
          Positioned(top: 2.h, left: 3.w, child: _salesRecordButton()),
          Positioned(
            top: 2.h,
            right: 3.w,
            child: _closeButton(),
          )
        ],
      ),
    );
  }

  //販售紀錄按鈕
  Widget _salesRecordButton() {
    return GestureDetector(
      onTap: () => _salesRecordOnTap(),
      child: Container(
        width: 30.w,
        height: 5.h,
        alignment: Alignment.center,
        padding: EdgeInsets.only(left: 1.w, right: 1.w),
        decoration: BoxDecoration(
          color: AppColor.beigeColor,
          borderRadius: BorderRadius.circular(5),
          border: Border.all(color: AppColor.yellowIcon, width: 2),
        ),
        child: FittedBox(
          child: Text(
            S.of(context).sales_record,
            style: TextStyle(
                color: AppColor.brownColor,
                fontWeight: FontWeight.bold,
                fontSize: 20.sp),
          ),
        ),
      ),
    );
  }

  //販售紀錄 onTap
  void _salesRecordOnTap() {
    //取販售紀錄
    _gameHomeProvider!.getPetsSellRecord(id: _memberProvider!.memberModel.id!).then((value){
      showGeneralDialog(
          context: context,
          pageBuilder: (context, anim1, anim2){
            return PigSalesRecordDialog(
              key: UniqueKey(),
            );
          }
      );
    });
  }

  //X
  Widget _closeButton() {
    return GestureDetector(
      onTap: () => _closeOnTap(),
      child: const Icon(
        Icons.cancel,
        size: 35,
        color: Colors.white,
      ),
    );
  }

  //X onTap
  void _closeOnTap() {
    Navigator.of(context).pop(false);
  }



  //豬隻列表
  Widget _pigListWidget() {
    return Container(
      padding: EdgeInsets.only(left: 3.w, right: 3.w, bottom: 2.h),
      child: Column(
        children: [
          //第一隻
          Expanded(
            child: Container(
              decoration: const BoxDecoration(
                  color: AppColor.gooseYellowColor,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(10),
                      topRight: Radius.circular(10))),
              child: Consumer<GameHomeProvider>(builder: (context, pig1, _){

                return _ordinaryPigWidget(
                    value: pig1.memberPig[0].curHp!,
                    growthState: GrowthState.values[pig1.memberPig[0].level! - 1],
                    time: Util().pigTime(pig1.memberPig[0].startAt!),
                    sellButtonType: (pig1.memberPig[0].curHp == pig1.memberPig[0].hp && pig1.memberPig[0].level == 3) ? PigButtonType.canSell : PigButtonType.cantSell,
                    feedingButtonType: pig1.memberPig[0].curHp != pig1.memberPig[0].hp ? PigButtonType.canFeeding : PigButtonType.cantFeeding,
                    sellOnClick: (){
                      if(pig1.memberPig[0].curHp == pig1.memberPig[0].hp && pig1.memberPig[0].level == 3){
                        _stompClientProvider!.sendSellPets(id: pig1.memberPig[0].id!);
                        Future.delayed(const Duration(milliseconds: 1000), (){
                          _stompClientProvider!.sendCheckPetsStatus();
                        });
                      }
                    },
                    feedingButtonOnClick: (){
                      //血量相同 不能餵食
                      if(pig1.memberPig[0].curHp == pig1.memberPig[0].hp){
                        return;
                      }
                      EasyLoading.show(status: S.of(context).pig_eating);
                      int hp = pig1.memberPig[0].hp! - pig1.memberPig[0].curHp!;
                      UpdateMemberPetsHpDTO memberPetsHp = UpdateMemberPetsHpDTO(
                          memberPetsId: pig1.memberPig[0].id!,
                          hp: hp);
                      _stompClientProvider!.sendAddMemberPetsHp(updateMemberPetsHpDTO: memberPetsHp);
                      Future.delayed(const Duration(milliseconds: 1000), (){
                        _stompClientProvider!.sendCheckPetsStatus();
                      });
                    },
                    againButtonOnClick: (){
                    }
                );
              },),
            ),
          ),
          SizedBox(
            height: 1.h,
          ),
          //第二隻
          Expanded(
            child: Container(
              decoration: const BoxDecoration(
                color: AppColor.gooseYellowColor,
              ),
              child: Consumer<GameHomeProvider>(builder: (context, pig2, _){
                return _ordinaryPigWidget(
                    value: pig2.memberPig[1].curHp!,
                    growthState: GrowthState.values[pig2.memberPig[1].level! - 1],
                    time: Util().pigTime(pig2.memberPig[1].startAt!),
                    sellButtonType: (pig2.memberPig[1].curHp == pig2.memberPig[1].hp && pig2.memberPig[1].level == 3) ? PigButtonType.canSell : PigButtonType.cantSell,
                    feedingButtonType: pig2.memberPig[1].curHp != pig2.memberPig[1].hp ? PigButtonType.canFeeding : PigButtonType.cantFeeding,
                    sellOnClick: (){
                      if(pig2.memberPig[1].curHp == pig2.memberPig[1].hp && pig2.memberPig[1].level == 3){
                        _stompClientProvider!.sendSellPets(id: pig2.memberPig[1].id!);
                        Future.delayed(const Duration(milliseconds: 1000), (){
                          _stompClientProvider!.sendCheckPetsStatus();
                        });
                      }
                    },
                    feedingButtonOnClick: (){
                      //血量相同 不能餵食
                      if(pig2.memberPig[1].curHp == pig2.memberPig[1].hp){
                        return;
                      }
                      EasyLoading.show(status: S.of(context).pig_eating);
                      int hp = pig2.memberPig[1].hp! - pig2.memberPig[1].curHp!;
                      UpdateMemberPetsHpDTO memberPetsHp = UpdateMemberPetsHpDTO(
                          memberPetsId: pig2.memberPig[1].id!,
                          hp: hp);
                      _stompClientProvider!.sendAddMemberPetsHp(updateMemberPetsHpDTO: memberPetsHp);
                      Future.delayed(const Duration(milliseconds: 1000), (){
                        _stompClientProvider!.sendCheckPetsStatus();
                      });
                    },
                    againButtonOnClick: (){}
                );
              },),
            ),
          ),
          SizedBox(
            height: 1.h,
          ),
          //第三隻
          Expanded(
            child: Container(
              decoration: const BoxDecoration(
                  color: AppColor.gooseYellowColor,
                  borderRadius: BorderRadius.only(
                      bottomLeft: Radius.circular(10),
                      bottomRight: Radius.circular(10))),
              child: Consumer<MemberProvider>(builder: (context, member, _){
                return _vipPigWidget(vip: member.memberModel.vip == 1 ? true : false);
              },),
            ),
          ),
        ],
      ),
    );
  }

  /// 豬 的 卡
  /// * [value] 飢餓度
  /// * [growthState] 成長階段
  /// * [time] 成長時間
  /// * [sellButtonType] 販售按鈕的狀態
  /// * [feedingButtonType] 餵食按鈕的狀態
  /// * [sellOnClick] 販售按鈕點擊
  /// * [feedingButtonOnClick] 餵食按鈕點擊
  Widget _ordinaryPigWidget({
    required int value,
    required GrowthState growthState,
    required String time,
    required PigButtonType sellButtonType,
    required PigButtonType feedingButtonType,
    required VoidCallback sellOnClick,
    required VoidCallback feedingButtonOnClick,
    required VoidCallback againButtonOnClick,
  }) {
    //log('sellButtonType $sellButtonType');
    return Container(
      padding: const EdgeInsets.only(left: 10,),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          //豬 飢餓度
          Flexible(
              flex: 1,
              child: _pigImageAndHungerWidget(
                  value: value
              )
          ),
          //成長階段
          Expanded(
              flex: 1,
              child: _growingPhaseWidget(
                growingPhase: growthState.name,
                time: time
              )),
          //販售與餵食與重養
          Expanded(
              flex: 1,
              child: _sellingAndFeedingWidget(
                  sellButton: buttonWidget(text: S.of(context).sell, type: sellButtonType, onClick: sellOnClick),
                  feedingButton: buttonWidget(
                      text: feedingButtonType == PigButtonType.canAgain
                          ? S.of(context).again
                          : S.of(context).feeding,
                      type: feedingButtonType,
                      onClick: feedingButtonType == PigButtonType.canAgain
                          ? againButtonOnClick
                          : feedingButtonOnClick)
              )),
        ],
      ),
    );
  }
  ///飢餓度
  /// [value] 飢餓度
  Widget _pigImageAndHungerWidget({required int value}) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        //豬
        Container(
          width: Device.width,
          alignment: Alignment.center,
          child: Image(
            width: 24.w,
            image: AssetImage(AppImage.imgPig),
          ),
        ),
        //飢餓度
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            //愛心
            Image(
              width: 5.w,
              image: AssetImage(AppImage.iconHungry),
            ),
            //文字
            Text(
              S.of(context).hunger,
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 14.sp),
            )
          ],
        ),
        //進度條
        Container(
          width: 40.w,
          decoration: BoxDecoration(
              border: Border.all(color: Colors.black, width: 2),
              borderRadius: BorderRadius.circular(10)),
          child: ClipRRect(
            borderRadius: BorderRadius.circular(10),
            child: LinearProgressIndicator(
              value: value < 0 ? 0.0 : value / 100,
              backgroundColor: Colors.white,
              valueColor: const AlwaysStoppedAnimation<Color>(Colors.red),
              minHeight: 7, //最小寬度
            ),
          ),
        )
      ],
    );
  }
  ///成長階段
  /// * [growingPhase] 成長階段
  /// * [time] 成長時間
  Widget _growingPhaseWidget({required String growingPhase, required String time}) {
    return Container(
      padding: const EdgeInsets.only(top: 20, bottom: 20),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          //成長階段
          Text(
          S.of(context).growingPhase(growingPhase),
            style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 25.sp),
          ),
          //時間
          FittedBox(
            child: Text(
              time,
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 16.sp),
            ),
          )
        ],
      ),
    );
  }

  ///販售與餵食
  /// * [sellButton] 販售按鈕
  /// * [feedingButton] 餵食按鈕
  Widget _sellingAndFeedingWidget({required Widget sellButton, required Widget feedingButton}) {
    return Container(
      padding: const EdgeInsets.only(top: 15, bottom: 15),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          //販售
          sellButton,
          //餵食
          feedingButton,
        ],
      ),
    );
  }

  //VIP豬
  Widget _vipPigWidget({
    required bool vip,
  }) {
    if (!vip) {
      return SizedBox(
        width: Device.width,
        height: Device.height,
        child: Stack(
          alignment: Alignment.center,
          children: [
            //左上VIP
            Positioned(
                left: 0,
                top: 0,
                child: Container(
                  width: 12.w,
                  height: 4.h,
                  alignment: Alignment.center,
                  decoration: const BoxDecoration(color: AppColor.yellowIcon),
                  child: FittedBox(
                    child: Text(
                      'VIP',
                      style: TextStyle(color: Colors.white, fontSize: 18.sp),
                    ),
                  ),
                )),
            //中間的升級VIP會員
            GestureDetector(
              onTap: () => _memberUpgradeOnTap(),
              child: Container(
                width: 40.w,
                height: 6.h,
                alignment: Alignment.center,
                decoration: BoxDecoration(
                  color: AppColor.beigeColor,
                  borderRadius: BorderRadius.circular(5),
                  border: Border.all(color: AppColor.brownColor, width: 2),
                ),
                child: FittedBox(
                  child: Text(
                    S.of(context).upgrade_VIP_membership,
                    style: TextStyle(
                        color: AppColor.brownColor,
                        fontSize: 20.sp,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            )
          ],
        ),
      );
    } else {
      return Consumer<GameHomeProvider>(builder: (context, pig3, _){
        if(pig3.memberPig.length > 2){
          return _ordinaryPigWidget(
              value: pig3.memberPig[2].curHp!,
              growthState: GrowthState.values[pig3.memberPig[2].level! - 1 == -1 ? 0 : pig3.memberPig[2].level! - 1],
              time: Util().pigTime(pig3.memberPig[2].startAt!),
              sellButtonType: (pig3.memberPig[2].curHp == pig3.memberPig[2].hp && pig3.memberPig[2].level == 3) ? PigButtonType.canSell : PigButtonType.cantSell,
              feedingButtonType: pig3.memberPig[2].curHp != pig3.memberPig[2].hp ? PigButtonType.canFeeding : PigButtonType.cantFeeding,
              sellOnClick: (){
                if(pig3.memberPig[2].curHp == pig3.memberPig[2].hp && pig3.memberPig[2].level == 3){
                  _stompClientProvider!.sendSellPets(id: pig3.memberPig[2].id!);
                  Future.delayed(const Duration(milliseconds: 1000), (){
                    _stompClientProvider!.sendCheckPetsStatus();
                  });
                }
              },
              feedingButtonOnClick: (){
                //血量相同 不能餵食
                if(pig3.memberPig[2].curHp == pig3.memberPig[2].hp){
                  return;
                }
                EasyLoading.show(status: S.of(context).pig_eating);
                int hp = pig3.memberPig[2].hp! - pig3.memberPig[2].curHp!;
                UpdateMemberPetsHpDTO memberPetsHp = UpdateMemberPetsHpDTO(
                    memberPetsId: pig3.memberPig[2].id!,
                    hp: hp);
                _stompClientProvider!.sendAddMemberPetsHp(updateMemberPetsHpDTO: memberPetsHp);
                Future.delayed(const Duration(milliseconds: 1000), (){
                  _stompClientProvider!.sendCheckPetsStatus();
                });
              },
              againButtonOnClick: (){}
          );
        }
        return SizedBox(
          width: Device.width,
          height: Device.height,
          child: Stack(
            alignment: Alignment.center,
            children: [
              //左上VIP
              Positioned(
                  left: 0,
                  top: 0,
                  child: Container(
                    width: 12.w,
                    height: 4.h,
                    alignment: Alignment.center,
                    decoration: const BoxDecoration(color: AppColor.yellowIcon),
                    child: FittedBox(
                      child: Text(
                        'VIP',
                        style: TextStyle(color: Colors.white, fontSize: 18.sp),
                      ),
                    ),
                  )),
              //中間的升級VIP會員
              GestureDetector(
                onTap: () => null,
                child: Container(
                  width: 40.w,
                  height: 6.h,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: AppColor.beigeColor,
                    borderRadius: BorderRadius.circular(5),
                    border: Border.all(color: AppColor.brownColor, width: 2),
                  ),
                  child: FittedBox(
                    child: Text(
                      S.of(context).upgrade_VIP_membership,
                      style: TextStyle(
                          color: AppColor.brownColor,
                          fontSize: 20.sp,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
              )
            ],
          ),
        );
      },);
    }
  }

  Future<bool> pop() async {
    Navigator.of(context).pop(false);
    return true;
  }

  ///按鈕樣式
  /// * [text] 按鈕要顯示的字
  /// * [type] 按鈕狀態
  /// * [onClick] 點擊方法
  Widget buttonWidget({required String text, required PigButtonType type, required VoidCallback onClick}){
    return GestureDetector(
      onTap: () => onClick(),
      child: Container(
        width: 20.w,
        height: 6.h,
        alignment: Alignment.center,
        decoration: BoxDecoration(
            color: buttonColor(type: type),
            borderRadius: BorderRadius.circular(10),
            border: Border.all(
                color: borderColor(type: type),
                width: 2
            )
        ),
        child: Text(
          text,
          style: TextStyle(
              color: textColor(type: type),
              fontSize: 20.sp,
              fontWeight: FontWeight.bold
          ),
        ),
      ),
    );
  }
  /// 按鈕顏色
  /// * [type] PigButtonType
  Color buttonColor({required PigButtonType type}) {
    switch (type) {
      case PigButtonType.canSell:
        return AppColor.lightRed;
      case PigButtonType.cantSell:
        return AppColor.grayText;
      case PigButtonType.canFeeding:
        return AppColor.brightGreenColor;
      case PigButtonType.cantFeeding:
        return AppColor.grayText;
      case PigButtonType.canAgain:
        return AppColor.lightRed;
      case PigButtonType.cantAgain:
        return AppColor.grayText;
      default:
        return AppColor.grayText;
    }
  }
  /// 邊框顏色
  /// * [type] PigButtonType
  Color borderColor({required PigButtonType type}) {
    switch (type) {
      case PigButtonType.canSell:
        return AppColor.brownColor;
      case PigButtonType.cantSell:
        return AppColor.grayFrame;
      case PigButtonType.canFeeding:
        return AppColor.brownColor;
      case PigButtonType.cantFeeding:
        return AppColor.grayFrame;
      case PigButtonType.canAgain:
        return AppColor.brownColor;
      case PigButtonType.cantAgain:
        return AppColor.grayFrame;
      default:
        return AppColor.grayFrame;
    }
  }
  /// 文字顏色
  /// * [type] PigButtonType
  Color textColor({required PigButtonType type}) {
    switch (type) {
      case PigButtonType.canSell:
        return Colors.white;
      case PigButtonType.cantSell:
        return AppColor.grayFrame;
      case PigButtonType.canFeeding:
        return Colors.white;
      case PigButtonType.cantFeeding:
        return AppColor.grayFrame;
      case PigButtonType.canAgain:
        return Colors.white;
      case PigButtonType.cantAgain:
        return AppColor.grayFrame;
      default:
        return AppColor.grayFrame;
    }
  }

  void _memberUpgradeOnTap() async {
    //判斷金額是否足夠
    if(_memberProvider!.memberPoint.point! >= _utilApiProvider!.systemSetting!.vipPointsPerMonth!){
      bool? _result = await showGeneralDialog(
          context: context,
          pageBuilder: (context, anim1, anim2){
            return MemberUpgradeVipDialog(
              key: UniqueKey(),
              title: S.of(context).message,
              contentForward: S.of(context).does_it_cost,
              content: _utilApiProvider!.systemSetting!.vipPointsPerMonth!.toString(),
              contentBack: S.of(context).upgrade_vip,
              confirmButtonText: S.of(context).sure,
            );
          },
          barrierColor: Colors.black54,
          barrierDismissible: false,
          transitionDuration: const Duration(milliseconds: 100),
          transitionBuilder: (context, anim1, anim2, child) {
            return Transform.scale(
              scale: anim1.value,
              child: child,
            );
          }
      );
      if(_result != null){
        if(_result){
          //同意購買
          _stompClientProvider!.sendMemberUpgrade();
        }
      }
    } else {
      if(_utilApiProvider!.systemSetting!.addPointsStatus == PointsStatus.closure.index){
        EasyLoading.showToast(S.of(context).not_enough_points);
      }else{
        //點數不足
        _pointInsufficient();
      }
    }
  }

  ///點數不足統一方法
  void _pointInsufficient() async {
    bool? result = await _showPointInsufficient();
    if (result != null) {
      if (result) {
        //跳轉加值
        delegate.push(name: RouteName.myPointsPage);
      }
    }
  }

  ///點數不足
  Future<bool?> _showPointInsufficient() async {
    return await showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return GamePointInsufficientDialog(
              key: UniqueKey(),
              title: S.of(context).message,
              content: S.of(context).please_add_value,
              confirmButtonText: S.of(context).go_to_premium,
              cancelButtonText: S.of(context).cancel);
        },
        barrierColor: Colors.black54,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 100),
        transitionBuilder: (context, anim1, anim2, child) {
          return child;
        });
  }

}


