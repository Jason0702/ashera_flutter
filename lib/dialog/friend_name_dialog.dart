import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../generated/l10n.dart';
import '../utils/app_color.dart';
import '../utils/util.dart';
import '../widget/button_widget.dart';

///
/// 更改暱稱
/// * [title] 最上層顯示
/// * [confirmButtonText] 確認按鈕文字

class FriendNameDialog extends StatefulWidget{
  final String title;
  final String confirmButtonText;
  const FriendNameDialog({Key? key, required this.title, required this.confirmButtonText}): super(key: key);

  @override
  State createState() => _FriendNameDialogState();
}
class _FriendNameDialogState extends State<FriendNameDialog>{

  final TextEditingController _nickName = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BackButtonListener(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.black54,
          body: Center(
            child: Container(
              width: Device.width - 60,
              height: 40.h,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(20)
              ),
              child: Stack(
                children: [
                  //內容
                  Column(
                    children: [
                      //title
                      Container(
                        margin: EdgeInsets.symmetric(vertical: 1.5.h),
                        child: Text(
                          widget.title,
                          style: TextStyle(
                              fontSize: 20.sp,
                              color: Colors.black,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      //灰線
                      Container(
                        height: 0.3.h,
                        width: Device.width,
                        color: AppColor.grayLine,
                      ),
                      //內容
                      Expanded(
                        child: Container(
                          alignment: Alignment.center,
                          child: _nickNameTextField(),
                        ),
                      ),
                      //同意按鈕
                      GestureDetector(
                        onTap: () {
                          if(_nickName.text.trim().isNotEmpty && _nickName.text.trim().length <= Util.nicknameLen){
                            Navigator.of(context).pop(_nickName.text.trim());
                          } else if(_nickName.text.trim().isEmpty){
                            EasyLoading.showToast(S.of(context).enter_a_nickname);
                          } else if(_nickName.text.trim().length > Util.nicknameLen){
                            EasyLoading.showToast('${S.of(context).wordLimit}${Util.nicknameLen}');
                          }
                        },
                        child: Container(
                          margin: EdgeInsets.only(bottom: 2.h),
                          width: 58.w,
                          height: 7.h,
                          child: buttonWidget(widget.confirmButtonText),
                        ),
                      ),
                    ],
                  ),
                  //右上X
                  Positioned(
                      right: 10,
                      top: 10,
                      child: GestureDetector(
                        onTap: () => Navigator.of(context).pop(),
                        child: Icon(
                          Icons.cancel,
                          size: 10.w,
                        ),
                      )),
                ],
              ),
            )
          ),
        ),
        onBackButtonPressed: () => pop());
  }

  //暱稱
  Widget _nickNameTextField() {
    return SizedBox(
      width: 80.w,
      height: 8.h,
      child: TextFormField(
        style: const TextStyle(color: Colors.black),
        decoration: InputDecoration(
          fillColor: Colors.transparent,
          filled: true,
          hintStyle: const TextStyle(color: Colors.grey),
          hintText: S.of(context).enter_a_nickname,
        ),
        keyboardType: TextInputType.text,
        textInputAction: TextInputAction.next,
        controller: _nickName,
        validator: (value) {
          if (value == null || value.isEmpty) {
            return S.of(context).enter_a_nickname;
          }
          if (value.length > Util.nicknameLen) {
            return '${S.of(context).wordLimit}${Util.nicknameLen}';
          }
          return null;
        },
      ),
    );
  }

  Future<bool> pop() async {//不能讓使用者返回
    return true;
  }
}