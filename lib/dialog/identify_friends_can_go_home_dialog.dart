
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_font_icons/flutter_font_icons.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../enum/app_enum.dart';
import '../generated/l10n.dart';
import '../models/member_model.dart';
import '../provider/member_provider.dart';
import '../provider/stomp_client_provider.dart';
import '../utils/app_color.dart';
import '../utils/app_image.dart';
import '../utils/util.dart';
import '../widget/avatar_image.dart';
import '../widget/titlebar_widget.dart';

class IdentifyFriendsCanGoHomeDialog extends StatefulWidget{
  final String token;
  final int id;
  const IdentifyFriendsCanGoHomeDialog({Key? key, required this.token, required this.id}): super(key: key);

  @override
  State createState() => _IdentifyFriendsCanGoHomeDialogState();
}

class _IdentifyFriendsCanGoHomeDialogState extends State<IdentifyFriendsCanGoHomeDialog>{

  MemberProvider? _memberProvider;
  StompClientProvider? _stompClientProvider;

  _onLayoutDone(_)  {

  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  Widget build(BuildContext context) {
    _memberProvider = Provider.of<MemberProvider>(context, listen: false);
    _stompClientProvider = Provider.of<StompClientProvider>(context, listen: false);
    return BackButtonListener(
        child: SafeArea(
          child: Scaffold(
            resizeToAvoidBottomInset: false,
            backgroundColor: AppColor.appBackgroundColor,
            body: Column(
              children: [
                //返回 與 標題
                titleBarIntegrate([], S.of(context).member_profile, []),
                Expanded(child: FutureBuilder(
                    future: _memberProvider!.getMemberById(id: widget.id),
                    builder: (context, AsyncSnapshot<dynamic> snapshot){
                      if(snapshot.connectionState == ConnectionState.done){
                        if(snapshot.data != null){
                          return Column(
                            children: [
                              //會員資料
                              memberInfo(model: snapshot.data!),
                              //會員個人資料
                              Expanded(flex: 3,child: memberPersonalInfo(model: snapshot.data!)),
                              //是否倒數完成
                              Expanded(flex: 1,child: countDownOver(model: snapshot.data!))
                            ],
                          );
                        }
                        //沒資料
                        return SizedBox(
                          width: Device.width,
                          height: Device.height,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image(
                                width: 20.w,
                                height: 20.h,
                                image: AssetImage(AppImage.imgWaiting),
                              ),
                              Text(
                                S.of(context).waiting,
                                style: TextStyle(color: Colors.grey[300]!, fontWeight: FontWeight.bold, fontSize: 20.sp),
                              )
                            ],
                          ),
                        );
                      }
                      //沒資料
                      return SizedBox(
                        width: Device.width,
                        height: Device.height,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Image(
                              width: 20.w,
                              height: 20.h,
                              image: AssetImage(AppImage.imgWaiting),
                            ),
                            Text(
                              S.of(context).waiting,
                              style: TextStyle(color: Colors.grey[300]!, fontWeight: FontWeight.bold, fontSize: 20.sp),
                            )
                          ],
                        ),
                      );
                    }))
              ],
            ),
          ),
        ),
        onBackButtonPressed: () => pop());
  }

  //會員資料
  Widget memberInfo({required MeModel model}) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        SizedBox(height: 2.h,),
        //頭像
        model.mugshot!.isEmpty
            ? noAvatarImage()
            : Container(
          width: 16.w * 2,
          height: 8.h * 2,
          decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
                fit: BoxFit.cover,
                image: CachedNetworkImageProvider(
                  Util().getMemberMugshotUrl(model.mugshot!),
                  headers: {"authorization": "Bearer " + widget.token},
                ),
                onError: (error, stackTrace){
                  debugPrint('Error: ${error.toString()}');
                  if(error.toString().contains('statusCode: 404')){

                  }
                }
            ),
          ),
        ),
        //名稱
        FittedBox(
          child: Text(
            model.nickname!,
            style: TextStyle(
                fontWeight: FontWeight.bold,
                fontSize: 18.sp,
                color: Colors.black),
          ),
        ),
        //SizedBox(height: 1.h,),
        //ID
        FittedBox(
          child: Text(
            "ID：${model.asheraUid}",
            style: TextStyle(fontSize: 18.sp, color: Colors.black),
          ),
        ),
      ],
    );
  }

  //會員個人資料
  Widget memberPersonalInfo({required MeModel model}) {
    return Container(
        padding: EdgeInsets.symmetric(horizontal: 5.w,),
        color: Colors.white,
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              //關於我
              if(model.aboutMe != null)
                Container(
                  width: Device.boxConstraints.maxWidth,
                  decoration: const BoxDecoration(
                      color: Colors.white
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        S.of(context).about_me,
                        style: TextStyle(fontSize: 18.sp, color: Colors.black),
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      Container(
                        height: 70,
                        width: Device.boxConstraints.maxWidth,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            //border: Border.all(color: AppColor.grayLine, width: 1)
                        ),
                        child: Text(
                          model.aboutMe!.isEmpty ? '無' : model.aboutMe!,
                          maxLines: 3,
                          style: TextStyle(fontSize: 18.sp,
                              color: Colors.black,
                              height: 1.1),
                        ),
                      )
                    ],
                  ),
                ),
              //性別
              Row(
                children: [
                  Text(
                    S.of(context).gender,
                    style: TextStyle(fontSize: 18.sp, color: Colors.black),
                  ),
                  SizedBox(
                    width: 5.w,
                  ),
                  _maleOrFemaleText(gender: model.gender!),
                  SizedBox(
                    width: 1.w,
                  ),

                  _maleOrFemaleIcon(gender: model.gender!)
                ],
              ),
              const SizedBox(
                height: 5,
              ),
              //生日
              Row(
                children: [
                  Text(
                    S.of(context).birthday,
                    style: TextStyle(fontSize: 18.sp, color: Colors.black),
                  ),
                  SizedBox(
                    width: 5.w,
                  ),
                  Text(
                    model.hideBirthday! ? S.of(context).secret : model.birthday!,
                    style: TextStyle(
                        fontSize: 18.sp,
                        color: Colors.black,
                        ),
                  ),
                ],
              ),
              const SizedBox(
                height: 5,
              ),
              //身高
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text(
                    S.of(context).height,
                    style: TextStyle(fontSize: 18.sp, color: Colors.black),
                  ),
                  SizedBox(
                    width: 5.w,
                  ),
                  Text(
                    '${model.height}',
                    style: TextStyle(
                        fontSize: 18.sp,
                        color: Colors.black,
                        ),
                  ),
                  SizedBox(width: 2.w,),
                  Text(
                    "cm",
                    style: TextStyle(fontSize: 15.sp, color: Colors.black),
                  ),
                ],
              ),
              const SizedBox(
                height: 5,
              ),
              //體重
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text(
                    S.of(context).weight,
                    style: TextStyle(fontSize: 18.sp, color: Colors.black),
                  ),
                  SizedBox(
                    width: 5.w,
                  ),
                  Text(
                    model.hideWeight! ? S.of(context).secret : '${model.weight}',
                    style: TextStyle(
                        fontSize: 18.sp,
                        color: Colors.black,
                        ),
                  ),
                  SizedBox(width: 2.w,),
                  Text(
                    "kg",
                    style: TextStyle(fontSize: 15.sp, color: Colors.black),
                  ),
                ],
              ),
              const SizedBox(
                height: 5,
              ),
              //血型
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text(
                    S.of(context).blood_type,
                    style: TextStyle(fontSize: 18.sp, color: Colors.black),
                  ),
                  SizedBox(
                    width: 5.w,
                  ),
                  Text(
                    model.bloodType ?? 'O',
                    style: TextStyle(
                        fontSize: 18.sp,
                        color: Colors.black,
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 5,
              ),
              //星座
              Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text(
                    S.of(context).constellation,
                    style: TextStyle(fontSize: 18.sp, color: Colors.black),
                  ),
                  SizedBox(
                    width: 5.w,
                  ),
                  Text(
                    S.of(context).horoscope(model.zodiacSign ?? Horoscope.aries.name),
                    style: TextStyle(
                        fontSize: 18.sp,
                        color: Colors.black,
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 5,
              ),
              //職業
              Row(
                children: [
                  Text(
                    S.of(context).profession,
                    style: TextStyle(fontSize: 18.sp, color: Colors.black),
                  ),
                  SizedBox(
                    width: 5.w,
                  ),
                  Text(
                    model.hideJob! ? S.of(context).secret : model.job ?? '民意代表、主管及經理人員',
                    style: TextStyle(
                        fontSize: 18.sp,
                        color: Colors.black,
                    ),
                  ),
                ],
              ),
              const SizedBox(
                height: 5,
              ),
              //名片自介
              if(model.aboutMe2 != null)
                Container(
                  width: Device.boxConstraints.maxWidth,
                  decoration: const BoxDecoration(
                      color: Colors.white
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        S.of(context).business_card_introduction,
                        style: TextStyle(fontSize: 18.sp, color: Colors.black),
                      ),
                      const SizedBox(
                        height: 5,
                      ),
                      Container(
                        height: 70,
                        width: Device.boxConstraints.maxWidth,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            border: Border.all(color: AppColor.grayLine, width: 1)
                        ),
                        child: Text(
                          model.aboutMe2!.isEmpty ? '無' : model.aboutMe2!,
                          maxLines: 3,
                          style: TextStyle(fontSize: 18.sp,
                              color: Colors.black,
                              height: 1.1),
                        ),
                      )
                    ],
                  ),
                ),
            ],
          ),
        ));
  }
  ///
  ///男或女 字
  ///
  ///[gender] 性別
  Widget _maleOrFemaleText({required int gender}) {
    return gender == 1
        ? Text(
      S.of(context).female,
      style: TextStyle(
          fontSize: 18.sp,
          color: Colors.black,
          ),
    )
        : Text(
      S.of(context).male,
      style: TextStyle(
          fontSize: 18.sp,
          color: Colors.black,
          ),
    );
  }
  ///
  ///男或女 Icon
  ///
  ///[gender] 性別
  Widget _maleOrFemaleIcon({required int gender}) {
    return gender == 1
    //女
        ? const Icon(
      FontAwesome5Solid.venus,
      size: 23,
      color: AppColor.femaleColor,
    )
    //男
        : const Icon(FontAwesome5Solid.mars,
        size: 23, color: AppColor.maleColor);
  }

  Future<bool> pop() async {
    Navigator.of(context).pop(true);
    return true;
  }


  //倒數時間結束
  Widget countDownOver({required MeModel model}) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        _isAddOrCanAddText(model, _stompClientProvider!.friendList),
        SizedBox(
          height: 1.h,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            _isAddOrCanAdd(model, _stompClientProvider!.friendList),
            SizedBox(
              width: 10.w,
            ),
            GestureDetector(
              onTap: () => _noOnTap(),
              child: SizedBox(width: 20.w, child: Image.asset(AppImage.iconNo)),
            )
          ],
        )
      ],
    );
  }
  ///
  /// 添加好友或已是好友 字
  ///
  /// [faceImageModel] 人臉辨識結果
  /// [friendList] 好友列表
  Widget _isAddOrCanAddText(MeModel faceImageModel, List<MemberModel> friendList){
    if(friendList.where((element) => element.asheraUid == faceImageModel.asheraUid).isEmpty){
      return Text(
        S.of(context).identify_time_over_content,
        style: TextStyle(fontSize: 20.sp, color: Colors.black),
      );
    }else{
      return Text(
        S.of(context).added_friend,
        style: TextStyle(fontSize: 20.sp, color: Colors.black),
      );
    }
  }
  ///
  ///添加好友或已是好友 按鈕
  ///
  /// [faceImageModel] 人臉辨識結果
  /// [friendList] 好友列表
  ///
  /// 判斷已有此好友，愛心按鈕將反灰
  Widget _isAddOrCanAdd(MeModel faceImageModel, List<MemberModel> friendList){
    if(_memberProvider!.memberModel.asheraUid != faceImageModel.asheraUid){
      if(friendList.where((element) => element.asheraUid == faceImageModel.asheraUid).isEmpty){
        return GestureDetector(
          onTap: () => _yesOnTap(faceImageModel.id!),
          child: SizedBox(width: 20.w, child: Image(
            image: AssetImage(AppImage.iconYes),
          )),
        );
      } else {
        return SizedBox(
          width: 20.w,
          child: Image(
            image: AssetImage(AppImage.iconYesGray),
          ),
        );
      }
    } else {
      return SizedBox(
        width: 20.w,
        child: Image(
          image: AssetImage(AppImage.iconYesGray),
        ),
      );
    }
  }

  void _yesOnTap(int id){
    _stompClientProvider!.sendFollowerRequest(_memberProvider!.memberModel.id!, id);
    EasyLoading.showSuccess(S.of(context).friendship_invitation_has_been_sent);//已送出交友邀請
    Navigator.of(context).pop(false);
  }

  void _noOnTap(){
    Navigator.of(context).pop(false);
  }

 /* Widget cancel(){
    return GestureDetector(
      onTap: () => _goToHouse(),
      child: Container(
        margin: EdgeInsets.only(right: 5.w),
        child: FittedBox(
          child: SizedBox(
              height: 8.w,
              width: 8.w,
              child: Image.asset(AppImage.iconExit)),
        ),
      ),
    );
  }*/

  void _goToHouse(){
    Navigator.of(context).pop(true);
  }

}