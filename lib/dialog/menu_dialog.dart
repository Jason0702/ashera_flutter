//內容
import 'package:ashera_flutter/enum/app_enum.dart';
import 'package:ashera_flutter/provider/member_provider.dart';
import 'package:ashera_flutter/provider/util_api_provider.dart';
import 'package:ashera_flutter/widget/titlebar_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../app.dart';
import '../generated/l10n.dart';
import '../provider/identification_provider.dart';
import '../provider/menu_status_provider.dart';
import '../routers/route_name.dart';
import '../utils/app_color.dart';
import '../utils/app_image.dart';
import '../utils/no_shadow_scroll_behavior.dart';

class MenuWidget extends StatefulWidget {
  const MenuWidget({Key? key}) : super(key: key);


  @override
  _MenuWidgetState createState() => _MenuWidgetState();
}

class _MenuWidgetState extends State<MenuWidget> {
  MenuStatusProvider? _menuStatusProvider;
  IdentificationProvider? _identificationProvider;
  MemberProvider? _memberProvider;

  @override
  Widget build(BuildContext context) {
    _menuStatusProvider = Provider.of<MenuStatusProvider>(context);
    _memberProvider = Provider.of<MemberProvider>(context);
    _identificationProvider = Provider.of<IdentificationProvider>(context);
    return BackButtonListener(child: Column(
      children: [
        //選單與X
        titleBarIntegrate([],S.of(context).menu, [closeWidget()]),
        //內容
        Expanded(flex: 10, child: bodyMenu(context))
      ],
    ), onBackButtonPressed: () => pop());
  }

  Future<bool> pop() async {
    _menuStatusProvider!.closeMenu();
    return true;
  }

  void menuCloseOnTap() {
    _menuStatusProvider!.closeMenu();
  }

  Widget closeWidget(){
    return GestureDetector(
      onTap: () => menuCloseOnTap(),
      child: const Icon(
        Icons.clear,
        color: AppColor.appTitleBarTextColor,
        size: 30,
      ),
    );
  }

  Widget bodyMenu(context) {
    return Container(
      decoration: const BoxDecoration(
        color: Colors.transparent,
      ),
      child: ScrollConfiguration(
        behavior: NoShadowScrollBehavior(),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(
                height: 3.h,
              ),
              _itemButton(AppImage.iconIdentityRecord,
                  S.of(context).identification_record, _identificationOnTap),
              SizedBox(
                height: 3.h,
              ),
              _itemButton(AppImage.iconSuggestion, S.of(context).opinion,
                  _opinionOnTap),
            ],
          ),
        ),
      ),
    );
  }

  //辨識紀錄
  void _identificationOnTap() async {
    EasyLoading.show(status: 'Loading...');
    await _identificationProvider!.getFacesDetectHistoryByTargetMemberIdAndFromMemberId(_memberProvider!.memberModel.id!);
    EasyLoading.dismiss();
    //預設辯識過我的
    _identificationProvider!.setFacesMember(faces: FacesMember.facesTargetMember);
    delegate.push(name: RouteName.identificationPage);
  }

//意見
  void _opinionOnTap() {
    delegate.push(name: RouteName.opinionPage);
  }

  //選項
  Widget _itemButton(String _icon, String _title, VoidCallback _click) {
    return GestureDetector(
      onTap: () => _click(),
      child: Container(
        height: 12.h,
        width: Device.boxConstraints.maxWidth,
        margin: EdgeInsets.only(left: 10.w, right: 10.w),
        decoration: BoxDecoration(
            gradient: AppColor.appMainColor,
            borderRadius: BorderRadius.circular(15)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            SizedBox(
              width: 8.w,
            ),
            Image(
              width: 15.w,
              image: AssetImage(_icon),
            ),
            SizedBox(
              width: 5.w,
            ),
            Text(
              _title,
              style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 20.sp),
            )
          ],
        ),
      ),
    );
  }
}
