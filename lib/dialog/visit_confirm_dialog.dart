import 'dart:async';
import 'dart:developer';

import 'package:ashera_flutter/provider/member_provider.dart';
import 'package:ashera_flutter/provider/visit_game_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../app.dart';
import '../generated/l10n.dart';
import '../models/visit_record_model.dart';
import '../provider/util_api_provider.dart';
import '../routers/route_name.dart';
import '../utils/app_color.dart';
import '../utils/app_image.dart';
import '../utils/util.dart';
import '../widget/button_widget.dart';
import '../widget/gender_icon.dart';
import '../widget/mugshot_widget.dart';

///
/// 探班 同意或不同意
///
/// * [title] 最上層顯示
/// * [content] 內容
/// * [confirmButtonText] 確認按鈕文字
/// * [cancelButtonText] 取消按鈕文字
///

class VisitConfirmDialog extends StatefulWidget {
  final String title;
  final VisitRecordModel content;
  final String confirmButtonText;
  final String cancelButtonText;

  const VisitConfirmDialog(
      {Key? key,
      required this.title,
      required this.content,
      required this.confirmButtonText,
      required this.cancelButtonText})
      : super(key: key);

  @override
  State createState() => _VisitConfirmDialogState();
}

class _VisitConfirmDialogState extends State<VisitConfirmDialog> {


  Future<double>? _future;
  UtilApiProvider? _utilApiProvider;
  VisitGameProvider? _visitGameProvider;

  @override
  Widget build(BuildContext context) {
    _utilApiProvider = Provider.of<UtilApiProvider>(context, listen: false);
    _visitGameProvider = Provider.of<VisitGameProvider>(context, listen: false);
    return BackButtonListener(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.black54,
          body: Center(
            child: Container(
              width: Device.width - 60,
              height: 50.h,
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(20)),
              child: Column(
                children: [
                  //title
                  SizedBox(
                    width: MediaQuery.of(context).size.width,
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 1.5.h),
                          child: Text(
                            widget.title,
                            style: TextStyle(
                                fontSize: 20.sp,
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        Positioned(
                            right: 15,
                            child: GestureDetector(
                              onTap: () => Navigator.of(context).pop(false),
                              child: Container(
                                width: 30,
                                alignment: Alignment.center,
                                decoration: const BoxDecoration(
                                    color: Colors.grey, shape: BoxShape.circle),
                                child: const Icon(
                                  Icons.close,
                                  size: 30,
                                  color: Colors.white,
                                ),
                              ),
                            ))
                      ],
                    ),
                  ),
                  //灰線
                  Container(
                    height: 0.3.h,
                    width: Device.width,
                    color: AppColor.grayLine,
                  ),
                  //內容
                  Expanded(
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            const SizedBox(
                              width: 15,
                            ),
                            //頭像
                            Flexible(child: Consumer<MemberProvider>(
                              builder: (context, member, _) {
                                log('這邊再刷?');
                                return SizedBox(
                                  width: 70,
                                  height: 70,
                                  child: MugshotWidget(
                                      fromMemberId: member.memberModel.id!,
                                      nickName:
                                          widget.content.fromMemberNickname!,
                                      gender: widget.content.fromMemberGender!,
                                      birthday:
                                          widget.content.fromMemberBirthday!,
                                      targetMemberId:
                                          widget.content.fromMemberId!,
                                      interactive: true),
                                );
                              },
                            )),
                            const SizedBox(
                              width: 10,
                            ),
                            Expanded(
                                child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                //名稱 性別年齡
                                Row(
                                  children: [
                                    const SizedBox(
                                      width: 15,
                                    ),
                                    //名稱
                                    Container(
                                      alignment: Alignment.center,
                                      child: Text(
                                        widget.content.fromMemberNickname!,
                                        style: TextStyle(
                                            fontSize: 20.sp,
                                            color: Colors.black,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ),
                                    const SizedBox(
                                      width: 15,
                                    ),
                                    //性別
                                    getGender(widget.content.fromMemberGender!),
                                  ],
                                ),
                                SizedBox(
                                  height: 0.5.h,
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    //星星
                                    GestureDetector(
                                      onTap: () => _starOnTap(
                                          id: widget.content.fromMemberId!),
                                      child: Consumer<UtilApiProvider>(builder: (context, util, _){
                                        return Row(
                                          children: [
                                            Container(
                                              margin: const EdgeInsets.only(
                                                  left: 10, right: 5),
                                              width: 5.w,
                                              child: Image.asset(
                                                  AppImage.iconStar),
                                            ),
                                            FutureBuilder(
                                              future: _future,
                                              builder: (BuildContext context,
                                                  AsyncSnapshot<dynamic>
                                                  snapshot) {
                                                if (snapshot.connectionState ==
                                                    ConnectionState.done) {
                                                  return Text(
                                                    snapshot.data!
                                                        .toStringAsFixed(1),
                                                    style: const TextStyle(
                                                        color: Colors.black),
                                                  );
                                                }
                                                return Container();
                                              },
                                            ),
                                          ],
                                        );
                                      },),
                                    ),
                                    //選單
                                    PopupMenuButton(
                                      shape: const RoundedRectangleBorder(
                                          borderRadius: BorderRadius.all(
                                              Radius.circular(5))),
                                      itemBuilder: (BuildContext context) {
                                        return [
                                          PopupMenuItem(
                                              child: Container(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width /
                                                2.1,
                                            padding: const EdgeInsets.symmetric(
                                                vertical: 5, horizontal: 5),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                FittedBox(
                                                  fit: BoxFit.scaleDown,
                                                  child: Text(
                                                    S.of(context).gender,
                                                    style: const TextStyle(
                                                        color: Colors.black,
                                                        fontSize: 18,
                                                        height: 1.1),
                                                  ),
                                                ),
                                                Row(
                                                  children: [
                                                    //性別 文字
                                                    _maleOrFemaleText(
                                                        gender: widget.content
                                                            .fromMemberGender!),
                                                    //性別 Icon
                                                    getGender(widget.content
                                                        .fromMemberGender!),
                                                  ],
                                                ),
                                              ],
                                            ),
                                          )),
                                          PopupMenuItem(
                                              child: Container(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width /
                                                2.1,
                                            padding: const EdgeInsets.symmetric(
                                                vertical: 5, horizontal: 5),
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment
                                                      .spaceBetween,
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.center,
                                              children: [
                                                FittedBox(
                                                  fit: BoxFit.scaleDown,
                                                  child: Text(
                                                    S.of(context).age,
                                                    style: const TextStyle(
                                                        color: Colors.black,
                                                        fontSize: 18,
                                                        height: 1.1),
                                                  ),
                                                ),
                                                Row(
                                                  children: [
                                                    FittedBox(
                                                      fit: BoxFit.scaleDown,
                                                      child: Text(
                                                        '${int.parse(Util().getAge(widget.content.fromMemberBirthday!))}',
                                                        style: TextStyle(
                                                            fontSize: 18.sp,
                                                            color: Colors.black,
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            height: 1.1),
                                                      ),
                                                    ),
                                                    Container(
                                                      width: 35,
                                                    )
                                                  ],
                                                )
                                              ],
                                            ),
                                          )),
                                        ];
                                      },
                                      tooltip: '',
                                      offset: const Offset(0, 35),
                                      child: const Icon(
                                        Icons.keyboard_arrow_down_sharp,
                                        size: 30,
                                        color: Colors.grey,
                                      ),
                                    )
                                  ],
                                )
                              ],
                            ))
                          ],
                        ),
                        Positioned(
                          bottom: 5,
                          child: Consumer<VisitGameProvider>(builder: (context, visit, _){
                            return FittedBox(
                              fit: BoxFit.scaleDown,
                              child: Text(
                                '${visit.sec}s ${S.of(context).overdue_automatic_cancellation}',
                                style: const TextStyle(
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 18,
                                    height: 1.1),
                              ),
                            );
                          },),
                        )
                      ],
                    ),
                  ),
                  const SizedBox(
                    height: 15,
                  ),
                  //同意按鈕
                  GestureDetector(
                    onTap: () => Navigator.of(context).pop(true),
                    child: Container(
                      margin: EdgeInsets.only(bottom: 2.h),
                      width: 58.w,
                      height: 7.h,
                      child: buttonWidget(widget.confirmButtonText),
                    ),
                  ),
                  //不同意按鈕
                  //按鈕
                  GestureDetector(
                    onTap: () => Navigator.of(context).pop(false),
                    child: Container(
                      margin: EdgeInsets.only(bottom: 5.h),
                      width: 58.w,
                      height: 7.h,
                      child: buttonWidgetDispose(widget.cancelButtonText),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
        onBackButtonPressed: () => pop());
  }

  ///
  ///男或女 字
  ///
  ///[gender] 性別
  Widget _maleOrFemaleText({required int gender}) {
    return gender == 1
        ? FittedBox(
            fit: BoxFit.scaleDown,
            child: Text(
              S.of(context).female,
              style: TextStyle(
                  fontSize: 18.sp,
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  height: 1.1),
            ),
          )
        : FittedBox(
            fit: BoxFit.scaleDown,
            child: Text(
              S.of(context).male,
              style: TextStyle(
                  fontSize: 18.sp,
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  height: 1.1),
            ),
          );
  }

  _onLayoutDone(_) {
    _future =
        _utilApiProvider!.getListOtherStar(id: widget.content.fromMemberId!);
    _visitGameProvider!.startTimer();
    _visitGameProvider!.addListener(_listener);
  }

  @override
  void initState() {
    super.initState();
    //log('探班對話框初始化');
    util.dialogIsOpen = true;
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  void _listener(){
    if(_visitGameProvider!.sec == 0){
      if(delegate.pages.where((element) => element.name == RouteName.visitStarRecordIndexPage).isNotEmpty){
        delegate.popRoute();
        delegate.popRoute();
      }else if(delegate.pages.where((element) => element.name == RouteName.visitStarRecordPage).isNotEmpty){
        delegate.popRoute();
      }
      Future.delayed(const Duration(milliseconds: 100), (){
        Navigator.of(context).pop(false);
      });
    }
  }

  @override
  void dispose() {
    _visitGameProvider!.stopTimer();
    super.dispose();
    //log('探班對話框解除');
    util.dialogIsOpen = false;
  }

  Future<bool> pop() async {
    Navigator.of(context).pop(false);
    return true;
  }

  ///星星點擊
  /// * [id] 用戶ID
  void _starOnTap({required int id}) {
    //log('點的到星星? $id');
    delegate.push(name: RouteName.visitStarRecordPage, arguments: id);
  }
}
