import 'package:ashera_flutter/generated/l10n.dart';
import 'package:ashera_flutter/utils/app_color.dart';
import 'package:ashera_flutter/utils/no_shadow_scroll_behavior.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../utils/util.dart';
import '../widget/button_widget.dart';
import '../widget/gender_icon.dart';
import '../widget/mugshot_widget.dart';

class RatingDialog extends StatefulWidget {
  final String? nickName;
  final String? birthday;
  final int? gender;
  final int? targetMemberId;

  const RatingDialog({
    Key? key,
    required this.nickName,
    required this.birthday,
    required this.gender,
    required this.targetMemberId
  }) : super(key: key);

  @override
  _RatingDialogState createState() => _RatingDialogState();
}

class _RatingDialogState extends State<RatingDialog> {

  double _rating = 5;

  //評語輸入控制器
  final TextEditingController _input = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BackButtonListener(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.black54,
          body: Center(
            child: Container(
              width: Device.width - 60,
              height: 75.h,
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(20)),
              child: ScrollConfiguration(
                behavior: NoShadowScrollBehavior(),
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      //title
                      Container(
                        margin: EdgeInsets.symmetric(vertical: 1.h),
                        child: Text(
                          S.of(context).rating_bar,
                          style: TextStyle(
                              fontSize: 20.sp,
                              color: Colors.black,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      //灰線
                      Container(
                        height: 0.3.h,
                        width: Device.width,
                        color: AppColor.grayLine,
                      ),
                      //對方的資料
                      memberInfo(),
                      //評分欄
                      _ratingBar(),
                      //評語輸入框
                      commitInput(),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
        onBackButtonPressed: () => pop());
  }

  Future<bool> pop() async {
    Navigator.of(context).pop();
    return true;
  }

  //對方的資料
  Widget memberInfo() {
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 40, vertical: 10),
      child: Row(
        children: [
          //頭像
          MugshotWidget(
              fromMemberId: 0,
              birthday: widget.birthday!,
              nickName: widget.nickName!,
              gender: widget.gender!,
              targetMemberId: widget.targetMemberId!,
              interactive: true,
          ),
          const SizedBox(width: 10,),
          //名稱&性別年齡&距離
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                //名稱
                FittedBox(
                  child: Text(
                    widget.nickName!,
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 20.sp,
                        color: Colors.black),
                  ),
                ),
                SizedBox(
                  height: 0.5.h,
                ),
                //性別年齡
                Row(
                  children: [
                    getGender(widget.gender!),
                    //年齡
                    Container(
                      margin: const EdgeInsets.only(left: 5),
                      child: Text(
                        Util().getAge(widget.birthday!),
                        style: const TextStyle(color: AppColor.grayText),
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  //評分欄
  Widget _ratingBar() {
    return RatingBar.builder(
      initialRating: 5,
      minRating: 1,
      direction: Axis.horizontal,
      allowHalfRating: true,
      itemCount: 5,
      itemSize: 50,
      itemPadding: EdgeInsets.zero,
      itemBuilder: (context, _) => const Icon(
        Icons.star_rounded,
        color: AppColor.yellowStar,
      ),
      onRatingUpdate: (value) {
        _rating = value;
      },
    );
  }

  //評語輸入框
  Widget commitInput() {
    return Container(
      margin: EdgeInsets.only(top: 3.h),
      padding: EdgeInsets.symmetric(horizontal: 5.w),
      width: Device.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          //評語輸入框
          TextField(
            style: const TextStyle(color: Colors.black),
            decoration: InputDecoration(
              contentPadding: const EdgeInsets.all(5),
              enabledBorder: OutlineInputBorder(
                  borderSide: const BorderSide(color: AppColor.grayLine),
                  borderRadius: BorderRadius.circular(5)),
              border:
                  OutlineInputBorder(borderRadius: BorderRadius.circular(5)),
              fillColor: Colors.white,
              hintText: S.of(context).please_enter_your_commit,
              filled: true,
            ),
            maxLines: 8,
            keyboardType: TextInputType.multiline,
            textInputAction: TextInputAction.newline,
            controller: _input,
          ),
          //送出
          _sendButton()
        ],
      ),
    );
  }

  //送出
  Widget _sendButton() {
    return GestureDetector(
        onTap: () => _sendOnTap(),
        child: Container(
            height: 7.h,
            width: 58.w,
            margin: EdgeInsets.symmetric(vertical: 5.h),
            child: buttonWidget(S.of(context).send_commit)));
  }

  //送出事件
  void _sendOnTap() {
    Map<String, dynamic> _map = {
      'rating':_rating,
      'commit':_input.text
    };
    Navigator.of(context).pop(_map);
  }
}
