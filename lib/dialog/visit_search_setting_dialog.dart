
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../enum/app_enum.dart';
import '../generated/l10n.dart';
import '../provider/visit_game_provider.dart';
import '../utils/app_color.dart';
import '../widget/button_widget.dart';

class VisitSearchSettingDialog extends StatefulWidget{
  const VisitSearchSettingDialog({Key? key}): super(key: key);

  @override
  State createState() => _VisitSearchSettingDialogState();
}

class _VisitSearchSettingDialogState extends State<VisitSearchSettingDialog>{
  VisitGameProvider? _visitGameProvider;

  //年齡範圍
  RangeValues _ageRange = const RangeValues(18, 100);

  //距離範圍
  double _distance = 1;

  _onLayoutDone(_){
    _ageRange = RangeValues(
        double.parse(_visitGameProvider!.minAge.toString()),
        double.parse(_visitGameProvider!.maxAge.toString()));

    _distance = double.parse(_visitGameProvider!.distance.toString());

    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  Widget build(BuildContext context) {
    _visitGameProvider = Provider.of<VisitGameProvider>(context);
    return BackButtonListener(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.black12,
          body: Center(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 5.w, vertical: 5.h),
              width: Device.width,
              height: Device.height,
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(20)),
              child: Stack(
                children: [
                  _body(),
                  Positioned(top: 5, right: 15, child: _closeButton()),
                ],
              ),
            ),
          ),
        ),
        onBackButtonPressed: () => pop());
  }

  //內容
  Widget _body() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        //title
        Container(
          margin: EdgeInsets.symmetric(vertical: 1.5.h),
          child: Text(
            S.of(context).filter,
            style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 20.sp),
          ),
        ),
        //篩選條件
        Container(
          padding: EdgeInsets.symmetric(horizontal: 3.w, vertical: 2.h),
          width: Device.width,
          color: Colors.grey.shade200,
          child: Text(S.of(context).screening_condition,
              style: TextStyle(
                  color: Colors.grey,
                  fontWeight: FontWeight.bold,
                  fontSize: 17.sp)),
        ),
        //支付方式
        _paidModeSwitchButton(),
        //灰線
        Container(
          margin: EdgeInsets.only(top: 1.h, left: 5.w, right: 5.w),
          height: 0.3.h,
          width: Device.width,
          color: AppColor.grayLine,
        ),
        //年齡
        _ageWidget(),
        //灰線
        Container(
          margin: EdgeInsets.only(top: 4.h, left: 5.w, right: 5.w),
          height: 0.3.h,
          width: Device.width,
          color: AppColor.grayLine,
        ),
        _distanceWidget(),
        //灰線
        Container(
          margin: EdgeInsets.only(top: 4.h, left: 5.w, right: 5.w),
          height: 0.3.h,
          width: Device.width,
          color: AppColor.grayLine,
        ),
        //確定按鈕
        Expanded(child: Center(child: _confirmButton())),
      ],
    );
  }

  //支付方式
  Widget _paidModeSwitchButton() {
    return Consumer<VisitGameProvider>(builder: (context, p, _) {
      return Container(
        padding: EdgeInsets.symmetric(horizontal: 5.w),
        width: Device.width,
        color: Colors.white,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
                margin: EdgeInsets.symmetric(vertical: 1.h),
                child: Text(
                  S.of(context).paid_mode,
                  style:
                  TextStyle(fontSize: 18.sp, fontWeight: FontWeight.bold),
                )),
            Container(
              margin: EdgeInsets.only(bottom: 2.h),
              height: 8.h,
              decoration: BoxDecoration(
                  color: Colors.white,
                  border: Border.all(color: AppColor.grayLine, width: 1),
                  borderRadius: BorderRadius.circular(50)),
              child: Row(
                children: [
                  //全部
                  Expanded(
                      child: GestureDetector(
                        onTap: () => changePaidStatus(WhoPay.all),
                        child: Container(
                          alignment: Alignment.center,
                          margin: p.paidStatusMargin(_visitGameProvider!.paidStatus, WhoPay.all),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                              color: p.paidStatusButtonColor(_visitGameProvider!.paidStatus, WhoPay.all)),
                          child: Text(
                            S.of(context).all,
                            style: TextStyle(
                                fontSize: 18.sp,
                                color: p.paidStatusTextColor(_visitGameProvider!.paidStatus, WhoPay.all)),
                          ),
                        ),
                      )),
                  SizedBox(
                    width: 1.w,
                  ),
                  //探班
                  Expanded(
                      child: GestureDetector(
                        onTap: () => changePaidStatus(WhoPay.whoPaySelf),
                        child: Container(
                          alignment: Alignment.center,
                          margin: p.paidStatusMargin(_visitGameProvider!.paidStatus, WhoPay.whoPaySelf),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                              color: p.paidStatusButtonColor(_visitGameProvider!.paidStatus, WhoPay.whoPaySelf)),
                          child: Text(
                            S.of(context).self_paid,
                            style: TextStyle(
                                fontSize: 18.sp,
                                color: p.paidStatusTextColor(_visitGameProvider!.paidStatus, WhoPay.whoPaySelf)),
                          ),
                        ),
                      )),
                  SizedBox(
                    width: 1.w,
                  ),
                  //被探班
                  Expanded(
                      child: GestureDetector(
                        onTap: () => changePaidStatus(WhoPay.whoPayOther),
                        child: Container(
                          alignment: Alignment.center,
                          margin: p.paidStatusMargin(_visitGameProvider!.paidStatus, WhoPay.whoPayOther),
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(50),
                              color: p.paidStatusButtonColor(_visitGameProvider!.paidStatus, WhoPay.whoPayOther)),
                          child: Text(
                            S.of(context).opposite_paid,
                            style: TextStyle(
                                fontSize: 18.sp,
                                color: p.paidStatusTextColor(_visitGameProvider!.paidStatus, WhoPay.whoPayOther)),
                          ),
                        ),
                      )),
                ],
              ),
            ),
          ],
        ),
      );
    });
  }

  //切換支付方式事件
  void changePaidStatus(WhoPay input) {
    if (_visitGameProvider!.paidStatus == input) {
      return;
    }
    _visitGameProvider!.changePaidStatus(input);
  }

  //年齡
  Widget _ageWidget(){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 5.w),
      child: Column(
        children: [
          //年齡文字
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              //年齡文字
              Container(
                margin: EdgeInsets.symmetric(vertical: 1.7.h),
                child: Text(
                  S.of(context).age,
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 17.sp),
                ),
              ),
              //範圍
              Container(
                margin: EdgeInsets.symmetric(vertical: 1.7.h),
                child: Text(
                  "${_ageRange.start.toInt()} - ${_ageRange.end.toInt()}",
                  style: TextStyle(
                      color: AppColor.yellowIcon,
                      fontWeight: FontWeight.bold,
                      fontSize: 17.sp),
                ),
              ),
            ],
          ),
          //年齡範圍
          _ageRangeSlider(),
        ],
      ),
    );
  }

  //年齡
  Widget _ageRangeSlider() {
    return RangeSlider(
        values: _ageRange,
        min: 18,
        max: 100,
        activeColor: AppColor.pinkRedColor,
        inactiveColor: AppColor.buttonFrameDisposeColor,
        onChanged: (RangeValues newRange) {
          setState(() {
            if(newRange.start >=18 && newRange.end >=18){
              _ageRange = newRange;
              _visitGameProvider!.setSettingAge(min: newRange.start.ceil(),max: newRange.end.ceil());
            }
          });
        });
  }

  //距離
  Widget _distanceWidget(){
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 5.w),
      child: Column(
        children: [
          //距離文字
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              //距離文字
              Container(
                margin: EdgeInsets.symmetric(vertical: 1.7.h),
                child: Text(
                  S.of(context).distance,
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 17.sp),
                ),
              ),
              //距離
              Container(
                margin: EdgeInsets.symmetric(vertical: 1.7.h),
                child: Text(
                  "${_distance.toInt()}km",
                  style: TextStyle(
                      color: AppColor.yellowIcon,
                      fontWeight: FontWeight.bold,
                      fontSize: 17.sp),
                ),
              ),
            ],
          ),
          //距離範圍
          _distanceRangeSlider(),
        ],
      ),
    );
  }

  //距離
  Widget _distanceRangeSlider() {
    return Slider(
      value: _distance,
      onChanged: (value){
        setState(() {
          _distance = value;
        });
        _visitGameProvider!.setSettingDistance(value: _distance.ceil());
      },
      min: 1,
      max: 6,
      activeColor: AppColor.pinkRedColor,
      inactiveColor: AppColor.buttonFrameDisposeColor,
    );
  }

  //確定按鈕
  Widget _confirmButton(){
    return GestureDetector(
      onTap: () => Navigator.of(context).pop(true),
      child: Container(
        margin: EdgeInsets.only(bottom: 2.h),
        width: 58.w,
        height: 7.h,
        child: buttonWidget(S.of(context).sure),
      ),
    );
  }


  //X
  Widget _closeButton() {
    return GestureDetector(
      onTap: () => _closeOnTap(),
      child: const Icon(
        Icons.cancel,
        size: 35,
        color: Colors.grey,
      ),
    );
  }

  //X onTap
  void _closeOnTap() {
    Navigator.of(context).pop(false);
  }

  Future<bool> pop() async {
    Navigator.of(context).pop(false);
    return true;
  }
}