import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_font_icons/flutter_font_icons.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../generated/l10n.dart';
import '../utils/app_color.dart';
import '../utils/app_image.dart';
import '../widget/button_widget.dart';

class PurchasesDialog extends StatefulWidget {
  final String title;
  final int price;
  final String confirmButtonText;

  const PurchasesDialog(
      {Key? key,
      required this.title,
      required this.price,
      required this.confirmButtonText})
      : super(key: key);

  @override
  State createState() => _PurchasesDialogState();
}

class _PurchasesDialogState extends State<PurchasesDialog> {
  final TextEditingController _counterText = TextEditingController();
  int _counter = 1;

  @override
  void initState() {
    super.initState();
    _counterText.text = '$_counter';
  }

  @override
  Widget build(BuildContext context) {
    return BackButtonListener(
        child: Scaffold(
          resizeToAvoidBottomInset: true,
          backgroundColor: Colors.black54,
          body: Center(
            child: Container(
              width: Device.width - 60,
              height: 40.h,
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(20)),
              child: Stack(
                children: [
                  //內容
                  Column(
                    children: [
                      //title
                      Container(
                        margin: EdgeInsets.symmetric(vertical: 1.5.h),
                        child: Text(
                          widget.title,
                          style: TextStyle(
                              fontSize: 20.sp,
                              color: Colors.black,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      //灰線
                      Container(
                        height: 0.3.h,
                        width: Device.width,
                        color: AppColor.grayLine,
                      ),
                      //內容
                      Expanded(
                          child: Container(
                        alignment: Alignment.center,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            _buttonWidget(
                                symbol: FontAwesome5Solid.minus,
                                onTap: _minusOnTap),
                            SizedBox(
                              width: 5.w,
                            ),
                            Container(
                                width: 20.w,
                                height: 8.h,
                                alignment: Alignment.center,
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    border: Border.all(
                                        color: AppColor.appFaceBackgroundColor,
                                        width: 1.0),
                                    borderRadius: BorderRadius.circular(15),
                                    boxShadow: [
                                      BoxShadow(
                                          color: Colors.grey[400]!,
                                          offset:
                                              const Offset(0.1, 0.5), //陰影y軸偏移量
                                          blurRadius: 8, //陰影模糊程度
                                          spreadRadius: 3 //陰影擴散程度
                                          )
                                    ]),
                                child: TextFormField(
                                  textAlign: TextAlign.center,
                                  style: const TextStyle(color: Colors.black),
                                  decoration: const InputDecoration(
                                    border: InputBorder.none,
                                    focusedBorder: InputBorder.none,
                                    fillColor: Colors.transparent,
                                    filled: true,
                                    hintStyle: TextStyle(color: Colors.grey),
                                    hintText: '',
                                  ),
                                  inputFormatters: [
                                    FilteringTextInputFormatter.allow(
                                        RegExp(r"[0-9]")),
                                  ],
                                  keyboardType:
                                      const TextInputType.numberWithOptions(
                                          decimal: false, signed: false),
                                  textInputAction: TextInputAction.done,
                                  controller: _counterText,
                                  onChanged: (value) {
                                    if (int.parse(value) <= 99) {
                                      _counter = int.parse(value);
                                      setState(() {});
                                    } else {
                                      _counterText.text = '99';
                                    }
                                  },
                                )),
                            SizedBox(
                              width: 5.w,
                            ),
                            _buttonWidget(
                                symbol: FontAwesome5Solid.plus,
                                onTap: _plusOnTap)
                          ],
                        ),
                      )),
                      //價格
                      FittedBox(
                        child: RichText(
                          text: TextSpan(
                              text: S.of(context).ok_to_use,
                              style: const TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 20,
                                  height: 1.1),
                              children: [
                                WidgetSpan(
                                    child: Image(
                                  width: 5.w,
                                  image: AssetImage(AppImage.iconDiamond),
                                )),
                                TextSpan(
                                  text: '${widget.price * _counter}',
                                  style: const TextStyle(
                                      color: Colors.red,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20,
                                      height: 1.1),
                                ),
                                TextSpan(
                                  text: S.of(context).buy,
                                  style: const TextStyle(
                                      color: Colors.black,
                                      fontWeight: FontWeight.bold,
                                      fontSize: 20,
                                      height: 1.1),
                                )
                              ]),
                        ),
                      ),
                      SizedBox(height: 1.5.h,),
                      //同意按鈕
                      GestureDetector(
                        onTap: () => Navigator.of(context)
                            .pop({'status': true, 'value': _counter}),
                        child: Container(
                          margin: EdgeInsets.only(bottom: 2.h),
                          width: 58.w,
                          height: 7.h,
                          child: buttonWidget(widget.confirmButtonText),
                        ),
                      ),
                    ],
                  ),
                  //右上X
                  Positioned(
                      right: 10,
                      top: 10,
                      child: GestureDetector(
                        onTap: () =>
                            Navigator.of(context).pop({'status': false}),
                        child: Icon(
                          Icons.cancel,
                          size: 10.w,
                        ),
                      )),
                ],
              ),
            ),
          ),
        ),
        onBackButtonPressed: () => pop());
  }

  ///按鈕
  /// * [symbol] icon
  /// * [onTap] 點擊事件
  Widget _buttonWidget(
      {required IconData symbol, required VoidCallback onTap}) {
    return GestureDetector(
      onTap: () => onTap(),
      child: Container(
          width: 15.w,
          height: 8.h,
          alignment: Alignment.center,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              color: Colors.white,
              boxShadow: [
                BoxShadow(
                    color: Colors.grey[400]!,
                    offset: const Offset(0.1, 0.5), //陰影y軸偏移量
                    blurRadius: 8, //陰影模糊程度
                    spreadRadius: 3 //陰影擴散程度
                    )
              ]),
          child: Icon(
            symbol,
            color: AppColor.pinkRedColor,
          )),
    );
  }

  void _plusOnTap() {
    if (_counter < 99) {
      _counter++;
      _counterText.text = '$_counter';
      setState(() {});
    }
  }

  void _minusOnTap() {
    if (_counter > 1) {
      _counter--;
      _counterText.text = '$_counter';
      setState(() {});
    }
  }

  Future<bool> pop() async {
    Navigator.of(context).pop({'status': false});
    return true;
  }
}
