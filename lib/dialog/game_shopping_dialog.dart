import 'dart:developer';

import 'package:ashera_flutter/utils/app_color.dart';
import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../generated/l10n.dart';
import '../utils/app_image.dart';
import '../widget/button_widget.dart';

class GameShoppingDialog extends StatefulWidget{
  final String title;
  final String content;
  final String confirmButtonText;

  const GameShoppingDialog({
    Key? key,
    required this.title,
    required this.content,
    required this.confirmButtonText
  }): super(key: key);

  @override
  State createState() => _GameShoppingDialogState();
}

class _GameShoppingDialogState extends State<GameShoppingDialog>{

  @override
  Widget build(BuildContext context) {
    return BackButtonListener(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.black54,
          body: Center(
            child: Container(
              width: Device.width - 70,
              height: 40.h,
              decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(20)),
              child: Stack(
                children: [
                  //內容
                  Column(
                    children: [
                      //title
                      Container(
                        margin: EdgeInsets.symmetric(vertical: 1.5.h),
                        child: Text(
                          widget.title,
                          style: TextStyle(
                            fontSize: 20.sp,
                            color: Colors.black,
                            fontWeight: FontWeight.bold
                          ),
                        ),
                      ),
                      //灰線
                      Container(
                        height: 0.3.h,
                        width: Device.width,
                        color: AppColor.grayLine,
                      ),
                      //內容
                      Expanded(
                          child: Container(
                            alignment: Alignment.center,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                //確定使用
                                Text('${S.of(context).ok_to_use} ', style: const TextStyle(color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),),
                                //icon
                                Image(
                                  width: 5.w,
                                  image: AssetImage(AppImage.iconDiamond),
                                ),
                                //金額
                                Text(widget.content, style: const TextStyle(color: Colors.red, fontSize: 20, fontWeight: FontWeight.bold),),
                                //購買
                                Text(' ${S.of(context).buy}?', style: const TextStyle(color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold),),
                              ],
                            )
                          )
                      ),
                      //同意按鈕
                      GestureDetector(
                        onTap: () => Navigator.of(context).pop(true),
                        child: Container(
                          margin: EdgeInsets.only(bottom: 2.h),
                          width: 58.w,
                          height: 7.h,
                          child: buttonWidget(widget.confirmButtonText),
                        ),
                      ),

                    ],
                  ),
                  //右上X
                  Positioned(
                      right: 10,
                      top: 10,
                      child: GestureDetector(
                        onTap: () => Navigator.of(context).pop(false),
                        child: Icon(
                          Icons.cancel,
                          size: 10.w,
                        ),
                      )),
                ],
              ),
            ),
          ),
        ),
        onBackButtonPressed: () => pop());
  }

  Future<bool> pop() async {
    Navigator.of(context).pop(false);
    return true;
  }

  @override
  void initState() {
    super.initState();
    //log('購買對話框初始化');
  }

  @override
  void dispose() {
    super.dispose();
    //log('購買對話框解除');
  }
}