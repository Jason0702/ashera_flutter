import 'package:ashera_flutter/provider/member_provider.dart';
import 'package:ashera_flutter/provider/stomp_client_provider.dart';
import 'package:ashera_flutter/provider/util_api_provider.dart';
import 'package:ashera_flutter/utils/api.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../generated/l10n.dart';
import '../models/member_model.dart';
import '../utils/util.dart';
import '../widget/avatar_image.dart';
import '../widget/button_widget.dart';
import '../widget/titlebar_widget.dart';

class QRCheckDialog extends StatefulWidget {
  const QRCheckDialog({Key? key}) : super(key: key);

  @override
  State createState() => _QRCheckDialogState();
}

class _QRCheckDialogState extends State<QRCheckDialog> {

  MemberProvider? _memberProvider;
  StompClientProvider? _stompClientProvider;

  _onLayoutDone(_) async {
    debugPrint('QR有掃到陌生人畫面');
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _memberProvider = Provider.of<MemberProvider>(context);
    _stompClientProvider = Provider.of<StompClientProvider>(context);
    return BackButtonListener(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          body: SafeArea(
            child: Column(
              children: [
                //標題 與 X
                titleBarIntegrate([], S.of(context).add_friends, []),
                //內容
                Expanded(child: _body()),
              ],
            ),
          ),
        ),
        onBackButtonPressed: () => pop());
  }

  Widget _body() {
    return SizedBox(
      width: Device.boxConstraints.maxWidth,
      child: Consumer<UtilApiProvider>(
        builder: (context, member, _) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              SizedBox(
                height: 5.h,
              ),
              //大頭照
              if (member.memberModel != null)
                member.memberModel!.mugshot!.isEmpty
                    ? noAvatarImage()
                    //沒照片
                    : Container(
                        width: 30.w,
                        height: 20.h,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              fit: BoxFit.cover,
                              image: NetworkImage(
                                Util().getMemberMugshotUrl(
                                    member.memberModel!.mugshot!),
                                headers: {
                                  "authorization": "Bearer " + Api.accessToken
                                },
                              ),
                              onError: (error, stackTrace) {
                                debugPrint('Error: ${error.toString()}');
                              }),
                        ),
                      ),
              //名子
              if (member.memberModel != null)
                Text(
                  member.memberModel!.nickname!,
                  style: const TextStyle(
                      fontSize: 20, fontWeight: FontWeight.bold),
                ),
              SizedBox(
                height: 3.h,
              ),
              //加入
              _isAddOrCanAdd(member.memberModel!, _stompClientProvider!.friendList),
              SizedBox(
                height: 5.h,
              ),
              //取消
              GestureDetector(
                onTap: () => _cancelOnTap(),
                child: SizedBox(
                  height: 7.h,
                  width: 58.w,
                  child: buttonWidgetWaiting(S.of(context).cancel),
                ),
              )
            ],
          );
        },
      ),
    );
  }

  void _addOnTap() {
    Navigator.of(context).pop(true);
  }

  void _cancelOnTap() {
    Navigator.of(context).pop(false);
  }

  Future<bool> pop() async {
    Navigator.of(context).pop(false);
    return true;
  }

  ///
  /// 添加好友或已是好友 字
  ///
  /// [faceImageModel] 人臉辨識結果
  /// [friendList] 好友列表
  Widget _isAddOrCanAddText(MeModel faceImageModel, List<MemberModel> friendList){
    if(friendList.where((element) => element.asheraUid == faceImageModel.asheraUid).isEmpty){
      return Text(
        S.of(context).identify_time_over_content,
        style: TextStyle(fontSize: 20.sp, color: Colors.black),
      );
    }else{
      return Text(
        S.of(context).added_friend,
        style: TextStyle(fontSize: 20.sp, color: Colors.black),
      );
    }
  }

  ///
  ///添加好友或已是好友 按鈕
  ///
  /// [faceImageModel] 人臉辨識結果
  /// [friendList] 好友列表
  ///
  /// 判斷已有此好友，愛心按鈕將反灰
  Widget _isAddOrCanAdd(MeModel faceImageModel, List<MemberModel> friendList){
    if(_memberProvider!.memberModel.id != faceImageModel.id){
      if(friendList.where((element) => element.id == faceImageModel.id).isEmpty){
        return GestureDetector(
          onTap: () => _addOnTap(),
          child: SizedBox(
            height: 7.h,
            width: 58.w,
            child: buttonWidget(S.of(context).add),
          ),
        );
      } else {
        return GestureDetector(
          onTap: () => null,
          child: SizedBox(
            height: 7.h,
            width: 58.w,
            child: buttonWidgetDispose(S.of(context).added_friend),
          ),
        );
      }
    } else {
      return GestureDetector(
        onTap: () => null,
        child: SizedBox(
          height: 7.h,
          width: 58.w,
          child: buttonWidgetDispose(S.of(context).added_friend),
        ),
      );
    }
  }

}
