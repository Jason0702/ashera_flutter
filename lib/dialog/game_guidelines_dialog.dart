import 'dart:developer';

import 'package:ashera_flutter/utils/app_color.dart';
import 'package:ashera_flutter/utils/app_image.dart';
import 'package:ashera_flutter/widget/indicator.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../generated/l10n.dart';
import '../provider/game_home_provider.dart';
import '../provider/member_provider.dart';
import '../utils/util.dart';
import '../widget/avatar_image.dart';
import '../widget/button_widget.dart';
import '../widget/gender_icon.dart';


class GameGuidelinesDialog extends StatefulWidget{
  const GameGuidelinesDialog({Key? key}): super(key: key);

  @override
  State createState() => _GameGuidelinesDialogState();
}
class _GameGuidelinesDialogState extends State<GameGuidelinesDialog>{
  final PageController _pageController = PageController();
  //視角操控器
  TransformationController transformationController =
  TransformationController();

  //菜單高度
  final double _menuHeight = 15.h;

  //底部功能列按鈕寬度
  static final double _bottomWidth = 20.w;

  //左側功能列按鈕寬度
  static final double _leftWidth = 14.w;

  //家具高度
  final double _screenHeight = Device.height - 18.5.h;
  
  final List<AssetImage> _listImage = [
    AssetImage(AppImage.imgGameGuides1),
    AssetImage(AppImage.imgGameGuides2),
    AssetImage(AppImage.imgGameGuides3),
    AssetImage(AppImage.imgGameGuides4),
  ];
  _onLayoutDone(_) async {
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  Widget build(BuildContext context) {
    return BackButtonListener(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.black45,
          body: SizedBox(
            width: Device.width,
            height: Device.height,
            child: Stack(
              alignment: Alignment.center,
              children: [
                _centerWidget(),
                Positioned(
                    bottom: 5.h,
                    child: Indicator(
                      controller: _pageController,
                      itemCount: _listImage.length,
                      normalColor: Colors.grey,
                      selectedColor: AppColor.pinkRedColor,
                    ))
              ],
            ),
          ),
        ),
        onBackButtonPressed: () => pop());
  }
  
  Widget _centerWidget(){
    return PageView.builder(
        controller: _pageController,
        onPageChanged: (index) {
          //log('換到哪一頁了: $index');
          setState(() {});
        },
        itemCount: _listImage.length,
        itemBuilder: (context, index){
          if(index < 3){
            return Stack(
              children: [
                _yellowBackGround(),
                //選單背景
                Positioned(bottom: 0, child: _menuBackGround()),
                //家具內擺設
                _interior(),
                //底部功能按鈕
                Positioned(bottom: 0, child: _bottomBar()),
                //會員資訊
                Positioned(
                  top: 0,
                  left: 0,
                  child: Consumer2<MemberProvider, GameHomeProvider>(
                    builder: (context, member, game, _) {
                      return _memberCard(
                          member: member.memberModel,
                          point: member.memberPoint.point!,
                          clean: 0,
                          dung: 0);
                    },
                  ),
                ),
                //左側功能按鈕
                Positioned(left: 3.w, top: 18.h, child: _leftBar()),
                Container(
                  width: Device.width,
                  height: Device.height,
                  decoration: const BoxDecoration(
                    color: Colors.black54
                  ),
                ),
                Container(
                  height: Device.height,
                  decoration: const BoxDecoration(
                    color: Colors.black54
                  ),
                  child: Image(
                    image: _listImage[index],
                  ),
                )
              ],
            );
          }
          return Stack(
            fit: StackFit.loose,
            alignment: Alignment.center,
            children: [
              Image(
                image: _listImage[index],
              ),
              Positioned(
                  bottom: 10.h,
                  child: GestureDetector(
                    onTap: () => _closeOnTap(),
                    child: SizedBox(
                      width: 58.w,
                      height: 7.h,
                      child: buttonWidget(S.of(context).start_the_game),
                    ),
                  ))
            ],
          );
        }
    );
  }

  //選單背景
  Widget _menuBackGround() {
    return SizedBox(
      height: _menuHeight,
      width: Device.width,
      child: Image(
        fit: BoxFit.fill,
        filterQuality: FilterQuality.medium,
        image: AssetImage(AppImage.imgBgMenu),
      ),
    );
  }

  //黃色背景
  Widget _yellowBackGround() {
    return SizedBox(
      width: Device.width,
      height: Device.height,
      child: Image(
        fit: BoxFit.fill,
        filterQuality: FilterQuality.medium,
        image: AssetImage(AppImage.imgBgHome),
      ),
    );
  }

  //底部功能列按鈕
  Widget _bottomBar() {
    return Container(
      width: Device.width,
      padding: EdgeInsets.symmetric(horizontal: 10.w, vertical: 2.h),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          //房子
          SizedBox(
            width: _bottomWidth,
            child: Image.asset(AppImage.btnHomeSelected),
          ),
          //街道
          SizedBox(
            width: _bottomWidth,
            child: Image.asset(AppImage.btnStreet),
          ),
          //設定
          SizedBox(
            width: _bottomWidth,
            child: Image.asset(AppImage.btnSettings),
          ),
        ],
      ),
    );
  }
  //左側功能列按鈕
  Widget _leftBar() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        //購物車
        _shoppingCart(),
        SizedBox(
          height: 2.h,
        ),
        //信件
        _mail(),
        SizedBox(
          height: 2.h,
        ),
        //垃圾桶
        _trashCan(),
      ],
    );
  }

  //購物車按鈕
  Widget _shoppingCart() {
    return SizedBox(
      width: _leftWidth,
      child: Image.asset(AppImage.btnMall),
    );
  }

  //信件
  Widget _mail() {
    return SizedBox(
      width: _leftWidth,
      child: Image.asset(AppImage.btnMessage),
    );
  }

  //垃圾桶
  Widget _trashCan() {
    return SizedBox(
      width: _leftWidth,
      child: Image.asset(AppImage.btnClean),
    );
  }

  //家具內擺設
  Widget _interior() {
    return SizedBox(
      height: _screenHeight,
      child: InteractiveViewer(
        minScale: 1,
        maxScale: 2,
        transformationController: transformationController,
        constrained: false,
        child: Stack(
          alignment: Alignment.bottomLeft,
          children: [
            //空白 撐stack用
            Container(
              height: _screenHeight,
            ),
            //背景
            _roomBg(),
            //櫃子
            Positioned(bottom: 20.h, right: 17.w, child: _cabinet()),
            //圍籬
            Positioned(bottom: 3.h, right: 13.w, child: _fence()),
            //椅子
            Positioned(bottom: 25.h, left: 20.w, child: _chair()),
            //狗
            Positioned(bottom: 15.h, left: 22.w, child: _dog()),
            //床
            Positioned(bottom: 15.h, left: 77.w, child: _bed()),
            //桌子
            Positioned(bottom: 15.5.h, left: 93.w, child: _table()),
          ],
        ),
      ),
    );
  }


  //region 自己人物
  ///會員資訊
  /// * [member] 大頭照
  /// * [clean]  清潔次數
  /// * [dung]  大便次數
  Widget _memberCard(
      {required member,
        required int point,
        required int clean,
        required int dung}) {
    return SizedBox(
      height: 15.h,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              //大頭照
              Container(
                  height: 9.h,
                  margin: EdgeInsets.only(left: 3.w),
                  alignment: Alignment.center,
                  decoration: const BoxDecoration(color: Colors.transparent),
                  child: meAvatarImage(member)),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  //個人資料
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 1.h),
                    child: Row(
                      children: [
                        //名稱
                        Container(
                          child: FittedBox(
                            child: Text(
                              member.nickname!,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18.sp,
                                  color: Colors.black),
                            ),
                          ),
                          margin: const EdgeInsets.only(left: 10),
                        ),
                        //性別年齡
                        Container(
                          margin: const EdgeInsets.only(left: 10),
                          child: Row(
                            children: [
                              getGender(member.gender!),
                              //年齡
                              Container(
                                margin: const EdgeInsets.only(left: 5),
                                child: Text(
                                  Util().getAge(member.birthday!),
                                  style: const TextStyle(
                                      color: Colors.grey, fontSize: 18),
                                ),
                              )
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                      padding: EdgeInsets.only(left: 1.w),
                      child: _pointWidget(point: point))
                ],
              ),
            ],
          ),
          SizedBox(
            height: 1.5.h,
          ),
          Row(
            children: [
              //清潔次數
              _cleanWidget(frequency: clean),
              //大便次數
              _dongWidget(frequency: dung)
            ],
          )
        ],
      ),
    );
  }

  //點數
  Widget _pointWidget({required int point}) {
    return Stack(
      children: [
        Container(
          margin: EdgeInsets.only(left: 2.8.w),
          padding: const EdgeInsets.all(2),
          width: 26.w,
          height: 3.7.h,
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.circular(30),
              border: Border.all(color: AppColor.pinkRedColor, width: 2)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              //空格
              SizedBox(
                width: 5.w,
              ),
              //數字
              Expanded(
                child: FittedBox(
                  child: Text(
                    '$point',
                    style: const TextStyle(
                        color: Colors.black, fontWeight: FontWeight.bold),
                  ),
                ),
              ),
              //+ icon
              SizedBox(child: Image.asset(AppImage.iconTopUp))
            ],
          ),
        ),
        //鑽石
        Positioned(
          left: 0,
          top: -0.5.h,
          child:
          SizedBox(height: 4.7.h, child: Image.asset(AppImage.iconDiamond)),
        ),
      ],
    );
  }

  ///清潔次數
  /// * [frequency] 次數
  Widget _cleanWidget({required int frequency}) {
    return Container(
      margin: EdgeInsets.only(left: 2.8.w),
      padding: const EdgeInsets.all(2),
      width: 21.w,
      height: 3.8.h,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(30),
          border: Border.all(color: AppColor.pinkRedColor, width: 2)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          //掃把
          Image(
            image: AssetImage(AppImage.iconClean),
          ),
          //數字
          Expanded(
            child: FittedBox(
              child: Text(
                '$frequency',
                style: const TextStyle(
                    color: Colors.black, fontWeight: FontWeight.bold),
              ),
            ),
          ),
          //+ icon
          SizedBox(child: Image.asset(AppImage.iconTopUpCleanAndPoop))
        ],
      ),
    );
  }

  ///大便次數
  /// * [frequency] 次數
  Widget _dongWidget({required int frequency}) {
    return Container(
      margin: EdgeInsets.only(left: 2.8.w),
      padding: const EdgeInsets.all(2),
      width: 21.w,
      height: 3.7.h,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(30),
          border: Border.all(color: AppColor.pinkRedColor, width: 2)),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          //大便
          Image(
            image: AssetImage(AppImage.iconPoop),
          ),
          //數字
          Expanded(
            child: FittedBox(
              child: Text(
                '$frequency',
                textAlign: TextAlign.center,
                style: const TextStyle(
                    color: Colors.black, fontWeight: FontWeight.bold),
              ),
            ),
          ),
          //+ icon
          Container(
            width: 3.w,
          )
        ],
      ),
    );
  }

  void _closeOnTap(){
    Navigator.of(context).pop(false);
  }

  Future<bool> pop() async {
    Navigator.of(context).pop(false);
    return true;
  }

  //背景
  Widget _roomBg() {
    return SizedBox(
        height: _screenHeight - 10.h,
        child: Image.asset(
        AppImage.imgItemsRoomBg0,
        fit: BoxFit.fill,)
    );
  }

  //狗
  Widget _dog() {
    return SizedBox(
        width: 40.w,
        child: Image.asset(
          AppImage.imgItemsDog0,
          fit: BoxFit.fill,
        ));
  }

  //圍籬
  Widget _fence() {
    return SizedBox(
        width: 53.w,
        height: 21.h,
        child: Image.asset(
          AppImage.imgItemsFence1,
          fit: BoxFit.fill,
        ));
  }

  //櫃子
  Widget _cabinet() {
    return SizedBox(
        height: 40.h,
        child: Image.asset(
          AppImage.imgItemsCabinet0,
        ));
  }

  //椅子
  Widget _chair() {
    return SizedBox(
        width: 50.w,
        child: Image.asset(
          AppImage.imgItemsChair0,
        ));
  }

  //床
  Widget _bed() {
    return SizedBox(
        width: 120.w,
        child: Image.asset(
          AppImage.imgItemsBed0,
        ));
  }

  //桌子
  Widget _table() {
    return SizedBox(
        width: 49.w,
        child: Image.asset(
          AppImage.imgItemsTable1,
        ));
  }

}