import 'dart:developer';

import 'package:ashera_flutter/utils/api.dart';
import 'package:ashera_flutter/utils/app_image.dart';
import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../app.dart';
import '../utils/util.dart';

class ShowDogDungDialog extends StatefulWidget {
  final String? gifPath;
  const ShowDogDungDialog({Key? key, required this.gifPath}) : super(key: key);

  @override
  State createState() => _ShowDogDungDialogState();
}

class _ShowDogDungDialogState extends State<ShowDogDungDialog> {
  AssetImage? image;
  NetworkImage? imageNet;

  @override
  Widget build(BuildContext context) {
    return BackButtonListener(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.black54,
          body: Center(
            child: Container(
              width: Device.width - 60,
              height: 40.h,
              decoration: BoxDecoration(
                  color: Colors.transparent,
                  borderRadius: BorderRadius.circular(20)),
              alignment: Alignment.center,
              child: widget.gifPath == null
                  ? Image(
                      width: Device.width - 70,
                      image: image!,
                    )
                  : Image(width: Device.width - 70, image: imageNet!),
            ),
          ),
        ),
        onBackButtonPressed: () => pop());
  }

  Future<bool> pop() async {
    //Navigator.of(context).pop({'status': false});
    return true;
  }

  @override
  void initState() {
    super.initState();
    util.isShowDogDungDialog = true;
    //log('狗拉屎對話框: ${util.isShowDogDungDialog}');
    if (widget.gifPath != null) {
      imageNet = NetworkImage(
          Util().getMemberMugshotUrl(widget.gifPath!),
          headers: {"authorization": "Bearer " + Api.accessToken},
      );
    } else {
      image = AssetImage(AppImage.pooping);
    }
  }

  @override
  void dispose() {
    super.dispose();
    if (widget.gifPath != null) {
      imageNet!.evict();
    } else {
      image!.evict();
    }
  }
}
