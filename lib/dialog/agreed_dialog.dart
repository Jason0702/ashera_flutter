import 'package:ashera_flutter/generated/l10n.dart';
import 'package:ashera_flutter/provider/util_api_provider.dart';
import 'package:ashera_flutter/utils/app_color.dart';
import 'package:ashera_flutter/utils/no_shadow_scroll_behavior.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class AgreedDialog extends StatefulWidget {
  const AgreedDialog({Key? key}) : super(key: key);

  @override
  State createState() => _AgreedDialogState();
}

class _AgreedDialogState extends State<AgreedDialog> {
  @override
  Widget build(BuildContext context) {
    return BackButtonListener(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.transparent,
          body: Center(
            child: Container(
              width: MediaQuery.of(context).size.width * 0.9,
              height: MediaQuery.of(context).size.height * 0.9,
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(20)),
              child: Column(
                children: [
                  //title
                  SizedBox(
                    width: MediaQuery.of(context).size.width * 0.9,
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Container(
                          margin: const EdgeInsets.symmetric(vertical: 10),
                          child: Text(
                            S.of(context).user_agreement,
                            style: TextStyle(
                                fontSize: 20.sp,
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        Positioned(right: 5, child: closeWidget())
                      ],
                    ),
                  ),
                  //灰線
                  Container(
                    height: 0.5,
                    width: MediaQuery.of(context).size.width,
                    color: AppColor.grayLine,
                  ),
                  //內容
                  Expanded(
                      child: ScrollConfiguration(
                          behavior: NoShadowScrollBehavior(),
                          child: SingleChildScrollView(
                            child: Padding(
                              padding: const EdgeInsets.only(left: 5, right: 5),
                              child: Consumer<UtilApiProvider>(builder: (context, setting, _){
                                return Html(
                                  data: setting.systemSetting!.privacyPolicy!,
                                );
                              },),
                            ),
                          )))
                ],
              ),
            ),
          ),
        ),
        onBackButtonPressed: () => pop());
  }

  Widget closeWidget(){
    return GestureDetector(
      onTap: () => _closeOnTap(),
      child: const Icon(
        Icons.clear,
        color: AppColor.appTitleBarTextColor,
        size: 30,
      ),
    );
  }

  //關閉
  void _closeOnTap(){
    Navigator.of(context).pop();
  }

  Future<bool> pop() async {
    Navigator.of(context).pop();
    return true;
  }
}
