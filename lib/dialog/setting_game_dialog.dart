
import 'package:ashera_flutter/provider/game_street_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../enum/app_enum.dart';
import '../generated/l10n.dart';
import '../utils/app_color.dart';
import '../widget/button_widget.dart';

class SettingGameDialog extends StatefulWidget {
  const SettingGameDialog({Key? key}) : super(key: key);

  @override
  _SettingGameDialogState createState() => _SettingGameDialogState();
}

class _SettingGameDialogState extends State<SettingGameDialog> {
  GameStreetProvider? _gameStreetProvider;
  //性別index
  int _index = 0;

  //年齡範圍
  RangeValues _ageRange = const RangeValues(18, 100);

  //距離範圍
  double _distance = 1;

  final List<Color> _colorList = [
    AppColor.lightBlue,
    AppColor.pinkRedColor,
    AppColor.yellowIcon
  ];

  final Map<int, int> _genderMap = {
    0: 0,
    1: 1,
    -1: 2
  };
  final Map<int, int> _upDateGenderMap = {
    0: 0,
    1: 1,
    2: -1
  };

  _onLayoutDone(_){
    _index = _genderMap[_gameStreetProvider!.memberGameSetting.filterGender!]!;
    _distance = double.parse(_gameStreetProvider!.memberGameSetting.filterDistance!.toString()) > 6 ? 6 : double.parse(_gameStreetProvider!.memberGameSetting.filterDistance!.toString());
    _ageRange = RangeValues(
      double.parse(_gameStreetProvider!.memberGameSetting.olderThenAge.toString()),
        double.parse(_gameStreetProvider!.memberGameSetting.youngerThenAge.toString())
    );
    setState(() {});
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  Widget build(BuildContext context) {
    _gameStreetProvider = Provider.of<GameStreetProvider>(context, listen: false);
    return BackButtonListener(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.black12,
          body: Center(
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 5.w, vertical: 5.h),
              width: Device.width,
              height: Device.height,
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(20)),
              child: Stack(
                children: [
                  _body(),
                  Positioned(top: 5, right: 15, child: _closeButton()),
                ],
              ),
            ),
          ),
        ),
        onBackButtonPressed: () => pop());
  }

  //內容
  Widget _body() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        //title
        Container(
          margin: EdgeInsets.symmetric(vertical: 1.5.h),
          child: Text(
            S.of(context).setting,
            style: TextStyle(
                color: Colors.black,
                fontWeight: FontWeight.bold,
                fontSize: 20.sp),
          ),
        ),
        //篩選條件
        Container(
          padding: EdgeInsets.symmetric(horizontal: 3.w, vertical: 2.h),
          width: Device.width,
          color: Colors.grey.shade200,
          child: Text(S.of(context).screening_condition,
              style: TextStyle(
                  color: Colors.grey,
                  fontWeight: FontWeight.bold,
                  fontSize: 17.sp)),
        ),
        //性別
        _select(),
        //確定按鈕
        Expanded(child: Center(child: _confirmButton())),
      ],
    );
  }

  //性別
  Widget _select() {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 3.w),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          //性別
          Container(
            margin: EdgeInsets.symmetric(vertical: 1.7.h),
            child: Text(
              S.of(context).gender,
              style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.bold,
                  fontSize: 17.sp),
            ),
          ),
          //性別按鈕
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              _genderButtonWidget(GenderStatus.girl),
              _genderButtonWidget(GenderStatus.boy),
              _genderButtonWidget(GenderStatus.none),
            ],
          ),
          //灰線
          Container(
            margin: EdgeInsets.only(top: 4.h),
            height: 0.3.h,
            width: Device.width,
            color: AppColor.grayLine,
          ),
          //年齡文字
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              //年齡文字
              Container(
                margin: EdgeInsets.symmetric(vertical: 1.7.h),
                child: Text(
                  S.of(context).age,
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 17.sp),
                ),
              ),
              //範圍
              Container(
                margin: EdgeInsets.symmetric(vertical: 1.7.h),
                child: Text(
                  "${_ageRange.start.toInt()} - ${_ageRange.end.toInt()}",
                  style: TextStyle(
                      color: AppColor.yellowIcon,
                      fontWeight: FontWeight.bold,
                      fontSize: 17.sp),
                ),
              ),
            ],
          ),
          //年齡範圍
          _ageRangeSlider(),
          //灰線
          Container(
            margin: EdgeInsets.only(top: 4.h),
            height: 0.3.h,
            width: Device.width,
            color: AppColor.grayLine,
          ),
          //距離文字
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              //距離文字
              Container(
                margin: EdgeInsets.symmetric(vertical: 1.7.h),
                child: Text(
                  S.of(context).distance,
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.bold,
                      fontSize: 17.sp),
                ),
              ),
              //距離
              Container(
                margin: EdgeInsets.symmetric(vertical: 1.7.h),
                child: Text(
                  "${_distance.toInt()}km",
                  style: TextStyle(
                      color: AppColor.yellowIcon,
                      fontWeight: FontWeight.bold,
                      fontSize: 17.sp),
                ),
              ),
            ],
          ),
          //距離範圍
          _distanceRangeSlider(),
          Container(
            margin: EdgeInsets.only(top: 4.h),
            height: 0.3.h,
            width: Device.width,
            color: AppColor.grayLine,
          ),
        ],
      ),
    );
  }

  //性別切換按鈕
  Widget _genderButtonWidget(GenderStatus dataSet) {
    return GestureDetector(
      onTap: () {
        if (_index != dataSet.index) {
          _index = dataSet.index;
          setState(() {});
          _gameStreetProvider!.setMemberGameSettingGender(gender: _upDateGenderMap[_index]!);
        }
      },
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 7.w, vertical: 1.h),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(30),
          color: _index == dataSet.index ? _colorList[_index] : Colors.white,
          border: Border.all(
              color: _index == dataSet.index
                  ? _colorList[_index]
                  : AppColor.buttonFrameDisposeColor,
              width: 1),
        ),
        child: Text(
          _genderTextReturn(dataSet),
          style: TextStyle(
              color: _index == dataSet.index
                  ? Colors.white
                  : AppColor.buttonFrameDisposeColor,
              fontSize: 18.sp),
        ),
      ),
    );
  }

  //文字返回
  String _genderTextReturn(GenderStatus data) {
    switch (data) {
      case GenderStatus.boy:
        return S.of(context).male;
      case GenderStatus.girl:
        return S.of(context).female;
      case GenderStatus.none:
        return S.of(context).none_gender;
    }
  }

  //年齡
  Widget _ageRangeSlider() {
    return RangeSlider(
        values: _ageRange,
        min: 18,
        max: 100,
        activeColor: AppColor.pinkRedColor,
        inactiveColor: AppColor.buttonFrameDisposeColor,
        onChanged: (RangeValues newRange) {
          setState(() {
            if(newRange.start >=18 && newRange.end >=18){
              _ageRange = newRange;
              _gameStreetProvider!.setMemberGameSettingRange(range: newRange);
            }
          });
        });
  }

  //距離
  Widget _distanceRangeSlider() {
    return Slider(
      value: _distance,
      onChanged: (value){
        setState(() {
          _distance = value;
        });
        _gameStreetProvider!.setMemberGameSettingDistance(distance: _distance.ceil());
      },
      min: 1,
      max: 6,
      activeColor: AppColor.pinkRedColor,
      inactiveColor: AppColor.buttonFrameDisposeColor,
    );
  }

  //確定按鈕
  Widget _confirmButton(){
    return GestureDetector(
      onTap: () => Navigator.of(context).pop(true),
      child: Container(
        margin: EdgeInsets.only(bottom: 2.h),
        width: 58.w,
        height: 7.h,
        child: buttonWidget(S.of(context).sure),
      ),
    );
  }


  //X
  Widget _closeButton() {
    return GestureDetector(
      onTap: () => _closeOnTap(),
      child: const Icon(
        Icons.cancel,
        size: 35,
        color: Colors.grey,
      ),
    );
  }

  //X onTap
  void _closeOnTap() {
    Navigator.of(context).pop(false);
  }

  Future<bool> pop() async {
    Navigator.of(context).pop(false);
    return true;
  }
}
