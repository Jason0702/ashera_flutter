import 'package:ashera_flutter/provider/member_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../generated/l10n.dart';
import '../utils/app_color.dart';
import '../widget/titlebar_widget.dart';

class MyQRCodeDialog extends StatefulWidget{
  const MyQRCodeDialog({Key? key}):super(key: key);

  @override
  State createState() => _MyQRCodeDialogState();
}

class _MyQRCodeDialogState extends State<MyQRCodeDialog> {

  _onLayoutDone(_) async {
    debugPrint('我的QRCORD頁');
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BackButtonListener(
        child: Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Column(
          children: [
            //標題 與 X
            titleBarIntegrate([],S.of(context).my_QR_code, [closeWidget()]),
            //內容
            Expanded(child: _body()),
          ],
        ),
      ),
    ),
        onBackButtonPressed: () => pop());
  }
  //關閉
  void _closeOnTap(){
    Navigator.of(context).pop();
  }

  Widget closeWidget(){
    return GestureDetector(
      onTap: () => _closeOnTap(),
      child: const Icon(
        Icons.clear,
        color: AppColor.appTitleBarTextColor,
        size: 30,
      ),
    );
  }

  Widget _body(){
    return SizedBox(
      //height: 120.h,
      width: Device.boxConstraints.maxWidth,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          SizedBox(height: 5.h,),
          Container(
            color: Colors.white,
            width: 85.w,
            height: 45.h,
            alignment: Alignment.center,
            child: Consumer<MemberProvider>(builder: (context, member, _){
              return QrImage(
                data: member.memberModel.asheraUid!,
                version: QrVersions.auto,
                size: 300.0,
                gapless: false,
              );
            },),
          ),
          Container(
            margin: EdgeInsets.only(left: 3.w, right: 3.w, top: 2.h),
            child: FittedBox(
              child: Consumer<MemberProvider>(builder: (context, member, _){
                return Text('ID: ${member.memberModel.asheraUid!}', style: TextStyle(color: Colors.black, fontSize: 20.sp));
              },),
            ),
          ),
          //請掃描QRCode,加好友
          Container(
            margin: EdgeInsets.only(left: 3.w, right: 3.w, top: 1.h),
            child: FittedBox(
              child: Text(S.of(context).scan_qr_code_add_me, style: TextStyle(color: Colors.black, fontSize: 20.sp),),
            ),
          )
        ],
      ),
    );
  }

  Future<bool> pop() async {
    Navigator.of(context).pop();
    return true;
  }
}