import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../utils/app_color.dart';
import '../widget/button_widget.dart';

class RechargeDoneDialog extends StatefulWidget{
  final String title;
  final String image;
  final Widget content;
  final String confirmButtonText;

  const RechargeDoneDialog({
    Key? key,
    required this.title,
    required this.image,
    required this.content,
    required this.confirmButtonText
  }): super(key: key);

  @override
  State createState() => _RechargeDoneDialogState();
}

class _RechargeDoneDialogState extends State<RechargeDoneDialog>{

  @override
  void initState() {
    super.initState();
    //log('充值對話框初始化');
    Future.delayed(const Duration(seconds: 3), (){
      if(mounted){
        Navigator.of(context).pop();
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    //log('探班取消同意或不同意對話框解除');
  }

  @override
  Widget build(BuildContext context) {
    return BackButtonListener(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.black54,
          body: Center(
            child: Container(
              width: Device.width - 60,
              height: 50.h,
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(20)),
              child: Stack(
                children: [
                  //內容
                  Column(
                    children: [
                      //title
                      Container(
                        margin: EdgeInsets.symmetric(vertical: 1.5.h),
                        child: Text(
                          widget.title,
                          style: TextStyle(
                              fontSize: 20.sp,
                              color: Colors.black,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      //灰線
                      Container(
                        height: 0.3.h,
                        width: Device.width,
                        color: AppColor.grayLine,
                      ),
                      //內容
                      Expanded(
                        child: Container(
                          alignment: Alignment.center,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              Image(
                                width: 25.w,
                                image: AssetImage(widget.image),
                              ),
                              SizedBox(height: 2.h,),
                              widget.content
                            ],
                          ),
                        ),
                      ),
                      //同意按鈕
                      GestureDetector(
                        onTap: () => Navigator.of(context).pop(true),
                        child: Container(
                          margin: EdgeInsets.only(bottom: 2.h),
                          width: 58.w,
                          height: 7.h,
                          child: buttonWidget(widget.confirmButtonText),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
        onBackButtonPressed: () => pop());
  }

  Future<bool> pop() async {
    Navigator.of(context).pop();
    return true;
  }
}