import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../app.dart';
import '../utils/app_color.dart';
import '../widget/button_widget.dart';

class VisitAskCancelDialog extends StatefulWidget{
  final String title;
  final String content;
  final String confirmButtonText;
  final bool haveClose;

  const VisitAskCancelDialog({
    Key? key,
    required this.title,
    required this.content,
    required this.confirmButtonText,
    required this.haveClose
  }) : super(key: key);

  @override
  State createState() => _VisitAskCancelDialogState();
}

class _VisitAskCancelDialogState extends State<VisitAskCancelDialog>{

  @override
  Widget build(BuildContext context) {
    return BackButtonListener(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.black54,
          body: Center(
            child: Container(
              width: Device.width - 60,
              height: 50.h,
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(20)),
              child: Stack(
                children: [
                  //內容
                  Column(
                    children: [
                      //title
                      Container(
                        margin: EdgeInsets.symmetric(vertical: 1.5.h),
                        child: Text(
                          widget.title,
                          style: TextStyle(
                              fontSize: 20.sp,
                              color: Colors.black,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      //灰線
                      Container(
                        height: 0.3.h,
                        width: Device.width,
                        color: AppColor.grayLine,
                      ),
                      //內容
                      Expanded(
                        child: Container(
                          alignment: Alignment.center,
                          child: Text(
                            widget.content,
                            style: TextStyle(
                                fontSize: 20.sp,
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                      //同意按鈕
                      GestureDetector(
                        onTap: () => Navigator.of(context).pop(true),
                        child: Container(
                          margin: EdgeInsets.only(bottom: 2.h),
                          width: 58.w,
                          height: 7.h,
                          child: buttonWidget(widget.confirmButtonText),
                        ),
                      ),
                    ],
                  ),
                  if(widget.haveClose)
                  //右上X
                  Positioned(
                      right: 10,
                      top: 10,
                      child: GestureDetector(
                        onTap: () => Navigator.of(context).pop(false),
                        child: Icon(
                          Icons.cancel,
                          size: 10.w,
                        ),
                      )),
                ],
              ),
            ),
          ),
        ),
        onBackButtonPressed: () => pop());
  }

  Future<bool> pop() async {
    Navigator.of(context).pop(false);
    return true;
  }

  @override
  void initState() {
    super.initState();
    //log('探班取消同意或不同意對話框初始化');
    util.dialogIsOpen = true;
  }

  @override
  void dispose() {
    super.dispose();
    //log('探班取消同意或不同意對話框解除');
    util.dialogIsOpen = false;
  }
}