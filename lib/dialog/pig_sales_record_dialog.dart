import 'package:ashera_flutter/provider/game_home_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../generated/l10n.dart';
import '../models/game_model.dart';
import '../utils/app_image.dart';
import '../utils/util.dart';

class PigSalesRecordDialog extends StatefulWidget{

  const PigSalesRecordDialog({Key? key}): super(key: key);

  @override
  State createState() => _PigSalesRecordDialogState();
}

class _PigSalesRecordDialogState extends State<PigSalesRecordDialog>{

  @override
  Widget build(BuildContext context) {
    return BackButtonListener(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.black12,
          body: Center(
            child: Container(
              width: Device.width - 40,
              height: Device.height - 200,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(20)
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  //title
                  /*Flexible(child: )*/_title(),
                  //序號 成長時間 扣除點數 販售點數
                  _bar(),
                  //紀錄列表
                  Expanded(child: _body())
                ],
              ),
            ),
          ),
        ),
        onBackButtonPressed: () => pop());
  }

  //title
  Widget _title(){
    return Container(
      width: Device.width,
      height: 7.h,
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(color: Colors.grey[200]!, width: 1.0)
        )
      ),
      child: Stack(
        alignment: Alignment.center,
        children: [
          FittedBox(
            child: Text(S.of(context).sales_record, style: TextStyle(fontSize: 20.sp, fontWeight: FontWeight.bold),),
          ),
          Positioned(
            right: 3.w,
            child: _closeButton())
        ],
      ),
    );
  }

  //X
  Widget _closeButton() {
    return GestureDetector(
      onTap: () => _closeOnTap(),
      child: const Icon(
        Icons.cancel,
        size: 35,
        color: Colors.grey,
      ),
    );
  }

  //X onTap
  void _closeOnTap() {
    Navigator.of(context).pop(false);
  }

  //序號 成長時間 扣除點數 販售點數
  Widget _bar(){
    return Container(
      height: 8.h,
      decoration: const BoxDecoration(
        border: Border(
          bottom: BorderSide(color: Colors.black, width: 1.0)
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          //序號
          Expanded(child: Container(
            alignment: Alignment.center,
            child: Text(S.of(context).serial_number, style: const TextStyle(fontSize: 18,fontWeight: FontWeight.bold),),
          )),
          //成長時間
          Expanded(flex: 2, child: Container(
            alignment: Alignment.center,
            child: Text(S.of(context).growth_time, style: const TextStyle(fontSize: 18,fontWeight: FontWeight.bold)),
          ),),
          //扣除點數
          Expanded(child: Container(
            alignment: Alignment.center,
            child: FittedBox(child: Text(S.of(context).deduct_points, style: const TextStyle(fontSize: 18,fontWeight: FontWeight.bold)),),
          )),
          //販售點數
          Expanded(child: Container(
            alignment: Alignment.center,
            child: FittedBox(child: Text(S.of(context).point_of_sale, style: const TextStyle(fontSize: 18,fontWeight: FontWeight.bold)),),
          )),
        ],
      ),
    );
  }

  //紀錄列表
  Widget _body(){
    return Consumer<GameHomeProvider>(builder: (context, game, _){
      return ListView.builder(
          padding: EdgeInsets.zero,
          itemBuilder: (context, index) => _pigSalesRecord(index: _getIndex(game.petsSellRecord.length, index), model: game.petsSellRecord[index]),
          itemCount: game.petsSellRecord.length,
      );
    },);
  }

  int _getIndex(int length, int index){
    return length - index;
  }

  Widget _pigSalesRecord({required int index, required PetsSellRecordModel model}){
    return Container(
      height: 8.h,
      decoration: BoxDecoration(
        border: Border(
          bottom: BorderSide(
            color: Colors.grey[400]!,
            width: 1.0
          )
        ),
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          //序號
          Expanded(child: Container(
            alignment: Alignment.center,
            child: Text('$index', style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),)),
          //成長時間
          Expanded(flex: 2, child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              FittedBox(child: Text('${Util().getDateDateFormatHHmm(model.startAt!)}~', style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),),
              FittedBox(child: Text(Util().getDateDateFormatHHmm(model.endAt!), style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),)
            ],
          )),
          //扣除點數
          Expanded(child: Container(
            alignment: Alignment.center,
            child: Text('${model.pointsDeducted}', style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
          )),
          //販售點數
          Expanded(child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image(
                width: 20,
                image: AssetImage(AppImage.iconDiamond),
              ),
              SizedBox(width: 1.w,),
              FittedBox(
                child: Text('${model.getPoints}', style: const TextStyle(fontSize: 16, fontWeight: FontWeight.bold),),
              )
            ],
          ))
        ],
      ),
    );
  }

  Future<bool> pop() async {
    Navigator.of(context).pop(false);
    return true;
  }

  _onLayoutDone(_){

  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  void dispose() {
    super.dispose();
  }
}