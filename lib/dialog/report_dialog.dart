import 'dart:developer';
import 'dart:io';

import 'package:ashera_flutter/enum/app_enum.dart';
import 'package:ashera_flutter/models/complaint_record_dto.dart';
import 'package:ashera_flutter/provider/member_provider.dart';
import 'package:ashera_flutter/provider/util_api_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:image_pickers/image_pickers.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../app.dart';
import '../generated/l10n.dart';
import '../utils/app_color.dart';
import '../utils/app_size.dart';
import '../widget/button_widget.dart';

class ReportDialog extends StatefulWidget{
  const ReportDialog({Key? key}): super(key: key);

  @override
  State createState() => _ReportDialogState();
}

class _ReportDialogState extends State<ReportDialog>{
  String _localPic = '';
  int _onTapIndex = 0;
  UtilApiProvider? _utilApiProvider;
  MemberProvider? _memberProvider;
  int? targetMemberId;

  final Map<int, String> _reportMap = {
    0: '',
    1: '發佈不合適內容對我造成騷擾',
    2: '存在詐騙行為'
  };

  @override
  Widget build(BuildContext context) {
    _memberProvider = Provider.of<MemberProvider>(context, listen: false);
    _utilApiProvider = Provider.of<UtilApiProvider>(context, listen: false);
    targetMemberId ??= ModalRoute.of(context)!.settings.arguments as int;
    return BackButtonListener(
        child: SafeArea(
          child: Scaffold(
            resizeToAvoidBottomInset: false,
            backgroundColor: Colors.white,
            body: SizedBox(
              width: Device.width,
              height: Device.height,
              child: Column(
                children: [
                  //titleBar
                  _titleBar(S.of(context).report),
                  //body
                  Expanded(child: _body())
                ],
              ),
            ),
          ),
        ),
        onBackButtonPressed: () => pop());
  }

  Widget _titleBar(String _title) {
    return Container(
      height: AppSize.titleBarH,
      width: Device.boxConstraints.maxWidth,
      decoration: const BoxDecoration(
        color: Colors.white,
        boxShadow: [
          BoxShadow(
              color: Colors.grey,
              offset: Offset(0.0, 0.5), //陰影y軸偏移量
              blurRadius: 1, //陰影模糊程度
              spreadRadius: 1 //陰影擴散程度
          )
        ],
      ),
      child: Stack(
        alignment: Alignment.center,
        children: [
          //標題文字
          FittedBox(
            child: Text(
              _title,
              style: TextStyle(
                  color: AppColor.appTitleBarTextColor,
                  fontSize: 21.sp,
                  fontWeight: FontWeight.bold),
            ),
          ),
          Positioned(
              left: 5.w,
              child: GestureDetector(
                onTap: () => Navigator.of(context).pop(),
                child: const Icon(
                  Icons.arrow_back,
                  color: AppColor.appTitleBarTextColor,
                  size: 30,
                ),
              ))
        ],
      ),
    );
  }


  //body
  Widget _body(){
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        //選項
        Container(
            margin: const EdgeInsets.only(top: 10, bottom: 5, left: 30, right: 20),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                const FittedBox(
                  child: Text('請選擇檢舉問題',style: TextStyle(color: Colors.black, fontSize: 18, fontWeight: FontWeight.bold),),
                ),
                const SizedBox(height: 10,),
                _chooseReportContent(content: '發佈不合適內容對我造成騷擾', index: 1),
                _chooseReportContent(content: '存在詐騙行為',index: 2),
                const FittedBox(
                  fit: BoxFit.scaleDown,
                  child: Text('我們將在 24 小時內審核此舉報，如果認為該內容不合適，\n將在該期限內刪除該內容並對其作者採取行動。', maxLines: 2, style: TextStyle(color: Colors.red, fontSize: 18),),
                )
              ],
            ),
          ),
        Container(
          height: 1,
          width: Device.width,
          color: Colors.grey[400]!,
        ),
        //上傳截圖
        Container(
          width: Device.width,
          margin: const EdgeInsets.only(top: 10, bottom: 10, left: 30),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              const FittedBox(
                child: Text('請上傳截圖',style: TextStyle(color: Colors.black, fontSize: 18, fontWeight: FontWeight.bold),),
              ),
              const SizedBox(height: 10,),
              Container(
                width: Device.width / 2,
                height: Device.height / 2.3,
                margin: const EdgeInsets.only(right: 20),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(color: Colors.grey[400]!, width: 1.0),
                ),
                alignment: Alignment.center,
                child: GestureDetector(
                  onTap: () => _selectImage(),
                  child: _localPic.isEmpty ? const Icon(
                    Icons.add_photo_alternate,
                    size: 50,
                  ) : Image(
                    image: FileImage(File(_localPic)),
                  ),
                ),
              )
            ],
          ),
        ),
        //上傳
       GestureDetector(
         onTap: () {
           _upLoading();
         },
         child: Container(
           margin: EdgeInsets.only(bottom: 2.h),
           width: 58.w,
           height: 7.h,
           child: buttonWidget(S.of(context).send),
         ),
       )
      ],
    );
  }

  void _upLoading() async {
    if(_reportMap[_onTapIndex]!.isEmpty){
      return;
    }
    if(!EasyLoading.isShow){
      EasyLoading.show(status: '上傳中...');
      Map<String, dynamic> _file = await _utilApiProvider!.upDataImage(
          '${_memberProvider!.memberModel.name!}/${DateTime.now().month}/${DateTime.now().day}', _localPic, PhotoType.complaint);
      ComplaintRecordDTO _dto = ComplaintRecordDTO(
          fromMemberId: _memberProvider!.memberModel.id!,
          targetMemberId: targetMemberId!,
          pic: _file['filename'],
          reason: _reportMap[_onTapIndex]!);
      bool _result = await _utilApiProvider!.postComplaintRecord(dto: _dto);
      if(_result){
        EasyLoading.showToast(S.of(context).finish);
        delegate.popRoute();
      }else{
        EasyLoading.showToast(S.of(context).fail);
      }
    }
  }

  //打開相簿
  void _selectImage() async {
    ImagePickers.pickerPaths(
        galleryMode: GalleryMode.image,
        showGif: false,
        selectCount: 1,
        showCamera: false,
        cropConfig: CropConfig(enableCrop: false, height: 1, width: 1),
        compressSize: 500,
        uiConfig: UIConfig(uiThemeColor: AppColor.buttonFrameColor))
        .then((media) {
        _localPic = media.first.path.toString();
        setState(() {});
    });
  }

  Widget _chooseReportContent({required String content, required int index}){
    return SizedBox(
      height: 40,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            //核取方塊
            Transform.scale(
              scale: 1.3,
              child: Checkbox(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5)
                  ),
                  fillColor: MaterialStateProperty.resolveWith(getColor),
                  value: index == _onTapIndex,
                  onChanged: (value){
                    _onTapIndex = index;
                    setState(() {});
                  }),
            ),
            FittedBox(
              child: Text(content, style: const TextStyle(color: Colors.black, fontSize: 18, height: 1.1),),
            )
          ],
        )
    );
  }


  Future<bool> pop() async {
    delegate.popRoute();
    return true;
  }

  ///核取方塊顏色
  Color getColor(Set<MaterialState> states){
    return AppColor.buttonFrameColor;
  }

}