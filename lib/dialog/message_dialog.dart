import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../utils/app_color.dart';
import '../widget/button_widget.dart';

class MessageDialog extends StatefulWidget {
  const MessageDialog({
    Key? key,
    required this.title,
    required this.content,
    required this.buttonString,
    required this.buttonOnTap,
    this.haveClose = false,
  }): super(key: key);

  final String title;
  final String content;
  final bool haveClose;
  final String buttonString;
  final VoidCallback buttonOnTap;

  @override
  _MessageDialogState createState() => _MessageDialogState();
}

class _MessageDialogState extends State<MessageDialog> {
  final int tapDuration = 1; //防連點時間間隔
  DateTime? lastTapCloseTime; //上次點擊關閉時間
  DateTime? lastTapButtonTime; //上次點擊按鈕時間
  @override
  Widget build(BuildContext context) {
    return BackButtonListener(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.black54,
          body: Center(
            child: Container(
              width: Device.width - 60,
              height: 60.h,
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(20)),
              child: Stack(
                children: [
                  //內容
                  Column(
                    children: [
                      //title
                      Container(
                        margin: EdgeInsets.symmetric(vertical: 1.5.h),
                        child: Text(
                          widget.title,
                          style: TextStyle(
                              fontSize: 23.sp,
                              color: Colors.black,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      //灰線
                      Container(
                        height: 0.3.h,
                        width: Device.width,
                        color: AppColor.grayLine,
                      ),
                      //內容
                      Expanded(
                        child: Container(
                          alignment: Alignment.center,
                          child: Text(
                            widget.content,
                            style: TextStyle(
                                fontSize: 23.sp,
                                color: Colors.black,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                      ),
                      //按鈕
                      GestureDetector(
                        onTap: () {
                          if (lastTapCloseTime == null ||
                              DateTime.now().difference(lastTapCloseTime!) >
                                  Duration(seconds: tapDuration)) {
                            lastTapCloseTime = DateTime.now();
                            widget.buttonOnTap();
                          }
                        },
                        child: Container(
                          margin: EdgeInsets.only(bottom: 5.h),
                          width: 58.w,
                          height: 7.h,
                          child: buttonWidget(widget.buttonString),
                        ),
                      )
                    ],
                  ),
                  //X按鈕
                  if (widget.haveClose)
                    Positioned(
                        right: 10,
                        top: 10,
                        child: GestureDetector(
                          onTap: () {
                            if (lastTapCloseTime == null ||
                                DateTime.now().difference(lastTapCloseTime!) >
                                    Duration(seconds: tapDuration)) {
                              lastTapCloseTime = DateTime.now();
                              Navigator.pop(context);
                            }
                          },
                          child: Icon(
                            Icons.cancel,
                            size: 10.w,
                          ),
                        ))
                ],
              ),
            ),
          ),
        ),
        onBackButtonPressed: () => pop());
  }

  Future<bool> pop() async {
    Navigator.of(context).pop();
    return true;
  }
}

///使用方法
/*
showGeneralDialog(
context: context,
pageBuilder: (context, anim1, anim2) {
return Container();
},
barrierColor: Colors.black54,
barrierDismissible: false,
transitionDuration: const Duration(milliseconds: 100),
transitionBuilder: (context, anim1, anim2, child) {
return Transform.scale(
scale: anim1.value,
child: MessageDialog(
title: S.of(context).message,
buttonString: S.of(context).confirm,
buttonOnTap: () {
},
haveClose: true,
content: "${S.of(context).already}${S.of(context).cancel}${S.of(context).visit}",
),
);
});*/
