import 'package:ashera_flutter/provider/util_api_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../generated/l10n.dart';
import '../utils/app_color.dart';
import '../widget/button_widget.dart';

class PayRecoverTimersDialog extends StatefulWidget{
  final String title;
  final int recoverPoints;
  final int limit;
  final String confirmButtonText;
  final String cancelButtonText;

  const PayRecoverTimersDialog({
    Key? key,
    required this.title,
    required this.recoverPoints,
    required this.limit,
    required this.confirmButtonText,
    required this.cancelButtonText
  }): super(key: key);

  @override
  State<StatefulWidget> createState() => _PayRecoverTimersDialogState();
}

class _PayRecoverTimersDialogState extends State<PayRecoverTimersDialog> {

  @override
  Widget build(BuildContext context) {
    return BackButtonListener(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.black54,
          body: Center(
            child: Container(
              width: Device.width - 60,
              height: 40.h,
              decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(20)
              ),
              child: Column(
                children: [
                  //title
                  SizedBox(
                    width: MediaQuery.of(context).size.width,
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        Container(
                          margin: EdgeInsets.symmetric(vertical: 1.5.h),
                          child: Text(
                            widget.title,
                            style: TextStyle(
                              fontSize: 20.sp,
                              color: Colors.black,
                              fontWeight: FontWeight.bold
                            ),
                          ),
                        ),
                        Positioned(
                            right: 15,
                            child: GestureDetector(
                              onTap: () => Navigator.of(context).pop(false),
                              child: Container(
                                width: 30,
                                alignment: Alignment.center,
                                decoration: const BoxDecoration(
                                    color: Colors.grey, shape: BoxShape.circle
                                ),
                                child: const Icon(
                                  Icons.close,
                                  size: 30,
                                  color: Colors.white,
                                ),
                              ),
                            )
                        ),
                      ],
                    ),
                  ),

                  //灰線
                  Container(
                    height: 0.3.h,
                    width: Device.width,
                    color: AppColor.grayLine,
                  ),

                  //內容
                  Expanded(
                    child: Stack(
                      alignment: Alignment.center,
                      children: [
                        FittedBox(
                          fit: BoxFit.scaleDown,
                          child: RichText(
                            text: TextSpan(
                                text: S.of(context).does_it_cost,
                                style: const TextStyle(color: Colors.black, fontSize: 18, height: 1.1),
                                children: [
                                  TextSpan(
                                      text: '${widget.recoverPoints}',
                                      style: const TextStyle(color: Colors.red, fontSize: 18, fontWeight: FontWeight.bold, height: 1.1)
                                  ),
                                  TextSpan(
                                    text: S.of(context).buy,
                                    style: const TextStyle(color: Colors.black, fontSize: 18, height: 1.1),
                                  ),
                                  TextSpan(
                                    text: '${widget.limit}',
                                    style: const TextStyle(color: Colors.red, fontSize: 18, fontWeight: FontWeight.bold, height: 1.1)
                                  ),
                                  TextSpan(
                                    text: S.of(context).how_many_times,
                                    style: const TextStyle(color: Colors.black, fontSize: 18, height: 1.1),
                                  )
                                ]
                            ),
                          ),
                        )
                      ],
                    ),
                  ),

                  const SizedBox(
                    height: 15,
                  ),
                  //同意按鈕
                  GestureDetector(
                    onTap: () => Navigator.of(context).pop(true),
                    child: Container(
                      margin: EdgeInsets.only(bottom: 2.h),
                      width: 58.w,
                      height: 7.h,
                      child: buttonWidget(widget.confirmButtonText),
                    ),
                  ),
                  //不同意按鈕
                  //按鈕
                  GestureDetector(
                    onTap: () => Navigator.of(context).pop(false),
                    child: Container(
                      margin: EdgeInsets.only(bottom: 5.h),
                      width: 58.w,
                      height: 7.h,
                      child: buttonWidgetDispose(widget.cancelButtonText),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
        onBackButtonPressed: () => pop());
  }

  Future<bool> pop() async {
    Navigator.of(context).pop(false);
    return true;
  }

}