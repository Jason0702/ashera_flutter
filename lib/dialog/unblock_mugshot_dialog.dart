import 'package:ashera_flutter/generated/l10n.dart';
import 'package:ashera_flutter/provider/util_api_provider.dart';
import 'package:ashera_flutter/utils/app_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../utils/app_color.dart';
import '../utils/util.dart';
import '../widget/button_widget.dart';
import '../widget/gender_icon.dart';

class UnblockMugshotDialog extends StatefulWidget {
  final String nickName;
  final int gender;
  final String birthday;

  const UnblockMugshotDialog({Key? key, required this.nickName, required this.gender, required this.birthday}) : super(key: key);

  @override
  State createState() => _UnblockMugshotDialogState();
}

class _UnblockMugshotDialogState extends State<UnblockMugshotDialog> {
  final int tapDuration = 3; //防連點時間間隔
  DateTime? lastTapCloseTime; //上次點擊關閉時間
  DateTime? lastTapButtonTime; //上次點擊按鈕時間
  @override
  Widget build(BuildContext context) {
    return BackButtonListener(
        child: Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.black54,
      body: Center(
        child: Container(
          width: Device.width - 60,
          height: 50.h,
          decoration: BoxDecoration(
              color: Colors.white, borderRadius: BorderRadius.circular(20)),
          child: Stack(
            children: [
              //內容
              Column(
                children: [
                  //title
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 1.5.h),
                    child: Text(
                      S.of(context).unblock,
                      style: TextStyle(
                          fontSize: 23.sp,
                          color: Colors.black,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  //灰線
                  Container(
                    height: 0.3.h,
                    width: Device.width,
                    color: AppColor.grayLine,
                  ),
                  //內容
                  Expanded(
                    child: Column(
                      children: [
                        Container(
                          padding: const EdgeInsets.only(left: 30,top: 30),
                          child: Row(
                            children: [
                              //頭像
                              /*Container(
                                width: 20.w,
                                height: 20.w,
                                decoration: BoxDecoration(
                                    shape: BoxShape.circle, gradient: AppColor.appMainColor),
                                child: CircleAvatar(
                                    backgroundColor: Colors.transparent,
                                    child: Image.asset(AppImage.iconCat)
                                ),
                              )*/_getAvatar(widget.gender),
                              const SizedBox(width: 30,),
                              //名稱&性別年齡&距離
                              Expanded(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    //名稱
                                    FittedBox(
                                      child: Text(
                                        widget.nickName,
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 21.sp,
                                            color: Colors.black),
                                      ),
                                    ),
                                    SizedBox(
                                      height: 3.h,
                                    ),
                                    //性別年齡
                                    Row(
                                      children: [
                                        getGender(widget.gender),
                                        //年齡
                                        Container(
                                          margin: const EdgeInsets.only(left: 5),
                                          child: Text(
                                            Util().getAge(widget.birthday),
                                            style: TextStyle(color: Colors.black,fontSize: 17.sp),
                                          ),
                                        )
                                      ],
                                    )
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                        Expanded(
                          child: Center(
                            child: Text(
                              S.of(context).unblock_member_mugshot,
                              style: TextStyle(
                                  fontSize: 20.sp,
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Center(
                            child: Consumer<UtilApiProvider>(builder: (context, util, _){
                              return Text(
                                S.of(context).unblockNeedPoint(util.systemSetting!.unlockMugshotPoints!),
                                style: TextStyle(
                                    fontSize: 17.sp,
                                    color: Colors.grey,
                                    fontWeight: FontWeight.bold),
                              );
                            },),
                          ),
                        ),
                      ],
                    ),
                  ),
                  //按鈕
                  GestureDetector(
                    onTap: () {
                      if (lastTapCloseTime == null ||
                          DateTime.now().difference(lastTapCloseTime!) >
                              Duration(seconds: tapDuration)) {
                        lastTapCloseTime = DateTime.now();
                        unblockMugshotOnTap();
                      }
                    },
                    child: Container(
                      margin: EdgeInsets.only(bottom: 5.h),
                      width: 58.w,
                      height: 7.h,
                      child: buttonWidget(S.of(context).confirm),
                    ),
                  )
                ],
              ),
              //X按鈕
              Positioned(
                  right: 10,
                  top: 10,
                  child: GestureDetector(
                    onTap: () {
                      if (lastTapCloseTime == null ||
                          DateTime.now().difference(lastTapCloseTime!) >
                              Duration(seconds: tapDuration)) {
                        lastTapCloseTime = DateTime.now();
                        pop();
                      }
                    },
                    child: Icon(
                      Icons.cancel,
                      size: 10.w,
                    ),
                  ))
            ],
          ),
        ),
      ),
    ),
        onBackButtonPressed: () => pop());
  }

  //取得角色大頭貼
  Widget _getAvatar(int _gender) {
    switch (_gender) {
    //男
      case 0:
        return Container(
          width: 20.w,
          height: 20.w,
          decoration: BoxDecoration(
              shape: BoxShape.circle, gradient: AppColor.appMainColor),
          child: CircleAvatar(
            backgroundColor: Colors.transparent,
            child: Image.asset(AppImage.iconDog),
          ),
        );
    //女
      case 1:
        return Container(
          width: 20.w,
          height: 20.w,
          decoration: BoxDecoration(
              shape: BoxShape.circle, gradient: AppColor.appMainColor),
          child: CircleAvatar(
            backgroundColor: Colors.transparent,
            child: Image.asset(AppImage.iconCat),
          ),
        );
      default:
        return Container(
          width: 20.w,
          height: 20.w,
          decoration: BoxDecoration(
              shape: BoxShape.circle, gradient: AppColor.appMainColor),
          child: CircleAvatar(
            backgroundColor: Colors.transparent,
            child: Image.asset(AppImage.iconCat),
          ),
        );
    }
  }

  Future<bool> unblockMugshotOnTap() async{
    Navigator.pop(context,true);
    return true;
  }


  Future<bool> pop() async{
    Navigator.pop(context,false);
    return true;
  }
}
