
import 'package:ashera_flutter/provider/game_home_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../app.dart';
import '../generated/l10n.dart';
import '../models/member_model.dart';
import '../provider/member_provider.dart';
import '../routers/route_name.dart';
import '../utils/app_color.dart';
import '../utils/app_image.dart';
import '../utils/util.dart';
import '../widget/gender_icon.dart';
import '../widget/mugshot_widget.dart';

class MessageCardDialog extends StatefulWidget{
  final String message;
  final int gender;
  final String nickname;
  final String birthday;
  final int id;
  final int check;

  const MessageCardDialog({
    Key? key,
    required this.message,
    required this.gender,
    required this.nickname,
    required this.birthday,
    required this.id,
    required this.check
  }): super(key: key);

  @override
  State createState() => _MessageCardDialogState();
}

class _MessageCardDialogState extends State<MessageCardDialog>{

  //Member
  MemberProvider? _memberProvider;
  GameHomeProvider? _gameHomeProvider;


  @override
  Widget build(BuildContext context) {
    _memberProvider = Provider.of<MemberProvider>(context);
    _gameHomeProvider = Provider.of<GameHomeProvider>(context);
    return BackButtonListener(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.black54,
          body: Center(
            child: Container(
              width: Device.width - 20.w,
              height: 60.h,
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(20)),
              child: Stack(
                children: [
                  //內容
                  Column(
                    children: [
                      //title
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Expanded(child: Container()),
                          Container(
                            margin: EdgeInsets.symmetric(vertical: 1.5.h),
                            child: FittedBox(
                              fit: BoxFit.scaleDown,
                              child: Text(
                                S.of(context).leave_message,
                                style: TextStyle(
                                    fontSize: 23.sp,
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                              ),
                            ),
                          ),
                          Expanded(child: Container(
                            padding: const EdgeInsets.symmetric(horizontal: 15),
                            alignment: Alignment.centerRight,
                            child: GestureDetector(
                              onTap: () {
                                Navigator.of(context).pop(false);
                              },
                              child: const Icon(
                                Icons.clear,
                                size: 30,
                              ),
                            ),
                          ))
                        ],
                      ),
                      //灰線
                      Container(
                        height: 0.3.h,
                        width: Device.width,
                        color: AppColor.grayLine,
                      ),
                      //內容
                      Expanded(
                        child: Column(
                          children: [
                            Container(
                              padding: const EdgeInsets.only(left: 30, top: 30),
                              child: Row(
                                children: [
                                  //頭像
                                  Container(
                                    width: 20.w,
                                    height: 20.w,
                                    decoration: BoxDecoration(
                                        shape: BoxShape.circle,
                                        gradient: AppColor.appMainColor),
                                    child: MugshotWidget(
                                      fromMemberId: _memberProvider!.memberModel.id!,
                                      targetMemberId: widget.id,
                                      nickName: widget.nickname,
                                      gender: widget.gender,
                                      birthday: widget.birthday,
                                    ),
                                  ),

                                  const SizedBox(
                                    width: 30,
                                  ),
                                  //名稱&性別年齡
                                  Expanded(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                      children: [
                                        //名稱
                                        FittedBox(
                                          child: Text(
                                            widget.nickname,
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 21.sp,
                                                color: Colors.black),
                                          ),
                                        ),
                                        SizedBox(
                                          height: 3.h,
                                        ),
                                        //性別年齡
                                        Row(
                                          children: [
                                            getGender(widget.gender),
                                            //年齡
                                            Container(
                                              margin: const EdgeInsets.only(left: 5),
                                              child: Text(
                                                Util().getAge(widget.birthday),
                                                style: TextStyle(
                                                    color: Colors.black,
                                                    fontSize: 17.sp),
                                              ),
                                            )
                                          ],
                                        )
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Expanded(
                              child: Center(
                                child: Container(
                                  padding: const EdgeInsets.only(left: 10, right: 10),
                                  child: Text(
                                    widget.message,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        fontSize: 20.sp,
                                        color: Colors.black,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      //按鈕
                      Container(
                        margin: const EdgeInsets.only(left: 10, right: 10, bottom: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            GestureDetector(
                              onTap: () {
                                Navigator.of(context).pop(true);
                              },
                              child: Image(
                                width: 70,
                                image: AssetImage(/*widget.check == 1 ? */AppImage.btnClean/* : AppImage.iconYes*/),
                              ),
                            ),
                            GestureDetector(
                              onTap: () => _goToOtherHouse(),
                              child: Image(
                                width: 70,
                                image: AssetImage(AppImage.btnVisit),
                              ),
                            )
                          ],
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ),
        ),
        onBackButtonPressed: () => pop());
  }

  void _goToOtherHouse(){
    //要去別人家
    EasyLoading.show(status: 'Loading...');
    Future.wait([
      _memberProvider!.getMemberByIdToMemberModel(id: widget.id),
      _gameHomeProvider!.getStreetMemberPetsByMemberId(id: widget.id, refresh: false),
      _gameHomeProvider!.getStreetMemberFurnitureByMemberId(id: widget.id, refresh: false),
      _gameHomeProvider!.getThisPeopleHouseByMemberId(id: widget.id).then((value) {
        //用memberId取得陌生人家留言
        _gameHomeProvider!.getStreetMemberHouseMessage(id: _gameHomeProvider!.thisPeopleModel.id!);
        //用memberId取得陌生人家大便
        _gameHomeProvider!.getStreetMemberDungByMemberHouseId(id: _gameHomeProvider!.thisPeopleModel.id!);
      }),
    ]).then((value) {
      Future.delayed(const Duration(milliseconds: 1000), (){
        EasyLoading.dismiss();
        delegate.push(name: RouteName.gameHomeOtherPeoplePage, arguments: value[0] as MemberModel);
      });

    });
  }

  Future<bool> pop() async {
    Navigator.of(context).pop(false);
    return true;
  }

}