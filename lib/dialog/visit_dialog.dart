import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../app.dart';
import '../utils/app_color.dart';
import '../widget/button_widget.dart';

///
/// 探班 同意或不同意
///
/// * [title] 最上層顯示
/// * [content] 內容
/// * [confirmButtonText] 確認按鈕文字
/// * [cancelButtonText] 取消按鈕文字
///

class VisitDialog extends StatefulWidget{
  final String title;
  final String content;
  final String confirmButtonText;
  final String cancelButtonText;

  const VisitDialog({
    Key? key,
    required this.title,
    required this.content,
    required this.confirmButtonText,
    required this.cancelButtonText,
  }) : super(key: key);

  @override
  State createState() => _VisitDialogState();
}

class _VisitDialogState extends State<VisitDialog>{



  @override
  Widget build(BuildContext context) {
    return BackButtonListener(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.black54,
          body: Center(
            child: Container(
              width: Device.width - 60,
              height: 50.h,
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(20)),
              child: Column(
                children: [
                  //title
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 1.5.h),
                    child: Text(
                      widget.title,
                      style: TextStyle(
                          fontSize: 20.sp,
                          color: Colors.black,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  //灰線
                  Container(
                    height: 0.3.h,
                    width: Device.width,
                    color: AppColor.grayLine,
                  ),
                  //內容
                  Expanded(
                    child: Container(
                      alignment: Alignment.center,
                      child: Text(
                        widget.content,
                        style: TextStyle(
                            fontSize: 20.sp,
                            color: Colors.black,
                            fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  //同意按鈕
                  GestureDetector(
                    onTap: () => Navigator.of(context).pop(true),
                    child: Container(
                      margin: EdgeInsets.only(bottom: 2.h),
                      width: 58.w,
                      height: 7.h,
                      child: buttonWidget(widget.confirmButtonText),
                    ),
                  ),
                  //不同意按鈕
                  //按鈕
                  GestureDetector(
                    onTap: () => Navigator.of(context).pop(false),
                    child: Container(
                      margin: EdgeInsets.only(bottom: 5.h),
                      width: 58.w,
                      height: 7.h,
                      child: buttonWidgetDispose(widget.cancelButtonText),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
        onBackButtonPressed: () => pop());
  }
  Future<bool> pop() async {
    Navigator.of(context).pop();
    return true;
  }

  @override
  void initState() {
    super.initState();
    //log('探班對話框初始化');
    util.dialogIsOpen = true;
  }

  @override
  void dispose() {
    super.dispose();
    //log('探班對話框解除');
    util.dialogIsOpen = false;
  }
}