import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:webview_flutter/webview_flutter.dart';


class BonusDialog extends StatefulWidget{

  const BonusDialog({Key? key}): super(key: key);

  @override
  State createState() => _BonusDialogState();
}

class _BonusDialogState extends State<BonusDialog>{

  WebViewController? _controller;


  @override
  void initState() {
    super.initState();
    if(Platform.isAndroid)WebView.platform = SurfaceAndroidWebView();
  }


  @override
  void dispose() {
    super.dispose();
    _controller!.clearCache();
  }

  _loadHTML() async {
    try{
      /*String html = await _controller!.runJavascriptReturningResult("encodeURIComponent(document.documentElement.outerHTML)");
      log(Uri.decodeComponent(html));
      await _controller!.runJavascriptReturningResult('document.getElementsByClassName("logo")[0].remove();').onError((error, stackTrace) => '');
      await _controller!.runJavascriptReturningResult('document.getElementsByClassName("tr")[2].remove();').onError((error, stackTrace) => '');
      await _controller!.runJavascriptReturningResult('document.getElementsByClassName("tr")[3].remove();').onError((error, stackTrace) => '');*/
      await _controller!.runJavascriptReturningResult('window.alert = function () {return true};').onError((error, stackTrace) => '');
      await _controller!.runJavascriptReturningResult('window.confirm = function () {return true};').onError((error, stackTrace) => '');

      /*await _controller!.runJavascriptReturningResult('document.getElementById("customizedbanner").remove();').onError((error, stackTrace) => '');
      await _controller!.runJavascriptReturningResult('document.getElementsByClassName("respond-field")[0].remove();').onError((error, stackTrace) => '');
      await _controller!.runJavascriptReturningResult('document.getElementById("bottomFooter").remove();').onError((error, stackTrace) => '');
      await _controller!.runJavascriptReturningResult('document.getElementById("content").style.padding= 0').onError((error, stackTrace) => '');*/
     /* await _controller!.runJavascriptReturningResult('document.getElementsByName("members_id")[0].value = ${widget.rechargeRecordModel.memberId};');
      await _controller!.runJavascriptReturningResult('document.getElementsByName("cust_order_no")[0].value = "${widget.rechargeRecordModel.orderNumber}";');
      await _controller!.runJavascriptReturningResult('document.getElementsByName("order_amount")[0].value = ${widget.rechargeRecordModel.amount};');
      *//*await _controller!.runJavascriptReturningResult('document.getElementsByName("payer_name")[0].value = "${member.nickname}";');*//*
      *//*await _controller!.runJavascriptReturningResult('document.getElementsByName("payer_mobile")[0].value = "${memberRoleModel!.phone}";');*//*
      *//*await _controller!.runJavascriptReturningResult('submit()');*/
    }catch(e){
      e.toString();
    }
  }

  @override
  Widget build(BuildContext context) {
    dynamic obj = ModalRoute.of(context)?.settings.arguments;
    return SafeArea(child: WebView(
        initialUrl: obj,
        javascriptMode: JavascriptMode.unrestricted,
        gestureNavigationEnabled: true,
        onWebViewCreated: (WebViewController webViewController){
          _controller = webViewController;
        },
        onPageFinished: (_){
          _loadHTML();
        }
    ));
  }
}