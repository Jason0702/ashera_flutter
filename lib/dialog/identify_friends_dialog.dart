
import 'dart:developer';

import 'package:ashera_flutter/provider/member_provider.dart';
import 'package:ashera_flutter/provider/stomp_client_provider.dart';
import 'package:ashera_flutter/provider/util_api_provider.dart';
import 'package:ashera_flutter/utils/api.dart';
import 'package:ashera_flutter/utils/app_image.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_font_icons/flutter_font_icons.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../enum/app_enum.dart';
import '../generated/l10n.dart';
import '../models/face_image_model.dart';
import '../models/member_model.dart';
import '../provider/identification_provider.dart';
import '../utils/app_color.dart';
import '../utils/util.dart';
import '../widget/titlebar_widget.dart';

class IdentifyFriendsDialog extends StatefulWidget {
  final bool isGame;
  const IdentifyFriendsDialog({Key? key, required this.isGame}) : super(key: key);

  @override
  State createState() => _IdentifyFriendsDialogState();
}

class _IdentifyFriendsDialogState extends State<IdentifyFriendsDialog> {
  //倒數結束?
  bool isOver = true;
  IdentificationProvider? _identificationProvider;
  UtilApiProvider? _utilApiProvider;
  MemberProvider? _memberProvider;
  StompClientProvider? _stompClientProvider;

  _onLayoutDone(_) async {
    if(_utilApiProvider!.faceImageList.isNotEmpty){
      _utilApiProvider!.getUidSearchMember(_utilApiProvider!.faceImageList.first.asheraUid!);
    }
    _identificationProvider!.getFacesDetectHistoryByTargetMemberIdAndFromMemberId(_memberProvider!.memberModel.id!);
  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  Widget build(BuildContext context) {
    log('最外了');
    _utilApiProvider = Provider.of<UtilApiProvider>(context, listen: false);
    _memberProvider = Provider.of<MemberProvider>(context, listen: false);
    _stompClientProvider = Provider.of<StompClientProvider>(context, listen: false);
    _identificationProvider = Provider.of<IdentificationProvider>(context, listen: false);
    return BackButtonListener(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: AppColor.appBackgroundColor,
          body: Column(
            children: [
              //返回 與 標題
              titleBarIntegrate([], S.of(context).identification_record, []),
              //會員資料
              memberInfo(),
              //會員個人資料
              Expanded(flex: 3,child: memberPersonalInfo()),
              //是否倒數完成
              Expanded(flex: 1,child: isOver ? countDownOver() : countDown())
            ],
          ),
        ),
        onBackButtonPressed: () => pop());
  }

  Future<bool> pop() async {
    Navigator.of(context).pop(true);
    return true;
  }

  //會員資料
  Widget memberInfo() {
    return Consumer<UtilApiProvider>(builder: (context, searchMember, _) {
      FaceImageModel? _searchMember;
      if(searchMember.faceImageList.isNotEmpty){
        _searchMember = searchMember.faceImageList.first;
      }
      return Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(height: 2.h,),
          //頭像
          if(_searchMember != null)
            if(!widget.isGame)
              _searchMember.mugshot!.isEmpty
                ? Container(
            width: 16.w * 2,
            height: 8.h * 2,
            decoration: BoxDecoration(
                shape: BoxShape.circle, gradient: AppColor.appMainColor),
            child: const CircleAvatar(
              backgroundColor: Colors.transparent,
              child: Icon(
                FontAwesome5Solid.user,
                size: 30,
                color: Colors.white,
              ),
            ),
          )
                : Container(
            width: 16.w * 2,
            height: 8.h * 2,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              image: DecorationImage(
                  fit: BoxFit.cover,
                  image: CachedNetworkImageProvider(
                    Util().getMemberMugshotUrl(_searchMember.mugshot!),
                    headers: {"authorization": "Bearer " + Api.accessToken},
                  ),
                  onError: (error, stackTrace){
                    debugPrint('Error: ${error.toString()}');
                    if(error.toString().contains('statusCode: 404')){

                    }
                  }
              ),
            ),
          ),
          if(_searchMember != null)
            if(widget.isGame)
              _searchMember.interactivePic!.isEmpty
                  ? Container(
                width: 16.w * 2,
                height: 8.h * 2,
                decoration: BoxDecoration(
                    shape: BoxShape.circle, gradient: AppColor.appMainColor),
                child: const CircleAvatar(
                  backgroundColor: Colors.transparent,
                  child: Icon(
                    FontAwesome5Solid.user,
                    size: 30,
                    color: Colors.white,
                  ),
                ),
              )
                  : Container(
                width: 16.w * 2,
                height: 8.h * 2,
                decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                      fit: BoxFit.cover,
                      image: CachedNetworkImageProvider(
                        Util().getMemberMugshotUrl(_searchMember.interactivePic!),
                        headers: {"authorization": "Bearer " + Api.accessToken},
                      ),
                      onError: (error, stackTrace){
                        debugPrint('Error: ${error.toString()}');
                        if(error.toString().contains('statusCode: 404')){

                        }
                      }
                  ),
                ),
              ),
          //名稱
          if(_searchMember != null)
          FittedBox(
            child: Text(
              _searchMember.nickname!,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 18.sp,
                  color: Colors.black),
            ),
          ),
          //SizedBox(height: 1.h,),
          //ID
          if(_searchMember != null)
          FittedBox(
            child: Text(
              "ID：${_searchMember.asheraUid}",
              style: TextStyle(fontSize: 18.sp, color: Colors.black),
            ),
          ),
          Container(
              alignment: Alignment.center,
              width: Device.width,
              child: RichText(
                text: TextSpan(children: [
                  //一周內被刷臉
                  TextSpan(
                    text: S.of(context).get_face_in_this_week,
                    style: TextStyle(fontSize: 18.sp, color: AppColor.grayText),
                  ),
                  //幾
                  TextSpan(
                    text: "${searchMember.faceScanTime}",
                    style: TextStyle(
                        fontSize: 18.sp,
                        fontWeight: FontWeight.bold,
                        color: Colors.red),
                  ),
                  //次
                  TextSpan(
                    text: S.of(context).how_many_times,
                    style: TextStyle(fontSize: 18.sp, color: AppColor.grayText),
                  ),
                ]),
              ))
        ],
      );
    });
  }

  //會員個人資料
  Widget memberPersonalInfo() {
    log('外層刷新');
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 5.w,),
      color: Colors.white,
      child: SingleChildScrollView(
        child: Consumer<UtilApiProvider>(
          builder: (context, searchMember, _) {
            log('Provider刷新');
            FaceImageModel? _searchMember;
            if(searchMember.faceImageList.isNotEmpty){
              _searchMember ??= searchMember.faceImageList.first;
              //log('_searchMember: ${_searchMember.toJson()}');
            }
            return Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                //關於我
                if(_searchMember != null)
                  Container(
                    width: Device.boxConstraints.maxWidth,
                    decoration: const BoxDecoration(
                        color: Colors.white
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          S.of(context).about_me,
                          style: TextStyle(fontSize: 18.sp, color: Colors.black),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Container(
                          height: 70,
                          width: Device.boxConstraints.maxWidth,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              //border: Border.all(color: AppColor.grayLine, width: 1)
                          ),
                          child: Text(
                            _searchMember.aboutMe!.isEmpty ? '無' : _searchMember.aboutMe!,
                            maxLines: 3,
                            style: TextStyle(fontSize: 18.sp,
                                color: Colors.black,
                                height: 1.1),
                          ),
                        )
                      ],
                    ),
                  ),
                //性別
                Row(
                  children: [
                    Text(
                      S.of(context).gender,
                      style: TextStyle(fontSize: 18.sp, color: Colors.black),
                    ),
                    SizedBox(
                      width: 5.w,
                    ),
                    if(_searchMember != null)
                      _maleOrFemaleText(gender: _searchMember.gender!),
                    SizedBox(
                      width: 1.w,
                    ),
                    if(_searchMember != null)
                      _maleOrFemaleIcon(gender: _searchMember.gender!)
                  ],
                ),
                const SizedBox(
                  height: 5,
                ),
                //生日
                Row(
                  children: [
                    Text(
                      S.of(context).birthday,
                      style: TextStyle(fontSize: 18.sp, color: Colors.black),
                    ),
                    SizedBox(
                      width: 5.w,
                    ),
                    if(_searchMember != null)
                      Text(
                        _searchMember.hideBirthday! ? S.of(context).secret : _searchMember.birthday!,
                        style: TextStyle(
                            fontSize: 18.sp,
                            color: Colors.black,
                            ),
                      ),
                  ],
                ),
                const SizedBox(
                  height: 5,
                ),
                //身高
                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      S.of(context).height,
                      style: TextStyle(fontSize: 18.sp, color: Colors.black),
                    ),
                    SizedBox(
                      width: 5.w,
                    ),
                    if(_searchMember != null)
                      Text(
                        '${_searchMember.height}',
                        style: TextStyle(
                            fontSize: 18.sp,
                            color: Colors.black,
                            ),
                      ),
                    SizedBox(width: 2.w,),
                    Text(
                      "cm",
                      style: TextStyle(fontSize: 15.sp, color: Colors.black),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 5,
                ),
                //體重
                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      S.of(context).weight,
                      style: TextStyle(fontSize: 18.sp, color: Colors.black),
                    ),
                    SizedBox(
                      width: 5.w,
                    ),
                    if(_searchMember != null)
                      Text(
                        _searchMember.hideWeight! ? S.of(context).secret : '${_searchMember.weight}',
                        style: TextStyle(
                            fontSize: 18.sp,
                            color: Colors.black,
                            ),
                      ),
                    SizedBox(width: 2.w,),
                    Text(
                      "kg",
                      style: TextStyle(fontSize: 15.sp, color: Colors.black),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 5,
                ),
                //血型
                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      S.of(context).blood_type,
                      style: TextStyle(fontSize: 18.sp, color: Colors.black),
                    ),
                    SizedBox(
                      width: 5.w,
                    ),
                    if(_searchMember != null)
                      Text(
                        _searchMember.bloodType ?? 'O',
                        style: TextStyle(
                            fontSize: 18.sp,
                            color: Colors.black,
                            ),
                      ),
                  ],
                ),
                const SizedBox(
                  height: 5,
                ),
                //星座
                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      S.of(context).constellation,
                      style: TextStyle(fontSize: 18.sp, color: Colors.black),
                    ),
                    SizedBox(
                      width: 5.w,
                    ),
                    if(_searchMember != null)
                      Text(
                        S.of(context).horoscope(_searchMember.zodiacSign ?? Horoscope.aries.name),
                        style: TextStyle(
                            fontSize: 18.sp,
                            color: Colors.black,
                            ),
                      ),
                  ],
                ),
                const SizedBox(
                  height: 5,
                ),
                //職業
                Row(
                  children: [
                    Text(
                      S.of(context).profession,
                      style: TextStyle(fontSize: 18.sp, color: Colors.black),
                    ),
                    SizedBox(
                      width: 5.w,
                    ),
                    if(_searchMember != null)
                      Text(
                        _searchMember.hideJob! ? S.of(context).secret : _searchMember.job ?? '民意代表、主管及經理人員',
                        style: TextStyle(
                            fontSize: 18.sp,
                            color: Colors.black,
                            ),
                      ),
                  ],
                ),
                const SizedBox(
                  height: 5,
                ),
                //名片自介
                if(_searchMember != null)
                  Container(
                    width: Device.boxConstraints.maxWidth,
                    decoration: const BoxDecoration(
                        color: Colors.white
                    ),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          S.of(context).business_card_introduction,
                          style: TextStyle(fontSize: 18.sp, color: Colors.black),
                        ),
                        const SizedBox(
                          height: 5,
                        ),
                        Container(
                          height: 70,
                          width: Device.boxConstraints.maxWidth,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              //border: Border.all(color: AppColor.grayLine, width: 1)
                          ),
                          child: Text(
                            _searchMember.aboutMe2!.isEmpty ? '無' : _searchMember.aboutMe2!,
                            maxLines: 3,
                            style: TextStyle(fontSize: 18.sp,
                                color: Colors.black,
                                height: 1.1),
                          ),
                        )
                      ],
                    ),
                  ),
                //辨識度
                Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(
                      S.of(context).identify_percent,
                      style: TextStyle(fontSize: 18.sp, color: Colors.black),
                    ),
                    SizedBox(
                      width: 5.w,
                    ),
                    if(_searchMember != null)
                      Text(
                        "${_searchMember.confidence}%",
                        style: TextStyle(
                            fontSize: 18.sp,
                            color: Colors.black,
                        ),
                      ),
                  ],
                ),
              ],
            );
          },
        ),
      ),
    );
  }
  ///
  ///男或女 字
  ///
  ///[gender] 性別
  Widget _maleOrFemaleText({required int gender}) {
    return gender == 1
        ? Text(
            S.of(context).female,
            style: TextStyle(
                fontSize: 18.sp,
                color: Colors.black,
                ),
          )
        : Text(
            S.of(context).male,
            style: TextStyle(
                fontSize: 18.sp,
                color: Colors.black,
                ),
          );
  }
  ///
  ///男或女 Icon
  ///
  ///[gender] 性別
  Widget _maleOrFemaleIcon({required int gender}) {
    return gender == 1
        //女
        ? const Icon(
            FontAwesome5Solid.venus,
            size: 23,
            color: AppColor.femaleColor,
          )
        //男
        : const Icon(FontAwesome5Solid.mars,
            size: 23, color: AppColor.maleColor);
  }

  //倒數時間
  Widget countDown() {
    return Container(
      alignment: Alignment.center,
      child: Text(
        "${S.of(context).count_down}00:10",
        style: TextStyle(fontSize: 20.sp, color: Colors.black),
      ),
    );
  }

  //倒數時間結束
  Widget countDownOver() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        if(_utilApiProvider!.faceImageList.isNotEmpty)
        _isAddOrCanAddText(_utilApiProvider!.faceImageList.first, _stompClientProvider!.friendList),
        SizedBox(
          height: 1.h,
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            if(_utilApiProvider!.faceImageList.isNotEmpty)
            _isAddOrCanAdd(_utilApiProvider!.faceImageList.first, _stompClientProvider!.friendList),
            SizedBox(
              width: 10.w,
            ),
            GestureDetector(
              onTap: () => _noOnTap(),
              child: SizedBox(width: 20.w, child: Image.asset(AppImage.iconNo)),
            )
          ],
        )
      ],
    );
  }
  ///
  /// 添加好友或已是好友 字
  ///
  /// [faceImageModel] 人臉辨識結果
  /// [friendList] 好友列表
  Widget _isAddOrCanAddText(FaceImageModel faceImageModel, List<MemberModel> friendList){
    if(friendList.where((element) => element.asheraUid == faceImageModel.asheraUid).isEmpty){
      return Text(
        S.of(context).identify_time_over_content,
        style: TextStyle(fontSize: 20.sp, color: Colors.black),
      );
    }else{
      return Text(
        S.of(context).added_friend,
        style: TextStyle(fontSize: 20.sp, color: Colors.black),
      );
    }
  }
  ///
  ///添加好友或已是好友 按鈕
  ///
  /// [faceImageModel] 人臉辨識結果
  /// [friendList] 好友列表
  ///
  /// 判斷已有此好友，愛心按鈕將反灰
  Widget _isAddOrCanAdd(FaceImageModel faceImageModel, List<MemberModel> friendList){
    if(_memberProvider!.memberModel.asheraUid != faceImageModel.asheraUid){
      if(friendList.where((element) => element.asheraUid == faceImageModel.asheraUid).isEmpty){
        return GestureDetector(
          onTap: () => _yesOnTap(),
          child: SizedBox(width: 20.w, child: Image(
            image: AssetImage(AppImage.iconYes),
          )),
        );
      } else {
        return SizedBox(
          width: 20.w,
          child: Image(
            image: AssetImage(AppImage.iconYesGray),
          ),
        );
      }
    } else {
      return SizedBox(
        width: 20.w,
        child: Image(
          image: AssetImage(AppImage.iconYesGray),
        ),
      );
    }
  }

  void _yesOnTap(){
    _stompClientProvider!.sendFollowerRequest(_memberProvider!.memberModel.id!, _utilApiProvider!.memberModel!.id!);
    EasyLoading.showSuccess(S.of(context).friendship_invitation_has_been_sent);//已送出交友邀請
    _utilApiProvider!.setUidSearchMemberClear();
    Navigator.of(context).pop(true);
  }

  void _noOnTap(){
    Navigator.of(context).pop(true);
  }
}
