import 'package:flutter/material.dart';
import 'package:flutter_font_icons/flutter_font_icons.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../generated/l10n.dart';
import '../utils/app_color.dart';
import '../widget/button_widget.dart';

class RecognitionFailedDialog extends StatefulWidget {
  const RecognitionFailedDialog({Key? key}) : super(key: key);

  @override
  State createState() => _RecognitionFailedDialogState();
}

class _RecognitionFailedDialogState extends State<RecognitionFailedDialog> {
  int index = 0;

  @override
  Widget build(BuildContext context) {
    return BackButtonListener(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.black54,
          body: Center(
              child: Container(
            width: 85.w,
            height: 65.h,
            decoration: BoxDecoration(
                color: Colors.white, borderRadius: BorderRadius.circular(20)),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: [
                //title
                Container(
                  margin: EdgeInsets.symmetric(vertical: 1.h),
                  child: Text(
                    S.of(context).identify_friends,
                    style: TextStyle(
                        fontSize: 23.sp,
                        color: Colors.black,
                        fontWeight: FontWeight.bold),
                  ),
                ),
                //灰線
                Container(
                  height: 0.2.h,
                  width: Device.width,
                  color: AppColor.grayLine,
                ),
                //辨識失敗，請選擇要加的好友
                Container(
                    height: 10.h,
                    width: 70.w,
                    alignment: Alignment.center,
                    child: FittedBox(
                      child: Text(
                        S.of(context).recognition_failed,
                        style: TextStyle(
                            fontSize: 20.sp,
                            color: Colors.black,
                            fontWeight: FontWeight.bold),
                      ),
                    )),
                //待選擇的人
                _waitingToChoose(),
                //確認按鈕
                _confirmButton(),
                SizedBox(
                  height: 3.h,
                )
              ],
            ),
          )),
        ),
        onBackButtonPressed: () => pop());
  }

  Future<bool> pop() async {
    Navigator.of(context).pop();
    return true;
  }

  //待選擇的人
  Widget _waitingToChoose() {
    return SizedBox(
      height: 30.h,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Expanded(child: _otherMemberInfo(0, '林888', _click0)),
          Expanded(child: _otherMemberInfo(1, '張小英', _click1)),
          Expanded(child: _otherMemberInfo(2, '陳小英', _click2))
        ],
      ),
    );
  }

  //點了第一個
  void _click0() {
    setState(() {
      index = 0;
    });
  }

  //點了第二個
  void _click1() {
    setState(() {
      index = 1;
    });
  }

  //點了第三個
  void _click2() {
    setState(() {
      index = 2;
    });
  }

  //確認按鈕
  Widget _confirmButton() {
    return GestureDetector(
      onTap: () {
        //關閉對話框
        Navigator.of(context).pop();
      },
      child: SizedBox(
        height: 7.h,
        width: 58.w,
        child: buttonWidget(S.of(context).confirm),
      ),
    );
  }

  //其他人的資料
  Widget _otherMemberInfo(int _index, String _name, VoidCallback _click) {
    return GestureDetector(
      onTap: () => _click(),
      child: Container(
        color: _index == index ? AppColor.buttonFrameColor : Colors.white,
        child: Container(
          margin: EdgeInsets.only(left: 6.w, right: 6.w),
          child: Row(
            children: [
              //頭像
              Container(
                width: 15.w,
                height: 15.w,
                decoration: BoxDecoration(
                    shape: BoxShape.circle, gradient: AppColor.appMainColor),
                child: const CircleAvatar(
                  backgroundColor: Colors.transparent,
                  child: Icon(
                    FontAwesome5Solid.user,
                    size: 30,
                    color: Colors.white,
                  ),
                ),
              ),
              SizedBox(
                width: 3.w,
              ),
              //名稱
              FittedBox(
                child: Text(
                  _name,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 20.sp,
                      color: Colors.black),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
