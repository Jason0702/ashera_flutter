import 'dart:async';
import 'dart:developer';

import 'package:ashera_flutter/app.dart';
import 'package:ashera_flutter/enum/app_enum.dart';
import 'package:ashera_flutter/provider/stomp_client_provider.dart';
import 'package:ashera_flutter/utils/util.dart';
import 'package:ashera_flutter/widget/water_ripple_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_font_icons/flutter_font_icons.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import '../generated/l10n.dart';
import '../models/visit_record_model.dart';
import '../utils/app_color.dart';

class CancelVisitDialog extends StatefulWidget {
  const CancelVisitDialog({Key? key}) : super(key: key);

  @override
  _CancelVisitDialogState createState() => _CancelVisitDialogState();
}

class _CancelVisitDialogState extends State<CancelVisitDialog> {

  StompClientProvider? _stompClientProvider; //STOMP Provider
  VisitRecordModel? _visitRecordModel;

  //倒數計時
  Timer? _timer;
  int _countdownTime = 0;

  _onLayoutDone(_){
    if(_timer == null && _countdownTime == 0){
      _countdownTime = 30;
      setState(() {});
      _startCountdownTimer();
    }
  }

  void _startCountdownTimer(){
    _timer = Timer.periodic(const Duration(milliseconds: 1000), (timer) {
      if(mounted){
        setState(() {
          if(_countdownTime < 1){
            _timer!.cancel();
            _timer = null;
            if(_visitRecordModel!.cancelBy == WhoSide.from.index){ //探班方撤回取消
              _stompClientProvider!.sendCancelBackByFromMember(id: _visitRecordModel!.id!);
            } else {
              _stompClientProvider!.sendCancelBackByTargetMember(id: _visitRecordModel!.id!);
            }
            delegate.popRoute();
          } else {
            _countdownTime--;
          }
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    _stompClientProvider = Provider.of<StompClientProvider>(context);
    dynamic obj = ModalRoute.of(context)?.settings.arguments;
    _visitRecordModel ??= obj;
    return BackButtonListener(
        child: Scaffold(
          body: Container(
            decoration: BoxDecoration(gradient: AppColor.appMainColor),
            child: Stack(
              children: [
                //漣漪
                Center(
                  child: SizedBox(
                    width: Device.width - 10.w,
                    height: Device.width - 10.w,
                    child: const WaterRipple(
                      count: 2,
                      color: AppColor.watterRippleColor,
                    ),
                  ),
                ),
                //title
                Positioned(
                  top: 15.h,
                  child: Container(
                    width: Device.width,
                    alignment: Alignment.center,
                    child: Text(
                      S.of(context).wait_other_side_cancel_visit,
                      style: TextStyle(
                          color: AppColor.buttonTextColor, fontSize: 22.sp),
                    ),
                  ),
                ),
                //頭像
                Center(
                  child: Container(
                    width: 30.w,
                    height: 30.w,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        gradient: AppColor.appMainColor),
                    child: const CircleAvatar(
                      backgroundColor: Colors.transparent,
                      child: Icon(
                        FontAwesome5Solid.user,
                        size: 40,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
                //等待時間
                Positioned(
                  bottom: 22.h,
                  child: Container(
                    width: Device.width,
                    alignment: Alignment.center,
                    child: Text(
                      "${S.of(context).wait_time}: 00:${Util.formatter.format(_countdownTime)}",
                      style: TextStyle(
                          color: AppColor.buttonTextColor, fontSize: 23.sp),
                    ),
                  ),
                ),
                //取消
                Positioned(
                  bottom: 10.h,
                  child: GestureDetector(
                    onTap: () => _cancelOnTap(),
                    child: Container(
                      width: Device.width,
                      alignment: Alignment.center,
                      child: Container(
                        width: 60.w,
                        padding: EdgeInsets.symmetric(
                            horizontal: 10.w, vertical: 1.h),
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                            border: Border.all(
                                color: AppColor.buttonTextColor, width: 1),
                            borderRadius: BorderRadius.circular(30)),
                        child: Text(
                          S.of(context).cancel,
                          style: TextStyle(
                              color: AppColor.buttonTextColor, fontSize: 23.sp),
                        ),
                      ),
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
        onBackButtonPressed: () => pop());
  }
  //撤回取消
  void _cancelOnTap() {
    if(_visitRecordModel!.cancelBy == WhoSide.from.index){ //探班方撤回取消
      _stompClientProvider!.sendCancelBackByFromMember(id: _visitRecordModel!.id!);
    } else {
      _stompClientProvider!.sendCancelBackByTargetMember(id: _visitRecordModel!.id!);
    }
    Navigator.of(context).pop();
  }

  Future<bool> pop() async {//不能讓使用者返回
    return true;
  }

  @override
  void initState() {
    super.initState();
    //log('探班取消等待對話框初始化');
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  void dispose() {
    super.dispose();
    //log('探班取消等待對話框解除');
    if(_timer != null){
      _timer!.cancel();
      _timer = null;
    }
  }
}
