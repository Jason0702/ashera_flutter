import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../generated/l10n.dart';
import '../utils/app_color.dart';
import '../widget/button_widget.dart';

class MemberHouseMessageDialog extends StatefulWidget{
  final String title;
  final String confirmButtonText;

  const MemberHouseMessageDialog({
    Key? key,
    required this.title,
    required this.confirmButtonText
  }): super(key: key);

  @override
  State createState() => _MemberHouseMessageDialogState();
}

class _MemberHouseMessageDialogState extends State<MemberHouseMessageDialog>{
  //留言
  final TextEditingController _message = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return BackButtonListener(
        child: Scaffold(
          resizeToAvoidBottomInset: false,
          backgroundColor: Colors.black54,
          body: Center(
            child: Container(
              width: Device.width - 60,
              height: 43.h,
              decoration: BoxDecoration(
                  color: Colors.white, borderRadius: BorderRadius.circular(20)),
              child: Stack(
                children: [
                  //內容
                  Column(
                    children: [
                      //title
                      Container(
                        margin: EdgeInsets.symmetric(vertical: 1.5.h),
                        child: Text(
                          widget.title,
                          style: TextStyle(
                              fontSize: 20.sp,
                              color: Colors.black,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      //灰線
                      Container(
                        height: 0.3.h,
                        width: Device.width,
                        color: AppColor.grayLine,
                      ),
                      //內容
                      Expanded(
                          child: Container(
                            width: Device.boxConstraints.maxWidth,
                            decoration: const BoxDecoration(color: Colors.white),
                            child: Container(
                              margin: EdgeInsets.only(left: 3.w, right: 3.w),
                              alignment: Alignment.centerLeft,
                              child: TextField(
                                style: const TextStyle(color: Colors.black),
                                decoration: InputDecoration(
                                  contentPadding: const EdgeInsets.all(5),
                                  enabledBorder: OutlineInputBorder(
                                      borderSide: const BorderSide(color: AppColor.grayLine),
                                      borderRadius: BorderRadius.circular(5)),
                                  border:
                                  OutlineInputBorder(borderRadius: BorderRadius.circular(5)),
                                  fillColor: Colors.white,
                                  hintText: S.of(context).enter_a_message,
                                  filled: true,
                                ),
                                maxLines: 8,
                                keyboardType: TextInputType.multiline,
                                textInputAction: TextInputAction.newline,
                                controller: _message,
                              ),
                            ),
                          )
                      ),
                      SizedBox(
                        height: 1.5.h,
                      ),
                      //同意按鈕
                      GestureDetector(
                        onTap: () {
                          if(_message.text.trim().isNotEmpty && _message.text.trim().length >= 8 && _message.text.trim().length <= 50){
                            Navigator.of(context).pop({'status': true, 'value': _message.text});
                          } else if(_message.text.trim().isEmpty || _message.text.trim().length < 8){
                            EasyLoading.showToast(S.of(context).enter_a_message);
                          } else if(_message.text.trim().length > 50){
                            EasyLoading.showToast(S.of(context).the_word_count_exceeds_the_limit_of_50_characters);
                          }
                        },
                        child: Container(
                          margin: EdgeInsets.only(bottom: 2.h),
                          width: 58.w,
                          height: 7.h,
                          child: buttonWidget(widget.confirmButtonText),
                        ),
                      ),
                    ],
                  ),
                  //右上X
                  Positioned(
                      right: 10,
                      top: 10,
                      child: GestureDetector(
                        onTap: () => Navigator.of(context).pop({'status': false}),
                        child: Icon(
                          Icons.cancel,
                          size: 10.w,
                        ),
                      )),
                ],
              ),
            ),
          ),
        ),
        onBackButtonPressed: () => pop());
  }

  Future<bool> pop() async {
    Navigator.of(context).pop({'status': false});
    return true;
  }
}