enum AppPosition{
  home,
  visit,
  peekaboo,
  game,
  gameHome,
}

extension AppPositionExtension on AppPosition{
  static List<String> zhList = [
    '首頁',
    '探班',
    '躲貓貓',
    '小遊戲',
    '小遊戲房間'
  ];
  String get zh => zhList[index];
}