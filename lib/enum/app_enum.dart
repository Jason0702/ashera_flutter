//驗證狀態
import 'package:ashera_flutter/utils/app_image.dart';

enum AuthStatus {
  notLoggedIn, //未登入
  notRegistered, //未註冊
  loggedIn, //登入
  registered, //註冊
  authenticating, //驗證中
  loggedOut //登出
}
enum ApiStatus{
  idle,
  inExecution
}

//上線狀態
enum OnlineStatus {
  onlineStatusOffline, //離線
  onlineStatusOnline, //上線
}
//充值按鈕
enum PointsStatus{
  closure, //關閉
  show, //顯示
}
//VIP升級按鈕
enum VIPStatus{
  show, //顯示
  closure //關閉
}

//上傳狀態
enum UpLoadingStatus { notUpLoading, upLoading, uploadCompleted, uploadFailed }

//選項SS
enum BottomNavigationBarStatus { friend, chat, scan, game, me }

//選單狀態
enum MenuStatus { none, menu }

//新增好友或掃描QRCode
enum AddFriendStatus {
  none,
  addFriend,
  scanQr,
}

//躲貓貓使用者狀態
enum PeekabooUserStatus {
  //找人的
  seek,
  //被找的
  sought
}

//性別
enum GenderStatus {
  //男生
  boy,
  //女生
  girl,
  //不拘
  none,
}

//探班狀態
enum VisitStatus {
  //配對中
  pairing,
  //已完成
  complete,
  //進行中
  processing,
  //已取消
  cancel,
  //取消等待同意
  cancelWaitingApprove,
  //探班等待同意
  waitingApprove,
}

//按鈕狀態
enum ButtonStatus { click, notClick }

//充值或充值紀錄
enum RechargeOrRechargeRecord {
  recharge,
  rechargeRecord,
}

//辯識過我的還是我辯識過的
enum FacesMember{
  facesTargetMember, //辯識過我的
  facesFromMember //我辯識過的
}

//付款方式
enum PaymentType{
  ATM,
  CREDIT_CARD,
  CONVENIENCE_STORE,
  GOOGLE_IAP,
  APPLE_IAP,
}

extension PaymentTypeExtension on PaymentType{

  static List<String> zhs = [
    'ATM',
    '信用卡',
    '超商',
    'Google 應用程式內購',
    'Apple 應用程式內購'
  ];

  String get zh => zhs[index];
}

//拍照或掃QRCode
enum PhotographOrScanning { photograph, scanning }

//性別
enum Gender { male , female }

//星座
enum Horoscope {
  aries, //牡羊座
  taurus, //金牛座
  gemini, //雙子座
  cancer, //巨蟹座
  leo, //獅子座
  virgo, //處女座
  libra, //天秤座
  scorpio, //天蠍座
  sagittarius, //射手座
  capricorn, //摩羯座
  aquarius, //水瓶座
  pisces, //雙魚座
}

enum STOMPStatus{
  onConnect,
  onDisconnect,
}

enum MessageType{
  TEXT,
  AUDIO,
  VIDEO,
  PIC,
  VIDEO_CALL,
  VOICE_CALL,
}

enum FollowerType{
  wantAddMe,
  follower
}

enum PhotoType{
  tmp,
  mugshot,
  face,
  interactive,
  complaint,
}

enum VisitMode{
  visitModeVisit, //探班
  visitModeBeingVisit //被探班
}

enum WhoPay{
  whoPaySelf,  //己方支付
  whoPayOther,//對方支付
  all //全部
}

//抓取狀態
enum CatchStatus{
  catchStatusWaiting,     //等待中
  catchStatusComplete,    //已完成
  catchStatusProcessing,  //進行中
  catchStatusCancel,      //已取消
}

enum Catch{
  isCatch, //抓到
  beCaught,//被抓到
}

enum WhoSide{
  from,
  target,
}

enum GrowthState{
  childhood, //幼年
  growingUp, //成長
  mature, //成熟
  die, //死亡
}

enum PigButtonType{
  canSell, //能販售
  cantSell, //不能販售
  canFeeding, //能餵食
  cantFeeding, //不能餵食
  canAgain, //能重養
  cantAgain, //不能重養
}

enum DetectType{
  followFriend, //加好友
  hideAndSeek //躲貓貓
}

enum VerifyType{
  no, //未驗證
  yes,//已驗證
  reVerify,//需重新驗證
  review,//審核中
}

enum VIP{
  noVip, //不是VIP
  isVip, //是VIP
}

enum VIPSubscription{
  noRenew,
  isRenew
}
//隱藏生日
enum HideBirthday{
  no,
  yes
}
//物品項目
enum StuffItem{
  none, //無
  house, //房子
  furniture, //家具
  pets, //寵物
  dung //大便
}
//家具
enum Interior{
  wallpaper,
  dog,
  fence,
  cabinet,
  chair,
  table,
  doghouse,
  mat,
  house,
}

//刷新狀態管理
enum GameRefreshStatus{
  shopping,
  replace,
  cleaning
}

enum HideColumn{
  birthday,
  weight,
  job,
}

extension Extension on Interior {
  static List<String> labels = [
    AppImage.iconBg,
    AppImage.iconPet,
    AppImage.iconFence,
    AppImage.iconCabinet,
    AppImage.iconChair,
    AppImage.iconTable,
    AppImage.iconDogHouse,
    AppImage.iconMat
  ];
  static List<String> selectLabels = [
    AppImage.iconBgSelected,
    AppImage.iconPetSelected,
    AppImage.iconFenceSelected,
    AppImage.iconCabinetSelected,
    AppImage.iconChairSelected,
    AppImage.iconTableSelected,
    AppImage.iconDogHouseSelected,
    AppImage.iconMatSelected
  ];

  String get label => labels[index];
  String get selectLabel => selectLabels[index];
}

enum StoreOrderType{
  NONE,
  ADD_POINTS
}

extension StoreOrderTypeExtension on StoreOrderType{

  static List<String> zhs = [
    '無',
    '儲值點數'
  ];

  String get zh => zhs[index];
}

