import 'dart:convert';

import 'member_model.dart';

class FacesDetectHistoryModel{
  int? id;
  int? fromMemberId;
  String? fromMemberName;
  String? fromMemberNickname;
  int? fromMemberGender;
  int? targetMemberId;
  String? targetMemberName;
  String? targetMemberNickname;
  int? targetMemberGender;
  double? latitude;
  double? longitude;
  String? address;
  int? detectType;
  int? status;
  double? createdAt;
  double? updatedAt;
  MemberModel? fromMember;
  MemberModel? targetMember;

  FacesDetectHistoryModel.fromJson(Map<String, dynamic> json){
    id = json['id'];
    fromMemberId = json['fromMemberId'];
    fromMemberName = json['fromMemberName'];
    fromMemberNickname = json['fromMemberNickname'];
    fromMemberGender = json['fromMemberGender'];
    targetMemberId = json['targetMemberId'];
    targetMemberName = json['targetMemberName'];
    targetMemberNickname = json['targetMemberNickname'];
    targetMemberGender = json['targetMemberGender'];
    latitude = double.tryParse(json['latitude'].toString()) ?? 0.0;
    longitude = double.tryParse(json['longitude'].toString()) ?? 0.0;
    address = json['address'];
    detectType = json['detectType'];
    status = json['status'];
    createdAt = double.tryParse(json['createdAt'].toString()) ?? 0.0;
    updatedAt = double.tryParse(json['updatedAt'].toString()) ?? 0.0;
    fromMember = MemberModel.fromJson(jsonDecode(jsonEncode(json['fromMember'])));
    targetMember = MemberModel.fromJson(jsonDecode(jsonEncode(json['targetMember'])));
  }

  Map<String, dynamic> toJson() => {
    'id':id,
    'fromMemberId':fromMemberId,
    'fromMemberName':fromMemberName,
    'fromMemberNickname':fromMemberNickname,
    'fromMemberGender':fromMemberGender,
    'targetMemberId':targetMemberId,
    'targetMemberName':targetMemberName,
    'targetMemberNickname':targetMemberNickname,
    'targetMemberGender':targetMemberGender,
    'latitude':latitude,
    'longitude':longitude,
    'address':address,
    'detectType':detectType,
    'status':status,
    'createdAt':createdAt,
    'updatedAt':updatedAt,
    'fromMember': fromMember!.toJson(),
    'targetMember': targetMember!.toJson()
  };
}
class AddFacesDetectHistoryModel{
  int? fromMemberId;
  String? fromMemberName;
  String? fromMemberNickname;
  int? fromMemberGender;
  int? targetMemberId;
  String? targetMemberName;
  String? targetMemberNickname;
  int? targetMemberGender;
  double? latitude;
  double? longitude;
  String? address;
  int? detectType;

  AddFacesDetectHistoryModel({
    required this.fromMemberId,
    required this.fromMemberName,
    required this.fromMemberNickname,
    required this.fromMemberGender,
    required this.targetMemberId,
    required this.targetMemberName,
    required this.targetMemberNickname,
    required this.targetMemberGender,
    required this.latitude,
    required this.longitude,
    required this.address,
    required this.detectType
  });

  Map<String, dynamic> toJson() => {
    'fromMemberId':fromMemberId,
    'fromMemberName':fromMemberName,
    'fromMemberNickname':fromMemberNickname,
    'fromMemberGender':fromMemberGender,
    'targetMemberId':targetMemberId,
    'targetMemberName':targetMemberName,
    'targetMemberNickname':targetMemberNickname,
    'targetMemberGender':targetMemberGender,
    'latitude':latitude,
    'longitude':longitude,
    'address':address,
    'detectType':detectType
  };
}