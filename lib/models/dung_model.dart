class DungModel{
  int? id;
  String? name;
  int? animalType;
  int? level;
  String? color;
  int? cost;
  String? pic;
  int? status;
  double? createdAt;
  double? updateAt;
  bool? isBuy;
  bool? isUsed;
  bool? memberDefault;

  DungModel.fromJson(Map<String, dynamic> json){
    id = json['id'];
    name = json['name'];
    animalType = json['animalType'];
    level = json['level'];
    color = json['color'];
    cost = json['cost'];
    pic = json['pic'];
    status = json['status'];
    createdAt = double.tryParse(json['createdAt'].toString()) ?? 0.0;
    updateAt = double.tryParse(json['updateAt'].toString()) ?? 0.0;
    isBuy = false;
    isUsed = false;
    memberDefault = json['memberDefault'];
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'name': name,
    'animalType': animalType,
    'level': level,
    'color': color,
    'cost': cost,
    'pic': pic,
    'status': status,
    'createdAt': createdAt,
    'updateAt': updateAt,
    'isBuy': isBuy,
    'isUsed': isUsed,
    'memberDefault':memberDefault
  };
}

class ClearOneMemberHouseDungDTO{
  int memberCleaningToolsId;
  int memberId;
  int memberHouseId;
  int memberHouseDungId;

  ClearOneMemberHouseDungDTO({
    required this.memberCleaningToolsId,
    required this.memberId,
    required this.memberHouseId,
    required this.memberHouseDungId
  });

  Map<String, dynamic> toJson() => {
    'memberCleaningToolsId': memberCleaningToolsId,
    'memberId':memberId,
    'memberHouseId':memberHouseId,
    'memberHouseDungId':memberHouseDungId,
  };
}