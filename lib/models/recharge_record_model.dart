class RechargeRecordModel {
  int? id;
  //加值方案
  int? topUpPlanId;
  //會員ID
  int? memberId;
  //交易金額
  int? amount;
  //點數
  int? points;
  //原本的點數
  int? beforePoints;
  //加值後的點數
  int? afterPoints;
  //訂單編號
  String? orderNumber;
  //付款狀態
  bool? payment;
  //付款時間
  int? paymentAt;
  //付款方式
  int? paymentType;
  //狀態
  int? status;
  //建立日期
  dynamic createdAt;
  //更新時間
  dynamic updatedAt;

  RechargeRecordModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    topUpPlanId = json['topUpPlanId'];
    memberId = json['memberId'];
    amount = json['amount'];
    points = json['points'];
    beforePoints = json['beforePoints'];
    afterPoints = json['afterPoints'];
    orderNumber = json['orderNumber'];
    payment = json['payment'];
    paymentAt = json['paymentAt'];
    paymentType = json['paymentType'];
    status = json['status'];
    createdAt = json['createdAt'].runtimeType == double ? json['createdAt'] : 0.0;
    updatedAt = json['updatedAt'].runtimeType == double ? json['updatedAt'] : 0.0;
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'topUpPlanId': topUpPlanId,
        'memberId': memberId,
        'amount': amount,
        'points': points,
        'beforePoints': beforePoints,
        'afterPoints': afterPoints,
        'orderNumber': orderNumber,
        'payment': payment,
        'paymentAt': paymentAt,
        'paymentType': paymentType,
        'status': status,
        'createdAt': createdAt,
        'updatedAt': updatedAt,
      };
}

class AddPointsHistoryDTO {
  int amount;
  int memberId;
  int paymentType;
  int points;
  int topUpPlanId;

  AddPointsHistoryDTO(
      {required this.amount,
      required this.memberId,
      required this.paymentType,
      required this.points,
      required this.topUpPlanId});

  Map<String, dynamic> toJson() => {
        'amount': amount,
        'memberId': memberId,
        'paymentType': paymentType,
        'points': points,
        'topUpPlanId': topUpPlanId
      };
}

class TopUpPlanModel {
  int? id;
  String? name;
  String? iapProductId;
  int? amount;
  int? point;
  int? status;
  double? createdAt;
  double? updatedAt;

  TopUpPlanModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    iapProductId = json['iapProductId'];
    amount = json['amount'];
    point = json['point'];
    status = json['status'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'iapProductId': iapProductId,
        'amount': amount,
        'point': point,
        'status': status,
        'createdAt': createdAt,
        'updatedAt': updatedAt
      };
}

class YauYangPaymentReqDTO{
  String custOrderNo;
  int memberId;
  String? custId;
  String orderAmount;
  String orderDetail;
  String productName;
  String payerName;
  String payerAddress;
  String payerMobile;
  String? payerEmail;
  String successURL;

  YauYangPaymentReqDTO({
    required this.custOrderNo,
    required this.memberId,
    required this.custId,
    required this.orderAmount,
    required this.orderDetail,
    required this.productName,
    required this.payerName,
    required this.payerAddress,
    required this.payerMobile,
    required this.payerEmail,
    required this.successURL
  });

  Map<String, dynamic> toJson() => {
    'cust_order_no': custOrderNo,
    'member_id': memberId,
    'cust_id': custId,
    'order_amount': orderAmount,
    'order_detail': orderDetail,
    'product_name': productName,
    'payer_name': payerName,
    'payer_address': payerAddress,
    'payer_mobile': payerMobile,
    'payer_email': payerEmail,
    'success_url': successURL,
  };
}

class PaymentResDTO{
  String? orderNumber;
  String? url;
  PaymentResDTO.fromJson(Map<String, dynamic> json){
    orderNumber = json['orderNumber'];
    url = json['url'];
  }
}
