class FrameHeadersModel{
 String? destination;
 String? contentType;
 String? subscription;
 String? messageId;
 int? contentLength;

 FrameHeadersModel.fromJson(Map<String, dynamic> json){
   destination = json['destination'];
   contentType = json['content-type'];
   subscription = json['subscription'];
   messageId = json['message-id'];
   contentLength = json['content-length'];
 }

 Map<String, dynamic> toJson() => {
   'destination': destination,
   'contentType': contentType,
   'subscription':subscription,
   'messageId':messageId,
   'contentLength':contentLength
 };
}