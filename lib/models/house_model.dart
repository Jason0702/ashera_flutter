class HouseModel{
  int? id;
  String? name;
  double? height;
  double? weight;
  int? level;
  String? color;
  int? cost;
  String? pic;
  int? status;
  double? createdAt;
  double? updatedAt;
  bool? isBuy;
  bool? isUsed;

  HouseModel.fromJson(Map<String, dynamic> json){
    id = json['id'];
    name = json['name'];
    height = double.tryParse(json['height'].toString()) ?? 0.0;
    weight = double.tryParse(json['weight'].toString()) ?? 0.0;
    level = json['level'];
    color = json['color'];
    cost = json['cost'];
    pic = json['pic'];
    status = json['status'];
    createdAt = double.tryParse(json['createdAt'].toString()) ?? 0.0;
    updatedAt = double.tryParse(json['updatedAt'].toString()) ?? 0.0;
    isBuy = false;
    isUsed = false;
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'name': name,
    'height': height,
    'weight': weight,
    'level': level,
    'color': color,
    'cost': cost,
    'pic': pic,
    'status': status,
    'createdAt': createdAt,
    'updatedAt': updatedAt,
    'isBuy': isBuy,
    'isUsed': isUsed
  };
}