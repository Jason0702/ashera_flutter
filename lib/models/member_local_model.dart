class MemberLocalModel{
  int? id;
  String? nickname;
  int? gender;
  String? birthday;
  double? latitude;
  double? longitude;
  int? hideStatus;
  int? hideType;
  String? interactivePic;
  bool? online;

  MemberLocalModel.fromJson(Map<String, dynamic> json){
    id = json['id'];
    nickname = json['nickname'];
    gender = json['gender'];
    birthday = json['birthday'];
    latitude = json['latitude'];
    longitude = json['longitude'];
    hideStatus = json['hideStatus'];
    hideType = json['hideType'];
    interactivePic = json['interactivePic'];
    online = json['online'];
  }

  Map<String, dynamic> toJson() => {
    'id':id,
    'nickname':nickname,
    'gender': gender,
    'birthday': birthday,
    'latitude':latitude,
    'longitude':longitude,
    'hideStatus':hideStatus,
    'hideType':hideType,
    'interactivePic':interactivePic
  };
}