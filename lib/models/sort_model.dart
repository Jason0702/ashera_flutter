class SortModel{
  bool? sorted;
  bool? unsorted;
  bool? empty;

  SortModel.fromJson(Map<String, dynamic> json){
    sorted = json['sorted'];
    unsorted = json['unsorted'];
    empty = json['empty'];
  }

  Map<String, dynamic> toJson() => {
    'sorted':sorted,
    'unsorted':unsorted,
    'empty':empty
  };
}