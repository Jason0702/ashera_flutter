class UpdateMemberPetsHpDTO{
  int memberPetsId;
  int hp;

  UpdateMemberPetsHpDTO({
    required this.memberPetsId,
    required this.hp
  });

  Map<String, dynamic> toJson() => {
    'memberPetsId':memberPetsId,
    'hp':hp
  };
}
class UpdateMemberHouseDTO{
  String name;
  int memberId;
  int houseId;
  int status;

  UpdateMemberHouseDTO({
    required this.name,
    required this.memberId,
    required this.houseId,
    required this.status
  });

  Map<String, dynamic> toJson() => {
    'name':name,
    'memberId': memberId,
    'houseId':houseId,
    'status':status
  };
}

class UpdateMemberHouseStyleDTO{
  String name;
  int memberId;
  int houseStyleId;
  int status;

  UpdateMemberHouseStyleDTO({
    required this.name,
    required this.memberId,
    required this.houseStyleId,
    required this.status
  });

  Map<String, dynamic> toJson() => {
    'name':name,
    'memberId': memberId,
    'houseStyleId':houseStyleId,
    'status':status
  };
}

class UpdateMemberFurnitureDTO{
  String name;
  int memberId;
  int furnitureId;
  int status;

  UpdateMemberFurnitureDTO({
    required this.name,
    required this.memberId,
    required this.furnitureId,
    required this.status
  });

  Map<String, dynamic> toJson() => {
    'name': name,
    'memberId': memberId,
    'furnitureId': furnitureId,
    'status':status
  };
}

class UpdateMemberPetsDTO{
  String name;
  int animalType;
  int memberId;
  int petsId;
  int status;

  UpdateMemberPetsDTO({
    required this.name,
    required this.animalType,
    required this.memberId,
    required this.petsId,
    required this.status
  });

  Map<String, dynamic> toJson() => {
    'name':name,
    'animalType': animalType,
    'memberId': memberId,
    'petsId': petsId,
    'status':status
  };
}

class ChangeItemDTO{
  int fromItemId;
  int targetItemId;
  int posTop;
  int posLeft;

  ChangeItemDTO({
    required this.fromItemId,
    required this.targetItemId,
    required this.posTop,
    required this.posLeft
  });

  Map<String, dynamic> toJson() => {
    'fromItemId': fromItemId,
    'targetItemId': targetItemId,
    'posTop': posTop,
    'posLeft': posLeft,
  };
}

class ChangeStyleDTO{
  int fromStyleId;
  int targetStyleId;
  int useToId;

  ChangeStyleDTO({
    required this.fromStyleId,
    required this.targetStyleId,
    required this.useToId
  });

  Map<String, dynamic> toJson() => {
    'fromStyleId': fromStyleId,
    'targetStyleId': targetStyleId,
    'useToId': useToId,
  };
}

class UseItemDTO{
  int id;
  int qty;
  int posTop;
  int posLeft;

  UseItemDTO({
    required this.id,
    required this.qty,
    required this.posTop,
    required this.posLeft,
  });

  Map<String, dynamic> toJson() => {
    'id': id,
    'qty': qty,
    'posTop': posTop,
    'posLeft':posLeft,
  };
}

class UseItemDTOForMemberHouse {
  int id;
  int qty;
  int posTop;
  int posLeft;
  int memberHouseId;

  UseItemDTOForMemberHouse({
    required this.id,
    required this.qty,
    required this.posTop,
    required this.posLeft,
    required this.memberHouseId
  });

  Map<String, dynamic> toJson() => {
    'id': id,
    'qty': qty,
    'posTop': posTop,
    'posLeft':posLeft,
    'memberHouseId': memberHouseId
  };
}

class AddMemberItemDTO {
  int memberId;
  int itemId;
  int qty;

  AddMemberItemDTO({
    required this.memberId,
    required this.itemId,
    required this.qty
  });

  Map<String, dynamic> toJson() => {
    'memberId': memberId,
    'itemId': itemId,
    'qty': qty
  };
}