import 'package:stomp_dart_client/stomp_frame.dart';

class VisitEventModel{
  String title;
  StompFrame? frame;

  VisitEventModel(this.title, this.frame);
}

class RechargeEventModel{
  String title;
  StompFrame? frame;

  RechargeEventModel(this.title, this.frame);
}