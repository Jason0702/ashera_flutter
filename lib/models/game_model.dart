import 'dart:convert';

import 'member_model.dart';

class MemberHouseMessageDTO {
  int? memberHouseId;
  int? memberId;
  String? message;

  MemberHouseMessageDTO({
    required this.memberHouseId,
    required this.memberId,
    required this.message,
  });

  Map<String, dynamic> toJson() => {
        'memberHouseId': memberHouseId,
        'memberId': memberId,
        'message': message,
      };
}

class MemberHouseMessageModel {
  int? id;
  int? memberHouseId;
  int? memberId;
  String? message;
  int? checked;
  int? status;
  double? createdAt;
  double? updatedAt;

  MemberHouseMessageModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    memberHouseId = json['memberHouseId'];
    memberId = json['memberId'];
    message = json['message'];
    checked = json['checked'] == true ? 1 : 0;
    status = json['status'];
    createdAt = double.tryParse(json['createdAt'].toString()) ?? 0.0;
    updatedAt = double.tryParse(json['updatedAt'].toString()) ?? 0.0;
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'memberHouseId': memberHouseId,
        'memberId': memberId,
        'message': message,
        'checked': checked,
        'status': status,
        'createdAt': createdAt,
        'updatedAt': updatedAt
      };
}

class MemberHouseDungModel {
  int? id;
  int? memberDungId;
  int? dungId;
  int? memberHouseId;
  int? memberHouseMessageId;
  int? memberId;
  int? status;
  double? createdAt;
  double? updatedAt;
  bool? clean;
  int? posLeft;
  int? posTop;
  MemberModel? member;

  MemberHouseDungModel({
    required this.memberDungId,
    required this.dungId,
    required this.memberHouseMessageId,
    required this.memberHouseId,
    required this.memberId,
    required this.posLeft,
    required this.posTop,
  });

  MemberHouseDungModel.fromJson(Map<String, dynamic> resp) {
    id = resp['id'];
    memberDungId = resp['memberDungId'];
    dungId = resp['dungId'];
    memberHouseId = resp['memberHouseId'];
    memberHouseMessageId = resp['memberHouseMessageId'];
    memberId = resp['memberId'];
    status = resp['status'];
    createdAt = double.tryParse(resp['createdAt'].toString()) ?? 0.0;
    updatedAt = double.tryParse(resp['updatedAt'].toString()) ?? 0.0;
    clean = resp['clean'];
    posLeft = resp['posLeft'];
    posTop = resp['posTop'];
    member = MemberModel.fromJson(json.decode(json.encode(resp['member'])));
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'memberDungId': memberDungId,
        'dungId': dungId,
        'memberHouseId': memberHouseId,
        'memberHouseMessageId': memberHouseMessageId,
        'memberId': memberId,
        'status': status,
        'createdAt': createdAt,
        'updatedAt': updatedAt,
        'clean': clean,
        'posLeft': posLeft,
        'posTop': posTop,
        'member': member?.toJson()
      };
}

class MemberFurnitureModel {
  int? id;
  String? name;
  int? memberId;
  int? furnitureId;
  bool? used;
  int? status;
  double? createdAt;
  double? updatedAt;

  MemberFurnitureModel.formJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    memberId = json['memberId'];
    furnitureId = json['furnitureId'];
    used = json['used'];
    status = json['status'];
    createdAt = double.tryParse(json['createdAt'].toString()) ?? 0.0;
    updatedAt = double.tryParse(json['updatedAt'].toString()) ?? 0.0;
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'memberId': memberId,
        'furnitureId': furnitureId,
        'used': used,
        'status': status,
        'createdAt': createdAt,
        'updatedAt': updatedAt
      };
}

class MemberPetsModel {
  int? id;
  int? memberId;
  int? animalType;
  int? petsId;
  String? name;
  int? level;
  int? hp;
  int? curHp;
  bool? used;
  int? status;
  double? startAt;
  double? lastEatAt;
  double? createdAt;
  double? updatedAt;

  MemberPetsModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    memberId = json['memberId'];
    animalType = json['animalType'];
    petsId = json['petsId'];
    name = json['name'];
    level = json['level'];
    hp = json['hp'];
    curHp = json['curHp'];
    used = json['used'];
    status = json['status'];
    startAt = double.tryParse(json['startAt'].toString()) ?? 0.0;
    lastEatAt = double.tryParse(json['lastEatAt'].toString()) ?? 0.0;
    createdAt = double.tryParse(json['createdAt'].toString()) ?? 0.0;
    updatedAt = double.tryParse(json['updatedAt'].toString()) ?? 0.0;
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'memberId': memberId,
        'animalType': animalType,
        'petsId': petsId,
        'name': name,
        'level': level,
        'hp': hp,
        'curHp': curHp,
        'used': used,
        'status': status,
        'startAt': startAt,
        'lastEatAt': lastEatAt,
        'createdAt': createdAt,
        'updatedAt': updatedAt,
      };
}

class MemberHouseModel {
  int? id;
  String? name;
  int? memberId;
  int? houseId;
  int? memberHouseStyleId;
  int? houseStyleId;
  bool? used;
  int? status;
  double? createdAt;
  double? updatedAt;

  MemberHouseModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    memberId = json['memberId'];
    houseId = json['houseId'];
    memberHouseStyleId = json['memberHouseStyleId'];
    houseStyleId = json['houseStyleId'];
    used = json['used'];
    status = json['status'];
    createdAt = double.tryParse(json['createdAt'].toString()) ?? 0.0;
    updatedAt = double.tryParse(json['updatedAt'].toString()) ?? 0.0;
  }
  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'memberId': memberId,
        'houseId': houseId,
        'memberHouseStyleId': memberHouseStyleId,
        'houseStyleId':houseStyleId,
        'used': used,
        'status': status,
        'createdAt': createdAt,
        'updatedAt': updatedAt,
      };
}

class MemberHouseStyleModel{
  int? id;
  String? name;
  int? memberId;
  int? houseStyleId;
  bool? used;
  int? status;
  double? createdAt;
  double? updatedAt;

  MemberHouseStyleModel.fromJson(Map<String, dynamic> json){
    id = json['id'];
    name = json['name'];
    memberId = json['memberId'];
    houseStyleId = json['houseStyleId'];
    used = json['used'];
    status = json['status'];
    createdAt = double.tryParse(json['createdAt'].toString()) ?? 0.0;
    updatedAt = double.tryParse(json['updatedAt'].toString()) ?? 0.0;
  }
  Map<String, dynamic> toJson() => {
    'id': id,
    'name': name,
    'memberId': memberId,
    'houseStyleId': houseStyleId,
    'used': used,
    'status': status,
    'createdAt': createdAt,
    'updatedAt': updatedAt,
  };
}

class MemberCleaningToolsModel {
  int? id;
  int? memberId;
  String? name;
  int? cleaningToolsId;
  int? status;
  double? createdAt;
  double? updatedAt;
  bool? memberDefault;
  bool? used;
  int? usedAt;

  MemberCleaningToolsModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    memberId = json['memberId'];
    name = json['name'];
    cleaningToolsId = json['cleaningToolsId'];
    status = json['status'];
    createdAt = double.tryParse(json['createdAt'].toString()) ?? 0.0;
    updatedAt = double.tryParse(json['updatedAt'].toString()) ?? 0.0;
    memberDefault = json['memberDefault'];
    used = json['used'];
    usedAt = json['usedAt'];
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'memberId': memberId,
        'name': name,
        'cleaningToolsId': cleaningToolsId,
        'status': status,
        'createdAt': createdAt,
        'updatedAt': updatedAt,
        'memberDefault': memberDefault,
        'used': used,
        'usedAt': usedAt
      };
}

class CleaningToolsModel {
  int? id;
  String? name;
  int? level;
  int? cost;
  String? pic;
  bool? memberDefault;
  int? salesPoints;
  int? status;
  double? createdAt;
  double? updatedAt;

  CleaningToolsModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    name = json['name'];
    level = json['level'];
    cost = json['cost'];
    pic = json['pic'];
    memberDefault = json['memberDefault'];
    salesPoints = json['salesPoints'];
    status = json['status'];
    createdAt = double.tryParse(json['createdAt'].toString()) ?? 0.0;
    updatedAt = double.tryParse(json['updatedAt'].toString()) ?? 0.0;
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'level': level,
        'cost': cost,
        'pic': pic,
        'memberDefault': memberDefault,
        'salesPoints': salesPoints,
        'status': status,
        'createdAt': createdAt,
        'updatedAt': updatedAt
      };
}

class CleaningToolsShoppingHistoryModel {
  int? id;
  int? memberId;
  int? cleaningToolsId;
  String? cleaningToolsName;
  int? amount;
  int? status;
  double? createdAt;
  double? updatedAt;

  CleaningToolsShoppingHistoryModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    memberId = json['memberId'];
    cleaningToolsId = json['cleaningToolsId'];
    cleaningToolsName = json['cleaningToolsName'];
    amount = json['amount'];
    status = json['status'];
    createdAt = double.tryParse(json['createdAt'].toString()) ?? 0.0;
    updatedAt = double.tryParse(json['updatedAt'].toString()) ?? 0.0;
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'memberId': memberId,
        'cleaningToolsId': cleaningToolsId,
        'cleaningToolsName': cleaningToolsName,
        'amount': amount,
        'status': status,
        'createdAt': createdAt,
        'updatedAt': updatedAt
      };
}

class MemberDungModel {
  int? id;
  int? memberId;
  int? dungId;
  int? cost;
  int? posLeft;
  int? posTop;
  int? status;
  double? createdAt;
  double? updatedAt;
  bool? used;
  bool? memberDefault;

  MemberDungModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    memberId = json['memberId'];
    dungId = json['dungId'];
    cost = json['cost'];
    posLeft = json['posLeft'];
    posTop = json['posTop'];
    status = json['status'];
    createdAt = double.tryParse(json['createdAt'].toString()) ?? 0.0;
    updatedAt = double.tryParse(json['updatedAt'].toString()) ?? 0.0;
    used = json['used'];
    memberDefault = json['memberDefault'];
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'memberId': memberId,
        'dungId': dungId,
        'cost': cost,
        'posLeft': posLeft,
        'posTop': posTop,
        'status': status,
        'createdAt': createdAt,
        'updatedAt': updatedAt,
        'used': used,
        'memberDefault': memberDefault
      };
}

class MemberGameSettingModel {
  int? id;
  int? memberId;
  int? olderThenAge;
  int? youngerThenAge;
  int? filterDistance;
  int? filterGender;
  int? status;
  double? createdAt;
  double? updatedAt;

  MemberGameSettingModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    memberId = json['memberId'];
    olderThenAge = json['olderThenAge'];
    youngerThenAge = json['youngerThenAge'];
    filterDistance = json['filterDistance'];
    filterGender = json['filterGender'];
    status = json['status'];
    createdAt = double.tryParse(json['createdAt'].toString()) ?? 0.0;
    updatedAt = double.tryParse(json['updatedAt'].toString()) ?? 0.0;
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'memberId': memberId,
        'olderThenAge': olderThenAge,
        'youngerThenAge': youngerThenAge,
        'filterDistance': filterDistance,
        'filterGender': filterGender,
        'status': status,
        'createdAt': createdAt,
        'updatedAt': updatedAt
      };
}
class PetsSellRecordModel{
  int? id;
  int? memberId;
  int? memberPetsId;
  int? getPoints;
  int? points;
  int? pointsDeducted;
  int? status;
  int? startAt;
  int? endAt;
  double? updatedAt;
  double? createdAt;

  PetsSellRecordModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    memberId = json['memberId'];
    memberPetsId = json['memberPetsId'];
    getPoints = json['getPoints'];
    points = json['points'];
    pointsDeducted = json['pointsDeducted'];
    status = json['status'];
    startAt = json['startAt'];
    endAt = json['endAt'];
    createdAt = double.tryParse(json['createdAt'].toString()) ?? 0.0;
    updatedAt = double.tryParse(json['updatedAt'].toString()) ?? 0.0;
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'memberId': memberId,
    'memberPetsId': memberPetsId,
    'getPoints': getPoints,
    'points': points,
    'pointsDeducted': pointsDeducted,
    'status': status,
    'startAt': startAt,
    'endAt': endAt,
    'updatedAt': updatedAt,
    'createdAt': createdAt
  };
}