import 'dart:convert';
import 'dart:developer';

import 'package:ashera_flutter/enum/app_enum.dart';

class FaceImageModel {
  int? id;
  String? name;
  String? nickname;
  int? gender;
  double? height;
  double? weight;
  String? birthday;
  String? mugshot;
  String? zodiacSign;
  String? bloodType;
  String? aboutMe;
  String? aboutMe2;
  String? job;
  String? asheraUid;
  double? latitude;
  double? longitude;
  int? verify;
  bool? hideBirthday;
  bool? hideWeight;
  bool? hideJob;
  double? confidence;
  String? interactivePic;

  FaceImageModel.fromJson(Map<String, dynamic> response) {
    id = response['id'];
    name = response['name'];
    nickname = response['nickname'];
    gender = response['gender'];
    height = double.tryParse(response['height'].toString()) ?? 0.0;
    weight = double.tryParse(response['weight'].toString()) ?? 0.0;
    birthday = response['birthday'];
    mugshot = response['mugshot'] ?? '';
    zodiacSign = response['zodiacSign'];
    bloodType = response['bloodType'];
    aboutMe = response['aboutMe'] ?? '';
    aboutMe2 = response['aboutMe2'] ?? '';
    job = response['job'];
    asheraUid = response['asheraUid'];
    latitude = double.tryParse(response['latitude'].toString()) ?? 0.0;
    longitude = double.tryParse(response['longitude'].toString()) ?? 0.0;
    verify = response['verify'];
    List hideColumn = response['hideColumn'] != null && response['hideColumn'].toString().trim().isNotEmpty ? json.decode(response['hideColumn']) : [];
    hideBirthday = hideColumn.isNotEmpty ? List<String>.from(hideColumn).where((element) => element == HideColumn.birthday.name).isNotEmpty ? true : false : false;
    hideWeight = hideColumn.isNotEmpty ? List<String>.from(hideColumn).where((element) => element == HideColumn.weight.name).isNotEmpty ? true : false : false;
    hideJob = hideColumn.isNotEmpty ? List<String>.from(hideColumn).where((element) => element == HideColumn.job.name).isNotEmpty ? true : false : false;
    confidence = double.tryParse(response['confidence'].toString()) ?? 0.0;
    interactivePic = response['interactivePic'] ?? '';
  }

  Map<String, dynamic> toJson() => {
        'id':id,
        'name': name,
        'nickname': nickname,
        'gender': gender,
        'height': height,
        'weight': weight,
        'birthday': birthday,
        'mugshot': mugshot,
        'zodiacSign': zodiacSign,
        'bloodType': bloodType,
        'aboutMe': aboutMe,
        'aboutMe2': aboutMe2,
        'job': job,
        'asheraUid': asheraUid,
        'latitude': latitude,
        'longitude': longitude,
        'verify': verify,
        'hideBirthday': hideBirthday,
        'hideWeight': hideWeight,
        'hideJob': hideJob,
        'confidence': confidence,
        'interactivePic': interactivePic
      };
}
