class MemberFriendNameModel{
  int? id;
  int? fromMemberId;
  int? targetMemberId;
  String? targetMemberName;
  int? status;
  double? createdAt;
  double? updatedAt;

  MemberFriendNameModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    fromMemberId = json['fromMemberId'];
    targetMemberId = json['targetMemberId'];
    targetMemberName = json['targetMemberName'];
    status = json['status'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'fromMemberId': fromMemberId,
    'targetMemberId': targetMemberId,
    'targetMemberName': targetMemberName,
    'status': status,
    'createdAt': createdAt,
    'updatedAt': updatedAt
  };

}

class MemberFriendNameDTO{
  int? id;
  int fromMemberId;
  int targetMemberId;
  String targetMemberName;

  MemberFriendNameDTO({
    required this.id,
    required this.fromMemberId,
    required this.targetMemberId,
    required this.targetMemberName
  });

  Map<String, dynamic> toJson() => {
    'id':id,
    'fromMemberId':fromMemberId,
    'targetMemberId':targetMemberId,
    'targetMemberName':targetMemberName
  };
}