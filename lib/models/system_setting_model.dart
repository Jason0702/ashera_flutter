class SystemSettingModel{
  int? id;
  String? vipMemberInstructions;
  ///隱私政策
  String? privacyPolicy;
  ///VIP費用
  int? vipPointsPerMonth;
  ///顯示VIP選項
  bool? showVip;
  ///解鎖大頭照天數
  int? unlockMugshotExpireDay;
  ///解鎖大頭照費用
  int? unlockMugshotPoints;
  ///充值按鈕顯示開關
  int? addPointsStatus;
  ///appleStoreAppUrl
  String? appleStoreAppUrl;
  ///playStoreAppUrl
  String? playStoreAppUrl;
  ///appVersionNumber
  String? appVersionNumber;

  SystemSettingModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    vipMemberInstructions = json['vipMemberInstructions'] ?? '';
    privacyPolicy = json['privacyPolicy'] ?? '';
    vipPointsPerMonth = json['vipPointsPerMonth'] ?? 0;
    showVip = json['showVip'];
    unlockMugshotExpireDay = json['unlockMugshotExpireDay'] ?? 0;
    unlockMugshotPoints = json['unlockMugshotPoints'] ?? 0;
    addPointsStatus = json['addPointsStatus'] ?? '';
    appleStoreAppUrl = json['appleStoreAppUrl'] ?? '';
    playStoreAppUrl = json['playStoreAppUrl'] ?? '';
    appVersionNumber = json['appVersionNumber'] ?? '';
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'vipMemberInstructions': vipMemberInstructions,
    'privacyPolicy': privacyPolicy,
    'vipPointsPerMonth': vipPointsPerMonth,
    'unlockMugshotPoints': unlockMugshotPoints,
    'addPointsStatus': addPointsStatus,
    'appleStoreAppUrl': appleStoreAppUrl,
    'playStoreAppUrl': playStoreAppUrl,
    'appVersionNumber': appVersionNumber,
    'unlockMugshotExpireDay': unlockMugshotExpireDay,
    'showVip': showVip
  };
}