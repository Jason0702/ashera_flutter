class MemberPointModel{
  int? id;
  int? memberId;
  int? point;
  int? status;
  int? cleanTimes;
  int? shitTimes;
  double? createdAt;
  double? updatedAt;

  MemberPointModel.fromJson(Map<String, dynamic> json){
    id = json['id'];
    memberId = json['memberId'];
    point = json['point'] ?? 0;
    status = json['status'];
    cleanTimes = json['cleanTimes'];
    shitTimes = json['shitTimes'];
    createdAt = double.tryParse(json['createdAt'].toString()) ?? 0.0;
    updatedAt = double.tryParse(json['updatedAt'].toString()) ?? 0.0;
  }

  Map<String, dynamic> toJson() => {
    'id':id,
    'memberId':memberId,
    'point':point,
    'cleanTimes': cleanTimes,
    'shitTimes': shitTimes,
    'status':status,
    'createdAt':createdAt,
    'updatedAt':updatedAt
  };
}