class MessageLastRecordModel{
  int? id;
  String? fromMember;
  String? targetMember;
  String? content;
  int? type;
  int? unreadCnt;
  int? status;
  String? createdAt;
  String? updatedAt;
  bool? block;

  MessageLastRecordModel.fromJson(Map<String, dynamic> json){
    id = json['id'];
    fromMember = json['fromMember'];
    targetMember = json['targetMember'];
    content = json['content'];
    type = json['type'];
    unreadCnt = json['unreadCnt'];
    status = json['status'];
    createdAt = json['createdAt'];
    updatedAt = json['updatedAt'];
    block = json['block'];
  }

  Map<String, dynamic> toJson() => {
    'id':id,
    'fromMember':fromMember,
    'targetMember':targetMember,
    'content':content,
    'type':type,
    'unreadCnt':unreadCnt,
    'status':status,
    'createdAt':createdAt,
    'updatedAt':updatedAt,
    'block':block
  };
}

class VisitMessageLastRecordModel extends MessageLastRecordModel{
  int? visitRecordId;
  VisitMessageLastRecordModel.fromJson(Map<String, dynamic> json) : super.fromJson(json){
    visitRecordId = json['visitRecordId'];
  }


  @override
  Map<String, dynamic> toJson() => {
    'id':id,
    'visitRecordId': visitRecordId,
    'fromMember':fromMember,
    'targetMember':targetMember,
    'content':content,
    'type':type,
    'unreadCnt':unreadCnt,
    'status':status,
    'createdAt':createdAt,
    'updatedAt':updatedAt,
    'block':block
  };
}