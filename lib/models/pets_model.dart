class PetsModel {
  int? id;
  int? animalType;
  String? name;
  int? level;
  int? hp;
  int? curHp;
  int? mp;
  int? exp;
  int? sp;
  int? cost;
  String? pic;
  String? gif;
  int? status;
  double? createdAt;
  double? updatedAt;
  bool? isBuy;
  bool? isUsed;
  int? growthDays1;
  int? growthDays2;
  int? salesPoints;

  PetsModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    animalType = json['animalType'];
    name = json['name'];
    level = json['level'];
    hp = json['hp'];
    curHp = json['curHp'];
    mp = json['mp'];
    exp = json['exp'];
    sp = json['sp'];
    cost = json['cost'];
    pic = json['pic'];
    gif = json['gif'];
    status = json['status'];
    createdAt = double.tryParse(json['createdAt'].toString()) ?? 0.0;
    updatedAt = double.tryParse(json['updatedAt'].toString()) ?? 0.0;
    isBuy = false;
    isUsed = false;
    growthDays1 = json['growthDays1'];
    growthDays2 = json['growthDays2'];
    salesPoints = json['salesPoints'];
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'animalType': animalType,
        'name': name,
        'level': level,
        'hp': hp,
        'curHp': curHp,
        'mp': mp,
        'exp': exp,
        'sp': sp,
        'cost': cost,
        'pic': pic,
        'gif':gif,
        'status': status,
        'createdAt': createdAt,
        'updatedAt': updatedAt,
        'isBuy': isBuy,
        'isUsed': isUsed,
        'growthDays1': growthDays1,
        'growthDays2': growthDays2,
        'salesPoints': salesPoints,
      };
}
