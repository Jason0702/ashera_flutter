class UpdateMemberLocationDTO{
  double? latitude;
  double? longitude;

  UpdateMemberLocationDTO({
    required this.latitude,
    required this.longitude
  });

  Map<String, dynamic> toJson() => {
    'latitude':latitude,
    'longitude':longitude
  };
}