class UnlockPaymentRecordModel{
  int? id;
  int? fromMemberId;
  int? targetMemberId;
  int? points;
  int? status;
  double? createdAt;
  double? updatedAt;
  int? unlockType;

  UnlockPaymentRecordModel.fromJson(Map<String, dynamic> json){
    id = json['id'];
    fromMemberId = json['fromMemberId'];
    targetMemberId = json['targetMemberId'];
    points = json['point'] ?? 0;
    status = json['status'];
    createdAt = double.tryParse(json['createdAt'].toString()) ?? 0.0;
    updatedAt = double.tryParse(json['updatedAt'].toString()) ?? 0.0;
    unlockType = json['unlockType'];
  }

  Map<String, dynamic> toJson() => {
    'id':id,
    'fromMemberId':fromMemberId,
    'targetMemberId':targetMemberId,
    'points':points,
    'status':status,
    'createdAt':createdAt,
    'updatedAt':updatedAt,
    'unlockType': unlockType,
  };
}