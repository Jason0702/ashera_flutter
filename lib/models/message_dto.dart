import 'package:flutter_font_icons/flutter_font_icons.dart';

import '../enum/app_enum.dart';

class MessageDTO{
  String? fromMember;
  String? fromMemberNickname;
  String? content;
  String? targetMember;
  String? destination;
  MessageType? messageType;

  MessageDTO({required this.fromMember, required this.fromMemberNickname, required this.content, required this.targetMember, required this.destination, required this.messageType});

  Map<String, dynamic> toJson() => {
    'fromMember':fromMember,
    'fromMemberNickname':fromMemberNickname,
    'content':content,
    'targetMember':targetMember,
    'destination':destination,
    'type': messageType?.name,
  };
}

class VisitChatMessageDTO extends MessageDTO{
  int? visitRecordId;

  VisitChatMessageDTO({
    required this.visitRecordId,
    required String? fromMember,
    required String? fromMemberNickname,
    required String? content,
    required String? targetMember,
    required String? destination,
    required MessageType? messageType
  }) : super(fromMember: fromMember, fromMemberNickname: fromMemberNickname, content: content, targetMember: targetMember, destination: destination, messageType: messageType);

  @override
  Map<String, dynamic> toJson() => {
    'visitRecordId': visitRecordId,
    'fromMember':fromMember,
    'fromMemberNickname':fromMemberNickname,
    'content':content,
    'targetMember':targetMember,
    'destination':destination,
    'type': messageType?.name,
  };
}

class GetLastVisitChatMessageDTO {
  String username;
  int visitRecordId;

  GetLastVisitChatMessageDTO({
    required this.username,
    required this.visitRecordId
  });

  Map<String, dynamic> toJson() => {
    'username': username,
    'visitRecordId': visitRecordId
  };
}