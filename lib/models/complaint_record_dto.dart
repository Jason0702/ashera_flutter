class ComplaintRecordDTO{
  int fromMemberId;
  int targetMemberId;
  String pic;
  String reason;

  ComplaintRecordDTO({
    required this.fromMemberId,
    required this.targetMemberId,
    required this.pic,
    required this.reason
  });

  Map<String, dynamic> toJson() => {
    'fromMemberId': fromMemberId,
    'targetMemberId': targetMemberId,
    'pic': pic,
    'reason': reason
  };
}