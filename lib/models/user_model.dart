class User{
  int? id;
  String? token;
  int? expiredTime;

  User({required this.id, required this.token, required this.expiredTime});

  User.fromJsonRefresh(Map<String, dynamic> json) {
    token = json['token'] ?? '';
    expiredTime = json['expiredTime'] ?? 0;
  }

  User.fromJson(Map<String, dynamic> responseData) {
    id = responseData['userId'] ?? 0;
    token = responseData['body']['token'] ?? '';
    expiredTime = responseData['body']['expiredTime'] ?? 0;
  }
}