import 'package:ashera_flutter/enum/app_enum.dart';

class AddVisitRecordDTO{
  int? fromMemberId;
  String? fromMemberName;
  int? targetMemberId;
  String? targetMemberName;
  String? address;
  double? fromLatitude;
  double? fromLongitude;
  double? endLatitude;
  double? endLongitude;
  //誰付款 0 自己 1 對方
  int? whoPay;
  //模式 0 探班 1 被探班
  int? mode = VisitMode.visitModeVisit.index;

  AddVisitRecordDTO({
    required this.fromMemberId,
    required this.fromMemberName,
    required this.targetMemberId,
    required this.targetMemberName,
    required this.address,
    required this.fromLatitude,
    required this.fromLongitude,
    required this.endLatitude,
    required this.endLongitude,
    required this.whoPay,
  });

  Map<String, dynamic> toJson() => {
    'fromMemberId': fromMemberId,
    'fromMemberName': fromMemberName,
    'targetMemberId': targetMemberId,
    'targetMemberName': targetMemberName,
    'address': address,
    'fromLatitude': fromLatitude,
    'fromLongitude': fromLongitude,
    'endLatitude': endLatitude,
    'endLongitude': endLongitude,
    'whoPay': whoPay,
    'mode': mode
  };
}

class AddBeingVisitRecordDTO{
  int? targetMemberId;
  String? targetMemberName;
  String? targetMemberNickname;
  int? targetMemberGender;
  String? targetMemberBirthday;
  //需求 至少8個字
  String? requirement;
  String? address;
  double? endLatitude;
  double? endLongitude;
  //誰付款 0 自己 1 對方
  int? whoPay;
  //模式 0 探班 1 被探班
  int? mode = VisitMode.visitModeBeingVisit.index;

  AddBeingVisitRecordDTO({
    required this.targetMemberId,
    required this.targetMemberName,
    required this.targetMemberNickname,
    required this.targetMemberGender,
    required this.targetMemberBirthday,
    required this.requirement,
    required this.address,
    required this.endLatitude,
    required this.endLongitude,
    required this.whoPay,
  });

  Map<String, dynamic> toJson() => {
    'targetMemberId': targetMemberId,
    'targetMemberName': targetMemberName,
    'targetMemberNickname':targetMemberNickname,
    'targetMemberGender':targetMemberGender,
    'targetMemberBirthday':targetMemberBirthday,
    'requirement':requirement,
    'address': address,
    'endLatitude': endLatitude,
    'endLongitude': endLongitude,
    'whoPay': whoPay,
    'mode': mode
  };
}

class RateVisitRecordDTO{
  int? id;
  int? memberId;
  String? memberName;
  //評分 最少是1 最多是5
  double? score;
  String? comment;

  RateVisitRecordDTO({
    required this.id,
    required this.memberId,
    required this.memberName,
    required this.score,
    required this.comment,
  });

  Map<String, dynamic> toJson() => {
    'id': id,
    'memberId': memberId,
    'memberName': memberName,
    'score':score,
    'comment': comment,
  };
}

class WannaVisitDTO{
  int? id;
  int? fromMemberId;
  String? fromMemberName;
  String? fromMemberNickname;
  int? fromMemberGender;
  String? fromMemberBirthday;
  double? fromLatitude;
  double? fromLongitude;

  WannaVisitDTO({
    required this.id,
    required this.fromMemberId,
    required this.fromMemberName,
    required this.fromMemberNickname,
    required this.fromMemberGender,
    required this.fromMemberBirthday,
    required this.fromLatitude,
    required this.fromLongitude
  });

  Map<String, dynamic> toJson() => {
    'id':id,
    'fromMemberId': fromMemberId,
    'fromMemberName': fromMemberName,
    'fromMemberNickname':fromMemberNickname,
    'fromMemberGender':fromMemberGender,
    'fromMemberBirthday':fromMemberBirthday,
    'fromLatitude':fromLatitude,
    'fromLongitude':fromLongitude
  };
}

class ApproveVisitRecordDTO{
  int? id;
  bool? approve;

  ApproveVisitRecordDTO({
    required this.id,
    required this.approve,
  });

  Map<String, dynamic> toJson() => {
    'id': id,
    'approve': approve
  };
}

class ChangeVisitRecordAddressDTO{
  String? changeAddress;
  double? changeEndLatitude;
  double? changeEndLongitude;
  int? targetMemberId;

  ChangeVisitRecordAddressDTO({
    required this.changeAddress,
    required this.changeEndLatitude,
    required this.changeEndLongitude,
    required this.targetMemberId,
  });

  Map<String, dynamic> toJson() => {
    'changeAddress': changeAddress,
    'changeEndLatitude': changeEndLatitude,
    'changeEndLongitude': changeEndLongitude,
    'targetMemberId': targetMemberId
  };
}