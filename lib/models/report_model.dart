class ReportModel{
  String? describe;
  bool? isCheck;

  ReportModel({
    required this.describe,
    required this.isCheck
  });

  Map<String, dynamic> toJson() => {
    'describe':describe,
    'isCheck':isCheck
  };
}