import 'package:google_maps_cluster_manager/google_maps_cluster_manager.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'member_model.dart';

class Place with ClusterItem{
  final String name;
  final MemberModel model;
  final bool isClosed;
  final LatLng latLng;

  Place({
    required this.name,
    required this.model,
    required this.latLng,
    this.isClosed = false});

  @override
  String toString() {
    return 'Place $name (close: $isClosed)';
  }

  @override
  LatLng get location => latLng;
}