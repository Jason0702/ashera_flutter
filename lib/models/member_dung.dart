
class MemberDung{
  int? id;
  int? memberId;
  int? dungId;
  int? cost;
  int? qty;
  int? createdAt;
  int? updatedAt;

  MemberDung({
    required this.id,
    required this.memberId,
    required this.dungId,
    required this.cost,
    required this.qty,
    required this.createdAt,
    required this.updatedAt
  });

  Map<String, dynamic> toJson() => {
    'id':id,
    'memberId':memberId,
    'dungId':dungId,
    'cost':cost,
    'qty':qty,
    'createdAt':createdAt,
    'updatedAt':updatedAt
  };
}