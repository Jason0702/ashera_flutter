class UseItemDTO{
  int? id;
  int? qty;

  UseItemDTO({
    required this.id,
    required this.qty
  });

  Map<String, dynamic> toJson() => {
    'id':id,
    'qty':qty
  };
}