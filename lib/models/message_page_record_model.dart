import 'dart:convert';

import 'package:ashera_flutter/models/sort_model.dart';

import 'chat_message_model.dart';
import 'pageable_model.dart';

class MessagePageRecordModel{
  List<ChatMessageModel> content = [];
  PageableModel? pageable;
  int? totalPages;
  int? totalElements;
  bool? last;
  int? number;
  SortModel? sort;
  int? size;
  int? numberOfElements;
  bool? first;
  bool? empty;

  MessagePageRecordModel.fromJson(Map<String, dynamic> data){
    List<dynamic> _list = json.decode(json.encode(data['content']));
    content = _list.map((e) => ChatMessageModel.fromJson(e)).toList();
    pageable = PageableModel.fromJson(json.decode(json.encode(data['pageable'])));
    totalPages = data['totalPages'];
    totalElements = data['totalElements'];
    last = data['last'];
    number = data['number'];
    sort = SortModel.fromJson(json.decode(json.encode(data['sort'])));
    size = data['size'];
    numberOfElements = data['numberOfElements'];
    first = data['first'];
    empty = data['empty'];
  }

  Map<String, dynamic> toJson() => {
    'content':content,
    'pageable':pageable!.toJson(),
    'totalPages':totalPages,
    'totalElements':totalElements,
    'last':last,
    'number':number,
    'sort':sort!.toJson(),
    'size':size,
    'numberOfElements':numberOfElements,
    'first':first,
    'empty':empty
  };
}