class GetChatMessageDTO{
  String? fromMember;
  String? targetMember;
  String? date;
  int? page;
  int? size;
  String? sortBy;

  GetChatMessageDTO({
    required this.fromMember,
    required this.targetMember,
    required this.date,
    required this.page,
    required this.size,
    required this.sortBy
  });

  Map<String, dynamic> toJson() => {
    'fromMember':fromMember,
    'targetMember':targetMember,
    'date':date,
    'page':page,
    'size':size,
    'sortBy':sortBy
  };
}
