class ChatMessageModel {
  int? id;
  String? fromMember;
  String? content;
  String? targetMember;
  int? type;
  int? status;
  String? createdAt;
  String? updatedAt;
  int? isRead;
  int? block;

  ChatMessageModel(
      {required this.id,
      required this.fromMember,
      required this.content,
      required this.targetMember,
      required this.type,
      required this.status,
      required this.createdAt,
      required this.updatedAt,
      required this.isRead,
      required this.block});

  ChatMessageModel.fromJson(Map<String, dynamic> responseJson) {
    id = responseJson['id'];
    fromMember = responseJson['fromMember'];
    content = responseJson['content'];
    targetMember = responseJson['targetMember'];
    type = responseJson['type'];
    status = responseJson['status'];
    isRead = responseJson['isRead'];
    createdAt = responseJson['createdAt'];
    updatedAt = responseJson['updatedAt'];
    block = responseJson['block'] == true ? 1 : 0;
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'fromMember': fromMember,
        'content': content,
        'targetMember': targetMember,
        'type': type,
        'status': status,
        'isRead': isRead,
        'createdAt': createdAt,
        'updatedAt': updatedAt,
        'block': block
      };

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
    return other is ChatMessageModel && other.id == id;
  }

  @override
  int get hashCode {
    return id.hashCode;
  }
}

class VisitChatMessageModel extends ChatMessageModel {
  int? visitRecordId;

  VisitChatMessageModel(
      {required int? id,
      required this.visitRecordId,
      required String? fromMember,
      required String? content,
      required String? targetMember,
      required int? type,
      required int? status,
      required String? createdAt,
      required String? updatedAt,
      required int? isRead,
      required int? block})
      : super(
            id: id,
            fromMember: fromMember,
            content: content,
            targetMember: targetMember,
            type: type,
            status: status,
            createdAt: createdAt,
            updatedAt: updatedAt,
            isRead: isRead,
            block: block);
  VisitChatMessageModel.fromJson(Map<String, dynamic> responseJson)
      : super.fromJson(responseJson) {
    visitRecordId = responseJson['visitRecordId'];
  }

  @override
  Map<String, dynamic> toJson() => {
    'id': id,
    'visitRecordId': visitRecordId,
    'fromMember': fromMember,
    'content': content,
    'targetMember': targetMember,
    'type': type,
    'status': status,
    'isRead': isRead,
    'createdAt': createdAt,
    'updatedAt': updatedAt,
    'block': block
  };
}
