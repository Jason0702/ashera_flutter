class AboutUsModel{
  int? id;
  String? about;
  double? updatedAt;

  AboutUsModel.fromJson(Map<String, dynamic> responseData){
    id = responseData['id'];
    about = responseData['about'];
    updatedAt = responseData['updatedAt'];
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'about': about,
    'updatedAt': updatedAt
  };
}