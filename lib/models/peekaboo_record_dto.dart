class AddHideAndSeekRecordDTO {
  int? fromMemberId;
  String? fromMemberName;
  String? fromMemberNickname;
  int? fromMemberGender;
  String? fromMemberBirthday;
  int? targetMemberId;
  String? targetMemberName;
  String? targetMemberNickname;
  int? targetMemberGender;
  String? targetMemberBirthday;
  String? address;
  double? latitude;
  double? longitude;
  int? catchType;

  AddHideAndSeekRecordDTO({
    required this.fromMemberId,
    required this.fromMemberName,
    required this.fromMemberNickname,
    required this.fromMemberGender,
    required this.fromMemberBirthday,
    required this.targetMemberId,
    required this.targetMemberName,
    required this.targetMemberNickname,
    required this.targetMemberGender,
    required this.targetMemberBirthday,
    required this.address,
    required this.latitude,
    required this.longitude,
    required this.catchType,
  });

  Map<String, dynamic> toJson() => {
        'fromMemberId': fromMemberId,
        'fromMemberName': fromMemberName,
        'fromMemberNickname': fromMemberNickname,
        'fromMemberGender': fromMemberGender,
        'fromMemberBirthday': fromMemberBirthday,
        'targetMemberId': targetMemberId,
        'targetMemberName': targetMemberName,
        'targetMemberNickname': targetMemberNickname,
        'targetMemberGender': targetMemberGender,
        'targetMemberBirthday': targetMemberBirthday,
        'address':address,
        'latitude': latitude,
        'longitude': longitude,
        'catchType': catchType
      };
}

class AddBeingHideAndSeekRecordDTO {
  int? targetMemberId;
  String? targetMemberName;
  String? targetMemberNickname;
  int? targetMemberGender;
  String? targetMemberBirthday;
  String? address;
  double? latitude;
  double? longitude;
  int? catchType;

  AddBeingHideAndSeekRecordDTO(
      {required this.targetMemberId,
      required this.targetMemberName,
      required this.targetMemberNickname,
      required this.targetMemberGender,
      required this.targetMemberBirthday,
      required this.address,
      required this.latitude,
      required this.longitude,
      required this.catchType});

  Map<String, dynamic> toJson() => {
        'targetMemberId': targetMemberId,
        'targetMemberName': targetMemberName,
        'targetMemberNickname': targetMemberNickname,
        'targetMemberGender': targetMemberGender,
        'targetMemberBirthday': targetMemberBirthday,
        'address': address,
        'latitude': latitude,
        'longitude': longitude,
        'catchType': catchType
      };
}

class CatchHideAndSeekRecordDTO{
  int? id;
  int? fromMemberId;
  String? fromMemberName;
  String? fromMemberNickname;
  int? fromMemberGender;
  String? fromMemberBirthday;

  CatchHideAndSeekRecordDTO({
    required this.id,
    required this.fromMemberId,
    required this.fromMemberName,
    required this.fromMemberNickname,
    required this.fromMemberGender,
    required this.fromMemberBirthday,
  });

  Map<String, dynamic> toJson() => {
    'id': id,
    'fromMemberId':fromMemberId,
    'fromMemberName':fromMemberName,
    'fromMemberNickname':fromMemberNickname,
    'fromMemberGender':fromMemberGender,
    'fromMemberBirthday':fromMemberBirthday
  };
}
