import 'dart:convert';
import 'dart:ffi';

import '../enum/app_enum.dart';

class MeModel{
  int? id;
  String? name;
  String? nickname;
  int? gender;
  double? height;
  double? weight;
  String? birthday;
  String? cellphone;
  String? mugshot;
  String? zodiacSign;
  String? bloodType;
  String? aboutMe;
  String? aboutMe2;
  String? job;
  String? asheraUid;
  String? updatedAt;
  double? latitude;
  double? longitude;
  String? faceId;
  int? verify;
  /*int? reVerify;*/
  String? facePic;
  String? s3Etag;
  int? vip;
  int? vipSubscription;
  int? activeStart;
  int? activeEnd;
  bool? hideBirthday;
  bool? hideWeight;
  bool? hideJob;
  int? status;
  int? initGame;
  int? initGameAt;
  int? age;
  String? interactivePic;
  int? hideLastOnlineAt;
  int? hideLastOfflineAt;
  List<String>? hideColumn;

  MeModel.fromJson(Map<String, dynamic> response){
    id = response['id'];
    name = response['name'];
    nickname = response['nickname'];
    gender = response['gender'];
    height = response['height'];
    weight = response['weight'];
    birthday = response['birthday'];
    cellphone = response['cellphone'];
    mugshot = response['mugshot'] ?? '';
    zodiacSign = response['zodiacSign'] ?? '';
    bloodType = response['bloodType'];
    aboutMe = response['aboutMe'];
    aboutMe2 = response['aboutMe2'] ?? '';
    job = response['job'];
    asheraUid = response['asheraUid'];
    updatedAt = response['updatedAt'].toString();
    latitude = double.tryParse(response['latitude'].toString());
    longitude = double.tryParse(response['longitude'].toString());
    faceId = response['faceId'];
    verify = response['verify'];
    /*reVerify = response['reVerify'] == true ? 1 : 0;*/
    facePic = response['facePic'] ?? '';
    s3Etag = response['s3Etag'];
    vip = response['vip'] == true ? 1 : 0;
    vipSubscription = response['vipSubscription'] == true ? 1 : 0;
    activeStart = response['activeStart'];
    activeEnd = response['activeEnd'];
    status = response['status'];
    initGame = response['initGame'] == true ? 1 : 0;
    initGameAt = response['initGameAt'];
    age = response['age'];
    interactivePic = response['interactivePic'] ?? '';
    hideLastOnlineAt = response['hideLastOnlineAt'];
    hideLastOfflineAt = response['hideLastOfflineAt'];
    hideColumn = response['hideColumn'] != null && response['hideColumn'].toString().trim() !=  "" ? List<String>.from(json.decode(response['hideColumn'])) : [];
    hideBirthday = hideColumn!.isNotEmpty ? List<String>.from(hideColumn!).where((element) => element == HideColumn.birthday.name).isNotEmpty ? true : false : false;
    hideWeight = hideColumn!.isNotEmpty ? List<String>.from(hideColumn!).where((element) => element == HideColumn.weight.name).isNotEmpty ? true : false : false;
    hideJob = hideColumn!.isNotEmpty ? List<String>.from(hideColumn!).where((element) => element == HideColumn.job.name).isNotEmpty ? true : false : false;
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'name': name,
    'nickname': nickname,
    'gender': gender,
    'height': height,
    'weight': weight,
    'birthday': birthday,
    'cellphone': cellphone,
    'mugshot': mugshot,
    'zodiacSign': zodiacSign,
    'bloodType': bloodType,
    'aboutMe':aboutMe,
    'aboutMe2': aboutMe2,
    'job': job,
    'asheraUid': asheraUid,
    'updatedAt': updatedAt,
    'latitude': latitude,
    'longitude': longitude,
    'faceId': faceId,
    'verify': verify,
    /*'reVerify': reVerify,*/
    'facePic': facePic,
    's3Etag': s3Etag,
    'vip': vip,
    'vipSubscription': vipSubscription,
    'activeStart': activeStart,
    'activeEnd': activeEnd,
    'hideBirthday': hideBirthday,
    'status': status,
    'initGame': initGame,
    'initGameAt': initGameAt,
    'interactivePic': interactivePic,
    'hideLastOnlineAt': hideLastOnlineAt,
    'hideLastOfflineAt':hideLastOfflineAt,
    'hideColumn': json.encode(hideColumn)
  };

}

class MemberModel {
  int? id;
  String? name;
  String? nickname;
  int? gender;
  double? height;
  double? weight;
  String? birthday;
  String? cellphone;
  String? mugshot;
  String? zodiacSign;
  String? bloodType;
  String? aboutMe;
  String? aboutMe2;
  String? job;
  String? asheraUid;
  String? updatedAt;
  double? latitude;
  double? longitude;
  String? faceId;
  int? verify;
  /*int? reVerify;*/
  String? facePic;
  String? s3Etag;
  int? vip;
  int? activeStart;
  int? activeEnd;
  int? hideBirthday;
  int? status;
  int? initGame;
  int? initGameAt;
  int? age;
  String? interactivePic;
  int? hideLastOnlineAt;
  int? hideLastOfflineAt;
  bool? online;
  int? hideStatus;
  bool? customerService;

  MemberModel({
    required this.id,
    required this.name,
    required this.nickname,
    required this.gender,
    required this.height,
    required this.weight,
    required this.birthday,
    required this.cellphone,
    required this.mugshot,
    required this.zodiacSign,
    required this.bloodType,
    required this.aboutMe,
    required this.job,
    required this.asheraUid,
    required this.updatedAt,
    required this.latitude,
    required this.longitude,
    required this.faceId,
    required this.verify,
    /*this.reVerify,*/
    required this.facePic,
    required this.s3Etag,
    required this.vip,
    required this.activeStart,
    required this.activeEnd,
    required this.hideBirthday,
    required this.status,
    required this.initGame,
    required this.initGameAt,
    required this.interactivePic,
    this.hideLastOnlineAt = 0,
    this.hideLastOfflineAt = 0,
    this.online,
    this.hideStatus,
    this.customerService
  });

  MemberModel.fromJson(Map<String, dynamic> json){
    id = json['id'];
    name = json['name'];
    nickname = json['nickname'];
    gender = json['gender'];
    height = json['height'];
    weight = json['weight'];
    birthday = json['birthday'];
    cellphone = json['cellphone'];
    mugshot = json['mugshot'] ?? '';
    zodiacSign = json['zodiacSign'];
    bloodType = json['bloodType'];
    aboutMe = json['aboutMe'];
    aboutMe2 = json['aboutMe2'] ?? '';
    job = json['job'];
    asheraUid = json['asheraUid'];
    updatedAt = json['updatedAt'].toString();
    latitude = double.tryParse(json['latitude'].toString());
    longitude = double.tryParse(json['longitude'].toString());
    faceId = json['faceId'];
    verify = json['verify'].runtimeType == Int ? json['verify'] : 1;
    /*reVerify = json['reVerify'] == true ? 1 : 0;*/
    facePic = json['facePic'];
    s3Etag = json['s3Etag'];
    vip = json['vip'] == true ? 1 : 0;
    activeStart = json['activeStart'];
    activeEnd = json['activeEnd'];
    hideBirthday = json['hideBirthday'] == true ? 1 : 0;
    status = json['status'];
    initGame = json['initGame'] == true ? 1 : 0;
    initGameAt = json['initGameAt'];
    age = json['age'];
    interactivePic = json['interactivePic'] ?? '';
    hideLastOnlineAt = json['hideLastOnlineAt'];
    hideLastOfflineAt = json['hideLastOfflineAt'];
    online = json['online'];
    hideStatus = json['hideStatus'];
    customerService = json['customerService'];
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'name': name,
    'nickname': nickname,
    'gender': gender,
    'height': height,
    'weight': weight,
    'birthday': birthday,
    'cellphone': cellphone,
    'mugshot': mugshot,
    'zodiacSign': zodiacSign,
    'bloodType': bloodType,
    'aboutMe': aboutMe,
    'aboutMe2': aboutMe2,
    'job': job,
    'asheraUid': asheraUid,
    'updatedAt': updatedAt,
    'latitude': latitude,
    'longitude': longitude,
    'faceId': faceId,
    'verify': verify,
    'facePic': facePic,
    's3Etag': s3Etag,
    'vip': vip,
    'activeStart': activeStart,
    'activeEnd': activeEnd,
    'hideBirthday': hideBirthday,
    'status': status,
    'initGame': initGame,
    'initGameAt': initGameAt,
    'interactivePic': interactivePic,
    'hideLastOnlineAt': hideLastOnlineAt,
    'hideLastOfflineAt':hideLastOfflineAt,
    'online': online,
    'hideStatus': hideStatus,
    'customerService': customerService
  };

  Map<String, dynamic> insertDB() => {
    'id': id,
    'name': name,
    'nickname': nickname,
    'gender': gender,
    'height': height,
    'weight': weight,
    'birthday': birthday,
    'cellphone': cellphone,
    'mugshot': mugshot,
    'zodiacSign': zodiacSign,
    'bloodType': bloodType,
    'aboutMe':aboutMe,
    'job': job,
    'asheraUid': asheraUid,
    'updatedAt': updatedAt,
    'latitude': latitude,
    'longitude': longitude,
    'faceId': faceId,
    'verify': verify,
    'facePic': facePic,
    's3Etag': s3Etag,
    'vip': vip,
    'activeStart': activeStart,
    'activeEnd': activeEnd,
    'hideBirthday': hideBirthday,
    'status': status,
    'initGame': initGame,
    'initGameAt': initGameAt,
    'interactivePic': interactivePic
  };

  Map<String, dynamic> insertDBUpdateAt() => {
    'id': id,
    'updatedAt': updatedAt,
  };

  Map<String, dynamic> insertDBNickname() => {
    'id': id,
    'nickname': nickname,
  };

  @override
  bool operator == (Object other) {
    if (identical(this, other)) return true;

    return other is MemberModel &&
        other.id == id &&
        other.nickname == nickname &&
        other.mugshot == mugshot &&
        other.hideBirthday == hideBirthday &&
        other.interactivePic == interactivePic;
  }

  @override
  int get hashCode {
    return id.hashCode ^ name.hashCode ^ mugshot.hashCode ^ hideBirthday.hashCode ^ interactivePic.hashCode;
  }

}

class MemberBlackModel{
  int? id;
  int? fromMemberId;
  int? targetMemberId;

  MemberBlackModel({
    required this.id,
    required this.fromMemberId,
    required this.targetMemberId
  });

  MemberBlackModel.fromJson(Map<String, dynamic> json){
    id = json['id'];
    fromMemberId = json['fromMemberId'];
    targetMemberId = json['targetMemberId'];
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'fromMemberId': fromMemberId,
    'targetMemberId': targetMemberId
  };
}

class MemberForgotPasswordDTO{
  String number;
  String code;
  String password;

  MemberForgotPasswordDTO({required this.number, required this.code, required this.password});

  Map<String, dynamic> toJson() => {
    'number':number,
    'code':code,
    'password':password
  };
}

class MemberBlacklistDTO{
  int fromMemberId;
  int targetMemberId;

  MemberBlacklistDTO({
    required this.fromMemberId,
    required this.targetMemberId
  });

  Map<String, dynamic> toJson() => {
    'fromMemberId': fromMemberId,
    'targetMemberId': targetMemberId
  };
}

class MemberRegisterDTO{
  String name;
  String password;
  String nickname;
  String cellphone;
  int gender;
  String birthday;
  String mugshot;
  String age;
  String zodiacSign;

  MemberRegisterDTO({
    required this.name,
    required this.password,
    required this.nickname,
    required this.cellphone,
    required this.gender,
    required this.birthday,
    required this.mugshot,
    required this.age,
    required this.zodiacSign
  });

  Map<String, dynamic> toJson() => {
    'name': name,
    'password': password,
    'nickname': nickname,
    'cellphone': cellphone,
    'gender': gender,
    'birthday': birthday,
    'mugshot': mugshot,
    'age': age,
    'zodiacSign': zodiacSign
  };
}
//Google
class PurchaseGetProductDTO{
  String packageName;
  String productId;
  String token;
  String storeOrderNumber;
  int storeOrderType;

  PurchaseGetProductDTO({
    required this.packageName,
    required this.productId,
    required this.token,
    required this.storeOrderNumber,
    required this.storeOrderType
  });

  Map<String, dynamic> toJson() => {
    'packageName': packageName,
    'productId': productId,
    'token': token,
    'storeOrderNumber': storeOrderNumber,
    'storeOrderType': storeOrderType
  };
}

//Apple
class IosPayDTO{
  int? priceId;
  String? transactionId;
  String? payload;
  String? storeOrderNumber;
  int? storeOrderType;

  IosPayDTO({
    required this.priceId,
    required this.transactionId,
    required this.payload,
    required this.storeOrderNumber,
    required this.storeOrderType
  });

  Map<String, dynamic> toJson() => {
    'priceId': priceId,
    'transactionId': transactionId,
    'payload': payload,
    'storeOrderNumber': storeOrderNumber,
    'storeOrderType': storeOrderType
  };
}