import 'dart:convert';

import 'package:ashera_flutter/models/member_model.dart';

class FollowerModel{
  int? id;
  int? memberId;
  int? followerId;
  MemberModel? memberModel;
  MemberModel? followerModel;

  FollowerModel.fromJson(Map<String, dynamic> responseData){
    id = responseData['id'];
    memberId = responseData['memberId'];
    followerId = responseData['followerId'];
    memberModel = MemberModel.fromJson(json.decode(json.encode(responseData['member'])));
    followerModel = MemberModel.fromJson(json.decode(json.encode(responseData['follower'])));
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'memberId':memberId,
    'followerId':followerId,
    'member':memberModel!.toJson().toString(),
    'follower':followerModel!.toJson().toString()
  };
}