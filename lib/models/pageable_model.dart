import 'dart:convert';

import 'package:ashera_flutter/models/sort_model.dart';

class PageableModel{
  SortModel? sortModel;
  int? offset;
  int? pageNumber;
  int? pageSize;
  bool? paged;
  bool? unpaged;

  PageableModel.fromJson(Map<String, dynamic> data){
    sortModel = SortModel.fromJson(json.decode(json.encode(data['sort'])));
    offset = data['offset'];
    pageNumber = data['pageNumber'];
    pageSize = data['pageSize'];
    paged = data['paged'];
    unpaged = data['unpaged'];
  }

  Map<String, dynamic> toJson() => {
    'sortModel': sortModel!.toJson(),
    'offset':offset,
    'pageNumber':pageNumber,
    'pageSize':pageSize,
    'paged':paged,
    'unpaged':unpaged
  };
}