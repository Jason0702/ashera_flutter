class MessageModel{
  String? roomName;
  int? receiverId;
  int? senderId;
  String? createAt;
  int? read; // 0 unread 1 read
  String? message;
  int? messageType;//0 普通訊息 1 圖片 2 影片 3 錄音 4 通知
  String? name;
  String? mugshot;

  MessageModel({this.roomName,this.receiverId,this.senderId,this.createAt,this.read,this.message,this.messageType,
    this.name,
    this.mugshot
  });

  MessageModel.fromJson(Map<String,dynamic> json){
    roomName = json['roomName'];
    receiverId = json['receiver'];
    senderId = json['senderId'];
    createAt = json['createAt'];
    read = json['read'];
    messageType = json['messageType'];
    message = json['message'];
  }

  Map<String, dynamic> toJson() => {
    'roomName': roomName,
    'receiver': receiverId,
    'messageType': messageType,
    'senderId': senderId,
    'createAt': createAt,
    'read': read,
    'message': message,
  };

  MessageModel.fromJsonForFriendZone(Map<String,dynamic> json){
    receiverId = json['friendZoneId'];
    senderId = json['senderId'];
    createAt = json['createAt'];
    messageType = json['messageType'];
    message = json['message'];
  }
  Map<String, dynamic> toFriendZone() => {
    'friendZoneId': receiverId,
    'messageType': messageType,
    'senderId': senderId,
    'createAt': createAt,
    'message': message,
  };
}