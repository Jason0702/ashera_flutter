class VisitRecordModel {
  int? id;
  int? fromMemberId;
  String? fromMemberName;
  String? fromMemberNickname;
  int? fromMemberGender;
  String? fromMemberBirthday;
  int? targetMemberId;
  String? targetMemberName;
  String? targetMemberNickname;
  int? targetMemberGender;
  String? targetMemberBirthday;
  String? requirement;
  String? address;
  double? fromLatitude;
  double? fromLongitude;
  double? endLatitude;
  double? endLongitude;
  int? whoPay;
  double? fromMemberScore;
  String? fromMemberComment;
  double? targetMemberScore;
  String? targetMemberComment;
  int? mode;
  bool? canceled;
  int? completeAt;
  int? cancelAt;
  int? cancelBy;
  bool? approved;
  int? approveAt;
  int? expireAt;
  int? wannaVisitAt;
  int? status;
  double? createdAt;
  double? updatedAt;
  String? fromMemberMugshot;
  String? targetMemberMugshot;

  VisitRecordModel.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    fromMemberId = json['fromMemberId'];
    fromMemberName = json['fromMemberName'];
    fromMemberNickname = json['fromMemberNickname'];
    fromMemberGender = json['fromMemberGender'];
    fromMemberBirthday = json['fromMemberBirthday'];
    targetMemberId = json['targetMemberId'];
    targetMemberName = json['targetMemberName'];
    targetMemberNickname = json['targetMemberNickname'];
    targetMemberGender = json['targetMemberGender'];
    targetMemberBirthday = json['targetMemberBirthday'];
    requirement = json['requirement'];
    address = json['address'];
    fromLatitude = double.tryParse(json['fromLatitude'].toString()) ?? 0.0;
    fromLongitude = double.tryParse(json['fromLongitude'].toString()) ?? 0.0;
    endLatitude = double.tryParse(json['endLatitude'].toString()) ?? 0.0;
    endLongitude = double.tryParse(json['endLongitude'].toString()) ?? 0.0;
    whoPay = json['whoPay'];
    fromMemberScore =
        double.tryParse(json['fromMemberScore'].toString()) ?? 0.0;
    fromMemberComment = json['fromMemberComment'];
    targetMemberScore =
        double.tryParse(json['targetMemberScore'].toString()) ?? 0.0;
    targetMemberComment = json['targetMemberComment'];
    mode = json['mode'];
    canceled = json['canceled'];
    completeAt = json['completeAt'];
    cancelAt = json['cancelAt'];
    cancelBy = json['cancelBy'];
    approved = json['approved'];
    approveAt = json['approveAt'];
    wannaVisitAt = json['wannaVisitAt'];
    expireAt = json['expireAt'];
    status = json['status'];
    createdAt = double.tryParse(json['createdAt'].toString()) ?? 0.0;
    updatedAt = double.tryParse(json['updatedAt'].toString()) ?? 0.0;
  }

  Map<String, dynamic> toJson() => {
        'id': id,
        'fromMemberId': fromMemberId,
        'fromMemberName': fromMemberName,
        'fromMemberNickname': fromMemberNickname,
        'fromMemberGender': fromMemberGender,
        'fromMemberBirthday': fromMemberBirthday,
        'targetMemberId': targetMemberId,
        'targetMemberName': targetMemberName,
        'targetMemberNickname': targetMemberNickname,
        'targetMemberGender': targetMemberGender,
        'targetMemberBirthday': targetMemberBirthday,
        'requirement': requirement,
        'address': address,
        'fromLatitude': fromLatitude,
        'fromLongitude': fromLongitude,
        'endLatitude': endLatitude,
        'endLongitude': endLongitude,
        'whoPay': whoPay,
        'fromMemberScore': fromMemberScore,
        'fromMemberComment': fromMemberComment,
        'targetMemberScore': targetMemberScore,
        'targetMemberComment': targetMemberComment,
        'mode': mode,
        'canceled': canceled,
        'completeAt': completeAt,
        'cancelAt': cancelAt,
        'cancelBy': cancelBy,
        'approved': approved,
        'approveAt': approveAt,
        'expireAt': expireAt,
        'wannaVisitAt': wannaVisitAt,
        'status': status,
        'createdAt': createdAt,
        'updatedAt': updatedAt,
        'fromMemberMugshot':fromMemberMugshot,
        'targetMemberMugshot':targetMemberMugshot
      };
}
/*
* {
* id: 2700,
* fromMemberId: 2651,
* fromMemberName: 0988168168,
* fromMemberNickname: 炫彬,
* fromMemberGender: 0,
* fromMemberBirthday: 2004-08-09,
* targetMemberId: 2648,
* targetMemberName: 0987878787,
* targetMemberNickname: 吖哦,
* targetMemberGender: 0,
* targetMemberBirthday: 2004-08-09,
* requirement: eeeeeeeeeeeee,
* address: 高雄市鼓山區美術東四路55,
* fromLatitude: 22.6525282,
* fromLongitude: 120.2895749,
* endLatitude: 22.6525297,
* endLongitude: 120.2895833,
* whoPay: 0,
* fromMemberScore: 5.0,
* fromMemberComment: 很好相處,
* targetMemberScore: 5.0,
* targetMemberComment: 很適合,
* mode: 1,
* canceled: false,
* completeAt: 1660206608,
* cancelAt: 0,
* cancelBy: -1,
* approved: true,
* approveAt: null,
* expireAt: 1660210164,
* wannaVisitAt: 1660206562,
* status: 1,
* createdAt: 1660206366.0,
* updatedAt: 1660206608.0,
* fromMemberMugshot: null,
* targetMemberMugshot: null}
* */
class VisitStarRecordModel{
  int? memberId;
  String? memberName;
  String? memberNickname;
  double? memberScore;
  String? memberComment;
  num? completeAt;

  VisitStarRecordModel.fromMemberJson(Map<String, dynamic> json) {
    memberId = json['fromMemberId'];
    memberName = json['fromMemberName'];
    memberNickname = json['fromMemberNickname'];
    memberScore = json['fromMemberScore'];
    memberComment = json['fromMemberComment'];
    completeAt = json['completeAt'];
  }

  VisitStarRecordModel.targetMemberJson(Map<String, dynamic> json) {
    memberId = json['targetMemberId'];
    memberName = json['targetMemberName'];
    memberNickname = json['targetMemberNickname'];
    memberScore = json['targetMemberScore'];
    memberComment = json['targetMemberComment'];
    completeAt = json['completeAt'];
  }

  Map<String, dynamic> toJson() => {
    'memberId': memberId,
    'memberName': memberName,
    'memberNickname': memberNickname,
    'memberScore': memberScore,
    'memberComment': memberComment,
    'completeAt': completeAt,
  };
}
