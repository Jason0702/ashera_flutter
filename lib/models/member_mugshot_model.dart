class MemberMugshotModel{
  int? id;
  String? mugshot;
  String? interactivePic;

  MemberMugshotModel.fromJson(Map<String, dynamic> json){
    id = json['id'];
    mugshot = json['mugshot'];
    interactivePic = json['interactivePic'] ?? '';
  }

  Map<String, dynamic> toJson() => {
    'id':id,
    'mugshot':mugshot,
    'interactivePic':interactivePic
  };
}