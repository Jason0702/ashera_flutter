class ErrDTO{
  int? code;
  dynamic data;

  ErrDTO.fromJson(Map<String, dynamic> json){
    code = json['code'];
    data = json['data'];
  }

  Map<String, dynamic> toJson() => {
    'code':code,
    'data':data
  };
}