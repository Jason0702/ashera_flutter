/*
* {
* kind: androidpublisher#productPurchase,
* purchaseTimeMillis: 1667801799022,
* purchaseState: 0,
* consumptionState: 1,
* developerPayload: ,
* orderId: GPA.3350-2877-6685-08805,
* purchaseType: 0,
* acknowledgementState: 1,
* purchaseToken: null,
* productId: null,
* quantity: 0,
* obfuscatedExternalAccountId: null,
* obfuscatedExternalProfileId: null,
* regionCode: TW}
* */

class GoogleIAPModel{
  String? kind;
  String? purchaseTimeMillis;
  int? consumptionState;
  String? orderId;
  String? regionCode;

  GoogleIAPModel.fromJson(Map<String, dynamic> json){
    kind = json['kind'];
    purchaseTimeMillis = json['purchaseTimeMillis'];
    consumptionState = json['consumptionState'];
    orderId = json['orderId'];
    regionCode = json['regionCode'];
  }

  Map<String, dynamic> toJson() => {
    'kind': kind,
    'purchaseTimeMillis': purchaseTimeMillis,
    'consumptionState': consumptionState,
    'orderId': orderId,
    'regionCode': regionCode
  };
}