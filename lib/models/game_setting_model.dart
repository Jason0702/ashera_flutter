class GameSettingModel{
  int? id;
  int? cleanLimit;
  int? shitLimit;
  int? recoverCleanPoints;
  int? recoverShitPoints;

  GameSettingModel.fromJson(Map<String, dynamic> r){
    id = r['id'];
    cleanLimit = r['cleanLimit'];
    shitLimit = r['shitLimit'];
    recoverCleanPoints = r['recoverCleanPoints'];
    recoverShitPoints = r['recoverShitPoints'];
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'cleanLimit': cleanLimit,
    'shitLimit': shitLimit,
    'recoverCleanPoints': recoverCleanPoints,
    'recoverShitPoints': recoverShitPoints
  };
}