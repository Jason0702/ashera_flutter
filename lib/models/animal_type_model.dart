class AnimalTypeModel{
  int? id;
  String? name;
  String? chtName;
  int? status;

  AnimalTypeModel.fromJson(Map<String, dynamic> json){
    id = json['id'];
    name = json['name'];
    chtName = json['chtName'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() => {
    'id': id,
    'name': name,
    'chtName': chtName,
    'status': status
  };
}