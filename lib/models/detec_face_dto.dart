
class DetectFaceDTO{
  String? filePath;
  String? imageName;
  bool local = true;

  DetectFaceDTO({
    required this.filePath,
    required this.imageName
  });

  Map<String, dynamic> toJson() => {
    'filePath':filePath,
    'imageName':imageName,
    'local':local
  };
}