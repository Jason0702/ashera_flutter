class AddFollowerRequestDTO{
  int followerId;
  int memberId;

  AddFollowerRequestDTO({
    required this.followerId,
    required this.memberId,
  });

  Map<String, dynamic> toJson() =>{
    'followerId': followerId,
    'memberId': memberId,
  };
}

class AcceptFollowerRequestDTO{
  int followerId;
  int memberId;
  bool approve;

  AcceptFollowerRequestDTO({
    required this.followerId,
    required this.memberId,
    required this.approve
  });

  Map<String, dynamic> toJson() =>{
    'followerId': followerId,
    'memberId': memberId,
    'approve': approve
  };
}