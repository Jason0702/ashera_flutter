class AddUnlockPaymentRecordDTO{
  int fromMemberId;
  int targetMemberId;

  AddUnlockPaymentRecordDTO({required this.fromMemberId, required this.targetMemberId});

  Map<String, dynamic> toJson() => {
    'fromMemberId':fromMemberId,
    'targetMemberId':targetMemberId
  };
}