
import 'package:ashera_flutter/routers/route_name.dart';
import 'package:flutter/cupertino.dart';

class AsheraRouteInformationParser extends RouteInformationParser<List<RouteSettings>>{
  const AsheraRouteInformationParser() : super();

  @override
  Future<List<RouteSettings>> parseRouteInformation(RouteInformation routeInformation) {
    final uri = Uri.parse(routeInformation.location!);
    if(uri.pathSegments.isEmpty){
      return Future.value([const RouteSettings(name: RouteName.splashPage)]);
    }
    final routeSettings = uri.pathSegments.map((path) => RouteSettings(
      name: '/$path',
      arguments: path == uri.pathSegments.last
          ? uri.queryParameters
          : null,
    )).toList();
    return Future.value(routeSettings);
  }

  @override
  RouteInformation restoreRouteInformation(List configuration) {
    final location = configuration.last.name;
    final arguments = _restoreArguments(configuration.last);
    return RouteInformation(location: '$location$arguments');
  }

  String _restoreArguments(RouteSettings routeSettings) {
    if(routeSettings.name != '/') return '';
    var args = routeSettings.arguments as Map;
    return '?name=${args['name']}&imgUrl=${args['imgUrl']}';
  }
}