
import 'package:ashera_flutter/dialog/my_qr_code_dialog.dart';
import 'package:ashera_flutter/pages/app_bar_page.dart';
import 'package:ashera_flutter/pages/dynamic_app_icon_page.dart';
import 'package:ashera_flutter/pages/forget_password_page.dart';
import 'package:ashera_flutter/pages/game_visit/visit_star_record_index_page.dart';
import 'package:ashera_flutter/pages/login_page.dart';
import 'package:ashera_flutter/pages/notice_page.dart';
import 'package:ashera_flutter/pages/phone_verification_page.dart';
import 'package:ashera_flutter/routers/route_name.dart';
import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../dialog/bonus_dialog.dart';
import '../dialog/cancel_visit_dialog.dart';
import '../dialog/report_dialog.dart';
import '../pages/about_ashera_page.dart';
import '../pages/add_friend_page.dart';
import '../pages/black_list_page.dart';
import '../pages/camera_page.dart';
import '../pages/change_password_page.dart';
import '../pages/chat_room_page.dart';
import '../pages/colorful_social_page.dart';
import '../pages/edit_profile_page.dart';
import '../pages/face_recognition_page.dart';
import '../pages/face_register_page.dart';
import '../pages/game_home/game_home.dart';
import '../pages/game_home/game_home_other_people_page.dart';
import '../pages/game_home/game_street.dart';
import '../pages/game_peekaboo/peekaboo_camera.dart';
import '../pages/game_peekaboo/peekaboo_huntlist.dart';
import '../pages/game_peekaboo/peekaboo_page.dart';
import '../pages/game_peekaboo/peekaboo_record.dart';
import '../pages/game_visit/visit_chat_room_page.dart';
import '../pages/game_visit/visit_page.dart';
import '../pages/game_visit/visit_process_page.dart';
import '../pages/game_visit/visit_record.dart';
import '../pages/game_visit/visit_release_requirements_page.dart';
import '../pages/game_visit/visit_star_record_page.dart';
import '../pages/identification_page.dart';
import '../pages/my_points_page.dart';
import '../pages/opinion_page.dart';
import '../pages/register_page.dart';
import '../pages/splash_page.dart';
import '../pages/welcome_page.dart';

//路由2.0
class AsheraRouterDelegate extends RouterDelegate<List<RouteSettings>>
    with ChangeNotifier, PopNavigatorRouterDelegateMixin<List<RouteSettings>> {
  final List<Page> _pages = [];
  List<Page> get pages => _pages;

  @override
  GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  @override
  Widget build(BuildContext context) {
    return Navigator(
      key: navigatorKey,
      pages: List.of(_pages),
      onPopPage: _onPopPage,
    );
  }

  @override
  Future<void> setNewRoutePath(List<RouteSettings> configuration) async {
    debugPrint('setNewRoutePath ${configuration.last.name}');

    _setPath(configuration
        .map((routeSettings) => _createPage(routeSettings))
        .toList());
    return Future.value(null);
  }

  void _setPath(List<Page> pages) {
    _pages.clear();
    _pages.addAll(pages);
    if (_pages.first.name != '/') {}
    notifyListeners();
  }
  ///回上一頁
  @override
  Future<bool> popRoute() {
    if (canPop()) {
      _pages.removeLast();
      notifyListeners();
      return Future.value(true);
    }
    return _confirmExit();
  }
  ///回到第一頁
  Future<bool> popUtil(){
    if (canPop()) {
      Page _pageCopy = _pages.first;
      _pages.clear();
      _pages.add(_pageCopy);
      notifyListeners();
      return Future.value(true);
    }
    return _confirmExit();
  }
  ///關閉當前畫面並建立下一頁
  void popAndPushRouter({required String name, dynamic arguments}){
    if(_pages.isNotEmpty){
      _pages.removeAt(_pages.length - 1);
    }
    push(name: name, arguments: arguments);
  }

  bool canPop() {
    return _pages.length > 1;
  }

  bool _onPopPage(Route route, dynamic result) {
    if (!route.didPop(result)) return false;
    if (canPop()) {
      _pages.removeLast();
      return true;
    } else {
      return false;
    }
  }

  void push({required String name, dynamic arguments}) {
    _pages.add(_createPage(RouteSettings(name: name, arguments: arguments)));
    notifyListeners();
  }

  void replace({required String name, dynamic arguments}) {
    if (_pages.isNotEmpty) {
      _pages.clear();
    }
    push(name: name, arguments: arguments);
  }

  MaterialPage _createPage(RouteSettings routeSettings) {
    Widget child;
    switch (routeSettings.name) {
      case RouteName.splashPage:
        child = const SplashPage();
        break;
      case RouteName.welcomePage:
        child = const WelcomePage();
        break;
      case RouteName.loginPage:
        child = const LoginPage();
        break;
      case RouteName.registerPage:
        child = const RegisterPage();
        break;
      case RouteName.phoneVerificationPage:
        child = const PhoneVerificationPage();
        break;
      case RouteName.faceRegisterPage:
        child = const FaceRegisterPage();
        break;
      case RouteName.gamePage:
        child = const GamePage();
        break;
      case RouteName.gameHomePage:
        child = const GameHome();
        break;
      case RouteName.forgetPasswordPage:
        child = const ForgetPasswordPage();
        break;
      case RouteName.appBarPage:
        child = const AppBarPage();
        break;
      case RouteName.identificationPage:
        child = const IdentificationPage();
        break;
      case RouteName.colorfulSocialPage:
        child = const ColorfulSocialPage();
        break;
      case RouteName.opinionPage:
        child = const OpinionPage();
        break;
      case RouteName.chatRoomPage:
        child = const ChatRoomPage();
        break;
      case RouteName.noticePage:
        child = const NoticePage();
        break;
      case RouteName.changePasswordPage:
        child = const ChangePasswordPage();
        break;
      case RouteName.editProfilePage:
        child = const EditProfilePage();
        break;
      case RouteName.peekabooPage:
        child = const PeekabooPage();
        break;
      case RouteName.myPointsPage:
        child = const MyPointsPage();
        break;
      case RouteName.visitReleaseRequirementsPage:
        child = const VisitReleaseRequirementsPage();
        break;
      case RouteName.visitPage:
        child = const VisitPage();
        break;
      case RouteName.peekabooRecordPage:
        child = const PeekabooRecord();
        break;
      case RouteName.visitRecordPage:
        child = const VisitRecord();
        break;
      case RouteName.cameraPage:
        child = const CameraPage();
        break;
      case RouteName.visitProcessPage:
        child = const VisitProcessPage();
        break;
      case RouteName.visitStarRecordPage:
        child = const VisitStarRecordPage();
        break;
      case RouteName.visitStarRecordIndexPage:
        child = const VisitStarRecordIndexPage();
        break;
      case RouteName.addFriendPage:
        child = const AddFriendPage();
        break;
      case RouteName.aboutAsheraPage:
        child = const AboutAsheraPage();
        break;
      case RouteName.faceRecognitionPage:
        child = const FaceRecognitionPage();
        break;
      case RouteName.cancelVisitDialog:
        child = const CancelVisitDialog();
        break;
      case RouteName.peekabooCameraPage:
        child = const PeekabooCamera();
        break;
      case RouteName.bonusDialog:
        child = const BonusDialog();
        break;
      case RouteName.dynamicAppIconPage:
        child = const DynamicAppIconPage();
        break;
      case RouteName.blackListPage:
        child = const BlackListPage();
        break;
      case RouteName.gameHomeOtherPeoplePage:
        child = const GameHomeOtherPeoplePage();
        break;
      case RouteName.peekabooHuntListPage:
        child = const PeekabooHuntList();
        break;
      case RouteName.reportDialog:
        child = const ReportDialog();
        break;
      case RouteName.visitChatRoomPage:
        child = const VisitChatRoomPage();
        break;
      case RouteName.myQrCodePage:
        child = const MyQRCodeDialog();
        break;
     default:
        child = const Scaffold();
    }
    return MaterialPage(
        child: ResponsiveSizer(
          builder: (context, orientation, screenType) {
            return SafeArea(top: routeSettings.name == RouteName.splashPage ? false : true , bottom: false, child: child);
          },
        ),
        key: Key(routeSettings.name!) as LocalKey,
        name: routeSettings.name,
        arguments: routeSettings.arguments);
  }

  Future<bool> _confirmExit() async {
    final result = await showDialog<bool>(
        context: navigatorKey.currentContext!,
        builder: (context) {
          return AlertDialog(
            content: const Text(
              '確定要退出APP嗎?',
              textScaleFactor: 1,
            ),
            actions: [
              TextButton(
                onPressed: () => Navigator.pop(context, true),
                child: const Text(
                  '取消',
                  textScaleFactor: 1,
                ),
              ),
              TextButton(
                onPressed: () => Navigator.pop(context, false),
                child: const Text(
                  '確認',
                  textScaleFactor: 1,
                ),
              )
            ],
          );
        });
    return result ?? true;
  }
}
