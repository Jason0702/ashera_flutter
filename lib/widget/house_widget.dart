import 'dart:developer';
import 'dart:io';

import 'package:ashera_flutter/models/member_model.dart';
import 'package:ashera_flutter/provider/game_home_provider.dart';
import 'package:ashera_flutter/provider/member_provider.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../app.dart';
import '../enum/app_enum.dart';
import '../generated/l10n.dart';
import '../provider/game_street_provider.dart';
import '../routers/route_name.dart';
import '../utils/app_color.dart';
import '../utils/app_image.dart';
import '../utils/util.dart';
import 'gender_icon.dart';
import 'mugshot_widget.dart';
import 'package:async/async.dart';

class HouseWidget extends StatefulWidget{
  final MemberModel memberModel;
  final int index;
  const HouseWidget({Key? key, required this.memberModel, required this.index}): super(key: key);

  @override
  State createState() => _HouseWidgetState();
}
class _HouseWidgetState extends State<HouseWidget> with AutomaticKeepAliveClientMixin<HouseWidget>{
  GameHomeProvider? _gameHomeProvider;
  GameStreetProvider? _streetProvider;
  MemberProvider? _memberProvider;
  final AsyncMemoizer _memoizer = AsyncMemoizer();
  final AsyncMemoizer _memoizerSmoke = AsyncMemoizer();

  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    _gameHomeProvider = Provider.of<GameHomeProvider>(context, listen: false);
    _streetProvider = Provider.of<GameStreetProvider>(context, listen: false);
    _memberProvider = Provider.of<MemberProvider>(context, listen: false);
    return Container(
      padding: EdgeInsets.only(left: widget.index == 0 ? 10.w : 0),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          //房子
          GestureDetector(
            onTap: () => _joinOtherPeopleRoomOnTap(member: widget.memberModel),
            child: SizedBox(
              height: 55.h,
              width: Device.width - 20.w,
              child: Stack(
                alignment: Alignment.bottomCenter,
                children: [
                  Container(
                    height: 50.h,
                    width: 90.w,
                    alignment: Alignment.bottomCenter,
                    child: FutureBuilder(
                      builder: (context, AsyncSnapshot<dynamic> snapshot) {
                        if(snapshot.connectionState == ConnectionState.done){
                          if(snapshot.data == null){
                            return Container();
                          }
                          if(snapshot.data!.toString().contains('https')){
                            log('這裡有刷新嗎?001');
                            return CachedNetworkImage(
                              fit: BoxFit.contain,
                              filterQuality: FilterQuality.medium,
                              imageUrl: snapshot.data!.toString(),
                            );
                          } else {
                            log('這裡有刷新嗎?002');
                            return Image(
                              fit: BoxFit.contain,
                              filterQuality: FilterQuality.medium,
                              image: FileImage(File(snapshot.data!.toString()),scale: 0.1),
                            );
                          }
                        }
                        return Container(
                          alignment: Alignment.center,
                          child: Image(
                              fit: BoxFit.contain,
                              filterQuality: FilterQuality.medium,
                              image: AssetImage(AppImage.loading)
                          ),
                        );
                      },
                      future: /*_memoizer.runOnce(() async {
                        return await _streetProvider!.getStreetMemberHouseByMemberId(id: widget.memberModel.id!, house: _gameHomeProvider!.allHouseStyleList);
                      })*/ _streetProvider!.getStreetMemberHouseByMemberId(id: widget.memberModel.id!, house: _gameHomeProvider!.allHouseStyleList),
                    ),
                  ),
                  //煙
                  Positioned(
                    top: 10.h,
                    child: SizedBox(
                      width: Device.width - 20.w,
                      child:
                      FutureBuilder(
                        initialData: Image(
                          fit: BoxFit.contain,
                          filterQuality: FilterQuality.medium,
                          image: AssetImage(AppImage.imgSmoke),
                        ),
                        builder: (context, AsyncSnapshot<dynamic> snapshot){
                          if(snapshot.connectionState == ConnectionState.done){
                            if(snapshot.data!.toString() == ''){
                              return Container();
                            } else {
                              return Image(
                                fit: BoxFit.contain,
                                filterQuality: FilterQuality.medium,
                                image: AssetImage(snapshot.data!.toString()),
                              );
                            }
                          }
                          return Container();
                        },
                        future: _memoizerSmoke.runOnce(() async {
                          return await _streetProvider!.getStreetMemberHousePoopByMemberHouseId(id: widget.memberModel.id!);
                        }),
                      ),
                    ),
                  ),
                  //我
                  if(_memberProvider!.memberModel.id == widget.memberModel.id!)
                    Positioned(
                      top: 7.h,
                      child: Container(
                        child: FittedBox(
                          child: Text(
                            S.of(context).me,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 18.sp,
                                color: AppColor.meColor),
                          ),
                        ),
                        margin: const EdgeInsets.only(left: 10, bottom: 10),
                      ),
                    ),
                  if(_memberProvider!.memberModel.id != widget.memberModel.id!)
                    Positioned(
                      top: 7.h,
                      child: _distanceBetween(model: widget.memberModel),
                    ),
                  //會員資訊
                  Positioned(
                    top: 12.h,
                    child: SizedBox(
                      height: 8.h,
                      child: _memberCard(model: widget.memberModel,
                          gender: widget.memberModel.gender! == 0 ? GenderStatus.boy : GenderStatus.girl, name: widget.memberModel.nickname!, birthday: widget.memberModel.birthday!),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  //距離
  Widget _distanceBetween({required MemberModel model}){
    LatLng _myLatLng = LatLng(
        _memberProvider!.memberModel.latitude!,
        _memberProvider!.memberModel.longitude!);
    LatLng _otherLatLng = LatLng(model.latitude!, model.longitude!);
    //log('座標: ${model.latitude} ${model.longitude} 距離: ${_getDistanceBetween(_myLatLng, _otherLatLng)}');
    return Container(
      child: FittedBox(
        child: Text(
          _getDistanceBetween(_myLatLng, _otherLatLng),
          style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 18.sp,
              color: AppColor.meColor),
        ),
      ),
      margin: const EdgeInsets.only(left: 10, bottom: 10),
    );
  }

  //距離計算
  String _getDistanceBetween(LatLng _myLatLng, LatLng _visitLatLng) {
    String _value = '';
    double _distance = Geolocator.distanceBetween(_myLatLng.latitude,
        _myLatLng.longitude, _visitLatLng.latitude, _visitLatLng.longitude);
    if (_distance > 1000) {
      _value = '${(_distance / 1000).toStringAsFixed(2)}${S.of(context).km}';
    } else {
      _value = '${_distance.toStringAsFixed(2)}${S.of(context).m}';
    }
    return _value;
  }

  //會員資訊
  Widget _memberCard(
      {
        required MemberModel model,
        required GenderStatus gender,
        required String name,
        required String birthday}) {
    return Row(
      children: [
        //大頭照
        MugshotWidget(
          fromMemberId: _memberProvider!.memberModel.id!,
          targetMemberId: model.id!,
          nickName: name,
          gender: model.gender!,
          birthday: birthday,
        ),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            //名稱
            Container(
              child: FittedBox(
                child: Text(
                  name,
                  style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 18.sp,
                      color: Colors.black),
                ),
              ),
              margin: const EdgeInsets.only(left: 10),
            ),
            //性別年齡
            Container(
              margin: const EdgeInsets.only(left: 10),
              child: Row(
                children: [
                  getGender(gender.index),
                  //年齡
                  Container(
                    margin: const EdgeInsets.only(left: 5),
                    child: Text(
                      Util().getAge(birthday),
                      style: const TextStyle(color: Colors.white),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ],
    );
  }

  //進入別人房間
  void _joinOtherPeopleRoomOnTap({required MemberModel member}) {
    if(EasyLoading.isShow){
      return;
    }
    if(_memberProvider!.memberModel.id! == member.id!){
      _houseOnTap();
    } else {
      EasyLoading.show(status: 'Loading...');
      Future.wait([
        _gameHomeProvider!.getStreetMemberPetsByMemberId(id: member.id!, refresh: false),
        _gameHomeProvider!.getStreetMemberFurnitureByMemberId(id: member.id!, refresh: false),
        _gameHomeProvider!.getThisPeopleHouseByMemberId(id: member.id!).then((value) {
          //用memberId取得陌生人家留言
          _gameHomeProvider!.getStreetMemberHouseMessage(id: _gameHomeProvider!.thisPeopleModel.id!);
          //用memberId取得陌生人家大便
          _gameHomeProvider!.getStreetMemberDungByMemberHouseId(id: _gameHomeProvider!.thisPeopleModel.id!);
        }),
      ]).then((value) {
        Future.delayed(const Duration(milliseconds: 1000), (){
          EasyLoading.dismiss();
          delegate.push(name: RouteName.gameHomeOtherPeoplePage, arguments: member);
        });

      });
    }
  }

  //房子按鈕OnTap
  void _houseOnTap() async {
    if(EasyLoading.isShow){
      return;
    }
    _gameHomeProvider!.tidyMemberFurniture(refresh: true);
    _gameHomeProvider!.tidyMemberHouse(refresh: true);
    _gameHomeProvider!.tidyMemberPets(refresh: true);
    delegate.push(name: RouteName.gameHomePage);
  }
}