
import 'package:flutter/cupertino.dart';
import 'package:flutter_font_icons/flutter_font_icons.dart';

import '../utils/app_color.dart';
//性別Icon
StatelessWidget getGender(int _gender) {
  switch (_gender) {
  //男
    case 0:
      return const Icon(
        FontAwesome5Solid.mars,
        size: 20,
        color: AppColor.maleColor,
      );
  //女
    case 1:
      return const Icon(
        FontAwesome5Solid.venus,
        size: 20,
        color: AppColor.femaleColor,
      );
    default:
      return Container();
  }
}