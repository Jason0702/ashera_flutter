import 'dart:developer';

import 'package:ashera_flutter/utils/api.dart';
import 'package:async/async.dart';

import 'package:ashera_flutter/provider/game_home_provider.dart';
import 'package:ashera_flutter/provider/unlock_mugshot_provider.dart';
import 'package:ashera_flutter/provider/visit_game_provider.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../app.dart';
import '../dialog/game_point_insufficient_dialog.dart';
import '../dialog/identify_friends_can_go_home_dialog.dart';
import '../dialog/unblock_mugshot_dialog.dart';
import '../enum/app_enum.dart';
import '../generated/l10n.dart';
import '../models/add_unlock_payment_record_dto.dart';
import '../models/member_model.dart';
import '../provider/member_provider.dart';
import '../provider/stomp_client_provider.dart';
import '../provider/util_api_provider.dart';
import '../routers/route_name.dart';
import '../utils/app_color.dart';
import '../utils/app_image.dart';
import '../utils/util.dart';
import 'image_shower.dart';

class MugshotWidget extends StatefulWidget{
  final int fromMemberId;
  final int targetMemberId;
  final String nickName;
  final int gender;
  final String birthday;
  final bool? interactive;
  final bool? inGame;
  final String? interactivePic;
  final bool? customerService;

  const MugshotWidget({
    Key? key,
    required this.fromMemberId,
    required this.targetMemberId,
    required this.nickName,
    required this.gender,
    required this.birthday,
    this.interactive,
    this.inGame,
    this.interactivePic,
    this.customerService
  }): super(key: key);

  @override
  State createState() => _MugshotWidgetState();
}

class _MugshotWidgetState extends State<MugshotWidget>{
  MemberProvider? _memberProvider;
  UtilApiProvider? _utilApiProvider;
  StompClientProvider? _stompClientProvider;
  VisitGameProvider? _visitGameProvider;
  UnlockMugshotProvider? _unlockMugshotProvider;
  GameHomeProvider? _gameHomeProvider;

  final AsyncMemoizer _memoizerVIP = AsyncMemoizer();

  @override
  Widget build(BuildContext context) {
    _memberProvider = Provider.of<MemberProvider>(context, listen: false);
    _stompClientProvider = Provider.of<StompClientProvider>(context, listen: false);
    _utilApiProvider = Provider.of<UtilApiProvider>(context, listen: false);
    _visitGameProvider = Provider.of<VisitGameProvider>(context, listen: false);
    _unlockMugshotProvider = Provider.of<UnlockMugshotProvider>(context);
    _gameHomeProvider = Provider.of<GameHomeProvider>(context, listen: false);
    if(widget.fromMemberId == widget.targetMemberId){
      //log('我自己的照片');
      if(_memberProvider!.memberModel.mugshot!.isEmpty){
        return _getAvatar(GenderStatus.girl.index);
      }
      return GestureDetector(
        onTap: () => showImage(files: widget.interactive == null ?  _memberProvider!.memberModel.mugshot! : _memberProvider!.memberModel.interactivePic!),
        child: Container(
          width: 13.w,
          height: 9.h,
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              image: DecorationImage(
                  fit: BoxFit.cover,
                  image: CachedNetworkImageProvider(
                    Util().getMemberMugshotUrl( widget.interactive == null ? _memberProvider!.memberModel.mugshot! : _memberProvider!.memberModel.interactivePic!),
                    headers: {"authorization": "Bearer " + Api.accessToken},
                  ),
                  onError: (error, stackTrace) {
                    debugPrint('Error: ${error.toString()}');
                  })),
        ),
      );
    } else if(widget.customerService != null){
      if(_memberProvider!.customerService!.mugshot!.isEmpty){
        return Container(
          width: 13.w,
          height: 9.h,
          decoration: BoxDecoration(
              shape: BoxShape.circle, gradient: AppColor.appMainColor),
          child: CircleAvatar(
            backgroundColor: Colors.transparent,
            child: Image.asset(AppImage.iconCat),
          ),
        );
      }
      return Container(
        width: 13.w,
        height: 9.h,
        decoration: BoxDecoration(
            shape: BoxShape.circle,
            image: DecorationImage(
                fit: BoxFit.cover,
                image: CachedNetworkImageProvider(
                  Util().getMemberMugshotUrl(_memberProvider!.customerService!.mugshot!),
                  headers: {"authorization": "Bearer " + Api.accessToken},
                ),
                onError: (error, stackTrace) {
                  debugPrint('Error: ${error.toString()}');
                })),
      );
    } else if (_stompClientProvider!.friendList
        .where((element) => element.id == widget.targetMemberId)
        .isNotEmpty) {
      //log('使用好友照片');
      MemberModel _model = _stompClientProvider!.friendList
          .where((element) => element.id == widget.targetMemberId).first;
      String _url = widget.interactive != null ? _model.interactivePic! : _model.mugshot!;
      if(_url.isNotEmpty){
        if(widget.inGame != null){
          return GestureDetector(
            onTap: () => _addFriendDialog(widget.targetMemberId),
            child: Container(
              width: 13.w,
              height: 9.h,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                      fit: BoxFit.cover,
                      image: CachedNetworkImageProvider(
                        Util().getMemberMugshotUrl(_url),
                        headers: {"authorization": "Bearer " + Api.accessToken},
                      ),
                      onError: (error, stackTrace) {
                        debugPrint('Error: ${error.toString()}');
                      })),
            ),
          );
        }else{
          return GestureDetector(
            onTap: () => _addFriendDialog(widget.targetMemberId) /*showImage(files: _url)*/,
            child: Container(
              width: 13.w,
              height: 9.h,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                      fit: BoxFit.cover,
                      image: CachedNetworkImageProvider(
                        Util().getMemberMugshotUrl(_url),
                        headers: {"authorization": "Bearer " + Api.accessToken},
                      ),
                      onError: (error, stackTrace) {
                        debugPrint('Error: ${error.toString()}');
                      })),
            ),
          );
        }
      }else{
        return GestureDetector(
          onTap: () => _addFriendDialog(widget.targetMemberId),
          child: _getAvatar(widget.gender),
        );
      }
    } else if (_unlockMugshotProvider!.memberMugshotList
        .where((element) => element.id == widget.targetMemberId)
        .isNotEmpty) {
      //log('使用解鎖照片');
      if(widget.interactive == null){
        if(_unlockMugshotProvider!.memberMugshotList.firstWhere((element) => element.id == widget.targetMemberId).mugshot!.isNotEmpty){
          if(widget.inGame != null){
            return GestureDetector(
              onTap: () => _addFriendDialog(widget.targetMemberId),
              child: Container(
                width: 13.w,
                height: 9.h,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        fit: BoxFit.cover,
                        image: CachedNetworkImageProvider(
                          Util().getMemberMugshotUrl(_unlockMugshotProvider!.memberMugshotList
                              .where((element) => element.id == widget.targetMemberId)
                              .first
                              .mugshot!),
                          headers: {"authorization": "Bearer " + Api.accessToken},
                        ),
                        onError: (error, stackTrace) {
                          debugPrint('Error: ${error.toString()}');
                        })),
              ),
            );
          }else{
            return GestureDetector(
              onTap: () => _addFriendDialog(widget.targetMemberId) /*showImage(files: _unlockMugshotProvider!.memberMugshotList.firstWhere((element) => element.id == widget.targetMemberId).mugshot!)*/,
              child: Container(
                width: 13.w,
                height: 9.h,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        fit: BoxFit.cover,
                        image: CachedNetworkImageProvider(
                          Util().getMemberMugshotUrl(_unlockMugshotProvider!.memberMugshotList
                              .where((element) => element.id == widget.targetMemberId)
                              .first
                              .mugshot!),
                          headers: {"authorization": "Bearer " + Api.accessToken},
                        ),
                        onError: (error, stackTrace) {
                          debugPrint('Error: ${error.toString()}');
                        })),
              ),
            );
          }
        } else {
          return _getAvatar(widget.gender);
        }
      }else{
        if(_unlockMugshotProvider!.memberMugshotList.firstWhere((element) => element.id == widget.targetMemberId).interactivePic!.isNotEmpty){
          return GestureDetector(
            onTap: () => _addFriendDialog(widget.targetMemberId) /*showImage(files: _unlockMugshotProvider!.memberMugshotList.firstWhere((element) => element.id == widget.targetMemberId).interactivePic!)*/,
            child: Container(
              width: 13.w,
              height: 9.h,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  image: DecorationImage(
                      fit: BoxFit.cover,
                      image: CachedNetworkImageProvider(
                        Util().getMemberMugshotUrl(_unlockMugshotProvider!.memberMugshotList
                            .where((element) => element.id == widget.targetMemberId)
                            .first
                            .interactivePic!),
                        headers: {"authorization": "Bearer " + Api.accessToken},
                      ),
                      onError: (error, stackTrace) {
                        debugPrint('Error: ${error.toString()}');
                      })),
            ),
          );
        }else{
          return _getAvatar(widget.gender);
        }
      }
    } else if(_memberProvider!.memberModel.vip == VIPStatus.closure.index) {
      //log('輪到誰了: ${widget.targetMemberId} ${widget.nickName}');
      if(widget.interactivePic != null){
        if(widget.interactivePic!.isNotEmpty){
          if(widget.interactive != null){
            return GestureDetector(
              onTap: () => _addFriendDialog(widget.targetMemberId),
              child: Container(
                  width: 13.w,
                  height: 9.h,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          fit: BoxFit.cover,
                          image: CachedNetworkImageProvider(
                            Util().getMemberMugshotUrl(widget.interactivePic!),
                            headers: {"authorization": "Bearer " + Api.accessToken},
                          ),
                          onError: (error, stackTrace) {
                            debugPrint('Error: ${error.toString()}');
                          }))),
            );
          }
        }
      }
      return FutureBuilder(
          future: _memoizerVIP.runOnce(() async {
            return await _visitGameProvider!.getMugshotMemberById(id: widget.targetMemberId, interactive: widget.interactive != null);
          }),
          builder: (context, AsyncSnapshot<dynamic> snapshot){
            if(snapshot.connectionState == ConnectionState.done){
              if(snapshot.data!.toString().isNotEmpty){
                if(widget.inGame != null){
                  return GestureDetector(
                    onTap: () => _addFriendDialog(widget.targetMemberId),
                    child: Container(
                        width: 13.w,
                        height: 9.h,
                        decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                                fit: BoxFit.cover,
                                image: CachedNetworkImageProvider(
                                  Util().getMemberMugshotUrl(snapshot.data!),
                                  headers: {"authorization": "Bearer " + Api.accessToken},
                                ),
                                onError: (error, stackTrace) {
                                  debugPrint('Error: ${error.toString()}');
                                }))),
                  );
                }else{
                  return Container(
                      width: 13.w,
                      height: 9.h,
                      decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              fit: BoxFit.cover,
                              image: CachedNetworkImageProvider(
                                Util().getMemberMugshotUrl(snapshot.data!),
                                headers: {"authorization": "Bearer " + Api.accessToken},
                              ),
                              onError: (error, stackTrace) {
                                debugPrint('Error: ${error.toString()}');
                              })));
                }
              } else {
                return _getAvatar(widget.gender);
              }
            }
            return Container(
              width: 13.w,
              height: 9.h,
              decoration: BoxDecoration(
                  shape: BoxShape.circle, gradient: AppColor.appMainColor),
              child: CircleAvatar(
                  backgroundColor: Colors.transparent,
                  child: Image.asset(AppImage.iconCat)
              ),
            );
          });
    } else {
      //log('貓或狗');
      return GestureDetector(
        onTap: () => unblockMemberMugshot(
            widget.fromMemberId, widget.nickName, widget.gender, widget.birthday, widget.targetMemberId),
        child: _getAvatar(widget.gender),
      );
    }
  }

  ///照片放大功能
  void showImage({required String files}){
    showGeneralDialog(
        context: context,
        pageBuilder: (context, animation, secondaryAnimation) =>
            ImageShower(data: [files], index: 0),
        transitionBuilder: (context, animation, secondaryAnimation, child) {
          const begin = Offset(0.0, 1.0);
          const end = Offset.zero;
          const curve = Curves.ease;
          var tween =
          Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
          return SlideTransition(
            position: animation.drive(tween),
            child: child,
          );
        });
  }

  //解鎖大頭照
  Future<void> unblockMemberMugshot(int fromMemberId, String nickName,
      int gender, String birthday, int targetMemberId) async {
    if (_isFriend(id: targetMemberId)) {
      showImage(files: widget.interactive == null ? _stompClientProvider!.friendList.firstWhere((element) => element.id == targetMemberId).mugshot! : _stompClientProvider!.friendList.firstWhere((element) => element.id == targetMemberId).interactivePic!);
      return;
    } else if (_isUnlock(id: targetMemberId)) {
      showImage(files: widget.interactive == null ? _unlockMugshotProvider!.memberMugshotList.firstWhere((element) => element.id == targetMemberId).mugshot! : _unlockMugshotProvider!.memberMugshotList.firstWhere((element) => element.id == targetMemberId).interactivePic!);
      return;
    } else {
      bool? result = await showGeneralDialog(
          context: context,
          pageBuilder: (context, anim1, anim2) {
            return UnblockMugshotDialog(
              key: UniqueKey(),
              nickName: nickName,
              gender: gender,
              birthday: birthday,
            );
          },
          barrierColor: Colors.black54,
          barrierDismissible: false,
          transitionDuration: const Duration(milliseconds: 100),
          transitionBuilder: (context, anim1, anim2, child) {
            return Transform.scale(
              scale: anim1.value,
              child: child,
            );
          });
      if (result != null) {
        if (result) {
          //想要解鎖
          if (_memberProvider!.memberPoint.point! >= _utilApiProvider!.systemSetting!.unlockMugshotPoints!) {
            //點數判斷夠不夠
            AddUnlockPaymentRecordDTO _addUnlockPaymentRecordDTO =
            AddUnlockPaymentRecordDTO(
                fromMemberId: fromMemberId, targetMemberId: targetMemberId);
            _stompClientProvider!.sendUnlockMugshot(
                addUnlockPaymentRecordDTO: _addUnlockPaymentRecordDTO);
          } else {
            if(_utilApiProvider!.systemSetting!.addPointsStatus == PointsStatus.closure.index){
              EasyLoading.showToast(S.of(context).not_enough_points);
            }else{
              //點數不足
              _pointInsufficient();
            }
          }
        }
      }
    }
  }

  //判斷是否是朋友
  bool _isFriend({required int id}) {
    return _stompClientProvider!.friendList
        .where((element) => element.id == id)
        .isNotEmpty;
  }

  //判斷此人是否有解鎖過
  bool _isUnlock({required int id}) {
    return _unlockMugshotProvider!.memberMugshotList
        .where((element) => element.id == id)
        .isNotEmpty;
  }

  ///點數不足統一方法
  void _pointInsufficient() async {
    bool? result = await _showPointInsufficient();
    if (result != null) {
      if (result) {
        //跳轉加值
        delegate.push(name: RouteName.myPointsPage);
      }
    }
  }

  ///點數不足
  Future<bool?> _showPointInsufficient() async {
    return await showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2) {
          return GamePointInsufficientDialog(
              key: UniqueKey(),
              title: S.of(context).message,
              content: S.of(context).please_add_value,
              confirmButtonText: S.of(context).go_to_premium,
              cancelButtonText: S.of(context).cancel);
        },
        barrierColor: Colors.black54,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 100),
        transitionBuilder: (context, anim1, anim2, child) {
          return child;
        });
  }

  //取得角色大頭貼
  Widget _getAvatar(int _gender) {
    switch (_gender) {
    //男
      case 0:
        return Container(
          width: 13.w,
          height: 9.h,
          decoration: BoxDecoration(
              shape: BoxShape.circle, gradient: AppColor.appMainColor),
          child: CircleAvatar(
            backgroundColor: Colors.transparent,
            child: Image.asset(AppImage.iconDog),
          ),
        );
    //女
      case 1:
        return Container(
          width: 13.w,
          height: 9.h,
          decoration: BoxDecoration(
              shape: BoxShape.circle, gradient: AppColor.appMainColor),
          child: CircleAvatar(
            backgroundColor: Colors.transparent,
            child: Image.asset(AppImage.iconCat),
          ),
        );
      default:
        return Container(
          width: 13.w,
          height: 9.h,
          decoration: BoxDecoration(
              shape: BoxShape.circle, gradient: AppColor.appMainColor),
          child: CircleAvatar(
            backgroundColor: Colors.transparent,
            child: Image.asset(AppImage.iconCat),
          ),
        );
    }
  }

  //加好友對話框
  void _addFriendDialog(int id) async {
    bool? _result = await showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2){
          return IdentifyFriendsCanGoHomeDialog(
            key: UniqueKey(),
            token: Api.accessToken,
            id: id,
          );
        },
        barrierColor: Colors.transparent,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 200),
        transitionBuilder: (context, anim1, anim2, child){
          return Transform.scale(
            scale: anim1.value,
            child: child,
          );
        }
    );
    if(_result != null){
      if(_result){
        //要去別人家
        EasyLoading.show(status: 'Loading...');
        Future.wait([
          _memberProvider!.getMemberByIdToMemberModel(id: id),
          _gameHomeProvider!.getStreetMemberPetsByMemberId(id: id, refresh: false),
          _gameHomeProvider!.getStreetMemberFurnitureByMemberId(id: id, refresh: false),
          _gameHomeProvider!.getThisPeopleHouseByMemberId(id: id).then((value) {
            //用memberId取得陌生人家留言
            _gameHomeProvider!.getStreetMemberHouseMessage(id: _gameHomeProvider!.thisPeopleModel.id!);
            //用memberId取得陌生人家大便
            _gameHomeProvider!.getStreetMemberDungByMemberHouseId(id: _gameHomeProvider!.thisPeopleModel.id!);
          }),
        ]).then((value) {
          Future.delayed(const Duration(milliseconds: 1000), (){
            EasyLoading.dismiss();
            delegate.push(name: RouteName.gameHomeOtherPeoplePage, arguments: value[0] as MemberModel);
          });

        });
      }
    }
  }
}