import 'dart:developer';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import 'package:flutter_font_icons/flutter_font_icons.dart';

import '../dialog/identify_friends_byId_dialog.dart';
import '../enum/app_enum.dart';
import '../models/faces_detect_history_model.dart';
import '../provider/identification_provider.dart';
import '../utils/api.dart';
import '../utils/app_color.dart';
import '../utils/app_image.dart';
import '../utils/util.dart';

class IdentificationListViewCard extends StatefulWidget{
  final FacesDetectHistoryModel data;
  const IdentificationListViewCard({Key? key, required this.data}): super(key: key);

  @override
  State<StatefulWidget> createState() => _IdentificationListViewCardState();
}
class _IdentificationListViewCardState extends State<IdentificationListViewCard> with AutomaticKeepAliveClientMixin{

  FacesDetectHistoryModel get data => widget.data;

  @override
  bool get wantKeepAlive => true;


  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      decoration: const BoxDecoration(
          border:
          Border(bottom: BorderSide(width: 1, color: AppColor.grayLine))),
      child: Consumer<IdentificationProvider>(
        builder: (context, record, _) {
          return Row(
            children: [
              //頭像
              _avatarImageJudge(record.facesMember),
              Expanded(
                child: Container(
                  margin: const EdgeInsets.only(left: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          //名稱
                          FittedBox(
                            child: Text(
                              record.facesMember ==
                                  FacesMember.facesTargetMember
                                  ? data.fromMember!.nickname!
                                  : data.targetMember!.nickname!,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18.sp,
                                  color: Colors.black),
                            ),
                          ),
                          //時間
                          FittedBox(
                            alignment: Alignment.topCenter,
                            child: Text(
                              _getTime(data.createdAt!.ceil()),
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15.sp,
                                  color: AppColor.grayText),
                            ),
                          ),
                        ],
                      ),
                      //地址
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          const Icon(
                            Ionicons.ios_location_sharp,
                            color: AppColor.yellowIcon,
                          ),
                          FittedBox(
                            child: Text(
                              data.address!,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15.sp,
                                  color: AppColor.grayText),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          );
        },
      ),
    );
  }
  //頭像判斷
  Widget _avatarImageJudge(FacesMember facesMember){
    if(facesMember == FacesMember.facesTargetMember){
      return GestureDetector(
        onTap: () => _avatarOnTap(data.fromMember!.id!),
        child: _avatarImage(data.fromMember!.mugshot ?? ''),
      );
    }
    return GestureDetector(
      onTap: () => _avatarOnTap(data.targetMember!.id!),
      child: _avatarImage(data.targetMember!.mugshot ?? ''),
    );
  }
  //頭像
  Widget _avatarImage(String _url){
    if(_url.isNotEmpty){
      return Container(
          width: 13.w,
          height: 9.h,
          decoration: BoxDecoration(
              shape: BoxShape.circle,
              image: DecorationImage(
                  fit: BoxFit.cover,
                  image: CachedNetworkImageProvider(
                    Util().getMemberMugshotUrl(_url),
                    headers: {
                      "authorization":
                      "Bearer " + Api.accessToken
                    },
                  ),
                  onError: (error, stackTrace) {
                    debugPrint('Error: ${error.toString()}');
                  })));
    }
    return Container(
      width: 13.w,
      height: 9.h,
      decoration: BoxDecoration(
          shape: BoxShape.circle,
          gradient: AppColor.appMainColor),
      child: CircleAvatar(
          backgroundColor: Colors.transparent,
          child: Image.asset(AppImage.iconCat)),
    );
  }

  void _avatarOnTap(int id){
    showGeneralDialog(
        context: context,
        pageBuilder: (context, anim1, anim2){
          return IdentifyFriendsByIdDialog(
            key: UniqueKey(),
            id: id,
          );
        },
        barrierColor: Colors.transparent,
        barrierDismissible: false,
        transitionDuration: const Duration(milliseconds: 200),
        transitionBuilder: (context, anim1, anim2, child){
          return Transform.scale(
            scale: anim1.value,
            child: child,
          );
        }
    );
  }

  String _getTime(int _time) {
    DateTime _createdAt =
    DateTime.fromMillisecondsSinceEpoch(_time * 1000, isUtc: false);
    return Util().visitRecordTime(_createdAt);
  }
}