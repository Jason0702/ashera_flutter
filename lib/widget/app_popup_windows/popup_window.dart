
import 'app_popup_menu.dart';
import 'popup_menu_copy.dart';
class PopupWindows<T> extends AppPopupMenu<T> {
  PopupWindows({
    Key? key,
    List<T>? items,
    List<PopupMenuItem<T>>? menuItems,
    PopupMenuItemBuilder<T>? itemBuilder,
    T? initialValue,
    PopupMenuItemSelected<T>? onSelected,
    PopupMenuCanceled? onCanceled,
    String? tooltip,
    double? elevation,
    EdgeInsetsGeometry? padding,
    Widget? child,
    Widget? icon,
    Offset? offset,
    bool? enabled,
    ShapeBorder? shape,
    Color? color,
  }) : super(
    key: key,
    items: items,
    menuItems: menuItems,
    itemBuilder: itemBuilder,
    initialValue: initialValue,
    onSelected: onSelected,
    onCanceled: onCanceled,
    tooltip: tooltip,
    elevation: elevation,
    padding: padding,
    child: child,
    icon: icon,
    offset: offset,
    enabled: enabled,
    shape: shape,
    color: color,
  );
}