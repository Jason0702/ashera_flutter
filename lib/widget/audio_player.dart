import 'dart:async';
import 'dart:developer';
import 'package:fijkplayer/fijkplayer.dart';
import 'package:flutter/material.dart';

class RecordPlayer extends StatefulWidget {
  const RecordPlayer({Key? key,required this.path}) : super(key: key);
  final String? path;
  @override
  _RecordPlayerState createState() => _RecordPlayerState();
}

class _RecordPlayerState extends State<RecordPlayer>  with WidgetsBindingObserver{
  final FijkPlayer player = FijkPlayer();
  String? get path => widget.path;
  bool isPlayed = false;
  String maxDuration = "00:00";
  String playDuration = "00:00";
  late StreamSubscription currentPos;
  bool isFirst = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    WidgetsBinding.instance.addObserver(this);
    iniPlayer();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    // TODO: implement didChangeAppLifecycleState
    super.didChangeAppLifecycleState(state);
    if (state == AppLifecycleState.paused) {
      player.pause();
      isPlayed = false;
    }
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    player.removeListener(_valueChangeListener);
    currentPos.cancel();
    player.release();
  }

  void _valueChangeListener(){
    FijkValue value = player.value;
    //log("${value.duration} ${value.state}");
    if(value.state == FijkState.started && !isFirst){
      //log("player pause");
      player.pause();
      isFirst = true;
    }
    if(value.state == FijkState.completed){
      isPlayed = false;
    }
    maxDuration = value.duration.toString().substring(2,7);
    setState(() {

    });
  }

  void iniPlayer(){
    player.setDataSource(path!,autoPlay: true);
    player.addListener(_valueChangeListener);
    currentPos = player.onCurrentPosUpdate.listen((event) {
      playDuration = event.toString().substring(2,7);
      setState(() {
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        if(isPlayed){
          isPlayed = false;
          _stopRecord();
        }else{
          isPlayed = true;
          _playRecord();
        }
        setState(() {

        });
      },
      child: Row(
        children: [
          Container(
            padding: const EdgeInsets.all(5),
            decoration: const BoxDecoration(
                color: Colors.black,
                shape: BoxShape.circle
            ),
            child: Container(
                decoration: const BoxDecoration(
                    color: Colors.white,
                    shape: BoxShape.circle
                ),

                child: Icon(
                  !isPlayed?
                  Icons.play_arrow_rounded :
                  Icons.pause_rounded
                  ,size: 20,)),
          ),
          const SizedBox(width:1,),
          !isPlayed && playDuration == "00:00" ?Text(maxDuration, style: const TextStyle(color: Colors.black, fontSize: 20),):
          Text(playDuration, style: const TextStyle(color: Colors.black, fontSize: 20),)
        ],
      ),
    );
  }

  void _playRecord() async{
    //log('開始聽取');
    player.start();
  }

  void _stopRecord(){
    //log('停止聽取');
    player.pause();
  }
}
