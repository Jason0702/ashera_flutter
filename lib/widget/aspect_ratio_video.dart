import 'dart:developer';
import 'dart:io';

import 'package:ashera_flutter/utils/api.dart';
import 'package:ashera_flutter/utils/app_color.dart';
import 'package:fijkplayer/fijkplayer.dart';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:video_player/video_player.dart';
import 'package:video_thumbnail/video_thumbnail.dart';

class AspectRatioVideo extends StatefulWidget {
  const AspectRatioVideo({Key? key, required this.list}) : super(key: key);

  final String? list;

  @override
  AspectRatioVideoState createState() => AspectRatioVideoState();
}

class AspectRatioVideoState extends State<AspectRatioVideo> {
  String? get list => widget.list;
  bool initialized = false;
  VideoPlayerController? controller;
  bool hasError = false;
  bool _isExists = false;
  String _imagePath = '';

  void _onVideoControllerUpdate() {
    if (!mounted) {
      return;
    }
    if (initialized != controller!.value.isInitialized) {
      initialized = controller!.value.isInitialized;
      setState(() {});
    }
    if(!controller!.value.isPlaying){
      setState(() {

      });
    }
    if (hasError != controller!.value.hasError) {
      hasError = controller!.value.hasError;
      setState(() {});
    }
  }

  @override
  void initState() {
    super.initState();
    controller = VideoPlayerController.network(list!, httpHeaders: {"authorization": "Bearer " + Api.accessToken});
    controller?.addListener(_onVideoControllerUpdate);
    controller?.initialize();
    _getFileImage(widget.list!).then((value) {
      //log('影片暫存圖: $value');
    });
  }
  //儲存影片暫存圖
  Future<String?> _getFileImage(String _url) async {
    //log('圖片Url: $_url');
    return await VideoThumbnail.thumbnailFile(
        headers: {"authorization": "Bearer " + Api.accessToken},
        video: _url,
        thumbnailPath: (await getTemporaryDirectory()).path,
        imageFormat: ImageFormat.JPEG,
        maxHeight: 350,
        quality: 100,
    ) ?? '';
  }
  //判斷是否有此影片暫存檔
  Future<Map<String, dynamic>> _isExistsImage(String _url)async{
    String _keepFileName = _url.split('/').last.toString().split('.').first;
    //log('保留檔案名稱: $_keepFileName');
    String _path = '${(await getTemporaryDirectory()).path}/$_keepFileName.jpg';
    //log('暫存檔案名稱: $_path');
    if(await File(_path).exists()){
      ///文件在
      //log('有暫存圖');
      _isExists = true;
      _imagePath = _path;
      return {'status': true, 'path': _path};
    } else {
      ///文件不在
      //log('沒有暫存圖');
      _isExists = false;
      return {'status': false, 'path': ''};
    }
  }

  @override
  void dispose() {
    controller!.removeListener(_onVideoControllerUpdate);
    controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return _isExists //<-逼不得已
        ? Container(
      width: 200,
      height: 200,
      alignment: Alignment.center,
      child: GestureDetector(
        onTap: videoOnTap,
        child: Stack(
          alignment: Alignment.center,
          children: [
            Image(
              fit: BoxFit.fitWidth,
              gaplessPlayback: true,
              image: FileImage(File(_imagePath)),
            ),
            controller!.value.isPlaying ?
            Container():
            Center(
              child: Container(
                  decoration: const BoxDecoration(
                      color: Colors.white,
                      shape: BoxShape.circle
                  ),
                  child: const Icon(Icons.play_arrow_rounded,size: 40,)),
            )
          ],
        ),
      ),
    )
        : FutureBuilder(
        future: _isExistsImage(widget.list!),
        builder: (context, AsyncSnapshot<Map<String, dynamic>> snapshot){
          if(snapshot.connectionState == ConnectionState.done){
            if(snapshot.data!['status']){
              //log('使用暫存圖');
              return Container(
                width: 200,
                height: 200,
                alignment: Alignment.center,
                child: GestureDetector(
                  onTap: videoOnTap,
                  child: Stack(
                    alignment: Alignment.center,
                    children: [
                      Image(
                        fit: BoxFit.cover,
                        gaplessPlayback: true,
                        image: FileImage(File(snapshot.data!['path'])),
                      ),
                      controller!.value.isPlaying ?
                      Container():
                      Center(
                        child: Container(
                            decoration: const BoxDecoration(
                                color: Colors.white,
                                shape: BoxShape.circle
                            ),
                            child: const Icon(Icons.play_arrow_rounded,size: 40,)),
                      )
                    ],
                  ),
                ),
              );
            } else {
              //log('使用播放器圖');
              return initialized ?
              Container(
                width: 200,
                height: 200,
                alignment: Alignment.center,
                child: GestureDetector(
                  onTap: videoOnTap,
                  child: AspectRatio(
                      aspectRatio: controller!.value.aspectRatio,
                      child: Stack(
                        children: [
                          VideoPlayer(controller!),
                          controller!.value.isPlaying ?
                          Container():
                          Center(
                            child: Container(
                                decoration: const BoxDecoration(
                                    color: Colors.white,
                                    shape: BoxShape.circle
                                ),
                                child: const Icon(Icons.play_arrow_rounded,size: 40,)),
                          )
                        ],
                      )),
                ),
              ):
              hasError?
              GestureDetector(
                onTap: videoOnTap,
                child: SizedBox(
                    width: 200,
                    height: 200,
                    child: Center(
                      child: Container(
                          padding: const EdgeInsets.all(5),
                          decoration: const BoxDecoration(
                              color: Colors.black,
                              shape: BoxShape.circle
                          ),
                          child: Container(
                              decoration: const BoxDecoration(
                                  color: Colors.white,
                                  shape: BoxShape.circle
                              ),
                              child: const Icon(Icons.play_arrow_rounded,size: 40,))),
                    )
                ),
              ):
              GestureDetector(
                onTap: videoOnTap,
                child: const SizedBox(
                  width: 200,
                  height: 200,
                  child: Padding(
                    padding: EdgeInsets.all(40),
                    child: CircularProgressIndicator(
                      color: AppColor.buttonFrameColor,
                    ),
                  ),
                ),
              );
            }
          }
          return const SizedBox(
            width: 200,
            height: 200,
            child: Padding(
              padding: EdgeInsets.all(40),
              child: CircularProgressIndicator(
                color: AppColor.buttonFrameColor,
              ),
            ),
          );
        });
  }

  void videoOnTap(){
    showGeneralDialog(
        context: context,
        pageBuilder: (context, animation, secondaryAnimation) =>
            FullScreenPlayer(list:list),
        transitionBuilder: (context, animation, secondaryAnimation, child) {
          const begin = Offset(0.0, 1.0);
          const end = Offset.zero;
          const curve = Curves.ease;
          var tween =
          Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
          return SlideTransition(
            position: animation.drive(tween),
            child: child,
          );
        });
  }
}

class FullScreenPlayer extends StatefulWidget {
  const FullScreenPlayer({Key? key, this.list}) : super(key: key);
  final String? list;
  @override
  _FullScreenPlayerState createState() => _FullScreenPlayerState();
}

class _FullScreenPlayerState extends State<FullScreenPlayer> {
  String? get list => widget.list;
  final FijkPlayer player = FijkPlayer();


  @override
  void initState() {
    super.initState();
    _playInit();
  }

  void _playInit() async {
    await player.setOption(FijkOption.formatCategory, "headers", "authorization: Bearer " + Api.accessToken);
    await player.setDataSource(list!,autoPlay: true,showCover: true);
  }

  @override
  void dispose() {
    super.dispose();
    player.release();
  }

  @override
  Widget build(BuildContext context) {
    return BackButtonListener(
        child: SafeArea(
          bottom: false,
          child: Stack(
      children: [
        FijkView(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          player: player,
          fit: FijkFit.contain,
          fs: true,
          color: Colors.black,
        ),
        Positioned(
          left: 10,
          top: 10,
          child: GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: const Icon(
                Icons.arrow_back_ios_rounded,
                color: Colors.white,
                size: 30,
              )),
        ),
      ],
    ),),
        onBackButtonPressed: () => pop());
  }

  Future<bool> pop() async {
    Navigator.of(context).pop();
    return true;
  }
}
