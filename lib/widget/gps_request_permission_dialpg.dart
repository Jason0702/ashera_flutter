

import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class GPSRequestPermissionDialog extends StatefulWidget{
  const GPSRequestPermissionDialog({Key? key}): super(key: key);

  @override
  State createState() => _GPSRequestPermissionDialogState();
}

class _GPSRequestPermissionDialogState extends State<GPSRequestPermissionDialog> with WidgetsBindingObserver{


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.black54,
      body: Center(
        child: Container(
          width: 70.w,
          height: 35.h,
          decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: Colors.black, width: 1),
            borderRadius: BorderRadius.circular(20)
          ),
          padding: const EdgeInsets.all(10),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(flex: 3,child: Center(
                child: Text('請至設定開啟定位服務權限', textScaleFactor: 1, style: TextStyle(fontSize: 20.sp),),
              ),),
              Container(color: Colors.black, height: 0.1.h,),
              Expanded(child: GestureDetector(
                onTap: (){
                  Navigator.of(context).pop(true);
                },
                child: Center(
                  child: Text('前往', textScaleFactor: 1, style: TextStyle(color: Colors.blueAccent, fontSize: 20.sp),
                ),
              ),))
            ],
          ),
        ),
      ),
    );
  }
}