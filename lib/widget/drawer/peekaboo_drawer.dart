import 'dart:developer';

import 'package:ashera_flutter/provider/member_provider.dart';
import 'package:ashera_flutter/provider/peekaboo_game_provider.dart';
import 'package:ashera_flutter/utils/util.dart';
import 'package:ashera_flutter/widget/drawer/buttom_drawer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_font_icons/flutter_font_icons.dart';
import 'package:provider/provider.dart';

import '../../models/member_model.dart';
import '../../models/place.dart';
import '../../provider/drawer_vm.dart';
import '../../utils/app_color.dart';
import '../mugshot_widget.dart';

class PeekabooDrawer extends StatefulWidget{
  //左側抽屜key
  final BottomDrawerController globalKey;
  const PeekabooDrawer({Key? key, required this.globalKey}): super(key: key);

  @override
  State<StatefulWidget> createState() => _PeekabooDrawerState();
}

class _PeekabooDrawerState extends State<PeekabooDrawer>{
  PeekabooGameProvider? _peekabooGameProvider;
  MemberProvider? _memberProvider;
  DrawerVm? _drawerVm;
  final double _headerHeight = 0.0;

  _onLayoutDone(_){

  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  Widget build(BuildContext context) {
    _peekabooGameProvider = Provider.of<PeekabooGameProvider>(context);
    _memberProvider = Provider.of<MemberProvider>(context);
    _drawerVm = Provider.of<DrawerVm>(context);
    return BottomDrawer(
      header: Container(
        padding: const EdgeInsets.symmetric(vertical: 10, horizontal: 20),
        alignment: Alignment.centerLeft,
        decoration: const BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(topLeft: Radius.circular(12), topRight: Radius.circular(12))
        ),
        child: const FittedBox(
          fit: BoxFit.scaleDown,
          child: Text(
            '附近的人',
            style: TextStyle(
                color: Colors.black,
                fontSize: 18,
                fontWeight: FontWeight.bold
            ),
          ),
        ),
      ),
      body: _buildBottomDrawerBody(),
      headerHeight: _headerHeight,
      //drawerHeight: vm.bodyHeight,
      controller: widget.globalKey,
      callback: (bool _isOpened) {
        log('是否開啟: $_isOpened');
        if(_isOpened){
          setHeight(_peekabooGameProvider!.items);
        }
      },
      boxShadow: [
        BoxShadow(
          color: Colors.black.withOpacity(0.15),
          blurRadius: 60,
          spreadRadius: 5,
          offset: const Offset(2, -6), // changes position of shadow
        ),
      ],
    );
  }

  Widget _buildBottomDrawerBody(){
    return Consumer<PeekabooGameProvider>(builder: (context, pg, _){
      if(pg.items.isEmpty){
        return const Center(
          child: FittedBox(
            fit: BoxFit.scaleDown,
            child: Text('目前附近沒有人哦', style: TextStyle(fontSize: 18, color: Colors.black),),
          ),
        );
      }
      return Container(
        margin: const EdgeInsets.symmetric(horizontal: 15, vertical: 5),
        decoration: BoxDecoration(
          color: Colors.grey[200],
          borderRadius: BorderRadius.circular(15),
        ),
        child: ListView.builder(
            itemCount: pg.items.length,
            itemBuilder: (context, index) => _itemWidget(pg.items.elementAt(index))
        ),
      );
    },);
  }

  void setHeight(Iterable<Place> items){
    log('高度是否變更?');
    if(items.isEmpty){
      _drawerVm!.drawerBodyHeight(Util().drawerTwoHeight);
      return;
    }
    switch(items.length){
      case 2:
        log('雙人高度: ${Util().drawerTwoHeight}');
        _drawerVm!.drawerBodyHeight(Util().drawerTwoHeight);
        break;
      case 3:
        _drawerVm!.drawerBodyHeight(Util().drawerThreeHeight);
        break;
      case 4:
        _drawerVm!.drawerBodyHeight(Util().drawerFourHeight);
        break;
    }
  }

  Widget _itemWidget(Place value){
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20),
      child: Row(
        children: [
          //頭像
          _mugshot(value.model),
          //名稱與自介
          Flexible(child: _nameAndAbout(value.model))
        ],
      ),
    );
  }

  Widget _mugshot(MemberModel model){
    return MugshotWidget(
      fromMemberId: _memberProvider!.memberModel.id!,
      targetMemberId: model.id!,
      nickName: model.nickname!,
      gender: model.gender!,
      birthday: model.birthday!,
      interactive: true,
      interactivePic: model.interactivePic,);
  }

  Widget _nameAndAbout(MemberModel model){
    return Container(
      padding: const EdgeInsets.only(left: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              FittedBox(
                fit: BoxFit.scaleDown,
                child: Text(
                  model.nickname!,
                  style: const TextStyle(
                      color: Colors.black,
                      fontSize: 16
                  ),
                ),
              ),
              _maleOrFemaleIcon(gender: model.gender!)
            ],
          ),
          FittedBox(
            fit: BoxFit.scaleDown,
            child: Text(
               _getAbout(model.aboutMe),
              maxLines: 2,
              style: TextStyle(
                  color: Colors.grey[500],
                  fontSize: 16
              ),
            ),
          )
        ],
      ),
    );
  }

  String _getAbout(String? about){
    if(about == null){
      return '無';
    }else if(about.isEmpty){
      return '無';
    }else{
      return about;
    }
  }

  ///
  ///男或女 Icon
  ///
  ///[gender] 性別
  Widget _maleOrFemaleIcon({required int gender}) {
    return gender == 1
    //女
        ? const Icon(
      FontAwesome5Solid.venus,
      size: 23,
      color: AppColor.femaleColor,
    )
    //男
        : const Icon(FontAwesome5Solid.mars,
        size: 23, color: AppColor.maleColor);
  }
}