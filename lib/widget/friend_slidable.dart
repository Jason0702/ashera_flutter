
import 'package:ashera_flutter/utils/app_color.dart';
import 'package:flutter/material.dart';
import 'package:flutter_font_icons/flutter_font_icons.dart';
import 'package:flutter_slidable/flutter_slidable.dart';

import '../generated/l10n.dart';

class FriendSlidable extends StatefulWidget {
  final Widget child;
  final Function(BuildContext) blockade;
  final Function(BuildContext) delete;

  const FriendSlidable({
    Key? key,
    required this.blockade,
    required this.delete,
    required this.child
  }) : super(key: key);

  @override
  State createState() => _FriendSlidableState();
}

class _FriendSlidableState extends State<FriendSlidable> {
  @override
  Widget build(BuildContext context) {
    return Slidable(
        endActionPane: ActionPane(
          motion: const DrawerMotion(),
          dragDismissible: false,
          dismissible: DismissiblePane(
            onDismissed: () {},
          ),
          children: [
            SlidableAction(
              onPressed: widget.blockade,
              backgroundColor: AppColor.appTitleBarTextColor,
              foregroundColor: Colors.white,
              icon: FontAwesome5Solid.ban,
              label: S.of(context).blockade,
            ),
            SlidableAction(
              onPressed: widget.delete,
              backgroundColor: AppColor.lightRed,
              foregroundColor: Colors.white,
              icon: Icons.delete,
              label: S.of(context).delete,
            ),
          ],
        ),
        child: widget.child);
  }
}
