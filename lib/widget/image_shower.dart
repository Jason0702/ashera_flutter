import 'dart:developer';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../utils/api.dart';
import '../utils/util.dart';

class ImageShower extends StatefulWidget {
  const ImageShower({Key? key, required this.data, required this.index})
      : super(key: key);
  final List data;
  final int? index;

  @override
  _ImageShowerState createState() => _ImageShowerState();
}

class _ImageShowerState extends State<ImageShower> {
  List? get data => widget.data;
  int _index = 0;

  _onLayoutDone(_) async {
  }

  @override
  void initState() {
    super.initState();
    _index = widget.index ?? 0;
    WidgetsBinding.instance.addPostFrameCallback(_onLayoutDone);
  }

  @override
  Widget build(BuildContext context) {
    return BackButtonListener(
        child: Scaffold(
          resizeToAvoidBottomInset: true,
          backgroundColor: Colors.black,
          body: SafeArea(
            top: false,
            bottom: false,
            child: Stack(
              children: [
                Container(
                  padding: EdgeInsets.symmetric(vertical: 30.h),
                  child: PhotoView(
                    loadingBuilder: (context, event) => const Padding(
                      padding: EdgeInsets.all(10.0),
                      child: CircularProgressIndicator(
                        color: Colors.blueAccent,
                      ),
                    ),
                    minScale: 0.2,
                    imageProvider: CachedNetworkImageProvider(
                        Util().getMemberMugshotUrl(data![_index]),
                      headers: {"authorization": "Bearer " + Api.accessToken},),
                  ),
                ),
                Positioned(
                    top: 20,
                    child: Container(
                      height: 15.w,
                      width: Device.width,
                      color: Colors.black54,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            "${_index + 1}/${data?.length}",
                            textScaleFactor: 1,
                            style: const TextStyle(color: Colors.white),
                          )
                        ],
                      ),
                    )),
                Positioned(
                  left: 10,
                  top: 30,
                  child: GestureDetector(
                      onTap: () {
                        Navigator.of(context).pop();
                      },
                      child: Icon(
                        Icons.arrow_back_ios_rounded,
                        color: Colors.white,
                        size: 10.w,
                      )),
                ),
                Positioned(
                  bottom: 0,
                  child: Container(
                    padding: const EdgeInsets.symmetric(vertical: 5),
                    color: Colors.black54,
                    width: Device.boxConstraints.maxWidth,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        _index + 1 != 1
                            ? GestureDetector(
                                onTap: () {
                                  indexMinus();
                                },
                                child: const Icon(
                                  Icons.arrow_back_ios_rounded,
                                  size: 80,
                                  color: Colors.white,
                                ),
                              )
                            : const SizedBox(
                                width: 60,
                              ),
                        _index + 1 != widget.data.length
                            ? GestureDetector(
                                onTap: () {
                                  indexPlus();
                                },
                                child: const Icon(
                                  Icons.arrow_forward_ios_rounded,
                                  size: 80,
                                  color: Colors.white,
                                ),
                              )
                            : const SizedBox(
                                width: 60,
                              ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ),
        onBackButtonPressed: () => pop());
  }

  void indexPlus() {
    //log("index++ $_index");
    _index = _index + 1;
    setState(() {});
  }

  void indexMinus() {
    //log("index-- $_index");
    _index = _index - 1;
    setState(() {});
  }

  Future<bool> pop() async {
    Navigator.of(context, rootNavigator: true).pop();
    return true;
  }
}
