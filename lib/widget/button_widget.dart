import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';
import '../utils/app_color.dart';

Widget buttonWidget(String text) {
  return Container(
    alignment: Alignment.center,
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(50),
        gradient: AppColor.appMainColor),
    child: FittedBox(
      child: Text(
        text,
        style: TextStyle(
            color: AppColor.buttonTextColor,
            fontSize: 18.sp,
            fontWeight: FontWeight.bold),
      ),
    ),
  );
}

Widget buttonWidgetWaiting(String text) {
  return Container(
    alignment: Alignment.center,
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(30),
        border: Border.all(color: AppColor.buttonFrameColor,width: 1),
        color: Colors.transparent),
    child: FittedBox(
      child: Text(
        text,
        style: TextStyle(
            color:AppColor.buttonFrameColor,
            fontSize: 18.sp,
            fontWeight: FontWeight.bold),
      ),
    ),
  );
}

Widget buttonWidgetDispose(String text) {
  return Container(
    alignment: Alignment.center,
    decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(50),
       color: AppColor.buttonFrameDisposeColor),
    child: FittedBox(
      child: Text(
        text,
        style: TextStyle(
            color: AppColor.buttonTextColor,
            fontSize: 18.sp,
            fontWeight: FontWeight.bold),
      ),
    ),
  );
}
///使用方法
/*
//送出
Widget _sendButton(){
  return GestureDetector(
      onTap: () => _sendOnTap(),
      child: Container(
          height: 7.h,
          width: 80.w,
          margin: EdgeInsets.symmetric(vertical: 5.h),
          child: buttonWidget(S.of(context).send))
  );
}
*/
