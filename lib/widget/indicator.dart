import 'package:flutter/material.dart';

class Indicator extends StatelessWidget{
  ///PageView的控制器
  final PageController controller;
  ///指示器個數
  final int itemCount;
  ///普通的顏色
  final Color normalColor;
  ///選中的顏色
  final Color selectedColor;
  ///點的大小
  final double size = 8.0;
  ///點的間距
  final double spacing = 4.0;

  const Indicator({
    Key? key,
    required this.controller,
    required this.itemCount,
    required this.normalColor,
    required this.selectedColor,
  }): super(key: key);

  ///點的Widget
  Widget _buildIndicator({required int index, required int pageCount, required double dotSize, required double spacing}){
    //是否是當前頁面被選中
    bool isCurrentPageSelected = index == (controller.page != null ? controller.page!.round() % pageCount : 0);

    return SizedBox(
      height: size,
        width: size + (2 * spacing),
      child: Center(
        child: Material(
          color: isCurrentPageSelected ? selectedColor : normalColor,
          type: MaterialType.circle,
          child: SizedBox(
            width: dotSize,
            height: dotSize,
          ),
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: List<Widget>.generate(itemCount, (index) => _buildIndicator(index: index, pageCount: itemCount, dotSize: size, spacing: spacing)),
    );
  }
}