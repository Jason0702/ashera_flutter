import 'package:flutter/material.dart';
import 'package:flutter_font_icons/flutter_font_icons.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../generated/l10n.dart';
import '../models/peekaboo_record_model.dart';
import '../utils/app_color.dart';
import '../../utils/util.dart';
import 'mugshot_widget.dart';


class PeekabooRecordFromListCard extends StatefulWidget{
  final PeekabooRecordModel model;
  const PeekabooRecordFromListCard({Key? key, required this.model}):super(key: key);

  @override
  State<StatefulWidget> createState() => _PeekabooRecordFromListCardState();
}

class _PeekabooRecordFromListCardState extends State<PeekabooRecordFromListCard> with AutomaticKeepAliveClientMixin{

  PeekabooRecordModel get model => widget.model;

  @override
  bool get wantKeepAlive => true;


  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      decoration: const BoxDecoration(
          color: Colors.white,
          border: Border(bottom: BorderSide(width: 1, color: AppColor.grayLine))),
      child: Row(
        children: [
          //頭像
          MugshotWidget(
            fromMemberId: 0,
            targetMemberId: model.targetMemberId!,
            nickName: model.targetMemberNickname!,
            gender: model.targetMemberGender!,
            birthday: model.targetMemberBirthday!,
            interactive: true,),
          Expanded(
            child: Container(
              margin: const EdgeInsets.only(left: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  //名稱&時間
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      //名稱
                      FittedBox(
                        child: Text(
                          model.targetMemberNickname!,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 18.sp,
                              color: Colors.black),
                        ),
                      ),
                      //時間
                      FittedBox(
                        alignment: Alignment.topCenter,
                        child: Text(
                          _getTime(model.createdAt!
                              .ceil()),
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 15.sp,
                              color: AppColor.grayText),
                        ),
                      ),
                    ],
                  ),
                  //地址
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      //icon
                      const Icon(
                        Ionicons.ios_location_sharp,
                        color: AppColor.yellowIcon,
                      ),
                      Expanded(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              //地址
                              Expanded(
                                  child: Text(
                                    model.address!,
                                    maxLines: 3,
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 16.sp,
                                        color: AppColor.grayText),
                                  )),
                              SizedBox(width: 2.w),
                              //捕捉被捕捉
                              FittedBox(
                                child: Text(
                                  S.of(context).gotcha,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 15.sp,
                                      color: Colors.black),
                                ),
                              )
                            ],
                          ))
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

class PeekabooRecordTargetListCard extends StatefulWidget{
  final PeekabooRecordModel model;
  const PeekabooRecordTargetListCard({Key? key, required this.model}): super(key: key);
  @override
  State<StatefulWidget> createState() => _PeekabooRecordTargetListCardState();
}

class _PeekabooRecordTargetListCardState extends State<PeekabooRecordTargetListCard> with AutomaticKeepAliveClientMixin {
  PeekabooRecordModel get model => widget.model;

  @override
  bool get wantKeepAlive => true;

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Container(
      padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 5),
      decoration: const BoxDecoration(
          color: Colors.white,
          border:
          Border(bottom: BorderSide(width: 1, color: AppColor.grayLine))),
      child: Row(
        children: [
          //頭像
          MugshotWidget(
            fromMemberId: 0,
            targetMemberId: model.fromMemberId!,
            nickName: model.fromMemberNickname!,
            gender: model.fromMemberGender!,
            birthday: model.fromMemberBirthday!,
            interactive: true,),
          Expanded(
            child: Container(
              margin: const EdgeInsets.only(left: 10),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  //名稱&時間
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      //名稱
                      FittedBox(
                        child: Text(
                          model.fromMemberNickname!,
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 18.sp,
                              color: Colors.black),
                        ),
                      ),
                      //時間
                      FittedBox(
                        alignment: Alignment.topCenter,
                        child: Text(
                          _getTime(model.createdAt!.ceil()),
                          style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 15.sp,
                              color: AppColor.grayText),
                        ),
                      ),
                    ],
                  ),
                  //地址
                  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      //icon
                      const Icon(
                        Ionicons.ios_location_sharp,
                        color: AppColor.yellowIcon,
                      ),
                      Expanded(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              //地址
                              Expanded(child: Text(
                                model.address!,
                                maxLines: 3,
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16.sp,
                                    color: AppColor.grayText),
                              )),
                              SizedBox(width: 2.w),
                              //捕捉被捕捉
                              FittedBox(
                                child: Text(
                                  S.of(context).be_gotcha,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 15.sp,
                                      color: Colors.black),
                                ),
                              ),
                            ],
                          ))
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

String _getTime(int _time) {
  DateTime _createdAt =
  DateTime.fromMillisecondsSinceEpoch(_time * 1000, isUtc: false);
  return Util().visitRecordTime(_createdAt);
}