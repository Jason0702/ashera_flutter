import 'dart:developer';

import 'package:ashera_flutter/utils/app_image.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../models/member_model.dart';
import '../provider/stomp_client_provider.dart';
import '../utils/api.dart';
import '../utils/util.dart';
//好友
Widget avatarImage(int index, StompClientProvider friend,List<MemberModel> items) {
  if(items[index].mugshot != null){
    if (items[index].mugshot!.isEmpty) {
      return noAvatarImage();
    } else {
      return Container(
      width: 16.w,
      height: 8.h,
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        image: DecorationImage(
            fit: BoxFit.cover,
            image: CachedNetworkImageProvider(
              Util().getMemberMugshotUrl(items[index].mugshot!),
              headers: {"authorization": "Bearer " + Api.accessToken},
            ),
            onError: (error, stackTrace) {
              debugPrint('Error: ${error.toString()}');
              if (error.toString().contains('statusCode: 404')) {
                friend.setFriendAvatarNull(items[index].name!);
              }
            }),
      ),
    );
    } //有照片
  }else{
    //log('item: ${items[index].toJson()}');
   return noAvatarImage();//沒照片
  }
}
//通知
Widget wantAddMeAvatarImage(int index, StompClientProvider notice){
  return notice.wantAddMeList[index].memberModel!.mugshot!.isEmpty
      ? noAvatarImage()
      : Container(
    width: 16.w,
    height: 8.h,
    decoration: BoxDecoration(
      shape: BoxShape.circle,
      image: DecorationImage(
          fit: BoxFit.cover,
          image: CachedNetworkImageProvider(
            Util().getMemberMugshotUrl(notice.wantAddMeList[index].memberModel!.mugshot!),
            headers: {"authorization": "Bearer " + Api.accessToken},),
          onError: (error, stackTrace){
            debugPrint('Error: ${error.toString()}');
            if(error.toString().contains('statusCode: 404')){

            }
          }
      ),
    ),
  );
}
//新增好友
Widget followerAvatarImage(int index, StompClientProvider follower){
  return follower.followerList[index].followerModel!.mugshot!.isEmpty
      ? noAvatarImage()
      : Container(
    width: 16.w,
    height: 8.h,
    decoration: BoxDecoration(
      shape: BoxShape.circle,
      image: DecorationImage(
          fit: BoxFit.cover,
          image: CachedNetworkImageProvider(
            Util().getMemberMugshotUrl(follower.followerList[index].followerModel!.mugshot!),
            headers: {"authorization": "Bearer " + Api.accessToken},),
          onError: (error, stackTrace){
            debugPrint('Error: ${error.toString()}');
            if(error.toString().contains('statusCode: 404')){

            }
          }
      ),
    ),
  );
}
//我
Widget meAvatarImage(member){
  return member.mugshot!.isEmpty
      ? noAvatarImage() //沒照片
      : SizedBox(
    width: 20.w,
    height: 10.h,
    child: CachedNetworkImage(
        imageUrl: Util().getMemberMugshotUrl(member.mugshot!),
        httpHeaders: {"authorization": "Bearer ${Api.accessToken}"},
        imageBuilder: (context, image){
          return Container(
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              image: DecorationImage(
                fit: BoxFit.cover,
                image: image
              )

            ),
          );
        },
    ),
  );//有照片
}
//互動用大頭照
Widget interactiveAvatarImage(member, int version){
  return member.interactivePic!.isEmpty
      ? noAvatarImage() //沒照片
      : Container(
    width: 20.w,
    height: 10.h,
    decoration: BoxDecoration(
      shape: BoxShape.circle,
      image: DecorationImage(
          fit: BoxFit.cover,
          image: CachedNetworkImageProvider(
            Util().getMemberMugshotUrl(member.interactivePic!) + '?v=$version',
            headers: {"authorization": "Bearer " + Api.accessToken}, )),
    ),
  );//有照片
}

Widget customerServiceAvatarImage(member){
  return member.mugshot!.isEmpty
      ? noAvatarImage() //沒照片
      : Container(
    width: 16.w,
    height: 8.h,
    decoration: BoxDecoration(
      shape: BoxShape.circle,
      image: DecorationImage(
          fit: BoxFit.cover,
          image: CachedNetworkImageProvider(
            Util().getMemberMugshotUrl(member.mugshot!),
            headers: {"authorization": "Bearer " + Api.accessToken},)),
    ),
  );//有照片
}

//沒有大頭照UI統一
Widget noAvatarImage(){
  return Container(
    width: 16.w,
    height: 8.h,
    decoration: const BoxDecoration(
        shape: BoxShape.circle),
    child: CircleAvatar(
      backgroundColor: Colors.transparent,
      child: Image(
        image: AssetImage(AppImage.iconCat),
      ),
    ),
  );
}