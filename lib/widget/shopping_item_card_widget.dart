import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:provider/provider.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../provider/game_home_provider.dart';
import '../utils/app_image.dart';
import '../utils/util.dart';

class ShoppingItemCardWidget extends StatefulWidget {
  final int index;
  const ShoppingItemCardWidget({Key? key, required this.index})
      : super(key: key);

  @override
  State createState() => _ShoppingItemCardWidgetState();
}

class _ShoppingItemCardWidgetState extends State<ShoppingItemCardWidget>
    with AutomaticKeepAliveClientMixin<ShoppingItemCardWidget> {
  @override
  bool get wantKeepAlive => true;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);
    return Consumer<GameHomeProvider>(builder: (context, home, _) {
      return GestureDetector(
        onTap: () {
          if (EasyLoading.isShow) {
            return;
          }
          if (home.itemIndex != widget.index) {
            home.setItemIndex(value: widget.index);
          }
        },
        child: Container(
          height: 25.w,
          width: 25.w,
          margin: EdgeInsets.symmetric(horizontal: 2.w, vertical: 1.h),
          padding: EdgeInsets.symmetric(horizontal: 3.w),
          alignment: Alignment.center,
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(5),
              border: Border.all(
                  color: home.itemIndex == widget.index
                      ? Colors.orange
                      : Colors.white,
                  width: 4),
              color: Colors.white),
          child: Column(
            children: [
              SizedBox(
                  width: 17.w,
                  height: 7.h,
                  child: _builderWidget(Util()
                      .getMemberMugshotUrl(home.showList[widget.index].pic))),
              Expanded(
                  child: Container(
                child: home.showList[widget.index].isBuy
                    ? _furnitureIsUsed(
                        isUsed: home.showList[widget.index].isUsed)
                    : _diamondAndCost(cost: home.showList[widget.index].cost),
              ))
            ],
          ),
        ),
      );
    });
  }

  ///鑽石與價格
  /// * [cost] 價格
  Widget _diamondAndCost({required int cost}) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        SizedBox(
            width: 5.w,
            child: Image(
              gaplessPlayback: true,
              image: AssetImage(AppImage.iconDiamond),
            )),
        Text(
          '$cost',
          style: const TextStyle(fontWeight: FontWeight.bold),
        ),
      ],
    );
  }

  ///是否使用中
  /// * [isUsed] 使用
  Widget _furnitureIsUsed({required bool isUsed}) {
    if (isUsed) {
      //使用中
      return Image(
        width: 6.w,
        gaplessPlayback: true,
        image: AssetImage(AppImage.iconBought),
      );
    } else {
      //未使用
      return Image(
        width: 6.w,
        gaplessPlayback: true,
        image: AssetImage(AppImage.iconBoughtGray),
      );
    }
  }

  ///圖片加載判斷
  Widget _builderWidget(String snapshot) {
    if (snapshot.toString().contains('https')) {
      return CachedNetworkImage(
        imageUrl: snapshot.toString(),
        fit: BoxFit.contain,
      );
    } else {
      return Image(
        gaplessPlayback: true,
        fit: BoxFit.contain,
        image: FileImage(File(snapshot.toString())),
      );
    }
  }
}
