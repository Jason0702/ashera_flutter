import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../app.dart';
import '../utils/app_color.dart';
import '../utils/app_image.dart';
import '../utils/app_size.dart';

//返回 與 標題
Widget titleBar(String _title, [VoidCallback? titleOnClick]) {
  return Container(
    height: AppSize.titleBarH,
    width: Device.boxConstraints.maxWidth,
    decoration: const BoxDecoration(
      color: Colors.white,
      boxShadow: [
        BoxShadow(
            color: Colors.grey,
            offset: Offset(0.0, 0.5), //陰影y軸偏移量
            blurRadius: 1, //陰影模糊程度
            spreadRadius: 1 //陰影擴散程度
        )
      ],
    ),
    child: Stack(
      alignment: Alignment.center,
      children: [
        //標題文字
        FittedBox(
          child: Text(
            _title,
            style: TextStyle(
                color: AppColor.appTitleBarTextColor,
                fontSize: 21.sp,
                fontWeight: FontWeight.bold),
          ),
        ),
        Positioned(
            left: 5.w,
            child: GestureDetector(
              onTap: () => {delegate.popRoute()},
              child: const Icon(
                Icons.arrow_back,
                color: AppColor.appTitleBarTextColor,
                size: 30,
              ),
            ))
      ],
    ),
  );
}

Widget titleBarChatRoom(String _title, [VoidCallback? titleOnClick]) {
  return Container(
    height: AppSize.titleBarH,
    width: Device.boxConstraints.maxWidth,
    decoration: const BoxDecoration(
      color: Colors.white,
      boxShadow: [
        BoxShadow(
            color: Colors.grey,
            offset: Offset(0.0, 0.5), //陰影y軸偏移量
            blurRadius: 1, //陰影模糊程度
            spreadRadius: 1 //陰影擴散程度
        )
      ],
    ),
    child: Stack(
      alignment: Alignment.center,
      children: [
        //標題文字
        GestureDetector(
          onTap: () => titleOnClick != null ? titleOnClick() : null,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              FittedBox(
                child: Text(
                  _title,
                  style: TextStyle(
                      color: AppColor.appTitleBarTextColor,
                      fontSize: 21.sp,
                      fontWeight: FontWeight.bold),
                ),
              ),
              SizedBox(width: 2.w,),
              Image(
                width: 4.w,
                image: AssetImage(AppImage.iconEdit),
              )
            ],
          ),
        ),

        Positioned(
            left: 5.w,
            child: GestureDetector(
              onTap: () => {delegate.popRoute()},
              child: const Icon(
                Icons.arrow_back,
                color: AppColor.appTitleBarTextColor,
                size: 30,
              ),
            ))
      ],
    ),
  );
}

//標題 跟 左邊右邊list 自定義icon
Widget titleBarIntegrate(List<Widget> _left, String _title, List<Widget> _right){
  return Container(
    height: AppSize.titleBarH,
    width: Device.boxConstraints.maxWidth,
    decoration: const BoxDecoration(
      color: Colors.white,
      boxShadow: [
        BoxShadow(
            color: Colors.grey,
            offset: Offset(0.0, 0.5), //陰影y軸偏移量
            blurRadius: 1, //陰影模糊程度
            spreadRadius: 1 //陰影擴散程度
        )
      ],
    ),
    child: Stack(
      alignment: Alignment.center,
      children: [
        //左按鈕
        if(_left.isNotEmpty)
        Positioned(left: 5.w,child: Row(
          children: _left,
        )),
        //標題文字
        FittedBox(
          child: Text(
            _title,
            style: TextStyle(
                color: AppColor.appTitleBarTextColor,
                fontSize: 21.sp,
                fontWeight: FontWeight.bold),
          ),
        ),
        //右按紐
        if(_right.isNotEmpty)
        Positioned(right: 5.w,child: Row(
          children: _right,
        )),
      ],
    ),
  );
}

//標題 跟 左邊右邊list 自定義icon
Widget titleBarChatIntegrate(List<Widget> _left, String _title, List<Widget> _right, [VoidCallback? titleOnClick, bool? isCustomerService]){
  return Container(
    height: AppSize.titleBarH,
    width: Device.boxConstraints.maxWidth,
    decoration: const BoxDecoration(
      color: Colors.white,
      boxShadow: [
        BoxShadow(
            color: Colors.grey,
            offset: Offset(0.0, 0.5), //陰影y軸偏移量
            blurRadius: 1, //陰影模糊程度
            spreadRadius: 1 //陰影擴散程度
        )
      ],
    ),
    child: Stack(
      alignment: Alignment.center,
      children: [
        //左按鈕
        if(_left.isNotEmpty)
          Positioned(left: 5.w,child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: _left,
          )),
        //標題文字
        GestureDetector(
          onTap: () => titleOnClick != null ? titleOnClick() : null,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              FittedBox(
                child: Text(
                  _title,
                  style: TextStyle(
                      color: AppColor.appTitleBarTextColor,
                      fontSize: 21.sp,
                      fontWeight: FontWeight.bold),
                ),
              ),
              if(isCustomerService == null)
              SizedBox(width: 2.w,),
              if(isCustomerService == null)
              Image(
                width: 4.w,
                image: AssetImage(AppImage.iconEdit),
              )
            ],
          ),
        ),
        //右按紐
        if(_right.isNotEmpty)
          Positioned(right: 5.w,child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: _right,
          )),
      ],
    ),
  );
}