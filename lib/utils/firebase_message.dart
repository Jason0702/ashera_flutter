import 'dart:developer';
import 'dart:io';

import 'package:ashera_flutter/config.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

import '../app.dart';

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
FlutterLocalNotificationsPlugin();

const AndroidNotificationChannel channel = AndroidNotificationChannel(
    'ahsera_channel',//id
    'ashera_Notifications',//title
    importance: Importance.max,
    sound: RawResourceAndroidNotificationSound('y1197'),
    playSound: true
);
const AndroidInitializationSettings initializationSettingsAndroid =  AndroidInitializationSettings('@mipmap/ic_launcher');
const IOSInitializationSettings initializationSettingsIOS =  IOSInitializationSettings(
    requestAlertPermission: true,
    requestBadgePermission: true,
    requestSoundPermission: true,
    /*onDidReceiveLocalNotification: onDidReceiveLocalNotification*/);

const InitializationSettings initializationSettings = InitializationSettings(
    android: initializationSettingsAndroid,
    iOS: initializationSettingsIOS);

FirebaseMessaging messaging = FirebaseMessaging.instance;

class FirebaseMessage {
  ///Firebase init
  Future<void> fireBaseInit() async{
    await Firebase.initializeApp();
    await flutterLocalNotificationsPlugin.initialize(initializationSettings);

    messaging.setForegroundNotificationPresentationOptions(
      alert: true,
      badge: true,
      sound: true
    );

    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<AndroidFlutterLocalNotificationsPlugin>()
        ?.createNotificationChannel(channel);

    ///Firebase 前台監聽
    FirebaseMessaging.onMessage.listen(_firebaseMessagingHandler);


    ///
    //FirebaseMessaging.onMessageOpenedApp.listen(_firebaseMessagingOpenedHandler);

  }

  /// 用戶是否同意推播權限
  /// * authorized 用戶授予權限
  /// * denied 用戶拒絕權限
  /// * notDetermined 用戶尚未選擇是否授予權限
  /// * provisional 用戶授予臨時權限
  void iosSetting() async {
    NotificationSettings settings = await messaging.requestPermission(
        alert: true,
        announcement: false,
        badge: true,
        carPlay: false,
        criticalAlert: false,
        provisional: false,
        sound: true
    );

    //log('用戶是否同意推播權限: ${settings.authorizationStatus}');
  }
  ///取得FirebaseToken
  Future<String?> getToken() async {
    return await messaging.getToken();
  }
}

Future<void> _firebaseMessagingHandler(RemoteMessage message) async {
  //log('Got a message whilst in the foreground!');
  //log('message: ${message.notification!.toMap()}');
  //log('Message data: ${message.data}');
  if(message.notification != null){
    if(message.notification!.body != null){
      if(message.notification!.body!.contains('同意您的交友')){
        friend.fire('refreshFriend');
      }
      if(message.notification!.body!.contains('辨識')){
        friend.fire(Config.refreshFacesDetectHistory);
      }
    }
  }


  if(Platform.isAndroid){
    RemoteNotification? notification = message.notification;
    if (notification != null) {
      AndroidNotification? android = notification.android;
      await flutterLocalNotificationsPlugin.show(
          message.messageId.hashCode,
          notification.title,
          notification.body,
          NotificationDetails(
              android: AndroidNotificationDetails(
                  channel.id,
                  channel.name,
                  icon: android?.smallIcon,
                  sound: channel.sound,
                  playSound: true
              )
          ));
    }
  }
}

Future<void> _firebaseMessagingOpenedHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
  //log("Handling a Opened message: ${message.messageId}");
  //log("Handling a Opened message: ${message.data}");
  //log("Handling a Opened message: ${message.notification!.toMap()}");

  /*RemoteNotification? notification = message.notification;
  if (notification != null) {
    AndroidNotification? android = notification.android;
    await flutterLocalNotificationsPlugin.show(
        message.messageId.hashCode,
        notification.title,
        notification.body,
        NotificationDetails(
            android: AndroidNotificationDetails(
                channel.id,
                channel.name,
                icon: android?.smallIcon
            )
        ));
  }*/
}



