import 'package:ashera_flutter/utils/util.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferenceUtil{
  //accessToken
  static const String accessToken = 'accessToken';
  //refreshToken
  static const String refreshToken = 'refreshToken';
  //refreshTime
  static const String refreshTime = 'refreshTime';
  //memberId
  static const String memberId = 'memberId';
  //帳號
  static const String account = 'account';
  //密碼
  static const String password = 'password';

  //保存Token
  void saveToken(String _accessToken, String _refreshToken){
    saveAccessToken(_accessToken);
    saveRefreshToken(_refreshToken);
  }

  //保存accessToken
  void saveAccessToken(String _accessToken) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString(accessToken, _accessToken);
  }

  //保存refreshToken
  void saveRefreshToken(String _refreshToken) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString(refreshToken, _refreshToken);
  }

  //讀取Token
  Future<Map<String, String>> getToken() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    Map<String, String> _map = {
      accessToken: sharedPreferences.getString(accessToken) ?? '',
      refreshToken: sharedPreferences.getString(refreshToken) ?? ''
    };
    return _map;
  }

  //讀取accessToken
  Future<String> getAccessToken() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString(accessToken) ?? '';
  }

  //讀取refreshToken
  Future<String> getRefreshToken() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString(refreshToken) ?? '';
  }

  //刪除Token
  void delToken() {
    delAccessToken();
    delRefreshToken();
  }

  //刪除accessToken
  void delAccessToken() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.remove(accessToken);
  }

  //刪除refreshToken
  void delRefreshToken() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.remove(refreshToken);
  }

  //保存時間
  void saveRefreshTime() async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString(refreshTime, Util().dateFormat.format(DateTime.now()));
  }
  //讀取時間
  Future<DateTime> getRefreshTime() async {
    SharedPreferences sharedPreference = await SharedPreferences.getInstance();
    return DateTime.parse(sharedPreference.getString(refreshTime) ?? '');
  }

  //保存會員ID
  void saveMemberId(int _id) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setInt(memberId, _id);
  }
  //讀取會員ID
  Future<int> getMemberId() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getInt(memberId) ?? 0;
  }
  //刪除會員ID
  void delMemberId() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.remove(memberId);
  }
  
  //保存帳號與密碼
  void saveAccountAndPassword(String _account, String _password){
    saveAccount(_account);
    savePassword(_password);
  }

  //保存帳號
  void saveAccount(String _account) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString(account, _account);
  }
  
  //保存密碼
  void savePassword(String _password) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setString(password, _password);
  }

  Future<Map<String, dynamic>> getAccountAndPassword() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    Map<String, dynamic> _map = {
    'name': sharedPreferences.getString(account) ?? '',
    'password': sharedPreferences.getString(password) ?? ''
    };
    return _map;
  }
  
  //取得帳號
  Future<String> getAccount() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString(account) ?? '';
  }
  //取得密碼
  Future<String> getPassword() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getString(password) ?? '';
  }
  //刪除帳號與密碼
  void delAccountAndPassword() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.remove(account);
    sharedPreferences.remove(password);
  }
  //保存會員訊息紀錄讀取到第幾頁
  void saveUserMessageRecord(String _name, int _page) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setInt(_name, _page);
  }
  //讀取會員訊息紀錄讀取到第幾頁
  Future<int> getUserMessageRecord(String _name) async{
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getInt(_name) ?? 0;
  }

  //保存總頁數
  void saveUserAllMessageRecord(String _name, int _page) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    sharedPreferences.setInt(_name, _page);
  }

  //讀取總頁數
  Future<int> getUserAllMessageRecord(String _name) async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    return sharedPreferences.getInt(_name) ?? 0;
  }

}