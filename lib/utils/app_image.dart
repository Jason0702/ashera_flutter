class AppImage{
  //粉色Logo
  static String logo = 'assets/images/logo.png';
  //白色Logo
  static String logoWhite = 'assets/images/logo_white.png';
  //登入頁 人物相片
  static String imgLogin = 'assets/images/img_login.png';
  //選單 意見
  static String iconSuggestion = 'assets/images/menu_icon/icon_suggestion.png';
  //選單 繽紛社交
  static String iconSocial = 'assets/images/menu_icon/icon_social.png';
  //選單 辨識紀錄
  static String iconIdentityRecord = 'assets/images/menu_icon/icon_identity_record.png';
  //底部選項 好友
  //選中
  static String iconFriendPressed = 'assets/images/bottom_navigation_bar_Icon/icon_friend_pressed.png';
  //未選中
  static String iconFriendNormal = 'assets/images/bottom_navigation_bar_Icon/icon_friend_normal.png';
  //底部選項 聊天
  //選中
  static String iconChatPressed = 'assets/images/bottom_navigation_bar_Icon/icon_chat_pressed.png';
  //未選中
  static String iconChatNormal = 'assets/images/bottom_navigation_bar_Icon/icon_chat_normal.png';
  //底部選項 遊戲
  //選中
  static String iconGamePressed = 'assets/images/bottom_navigation_bar_Icon/icon_game_pressed.png';
  //未選中
  static String iconGameNormal = 'assets/images/bottom_navigation_bar_Icon/icon_game_normal.png';
  //底部選項 我
  //選中
  static String iconMePressed = 'assets/images/bottom_navigation_bar_Icon/icon_me_pressed.png';
  //未選中
  static String iconMeNormal = 'assets/images/bottom_navigation_bar_Icon/icon_me_normal.png';
  //相機
  static String iconScan = 'assets/images/bottom_navigation_bar_Icon/icon_scan.png';
  //點數Icon
  static String iconDiamond = 'assets/images/icon_diamond.png';
  //貓Icon
  static String iconCat = 'assets/images/icon_cat.png';
  //貓沒點擊Icon
  static String iconCatUnpressed = 'assets/images/icon_cat_unpressed.png';
  //狗Icon
  static String iconDog = 'assets/images/icon_dog.png';
  //狗沒點擊Icon
  static String iconDogUnpressed = 'assets/images/icon_dog_unpressed.png';
  //禮物盒Icon
  static String iconGiftBox = 'assets/images/icon_giftbox.png';
  //躲貓貓紀錄Icon
  static String iconPeekabooRecord = 'assets/images/icon_peekaboo_record.png';
  //躲貓貓獵取名單Icon
  static String iconPeekabooTodayRecord = 'assets/images/icon_peekaboo_today_record.png';
  //列表Icon
  static String iconList = 'assets/images/icon_list.png';
  //地圖Icon
  static String iconMap = 'assets/images/icon_map.png';
  //針Icon
  static String iconPin = 'assets/images/icon_pin.png';
  //星星Icon
  static String iconStar = 'assets/images/icon_star.png';
  //探班紀錄Icon
  static String iconVisitRecord = 'assets/images/icon_visit_record.png';
  //躲貓貓Icon
  static String imgPeekaboo = 'assets/images/img_peekaboo.png';
  //探班Icon
  static String imgVisit = 'assets/images/img_visit.png';
  //大頭針Icon
  static String imgUser = 'assets/images/img_user.png';
  //聊天 Icon
  static String iconChat = 'assets/images/icon_chat.png';
  //No Icon
  static String iconNo = 'assets/images/icon_no.png';
  //位置Icon
  static String iconPosition = 'assets/images/icon_position.png';
  //visit位置Icon
  static String iconVisitLocation = 'assets/images/icon_visit_location.png';
  //visit需求 Icon
  static String iconVisitRequest = 'assets/images/icon_visit_request.png';
  //YES Icon
  static String iconYes = 'assets/images/icon_yes.png';
  //Yes Gray Icon
  static String iconYesGray = 'assets/images/icon_yes_gray.png';
  //開啟APP時的GIF
  static String asheraStart = 'assets/images/ashera_start.gif';
  ///房間遊戲
  //購買按鈕
  static String btnBuy = 'assets/images/game_home/btn_buy.png';
  //取消按鈕
  static String btnCancel = 'assets/images/game_home/btn_cancel.png';
  //清掃
  static String btnClean = 'assets/images/game_home/btn_clean.png';
  //清掃disable
  static String btnCleanDisabled = 'assets/images/game_home/btn_clean_disabled.png';
  //確認按鈕
  static String btnConfirm = 'assets/images/game_home/btn_confirm.png';
  //確認按鈕disable
  static String btnConfirmDisabled = 'assets/images/game_home/btn_confirm_disabled.png';
  //家
  static String btnHome = 'assets/images/game_home/btn_home.png';
  //家 已選擇
  static String btnHomeSelected = 'assets/images/game_home/btn_home_selected.png';
  //購物車
  static String btnMall = 'assets/images/game_home/btn_mall.png';
  //訊息
  static String btnMessage = 'assets/images/game_home/btn_message.png';
  //訊息disable
  static String btnMessageDisabled = 'assets/images/game_home/btn_message_disabled.png';
  //豬重養
  static String btnPetAgain = 'assets/images/game_home/btn_pet_again.png';
  //豬按鈕
  static String btnPig = 'assets/images/game_home/btn_pig.png';
  //豬販售按鈕
  static String btnSell = 'assets/images/game_home/btn_sell.png';
  //豬餵食按鈕
  static String btnFeed = 'assets/images/game_home/btn_feed.png';
  //豬販售按鈕disabled
  static String btnSellSisabled = 'assets/images/game_home/btn_sell_disabled.png';
  //設定
  static String btnSettings = 'assets/images/game_home/btn_settings.png';
  //販售紀錄按鈕
  static String btnSoldHistory = 'assets/images/game_home/btn_sold_history.png';
  //街道
  static String btnStreet = 'assets/images/game_home/btn_street.png';
  //街道選擇
  static String btnStreetSelected = 'assets/images/game_home/btn_street_selected.png';
  //床
  static String iconBed = 'assets/images/game_home/icon_bed.png';
  //床選擇
  static String iconBedSelected = 'assets/images/game_home/icon_bed_selected.png';
  //使用中
  static String iconBought = 'assets/images/game_home/icon_bought.png';
  //未使用
  static String iconBoughtGray = 'assets/images/game_home/icon_bought_gray.png';
  //衣櫃
  static String iconCabinet = 'assets/images/game_home/icon_cabinet.png';
  //衣櫃選擇
  static String iconCabinetSelected = 'assets/images/game_home/icon_cabinet_selected.png';
  //椅子
  static String iconChair = 'assets/images/game_home/icon_chair.png';
  //椅子選擇
  static String iconChairSelected = 'assets/images/game_home/icon_chair_selected.png';
  //圍籬
  static String iconFence = 'assets/images/game_home/icon_fence.png';
  //圍籬選擇
  static String iconFenceSelected = 'assets/images/game_home/icon_fence_selected.png';
  //豬餓了
  static String iconHungry = 'assets/images/game_home/icon_hungry.png';
  //寵物
  static String iconPet = 'assets/images/game_home/icon_pet.png';
  //寵物選擇
  static String iconPetSelected = 'assets/images/game_home/icon_pet_selected.png';
  //狗屋
  static String iconDogHouse = 'assets/images/game_home/icon_dog_house.png';
  //狗屋選擇
  static String iconDogHouseSelected = 'assets/images/game_home/icon_dog_house_selected.png';
  //地毯
  static String iconMat = 'assets/images/game_home/icon_mat.png';
  //地毯選擇
  static String iconMatSelected = 'assets/images/game_home/icon_mat_selected.png';
  //金幣
  static String iconPoint = 'assets/images/game_home/icon_point.png';
  //右側
  static String iconRightArrow = 'assets/images/game_home/icon_right_arrow.png';
  //桌子
  static String iconTable = 'assets/images/game_home/icon_table.png';
  //桌子選擇
  static String iconTableSelected = 'assets/images/game_home/icon_table_selected.png';
  //加
  static String iconTopUp = 'assets/images/game_home/icon_top_up.png';
  //家背景
  static String imgBgHome = 'assets/images/game_home/img_bg_home.jpg';
  //菜單背景
  static String imgBgMenu = 'assets/images/game_home/img_bg_menu.jpg';
  //天空背景
  static String imgBgSky = 'assets/images/game_home/img_bg_sky.jpg';
  //街道背景
  static String imgBgStreet = 'assets/images/game_home/img_bg_street.jpg';
  //椅子0
  static String imgItemsChair0 = 'assets/images/game_home/img_items_chair_0.png';
  //椅子1
  static String imgItemsChair1 = 'assets/images/game_home/img_items_chair_1.png';
  //床0
  static String imgItemsBed0 = 'assets/images/game_home/img_items_bed_0.png';
  //衣櫃0
  static String imgItemsCabinet0 = 'assets/images/game_home/img_items_cabinet_0.png';
  //衣櫃1
  static String imgItemsCabinet1 = 'assets/images/game_home/img_items_cabinet_1.png';
  //狗0
  static String imgItemsDog0 = 'assets/images/game_home/img_items_dog_0.png';
  //狗1
  static String imgItemsDog1 = 'assets/images/game_home/img_items_dog_1.png';
  //狗2
  static String imgItemsDog2 = 'assets/images/game_home/img_items_dog_2.png';
  //圍籬0
  static String imgItemsFence0 = 'assets/images/game_home/img_items_fence_0.png';
  //圍籬1
  static String imgItemsFence1 = 'assets/images/game_home/img_items_fence_1.png';
  //家0
  static String imgItemsHouse0 = 'assets/images/game_home/img_items_house_0.png';
  //家1
  static String imgItemsHouse1 = 'assets/images/game_home/img_items_house_1.png';
  //房間0
  static String imgItemsRoomBg0 = 'assets/images/game_home/img_items_room_bg_0.png';
  //房間1
  static String imgItemsRoomBg1 = 'assets/images/game_home/img_items_room_bg_1.png';
  //大便0
  static String imgItemsShit0 = 'assets/images/game_home/img_items_shit_0.png';
  //大便1
  static String imgItemsShit1 = 'assets/images/game_home/img_items_shit_1.png';
  //桌子0
  static String imgItemsTable0 = 'assets/images/game_home/img_items_table_0.png';
  //桌子1
  static String imgItemsTable1 = 'assets/images/game_home/img_items_table_1.png';
  //豬
  static String imgPig = 'assets/images/game_home/img_pig.png';
  //正在大便
  static String imgPooping = 'assets/images/game_home/img_pooping.jpg';
  //房間EX
  static String imgRoomBgEx = 'assets/images/game_home/img_room_bg_ex.png';
  //烏煙瘴氣
  static String imgSmoke = 'assets/images/game_home/img_smoke.png';
  static String imgSmoke1 = 'assets/images/game_home/img_smoke_3shit.png';
  static String imgSmoke2 = 'assets/images/game_home/img_smoke_4shit.png';
  static String imgSmoke3 = 'assets/images/game_home/img_smoke_5shit.png';
  //返回按鈕
  static String btnMallBack = 'assets/images/game_home/btn_mall_back.png';
  //背景
  static String iconBg = 'assets/images/game_home/icon_bg.png';
  //背景選擇
  static String iconBgSelected = 'assets/images/game_home/icon_bg_selected.png';
  //便便對話框
  static String imgDialog = 'assets/images/game_home/img_dialog.png';
  //列表為空時的等待
  static String imgWaiting = 'assets/images/img_waiting.png';
  //便便&清掃次數的+號
  static String iconTopUpCleanAndPoop = 'assets/images/game_home/icon_top_up_clean_and_poop.png';
  //便便icon
  static String iconPoop = 'assets/images/game_home/icon_poop.png';
  //打掃icon
  static String iconClean = 'assets/images/game_home/icon_clean.png';
  //vipIcon
  static String iconVipMember = 'assets/images/icon_vip_member.png';
  //normalIcon
  static String iconNormalMember = 'assets/images/icon_normal_member.png';
  //vip升級
  static String imgVipDirections = 'assets/images/img_vip_directions.png';
  //充值成功
  static String iconOrderComplete = 'assets/images/icon_order_complete.png';
  //充值失敗
  static String iconOrderFailed = 'assets/images/icon_order_failed.png';
  //修改icon
  static String iconEdit = 'assets/images/icon_edit.png';
  //ashera_AppIcon
  static String iconAshera = 'assets/images/app_icon/ashera_icon.png';
  //Novel_APPIcon
  static String iconNovel = 'assets/images/app_icon/novel_icon.png';
  //TwNews_APPIcon
  static String iconTwNews = 'assets/images/app_icon/twnews_icon.png';
  //互動入口 game
  static String imgGame = 'assets/images/img_game.png';
  //門
  static String btnLeave = 'assets/images/game_home/btn_leave.png';
  //拉屎gif
  static String pooping = 'assets/images/game_home/pooping.gif';
  //遊戲指引
  static String imgGameGuides1 = 'assets/images/game_guidelines/img_game_guides_1.png';
  static String imgGameGuides2 = 'assets/images/game_guidelines/img_game_guides_2.png';
  static String imgGameGuides3 = 'assets/images/game_guidelines/img_game_guides_3.png';
  static String imgGameGuides4 = 'assets/images/game_guidelines/img_game_guides_4.png';
  //被探班取消
  static String iconExit = 'assets/images/icon_exit.png';
  //
  static String loading = 'assets/images/game_home/loading.gif';
  //
  static String iconFilter = 'assets/images/icon_filter.png';
  //去別人房間
  static String btnVisit = 'assets/images/btn_visit.png';
  //點數比例
  static String img3proportion = 'assets/images/img_3proportion.png';
  static String img5proportion = 'assets/images/img_5proportion.png';
  static String img7proportion = 'assets/images/img_7proportion.png';
  static String img9proportion = 'assets/images/img_9proportion.png';

  static String loadingJson = 'assets/json/loading.json';
}