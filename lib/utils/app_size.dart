import 'package:responsive_sizer/responsive_sizer.dart';

class AppSize{
  //AppBarPage
  static double iconSizeW = 8.w;
  static double iconSizeH = 8.h;
  //titleBar高度
  static double titleBarH = 9.h;

  static double buttonH = 7.h;
  static double buttonW = 60.w;
  static double buttonCircular = 50;
}