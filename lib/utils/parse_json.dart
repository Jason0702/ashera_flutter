//followerList JSON 解析隔離
import 'dart:convert';

import 'package:ashera_flutter/models/animal_type_model.dart';
import 'package:ashera_flutter/models/dung_model.dart';
import 'package:ashera_flutter/models/faces_detect_history_model.dart';
import 'package:ashera_flutter/models/friend_name_model.dart';
import 'package:ashera_flutter/models/furniture_model.dart';
import 'package:ashera_flutter/models/system_setting_model.dart';
import 'package:ashera_flutter/utils/util.dart';

import '../enum/app_enum.dart';
import '../models/face_image_model.dart';
import '../models/follower_model.dart';
import '../models/furniture_type_model.dart';
import '../models/game_model.dart';
import '../models/house_model.dart';
import '../models/member_local_model.dart';
import '../models/peekaboo_record_model.dart';
import '../models/pets_model.dart';
import '../models/unlock_payment_record_model.dart';
import '../models/visit_record_model.dart';

List<PetsSellRecordModel> parsePetsSellRecordModel(String response){
  final List<dynamic> _list = json.decode(response);
  return _list.map((e) => PetsSellRecordModel.fromJson(e)).toList();
}

List<MemberDungModel> parseMemberDungModel(String response){
  final List<dynamic> _list = json.decode(response);
  return _list.map((e) => MemberDungModel.fromJson(e)).where((element) => element.used == false).toList();
}

List<MemberCleaningToolsModel> parseMemberCleaningToolsModel(String response){
  final List<dynamic> _list = json.decode(response);
  return _list.map((e) => MemberCleaningToolsModel.fromJson(e)).toList();
}

List<CleaningToolsModel> parseCleaningToolsModel(String response){
  final List<dynamic> _list = json.decode(response);
  return _list.map((e) => CleaningToolsModel.fromJson(e)).toList();
}

List<MemberHouseDungModel> parseMemberHouseDungModel(String response){
  final List<dynamic> _list = json.decode(response);
  return _list.map((e) => MemberHouseDungModel.fromJson(e)).toList();
}

List<MemberHouseMessageModel> parseMemberHouseMessageModel(String response){
  final List<dynamic> _list = json.decode(response);
  return _list.map((e) => MemberHouseMessageModel.fromJson(e)).toList();
}

List<MemberHouseModel> parseMemberHouseModel(String response){
  final List<dynamic> _list = json.decode(response);
  return _list.map((e) => MemberHouseModel.fromJson(e)).toList();
}

List<MemberHouseStyleModel> parseMemberHouseStyleModel(String response){
  final List<dynamic> _list = json.decode(response);
  return _list.map((e) => MemberHouseStyleModel.fromJson(e)).toList();
}

List<MemberPetsModel> parseMemberPetsModel(String response){
  final List<dynamic> _list = json.decode(response);
  return _list.map((e) => MemberPetsModel.fromJson(e)).toList();
}

List<MemberFurnitureModel> parseMemberFurnitureModel(String response){
  final List<dynamic> _list = json.decode(response);
  return _list.map((e) => MemberFurnitureModel.formJson(e)).toList();
}

List<MemberFriendNameModel> parseMemberFriendNameModel(String response){
  final List<dynamic> _list = json.decode(response);
  return _list.map((e) => MemberFriendNameModel.fromJson(e)).toList();
}

SystemSettingModel parseSystemSettingModel(String response){
  return SystemSettingModel.fromJson(json.decode(response));
}

List<FacesDetectHistoryModel> parseFacesDetectHistoryModel(String response){
  final List<dynamic> _list = json.decode(response);
  return _list.map((e) => FacesDetectHistoryModel.fromJson(e)).toList();
}

List<VisitRecordModel> parseVisitRecordModel(String response){
  final List<dynamic> _list = json.decode(response);
  return _list.map((e) => VisitRecordModel.fromJson(e)).toList();
}

/*List<UnlockPaymentRecordModel> parseUnlockPaymentRecordByFromMemberId(
    String response) {
  final List<dynamic> _list = json.decode(response);
  return _list
      .map((e) => UnlockPaymentRecordModel.fromJson(e))
      .where((element) => element. == 1)
      .toList();
}*/

List<PeekabooRecordModel> parseHideAndSeekRecordByStatus(String response) {
  final List<dynamic> _list = json.decode(response);
  return _list.map((e) => PeekabooRecordModel.fromJson(e)).where((element) => element.targetMember!.online == true).toList();
}

List<PeekabooRecordModel> parsePeekabooRecordByTargetMemberName(
    String response) {
  final List _list = json.decode(response);
  return _list
      .map((e) => PeekabooRecordModel.fromJson(e))
      .where((element) =>
          element.status == CatchStatus.catchStatusProcessing.index)
      .toList();
}

List<FaceImageModel> parseMemberByFaceImage(String response) {
  final List<dynamic> _list = json.decode(response);
  return _list.map((e) => FaceImageModel.fromJson(e)).toList();
}

List<FollowerModel> parseFollower(String response) {
  final List<dynamic> list = json.decode(response);
  return list.map((e) => FollowerModel.fromJson(e)).toList();
}

//MemberLocal JSON 解析隔離
List<MemberLocalModel> parseMemberLocal(String response) {
  final List _list = json.decode(response);
  return _list.map((e) => MemberLocalModel.fromJson(e)).where((element) => element.online == true).toList();
}

List<HouseModel> parseHouseModel(String response){
  final List<dynamic> _list = json.decode(response);
  return _list.map((e) => HouseModel.fromJson(e)).where((element) => element.status == 1).toList();
}

List<PetsModel> parsePetsModel(String response){
  final List<dynamic> _list = json.decode(response);
  return _list.map((e) => PetsModel.fromJson(e)).where((element) => element.status == 1).toList();
}

List<FurnitureTypeModel> parseFurnitureTypeModel(String response){
  final List<dynamic> _list = json.decode(response);
  return _list.map((e) => FurnitureTypeModel.fromJson(e)).where((element) => element.status == 1).toList();
}

List<FurnitureModel> parseFurnitureModel(String response){
  final List<dynamic> _list = json.decode(response);
  return _list.map((e) => FurnitureModel.fromJson(e)).where((element) => element.status == 1).toList();
}

List<AnimalTypeModel> parseAnimalTypeModel(String response){
  final List<dynamic> _list = json.decode(response);
  return _list.map((e) => AnimalTypeModel.fromJson(e)).where((element) => element.status == 1).toList();
}

List<DungModel> parseDungModel(String response){
  final List<dynamic> _list = json.decode(response);
  return _list.map((e) => DungModel.fromJson(e)).where((element) => element.status == 1).toList();
}