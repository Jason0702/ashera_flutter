import 'dart:developer';
import 'dart:typed_data';
import 'dart:ui' as ui;
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:image/image.dart' as Images;
import 'package:responsive_sizer/responsive_sizer.dart';

import '../enum/app_enum.dart';
import 'api.dart';
import 'package:http/http.dart' as http;

import 'app_image.dart';

class Util{
  //抽屜的二人高度
  double drawerTwoHeight = 200.0;
  //抽屜的三人高度
  double drawerThreeHeight = 280.0;
  //抽屜的四人高度
  double drawerFourHeight = 350.0;

  final int _tokenExpired = 25; //Token過期時間
  static int nicknameLen = 6; //暱稱字數限制
  static int unblockMugshotPoint = 9; //大頭照解鎖點數
  int get tokenExpired => _tokenExpired;

  bool dialogIsOpen = false;
  bool isShowDogDungDialog = false;


  final DateFormat _sqlDateFormat = DateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
  final DateFormat _dateFormat = DateFormat("yyyy-MM-dd HH:mm:ss");
  DateFormat get dateFormat => _dateFormat;
  static DateFormat dateFormatISO = DateFormat("yyyy-MM-dd");
  static DateFormat dateFormatBirthDay = DateFormat("yyyy/MM/dd");
  static DateFormat dateFormatHHmm = DateFormat("yyyy-MM-dd HH:mm");
  static DateFormat dateDay = DateFormat("MM/dd");
  static NumberFormat formatter = NumberFormat("00");

  //反轉 MessageType
  static final Map<int, MessageType> messageType = {
    0: MessageType.TEXT,
    1: MessageType.AUDIO,
    2: MessageType.VIDEO,
    3: MessageType.PIC,
    4: MessageType.VIDEO_CALL,
    5: MessageType.VOICE_CALL,
  };
  //轉成紀錄顯示格式
  String getDateDateFormatHHmm(int _time){
    DateTime _at = _dateFormat.parse(DateTime.fromMillisecondsSinceEpoch(_time * 1000, isUtc: false).toString());
    return dateFormatHHmm.format(_at);
  }

  //判斷是否是今天
  bool isToDay(int _createAt){
    DateTime _nowTime = dateFormatISO.parse(DateTime.now().toString());
    DateTime _at = dateFormatISO.parse(DateTime.fromMillisecondsSinceEpoch(_createAt * 1000, isUtc: false).toString());
    return _nowTime.compareTo(_at) == 0;
  }
  /// is Week.
  /// 判斷是否是本周
  bool isWeek(int? ms, {bool isUtc = false, int? locMs}) {
    if (ms == null || ms <= 0) {
      return false;
    }
    DateTime _old = DateTime.fromMillisecondsSinceEpoch(ms * 1000, isUtc: isUtc);
    DateTime _now;
    if (locMs != null) {
      _now = getDateTimeByMs(locMs, isUtc: isUtc);
    } else {
      _now = isUtc ? DateTime.now().toUtc() : DateTime.now().toLocal();
    }

    DateTime old =
    _now.millisecondsSinceEpoch > _old.millisecondsSinceEpoch ? _old : _now;
    DateTime now =
    _now.millisecondsSinceEpoch > _old.millisecondsSinceEpoch ? _now : _old;
    return (now.weekday >= old.weekday) &&
        (now.millisecondsSinceEpoch - old.millisecondsSinceEpoch <=
            7 * 24 * 60 * 60 * 1000);
  }

  int getDifference(int _createAt){
    DateTime _nowTime = _dateFormat.parse(DateTime.now().toString());
    DateTime _at = _dateFormat.parse(DateTime.fromMillisecondsSinceEpoch(_createAt * 1000, isUtc: false).toString());
    log('大頭照解鎖天數: ${_nowTime.difference(_at).inDays}');
    return _nowTime.difference(_at).inDays;
  }

  DateTime getDateTimeByMs(int ms, {bool isUtc = false}) {
    return DateTime.fromMillisecondsSinceEpoch(ms, isUtc: isUtc);
  }

  bool isWithinTwentySeconds(int _wannaVisitAt){
    DateTime _nowTime = _dateFormat.parse(DateTime.now().toString());
    DateTime _at = _dateFormat.parse(DateTime.fromMillisecondsSinceEpoch(_wannaVisitAt * 1000, isUtc: false).toString());
    final difference = _nowTime.difference(_at).inSeconds;
    //log('相差多少秒 $difference');
    if(difference > 20 || difference < -5) {
      return false;
    }
    return true;
  }

  //判斷是否是這周
  bool isThisWeek(int _createAt){
    DateTime _nowTime = dateFormatISO.parse(DateTime.now().toString());
    DateTime _at = dateFormatISO.parse(DateTime.fromMillisecondsSinceEpoch(_createAt * 1000, isUtc: false).toString());
    DateTime aWeek = _at.add(const Duration(days: -7));
    //log('Now: $_nowTime aWeek: $aWeek ${_nowTime.isAfter(aWeek)}');
    return _at.isAfter(aWeek);
  }

  String visitRecordTime(DateTime _time){
    return _dateFormat.format(_time);
  }

  //取得生日DateTime
  DateTime getDateTimeBirthDay(String _date){
    return dateFormatISO.parse(_date);
  }

  //註冊用
  DateTime getRegionDateTimeBirthDay(String _date){
    return dateFormatBirthDay.parse(_date);
  }

  //取得生日時轉換
  String getBirthDayFormat(String _date){
    DateTime _birthDay = dateFormatISO.parse(_date);
    return dateFormatBirthDay.format(_birthDay);
  }
  //要上傳生日時轉換
  String upLoadBirthDayFormat(String _date){
    DateTime _birthDay = dateFormatBirthDay.parse(_date);
    return dateFormatISO.format(_birthDay);
  }

  //生日時間轉換
  Map<String, String> getBirthDay(DateTime _date){
    Map<String, String> _map = {};
    _map['showText'] = dateFormatBirthDay.format(_date);
    _map['apiText'] = dateFormatISO.format(_date);
    return _map;
  }
  //時區處理
  DateTime utcTimeToLocal(String time){
    DateTime localDate = _sqlDateFormat.parse(time, true).toLocal();
    return localDate;
  }
  //時區處理String
  String utcTimeToLocalString(String time){
    String localDate = _dateFormat.parse(time, true).toLocal().toString();
    return localDate;
  }
  //聊天系統氣泡時間
  String titleTime(DateTime time){
    String month = formatter.format(time.month);
    String day = formatter.format(time.day);
    return "$month/$day(${chineseWeekDay(time.weekday)})";
  }

  //訊息時間
  String messageTime(String data){
    DateTime time = utcTimeToLocal(data);
    String hour = formatter.format(time.hour);
    String minute = formatter.format(time.minute);
    if(time.hour > 12){
      return " $hour:$minute ";
    }else{
      return " $hour:$minute ";
    }
  }
  //聊天頁 最後一筆訊息時間
  String lastMessageTime(String data){
    DateTime time = utcTimeToLocal(data);
    return dateDay.format(time);
  }

  //星期轉成中文
  String chineseWeekDay(int weekday){
    switch (weekday){
      case 1:
        return "一";
      case 2:
        return "二";
      case 3:
        return "三";
      case 4:
        return "四";
      case 5:
        return "五";
      case 6:
        return "六";
      case 7:
        return "日";
      default:
        return "零";
    }
  }

  //小紅點寬度計算
  double redDotWidth(String num) {
    if (num.length == 1) {
      return 5.w;
    } else if (num.length == 2) {
      return 6.w;
    } else if (num.length == 3) {
      return 8.w;
    } else {
      return 10.w;
    }
  }

  //使用方式
  /*final Uint8List? markerIcon = await Util().makeReceiptImage(utl, AppImage.imgUser);*/
  Future<Uint8List> makeReceiptImage({required String avatar,required String marker}) async {
    ByteData imageData;
    //load avatar image
    if(avatar.contains('http') || avatar.contains('https')){
      imageData = await _getNetworkImage(avatar);
    } else if(avatar.isEmpty) {
      imageData = await rootBundle.load(AppImage.iconCat);
    } else {
      imageData = await rootBundle.load(avatar);
    }
    List<int> bytes = Uint8List.view(imageData.buffer);
    var avatarImage = Images.decodeImage(bytes);

    //load marker image
    imageData = await rootBundle.load(marker);
    bytes = Uint8List.view(imageData.buffer);
    var markerImage = Images.decodeImage(bytes);

    //resize the avatar image to fit inside the marker image
    avatarImage = Images.copyResize(avatarImage!, width: markerImage!.width ~/ 1.2, height: markerImage.height ~/ 1.3);
    var radius = 195;
    int originX = avatarImage.width ~/ 2;
    int originY = avatarImage.height ~/ 2;

    for(int y = -radius; y <= radius; y++){
      for(int x = -radius; x <= radius; x++){
        if(x * x + y * y <= radius * radius){
          markerImage.setPixelSafe(originX + x + 40, originY + y + 30, avatarImage.getPixelSafe(originX + x, originY + y));
        }
      }
    }
    markerImage = Images.copyResize(markerImage, width: 150, height: 160);
    return Uint8List.fromList(Images.encodePng(markerImage));
  }
  final ui.PictureRecorder pictureRecorder = ui.PictureRecorder();

  Future<ByteData> _getNetworkImage(String _avatar) async {
    final Canvas canvas = Canvas(pictureRecorder);
    http.Response response = await http.get(Uri.parse(_avatar), headers: {"authorization": "Bearer " + Api.accessToken});
    Uint8List originalUnit8List = response.bodyBytes;
    var codec = await ui.instantiateImageCodec(originalUnit8List,
        targetHeight: 200, targetWidth: 200);
    var frameInfo = await codec.getNextFrame();
    paintImage(
      fit: BoxFit.cover,
      canvas:canvas,
        rect: const Rect.fromLTWH(0, -40, 200, 200),
        image: frameInfo.image
    );
    final _image = await pictureRecorder.endRecording().toImage(200, 110);
    ByteData? targetByteData = await _image.toByteData(format: ui.ImageByteFormat.png);
    return targetByteData!.buffer.asByteData();
  }

  //手機號碼驗證
  bool phoneVerification(String _phone){
    if(_phone.isEmpty)return false;
    bool _regExp = RegExp(r'^09\d{8}$').hasMatch(_phone);
    return _regExp;
  }

  String getMemberMugshotUrl(String _url){
    if(_url.isEmpty){
      return '';
    }
    return '${Api.baseUrl}${Api.getFile}$_url';
  }

  //年齡計算
  String getAge(String _birthday) {
    int age = 0;
    DateTime _dteBirthday = Util().getDateTimeBirthDay(_birthday);
    DateTime dateTime = DateTime.now();
    if (dateTime.isBefore(_dteBirthday)) {
      return '';
    }
    int yearNow = dateTime.year; //當前年分
    int monthNow = dateTime.month; //當前月份
    int dayOfMonthNow = dateTime.day; //當前日期

    int yearBirth = _dteBirthday.year;
    int monthBirth = _dteBirthday.month;
    int dayOfMonthBirth = _dteBirthday.day;

    age = yearNow - yearBirth; //計算歲數
    if (monthNow <= monthBirth) {
      if (monthNow == monthBirth) {
        if (dayOfMonthNow < dayOfMonthBirth) age--; //當前日期在生日之前 年齡減一
      } else {
        age--; //當前月份在生日之前 年齡減一
      }
    }
    return '$age';
  }

  String getRegionAge(String _birthday){
    int age = 0;
    DateTime _dteBirthday = Util().getRegionDateTimeBirthDay(_birthday);
    DateTime dateTime = DateTime.now();
    if (dateTime.isBefore(_dteBirthday)) {
      return '';
    }
    int yearNow = dateTime.year; //當前年分
    int monthNow = dateTime.month; //當前月份
    int dayOfMonthNow = dateTime.day; //當前日期

    int yearBirth = _dteBirthday.year;
    int monthBirth = _dteBirthday.month;
    int dayOfMonthBirth = _dteBirthday.day;

    age = yearNow - yearBirth; //計算歲數
    if (monthNow <= monthBirth) {
      if (monthNow == monthBirth) {
        if (dayOfMonthNow < dayOfMonthBirth) age--; //當前日期在生日之前 年齡減一
      } else {
        age--; //當前月份在生日之前 年齡減一
      }
    }
    return '$age';
  }

  String poopTime(double startTime){
    DateTime time = _dateFormat.parse(DateTime.fromMillisecondsSinceEpoch(startTime.ceil() * 1000, isUtc: false).toString());
    Duration diff = DateTime.now().difference(time);
    if(diff.inHours < 100){
      return "${formatter.format(diff.inHours)}hr";
    }else{
      return " 99+hr";
    }
  }

  String pigTime(double startTime){
    DateTime time = _dateFormat.parse(DateTime.fromMillisecondsSinceEpoch(startTime.ceil() * 1000, isUtc: false).toString());
    Duration diff = DateTime.now().difference(time);
      return "${formatter.format(diff.inDays).padLeft(2,'0')}天${formatter.format(diff.inHours % 24).padLeft(2,'0')}時${formatter.format((diff.inHours % 24 % 60)).padLeft(2,'0')}分";
  }
  //星座日期轉換
  Horoscope dateToHoroscope(String _date) {
    DateTime time = Util.dateFormatBirthDay.parse(_date);
    Horoscope _horoscope = Horoscope.aquarius;
    int mouth = time.month;
    int day = time.day;
    switch (mouth) {
      case DateTime.january:
        _horoscope =
        day < 21 ? Horoscope.capricorn : Horoscope.aquarius;
        break;
      case DateTime.february:
        _horoscope = day < 20 ? Horoscope.aquarius : Horoscope.pisces;
        break;
      case DateTime.march:
        _horoscope = day < 21 ? Horoscope.pisces : Horoscope.aries;
        break;
      case DateTime.april:
        _horoscope = day < 21 ? Horoscope.aries : Horoscope.taurus;
        break;
      case DateTime.may:
        _horoscope = day < 21 ? Horoscope.taurus : Horoscope.gemini;
        break;
      case DateTime.june:
        _horoscope = day < 22 ? Horoscope.gemini : Horoscope.cancer;
        break;
      case DateTime.july:
        _horoscope = day < 23 ? Horoscope.cancer : Horoscope.leo;
        break;
      case DateTime.august:
        _horoscope = day < 23 ? Horoscope.leo : Horoscope.virgo;
        break;
      case DateTime.september:
        _horoscope = day < 23 ? Horoscope.virgo : Horoscope.libra;
        break;
      case DateTime.october:
        _horoscope = day < 24 ? Horoscope.libra : Horoscope.scorpio;
        break;
      case DateTime.november:
        _horoscope =
        day < 23 ? Horoscope.scorpio : Horoscope.sagittarius;
        break;
      case DateTime.december:
        _horoscope =
        day < 22 ? Horoscope.sagittarius : Horoscope.capricorn;
        break;
    }
    return _horoscope;
  }

  int getAppVersion({required String version}){
    return int.parse(version.replaceAll('.', ''));
  }
}


