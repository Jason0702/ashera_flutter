
class Api{
  ///token
  static String accessToken = '';
  static String refreshToken = '';
  ///base Url
  //static const String baseUrl = 'https://dev.ashera.cc/'; //測試用
  static const String baseUrl = 'https://ashera.cc/'; //正式
  static const String apiMiddleSection = 'ashera-api';
  static const String bonusMiddleSection = 'ashera-payment';
  ///memberLogin
  static const String memberLogin = '$apiMiddleSection/auth/memberLogin';//登入
  ///
  static const String verificationCode = '$apiMiddleSection/auth/verificationCode';//獲取驗證碼
  ///memberRegister
  static const String memberRegister = '$apiMiddleSection/auth/memberRegister';//註冊
  ///refreshToken
  static const String authRefreshToken = '$apiMiddleSection/auth/refreshToken';//刷新Token
  ///會員
  static const String member = '$apiMiddleSection/api/v1/members/my'; //取得
  static const String memberFaceImage = '$apiMiddleSection/api/v1/members'; //取得
  ///追隨者請求
  static const String followerRequest = '$apiMiddleSection/api/v1/follower-request';//請求、通過、獲取請求追隨我的資料、獲取我的追隨者請求列表
  ///意見
  static const String suggestion = '$apiMiddleSection/api/v1/suggestion';
  ///關於
  static const String aboutUs = '$apiMiddleSection/api/v1/about-us/1';
  ///會員點數
  static const String memberPoint = '$apiMiddleSection/api/v1/member-point';
  ///檔案上傳
  static const String fileUpload = '$apiMiddleSection/file/upload';
  ///取得檔案
  static const String getFile = '$apiMiddleSection/upload/';//後面帶檔名
  ///判斷帳號是否重複
  static const String isMemberNameExist = '$apiMiddleSection/api/v1/members/isMemberNameExist/';//後面帶帳號
  ///照片檢測
  static const String rekognitionFaces = '$apiMiddleSection/api/v1/rekognition-faces/detect';
  ///探班紀錄
  static const String visitRecord = '$apiMiddleSection/api/v1/visit-record';
  ///躲貓貓紀錄
  static const String hideAndSeekRecord = '$apiMiddleSection/api/v1/hide-and-seek-record';
  ///解所付費紀錄
  static const String unlockPaymentRecord = '$apiMiddleSection/api/v1/unlock-payment-record';
  ///辨識紀錄
  static const String faceDetectHistory = '$apiMiddleSection/api/v1/faces-detect-history';
  ///fcm test
  static const String firebaseMessagingTest = '$apiMiddleSection/api/v1/firebase-messaging-test';

  ///小遊戲API
  /// 便便
  static const String dung = '$apiMiddleSection/api/v1/dung';
  /// 動物類型
  static const String animalType = '$apiMiddleSection/api/v1/animal-type';
  /// 家具
  static const String furniture = '$apiMiddleSection/api/v1/furniture';
  /// 家具類型
  static const String furnitureType = '$apiMiddleSection/api/v1/furniture-type';
  /// 寵物
  static const String pets = '$apiMiddleSection/api/v1/pets';
  /// 房子
  static const String house = '$apiMiddleSection/api/v1/house';
  /// 房子風格
  static const String houseStyle = '$apiMiddleSection/api/v1/house-style';

  /// 會員便便
  static const String memberDung = '$apiMiddleSection/api/v1/member-dung';
  /// 會員清理工具
  static const String memberCleaningTools = '$apiMiddleSection/api/v1/member-cleaning-tools';
  /// 會員寵物
  static const String memberPets = '$apiMiddleSection/api/v1/member-pets';
  /// 會員的家
  static const String memberHome = '$apiMiddleSection/api/v1/member-house';
  /// 會員家的風格
  static const String memberHomeStyle = '$apiMiddleSection/api/v1/member-house-style';
  /// 會員的家具
  static const String memberFurniture = '$apiMiddleSection/api/v1/member-furniture';

  ///加值API
  ///加值方案
  static const String topUpPlan = '$apiMiddleSection/api/v1/top-up-plan';
  ///加值紀錄
  static const String addPointHistory = '$apiMiddleSection/api/v1/add-point-history';

  ///加值頁面
  static const String yauyangPayment = '$bonusMiddleSection/yauyang-payment';

  ///會員忘記密碼
  static const String forgotPassword = '$apiMiddleSection/auth/forgotPassword';
  ///獲取忘記密碼驗證碼資料
  static const String forgotPasswordVerificationCode = '$apiMiddleSection/auth/forgotPasswordVerificationCode/number/';
  ///好友名稱(備註)
  static const String friendName = '$apiMiddleSection/api/v1/friend-name';
  ///系統設定
  static const String systemSetting = '$apiMiddleSection/api/v1/system-setting/1';
  ///會員家的留言
  static const String houseMessage = '$apiMiddleSection/api/v1/member-house-message';
  ///會員家的大便
  static const String houseDung = '$apiMiddleSection/api/v1/member-house-dung';
  ///會員黑名單
  static const String memberBlacklist = '$apiMiddleSection/api/v1/member-blacklist';
  ///清理工具
  static const String cleaningTools = '$apiMiddleSection/api/v1/cleaning-tools';
  ///清理工具購買紀錄
  static const String cleaningToolsShoppingHistory = '$apiMiddleSection/api/v1/cleaning-tools-shopping-history';
  ///便便購買紀錄
  static const String dungShoppingHistory = '$apiMiddleSection/api/v1/dung-shopping-history';
  ///會員遊戲設定
  static const String memberGameSetting = '$apiMiddleSection/api/v1/member-game-setting';
  ///寵物出售紀錄
  static const String pestSellRecord = '$apiMiddleSection/api/v1/pets-sell-record';

  ///遊戲設定
  static const String gameSetting = '$apiMiddleSection/api/v1/game-setting';

  ///恢復大便次數
  static const String recoverShitTimes = '$apiMiddleSection/api/v1/member-point/recoverShitTimes/memberId';
  ///恢復清理次數
  static const String recoverCleanTimes = '$apiMiddleSection/api/v1/member-point/recoverCleanTimes/memberId';

  ///檢舉
  static const String complaint = '$apiMiddleSection/api/v1/complaint';
  ///檢舉紀錄
  static const String complaintRecord = '$apiMiddleSection/api/v1/complaint-record';
  ///google
  static const String getProduct = '$apiMiddleSection/api/v1/google-auth/getProduct';
  ///apple
  static const String iosPay = '$apiMiddleSection/api/v1/apple-auth/iosPay';
  ///會員取消訂閱
  static const String vipUnSubscription = '$apiMiddleSection/api/v1/members/my';
}

class Stomp{
  //static const String baseUrl = 'http://dev.ashera.cc:9002/my-websocket';//測試用
  static const String baseUrl = 'http://ashera.cc:9002/my-websocket'; //正式
  //接收
  static const String p2pMsg = '/user/queue/p2pMsg'; //點對點訊息通道
  static const String p2pMsgResp = '/user/queue/p2pMsgResp';//點對點自己的
  static const String p2pGetLastChatMsgResp = '/user/queue/p2pGetLastChatMsgResp';//取得最後聊天訊息回傳
  static const String p2pGetChatMsgResp = '/user/queue/p2pGetChatMsgResp';//點對點聊天紀錄
  static const String p2pMember = '/user/queue/p2pMember'; //點對點會員資料通道
  static const String followMe = '/user/queue/followMe'; //點對點發送消息，將資料發送到指定用戶
  static const String p2pFollowRequest = '/user/queue/p2pFollowRequest'; //請求好友資料通道
  static const String p2pAcceptFollowRequest = '/user/queue/p2pAcceptFollowRequest'; //允許好友資料通道
  static const String p2pUpdateUnreadChatMsgResp = '/user/queue/p2pUpdateUnreadChatMsgResp';// 更新未讀聊天訊息回傳
  static const String p2pServerPushMsg = '/user/queue/p2pServerPushMsg'; // 服務器推送訊息
  static const String p2pError = '/user/queue/p2pError';//Error
  /**
  * 探班
  * */
  ///想要探班資料通道
  static const String p2pWannaVisit = '/user/queue/p2pWannaVisit';
  ///探班評分資料通道
  static const String p2pRateVisit = '/user/queue/p2pRateVisit';
  ///同意探班資料通道
  static const String p2pApproveVisit = '/user/queue/p2pApproveVisit';
  ///取消探班資料通道
  static const String p2pCancelVisit = '/user/queue/p2pCancelVisit';
  ///同意取消探班資料通道
  static const String p2pAgreeToCancelVisit = '/user/queue/p2pAgreeToCancelVisit';
  ///探班會員取消探班資料通道
  static const String p2pCancelByFromMemberVisit = '/user/queue/p2pCancelByFromMemberVisit';
  ///進行中探班會員取消探班資料通道
  static const String p2pCancelByFromMemberInProcessingVisit = '/user/queue/p2pCancelByFromMemberInProcessingVisit';
  ///重配對探班資料通道
  static const String p2pRePairingVisit = '/user/queue/p2pRePairingVisit';
  ///請求取消探班資料通道
  static const String p2pCancelBackInWaitingCancel = '/user/queue/p2pCancelBackInWaitingCancel';
  ///發送
  static const String p2pVisitChatMsgResp = '/user/queue/p2pVisitChatMsgResp';
  ///對方發送的消息
  static const String p2pVisitMsg = '/user/queue/p2pVisitMsg';
  ///取得聊天紀錄
  static const String p2pGetVisitChatMsgResp = '/user/queue/p2pGetVisitChatMsgResp';
  ///取得最後一筆
  static const String p2pGetVisitLastChatMsgResp = '/user/queue/p2pGetLastVisitChatMsgResp';
  ///更新已讀
  static const String p2pUpdateUnreadVisitChatMsgResp = '/user/queue/p2pUpdateUnreadVisitChatMsgResp';
  /**
  * 躲貓貓
  */
  ///躲貓貓資料通道
  static const String p2pHideAndSeek = '/user/queue/p2pHideAndSeek';
  ///躲貓貓抓取資料通道
  static const String p2pDoCatchHideAndSeek = '/user/queue/p2pDoCatchHideAndSeek';

  /**
   * 解鎖
   **/
  ///解鎖
  static const String p2pUnlock = '/user/queue/p2pUnlock';
  /**
  * 小遊戲
  **/
  ///購買資料通道
  static const String p2pShopping = '/user/queue/p2pShopping';
  static const String p2pStuff = '/user/queue/p2pStuff';
  ///大便清除狀態
  static const String p2pClear = '/user/queue/p2pClear';

  ///充值資料通道
  static const String p2pPayment = '/user/queue/p2pPayment';



  ///發送
  ///點對點發送消息，將資料發送到指定用戶
  static const String getMemberInfo = '/app/getMemberInfo';
  ///發送訊息給指定用戶
  static const String peer2PeerMessage = '/app/p2pMsg';
  static const String broadcastMessage = '/app/broadcastMessage';
  ///取得好友
  static const String getFollowMe = '/app/followMe';
  ///取得紀錄
  static const String getP2pMsg = '/app/getChatMessage';
  ///取得最後聊天訊息
  static const String getLastChatMessage = '/app/getLastChatMessage';
  ///取得幾筆聊天紀錄
  static const String getChatMessagePage = '/app/getChatMessagePageDesc';
  ///請求好友資料通道
  static const String getAddFollowerRequest = '/app/addFollowerRequest';
  ///允許好友資料通道
  static const String getAcceptFollowerRequest = '/app/acceptFollowerRequest';
  ///新增 更新未讀聊天訊息回傳
  static const String updateChatMessageIsRead = '/app/updateChatMessageIsRead';
  ///更新會員資料
  static const String updateMemberInfo = '/app/updateMemberInfo';
  ///更新會員密碼
  static const String updateMemberPassword = '/app/updateMemberPassword';
  ///更新會員大頭照
  static const String updateMemberMugshot = '/app/updateMemberMugshot';
  ///追隨者請求
  static const String getFollowerMe = '/app/followerMe';
  ///追隨者請求
  static const String getMyFollower = '/app/myFollower';
  ///
  static const String deleteFollowerByMemberIdAndFollowerId = '/app/deleteFollowerByMemberIdAndFollowerId';
  /**
  * 探班
  * */
  ///我要探班(wannaVisitDTO)
  static const String wannaVisit = '/app/wannaVisit';
  ///同意探班(id)
  static const String approveVisit = '/app/approveVisit';
  ///取消探班
  static const String cancelVisit = '/app/cancelVisit';
  ///重新配對
  static const String reParingVisit = '/app/rePairingVisit';
  ///探班會員取消
  static const String cancelByFromMember = '/app/cancelVisitByFromMemberVisit';
  ///在進行中被 被探班會員 取消
  static const String cancelRecordByTargetMemberInProcessing = '/app/cancelVisitRecordByTargetMemberInProcessing';
  ///在進行中被 探班會員 取消
  static const String cancelRecordByFromMemberInProcessing = '/app/cancelVisitRecordByFromMemberInProcessing';
  ///同意取消探班
  static const String agreeToCancelVisit = '/app/agreeToCancelVisit';
  ///探班會員評分
  static const String fromMemberRateVisit = '/app/fromMemberRateVisit';
  ///被探班會員評分
  static const String targetMemberRateVisit = '/app/targetMemberRateVisit';
  ///探班會員 請求取消
  static const String cancelBackByFromMember = '/app/cancelBackByFromMember';
  ///被探班會員 請求取消
  static const String cancelBackByTargetMember = '/app/cancelBackByTargetMember';
  ///探班會員同意取消探班
  static const String agreeToCancelByFromMember = '/app/agreeToCancelByFromMember';
  ///被探班會員同意取消
  static const String agreeToCancelByTargetMember = '/app/agreeToCancelByTargetMember';
  ///發送訊息給指定用戶
  static const String peer2PeerVisitMessage = '/app/p2pVisitChatMsg';
  ///取得紀錄
  static const String getP2pVisitMsg = '/app/getVisitChatMessage';
  ///取得最後聊天訊息
  static const String getVisitLastChatMessage = '/app/getLastVisitChatMessage';
  ///取得幾筆聊天紀錄
  static const String getVisitChatMessagePage = '/app/getVisitChatMessageByVisitRecordId';
  ///新增 更新未讀聊天訊息回傳
  static const String updateVisitChatMessageIsRead = '/app/updateVisitChatMessageIsRead';
  /**
  * 躲貓貓
  * */
  ///更新會員位置
  static const String updateMemberLocation = '/app/updateMemberLocation';
  //設定躲貓貓上線狀態
  static const String updateHideStatus = '/app/updateHideStatus';
  ///設定躲貓貓抓取類型
  static const String updateHideType = '/app/updateHideType';
  ///抓取
  static const String doCatchHideAndSeek = '/app/doCatchHideAndSeek';
  ///開始
  static const String startHideAndSeek = '/app/startHideAndSeek';
  ///取消
  static const String cancelHideAndSeek = '/app/cancelHideAndSeek';

  /**
  * 解鎖頭像
  * */
  ///解鎖大頭照
  static const String unlockMugshot = '/app/unlockMugshot';

  /**
   * 小遊戲
   */
  ///會員初始遊戲
  static const String memberInitGame = '/app/memberInitGame';
  ///新增大便
  static const String addMemberDung = '/app/addMemberDung';
  ///使用大便
  static const String useMemberDung = '/app/useMemberDung';
  ///購買房子
  static const String buyHouse = '/app/addMemberHouse';
  ///購買
  static const String buyHouseStyle = '/app/addMemberHouseStyle';
  ///購買家具
  static const String buyFurniture = '/app/addMemberFurniture';
  ///購買寵物
  static const String buyPets = '/app/addMemberPets';
  ///改變大便
  static const String changeMemberDung = '/app/changeMemberDung';
  ///改變家具
  static const String changeMemberFurniture = '/app/changeMemberFurniture';
  ///改變房子
  static const String changeMemberHouse = '/app/changeMemberHouse';
  ///改變房子風格
  static const String changeMemberHouseStyle = '/app/changeMemberHouseStyle';
  ///改變寵物
  static const String changeMemberPets = '/app/changeMemberPets';
  ///買清理工具
  static const String addMemberCleaningTools = '/app/addMemberCleaningTools';
  ///使用清理工具
  static const String useMemberCleaningToolsForMemberHouseDung = '/app/useMemberCleaningToolsForOneMemberHouseDung';
  ///寵物狀態
  static const String checkPetsStatus = '/app/checkPetsStatus';
  ///出售寵物
  static const String sellPets = '/app/sellPets';
  ///寵物加HP
  static const String addMemberPetsHp = '/app/addMemberPetsHp';
  ///大便狀態
  static const String checkMemberDefaultDung = '/app/checkMemberDefaultDung';
  ///讀取會員家中訊息
  static const String readMemberHouseMessage = '/app/readMemberHouseMessage';
  ///清潔工具狀態
  static const String checkMemberDefaultCleaningTools = '/app/checkMemberDefaultCleaningTools';
  ///清除會員家中單一大便
  static const String clearOneMemberHouseDung = '/app/clearOneMemberHouseDung';
  /**
  * VIP
  */
  ///會員升級
  static const String memberUpgrade = '/app/memberUpgrade';

  ///上傳檔案
  //static const String webSocketBinary = 'ws://dev.ashera.cc:9002/ws/binary';//上傳檔案
  static const String webSocketBinary = 'ws://ashera.cc:9002/ws/binary';
}