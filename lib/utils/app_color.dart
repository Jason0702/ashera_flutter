import 'package:flutter/cupertino.dart';

class AppColor{
  static LinearGradient appMainColor = const LinearGradient(
    colors: [_appMainTopColor, _appMainBottomColor],
    begin: Alignment.topCenter,
    end: Alignment.bottomCenter
  );

  static const Color _appMainTopColor = Color(0xFFEF8B6B);
  static const Color _appMainBottomColor = Color(0xFFE66F8A);
  static const Color appFaceBackgroundColor = Color(0xFFFFB114);
  static const Color appBackgroundColor = Color(0xFFF6F6F7);
  static const Color appTitleBarTextColor = Color(0xFF000000);
  static const Color addButtonBackgroundColor = Color(0xFF303030);
  static const Color buttonFrameColor = Color(0xFFE57285);
  static const Color buttonTextColor = Color(0xFFFFFFFF);
  static const Color yellowButton = Color(0xFFFDB015);
  static const Color grayLine = Color(0xFFDEDEDE);
  static const Color grayText = Color(0xFFC4C4C4);
  static const Color grayFrame = Color(0xFFEFEFEF);
  static const Color maleColor = Color(0xFF30AFE8);
  static const Color femaleColor = Color(0xFFF65B76);
  static const Color yellowIcon = Color(0xFFFFB114);
  static const Color yellowStar = Color(0xFFFFE600);
  static const Color buttonFrameDisposeColor = Color(0xFFA4A4A4);
  static const Color watterRippleColor = Color(0xFFB73B4E);
  static const Color brownColor = Color(0xFF9B5C34);
  static const Color beigeColor = Color(0xFFFAF0E8);
  static const Color gooseYellowColor = Color(0xFFFDF4A9);
  static const Color pinkRedColor = Color(0xFFE77385);
  static const Color meColor = Color(0xFF5A5A5A);
  static const Color brightGreenColor = Color(0xFF00E070);
  static const Color orangeColor = Color(0xFFFDA153);
  static const Color lightBlue = Color(0xFF65CEFE);
  static const Color lightRed = Color(0xFFFF010B);
  static const Color meterYellow = Color(0xFFFFF6D1);
  static const Color meterRed = Color(0xFFFFEBEE);
  static const Color meterText = Color(0xFFE18A56);
  static const Color fauxSnow = Color(0xFFFFF9F9);
}